
/*
     Test code for comparing drawing times with the spheres in ...cairo_drawings/spring1.c. This
is using a netbook without a gpu and the fixed pipeline in opengl. This should be a better
comparison with 3d drawing in cairo.
     Unable to use the GTK opengl widget on this computer so there is some extra setup and libraries.
The compiler complains about gtk_widget_set_double_buffered() but it is needed in this case.

Compile with
    gcc -Wall sphereGL.c -o sphereGL -lGL -lGLU -lX11 -lm `pkg-config --cflags --libs gtk+-3.0 gdk-x11-3.0` -lm

    Tested with; GTK3.18 and Ubuntu16.04

    C. Eric Cashon
*/

#include<X11/Xlib.h>
#include<GL/glx.h>
#include<GL/gl.h>
#include<GL/glu.h>
#include<gtk/gtk.h>
#include<gdk/gdkx.h>
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<math.h>

struct point3d{
  float x;
  float y;
  float z;
}point3d;

static void initialize_ball(gint rings, gint points_per_ring);
static void change_drawing(GtkComboBox *combo, gpointer data);
static gboolean start_drawing(GtkWidget *da, cairo_t *cr, gpointer data);
static void draw_wire_sphere();
static void draw_sphere();
static void configureGL(GtkWidget *da, gpointer data);
static gboolean rotate(GtkWidget *da, GdkFrameClock *frame_clock, gpointer data);
static void stop_rotation(GtkWidget *da, gpointer data);
static void scale_drawing(GtkRange *range,  gpointer data);
static void rotation_axis(GtkWidget *axis, gpointer data);
static void close_program(GtkWidget *da);

static Window X_window;
static Display *X_display;
static GLXContext X_context;
static XVisualInfo *X_visual;
static XWindowAttributes X_attributes;
static GLint attributes[] = {GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None};
static float ang=0.0;
static guint tick_id=0;
static float scaleGL=2.5;
static float rotation[]={1.0 , 0.0, 0.0};
static bool rotate_drawing=true;
//Reference Ball.
GArray *ball=NULL;
static gint rings_t=0;
static gint points_per_ring_t=0;
//The current drawing.
static gint drawing_id=0;

int main(int argc, char **argv)
  {
    int x1=0;
    int y1=1;
    int z1=2;
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "SphereGL");
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), 400, 500);

    //Initialize the ball with some different rings and points.
    initialize_ball(20, 20);

    GtkWidget *rotate_menu=gtk_menu_new();
    GtkWidget *rotate_x=gtk_menu_item_new_with_label("Rotate x-axis");
    GtkWidget *rotate_y=gtk_menu_item_new_with_label("Rotate y-axis");
    GtkWidget *rotate_z=gtk_menu_item_new_with_label("Rotate z-axis");
    gtk_menu_shell_append(GTK_MENU_SHELL(rotate_menu), rotate_x);
    gtk_menu_shell_append(GTK_MENU_SHELL(rotate_menu), rotate_y);
    gtk_menu_shell_append(GTK_MENU_SHELL(rotate_menu), rotate_z);
    g_signal_connect(rotate_x, "activate", G_CALLBACK(rotation_axis), &x1);
    g_signal_connect(rotate_y, "activate", G_CALLBACK(rotation_axis), &y1);
    g_signal_connect(rotate_z, "activate", G_CALLBACK(rotation_axis), &z1);
    GtkWidget *menu_bar=gtk_menu_bar_new();
    GtkWidget *rotate_item=gtk_menu_item_new_with_label("Rotate");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(rotate_item), rotate_menu);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), rotate_item);

    GtkWidget *label1=gtk_label_new("OpenGL Drawing Area with Scale Slider");
    gtk_widget_set_hexpand(label1, TRUE);

    GtkWidget *scale1=gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL,1,10,1);
    gtk_widget_set_hexpand(scale1, TRUE);
    gtk_range_set_increments(GTK_RANGE(scale1),1,1);
    g_signal_connect(GTK_RANGE(scale1), "value_changed", G_CALLBACK(scale_drawing), NULL);

    GtkWidget *da=gtk_drawing_area_new();
    G_GNUC_BEGIN_IGNORE_DEPRECATIONS
    gtk_widget_set_double_buffered(da, FALSE);
    G_GNUC_END_IGNORE_DEPRECATIONS
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);
    gtk_widget_add_events(da, GDK_BUTTON_PRESS_MASK);
    g_signal_connect(da, "button-press-event", G_CALLBACK(stop_rotation), NULL);
    g_signal_connect(da, "draw", G_CALLBACK(start_drawing), NULL);
    g_signal_connect(da, "configure-event", G_CALLBACK(configureGL), NULL);

    g_signal_connect(window, "destroy", G_CALLBACK(close_program), da);

    GtkWidget *combo_drawing=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_drawing, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "1", "Wire Sphere");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "2", "Sphere");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
    g_signal_connect(combo_drawing, "changed", G_CALLBACK(change_drawing), da);  

    GtkWidget *grid1=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid1), menu_bar, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), label1, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale1, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_drawing, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), da, 0, 4, 1, 1);

    gtk_container_add(GTK_CONTAINER(window), grid1);

    gtk_widget_show_all(window);

    tick_id=gtk_widget_add_tick_callback(da, (GtkTickCallback)rotate, NULL, NULL);

    gtk_main();

   g_array_free(ball, TRUE);

    return 0;
  }
static void initialize_ball(gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    if(rings<5||rings>50)
      {
        rings=5;
        g_print("Range for rings is 5<=x<=50. Set default rings=5.\n");
      }
    if(points_per_ring<10||points_per_ring>50)
      {
        points_per_ring=16;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=16.\n");
      }
   
    rings_t=rings;
    points_per_ring_t=points_per_ring;
    gint array_size=rings_t*(points_per_ring_t+1)+2;
    struct point3d *p1=NULL;

    if(ball!=NULL) g_array_free(ball, TRUE);
    ball=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(ball, array_size);
    p1=&g_array_index(ball, struct point3d, 0);
          
    gint ring_points=points_per_ring_t+1;
    gdouble arc1=180.0/(gdouble)(rings_t+1);
    gdouble arc2=360.0/(gdouble)(points_per_ring_t);
    for(i=0;i<rings_t;i++)
      {
        for(j=0;j<ring_points;j++)
          {
            (*p1).x=sin(arc1*(gdouble)(i+1)*G_PI/180.0)*cos(arc2*(gdouble)j*G_PI/180.0);
            (*p1).y=sin(arc1*(gdouble)(i+1)*G_PI/180.0)*sin(arc2*(gdouble)j*G_PI/180.0);
            (*p1).z=cos(arc1*(gdouble)(i+1)*G_PI/180.0);
            p1++;
          }
      }
    //Top.
    (*p1).x=0.0;
    (*p1).y=0.0;
    (*p1).z=1.0;
    p1++;
    //Bottom.
    (*p1).x=0.0;
    (*p1).y=0.0;
    (*p1).z=-1.0;
    
  }
static void change_drawing(GtkComboBox *combo, gpointer data)
  {
    drawing_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static gboolean start_drawing(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    if(drawing_id==0)
      {
        glDisable(GL_MAP1_VERTEX_3);
        glDisable(GL_LINE_SMOOTH);
        glDisable(GL_DEPTH_TEST);
        draw_wire_sphere();
      }
    else
      {
        glShadeModel(GL_FLAT);
        glEnable(GL_MAP1_VERTEX_3);
        glEnable(GL_LINE_SMOOTH);
        glEnable(GL_DEPTH_TEST);
        draw_sphere();
      }
  
    return FALSE;
  }
static void draw_wire_sphere()
  {
    GTimer *timer=g_timer_new();
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glPushMatrix();
	
    glRotatef(ang, rotation[0], rotation[1], rotation[2]);

    glScalef(scaleGL, scaleGL, scaleGL);

    gint i=0;
    gint j=0;
    gint array_len=ball->len;
    struct point3d *p1=&g_array_index(ball, struct point3d, 0);
    struct point3d top=g_array_index(ball, struct point3d, array_len-2);
    struct point3d bottom=g_array_index(ball, struct point3d, array_len-1);

    //Draw the latitude rings.
    glLineWidth(2.0);
    glBegin(GL_LINES);
    glColor3f(1.0, 0.0, 1.0);
    for(i=0;i<rings_t;i++)
      {
        for(j=0;j<points_per_ring_t;j++)
          {
            glVertex3f(((*(p1)).x), ((*(p1)).y), ((*(p1)).z));
            glVertex3f(((*(p1+1)).x), ((*(p1+1)).y), ((*(p1)).z)); 
            p1++;
          }
         p1++;  
       } 
    glEnd(); 

    //Draw the longitude arcs from pole to pole.
    glBegin(GL_LINES);
    glColor3f(0.0, 0.0, 1.0);
    gint offset1=points_per_ring_t+1;
    gint offset2=0;
    p1=&g_array_index(ball, struct point3d, 0);
    for(i=0;i<points_per_ring_t;i++)
      {
        glVertex3f(top.x, top.y, top.z);
        for(j=0;j<rings_t;j++)
          {
            offset2=offset1*(j)+i;
            glVertex3f(((*(p1+offset2)).x), ((*(p1+offset2)).y), ((*(p1+offset2)).z));
            glVertex3f(((*(p1+offset2)).x), ((*(p1+offset2)).y), ((*(p1+offset2)).z)); 
          }
        glVertex3f(bottom.x, bottom.y, bottom.z);  
      }
    glEnd();

    glPopMatrix ();
    glXSwapBuffers(X_display, X_window);
    g_print("GL Draw time %f\n", g_timer_elapsed(timer, NULL));
    g_timer_destroy(timer);
  }
static void draw_sphere()
  {
    GTimer *timer=g_timer_new();
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPushMatrix();
	
    glRotatef(ang, rotation[0], rotation[1], rotation[2]);

    glScalef(scaleGL, scaleGL, scaleGL);

    //Color the triangles and quads of the ball.
    gint i=0;
    gint j=0;
    gint counter=0;
    gint array_len=ball->len;
    gint rings_c=rings_t-1;
    gint offset1=points_per_ring_t+2;
    gint offset2=points_per_ring_t+1;
    struct point3d *p1=&g_array_index(ball, struct point3d, 0);
    struct point3d top=g_array_index(ball, struct point3d, array_len-2);
    struct point3d bottom=g_array_index(ball, struct point3d, array_len-1);

    //Draw the top triangles.
    for(i=0;i<points_per_ring_t;i++)
      {
        if(counter%2==0) glColor3f(0.0, 0.0, 0.0);
        else glColor3f(0.0, 1.0, 0.0);        
        glBegin(GL_TRIANGLES);
        glVertex3f(top.x, top.y, top.z);
        glVertex3f(((*(p1)).x), ((*(p1)).y), ((*(p1)).z));
        glVertex3f(((*(p1+1)).x), ((*(p1+1)).y), ((*(p1+1)).z));
        glEnd();
        counter++;
        p1++;
      }

    //Draw middle quads.
    counter++;
    p1=&g_array_index(ball, struct point3d, 0);
    for(i=0;i<rings_c;i++)
      {
        for(j=0;j<points_per_ring_t;j++)
          {
            if(counter%2==0) glColor3f(0.0, 0.0, 0.0);
            else glColor3f(0.0, 1.0, 0.0);
            glBegin(GL_QUADS);
            glVertex3f(((*(p1)).x), ((*(p1)).y), ((*(p1)).z));
            glVertex3f(((*(p1+1)).x), ((*(p1+1)).y), ((*(p1+1)).z));
            glVertex3f(((*(p1+offset1)).x), ((*(p1+offset1)).y), ((*(p1+offset1)).z));
            glVertex3f(((*(p1+offset2)).x), ((*(p1+offset2)).y), ((*(p1+offset2)).z));
            glEnd();
            counter++;
            p1++;
          }
        counter++;
        p1++;
      }

    //Draw the bottom triangles.
    p1=&g_array_index(ball, struct point3d, (rings_t-1)*(points_per_ring_t+1));
    for(i=0;i<points_per_ring_t;i++)
      {
        if(counter%2==0) glColor3f(0.0, 0.0, 0.0);
        else glColor3f(0.0, 1.0, 0.0);       
        glBegin(GL_TRIANGLES);
        glVertex3f(bottom.x, bottom.y, bottom.z);
        glVertex3f(((*(p1)).x), ((*(p1)).y), ((*(p1)).z));
        glVertex3f(((*(p1+1)).x), ((*(p1+1)).y), ((*(p1+1)).z));
        glEnd();          
        counter++;
        p1++;
      }

    glPopMatrix();
    glXSwapBuffers(X_display, X_window);
    g_print("GL Draw time %f\n", g_timer_elapsed(timer, NULL));
    g_timer_destroy(timer);
  }
static void configureGL(GtkWidget *da, gpointer data)
  {
    printf("Congigure GL\n");
    GdkWindow *DrawingWindow=gtk_widget_get_window(GTK_WIDGET(da));

    if(DrawingWindow==NULL)
      {
        printf("Couldn't get GdkWindow\n");
      }
    else
      {
        X_window=gdk_x11_window_get_xid(GDK_WINDOW(DrawingWindow));
        X_display=gdk_x11_get_default_xdisplay();
        X_visual=glXChooseVisual(X_display, 0, attributes);
        X_context=glXCreateContext(X_display, X_visual, NULL, GL_TRUE);
      }

    XGetWindowAttributes(X_display, X_window, &X_attributes);
    glXMakeCurrent(X_display, X_window, X_context);
    XMapWindow(X_display, X_window);
    printf("Viewport %i %i\n", (int)X_attributes.width, (int)X_attributes.height);
    glViewport(0, 0, X_attributes.width, X_attributes.height);
    glOrtho(-10,10,-10,10,-100,100);
    glScalef(2.5, 2.5, 2.5);
  }
static gboolean rotate(GtkWidget *da, GdkFrameClock *frame_clock, gpointer data)
  {
    //Check frame rate.
    gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
    gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
    gint64 start = gdk_frame_clock_get_history_start(frame_clock);
    gint64 history_len=frame-start;
    GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
    gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
    g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
   
    if(rotate_drawing) ang++;

    gtk_widget_queue_draw(da);
    return G_SOURCE_CONTINUE;
  }
static void stop_rotation(GtkWidget *da, gpointer data)
  {
    if(rotate_drawing==true) rotate_drawing=false;
    else rotate_drawing=true;
  }
static void scale_drawing(GtkRange *range,  gpointer data)
  {  
    scaleGL=2.5-gtk_range_get_value(range)/5.0;     
  }
static void rotation_axis(GtkWidget *axis, gpointer data)
  {
    if(*(int*)data==0)
      {
        rotation[0]=1.0;
        rotation[1]=0.0;
        rotation[2]=0.0;
      }
    else if(*(int*)data==1)
      {
        rotation[0]=0.0;
        rotation[1]=1.0;
        rotation[2]=0.0;
      }
    else
      {
        rotation[0]=0.0;
        rotation[1]=0.0;
        rotation[2]=1.0;
      }
  }
static void close_program(GtkWidget *da)
  {
    if(tick_id!=0) gtk_widget_remove_tick_callback(da, tick_id);
    gtk_main_quit();
  }

