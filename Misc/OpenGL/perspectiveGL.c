
/*
    Test some matrices in opengl. The histogram_chart2.c perspective matrix needs to be changed. Match up some matrices and see how histogram_chart2.c can be corrected.

Compile with
    gcc -Wall perspectiveGL.c -o perspectiveGL -lGL -lGLU -lX11 -lm `pkg-config --cflags --libs gtk+-3.0 gdk-x11-3.0` -lm

    Tested with; GTK3.18 and Ubuntu16.04

    C. Eric Cashon
*/

#include<X11/Xlib.h>
#include<GL/glx.h>
#include<GL/gl.h>
#include<GL/glu.h>
#include<gtk/gtk.h>
#include<gdk/gdkx.h>
#include<stdbool.h>
#include<math.h>

static void change_drawing(GtkComboBox *combo, gpointer data);
static gboolean start_drawing(GtkWidget *da, cairo_t *cr, gpointer data);
static void draw_wire_cube(GLsizei w, GLsizei h);
static void configureGL(GtkWidget *da, gpointer data);
static gboolean rotate(GtkWidget *da, GdkFrameClock *frame_clock, gpointer data);
static void stop_rotation(GtkWidget *da, gpointer data);
static void scale_drawing(GtkRange *range,  gpointer data);
static void rotation_axis(GtkWidget *axis, gpointer data);
static void close_program(GtkWidget *da);

static Window X_window;
static Display *X_display;
static GLXContext X_context;
static XVisualInfo *X_visual;
static XWindowAttributes X_attributes;
static GLint attributes[] = {GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None};
static float ang=0.0;
static guint tick_id=0;
static float scaleGL=2.0;
static float rotation[]={1.0 , 0.0, 0.0};
static bool rotate_drawing=true;
//The current drawing.
static gint drawing_id=0;

int main(int argc, char **argv)
  {
    int x1=0;
    int y1=1;
    int z1=2;
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "PerspectiveGL");
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), 400, 500);

    GtkWidget *rotate_menu=gtk_menu_new();
    GtkWidget *rotate_x=gtk_menu_item_new_with_label("Rotate x-axis");
    GtkWidget *rotate_y=gtk_menu_item_new_with_label("Rotate y-axis");
    GtkWidget *rotate_z=gtk_menu_item_new_with_label("Rotate z-axis");
    gtk_menu_shell_append(GTK_MENU_SHELL(rotate_menu), rotate_x);
    gtk_menu_shell_append(GTK_MENU_SHELL(rotate_menu), rotate_y);
    gtk_menu_shell_append(GTK_MENU_SHELL(rotate_menu), rotate_z);
    g_signal_connect(rotate_x, "activate", G_CALLBACK(rotation_axis), &x1);
    g_signal_connect(rotate_y, "activate", G_CALLBACK(rotation_axis), &y1);
    g_signal_connect(rotate_z, "activate", G_CALLBACK(rotation_axis), &z1);
    GtkWidget *menu_bar=gtk_menu_bar_new();
    GtkWidget *rotate_item=gtk_menu_item_new_with_label("Rotate");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(rotate_item), rotate_menu);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), rotate_item);

    GtkWidget *label1=gtk_label_new("Scale and move cube.");
    gtk_widget_set_hexpand(label1, TRUE);

    GtkWidget *scale1=gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 1, 10, 1);
    gtk_widget_set_hexpand(scale1, TRUE);
    gtk_range_set_increments(GTK_RANGE(scale1), 1, 1);
    g_signal_connect(GTK_RANGE(scale1), "value_changed", G_CALLBACK(scale_drawing), NULL);

    GtkWidget *da=gtk_drawing_area_new();
    G_GNUC_BEGIN_IGNORE_DEPRECATIONS
    gtk_widget_set_double_buffered(da, FALSE);
    G_GNUC_END_IGNORE_DEPRECATIONS
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);
    gtk_widget_add_events(da, GDK_BUTTON_PRESS_MASK);
    g_signal_connect(da, "button-press-event", G_CALLBACK(stop_rotation), NULL);
    g_signal_connect(da, "draw", G_CALLBACK(start_drawing), NULL);
    g_signal_connect(da, "configure-event", G_CALLBACK(configureGL), NULL);

    g_signal_connect(window, "destroy", G_CALLBACK(close_program), da);

    GtkWidget *combo_drawing=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_drawing, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "1", "Wire Cube");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
    g_signal_connect(combo_drawing, "changed", G_CALLBACK(change_drawing), da);  

    GtkWidget *grid1=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid1), menu_bar, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), label1, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale1, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_drawing, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), da, 0, 4, 1, 1);

    gtk_container_add(GTK_CONTAINER(window), grid1);

    gtk_widget_show_all(window);

    tick_id=gtk_widget_add_tick_callback(da, (GtkTickCallback)rotate, NULL, NULL);

    gtk_main();

    return 0;
  }
static void change_drawing(GtkComboBox *combo, gpointer data)
  {
    drawing_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static gboolean start_drawing(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    GLsizei w=(GLsizei)gtk_widget_get_allocated_width(da);
    GLsizei h=(GLsizei)gtk_widget_get_allocated_height(da);
    if(drawing_id==0)
      {
        glDisable(GL_MAP1_VERTEX_3);
        glDisable(GL_LINE_SMOOTH);
        glDisable(GL_DEPTH_TEST);
        draw_wire_cube(w, h);
      }
    else g_print("No Drawing\n");
  
    return FALSE;
  }
static void draw_wire_cube(GLsizei w, GLsizei h)
  {
    gfloat m[16];
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glPushMatrix();

    glGetFloatv(GL_MODELVIEW_MATRIX, m);
    g_print("start\n");
    g_print("%f %f %f %f\n%f %f %f %f\n%f %f %f %f\n%f %f %f %f\n", m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8], m[9], m[10], m[11], m[12], m[13], m[14], m[15]);

    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    GLdouble fovy=90.0;
    GLdouble aspect=1.0;
    gdouble zNear=1.0;
    gdouble zFar=1000.0;
    GLdouble xmin, xmax, ymin, ymax;
    ymax=zNear*tan(fovy*M_PI/360.0 );
    ymin=-ymax;
    xmin=ymin*aspect;
    xmax=ymax*aspect;
    glFrustum(xmin, xmax, ymin, ymax, zNear, zFar);
    glGetFloatv(GL_PROJECTION_MATRIX, m);
    g_print("perspective\n");
    g_print("%f %f %f %f\n%f %f %f %f\n%f %f %f %f\n%f %f %f %f\n", m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8], m[9], m[10], m[11], m[12], m[13], m[14], m[15]);
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();
    glTranslatef(0.0, 0.0, -3.0);
    glRotatef(ang, rotation[0], rotation[1], rotation[2]);
    glScalef(scaleGL, scaleGL, scaleGL);
    glGetFloatv(GL_MODELVIEW_MATRIX, m);
    g_print("rotated\n");
    g_print("%f %f %f %f\n%f %f %f %f\n%f %f %f %f\n%f %f %f %f\n", m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8], m[9], m[10], m[11], m[12], m[13], m[14], m[15]);

    //Draw the wire cube.
    glLineWidth(6.0);
    glBegin(GL_LINES);
    glColor3f(0.0, 1.0, 0.0);
    //Back square.
    glVertex3f(-1.0, -1.0, -1.0); 
    glVertex3f(1.0, -1.0, -1.0);
    glVertex3f(1.0, -1.0, -1.0);
    glVertex3f(1.0, 1.0, -1.0);
    glVertex3f(1.0, 1.0, -1.0);
    glVertex3f(-1.0, 1.0, -1.0);
    glVertex3f(-1.0, 1.0, -1.0);
    glVertex3f(-1.0, -1.0, -1.0); 
    //Front square.
    glColor3f(1.0, 0.0, 0.0);
    glVertex3f(-1.0, -1.0, 1.0); 
    glVertex3f(1.0, -1.0, 1.0);
    glVertex3f(1.0, -1.0, 1.0);
    glVertex3f(1.0, 1.0, 1.0);
    glVertex3f(1.0, 1.0, 1.0);
    glVertex3f(-1.0, 1.0, 1.0);
    glVertex3f(-1.0, 1.0, 1.0);
    glVertex3f(-1.0, -1.0, 1.0); 
    //Connecting lines.
    glColor3f(0.0, 0.0, 1.0);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f(1.0, -1.0, -1.0);
    glVertex3f(1.0, -1.0, 1.0);
    glVertex3f(1.0, 1.0, -1.0);
    glVertex3f(1.0, 1.0, 1.0);
    glVertex3f(-1.0, 1.0, -1.0);
    glVertex3f(-1.0, 1.0, 1.0);    
    glEnd();

    glPopMatrix ();
    glXSwapBuffers(X_display, X_window);
  }
static void configureGL(GtkWidget *da, gpointer data)
  {
    printf("Congigure GL\n");
    GdkWindow *DrawingWindow=gtk_widget_get_window(GTK_WIDGET(da));

    if(DrawingWindow==NULL)
      {
        printf("Couldn't get GdkWindow\n");
      }
    else
      {
        X_window=gdk_x11_window_get_xid(GDK_WINDOW(DrawingWindow));
        X_display=gdk_x11_get_default_xdisplay();
        X_visual=glXChooseVisual(X_display, 0, attributes);
        X_context=glXCreateContext(X_display, X_visual, NULL, GL_TRUE);
      }

    XGetWindowAttributes(X_display, X_window, &X_attributes);
    glXMakeCurrent(X_display, X_window, X_context);
    XMapWindow(X_display, X_window);
    printf("Viewport %i %i\n", (int)X_attributes.width, (int)X_attributes.height);
  }
static gboolean rotate(GtkWidget *da, GdkFrameClock *frame_clock, gpointer data)
  {
    if(rotate_drawing) ang+=0.2;

    gtk_widget_queue_draw(da);
    return G_SOURCE_CONTINUE;
  }
static void stop_rotation(GtkWidget *da, gpointer data)
  {
    if(rotate_drawing==true) rotate_drawing=false;
    else rotate_drawing=true;
  }
static void scale_drawing(GtkRange *range,  gpointer data)
  {  
    scaleGL=2.5-gtk_range_get_value(range)/5.0;     
  }
static void rotation_axis(GtkWidget *axis, gpointer data)
  {
    if(*(int*)data==0)
      {
        rotation[0]=1.0;
        rotation[1]=0.0;
        rotation[2]=0.0;
      }
    else if(*(int*)data==1)
      {
        rotation[0]=0.0;
        rotation[1]=1.0;
        rotation[2]=0.0;
      }
    else
      {
        rotation[0]=0.0;
        rotation[1]=0.0;
        rotation[2]=1.0;
      }
  }
static void close_program(GtkWidget *da)
  {
    if(tick_id!=0) gtk_widget_remove_tick_callback(da, tick_id);
    gtk_main_quit();
  }

