
/* 
    Test a 3d fish and turtle with painters algorithm in cairo. 

    gcc -Wall fish2.c -o fish2 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<stdlib.h>

struct point3d{
  gdouble x;
  gdouble y;
  gdouble z;
};

//For sorting planes in the fish drawing. Sort quad and triangle planes.
struct quad_plane{
  gint id;
  gdouble x1;
  gdouble y1;
  gdouble z1;
  gdouble x2;
  gdouble y2;
  gdouble z2;
  gdouble x3;
  gdouble y3;
  gdouble z3;
  gdouble x4;
  gdouble y4;
  gdouble z4;
  gdouble r;
  gdouble g;
  gdouble b;
  gdouble a;
};

//The data arrays for the shape(*s) and shape rotated(*sr).
struct shape{
  gint cache_id;
  GArray *s;
  GArray *sr;
  gint rings;
  gint points_per_ring;
};

//These are the shape cache_id values or basic shapes. Start with a fish.
enum{ 
  FISH,
  TURTLE
};  

//The array to hold the shapes in.
GPtrArray *shape_cache=NULL;

//Combo ids for drawing options.
static gint rotate=0;
static gint drawing_id=0;
static gint initialize_id=0;

//Tick id for animation frame clock.
static guint tick_id=0;

//For the performance check boxes.
static gboolean frame_rate=FALSE;
static gboolean function_time=FALSE;

//Draw the main window transparent.
static gboolean draw_main_window(GtkWidget *window, cairo_t *cr, gpointer data);
//Initialization functions for the shapes.
static void initialize_fish(gint rings, gint points_per_ring);
static void initialize_turtle(gint rings, gint points_per_ring);
static void clear_shape_cache();
//UI functions for sliders and combo boxes.
static void show_frame_rate(GtkToggleButton *button, gpointer data);
static void show_function_time(GtkToggleButton *button, gpointer data);
static void rotate_drawing(GtkComboBox *combo, gpointer *rotations);
static void change_drawing(GtkComboBox *combo, gpointer data);
static void set_rotation_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations);
static void set_rotation_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations);
static void set_rotation_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations);
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer *rotations);
//The start of drawing.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer *rotations);
//Rotation functions.
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9]);
static void rotate_cache_shapes(gdouble qrs[9]);
static void rotate_shape(const struct shape *s1, gdouble qrs[9]);
static void rotate_shape_xyz(const struct shape *s1, gdouble qrs[9]);
static const struct shape* get_drawing_from_shape_cache(gint cache_id, gint rings, gint points_per_ring);
//Draw the shapes.
static void draw_fish(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
static void draw_turtle(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
//For comparing z values of planes. Painters algorithm.
int compare_quads(const void *a, const void *b);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Fish2");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    gtk_widget_set_app_paintable(window, TRUE);
    //Try to set transparency of main window.
    if(gtk_widget_is_composited(window))
      {
        GdkScreen *screen=gtk_widget_get_screen(window);  
        GdkVisual *visual=gdk_screen_get_rgba_visual(screen);
        gtk_widget_set_visual(window, visual);
      }
    else g_print("Can't set window transparency.\n");

    //Initialize the fish. The drawing is setup for a 17, 17 fish. Don't change.
    shape_cache=g_ptr_array_new();
    initialize_fish(17, 17);
    initialize_turtle(16, 16);

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);

    GtkWidget *combo_rotate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_rotate, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 0, "1", "Draw Shape");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 1, "2", "Animate Rotation");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_rotate), 0);

    /*
      The drawing_id is the first column and the initialize_id is the second column. The initialize_id
    is so that you can initialize and draw many different shapes. From 0-12 here cache_id
    is equal to initialize_id.
    */
    GtkWidget *combo_drawing=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_drawing, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "0", "Fish");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "1", "Turtle");
    
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
    g_signal_connect(combo_drawing, "changed", G_CALLBACK(change_drawing), da);  

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);
    
    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider, TRUE);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider, TRUE);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider, TRUE);

    //Set initial rotations. Yaw, roll and pitch.
    gdouble plane_r[3]={0.0, 0.0, 0.0};

    //Setup callbacks.
    gpointer rotations[]={plane_r, da};
    g_signal_connect(da, "draw", G_CALLBACK(draw_main), rotations);
    g_signal_connect(combo_rotate, "changed", G_CALLBACK(rotate_drawing), rotations);
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_rotation_pitch), rotations);   
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_rotation_roll), rotations);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_rotation_yaw), rotations);

    //Some checks for performance.
    GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
    gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
    g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), NULL);

    GtkWidget *check_functions=gtk_check_button_new_with_label("Show Function Time");
    gtk_widget_set_halign(check_functions, GTK_ALIGN_CENTER);
    g_signal_connect(check_functions, "toggled", G_CALLBACK(show_function_time), NULL);

    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_rotate, 0, 0, 3, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_drawing, 0, 1, 3, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_frame, 0, 4, 3, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_functions, 0, 5, 3, 1);

    GtkWidget *scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll, TRUE);
    gtk_widget_set_vexpand(scroll, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll), grid1);
   
    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), scroll, TRUE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 300);

    g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), paned1);
   
    gtk_container_add(GTK_CONTAINER(window), paned1);
    
    gtk_widget_show_all(window);

    gtk_main();

    //Clean up shape cache.
    clear_shape_cache();
    g_ptr_array_free(shape_cache, FALSE);

    return 0;  
  }
static gboolean draw_main_window(GtkWidget *window, cairo_t *cr, gpointer data)
  {
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.3);
    cairo_paint(cr);

    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
    gint width=gtk_paned_get_position(GTK_PANED(data));
    gint height=gtk_widget_get_allocated_height(window);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_rectangle(cr, width, 0.0, 10, height);
    cairo_fill(cr);

    return FALSE;
  }
static void initialize_shape(gint cache_id, gint rings, gint points_per_ring)
  {
    switch(cache_id)
      {
        case FISH:
          initialize_fish(rings, points_per_ring);
          break; 
        case TURTLE:
          initialize_turtle(rings, points_per_ring);
          break;         
        default:
          g_print("Warning: Undefined Shape!\n");
      }
  }
static void initialize_fish(gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    if(rings<5||rings>50)
      {
        rings=5;
        g_print("Range for rings is 5<=x<=50. Set default rings=5.\n");
      }
    if(points_per_ring<10||points_per_ring>50)
      {
        points_per_ring=16;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=16.\n");
      }
   
    //Extra points for head, tail and fins.
    gint array_size=rings*(points_per_ring+1)+9;
    struct point3d *p1=NULL;

    GArray *fish=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(fish, array_size);
    p1=&g_array_index(fish, struct point3d, 0);
          
    GArray *fish_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(fish_rotated, array_size);
          
    gint ring_points=points_per_ring+1;
    gdouble arc1=180.0/(gdouble)(rings+1);
    gdouble arc2=360.0/(gdouble)(points_per_ring);
    for(i=0;i<rings;i++)
      {
        for(j=0;j<ring_points;j++)
          {
            (*p1).x=3.0*cos(arc1*(gdouble)(i+1)*G_PI/180.0);
            (*p1).y=2.0*sin(arc1*(gdouble)(i+1)*G_PI/180.0)*cos(arc2*(gdouble)j*G_PI/180.0);
            (*p1).z=0.5*sin(arc1*(gdouble)(i+1)*G_PI/180.0)*sin(arc2*(gdouble)j*G_PI/180.0);
            p1++;
          }
      }
    //head of ellipse.
    (*p1).x=3.0;
    (*p1).y=0.0;
    (*p1).z=0.0;
    p1++;
    //tail of ellipse.
    (*p1).x=-3.0;
    (*p1).y=0.0;
    (*p1).z=0.0;
    p1++;
    //triangle tail.
    (*p1).x=-3.0;
    (*p1).y=0.0;
    (*p1).z=0.0;
    p1++;
    (*p1).x=-4.0;
    (*p1).y=-2.0;
    (*p1).z=0.0;
    p1++;
    (*p1).x=-4.0;
    (*p1).y=2.0;
    (*p1).z=0.0;
    p1++;
    //Top fin.
    (*p1).x=-0.7;
    (*p1).y=-2.5;
    (*p1).z=0.0;
    p1++;
    (*p1).x=0.0;
    (*p1).y=-1.95;
    (*p1).z=0.0;
    p1++;
    //Bottom fin.
    (*p1).x=-0.7;
    (*p1).y=2.5;
    (*p1).z=0.0;
    p1++;
    (*p1).x=0.0;
    (*p1).y=1.95;
    (*p1).z=0.0;
    
    struct shape *p=g_new(struct shape, 1);
    p->cache_id=FISH;
    p->s=fish;
    p->sr=fish_rotated;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    g_ptr_array_add(shape_cache, p);
  }
static void initialize_turtle(gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    if(rings<5||rings>50)
      {
        rings=5;
        g_print("Range for rings is 5<=x<=50. Set default rings=5.\n");
      }
    if(points_per_ring<10||points_per_ring>50)
      {
        points_per_ring=16;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=16.\n");
      }
   
    //Extra points for head, tail and fins.
    gint array_size=rings*(points_per_ring+1)+6;
    struct point3d *p1=NULL;

    GArray *turtle=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(turtle, array_size);
    p1=&g_array_index(turtle, struct point3d, 0);
          
    GArray *turtle_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(turtle_rotated, array_size);
          
    gint ring_points=points_per_ring+1;
    gdouble arc1=180.0/(gdouble)(rings+1);
    gdouble arc2=360.0/(gdouble)(points_per_ring);
    for(i=0;i<rings;i++)
      {
        for(j=0;j<ring_points;j++)
          {
            if(i==0)
              {
                (*p1).x=3.2*cos(arc1*1.2*G_PI/180.0);
                (*p1).y=1.2*sin(25.0*G_PI/180.0)*cos(arc2*(gdouble)j*G_PI/180.0);
                (*p1).z=0.9*sin(20.0*G_PI/180.0)*sin(arc2*(gdouble)j*G_PI/180.0);
              }
            else if(i<4)
              {
                (*p1).x=3.2*cos(arc1*((gdouble)(i)+1.8)*G_PI/180.0);
                (*p1).y=1.2*sin(30.0*G_PI/180.0)*cos(arc2*(gdouble)j*G_PI/180.0);
                (*p1).z=0.9*sin(30.0*G_PI/180.0)*sin(arc2*(gdouble)j*G_PI/180.0);
              }
            else
              {
                (*p1).x=3.0*cos(arc1*(gdouble)(i+1)*G_PI/180.0);
                (*p1).y=2.0*sin(arc1*(gdouble)(i+1)*G_PI/180.0)*cos(arc2*(gdouble)j*G_PI/180.0);
                (*p1).z=1.0*sin(arc1*(gdouble)(i+1)*G_PI/180.0)*sin(arc2*(gdouble)j*G_PI/180.0);
              }
            p1++;
          }
      }
    //head of ellipse.
    (*p1).x=3.5;
    (*p1).y=0.0;
    (*p1).z=0.0;
    p1++;
    //tail of ellipse.
    (*p1).x=-4.0;
    (*p1).y=0.0;
    (*p1).z=0.0;
    p1++;
    //Right front flipper.
    (*p1).x=3.0;
    (*p1).y=-3.5;
    (*p1).z=0.0;
    p1++;
    //Left front flipper.
    (*p1).x=3.0;
    (*p1).y=3.5;
    (*p1).z=0.0;
    p1++;
    //Right back flipper.
    (*p1).x=-3.0;
    (*p1).y=-2.0;
    (*p1).z=0.0;
    p1++;
    //Left back flipper.
    (*p1).x=-3.0;
    (*p1).y=2.0;
    (*p1).z=0.0;
        
    struct shape *p=g_new(struct shape, 1);
    p->cache_id=TURTLE;
    p->s=turtle;
    p->sr=turtle_rotated;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    g_ptr_array_add(shape_cache, p);
  }
static void clear_shape_cache()
  {
    gint i=0;
    gint length=shape_cache->len;
    struct shape *p=NULL;
    for(i=0;i<length;i++)
      {
        p=g_ptr_array_index(shape_cache, 0);
        g_array_free(p->s, TRUE);
        g_array_free(p->sr, TRUE);
        g_free(p);
        g_ptr_array_remove_index_fast(shape_cache, 0);
      }
  }
static void show_frame_rate(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_function_time(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) function_time=TRUE;
    else function_time=FALSE;
  }
static void rotate_drawing(GtkComboBox *combo, gpointer *rotations)
  {
    rotate=gtk_combo_box_get_active(combo);
    //Reset rotations for animation.
    ((gdouble*)(rotations[0]))[0]=0.0;
    ((gdouble*)(rotations[0]))[1]=0.0;
    ((gdouble*)(rotations[0]))[2]=0.0;
    
    if(rotate==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(rotations[1]), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(rotations[1]));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(rotations[1]), (GtkTickCallback)animate_drawing, rotations, NULL);
          }
      }
    
  }
static void change_drawing(GtkComboBox *combo, gpointer data)
  {
    clear_shape_cache();
    initialize_id=atoi(gtk_combo_box_get_active_id(combo));

    if(initialize_id==FISH) initialize_fish(17, 17);
    else if(initialize_id==TURTLE) initialize_turtle(16, 16);    
    else g_print("Error: Couldn't initialize shapes in drawing! %i\n", initialize_id);   

    drawing_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_rotation_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations)
  {
    if(value>360.0) ((gdouble*)(rotations[0]))[0]=360.0;
    else if(value<0.0) ((gdouble*)(rotations[0]))[0]=0.0;
    else ((gdouble*)(rotations[0]))[0]=value; 

    gtk_widget_queue_draw(GTK_WIDGET(rotations[1]));
  }
static void set_rotation_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations)
  {
    if(value>360.0) ((gdouble*)(rotations[0]))[1]=360.0;
    else if(value<0.0) ((gdouble*)(rotations[0]))[1]=0.0;
    else ((gdouble*)(rotations[0]))[1]=value; 

    gtk_widget_queue_draw(GTK_WIDGET(rotations[1]));
  }
static void set_rotation_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations)
  {
    if(value>360.0) ((gdouble*)(rotations[0]))[2]=360.0;
    else if(value<0.0) ((gdouble*)(rotations[0]))[2]=0.0;
    else ((gdouble*)(rotations[0]))[2]=value; 

    gtk_widget_queue_draw(GTK_WIDGET(rotations[1]));
  }
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer *rotations)
  {
    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    //Apply angles of rotation for animation.
    ((gdouble*)(rotations[0]))[0]+=0.5;
    ((gdouble*)(rotations[0]))[1]+=0.5;
    ((gdouble*)(rotations[0]))[2]+=0.5;

    gtk_widget_queue_draw(drawing);
    return G_SOURCE_CONTINUE;
  }
//The top drawing function.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer *rotations)
  {
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;
    gdouble yaw=((gdouble*)(rotations[0]))[0]*G_PI/180.0;
    gdouble roll=((gdouble*)(rotations[0]))[1]*G_PI/180.0;
    gdouble pitch=((gdouble*)(rotations[0]))[2]*G_PI/180.0;

    //Paint background transparent.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.0);
    cairo_paint(cr);

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 3.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Draw in the center
    cairo_translate(cr, width/2.0, height/2.0);
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);      
 
    GTimer *timer=g_timer_new();
    if(drawing_id==0)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        //cairo_scale(cr, 0.25, 0.25);
        cairo_set_line_width(cr, 0.01); 
        draw_fish(cr, w1, h1, 17, 17);
      }
    else
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_line_width(cr, 1.0); 
        draw_turtle(cr, w1, h1, 16, 16);
      }
    
    if(function_time) g_print("Draw time %f\n", g_timer_elapsed(timer, NULL));
    g_timer_destroy(timer);
     
    return FALSE;
  }
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    GTimer *timer1=g_timer_new();
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);

    if(function_time) g_print("Quaternion time %f\n", g_timer_elapsed(timer1, NULL));
    g_timer_destroy(timer1);
  }
static void rotate_cache_shapes(gdouble qrs[9])
  {
    GTimer *timer2=g_timer_new();
    gint i=0;

    const struct shape *s1=NULL;
    for(i=0;i<(shape_cache->len);i++)
      {
        s1=g_ptr_array_index(shape_cache, i); 
        rotate_shape(s1, qrs);  
      } 

    if(function_time) g_print("Rotation time %f\n", g_timer_elapsed(timer2, NULL));
    g_timer_destroy(timer2);

  }
static void rotate_shape(const struct shape *s1, gdouble qrs[9])
  {
    if(s1->cache_id==FISH)
      {
        rotate_shape_xyz(s1, qrs);
      }
    else if(s1->cache_id==TURTLE)
      {
        rotate_shape_xyz(s1, qrs);
      }     
    else
      {
        g_print("Warning: Couldn't find shape to rotate!\n");
      }
  }
static void rotate_shape_xyz(const struct shape *s1, gdouble qrs[9])
  {
    gint i=0;
    struct point3d *p1=&g_array_index(s1->s, struct point3d, 0);
    struct point3d *p2=&g_array_index(s1->sr, struct point3d, 0);
    for(i=0;i<(s1->s->len);i++)
      {
        (*p2).x=((*p1).x*qrs[0])+((*p1).y*qrs[1])+((*p1).z*qrs[2]);
        (*p2).y=((*p1).x*qrs[3])+((*p1).y*qrs[4])+((*p1).z*qrs[5]);
        (*p2).z=((*p1).x*qrs[6])+((*p1).y*qrs[7])+((*p1).z*qrs[8]);
        p1++,p2++;
      }
  }
static const struct shape* get_drawing_from_shape_cache(gint cache_id, gint rings, gint points_per_ring)
  {
    gint i=0;
    gboolean shape_found=FALSE;

    //Get the drawing from the shape cache.
    struct shape *s1=NULL;

    if(cache_id>=0&&cache_id<=12)
      {
        for(i=0;i<shape_cache->len;i++)
          {
            s1=g_ptr_array_index(shape_cache, i);
            if((s1->cache_id==cache_id)&&(s1->rings==rings)&&(s1->points_per_ring==points_per_ring))
              {
                shape_found=TRUE;
                break;
              }        
          }

        /*
            Couldn't find the drawing in the cache so initialize it and try again. Could just
        auto initialize all shapes.
        */
        if(shape_found==FALSE)
          {
            g_print("Warning: Auto initialize shape! %i %i %i\n", cache_id, rings, points_per_ring);
            initialize_shape(cache_id, rings, points_per_ring);
            for(i=0;i<shape_cache->len;i++)
              {
                s1=g_ptr_array_index(shape_cache, i);
                if(s1->cache_id==cache_id) break;
              }
          }
      }
    else g_print("Warning: Invalid shape cache_id requested!\n");

    return s1;
  }
static void draw_fish(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;
    gint counter=0;

    const struct shape *s1=get_drawing_from_shape_cache(FISH, rings, points_per_ring);

    gint array_len=s1->sr->len;
    gint offset1=(s1->points_per_ring)+2;
    gint offset2=(s1->points_per_ring)+1;
    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    struct point3d head=g_array_index(s1->sr, struct point3d, array_len-9);
    struct point3d tail=g_array_index(s1->sr, struct point3d, array_len-8);
    struct point3d *fin=&g_array_index(s1->sr, struct point3d, array_len-4);

    //An array to sort quads.
    struct quad_plane *quads_start=g_malloc((s1->rings+1)*(s1->points_per_ring)*sizeof(struct quad_plane));
    struct quad_plane *quads=quads_start;

    //Fill the quad struct array.
    for(i=0;i<(s1->rings+1);i++)
      {
        for(j=0;j<(s1->points_per_ring);j++)
          {   
            quads->id=counter;
            counter++;         
            if(i==0)
              {
                quads->x1=((*(p1)).x);
                quads->y1=((*(p1)).y);
                quads->z1=((*(p1)).z);
                quads->x2=((*(p1+1)).x);
                quads->y2=((*(p1+1)).y);
                quads->z2=((*(p1+1)).z);            
                quads->x3=head.x;
                quads->y3=head.y;
                quads->z3=((*(p1)).z);
                quads->x4=head.x;
                quads->y4=head.y;
                quads->z4=0.0; 
                quads->r=0.0;
                quads->g=0.0;
                quads->b=0.0;
                quads->a=1.0;
              }
            else if(i==(s1->rings))
              {
                quads->x1=((*(p1)).x);
                quads->y1=((*(p1)).y);
                quads->z1=((*(p1)).z);
                quads->x2=((*(p1+1)).x);
                quads->y2=((*(p1+1)).y);
                quads->z2=((*(p1+1)).z);            
                quads->x3=tail.x;
                quads->y3=tail.y;
                quads->z3=((*(p1)).z);
                quads->x4=tail.x;
                quads->y4=tail.y;
                //Triangle at ends instead of quad. Set to 0 for qsort comparison.
                quads->z4=0.0;
                quads->r=1.0;
                quads->g=1.0;
                quads->b=0.0;
                quads->a=1.0;              
              }
            else
              {                
                quads->x1=((*(p1)).x);
                quads->y1=((*(p1)).y);
                quads->z1=((*(p1)).z);
                quads->x2=((*(p1+1)).x);
                quads->y2=((*(p1+1)).y);
                quads->z2=((*(p1+1)).z);            
                quads->x3=((*(p1+offset1)).x);
                quads->y3=((*(p1+offset1)).y);
                quads->z3=((*(p1+offset1)).z);
                quads->x4=((*(p1+offset2)).x);
                quads->y4=((*(p1+offset2)).y);
                quads->z4=((*(p1+offset2)).z);
                //The first if sets blue=1 which is then selected for the eye radial pattern.
                if((j==5||j==11)&&i==4)
                  {
                    quads->r=0.0;
                    quads->g=0.0;
                    quads->b=1.0;
                    quads->a=1.0;
                  }
                else if(j==2||j==4||j==6||j==10||j==12||j==14)
                  {
                    quads->r=0.8;
                    quads->g=0.0;
                    quads->b=0.8;
                    quads->a=1.0;
                  }
                else if(j==3||j==5||j==11||j==13)
                  {
                    quads->r=1.0;
                    quads->g=1.0;
                    quads->b=0.0;
                    quads->a=1.0;
                  }
                else
                  {
                    quads->r=1.0;
                    quads->g=0.0;
                    quads->b=0.9;
                    quads->a=0.3;
                  }
              }
            quads++;
            p1++;
          }
        ///Draw first ring twice.
        if(i==0) p1=&g_array_index(s1->sr, struct point3d, 0);
        else p1++;
      }
    
    //Sort the quad plane array based on z values.
    qsort(quads_start, (s1->rings+1)*(s1->points_per_ring), sizeof(struct quad_plane), compare_quads);

    //Draw the sorted quads.
    quads=quads_start;
    cairo_pattern_t *radial1=NULL;
    gdouble eye_x=0.0;
    gdouble eye_y=0.0;
    gboolean pattern_set=FALSE;
    for(i=0;i<(s1->rings+1);i++)
      {
        for(j=0;j<(s1->points_per_ring);j++)
          {
            if(quads->b>=0.95)
              {
                eye_x=w1*(quads->x1+quads->x2+quads->x3+quads->x4)/4.0;
                eye_y=h1*(quads->y1+quads->y2+quads->y3+quads->y4)/4.0;
                radial1=cairo_pattern_create_radial(eye_x, eye_y, 3.0, eye_x, eye_y, 6.0);  
                cairo_pattern_add_color_stop_rgba(radial1, 0.0, 0.0, 0.0, 0.0, 1.0);
                cairo_pattern_add_color_stop_rgba(radial1, 1.0, 1.0, 1.0, 0.0, 1.0);
                cairo_set_source(cr, radial1);
                pattern_set=TRUE;
              }
            else cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
            cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
            cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
            cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
            cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
            cairo_close_path(cr);
            cairo_fill(cr);
            quads++;
            if(pattern_set)
              {
                cairo_pattern_destroy(radial1);
                pattern_set=FALSE;
              } 
          }
      }

    //Draw tail.
    p1=&g_array_index(s1->sr, struct point3d, array_len-7);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_move_to(cr, w1*p1->x, h1*p1->y);
    cairo_line_to(cr, w1*((p1+1)->x), h1*((p1+1)->y));
    cairo_line_to(cr, w1*((p1+2)->x), h1*((p1+2)->y));
    cairo_close_path(cr);
    cairo_fill(cr);
    
    //Draw top fin has three triangles.
    p1=&g_array_index(s1->sr, struct point3d, 170);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_move_to(cr, w1*p1->x, h1*p1->y);
    cairo_line_to(cr, w1*(fin->x), h1*(fin->y));
    cairo_line_to(cr, w1*((fin+1)->x), h1*((fin+1)->y));
    cairo_close_path(cr);
    cairo_fill(cr);

    cairo_move_to(cr, w1*((p1+1)->x), h1*((p1+1)->y));
    cairo_line_to(cr, w1*(fin->x), h1*(fin->y));
    cairo_line_to(cr, w1*((fin+1)->x), h1*((fin+1)->y));
    cairo_close_path(cr);
    cairo_fill(cr);

    cairo_move_to(cr, w1*p1->x, h1*p1->y);
    cairo_line_to(cr, w1*(fin->x), h1*(fin->y));
    cairo_line_to(cr, w1*((p1+1)->x), h1*((p1+1)->y));
    cairo_close_path(cr);
    cairo_fill(cr);

    //Bottom fin has one triangle.
    p1=&g_array_index(s1->sr, struct point3d, 179);
    fin=&g_array_index(s1->sr, struct point3d, array_len-2);
    cairo_move_to(cr, w1*p1->x, h1*p1->y);
    cairo_line_to(cr, w1*(fin->x), h1*(fin->y));
    cairo_line_to(cr, w1*((fin+1)->x), h1*((fin+1)->y));
    cairo_close_path(cr);
    cairo_fill(cr);

    g_free(quads_start);    
  }
static void draw_turtle(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;
    gint counter=0;

    const struct shape *s1=get_drawing_from_shape_cache(TURTLE, rings, points_per_ring);

    gint array_len=s1->sr->len;
    gint offset1=(s1->points_per_ring)+2;
    gint offset2=(s1->points_per_ring)+1;
    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    struct point3d head=g_array_index(s1->sr, struct point3d, array_len-6);
    struct point3d tail=g_array_index(s1->sr, struct point3d, array_len-5);
    struct point3d flipper1=g_array_index(s1->sr, struct point3d, array_len-4);
    struct point3d flipper2=g_array_index(s1->sr, struct point3d, array_len-3);
    struct point3d flipper3=g_array_index(s1->sr, struct point3d, array_len-2);
    struct point3d flipper4=g_array_index(s1->sr, struct point3d, array_len-1);


    //An array to sort quads.
    struct quad_plane *quads_start=g_malloc((s1->rings+1)*(s1->points_per_ring)*sizeof(struct quad_plane));
    struct quad_plane *quads=quads_start;

    //Fill the quad struct array.
    for(i=0;i<(s1->rings+1);i++)
      {
        for(j=0;j<(s1->points_per_ring);j++)
          { 
            quads->id=counter;
            counter++;             
            if(i==0)
              {
                quads->x1=((*(p1)).x);
                quads->y1=((*(p1)).y);
                quads->z1=((*(p1)).z);
                quads->x2=((*(p1+1)).x);
                quads->y2=((*(p1+1)).y);
                quads->z2=((*(p1+1)).z);            
                quads->x3=head.x;
                quads->y3=head.y;
                quads->z3=((*(p1)).z);
                quads->x4=head.x;
                quads->y4=head.y;
                quads->z4=1.0; 
                quads->r=0.0;
                quads->g=1.0;
                quads->b=0.0;
                quads->a=1.0;
              }
            else if(i==(s1->rings))
              {
                quads->x1=((*(p1)).x);
                quads->y1=((*(p1)).y);
                quads->z1=((*(p1)).z);
                quads->x2=((*(p1+1)).x);
                quads->y2=((*(p1+1)).y);
                quads->z2=((*(p1+1)).z);            
                quads->x3=tail.x;
                quads->y3=tail.y;
                quads->z3=((*(p1)).z);
                quads->x4=tail.x;
                quads->y4=tail.y;
                //Triangle at ends instead of quad. Set to 0 for qsort comparison.
                quads->z4=0.0;
                quads->r=1.0;
                quads->g=1.0;
                quads->b=0.0;
                quads->a=1.0;              
              }
            else
              {                
                quads->x1=((*(p1)).x);
                quads->y1=((*(p1)).y);
                quads->z1=((*(p1)).z);
                quads->x2=((*(p1+1)).x);
                quads->y2=((*(p1+1)).y);
                quads->z2=((*(p1+1)).z);            
                quads->x3=((*(p1+offset1)).x);
                quads->y3=((*(p1+offset1)).y);
                quads->z3=((*(p1+offset1)).z);
                quads->x4=((*(p1+offset2)).x);
                quads->y4=((*(p1+offset2)).y);
                quads->z4=((*(p1+offset2)).z);
                //Front flippers
                if((j==8||j==15)&&i==4)
                  {
                    quads->r=1.0;
                    quads->g=1.0;
                    quads->b=0.0;
                    quads->a=1.0;
                  }
                //Back flippers
                else if((j==8||j==15)&&i==13)
                  {
                    quads->r=1.0;
                    quads->g=1.0;
                    quads->b=0.0;
                    quads->a=1.0;
                  }
                //Top
                else if(j>=0&&j<8)
                  {
                    quads->r=0.0;
                    quads->g=1.0;
                    quads->b=0.0;
                    quads->a=1.0;
                  }
                //Bottom
                else
                  {
                    quads->r=1.0;
                    quads->g=1.0;
                    quads->b=0.0;
                    quads->a=1.0;
                  }
              }
            quads++;
            p1++;
          }
        //Draw first ring twice.
        if(i==0) p1=&g_array_index(s1->sr, struct point3d, 0);
        else p1++;
      }
    
    //Sort the quad plane array based on z values.
    qsort(quads_start, (s1->rings+1)*(s1->points_per_ring), sizeof(struct quad_plane), compare_quads);

    //Draw the sorted quads.
    quads=quads_start;
    cairo_pattern_t *radial1=NULL;
    gdouble eye_x=0.0;
    gdouble eye_y=0.0;
    gboolean pattern_set=FALSE;
    for(i=0;i<(s1->rings+1);i++)
      {
        for(j=0;j<(s1->points_per_ring);j++)
          {
            if(quads->id==18||quads->id==21)
              {
                eye_x=w1*(quads->x1+quads->x2+quads->x3+quads->x4)/4.0;
                eye_y=h1*(quads->y1+quads->y2+quads->y3+quads->y4)/4.0;
                radial1=cairo_pattern_create_radial(eye_x, eye_y, 3.0, eye_x, eye_y, 6.0);  
                cairo_pattern_add_color_stop_rgba(radial1, 0.0, 0.0, 0.0, 0.0, 1.0);
                cairo_pattern_add_color_stop_rgba(radial1, 1.0, 0.0, 1.0, 0.0, 1.0);
                cairo_set_source(cr, radial1);
                pattern_set=TRUE;
              }
            else cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
            cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
            cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
            cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
            cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
            cairo_close_path(cr);
            if((quads->id)>80)
              {
                cairo_fill_preserve(cr);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);         
                cairo_stroke(cr);
              }
            else
              {
                cairo_fill(cr);
              }

            //Draw the flippers.
            if(quads->b>0.9) g_print("Quad ID %i\n", quads->id);
            if(quads->id==72)
              {
                cairo_set_line_width(cr, 4.0);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_line_to(cr, w1*flipper1.x, h1*flipper1.y);
                cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
 
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x2, h1*quads->y2);
                cairo_line_to(cr, w1*flipper1.x, h1*flipper1.y);
                cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
        
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x3, h1*quads->y3);
                cairo_line_to(cr, w1*flipper1.x, h1*flipper1.y);
                cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);

                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_line_to(cr, w1*flipper1.x, h1*flipper1.y);
                cairo_line_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
                cairo_set_line_width(cr, 1.0);
              }
            if(quads->id==79)
              {
                cairo_set_line_width(cr, 4.0);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_line_to(cr, w1*flipper2.x, h1*flipper2.y);
                cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
 
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x2, h1*quads->y2);
                cairo_line_to(cr, w1*flipper2.x, h1*flipper2.y);
                cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
        
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x3, h1*quads->y3);
                cairo_line_to(cr, w1*flipper2.x, h1*flipper2.y);
                cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);

                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_line_to(cr, w1*flipper2.x, h1*flipper2.y);
                cairo_line_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
                cairo_set_line_width(cr, 1.0);
              }
            if(quads->id==216)
              {
                cairo_set_line_width(cr, 4.0);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_line_to(cr, w1*flipper3.x, h1*flipper3.y);
                cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
 
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x2, h1*quads->y2);
                cairo_line_to(cr, w1*flipper3.x, h1*flipper3.y);
                cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
        
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x3, h1*quads->y3);
                cairo_line_to(cr, w1*flipper3.x, h1*flipper3.y);
                cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);

                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_line_to(cr, w1*flipper3.x, h1*flipper3.y);
                cairo_line_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
                cairo_set_line_width(cr, 1.0);
              }
            if(quads->id==223)
              {
                cairo_set_line_width(cr, 4.0);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_line_to(cr, w1*flipper4.x, h1*flipper4.y);
                cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
 
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x2, h1*quads->y2);
                cairo_line_to(cr, w1*flipper4.x, h1*flipper4.y);
                cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
        
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x3, h1*quads->y3);
                cairo_line_to(cr, w1*flipper4.x, h1*flipper4.y);
                cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);

                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_line_to(cr, w1*flipper4.x, h1*flipper4.y);
                cairo_line_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
                cairo_set_line_width(cr, 1.0);
              }
            quads++;
            if(pattern_set)
              {
                cairo_pattern_destroy(radial1);
                pattern_set=FALSE;
              } 
          }
      }

    g_free(quads_start);    
  }      
int compare_quads(const void *a, const void *b)
  {
    return(  (((struct quad_plane*)a)->z1+((struct quad_plane*)a)->z2+((struct quad_plane*)a)->z3+((struct quad_plane*)a)->z4) - (((struct quad_plane*)b)->z1+((struct quad_plane*)b)->z2+((struct quad_plane*)b)->z3+((struct quad_plane*)b)->z4));
  }

