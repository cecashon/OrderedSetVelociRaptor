/*
    From Python Playground and boids.py
    https://github.com/electronut/pp
    by Mahesh Venkitachalam

    Add some fish and turtles to the boids program. Always move the boids in the positive x 
direction(fabs(x)). The 3d fish and turtle drawing can also be found in fish2.c.
    
    gcc -Wall boids3.c -o boids3 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu20.04 and GTK3.24

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<stdlib.h>

struct boid3d{
  guint shape_id;
  gdouble x;
  gdouble y;
  gdouble z;
  gdouble vx;
  gdouble vy;
  gdouble vz;
};
GArray *boids=NULL;

struct velocity3d{
  gdouble vx;
  gdouble vy;
  gdouble vz;
};

struct point3d{
  gdouble x;
  gdouble y;
  gdouble z;
};

//For sorting planes in the fish drawing. Sort quad and triangle planes.
struct quad_plane{
  guint id;
  gdouble x1;
  gdouble y1;
  gdouble z1;
  gdouble x2;
  gdouble y2;
  gdouble z2;
  gdouble x3;
  gdouble y3;
  gdouble z3;
  gdouble x4;
  gdouble y4;
  gdouble z4;
  gdouble r;
  gdouble g;
  gdouble b;
  gdouble a;
};

//The data arrays for the shape(*s) and shape rotated(*sr).
struct shape{
  gint cache_id;
  GArray *s;
  GArray *sr;
  gint rings;
  gint points_per_ring;
};

//These are the shape cache_id values or basic shapes. Start with a fish.
enum{ 
  FISH,
  TURTLE
};  

//The array to hold the shapes in.
GPtrArray *shape_cache=NULL;

//Starting boids.
gint num_boids=10;
//max magnitude of velocities calculated by "rules"
gdouble max_rule_val=0.03;
//max maginitude of final velocity
gdouble max_vel=7.0;

gdouble cur_width=600;
gdouble cur_height=500;

static guint tick_id=0;
static guint drawing_id=0;

//For the performance check boxes.
static gboolean frame_rate=FALSE;
static gboolean draw_time=FALSE;
static gboolean rules_time=FALSE;

static void show_frame_rate(GtkToggleButton *button, gpointer data);
static void show_draw_time(GtkToggleButton *button, gpointer data);
static void show_rules_time(GtkToggleButton *button, gpointer data);
static void change_drawing(GtkComboBox *combo, gpointer data);
//Draw the main window transparent blue.
static gboolean draw_main_window(GtkWidget *window, cairo_t *cr, gpointer data);
static void initialize_fish(gint rings, gint points_per_ring);
static void initialize_turtle(gint rings, gint points_per_ring);
static void initialize_shape(gint cache_id, gint rings, gint points_per_ring);
static void clear_shape_cache();
static void initialize_boids();
static void apply_rules();
static void apply_bc();
static gboolean draw_boids(GtkWidget *da, cairo_t *cr, gpointer data);
static void scatter_boids(GtkWidget *button, gpointer data);
static void add_boid(GtkWidget *button, gpointer data);
static gboolean start_draw_boids(GtkWidget *da, GdkFrameClock *frame_clock, gpointer data);
//Rotation functions.
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9]);
//Rotate the initial shape.
static void rotate_cache_shapes(gdouble qrs[9]);
static void rotate_shape(const struct shape *s1, gdouble qrs[9]);
static void rotate_shape_xyz(const struct shape *s1, gdouble qrs[9]);
//Rotate the rotated shape.
static void rotate_cache_shapes2(gdouble qrs[9]);
static void rotate_shape2(const struct shape *s1, gdouble qrs[9]);
static void rotate_shape_xyz2(const struct shape *s1, gdouble qrs[9]);
static const struct shape* get_drawing_from_shape_cache(gint cache_id, gint rings, gint points_per_ring);
//Draw the shapes.
static void draw_fish(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring, gdouble stripe_color[4]);
static void draw_turtle(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
//For comparing z values of planes. Painters algorithm.
int compare_quads(const void *a, const void *b);
static void exit_program(GtkWidget *widget, GtkWidget *da);

int main(int argc, char **argv)
 {
   gtk_init(&argc, &argv);

   GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
   gtk_window_set_title(GTK_WINDOW(window), "Boid Fish");
   gtk_window_set_default_size(GTK_WINDOW(window), 600, 500);
   gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
   gtk_widget_set_app_paintable(window, TRUE);
   GdkScreen *screen=gtk_widget_get_screen(window); 
   if(gdk_screen_is_composited(screen))
     {
       GdkVisual *visual=gdk_screen_get_rgba_visual(screen);
       gtk_widget_set_visual(window, visual);
     }
   else
     {
       g_print("Can't set window transparency.\n");
     } 
   g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), NULL);

   //Initialize the fish and turtle. Don't change arguments.
   shape_cache=g_ptr_array_new();
   initialize_fish(17, 17);
   initialize_turtle(16, 16);

   //Initial positions and direction vectors for the boids or fish.
   initialize_boids();

   GtkWidget *da=gtk_drawing_area_new();
   gtk_widget_set_hexpand(da, TRUE);
   gtk_widget_set_vexpand(da, TRUE);
   g_signal_connect(da, "draw", G_CALLBACK(draw_boids), NULL);
   g_signal_connect(window, "destroy", G_CALLBACK(exit_program), da);

   GtkWidget *button1=gtk_button_new_with_label("Scatter");
   gtk_widget_set_hexpand(button1, TRUE);
   g_signal_connect(button1, "clicked", G_CALLBACK(scatter_boids), NULL);

   GtkWidget *button2=gtk_button_new_with_label("Add");
   gtk_widget_set_hexpand(button2, TRUE);
   g_signal_connect(button2, "clicked", G_CALLBACK(add_boid), NULL);

   GtkWidget *combo_drawing=gtk_combo_box_text_new();
   gtk_widget_set_hexpand(combo_drawing, TRUE);
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "0", "Fish");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "1", "Turtle");    
   gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
   g_signal_connect(combo_drawing, "changed", G_CALLBACK(change_drawing), da);  

   //Some checks for performance.
   GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
   gtk_widget_set_hexpand(check_frame, TRUE);
   gtk_widget_set_name(check_frame, "check_frame");
   g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), NULL);

   GtkWidget *check_draw=gtk_check_button_new_with_label("Show Draw Time");
   gtk_widget_set_hexpand(check_draw, TRUE);
   gtk_widget_set_name(check_draw, "check_draw");
   g_signal_connect(check_draw, "toggled", G_CALLBACK(show_draw_time), NULL);

   GtkWidget *check_rules=gtk_check_button_new_with_label("Show Rules Time");
   gtk_widget_set_hexpand(check_rules, TRUE);
   gtk_widget_set_name(check_rules, "check_rules");
   g_signal_connect(check_rules, "toggled", G_CALLBACK(show_rules_time), NULL);  

   GtkWidget *grid=gtk_grid_new();
   gtk_grid_set_column_homogeneous(GTK_GRID(grid), TRUE);
   gtk_grid_attach(GTK_GRID(grid), da, 0, 0, 3, 1);
   gtk_grid_attach(GTK_GRID(grid), combo_drawing, 0, 1, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), button2, 0, 2, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), button1, 1, 1, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), check_frame, 1, 2, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), check_draw, 2, 1, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), check_rules, 2, 2, 1, 1);
   
   gtk_container_add(GTK_CONTAINER(window), grid);

   tick_id=gtk_widget_add_tick_callback(da, (GtkTickCallback)start_draw_boids, NULL, NULL);
   
   gchar *css_string=g_strdup("#check_frame{background: rgba(0,255,255,1.0);} #check_draw{background: rgba(0,255,255,1.0);}  #check_rules{background: rgba(0,255,255,1.0);}");
   GError *css_error=NULL;
   GtkCssProvider *provider=gtk_css_provider_new();
   gtk_css_provider_load_from_data(provider, css_string, -1, &css_error);
   gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
   if(css_error!=NULL)
     {
       g_print("CSS loader error %s\n", css_error->message);
       g_error_free(css_error);
     }
   g_object_unref(provider);
   g_free(css_string);

   gtk_widget_show_all(window);

   gtk_main();

   //Clean up boids.
   g_array_free(boids, TRUE);

   //Clean up shape cache.
   clear_shape_cache();
   g_ptr_array_free(shape_cache, FALSE);

   return 0;  
 }
static void show_frame_rate(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_draw_time(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) draw_time=TRUE;
    else draw_time=FALSE;
  }
static void show_rules_time(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) rules_time=TRUE;
    else rules_time=FALSE;
  }
static void change_drawing(GtkComboBox *combo, gpointer data)
  {
    drawing_id=gtk_combo_box_get_active(combo);
  }
static gboolean draw_main_window(GtkWidget *window, cairo_t *cr, gpointer data)
  {
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.3);
    cairo_paint(cr);

    return FALSE;
  }
static void initialize_fish(gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    if(rings!=17)
      {
        rings=17;
        g_print("Fish Warning: Set default rings=17.\n");
      }
    if(points_per_ring!=17)
      {
        points_per_ring=17;
        g_print("Fish Warning: Set default points_per_ring=17.\n");
      }
   
    //Extra points for head, tail and fins.
    gint array_size=rings*(points_per_ring+1)+9;
    struct point3d *p1=NULL;

    GArray *fish=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(fish, array_size);
    p1=&g_array_index(fish, struct point3d, 0);
          
    GArray *fish_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(fish_rotated, array_size);
          
    gint ring_points=points_per_ring+1;
    gdouble arc1=180.0/(gdouble)(rings+1);
    gdouble arc2=360.0/(gdouble)(points_per_ring);
    for(i=0;i<rings;i++)
      {
        for(j=0;j<ring_points;j++)
          {
            (*p1).x=3.0*cos(arc1*(gdouble)(i+1)*G_PI/180.0);
            (*p1).y=2.0*sin(arc1*(gdouble)(i+1)*G_PI/180.0)*cos(arc2*(gdouble)j*G_PI/180.0);
            (*p1).z=0.5*sin(arc1*(gdouble)(i+1)*G_PI/180.0)*sin(arc2*(gdouble)j*G_PI/180.0);
            p1++;
          }
      }
    //head of ellipse.
    (*p1).x=3.0;
    (*p1).y=0.0;
    (*p1).z=0.0;
    p1++;
    //tail of ellipse.
    (*p1).x=-3.0;
    (*p1).y=0.0;
    (*p1).z=0.0;
    p1++;
    //triangle tail.
    (*p1).x=-3.0;
    (*p1).y=0.0;
    (*p1).z=0.0;
    p1++;
    (*p1).x=-4.0;
    (*p1).y=-2.0;
    (*p1).z=0.0;
    p1++;
    (*p1).x=-4.0;
    (*p1).y=2.0;
    (*p1).z=0.0;
    p1++;
    //Top fin.
    (*p1).x=-0.7;
    (*p1).y=-2.5;
    (*p1).z=0.0;
    p1++;
    (*p1).x=0.0;
    (*p1).y=-1.95;
    (*p1).z=0.0;
    p1++;
    //Bottom fin.
    (*p1).x=-0.7;
    (*p1).y=2.5;
    (*p1).z=0.0;
    p1++;
    (*p1).x=0.0;
    (*p1).y=1.95;
    (*p1).z=0.0;
    
    struct shape *p=g_new(struct shape, 1);
    p->cache_id=FISH;
    p->s=fish;
    p->sr=fish_rotated;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    g_ptr_array_add(shape_cache, p);
  }
static void initialize_turtle(gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    if(rings!=16)
      {
        rings=16;
        g_print("Turtle Warning: Set default rings=16.\n");
      }
    if(points_per_ring!=16)
      {
        points_per_ring=16;
        g_print("Turtle Warning: Set default points_per_ring=16.\n");
      }
   
    //Extra points for head, tail and fins.
    gint array_size=rings*(points_per_ring+1)+6;
    struct point3d *p1=NULL;

    GArray *turtle=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(turtle, array_size);
    p1=&g_array_index(turtle, struct point3d, 0);
          
    GArray *turtle_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(turtle_rotated, array_size);
          
    gint ring_points=points_per_ring+1;
    gdouble arc1=180.0/(gdouble)(rings+1);
    gdouble arc2=360.0/(gdouble)(points_per_ring);
    for(i=0;i<rings;i++)
      {
        for(j=0;j<ring_points;j++)
          {
            if(i==0)
              {
                (*p1).x=3.2*cos(arc1*1.2*G_PI/180.0);
                (*p1).y=1.2*sin(25.0*G_PI/180.0)*cos(arc2*(gdouble)j*G_PI/180.0);
                (*p1).z=0.9*sin(20.0*G_PI/180.0)*sin(arc2*(gdouble)j*G_PI/180.0);
              }
            else if(i<4)
              {
                (*p1).x=3.2*cos(arc1*((gdouble)(i)+1.8)*G_PI/180.0);
                (*p1).y=1.2*sin(30.0*G_PI/180.0)*cos(arc2*(gdouble)j*G_PI/180.0);
                (*p1).z=0.9*sin(30.0*G_PI/180.0)*sin(arc2*(gdouble)j*G_PI/180.0);
              }
            else
              {
                (*p1).x=3.0*cos(arc1*(gdouble)(i+1)*G_PI/180.0);
                (*p1).y=2.0*sin(arc1*(gdouble)(i+1)*G_PI/180.0)*cos(arc2*(gdouble)j*G_PI/180.0);
                (*p1).z=1.0*sin(arc1*(gdouble)(i+1)*G_PI/180.0)*sin(arc2*(gdouble)j*G_PI/180.0);
              }
            p1++;
          }
      }
    //head of ellipse.
    (*p1).x=3.5;
    (*p1).y=0.0;
    (*p1).z=0.0;
    p1++;
    //tail of ellipse.
    (*p1).x=-4.0;
    (*p1).y=0.0;
    (*p1).z=0.0;
    p1++;
    //Right front flipper.
    (*p1).x=3.0;
    (*p1).y=-3.5;
    (*p1).z=0.0;
    p1++;
    //Left front flipper.
    (*p1).x=3.0;
    (*p1).y=3.5;
    (*p1).z=0.0;
    p1++;
    //Right back flipper.
    (*p1).x=-3.0;
    (*p1).y=-2.0;
    (*p1).z=0.0;
    p1++;
    //Left back flipper.
    (*p1).x=-3.0;
    (*p1).y=2.0;
    (*p1).z=0.0;
        
    struct shape *p=g_new(struct shape, 1);
    p->cache_id=TURTLE;
    p->s=turtle;
    p->sr=turtle_rotated;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    g_ptr_array_add(shape_cache, p);
  }

static void initialize_shape(gint cache_id, gint rings, gint points_per_ring)
  {
    switch(cache_id)
      {
        case FISH:
          initialize_fish(rings, points_per_ring);
          break; 
        case TURTLE:
          initialize_turtle(rings, points_per_ring);
          break;        
        default:
          g_print("Warning: Undefined Shape!\n");
      }
  }
static void clear_shape_cache()
  {
    gint i=0;
    gint length=shape_cache->len;
    struct shape *p=NULL;
    for(i=0;i<length;i++)
      {
        p=g_ptr_array_index(shape_cache, 0);
        g_array_free(p->s, TRUE);
        g_array_free(p->sr, TRUE);
        g_free(p);
        g_ptr_array_remove_index_fast(shape_cache, 0);
      }
  }
static void initialize_boids()
  {
    gint i=0;
    gdouble angle1=0;
    gdouble angle2=0;
    boids=g_array_sized_new(FALSE, FALSE, sizeof(struct boid3d), num_boids);
    g_array_set_size(boids, num_boids);
    struct boid3d *p1=&g_array_index(boids, struct boid3d, 0);

    for(i=0;i<num_boids;i++)
      {
        //Start with fish boids.
        (*p1).shape_id=FISH;
        angle1=2.0*G_PI*g_random_double();
        angle2=g_random_double_range(-G_PI/2.0, G_PI/2.0);
        //Position.
        (*p1).x=300.0+10.0*sin(angle2)*cos(angle1);
        (*p1).y=250.0+10.0*sin(angle2)*sin(angle1);
        (*p1).z=10.0*cos(angle2);
        //Unit velocity vector.
        (*p1).vx=fabs(sin(angle2)*cos(angle1));
        (*p1).vy=sin(angle2)*sin(angle1);
        (*p1).vz=cos(angle2);   
        p1++;
      }
  }
static void apply_rules()
  {
    gint i=0;
    gint j=0;
    gdouble temp1=0.0;
    gdouble temp2=0.0;
    gdouble temp3=0.0;
    gdouble temp4=0.0;
    gint counter=0;
    gdouble x1=0;
    gdouble x2=0;
    gdouble y1=0;
    gdouble y2=0;
    gdouble z1=0.0;
    gdouble z2=0.0;
    gdouble mag=0.0;
    GArray *vel=g_array_sized_new(FALSE, FALSE, sizeof(struct velocity3d), boids->len);
    g_array_set_size(vel, boids->len);
    GArray *D=g_array_sized_new(FALSE, FALSE, sizeof(gboolean), (boids->len)*(boids->len));
    g_array_set_size(D, (boids->len)*(boids->len));
    GArray *sum=g_array_sized_new(FALSE, FALSE, sizeof(gint), boids->len);
    g_array_set_size(sum, boids->len);
    GArray *vel2_3=g_array_sized_new(FALSE, FALSE, sizeof(struct velocity3d), boids->len);
    g_array_set_size(sum, boids->len);
    struct velocity3d *v1=&g_array_index(vel, struct velocity3d, 0);
    struct boid3d *p1=&g_array_index(boids, struct boid3d, 0);
    gboolean *d1=&g_array_index(D, gboolean, 0);
    gint *s1=&g_array_index(sum, gint, 0);
    struct velocity3d *v2=&g_array_index(vel2_3, struct velocity3d, 0);

    //D = self.distMatrix < 25.0
    for(i=0;i<boids->len;i++)
      {
        x1=(p1+i)->x;
        y1=(p1+i)->y;
        z1=(p1+i)->z;
        counter=0;
        for(j=0;j<boids->len;j++)
          {
            x2=(p1+j)->x;
            y2=(p1+j)->y;
            z2=(p1+j)->z;
            temp1=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)+(z2-z1)*(z2-z1));
            //If the boid is bouncing off the front or back don't group.
            if( ((p1+i)->z>95.0) || ((p1+i)->z<-95.0) ) *d1=FALSE;
            else if(temp1<15.0)
              {
                counter++;
                *d1=TRUE;
              }
            else *d1=FALSE;
            d1++; 
          } 
        *(s1+i)=counter;     
      }

    //vel = self.pos*D.sum(axis=1).reshape(self.N, 1) - D.dot(self.pos)
    d1=&g_array_index(D, gboolean, 0);
    for(i=0;i<boids->len;i++)
      {
        temp2=0.0;
        temp3=0.0;
        temp4=0.0;
        for(j=0;j<boids->len;j++)
          {
            temp2+=(gdouble)(*d1)*((p1+j)->x);
            temp3+=(gdouble)(*d1)*((p1+j)->y);
            temp4+=(gdouble)(*d1)*((p1+j)->z);
            d1++;
          }
        (*v1).vx=((p1+i)->x)*(gdouble)(*(s1+i))-temp2;
        (*v1).vy=((p1+i)->y)*(gdouble)(*(s1+i))-temp3;
        (*v1).vz=((p1+i)->z)*(gdouble)(*(s1+i))-temp4;
        v1++;
      }

    //self.limit(vel, self.maxRuleVal)
    v1=&g_array_index(vel, struct velocity3d, 0);
    for(i=0;i<boids->len;i++)
      {
        mag=sqrt((v1->vx)*(v1->vx)+(v1->vy)*(v1->vy)+(v1->vz)*(v1->vz));
        if(mag>max_rule_val)
          {
            (*v1).vx=(v1->vx)*max_rule_val/mag;
            (*v1).vy=(v1->vy)*max_rule_val/mag;
            (*v1).vz=(v1->vz)*max_rule_val/mag;
          }
        v1++;
      }

    //D = self.distMatrix < 50.0
    d1=&g_array_index(D, gboolean, 0);
    for(i=0;i<boids->len;i++)
      {
        x1=(p1+i)->x;
        y1=(p1+i)->y;
        z1=(p1+i)->z;
        for(j=0;j<boids->len;j++)
          {
            x2=(p1+j)->x;
            y2=(p1+j)->y;
            z2=(p1+j)->z;
            temp1=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)+(z2-z1)*(z2-z1));
            //If the boid is bouncing off the front or back don't group.
            if( ((p1+i)->z>95.0) || ((p1+i)->z<-95.0) ) *d1=FALSE;
            else if(temp1<50.0)
              {
                *d1=TRUE;
              }
            else *d1=FALSE;
            d1++;
          }    
      }

    //vel2 = D.dot(self.vel)
    d1=&g_array_index(D, gboolean, 0);
    for(i=0;i<boids->len;i++)
      {
        temp2=0.0;
        temp3=0.0;
        temp4=0.0;
        for(j=0;j<boids->len;j++)
          {
            temp2+=(gdouble)(*d1)*((p1+j)->vx);
            temp3+=(gdouble)(*d1)*((p1+j)->vy);
            temp4+=(gdouble)(*d1)*((p1+j)->vz);
            d1++;
          }
        (*v2).vx=temp2;
        (*v2).vy=temp3;
        (*v2).vz=temp4;
        v2++;
      }

    //self.limit(vel2, self.maxRuleVel)
    v1=&g_array_index(vel, struct velocity3d, 0);
    v2=&g_array_index(vel2_3, struct velocity3d, 0);
    for(i=0;i<boids->len;i++)
      {
        mag=sqrt((v2->vx)*(v2->vx)+(v2->vy)*(v2->vy)+(v2->vz)*(v2->vz));
        if(mag>max_rule_val)
          {
            (*v2).vx=(v2->vx)*max_rule_val/mag;
            (*v2).vy=(v2->vy)*max_rule_val/mag;
            (*v2).vz=(v2->vz)*max_rule_val/mag;
          }
        v1++;v2++;
      }

    //vel += vel2
    v1=&g_array_index(vel, struct velocity3d, 0);
    v2=&g_array_index(vel2_3, struct velocity3d, 0);
    for(i=0;i<boids->len;i++)
      {
        (*v1).vx+=(v2->vx);
        (*v1).vy+=(v2->vy);
        (*v1).vz+=(v2->vz);
        v1++;v2++;
      }

    //vel3 = D.dot(self.pos) - self.pos
    p1=&g_array_index(boids, struct boid3d, 0);
    v2=&g_array_index(vel2_3, struct velocity3d, 0);
    d1=&g_array_index(D, gboolean, 0);
    for(i=0;i<boids->len;i++)
      {
        temp2=0.0;
        temp3=0.0;
        temp4=0.0;
        for(j=0;j<boids->len;j++)
          {
            temp2+=(gdouble)(*d1)*((p1+j)->x);
            temp3+=(gdouble)(*d1)*((p1+j)->y);
            temp4+=(gdouble)(*d1)*((p1+j)->z);
            d1++;
          }
        (*v2).vx=temp2-((p1+i)->x);
        (*v2).vy=temp3-((p1+i)->y);
        (*v2).vz=temp4-((p1+i)->z);
        v2++;
      }

    //self.limit(vel3, self.maxRuleVel)
    v2=&g_array_index(vel2_3, struct velocity3d, 0);
    for(i=0;i<boids->len;i++)
      {
        mag=sqrt((v2->vx)*(v2->vx)+(v2->vy)*(v2->vy)+(v2->vz)*(v2->vz));
        if(mag>max_rule_val)
          {
            (*v2).vx=(v2->vx)*max_rule_val/mag;
            (*v2).vy=(v2->vy)*max_rule_val/mag;
            (*v2).vz=(v2->vz)*max_rule_val/mag;
          }
        v2++;
      }

    //vel += vel3
    v1=&g_array_index(vel, struct velocity3d, 0);
    v2=&g_array_index(vel2_3, struct velocity3d, 0);
    for(i=0;i<boids->len;i++)
      {
        (*v1).vx+=(v2->vx);
        (*v1).vy+=(v2->vx);
        (*v1).vz+=(v2->vz);
        v1++;v2++;
      }

    //self.vel += self.applyRules()
    p1=&g_array_index(boids, struct boid3d, 0);
    v1=&g_array_index(vel, struct velocity3d, 0);
    for(i=0;i<boids->len;i++)
      {
        (*p1).vx+=v1->vx;
        (*p1).vy+=v1->vy;
        (*p1).vz+=v1->vz;
        p1++;v1++;
      }

    //self.limit(self.vel, self.maxVel)
    p1=&g_array_index(boids, struct boid3d, 0);
    for(i=0;i<boids->len;i++)
      {
        mag=sqrt((p1->vx)*(p1->vx)+(p1->vy)*(p1->vy)+(p1->vz)*(p1->vz));
        if(mag>max_vel)
          {
            (*p1).vx=(p1->vx)*max_vel/mag;
            (*p1).vy=(p1->vy)*max_vel/mag;
            (*p1).vz=(p1->vz)*max_vel/mag;
          }
        p1++;
      }

    //self.pos += self.vel
    p1=&g_array_index(boids, struct boid3d, 0);
    for(i=0;i<boids->len;i++)
      {
        (*p1).x+=(p1->vx);
        (*p1).y+=(p1->vy);
        (*p1).z+=(p1->vz);
        p1++;
      }
    
    g_array_free(vel, TRUE);
    g_array_free(D, TRUE);
    g_array_free(sum, TRUE);
    g_array_free(vel2_3, TRUE);    
  }
static void apply_bc()
  {
    gint i=0;
    gdouble delta_r=2.0;
    gdouble w1=cur_width;
    gdouble h1=cur_height;
    struct boid3d *p1=&g_array_index(boids, struct boid3d, 0);

    for(i=0;i<boids->len;i++)
      {
        //Boundries.
        if((p1->x)>(w1+delta_r)) (*p1).x+=-w1-delta_r;            
        if((p1->x)<(-w1-delta_r)) (*p1).x+=w1+delta_r;           
        if((p1->y)>(h1+delta_r)) (*p1).y+=-h1-delta_r;            
        if((p1->y)<(-h1-delta_r)) (*p1).y+=h1+delta_r;
        //Bounce the boids off the front and back.
        if(((p1->z)>(100.0+delta_r))||((p1->z)<(-100.0-delta_r)))
          {
            (*p1).vy=-(p1->vy);
            (*p1).vz=-(p1->vz);
          }           
        p1++;
      }
  }
static void scatter_boids(GtkWidget *button, gpointer data)
  {
    gint i=0;
    gdouble angle1=0.0;
    gdouble angle2=0.0;
    struct boid3d *p1=&g_array_index(boids, struct boid3d, 0);

    for(i=0;i<boids->len;i++)
      {        
        angle1=2.0*G_PI*g_random_double();
        angle2=g_random_double_range(-G_PI/2.0, G_PI/2.0);
        //Position.
        (*p1).x=cur_width/2.0+cur_width/2.0*sin(angle2)*cos(angle1);
        (*p1).y=cur_height/2.0+cur_height/2.0*sin(angle2)*sin(angle1);
        (*p1).z=100.0*cos(angle2);
        //Unit velocity vector.
        (*p1).vx=max_vel*fabs(sin(angle2)*cos(angle1));
        (*p1).vy=max_vel*sin(angle2)*sin(angle1);
        (*p1).vz=max_vel*cos(angle2);   
        p1++;
      }
  }
static void add_boid(GtkWidget *button, gpointer data)
  {
    gdouble angle1=0.0;
    gdouble angle2=0.0;
    struct boid3d b;

    b.shape_id=drawing_id;
    angle1=2.0*G_PI*g_random_double();
    angle2=g_random_double_range(-G_PI/2.0, G_PI/2.0);
    b.x=cur_width/2.0+10.0*sin(angle2)*cos(angle1);
    b.y=cur_height/2.0+10.0*sin(angle2)*sin(angle1);
    b.z=10.0*cos(angle2);
    //Positive vx and add max_vel/2.0 to force the direction of the new boid.
    b.vx=max_vel*fabs(sin(angle2)*cos(angle1))+max_vel/2.0;
    b.vy=max_vel*sin(angle2)*sin(angle1);
    b.vz=max_vel*cos(angle2);   
    g_array_append_val(boids, b);
  }
static gboolean start_draw_boids(GtkWidget *da, GdkFrameClock *frame_clock, gpointer data)
  {
    if(frame_rate)
      {
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);       
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start=gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    if(rules_time)
      {
        GTimer *timer=g_timer_new();
        apply_rules();
        apply_bc();
        g_print("Rules Time %f\n", g_timer_elapsed(timer, NULL));
        g_timer_destroy(timer);
      }
    else
      {
        apply_rules();
        apply_bc();
      }

    gtk_widget_queue_draw(da);
    return G_SOURCE_CONTINUE;
  }
static gboolean draw_boids(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    gint i=0;
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;
    cur_width=width;
    cur_height=height;

    GTimer *timer=NULL;
    if(draw_time)
      {
        timer=g_timer_new();
      }

    //Paint background transparent.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.0);
    cairo_paint(cr);

    gdouble scale=0.0;
    gdouble pitch=0.0;
    gdouble yaw=0.0;
    gdouble qrs[9];
    gdouble stripe_color[4]={1.0, 1.0, 0.0, 1.0};
    struct boid3d *p1=&g_array_index(boids, struct boid3d, 0);
    //Draw the initial fish boids.
    for(i=0;i<num_boids;i++)
      {
        cairo_save(cr); 
        //Scale based on Z.
        scale=(200.0+(p1->z))/900.0;
        cairo_scale(cr, scale, scale);
        cairo_translate(cr, (p1->x)*1.0/scale, (p1->y)*1.0/scale);

        yaw=atan((p1->vy)/(p1->vx));
        pitch=atan((p1->vz)/sqrt((p1->vx)*(p1->vx)+(p1->vy)*(p1->vy)));    
        if((p1->vx)>0.0) quaternion_rotation(yaw, 0.0, pitch, qrs);
        else quaternion_rotation(yaw+G_PI, 0.0, pitch+G_PI, qrs);
        rotate_cache_shapes(qrs);

        quaternion_rotation(0.0, G_PI/20.0, 0.0, qrs);                 
        rotate_cache_shapes2(qrs);

        cairo_set_line_width(cr, 0.01);
        draw_fish(cr, w1, h1, 17, 17, stripe_color);
        cairo_restore(cr);  
        p1++;
      }

    //Draw the added boids. Draw the new fish with cyan stripes.
    stripe_color[0]=0.0;
    //Funky programming here. Keep blue<0.95. Needs to use quads->id like the turtle instead.
    stripe_color[2]=0.9;
    for(i=num_boids;i<boids->len;i++)
      {
        if((*p1).shape_id==FISH)
          {
            cairo_save(cr); 
            //Scale based on Z.
            scale=(200.0+(p1->z))/900.0;
            cairo_scale(cr, scale, scale);
            cairo_translate(cr, (p1->x)*1.0/scale, (p1->y)*1.0/scale);

            yaw=atan((p1->vy)/(p1->vx));
            pitch=atan((p1->vz)/sqrt((p1->vx)*(p1->vx)+(p1->vy)*(p1->vy)));    
            if((p1->vx)>0.0) quaternion_rotation(yaw, 0.0, pitch, qrs);
            else quaternion_rotation(yaw+G_PI, 0.0, pitch+G_PI, qrs);
            rotate_cache_shapes(qrs);

            quaternion_rotation(0.0, G_PI/20.0, 0.0, qrs);                 
            rotate_cache_shapes2(qrs);

            cairo_set_line_width(cr, 0.01);
            draw_fish(cr, w1, h1, 17, 17, stripe_color);
            cairo_restore(cr);
          }
        //A turtle.
        else
          {
            cairo_save(cr); 
            //Scale based on Z.
            scale=(250.0+(p1->z))/900.0;
            cairo_scale(cr, scale, scale);
            cairo_translate(cr, (p1->x)*1.0/scale, (p1->y)*1.0/scale);

            yaw=atan((p1->vy)/(p1->vx));
            pitch=atan((p1->vz)/sqrt((p1->vx)*(p1->vx)+(p1->vy)*(p1->vy)));    
            if((p1->vx)>0.0) quaternion_rotation(yaw, 0.0, pitch, qrs);
            //Original turtle drawing different than the fish.
            else quaternion_rotation(yaw+G_PI, 0.0, pitch-G_PI/2.0, qrs);
            rotate_cache_shapes(qrs);

            quaternion_rotation(0.0, G_PI/20.0, 0.0, qrs);                 
            rotate_cache_shapes2(qrs);

            cairo_set_line_width(cr, 0.01);
            draw_turtle(cr, w1, h1, 16, 16);
            cairo_restore(cr);
          }  
        p1++;
      }

    if(timer!=NULL)
      {
        g_print("Draw Time %f\n", g_timer_elapsed(timer, NULL));
        g_timer_destroy(timer);
      }

    return FALSE;
  }
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
//Rotate the initial drawing.
static void rotate_cache_shapes(gdouble qrs[9])
  {
    gint i=0;

    const struct shape *s1=NULL;
    for(i=0;i<(shape_cache->len);i++)
      {
        s1=g_ptr_array_index(shape_cache, i); 
        rotate_shape(s1, qrs);  
      } 
  }
static void rotate_shape(const struct shape *s1, gdouble qrs[9])
  {
    if(s1->cache_id==FISH)
      {
        rotate_shape_xyz(s1, qrs);
      } 
    else if(s1->cache_id==TURTLE)
      {
        rotate_shape_xyz(s1, qrs);
      }     
    else
      {
        g_print("Warning: Couldn't find shape to rotate!\n");
      }
  }
static void rotate_shape_xyz(const struct shape *s1, gdouble qrs[9])
  {
    gint i=0;
    struct point3d *p1=&g_array_index(s1->s, struct point3d, 0);
    struct point3d *p2=&g_array_index(s1->sr, struct point3d, 0);
    for(i=0;i<(s1->s->len);i++)
      {
        (*p2).x=((*p1).x*qrs[0])+((*p1).y*qrs[1])+((*p1).z*qrs[2]);
        (*p2).y=((*p1).x*qrs[3])+((*p1).y*qrs[4])+((*p1).z*qrs[5]);
        (*p2).z=((*p1).x*qrs[6])+((*p1).y*qrs[7])+((*p1).z*qrs[8]);
        p1++,p2++;
      }
  }
//Rotate the rotated drawing.
static void rotate_cache_shapes2(gdouble qrs[9])
  {
    gint i=0;

    const struct shape *s1=NULL;
    for(i=0;i<(shape_cache->len);i++)
      {
        s1=g_ptr_array_index(shape_cache, i); 
        rotate_shape2(s1, qrs);  
      } 
  }
static void rotate_shape2(const struct shape *s1, gdouble qrs[9])
  {
    if(s1->cache_id==FISH)
      {
        rotate_shape_xyz2(s1, qrs);
      } 
    else if(s1->cache_id==TURTLE)
      {
        rotate_shape_xyz2(s1, qrs);
      }     
    else
      {
        g_print("Warning: Couldn't find shape to rotate!\n");
      }
  }
static void rotate_shape_xyz2(const struct shape *s1, gdouble qrs[9])
  {
    gint i=0;
    gdouble x=0.0;
    gdouble y=0.0;
    gdouble z=0.0;
    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    for(i=0;i<(s1->sr->len);i++)
      {
        x=(*p1).x;
        y=(*p1).y;
        z=(*p1).z;
        (*p1).x=(x*qrs[0])+(y*qrs[1])+(z*qrs[2]);
        (*p1).y=(x*qrs[3])+(y*qrs[4])+(z*qrs[5]);
        (*p1).z=(x*qrs[6])+(y*qrs[7])+(z*qrs[8]);
        p1++;
      }
  }
static const struct shape* get_drawing_from_shape_cache(gint cache_id, gint rings, gint points_per_ring)
  {
    gint i=0;
    gboolean shape_found=FALSE;

    //Get the drawing from the shape cache.
    struct shape *s1=NULL;

    if(cache_id>=0&&cache_id<=12)
      {
        for(i=0;i<shape_cache->len;i++)
          {
            s1=g_ptr_array_index(shape_cache, i);
            if((s1->cache_id==cache_id)&&(s1->rings==rings)&&(s1->points_per_ring==points_per_ring))
              {
                shape_found=TRUE;
                break;
              }        
          }

        /*
            Couldn't find the drawing in the cache so initialize it and try again. Could just
        auto initialize all shapes.
        */
        if(shape_found==FALSE)
          {
            g_print("Warning: Auto initialize shape! %i %i %i\n", cache_id, rings, points_per_ring);
            initialize_shape(cache_id, rings, points_per_ring);
            for(i=0;i<shape_cache->len;i++)
              {
                s1=g_ptr_array_index(shape_cache, i);
                if(s1->cache_id==cache_id) break;
              }
          }
      }
    else g_print("Warning: Invalid shape cache_id requested!\n");

    return s1;
  }
static void draw_fish(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring, gdouble stripe_color[4])
  {
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(FISH, rings, points_per_ring);

    gint array_len=s1->sr->len;
    gint offset1=(s1->points_per_ring)+2;
    gint offset2=(s1->points_per_ring)+1;
    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    struct point3d head=g_array_index(s1->sr, struct point3d, array_len-9);
    struct point3d tail=g_array_index(s1->sr, struct point3d, array_len-8);
    struct point3d *fin=&g_array_index(s1->sr, struct point3d, array_len-4);

    //An array to sort quads.
    struct quad_plane *quads_start=g_malloc((s1->rings+1)*(s1->points_per_ring)*sizeof(struct quad_plane));
    struct quad_plane *quads=quads_start;

    //Fill the quad struct array.
    for(i=0;i<(s1->rings+1);i++)
      {
        for(j=0;j<(s1->points_per_ring);j++)
          {            
            if(i==0)
              {
                quads->x1=((*(p1)).x);
                quads->y1=((*(p1)).y);
                quads->z1=((*(p1)).z);
                quads->x2=((*(p1+1)).x);
                quads->y2=((*(p1+1)).y);
                quads->z2=((*(p1+1)).z);            
                quads->x3=head.x;
                quads->y3=head.y;
                quads->z3=((*(p1)).z);
                quads->x4=head.x;
                quads->y4=head.y;
                quads->z4=0.0; 
                quads->r=0.0;
                quads->g=0.0;
                quads->b=0.0;
                quads->a=1.0;
              }
            else if(i==(s1->rings))
              {
                quads->x1=((*(p1)).x);
                quads->y1=((*(p1)).y);
                quads->z1=((*(p1)).z);
                quads->x2=((*(p1+1)).x);
                quads->y2=((*(p1+1)).y);
                quads->z2=((*(p1+1)).z);            
                quads->x3=tail.x;
                quads->y3=tail.y;
                quads->z3=((*(p1)).z);
                quads->x4=tail.x;
                quads->y4=tail.y;
                //Triangle at ends instead of quad. Set to 0 for qsort comparison.
                quads->z4=0.0;
                quads->r=stripe_color[0];
                quads->g=stripe_color[1];
                quads->b=stripe_color[2];
                quads->a=stripe_color[3];            
              }
            else
              {                
                quads->x1=((*(p1)).x);
                quads->y1=((*(p1)).y);
                quads->z1=((*(p1)).z);
                quads->x2=((*(p1+1)).x);
                quads->y2=((*(p1+1)).y);
                quads->z2=((*(p1+1)).z);            
                quads->x3=((*(p1+offset1)).x);
                quads->y3=((*(p1+offset1)).y);
                quads->z3=((*(p1+offset1)).z);
                quads->x4=((*(p1+offset2)).x);
                quads->y4=((*(p1+offset2)).y);
                quads->z4=((*(p1+offset2)).z);
                //The first if sets blue=1 which is then selected for the eye radial pattern.
                if((j==5||j==11)&&i==4)
                  {
                    quads->r=0.0;
                    quads->g=0.0;
                    quads->b=1.0;
                    quads->a=1.0;
                  }
                else if(j==2||j==4||j==6||j==10||j==12||j==14)
                  {
                    quads->r=0.8;
                    quads->g=0.0;
                    quads->b=0.8;
                    quads->a=1.0;
                  }
                else if(j==3||j==5||j==11||j==13)
                  {
                    quads->r=stripe_color[0];
                    quads->g=stripe_color[1];
                    quads->b=stripe_color[2];
                    quads->a=stripe_color[3];
                  }
                else
                  {
                    quads->r=1.0;
                    quads->g=0.0;
                    quads->b=0.9;
                    quads->a=0.3;
                  }
              }
            quads++;
            p1++;
          }
        ///Draw first ring twice.
        if(i==0) p1=&g_array_index(s1->sr, struct point3d, 0);
        else p1++;
      }
    
    //Sort the quad plane array based on z values.
    qsort(quads_start, (s1->rings+1)*(s1->points_per_ring), sizeof(struct quad_plane), compare_quads);

    //Draw the sorted quads.
    quads=quads_start;
    cairo_pattern_t *radial1=NULL;
    gdouble eye_x=0.0;
    gdouble eye_y=0.0;
    gboolean pattern_set=FALSE;
    for(i=0;i<(s1->rings+1);i++)
      {
        for(j=0;j<(s1->points_per_ring);j++)
          {
            if(quads->b>=0.95)
              {
                eye_x=w1*(quads->x1+quads->x2+quads->x3+quads->x4)/4.0;
                eye_y=h1*(quads->y1+quads->y2+quads->y3+quads->y4)/4.0;
                radial1=cairo_pattern_create_radial(eye_x, eye_y, 3.0, eye_x, eye_y, 6.0);  
                cairo_pattern_add_color_stop_rgba(radial1, 0.0, 0.0, 0.0, 0.0, 1.0);
                cairo_pattern_add_color_stop_rgba(radial1, 1.0, 1.0, 1.0, 0.0, 1.0);
                cairo_set_source(cr, radial1);
                pattern_set=TRUE;
              }
            else cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
            cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
            cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
            cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
            cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
            cairo_close_path(cr);
            cairo_fill(cr);
            quads++;
            if(pattern_set)
              {
                cairo_pattern_destroy(radial1);
                pattern_set=FALSE;
              } 
          }
      }

    //Draw tail.
    p1=&g_array_index(s1->sr, struct point3d, array_len-7);
    cairo_set_source_rgba(cr, stripe_color[0], stripe_color[1], stripe_color[2], stripe_color[3]);
    cairo_move_to(cr, w1*p1->x, h1*p1->y);
    cairo_line_to(cr, w1*((p1+1)->x), h1*((p1+1)->y));
    cairo_line_to(cr, w1*((p1+2)->x), h1*((p1+2)->y));
    cairo_close_path(cr);
    cairo_fill(cr);
    
    //Draw top fin has three triangles.
    p1=&g_array_index(s1->sr, struct point3d, 170);
    cairo_move_to(cr, w1*p1->x, h1*p1->y);
    cairo_line_to(cr, w1*(fin->x), h1*(fin->y));
    cairo_line_to(cr, w1*((fin+1)->x), h1*((fin+1)->y));
    cairo_close_path(cr);
    cairo_fill(cr);

    cairo_move_to(cr, w1*((p1+1)->x), h1*((p1+1)->y));
    cairo_line_to(cr, w1*(fin->x), h1*(fin->y));
    cairo_line_to(cr, w1*((fin+1)->x), h1*((fin+1)->y));
    cairo_close_path(cr);
    cairo_fill(cr);

    cairo_move_to(cr, w1*p1->x, h1*p1->y);
    cairo_line_to(cr, w1*(fin->x), h1*(fin->y));
    cairo_line_to(cr, w1*((p1+1)->x), h1*((p1+1)->y));
    cairo_close_path(cr);
    cairo_fill(cr);

    //Bottom fin has one triangle.
    p1=&g_array_index(s1->sr, struct point3d, 179);
    fin=&g_array_index(s1->sr, struct point3d, array_len-2);
    cairo_move_to(cr, w1*p1->x, h1*p1->y);
    cairo_line_to(cr, w1*(fin->x), h1*(fin->y));
    cairo_line_to(cr, w1*((fin+1)->x), h1*((fin+1)->y));
    cairo_close_path(cr);
    cairo_fill(cr);

    g_free(quads_start);    
  }
static void draw_turtle(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;
    gint counter=0;

    const struct shape *s1=get_drawing_from_shape_cache(TURTLE, rings, points_per_ring);

    gint array_len=s1->sr->len;
    gint offset1=(s1->points_per_ring)+2;
    gint offset2=(s1->points_per_ring)+1;
    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    struct point3d head=g_array_index(s1->sr, struct point3d, array_len-6);
    struct point3d tail=g_array_index(s1->sr, struct point3d, array_len-5);
    struct point3d flipper1=g_array_index(s1->sr, struct point3d, array_len-4);
    struct point3d flipper2=g_array_index(s1->sr, struct point3d, array_len-3);
    struct point3d flipper3=g_array_index(s1->sr, struct point3d, array_len-2);
    struct point3d flipper4=g_array_index(s1->sr, struct point3d, array_len-1);


    //An array to sort quads.
    struct quad_plane *quads_start=g_malloc((s1->rings+1)*(s1->points_per_ring)*sizeof(struct quad_plane));
    struct quad_plane *quads=quads_start;

    //Fill the quad struct array.
    for(i=0;i<(s1->rings+1);i++)
      {
        for(j=0;j<(s1->points_per_ring);j++)
          { 
            quads->id=counter;
            counter++;             
            if(i==0)
              {
                quads->x1=((*(p1)).x);
                quads->y1=((*(p1)).y);
                quads->z1=((*(p1)).z);
                quads->x2=((*(p1+1)).x);
                quads->y2=((*(p1+1)).y);
                quads->z2=((*(p1+1)).z);            
                quads->x3=head.x;
                quads->y3=head.y;
                quads->z3=((*(p1)).z);
                quads->x4=head.x;
                quads->y4=head.y;
                quads->z4=1.0; 
                quads->r=0.0;
                quads->g=1.0;
                quads->b=0.0;
                quads->a=1.0;
              }
            else if(i==(s1->rings))
              {
                quads->x1=((*(p1)).x);
                quads->y1=((*(p1)).y);
                quads->z1=((*(p1)).z);
                quads->x2=((*(p1+1)).x);
                quads->y2=((*(p1+1)).y);
                quads->z2=((*(p1+1)).z);            
                quads->x3=tail.x;
                quads->y3=tail.y;
                quads->z3=((*(p1)).z);
                quads->x4=tail.x;
                quads->y4=tail.y;
                //Triangle at ends instead of quad. Set to 0 for qsort comparison.
                quads->z4=0.0;
                quads->r=1.0;
                quads->g=1.0;
                quads->b=0.0;
                quads->a=1.0;              
              }
            else
              {                
                quads->x1=((*(p1)).x);
                quads->y1=((*(p1)).y);
                quads->z1=((*(p1)).z);
                quads->x2=((*(p1+1)).x);
                quads->y2=((*(p1+1)).y);
                quads->z2=((*(p1+1)).z);            
                quads->x3=((*(p1+offset1)).x);
                quads->y3=((*(p1+offset1)).y);
                quads->z3=((*(p1+offset1)).z);
                quads->x4=((*(p1+offset2)).x);
                quads->y4=((*(p1+offset2)).y);
                quads->z4=((*(p1+offset2)).z);
                //Front flippers
                if((j==8||j==15)&&i==4)
                  {
                    quads->r=1.0;
                    quads->g=1.0;
                    quads->b=0.0;
                    quads->a=1.0;
                  }
                //Back flippers
                else if((j==8||j==15)&&i==13)
                  {
                    quads->r=1.0;
                    quads->g=1.0;
                    quads->b=0.0;
                    quads->a=1.0;
                  }
                //Top
                else if(j>=0&&j<8)
                  {
                    quads->r=0.0;
                    quads->g=1.0;
                    quads->b=0.0;
                    quads->a=1.0;
                  }
                //Bottom
                else
                  {
                    quads->r=1.0;
                    quads->g=1.0;
                    quads->b=0.0;
                    quads->a=1.0;
                  }
              }
            quads++;
            p1++;
          }
        //Draw first ring twice.
        if(i==0) p1=&g_array_index(s1->sr, struct point3d, 0);
        else p1++;
      }
    
    //Sort the quad plane array based on z values.
    qsort(quads_start, (s1->rings+1)*(s1->points_per_ring), sizeof(struct quad_plane), compare_quads);

    //Draw the sorted quads.
    quads=quads_start;
    cairo_pattern_t *radial1=NULL;
    gdouble eye_x=0.0;
    gdouble eye_y=0.0;
    gboolean pattern_set=FALSE;
    for(i=0;i<(s1->rings+1);i++)
      {
        for(j=0;j<(s1->points_per_ring);j++)
          {
            if(quads->id==18||quads->id==21)
              {
                eye_x=w1*(quads->x1+quads->x2+quads->x3+quads->x4)/4.0;
                eye_y=h1*(quads->y1+quads->y2+quads->y3+quads->y4)/4.0;
                radial1=cairo_pattern_create_radial(eye_x, eye_y, 3.0, eye_x, eye_y, 6.0);  
                cairo_pattern_add_color_stop_rgba(radial1, 0.0, 0.0, 0.0, 0.0, 1.0);
                cairo_pattern_add_color_stop_rgba(radial1, 1.0, 0.0, 1.0, 0.0, 1.0);
                cairo_set_source(cr, radial1);
                pattern_set=TRUE;
              }
            else cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
            cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
            cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
            cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
            cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
            cairo_close_path(cr);
            if((quads->id)>80)
              {
                cairo_fill_preserve(cr);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);         
                cairo_stroke(cr);
              }
            else
              {
                cairo_fill(cr);
              }

            //Draw the flippers.
            if(quads->b>0.9) g_print("Quad ID %i\n", quads->id);
            if(quads->id==72)
              {
                cairo_set_line_width(cr, 4.0);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_line_to(cr, w1*flipper1.x, h1*flipper1.y);
                cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
 
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x2, h1*quads->y2);
                cairo_line_to(cr, w1*flipper1.x, h1*flipper1.y);
                cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
        
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x3, h1*quads->y3);
                cairo_line_to(cr, w1*flipper1.x, h1*flipper1.y);
                cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);

                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_line_to(cr, w1*flipper1.x, h1*flipper1.y);
                cairo_line_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
                cairo_set_line_width(cr, 1.0);
              }
            if(quads->id==79)
              {
                cairo_set_line_width(cr, 4.0);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_line_to(cr, w1*flipper2.x, h1*flipper2.y);
                cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
 
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x2, h1*quads->y2);
                cairo_line_to(cr, w1*flipper2.x, h1*flipper2.y);
                cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
        
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x3, h1*quads->y3);
                cairo_line_to(cr, w1*flipper2.x, h1*flipper2.y);
                cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);

                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_line_to(cr, w1*flipper2.x, h1*flipper2.y);
                cairo_line_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
                cairo_set_line_width(cr, 1.0);
              }
            if(quads->id==216)
              {
                cairo_set_line_width(cr, 4.0);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_line_to(cr, w1*flipper3.x, h1*flipper3.y);
                cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
 
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x2, h1*quads->y2);
                cairo_line_to(cr, w1*flipper3.x, h1*flipper3.y);
                cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
        
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x3, h1*quads->y3);
                cairo_line_to(cr, w1*flipper3.x, h1*flipper3.y);
                cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);

                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_line_to(cr, w1*flipper3.x, h1*flipper3.y);
                cairo_line_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
                cairo_set_line_width(cr, 1.0);
              }
            if(quads->id==223)
              {
                cairo_set_line_width(cr, 4.0);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_line_to(cr, w1*flipper4.x, h1*flipper4.y);
                cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
 
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x2, h1*quads->y2);
                cairo_line_to(cr, w1*flipper4.x, h1*flipper4.y);
                cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
        
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x3, h1*quads->y3);
                cairo_line_to(cr, w1*flipper4.x, h1*flipper4.y);
                cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);

                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_move_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_line_to(cr, w1*flipper4.x, h1*flipper4.y);
                cairo_line_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_close_path(cr);
                cairo_stroke_preserve(cr);
                cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_fill(cr);
                cairo_set_line_width(cr, 1.0);
              }
            quads++;
            if(pattern_set)
              {
                cairo_pattern_destroy(radial1);
                pattern_set=FALSE;
              } 
          }
      }

    g_free(quads_start);    
  }     
int compare_quads(const void *a, const void *b)
  {
    return(  (((struct quad_plane*)a)->z1+((struct quad_plane*)a)->z2+((struct quad_plane*)a)->z3+((struct quad_plane*)a)->z4) - (((struct quad_plane*)b)->z1+((struct quad_plane*)b)->z2+((struct quad_plane*)b)->z3+((struct quad_plane*)b)->z4));
  }

static void exit_program(GtkWidget *widget, GtkWidget *da)
  {
    if(tick_id!=0) gtk_widget_remove_tick_callback(da, tick_id);
    gtk_main_quit();
  }








