
/* 
    Another attempt to match cairo rotations with 3d rotations so that text and an object
can be rotated in 3d.  Rotate two triangles and try to match them. The ordering of rotations
in 3d is trouble for mapping to a cairo 2d matrix.
    Try mapping text to the surface of a cube. I don't have the angles figured out for this to
work correctly if it can be done. Can get some single axis rotations in 3d but that is it.
    OK, it is working. The text can be mapped to the surface of a cube. Looks very good. 
    See also the ellipse.c, freetype_gtk1.c and spring2.c drawings for drawing with rotations.
 
    gcc -Wall euler1.c -o euler1 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<stdlib.h>

//A reference triangle t1 and a rotated triangle t2.
gdouble t1[3][3]={  
      {0.0, -1.0, 0.0},  
      {-1.0, 1.0, 0.0},  
      {1.0, 1.0, 0.0}, 
      };
gdouble t2[3][3]={  
      {0.0, -1.0, 0.0},  
      {-1.0, 1.0, 0.0},  
      {1.0, 1.0, 0.0}, 
      };

//For sorting drawing planes to draw in order from -z to +z. Used in the last cube.
struct plane_order{
  gint id;
  gdouble value;
};

//Cube points and rotated cube points.
static GArray *cube=NULL;
static GArray *cube_rotated=NULL;

struct point3d{
  gdouble x;
  gdouble y;
  gdouble z;
};

//Combo id for animation.
static gint rotate=0;
static gint drawing_id=0;

//Tick id for animation frame clock.
static guint tick_id=0;

//The rotations.
static gdouble pitch=0.0;
static gdouble roll=0.0;
static gdouble yaw=0.0;

static void initialize_cube();
static void change_drawing(GtkComboBox *combo, gpointer data);
static void rotate_drawing(GtkComboBox *combo, gpointer data);
static void set_rotation_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, GtkWidget *da);
static void set_rotation_roll(GtkRange *range, GtkScrollType scroll, gdouble value, GtkWidget *da);
static void set_rotation_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, GtkWidget *da);
static void euler_rotation();
static void quaternion_rotation(gdouble yaw_r, gdouble roll_r, gdouble pitch_r, gdouble qrs[9]);
static void rotate_cube_quaternion(gdouble qrs[9]);
static void matrix_multiply_rotations(gdouble qrs1[9], gdouble qrs2[9], gdouble qrs3[9]);
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data);
static gboolean draw_shape(GtkWidget *da, cairo_t *cr, gpointer data);
static void draw_triangle_with_rotation(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_cube_with_rotation1(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_cube_with_rotation2(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_cube_with_rotation3(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_cube_with_rotation4(cairo_t *cr, gdouble w1, gdouble h1);
int compare_planes(const void *a, const void *b);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Euler Angles");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL); 

    initialize_cube();  

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);
    g_signal_connect(da, "draw", G_CALLBACK(draw_shape), NULL);

    GtkWidget *combo_rotate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_rotate, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 0, "1", "No Rotate");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 1, "2", "Animate Pitch, Roll and Yaw");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_rotate), 0);
    g_signal_connect(combo_rotate, "changed", G_CALLBACK(rotate_drawing), da);

    GtkWidget *combo_drawing=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_drawing, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "0", "Triangle");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "1", "Try Rotated Cube1");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 2, "2", "Try Rotated Cube2");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 3, "3", "Try Rotated Cube3");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 4, "4", "A Rotated Text Cube");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
    g_signal_connect(combo_drawing, "changed", G_CALLBACK(change_drawing), da);  

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider, TRUE);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider, TRUE);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider, TRUE);
    
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_rotation_pitch), da);
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_rotation_roll), da);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_rotation_yaw), da);

    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_rotate, 0, 0, 3, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_drawing, 0, 1, 3, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 3, 1, 1);
    GtkWidget *scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll, TRUE);
    gtk_widget_set_vexpand(scroll, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll), grid1);
   
    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), scroll, TRUE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 300);
   
    gtk_container_add(GTK_CONTAINER(window), paned1);

    gtk_widget_show_all(window);

    gtk_main();

    g_array_free(cube, TRUE);
    g_array_free(cube_rotated, TRUE);

    return 0;  
  }
static void initialize_cube()
  {
    cube=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 8);
    g_array_set_size(cube, 8);
    cube_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 8);
    g_array_set_size(cube_rotated, 8);

    struct point3d *p1=&g_array_index(cube, struct point3d, 0);
    (*p1).x=2.0;
    (*p1).y=2.0;
    (*p1).z=2.0;
    (*(p1+1)).x=2.0;
    (*(p1+1)).y=-2.0;
    (*(p1+1)).z=2.0;
    (*(p1+2)).x=-2.0;
    (*(p1+2)).y=-2.0;
    (*(p1+2)).z=2.0;
    (*(p1+3)).x=-2.0;
    (*(p1+3)).y=2.0;
    (*(p1+3)).z=2.0;
    (*(p1+4)).x=2.0;
    (*(p1+4)).y=2.0;
    (*(p1+4)).z=-2.0;
    (*(p1+5)).x=2.0;
    (*(p1+5)).y=-2.0;
    (*(p1+5)).z=-2.0;
    (*(p1+6)).x=-2.0;
    (*(p1+6)).y=-2.0;
    (*(p1+6)).z=-2.0;
    (*(p1+7)).x=-2.0;
    (*(p1+7)).y=2.0;
    (*(p1+7)).z=-2.0;
  }
static void change_drawing(GtkComboBox *combo, gpointer data)
  {
    drawing_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void rotate_drawing(GtkComboBox *combo, gpointer data)
  {
    rotate=gtk_combo_box_get_active(combo);
    //Reset rotations for animation.
    pitch=0.0;
    roll=0.0;
    yaw=0.0;

    if(rotate==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(data), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(data));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(data), (GtkTickCallback)animate_drawing, NULL, NULL);
          }
      }
    
  }
static void set_rotation_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, GtkWidget *da)
  {
    if(value>360.0) pitch=360.0;
    else if(value<0.0) pitch=0.0;
    else pitch=value; 

    gtk_widget_queue_draw(da);
  }
static void set_rotation_roll(GtkRange *range, GtkScrollType scroll, gdouble value, GtkWidget *da)
  {
    if(value>360.0) roll=360.0;
    else if(value<0.0) roll=0.0;
    else roll=value; 

    gtk_widget_queue_draw(da);
  }
static void set_rotation_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, GtkWidget *da)
  {
    if(value>360.0) yaw=360.0;
    else if(value<0.0) yaw=0.0;
    else yaw=value; 

    gtk_widget_queue_draw(da);
  }
static void euler_rotation()
  {
    gint i=0;
    gdouble pitch_r=(pitch*G_PI/180.0);
    gdouble roll_r=(roll*G_PI/180.0);
    gdouble yaw_r=(yaw*G_PI/180.0);

    /*
      The following is a bit of a mess seems to get 2 out of 3 angles to match.
    */
    gdouble x=0;
    gdouble y=0;
    gdouble z=0;
    if(drawing_id==0)
      {
        for(i=0;i<3;i++)
          {
            //Rx
            t2[i][0]=(t1[i][0]*cos(yaw_r))+(t1[i][1]*-sin(yaw_r))+(t1[i][2]*0.0);
            t2[i][1]=(t1[i][0]*sin(yaw_r))+(t1[i][1]*cos(yaw_r))+(t1[i][2]*0.0);
            t2[i][2]=(t1[i][0]*0.0)+(t1[i][1]*0.0)+(t1[i][2]*1.0);
            x=t2[i][0];
            y=t2[i][1];
            z=t2[i][2];
            //Ry
            t2[i][0]=(x*cos(roll_r))+(y*0.0)+(z*sin(roll_r));
            t2[i][1]=(x*0.0)+(y*1.0)+(z*0.0);
            t2[i][2]=(x*-sin(roll_r))+(y*0.0)+(z*cos(roll_r));
            x=t2[i][0];
            y=t2[i][1];
            z=t2[i][2];
            //Rz
            t2[i][0]=(x*1.0)+(y*0.0)+(z*0.0);
            t2[i][1]=(x*0.0)+(y*cos(pitch_r))+(z*-sin(pitch_r));
            t2[i][2]=(x*0.0)+(y*sin(pitch_r))+(z*cos(pitch_r));
          }
      }
    else if(drawing_id==1||drawing_id==2||drawing_id==3)
      {
        struct point3d *p1=&g_array_index(cube, struct point3d, 0);
        struct point3d *p2=&g_array_index(cube_rotated, struct point3d, 0);
        for(i=0;i<cube->len;i++)
          {
            //Rx
            (*p2).x=((*p1).x*cos(yaw_r))+((*p1).y*-sin(yaw_r))+((*p1).z*0.0);
            (*p2).y=((*p1).x*sin(yaw_r))+((*p1).y*cos(yaw_r))+((*p1).z*0.0);
            (*p2).z=((*p1).x*0.0)+((*p1).y*0.0)+((*p1).z*1.0);
            x=(*p2).x;
            y=(*p2).y;
            z=(*p2).z;
            //Ry
            (*p2).x=(x*cos(roll_r))+(y*0.0)+(z*sin(roll_r));
            (*p2).y=(x*0.0)+(y*1.0)+(z*0.0);
            (*p2).z=(x*-sin(roll_r))+(y*0.0)+(z*cos(roll_r));
            x=(*p2).x;
            y=(*p2).y;
            z=(*p2).z;
            //Rz
            (*p2).x=(x*1.0)+(y*0.0)+(z*0.0);
            (*p2).y=(x*0.0)+(y*cos(pitch_r))+(z*-sin(pitch_r));
            (*p2).z=(x*0.0)+(y*sin(pitch_r))+(z*cos(pitch_r));
            p1++;p2++;
          }
      } 
    else
      {
        //Rotate last cube with quaternions.
      } 
 
  }
static void quaternion_rotation(gdouble yaw_r, gdouble roll_r, gdouble pitch_r, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll_r=roll_r/2.0;
    pitch_r=pitch_r/2.0;
    yaw_r=yaw_r/2.0;
    gdouble qi=sin(pitch_r)*cos(roll_r)*cos(yaw_r)-cos(pitch_r)*sin(roll_r)*sin(yaw_r);
    gdouble qj=cos(pitch_r)*sin(roll_r)*cos(yaw_r)+sin(pitch_r)*cos(roll_r)*sin(yaw_r);
    gdouble qk=cos(pitch_r)*cos(roll_r)*sin(yaw_r)-sin(pitch_r)*sin(roll_r)*cos(yaw_r);
    gdouble qr=cos(pitch_r)*cos(roll_r)*cos(yaw_r)+sin(pitch_r)*sin(roll_r)*sin(yaw_r);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
static void rotate_cube_quaternion(gdouble qrs[9])
  {
    gint i=0;
    struct point3d *p1=&g_array_index(cube, struct point3d, 0);
    struct point3d *p2=&g_array_index(cube_rotated, struct point3d, 0);
    for(i=0;i<cube->len;i++)
      {
        (*p2).x=((*p1).x*qrs[0])+((*p1).y*qrs[1])+((*p1).z*qrs[2]);
        (*p2).y=((*p1).x*qrs[3])+((*p1).y*qrs[4])+((*p1).z*qrs[5]);
        (*p2).z=((*p1).x*qrs[6])+((*p1).y*qrs[7])+((*p1).z*qrs[8]);
        p1++,p2++;
      }
  }
static void matrix_multiply_rotations(gdouble qrs1[9], gdouble qrs2[9], gdouble qrs3[9])
  {
    qrs3[0]=qrs1[0]*qrs2[0]+qrs1[3]*qrs2[1]+qrs1[6]*qrs2[2];
    qrs3[1]=qrs1[1]*qrs2[0]+qrs1[4]*qrs2[1]+qrs1[7]*qrs2[2];
    qrs3[2]=qrs1[2]*qrs2[0]+qrs1[5]*qrs2[1]+qrs1[8]*qrs2[2];

    qrs3[3]=qrs1[0]*qrs2[3]+qrs1[3]*qrs2[4]+qrs1[6]*qrs2[5];
    qrs3[4]=qrs1[1]*qrs2[3]+qrs1[4]*qrs2[4]+qrs1[7]*qrs2[5];
    qrs3[5]=qrs1[2]*qrs2[3]+qrs1[5]*qrs2[4]+qrs1[8]*qrs2[5];

    qrs3[6]=qrs1[0]*qrs2[6]+qrs1[3]*qrs2[7]+qrs1[6]*qrs2[8];
    qrs3[7]=qrs1[1]*qrs2[6]+qrs1[4]*qrs2[7]+qrs1[7]*qrs2[8];
    qrs3[8]=qrs1[2]*qrs2[6]+qrs1[5]*qrs2[7]+qrs1[8]*qrs2[8];
  }
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data)
  {
    //Apply angles of rotation for animation.    
    if(pitch>359.0) pitch=0.0;
    else pitch+=0.2;
    if(roll>359.0) roll=0.0;
    else roll+=0.5;
    if(yaw>359.0) yaw=0.0;
    else yaw+=0.5;

    gtk_widget_queue_draw(drawing);
    return G_SOURCE_CONTINUE;
  }
static gboolean draw_shape(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;

    //Paint background.
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_paint(cr);

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 1.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Draw in the center
    cairo_translate(cr, width/2.0, height/2.0); 

    if(drawing_id==0)
      {
        euler_rotation();
        cairo_set_line_width(cr, 10.0);
        cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
        draw_triangle_with_rotation(cr, w1, h1);
      }
    else if(drawing_id==1)
      {
        euler_rotation();
        cairo_set_line_width(cr, 2.0); 
        draw_cube_with_rotation1(cr, w1, h1);
      }
    else if(drawing_id==2)
      {
        euler_rotation();
        cairo_set_line_width(cr, 2.0); 
        draw_cube_with_rotation2(cr, w1, h1);
      }
    else if(drawing_id==3)
      {
        euler_rotation();
        draw_cube_with_rotation3(cr, w1, h1);
      }
    else
      {
        //Rotate with quaternions.
        draw_cube_with_rotation4(cr, w1, h1);
      }

    return FALSE;
  }
static void draw_triangle_with_rotation(cairo_t *cr, gdouble w1, gdouble h1)
  {
    cairo_text_extents_t extents;
    gdouble pitch_r=(pitch*G_PI/180.0);
    gdouble roll_r=(roll*G_PI/180.0);
    gdouble yaw_r=(yaw*G_PI/180.0);

    //Draw Euler rotated triangle in blue.
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    w1=3.0*w1;
    h1=3.0*h1;
    cairo_move_to(cr, w1*t2[0][0], h1*t2[0][1]);
    cairo_line_to(cr, w1*t2[1][0], h1*t2[1][1]);
    cairo_stroke(cr);
    cairo_move_to(cr, w1*t2[1][0], h1*t2[1][1]);
    cairo_line_to(cr, w1*t2[2][0], h1*t2[2][1]);
    cairo_stroke(cr);
    cairo_move_to(cr, w1*t2[2][0], h1*t2[2][1]);
    cairo_line_to(cr, w1*t2[0][0], h1*t2[0][1]);
    cairo_stroke(cr);

    //Draw cairo rotations in green.
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    cairo_set_line_width(cr, 6.0);
    cairo_matrix_t matrix1;
    gdouble roll_cos=cos(roll_r);
    gdouble pitch_cos=cos(pitch_r);
    cairo_matrix_init(&matrix1, roll_cos, 0.0, 0.0, pitch_cos, 0.0, 0.0);
    cairo_transform(cr, &matrix1);
    cairo_rotate(cr, yaw_r);
    cairo_move_to(cr, w1*t1[0][0], h1*t1[0][1]);
    cairo_line_to(cr, w1*t1[1][0], h1*t1[1][1]);
    cairo_stroke(cr);
    cairo_move_to(cr, w1*t1[1][0], h1*t1[1][1]);
    cairo_line_to(cr, w1*t1[2][0], h1*t1[2][1]);
    cairo_stroke(cr);
    cairo_move_to(cr, w1*t1[2][0], h1*t1[2][1]);
    cairo_line_to(cr, w1*t1[0][0], h1*t1[0][1]);
    cairo_stroke(cr);
    //Add some text.
    cairo_set_font_size(cr, 40.0);
    cairo_text_extents(cr, "cairo", &extents);
    cairo_move_to(cr, -extents.width/2.0, extents.height); 
    cairo_show_text(cr, "cairo"); 
  }
static void draw_cube_with_rotation1(cairo_t *cr, gdouble w1, gdouble h1)
  {
    //Rotate cube and text with cairo.
    gdouble pitch_r=(pitch*G_PI/180.0);
    gdouble roll_r=(roll*G_PI/180.0);
    gdouble yaw_r=(yaw*G_PI/180.0);
    gdouble err=0.0000000001;
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    cairo_set_font_size(cr, 50.0);
    gboolean draw_all_sides=FALSE;

    struct point3d d1=g_array_index(cube_rotated, struct point3d, 0);
    struct point3d d2=g_array_index(cube_rotated, struct point3d, 1);
    struct point3d d3=g_array_index(cube_rotated, struct point3d, 2);
    struct point3d d4=g_array_index(cube_rotated, struct point3d, 3);
    struct point3d d5=g_array_index(cube_rotated, struct point3d, 4);
    struct point3d d6=g_array_index(cube_rotated, struct point3d, 5);
    struct point3d d7=g_array_index(cube_rotated, struct point3d, 6);
    struct point3d d8=g_array_index(cube_rotated, struct point3d, 7);

    //Side 1.
    if(draw_all_sides||(d1.z+d2.z+d3.z+d4.z>0.0))
      {
        cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.4);
        cairo_move_to(cr, w1*d1.x, h1*d1.y);
        cairo_line_to(cr, w1*d2.x, h1*d2.y);
        cairo_stroke_preserve(cr);
        cairo_line_to(cr, w1*d3.x, h1*d3.y);
        cairo_stroke_preserve(cr);
        cairo_line_to(cr, w1*d4.x, h1*d4.y);
        cairo_stroke_preserve(cr);
        cairo_close_path(cr);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        //Add some text. Translate the text to a face on the cube.
        cairo_save(cr);
        cairo_matrix_init(&matrix1, cos(roll_r)+err, 0.0, 0.0, cos(pitch_r)+err, w1*(d1.x+d2.x+d3.x+d4.x)/4.0, h1*(d1.y+d2.y+d3.y+d4.y)/4.0);
        cairo_transform(cr, &matrix1);
        cairo_rotate(cr, yaw_r);
        cairo_text_extents(cr, "cairo", &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, "cairo");
        cairo_restore(cr);
      }
   
    //Side 2.
    if(draw_all_sides||(d5.z+d6.z+d7.z+d8.z>0.0))
      {
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.4);
        cairo_move_to(cr, w1*d5.x, h1*d5.y);
        cairo_line_to(cr, w1*d6.x, h1*d6.y);
        cairo_stroke_preserve(cr);
        cairo_line_to(cr, w1*d7.x, h1*d7.y);
        cairo_stroke_preserve(cr);
        cairo_line_to(cr, w1*d8.x, h1*d8.y);
        cairo_stroke_preserve(cr);
        cairo_close_path(cr);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        cairo_save(cr);
        cairo_matrix_init(&matrix1, cos(roll_r)+err, 0.0, 0.0, cos(pitch_r-G_PI)+err, w1*(d5.x+d6.x+d7.x+d8.x)/4.0, h1*(d5.y+d6.y+d7.y+d8.y)/4.0);
        cairo_transform(cr, &matrix1);
        cairo_rotate(cr, -yaw_r);
        cairo_text_extents(cr, "2", &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, "2");
        cairo_restore(cr);
      }

    //Side 3.
    if(draw_all_sides||(d1.z+d5.z+d6.z+d2.z>0.0))
      {
        cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 0.4);
        cairo_move_to(cr, w1*d1.x, h1*d1.y);
        cairo_line_to(cr, w1*d5.x, h1*d5.y);
        cairo_stroke_preserve(cr);
        cairo_line_to(cr, w1*d6.x, h1*d6.y);
        cairo_stroke_preserve(cr);
        cairo_line_to(cr, w1*d2.x, h1*d2.y);
        cairo_stroke_preserve(cr);
        cairo_close_path(cr);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        cairo_save(cr);
        cairo_matrix_init(&matrix1, cos(roll_r+G_PI/2.0)+err, 0.0, 0.0, -cos(yaw_r+G_PI)+err, w1*(d1.x+d5.x+d6.x+d2.x)/4.0, h1*(d1.y+d5.y+d6.y+d2.y)/4.0);
        cairo_transform(cr, &matrix1);
        //cairo_rotate(cr, -yaw_r);
        cairo_text_extents(cr, "3", &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, "3");
        cairo_restore(cr);
      }

    //Side 4.
    if(draw_all_sides||(d2.z+d6.z+d7.z+d3.z>0.0))
      {
        cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 0.4);
        cairo_move_to(cr, w1*d2.x, h1*d2.y);
        cairo_line_to(cr, w1*d6.x, h1*d6.y);
        cairo_stroke_preserve(cr);
        cairo_line_to(cr, w1*d7.x, h1*d7.y);
        cairo_stroke_preserve(cr);
        cairo_line_to(cr, w1*d3.x, h1*d3.y);
        cairo_stroke_preserve(cr);
        cairo_close_path(cr);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        cairo_save(cr);
        cairo_matrix_init(&matrix1, cos(yaw_r)+err, 0.0, 0.0, sin(pitch_r)+err, w1*(d2.x+d6.x+d7.x+d3.x)/4.0, h1*(d2.y+d6.y+d7.y+d3.y)/4.0);
        cairo_transform(cr, &matrix1);
        cairo_rotate(cr, roll_r-G_PI/2.0);
        cairo_text_extents(cr, "4", &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, "4");
        cairo_restore(cr);
      }

    //Side 5.
    if(draw_all_sides||(d3.z+d7.z+d8.z+d4.z>0.0))
      {
        cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 0.4);
        cairo_move_to(cr, w1*d3.x, h1*d3.y);
        cairo_line_to(cr, w1*d7.x, h1*d7.y);
        cairo_stroke_preserve(cr);
        cairo_line_to(cr, w1*d8.x, h1*d8.y);
        cairo_stroke_preserve(cr);
        cairo_line_to(cr, w1*d4.x, h1*d4.y);
        cairo_stroke_preserve(cr);
        cairo_close_path(cr);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        cairo_save(cr);
        cairo_matrix_init(&matrix1, cos(roll_r-G_PI/2.0)+err, 0.0, 0.0, cos(pitch_r)+err, w1*(d3.x+d7.x+d8.x+d4.x)/4.0, h1*(d3.y+d7.y+d8.y+d4.y)/4.0);
        cairo_transform(cr, &matrix1);
        cairo_rotate(cr, yaw_r);
        cairo_text_extents(cr, "5", &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, "5");
        cairo_restore(cr);
      }

    //Side 6.
    if(draw_all_sides||(d4.z+d8.z+d5.z+d1.z>0.0))
      {
        cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 0.4);
        cairo_move_to(cr, w1*d4.x, h1*d4.y);
        cairo_line_to(cr, w1*d8.x, h1*d8.y);
        cairo_stroke_preserve(cr);
        cairo_line_to(cr, w1*d5.x, h1*d5.y);
        cairo_stroke_preserve(cr);
        cairo_line_to(cr, w1*d1.x, h1*d1.y);
        cairo_stroke_preserve(cr);
        cairo_close_path(cr);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        cairo_save(cr);
        cairo_matrix_init(&matrix1, cos(yaw_r)+err, 0.0, 0.0, sin(pitch_r)+err, w1*(d4.x+d8.x+d5.x+d1.x)/4.0, h1*(d4.y+d8.y+d5.y+d1.y)/4.0);
        cairo_transform(cr, &matrix1);
        cairo_rotate(cr, roll_r);
        cairo_text_extents(cr, "6", &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, "6");
        cairo_restore(cr);
      }
  }
static void draw_cube_with_rotation2(cairo_t *cr, gdouble w1, gdouble h1)
  {
    //Try matching a cairo rotated plane with a euler angle rotated plane.
    gdouble pitch_r=0.0;
    gdouble roll_r=0.0;
    gdouble yaw_r=0.0;
    gdouble err=0.0000000001;
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    cairo_set_font_size(cr, 50.0);
    gboolean draw_all_sides=TRUE;

    struct point3d d1=g_array_index(cube_rotated, struct point3d, 0);
    struct point3d d2=g_array_index(cube_rotated, struct point3d, 1);
    struct point3d d3=g_array_index(cube_rotated, struct point3d, 2);
    struct point3d d4=g_array_index(cube_rotated, struct point3d, 3);
    struct point3d d5=g_array_index(cube_rotated, struct point3d, 4);
    struct point3d d6=g_array_index(cube_rotated, struct point3d, 5);
    struct point3d d7=g_array_index(cube_rotated, struct point3d, 6);
    struct point3d d8=g_array_index(cube_rotated, struct point3d, 7);
    struct point3d *p1=&g_array_index(cube, struct point3d, 2);

    //Side 1.
    if(draw_all_sides||(d1.z+d2.z+d3.z+d4.z>0.0))
      {
        cairo_save(cr);
        pitch_r=(pitch*G_PI/180.0);
        roll_r=(roll*G_PI/180.0);
        yaw_r=(yaw*G_PI/180.0);
        cairo_matrix_init(&matrix1, cos(roll_r)+err, 0.0, 0.0, cos(pitch_r)+err, w1*(d1.x+d2.x+d3.x+d4.x)/4.0, h1*(d1.y+d2.y+d3.y+d4.y)/4.0);
        cairo_transform(cr, &matrix1);
        cairo_rotate(cr, yaw_r);

        cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.4);
        cairo_rectangle(cr, w1*(*p1).x, h1*(*p1).y, 4.0*w1, 4.0*h1);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        cairo_text_extents(cr, "cairo", &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, "cairo");
        cairo_restore(cr);
      }
   
    //Side 2.
    if(draw_all_sides||(d5.z+d6.z+d7.z+d8.z>0.0))
      {
        cairo_save(cr);
        pitch_r=(pitch*G_PI/180.0);
        roll_r=(roll*G_PI/180.0);
        yaw_r=(yaw*G_PI/180.0+G_PI);
        cairo_matrix_init(&matrix1, cos(roll_r)+err, 0.0, 0.0, cos(pitch_r)+err, w1*(d5.x+d6.x+d7.x+d8.x)/4.0, h1*(d5.y+d6.y+d7.y+d8.y)/4.0);
        cairo_transform(cr, &matrix1);
        cairo_rotate(cr, yaw_r);

        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.4);
        cairo_rectangle(cr, w1*(*p1).x, h1*(*p1).y, 4.0*w1, 4.0*h1);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        cairo_text_extents(cr, "2", &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, "2");
        cairo_restore(cr);
      }

    //Side 3.
    if(draw_all_sides||(d1.z+d5.z+d6.z+d2.z>0.0))
      {
        cairo_save(cr);
        pitch_r=(pitch*G_PI/180.0);
        roll_r=((roll+270)*G_PI/180.0);
        yaw_r=(yaw*G_PI/180.0);
        cairo_matrix_init(&matrix1, cos(roll_r)+err, 0.0, 0.0, cos(pitch_r)+err, w1*(d1.x+d5.x+d6.x+d2.x)/4.0, h1*(d1.y+d5.y+d6.y+d2.y)/4.0);
        cairo_transform(cr, &matrix1);
        cairo_rotate(cr, roll_r);

        cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 0.4);
        cairo_rectangle(cr, w1*(*p1).x, h1*(*p1).y, 4.0*w1, 4.0*h1);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        cairo_text_extents(cr, "3", &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, "3");
        cairo_restore(cr);

        cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 0.4);
        cairo_move_to(cr, w1*d1.x, h1*d1.y);
        cairo_line_to(cr, w1*d5.x, h1*d5.y);
        cairo_stroke_preserve(cr);
        cairo_line_to(cr, w1*d6.x, h1*d6.y);
        cairo_stroke_preserve(cr);
        cairo_line_to(cr, w1*d2.x, h1*d2.y);
        cairo_stroke_preserve(cr);
        cairo_close_path(cr);
        cairo_fill(cr);
      }
  }
static void draw_cube_with_rotation3(cairo_t *cr, gdouble w1, gdouble h1)
  {
    //Just draw with cairo rotation, planes and numbers.
    gdouble pitch_r=0.0;
    gdouble roll_r=0.0;
    gdouble yaw_r=0.0;
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    cairo_set_font_size(cr, 50.0);
    gboolean draw_all_sides=TRUE;

    struct point3d d1=g_array_index(cube_rotated, struct point3d, 0);
    struct point3d d2=g_array_index(cube_rotated, struct point3d, 1);
    struct point3d d3=g_array_index(cube_rotated, struct point3d, 2);
    struct point3d d4=g_array_index(cube_rotated, struct point3d, 3);
    struct point3d d5=g_array_index(cube_rotated, struct point3d, 4);
    struct point3d d6=g_array_index(cube_rotated, struct point3d, 5);
    struct point3d d7=g_array_index(cube_rotated, struct point3d, 6);
    struct point3d d8=g_array_index(cube_rotated, struct point3d, 7);
    struct point3d *p1=&g_array_index(cube, struct point3d, 2);

    //Side 1.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    if(draw_all_sides||(d1.z+d2.z+d3.z+d4.z>0.0))
      {
        cairo_save(cr);
        pitch_r=(pitch*G_PI/180.0);
        roll_r=(roll*G_PI/180.0);
        yaw_r=(yaw*G_PI/180.0);
        cairo_matrix_init(&matrix1, cos(roll_r), 0.0, 0.0, cos(pitch_r), w1*(d1.x+d2.x+d3.x+d4.x)/4.0, h1*(d1.y+d2.y+d3.y+d4.y)/4.0);
        cairo_transform(cr, &matrix1);
        cairo_rotate(cr, yaw_r);

        cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.4);
        cairo_rectangle(cr, w1*(*p1).x, h1*(*p1).y, 4.0*w1, 4.0*h1);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        cairo_text_extents(cr, "1", &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, "1");
        cairo_restore(cr);
      }
   
    //Side 2.
    if(draw_all_sides||(d5.z+d6.z+d7.z+d8.z>0.0))
      {
        cairo_save(cr);
        pitch_r=(pitch*G_PI/180.0);
        roll_r=(roll*G_PI/180.0);
        yaw_r=(yaw*G_PI/180.0+G_PI);
        cairo_matrix_init(&matrix1, cos(roll_r), 0.0, 0.0, cos(pitch_r), w1*(d5.x+d6.x+d7.x+d8.x)/4.0, h1*(d5.y+d6.y+d7.y+d8.y)/4.0);
        cairo_transform(cr, &matrix1);
        cairo_rotate(cr, yaw_r);

        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.4);
        cairo_rectangle(cr, w1*(*p1).x, h1*(*p1).y, 4.0*w1, 4.0*h1);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        cairo_text_extents(cr, "2", &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, "2");
        cairo_restore(cr);
      }

    //Side 3.
    if(draw_all_sides||(d1.z+d5.z+d6.z+d2.z>0.0))
      {
        cairo_save(cr);
        pitch_r=(pitch*G_PI/180.0);
        roll_r=((roll+270)*G_PI/180.0);
        yaw_r=(yaw*G_PI/180.0);
        cairo_matrix_init(&matrix1, cos(roll_r), 0.0, 0.0, cos(pitch_r), w1*(d1.x+d5.x+d6.x+d2.x)/4.0, h1*(d1.y+d5.y+d6.y+d2.y)/4.0);
        cairo_transform(cr, &matrix1);
        cairo_rotate(cr, yaw_r);

        cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 0.4);
        cairo_rectangle(cr, w1*(*p1).x, h1*(*p1).y, 4.0*w1, 4.0*h1);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        cairo_text_extents(cr, "3", &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, "3");
        cairo_restore(cr);
      }
    
    //Side 4.
    if(draw_all_sides||(d2.z+d6.z+d7.z+d3.z>0.0))
      {
        cairo_save(cr);
        pitch_r=(pitch*G_PI/180.0);
        roll_r=((roll+90.0)*G_PI/180.0);
        yaw_r=(yaw*G_PI/180.0);
        cairo_matrix_init(&matrix1, cos(roll_r), 0.0, 0.0, cos(pitch_r), w1*(d2.x+d6.x+d7.x+d3.x)/4.0, h1*(d2.y+d6.y+d7.y+d3.y)/4.0);
        cairo_transform(cr, &matrix1);
        cairo_rotate(cr, yaw_r);

        cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 0.4);
        cairo_rectangle(cr, w1*(*p1).x, h1*(*p1).y, 4.0*w1, 4.0*h1);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        cairo_text_extents(cr, "4", &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, "4");
        cairo_restore(cr);
      }

    //Side 5.
    if(draw_all_sides||(d3.z+d7.z+d8.z+d4.z>0.0))
      {
        cairo_save(cr);
        pitch_r=((pitch+270.0)*G_PI/180.0);
        roll_r=((roll+270)*G_PI/180.0);
        yaw_r=(yaw*G_PI/180.0);
        cairo_matrix_init(&matrix1, cos(roll_r), 0.0, 0.0, cos(pitch_r), w1*(d3.x+d7.x+d8.x+d4.x)/4.0, h1*(d3.y+d7.y+d8.y+d4.y)/4.0);
        cairo_transform(cr, &matrix1);
        cairo_rotate(cr, yaw_r);

        cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 0.4);
        cairo_rectangle(cr, w1*(*p1).x, h1*(*p1).y, 4.0*w1, 4.0*h1);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        cairo_text_extents(cr, "5", &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, "5");
        cairo_restore(cr);
      }

    //Side 6.
    if(draw_all_sides||(d4.z+d8.z+d5.z+d1.z>0.0))
      {
        cairo_save(cr);
        pitch_r=((pitch+90)*G_PI/180.0);
        roll_r=((roll+270)*G_PI/180.0);
        yaw_r=(yaw*G_PI/180.0);
        cairo_matrix_init(&matrix1, cos(roll_r), 0.0, 0.0, cos(pitch_r), w1*(d4.x+d8.x+d5.x+d1.x)/4.0, h1*(d4.y+d8.y+d5.y+d1.y)/4.0);
        cairo_transform(cr, &matrix1);
        cairo_rotate(cr, yaw_r);

        cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 0.4);
        cairo_rectangle(cr, w1*(*p1).x, h1*(*p1).y, 4.0*w1, 4.0*h1);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        cairo_text_extents(cr, "6", &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, "6");
        cairo_restore(cr);
      }
  }
static void draw_cube_with_rotation4(cairo_t *cr, gdouble w1, gdouble h1)
  {
    /*
        Draw the cube with painters algorithm or sorting the faces of the cube. Draw from back
    to front. This way you can draw with transparency and have the back facing fonts a different
    color than the front facing fonts. If you just want the front facing sides set the 
    draw_all_sides=FALSE and maybe change to solid colors.
    */
    gint i=0;
    gdouble pitch_r=(pitch*G_PI/180.0);
    gdouble roll_r=(roll*G_PI/180.0);
    gdouble yaw_r=(yaw*G_PI/180.0);
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    cairo_set_font_size(cr, 50.0);
    //Draw all the sides or just the top three of the cube.
    gboolean draw_all_sides=TRUE;
    gdouble qrs1[9];
    gdouble qrs2[9];
    gdouble qrs3[9];

    quaternion_rotation(yaw_r, roll_r, pitch_r, qrs1);
    rotate_cube_quaternion(qrs1);

    struct point3d d1=g_array_index(cube_rotated, struct point3d, 0);
    struct point3d d2=g_array_index(cube_rotated, struct point3d, 1);
    struct point3d d3=g_array_index(cube_rotated, struct point3d, 2);
    struct point3d d4=g_array_index(cube_rotated, struct point3d, 3);
    struct point3d d5=g_array_index(cube_rotated, struct point3d, 4);
    struct point3d d6=g_array_index(cube_rotated, struct point3d, 5);
    struct point3d d7=g_array_index(cube_rotated, struct point3d, 6);
    struct point3d d8=g_array_index(cube_rotated, struct point3d, 7);

    //Sort the planes.
    struct plane_order po[6];
    po[0].id=0;
    po[0].value=d1.z+d2.z+d3.z+d4.z;
    po[1].id=1;
    po[1].value=d5.z+d6.z+d7.z+d8.z;
    po[2].id=2;
    po[2].value=d3.z+d7.z+d8.z+d4.z;
    po[3].id=3;
    po[3].value=d2.z+d6.z+d7.z+d3.z;
    po[4].id=4;
    po[4].value=d1.z+d5.z+d6.z+d2.z;
    po[5].id=5;
    po[5].value=d4.z+d8.z+d5.z+d1.z;
    qsort(po, 6, sizeof(struct plane_order), compare_planes);

    //Draw cube faces in z-order.
    for(i=0;i<6;i++)
      {
        if(po[i].id==0&&(draw_all_sides||po[i].value>0.0))
          {
            cairo_save(cr);        
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.3);
            cairo_move_to(cr, w1*d1.x, h1*d1.y);
            cairo_line_to(cr, w1*d2.x, h1*d2.y);
            cairo_stroke_preserve(cr);
            cairo_line_to(cr, w1*d3.x, h1*d3.y);
            cairo_stroke_preserve(cr);
            cairo_line_to(cr, w1*d4.x, h1*d4.y);
            cairo_stroke_preserve(cr);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);

            quaternion_rotation(yaw_r, roll_r, pitch_r, qrs1);
            cairo_matrix_init(&matrix1, qrs1[0], qrs1[3], qrs1[1], qrs1[4], w1*(d1.x+d2.x+d3.x+d4.x)/4.0, h1*(d1.y+d2.y+d3.y+d4.y)/4.0);
            cairo_transform(cr, &matrix1);

            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_rectangle(cr, -60, -60, 120, 120);
            cairo_stroke(cr);  

            if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
            cairo_text_extents(cr, "4", &extents);
            cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
            cairo_show_text(cr, "4");
            cairo_restore(cr);
          }
  
        else if(po[i].id==1&&(draw_all_sides||po[i].value>0.0))
          {
            cairo_save(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.3);
            cairo_move_to(cr, w1*d5.x, h1*d5.y);
            cairo_line_to(cr, w1*d6.x, h1*d6.y);
            cairo_stroke_preserve(cr);
            cairo_line_to(cr, w1*d7.x, h1*d7.y);
            cairo_stroke_preserve(cr);
            cairo_line_to(cr, w1*d8.x, h1*d8.y);
            cairo_stroke_preserve(cr);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);

            quaternion_rotation(yaw_r, roll_r, pitch_r, qrs1);
            cairo_matrix_init(&matrix1, qrs1[0], qrs1[3], qrs1[1], qrs1[4], w1*(d5.x+d6.x+d7.x+d8.x)/4.0, h1*(d5.y+d6.y+d7.y+d8.y)/4.0);
            cairo_transform(cr, &matrix1);

            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_rectangle(cr, -60, -60, 120, 120);
            cairo_stroke(cr);  

            if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
            cairo_text_extents(cr, "3", &extents);
            cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
            cairo_show_text(cr, "3");
            cairo_restore(cr);
          }

        else if(po[i].id==2&&(draw_all_sides||po[i].value>0.0))
          {
            cairo_save(cr);
            cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 0.3);
            cairo_move_to(cr, w1*d3.x, h1*d3.y);
            cairo_line_to(cr, w1*d7.x, h1*d7.y);
            cairo_stroke_preserve(cr);
            cairo_line_to(cr, w1*d8.x, h1*d8.y);
            cairo_stroke_preserve(cr);
            cairo_line_to(cr, w1*d4.x, h1*d4.y);
            cairo_stroke_preserve(cr);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);

            quaternion_rotation(yaw_r, roll_r, pitch_r, qrs1);
            quaternion_rotation(0.0, -G_PI/2.0, 0.0, qrs2);
            matrix_multiply_rotations(qrs2, qrs1, qrs3);
            cairo_matrix_init(&matrix1, qrs3[0], qrs3[3], qrs3[1], qrs3[4], w1*(d3.x+d7.x+d8.x+d4.x)/4.0, h1*(d3.y+d7.y+d8.y+d4.y)/4.0);
            cairo_transform(cr, &matrix1);

            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_rectangle(cr, -60, -60, 120, 120);
            cairo_stroke(cr);  

            if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
            cairo_text_extents(cr, "1", &extents);
            cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
            cairo_show_text(cr, "1");
            cairo_restore(cr);
          }

        else if(po[i].id==3&&(draw_all_sides||po[i].value>0.0))
          {
            cairo_save(cr);
            cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 0.3);
            cairo_move_to(cr, w1*d2.x, h1*d2.y);
            cairo_line_to(cr, w1*d6.x, h1*d6.y);
            cairo_stroke_preserve(cr);
            cairo_line_to(cr, w1*d7.x, h1*d7.y);
            cairo_stroke_preserve(cr);
            cairo_line_to(cr, w1*d3.x, h1*d3.y);
            cairo_stroke_preserve(cr);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);

            quaternion_rotation(yaw_r, roll_r, pitch_r-G_PI/2.0, qrs1);
            cairo_matrix_init(&matrix1, qrs1[0], qrs1[3], qrs1[1], qrs1[4], w1*(d2.x+d6.x+d7.x+d3.x)/4.0, h1*(d2.y+d6.y+d7.y+d3.y)/4.0);
            cairo_transform(cr, &matrix1);

            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_rectangle(cr, -60, -60, 120, 120);
            cairo_stroke(cr);  

            if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
            cairo_text_extents(cr, "5", &extents);
            cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
            cairo_show_text(cr, "5");
            cairo_restore(cr);
          }

        else if(po[i].id==4&&(draw_all_sides||po[i].value>0.0))
          {
            cairo_save(cr);
            cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 0.3);
            cairo_move_to(cr, w1*d1.x, h1*d1.y);
            cairo_line_to(cr, w1*d5.x, h1*d5.y);
            cairo_stroke_preserve(cr);
            cairo_line_to(cr, w1*d6.x, h1*d6.y);
            cairo_stroke_preserve(cr);
            cairo_line_to(cr, w1*d2.x, h1*d2.y);
            cairo_stroke_preserve(cr);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);

            quaternion_rotation(yaw_r, roll_r, pitch_r, qrs1);
            quaternion_rotation(0.0, G_PI/2.0, 0.0, qrs2);
            matrix_multiply_rotations(qrs2, qrs1, qrs3);
            cairo_matrix_init(&matrix1, qrs3[0], qrs3[3], qrs3[1], qrs3[4], w1*(d1.x+d5.x+d6.x+d2.x)/4.0, h1*(d1.y+d5.y+d6.y+d2.y)/4.0);
            cairo_transform(cr, &matrix1);

            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_rectangle(cr, -60, -60, 120, 120);
            cairo_stroke(cr);  

            if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
            cairo_text_extents(cr, "6", &extents);
            cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
            cairo_show_text(cr, "6");
            cairo_restore(cr);
          }

        else if(po[i].id==5&&(draw_all_sides||po[i].value>0.0))
          {
            cairo_save(cr);
            cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 0.3);
            cairo_move_to(cr, w1*d4.x, h1*d4.y);
            cairo_line_to(cr, w1*d8.x, h1*d8.y);
            cairo_stroke_preserve(cr);
            cairo_line_to(cr, w1*d5.x, h1*d5.y);
            cairo_stroke_preserve(cr);
            cairo_line_to(cr, w1*d1.x, h1*d1.y);
            cairo_stroke_preserve(cr);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);

            quaternion_rotation(yaw_r, roll_r, pitch_r-G_PI/2.0, qrs1);
            cairo_matrix_init(&matrix1, qrs1[0], qrs1[3], qrs1[1], qrs1[4], w1*(d4.x+d8.x+d5.x+d1.x)/4.0, h1*(d4.y+d8.y+d5.y+d1.y)/4.0);
            cairo_transform(cr, &matrix1);

            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_rectangle(cr, -60, -60, 120, 120);
            cairo_stroke(cr);  

            if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
            cairo_text_extents(cr, "2", &extents);
            cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
            cairo_show_text(cr, "2");
            cairo_restore(cr);
          }
      }    
  }
int compare_planes(const void *a, const void *b)
  {
    return(((struct plane_order*)a)->value-((struct plane_order*)b)->value);
  }


