
/* 
    Test drawing an ellipse with transforming a circle, the Steiner method and parametrically.
Look at different line caps to see how they affect the drawing. Change the drawing method from curve
to line to see how the ends are being drawn.
    This uses a wide line to draw with. A better method would be to draw the border of the arc with a
thin line and then fill. That way you don't get the odd looking ends. The circular_gradient programs
show how it can be done.
 
    gcc -Wall ellipse1.c -o ellipse1 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>

struct point{
  gdouble x;
  gdouble y;
}points;

struct controls{
  gdouble x1;
  gdouble y1;
  gdouble x2;
  gdouble y2;
}controls;

static gint method_id=0;
static gint line_id=0;
static gint cap_id=0;
static gint join_id=0;
static gint smooth_id=0;

static void change_method(GtkComboBox *combo, gpointer data);
static void change_line(GtkComboBox *combo, gpointer data);
static void change_cap(GtkComboBox *combo, gpointer data);
static void change_join(GtkComboBox *combo, gpointer data);
static void change_smooth(GtkComboBox *combo, gpointer data);
static gboolean draw_circle(GtkWidget *da, cairo_t *cr, gpointer data);
static void draw_ellipse(GtkWidget *da, cairo_t *cr);
static void get_elliptical_points(GArray *coords1, gdouble w1, gdouble h1);
static void get_steiner_points(GArray *coords1, gdouble w1, gdouble h1);
static GArray* control_points_from_coords2(const GArray *dataPoints);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Ellipse");
    gtk_window_set_default_size(GTK_WINDOW(window), 450, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);
    g_signal_connect(da, "draw", G_CALLBACK(draw_circle), NULL);

    GtkWidget *combo_method=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_method, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_method), 0, "1", "Transform Circle");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_method), 1, "2", "Elliptical Points");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_method), 2, "3", "Steiner Points");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_method), 0);
    g_signal_connect(combo_method, "changed", G_CALLBACK(change_method), da);

    GtkWidget *combo_line=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_line, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_line), 0, "1", "Draw Curves");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_line), 1, "2", "Draw Lines");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_line), 0);
    g_signal_connect(combo_line, "changed", G_CALLBACK(change_line), da);

    GtkWidget *combo_cap=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_cap, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_cap), 0, "1", "Line Cap Butt");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_cap), 1, "2", "Line Cap Round");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_cap), 2, "3", "Line Cap Square");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_cap), 0);
    g_signal_connect(combo_cap, "changed", G_CALLBACK(change_cap), da);

    GtkWidget *combo_join=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_join, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_join), 0, "1", "Line Join Miter");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_join), 1, "2", "Line Join Bevel");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_join), 2, "3", "Line Join Round");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_join), 0);
    g_signal_connect(combo_join, "changed", G_CALLBACK(change_join), da);

    GtkWidget *combo_smooth=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_smooth, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_smooth), 0, "1", "Smooth 48 Points");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_smooth), 1, "2", "Smooth 24 Points");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_smooth), 0);
    g_signal_connect(combo_smooth, "changed", G_CALLBACK(change_smooth), da);

    GtkWidget *grid1=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid1), combo_method, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_line, 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_cap, 2, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_join, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_smooth, 1, 1, 1, 1);

    GtkWidget *scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll, TRUE);
    gtk_widget_set_vexpand(scroll, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll), grid1);
   
    GtkWidget *grid2=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid2), da, 0, 0, 5, 5);
    gtk_grid_attach(GTK_GRID(grid2), scroll, 0, 6, 1, 1);
   
    gtk_container_add(GTK_CONTAINER(window), grid2);

    gtk_widget_show_all(window);

    gtk_main();

    return 0;  
  }
static void change_method(GtkComboBox *combo, gpointer data)
  {
    method_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void change_cap(GtkComboBox *combo, gpointer data)
  {
    cap_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void change_join(GtkComboBox *combo, gpointer data)
  {
    join_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void change_line(GtkComboBox *combo, gpointer data)
  {
    line_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void change_smooth(GtkComboBox *combo, gpointer data)
  {
    smooth_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static gboolean draw_circle(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    GTimer *timer=g_timer_new();
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;

    //Paint background.
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_paint(cr);

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 1.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr); 

    if(method_id>0) draw_ellipse(da, cr);
    else
      {
        cairo_set_source_rgb(cr, 0.0, 1.0, 1.0);
        cairo_set_line_width(cr, 50.0);
        if(cap_id==0) cairo_set_line_cap(cr, CAIRO_LINE_CAP_BUTT);
        else if(cap_id==1) cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
        else cairo_set_line_cap(cr, CAIRO_LINE_CAP_SQUARE);
        if(join_id==0) cairo_set_line_join(cr, CAIRO_LINE_JOIN_MITER);
        else if(join_id==1) cairo_set_line_join(cr, CAIRO_LINE_JOIN_BEVEL);
        else cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
        cairo_translate(cr, width/2.0, height/2.0);

        if(line_id==0)
          {
            cairo_save(cr);
            cairo_scale(cr, width/400.0, height/400.0*0.5);
            cairo_arc_negative(cr, 0.0, 0.0, 160, 0.0, G_PI);
            cairo_restore(cr);
            cairo_stroke(cr);
          }

        gint i=0;
        gdouble hour_radius=160;
        gdouble hour_start=0.0;
        gdouble next_hour=G_PI/24.0;
        gdouble temp_cos=0;
        gdouble temp_sin=0;
        if(line_id==1)
          {
            temp_cos=cos(hour_start);
            temp_sin=sin(hour_start);
            for(i=1;i<25;i++)
              {
                if(i==23) cairo_set_source_rgb(cr, 1.0, 0.0, 1.0);
                else cairo_set_source_rgb(cr, 0.0, 1.0, 1.0);
                cairo_save(cr);
                cairo_scale(cr, width/400.0, height/400.0*0.5);
                cairo_move_to(cr, temp_cos*hour_radius, temp_sin*hour_radius);
                temp_cos=cos(hour_start-(next_hour*i));
                temp_sin=sin(hour_start-(next_hour*i));
                cairo_line_to(cr, temp_cos*hour_radius, temp_sin*hour_radius);
                cairo_restore(cr);
                cairo_stroke(cr);
              }
          }

        cairo_scale(cr, width/400.0, height/400.0*0.5);
        cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
        cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
        cairo_set_line_width(cr, 5.0);
        for(i=0;i<48;i++)
          {
            temp_cos=cos(hour_start-(next_hour*i));
            temp_sin=sin(hour_start-(next_hour*i));
            cairo_move_to(cr, temp_cos*hour_radius, temp_sin*hour_radius);
            cairo_line_to(cr, temp_cos*hour_radius, temp_sin*hour_radius);
            cairo_stroke(cr);
          }
        
      }

    g_print("Time %f\n", g_timer_elapsed(timer, NULL));
    g_timer_destroy(timer);
    return FALSE;
  }
static void draw_ellipse(GtkWidget *da, cairo_t *cr)
  {
    gint i=0;
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=8.0*width/10.0;
    gdouble h1=8.0*height/10.0;

    //Draw from the center.
    cairo_translate(cr, width/2.0, height/2.0);

    GArray *coords1=g_array_new(FALSE, FALSE, sizeof(struct point));
    if(method_id==1) get_elliptical_points(coords1, w1, h1); 
    else get_steiner_points(coords1, w1, h1); 

    //Smooth with 24 or 48 points. Changes the end of arc.
    if(smooth_id==1) g_array_remove_range(coords1, 25, 24);   
  
    //Get the control points.
    GArray *control1=control_points_from_coords2(coords1);

    //Draw the curves.
    cairo_set_source_rgb(cr, 0.0, 1.0, 1.0);
    cairo_set_line_width(cr, 50.0);
    if(cap_id==0) cairo_set_line_cap(cr, CAIRO_LINE_CAP_BUTT);
    else if(cap_id==1) cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
    else cairo_set_line_cap(cr, CAIRO_LINE_CAP_SQUARE);
    if(join_id==0) cairo_set_line_join(cr, CAIRO_LINE_JOIN_MITER);
    else if(join_id==1) cairo_set_line_join(cr, CAIRO_LINE_JOIN_BEVEL);
    else cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    struct point d1;
    struct point d2;
    struct controls c1; 
    gint length=0;
    if(smooth_id==0) length=(coords1->len-1)/2;
    else length=coords1->len-1;
    for(i=0;i<length;i++)
      {
        d1=g_array_index(coords1, struct point, i);
        d2=g_array_index(coords1, struct point, i+1);
        c1=g_array_index(control1, struct controls, i);
        if(i==22) cairo_set_source_rgb(cr, 1.0, 0.0, 1.0);
        else cairo_set_source_rgb(cr, 0.0, 1.0, 1.0);
        cairo_move_to(cr, d1.x, d1.y);
        if(line_id==0) cairo_curve_to(cr, c1.x1, c1.y1, c1.x2, c1.y2, d2.x, d2.y);
        else cairo_line_to(cr, d2.x, d2.y);
        cairo_stroke(cr);
      }

    //Draw the points.
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_set_line_width(cr, 3.0);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
    if(smooth_id==0) length=coords1->len;
    else length=2*coords1->len;
    for(i=0;i<length;i++)
      {
        d1=g_array_index(coords1, struct point, i);
        cairo_move_to(cr, d1.x, d1.y);
        cairo_line_to(cr, d1.x, d1.y);
        cairo_stroke(cr);
      }   
   
    g_array_free(coords1, TRUE);   
    g_array_free(control1, TRUE);  
  }
/*
  Points are evenly dispersed by angle but not along the arc.
*/
static void get_elliptical_points(GArray *coords1, gdouble w1, gdouble h1)
  {
    gint i=0;
    struct point p1;
    w1=w1/2.0;
    h1=h1/4.0;
    gdouble hour_radius=w1;
    gdouble hour_start=0.0;
    gdouble next_hour=G_PI/24.0;
    gdouble temp_cos=0;
    gdouble temp_sin=0;
    for(i=0;i<48;i++)
      {        
        temp_cos=cos(hour_start-(next_hour*i));
        temp_sin=sin(hour_start-(next_hour*i));
        hour_radius=(w1*h1)/sqrt((w1*w1*temp_sin*temp_sin) + (h1*h1*temp_cos*temp_cos));
        p1.x=temp_cos*hour_radius;
        p1.y=temp_sin*hour_radius;
        g_array_append_val(coords1, p1);          
      }
    //Add the last point.
    p1.x=w1;
    p1.y=0.0;
    g_array_append_val(coords1, p1);
  }
/*
  This evens out the points along the arc and draws a better ellipse.
*/
static void get_steiner_points(GArray *coords1, gdouble w1, gdouble h1)
  {
    gint i=0;
    gint j=0;
    struct point p1;
    //12 points per quadrant. 48 points total.
    gint points1=12;
    gint points2=2.0*points1;
    //Scale y up and down for testing the ellipse.
    gdouble scale_y=1.0;
    gdouble x1=w1;
    gdouble y1=0.5*h1;
    gdouble x2=w1;
    gdouble y2=0.0;
    gdouble x3=0.0;
    gdouble y3=0.5*h1;
    gdouble x4=w1;
    gdouble y4=0.5*h1;
    gdouble m1=0.0;
    gdouble m2=0.0;
    gdouble b1=0.0;
    gdouble b2=0.0;
    //Add the first point.
    p1.x=w1*0.5;
    p1.y=0.0;
    g_array_append_val(coords1, p1);
    for(i=0;i<4;i++)
      {
        if(i==2) y4=h1;
        for(j=0;j<points1;j++)
          {
            if(i==0)
              {
                x2=x2-w1/points1;
                y4=y4-h1/points2;
              }
            else if(i==1)
              {
                x4=x4-w1/points1;
                y2=y2+h1/points2;
              }
            else if(i==2)
              {
                x4=x4+w1/points1;
                y2=y2+h1/points2;
              }
            else
              {
                x2=x2+w1/points1;
                y4=y4-h1/points2;
              }
            m1=(y2-y1)/(x2-x1);
            m2=(y4-y3)/(x4-x3);
            b1=y2-m1*x2;
            b2=y4-m2*x4;
            //Divide by 0 problems at ends of ellipse.
            if(i==3&&j==points1-1)
              {
                p1.x=w1*0.5;
                p1.y=0.0;
              }
            else if(i==1&&j==points1-1)
              {
                p1.x=-w1*0.5;
                p1.y=0.0;
              }
            else
              {
                p1.x=(b2-b1)/(m1-m2)-w1*0.5;
                p1.y=(m1*b2-m2*b1)/(m1-m2)-scale_y*h1*0.5;
              }
            //g_print("x %f y %f\n", p1.x, p1.y);
            g_array_append_val(coords1, p1);
          } 
      }
  }
/*
    This is some exellent work done by Ramsundar Shandilya. Note the following for the original work
    and the rational behind it.
    
    https://medium.com/@ramshandilya/draw-smooth-curves-through-a-set-of-points-in-ios-34f6d73c8f9

    https://github.com/Ramshandilya/Bezier

    The MIT License (MIT)

    Copyright (c) 2015 Ramsundar Shandilya

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
  
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

    This is a translation of the original Swift code to C. It makes the function easy to use with GTK+,
    cairo and glib.
*/
static GArray* control_points_from_coords2(const GArray *dataPoints)
  {  
    gint i=0;
    GArray *controlPoints=NULL;      
    //Number of Segments
    gint count=0;
    if(dataPoints!=NULL) count=dataPoints->len-1;
    gdouble *fCP=NULL;
    gdouble *sCP=NULL;

    if(count>0)
      {
        fCP=(gdouble*)g_malloc(2*count*sizeof(gdouble));
        sCP=(gdouble*)g_malloc(2*count*sizeof(gdouble));
      }
        
    //P0, P1, P2, P3 are the points for each segment, where P0 & P3 are the knots and P1, P2 are the control points.
    if(count<1||dataPoints==NULL)
      {
        //Return NULL.
        controlPoints=NULL;
        g_print("Can't get control points from coordinates. NULL returned.\n");
      }
    else if(count==1)
      {
        struct point P0=g_array_index(dataPoints, struct point, 0);
        struct point P3=g_array_index(dataPoints, struct point, 1);

        //Calculate First Control Point
        //3P1 = 2P0 + P3
        struct point P1;
        P1.x=(2.0*P0.x+P3.x)/3.0;
        P1.y=(2.0*P0.y+P3.y)/3.0;

        *(fCP)=P1.x;
        *(fCP+1)=P1.y;

        //Calculate second Control Point
        //P2 = 2P1 - P0
        struct point P2;
        P2.x=(2.0*P1.x-P0.x);
        P2.y=(2.0*P1.y-P0.x);

        *(sCP)=P2.x;
        *(sCP+1)=P2.y;      
      }
    else
      {
        gdouble *rhs=(gdouble*)g_malloc(2*count*sizeof(gdouble));
        gdouble *a=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *b=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *c=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble rhsValueX=0;
        gdouble rhsValueY=0;
        struct point P0;
        struct point P3;        
        gdouble m=0;
        gdouble b1=0;
        gdouble r2x=0;
        gdouble r2y=0;
        gdouble P1_x=0;
        gdouble P1_y=0;
   
        for(i=0;i<count;i++)
          {
            P0=g_array_index(dataPoints, struct point, i);
            P3=g_array_index(dataPoints, struct point, i+1);

            if(i==0)
              {
                *(a)=0.0;
                *(b)=2.0;
                *(c)=1.0;

                //rhs for first segment
                rhsValueX=P0.x+2.0*P3.x;
                rhsValueY=P0.y+2.0*P3.y;
              }
            else if(i==count-1)
              {
                *(a+i)=2.0;
                *(b+i)=7.0;
                *(c+i)=0.0;

                //rhs for last segment
                rhsValueX=8.0*P0.x+P3.x;
                rhsValueY=8.0*P0.y+P3.y;
              }
            else
              {
                *(a+i)=1.0;
                *(b+i)=4.0;
                *(c+i)=1.0;

                rhsValueX=4.0*P0.x+2.0*P3.x;
                rhsValueY=4.0*P0.y+2.0*P3.y;
              }
            *(rhs+i*2)=rhsValueX;
            *(rhs+i*2+1)=rhsValueY;
          }

        //Solve Ax=B. Use Tridiagonal matrix algorithm a.k.a Thomas Algorithm
        for(i=1;i<count;i++)
          {
            m=(*(a+i))/(*(b+i-1));

            b1=(*(b+i))-m*(*(c+i-1));
            *(b+i)=b1;

            r2x=(*(rhs+i*2))-m*(*(rhs+(i-1)*2));
            r2y=(*(rhs+i*2+1))-m*(*(rhs+(i-1)*2+1));

            *(rhs+i*2)=r2x;
            *(rhs+i*2+1)=r2y;
          }

        //Get First Control Points
        
        //Last control Point
        gdouble lastControlPointX=(*(rhs+2*count-2))/(*(b+count-1));
        gdouble lastControlPointY=(*(rhs+2*count-1))/(*(b+count-1));

        *(fCP+2*count-2)=lastControlPointX;
        *(fCP+2*count-1)=lastControlPointY;

        gdouble controlPointX=0;
        gdouble controlPointY=0;

        for(i=count-2;i>=0;--i)
          {
            controlPointX=(*(rhs+i*2)-(*(c+i))*(*(fCP+(i+1)*2)))/(*(b+i));
            controlPointY=(*(rhs+i*2+1)-(*(c+i))*(*(fCP+(i+1)*2+1)))/(*(b+i));

             *(fCP+i*2)=controlPointX;
             *(fCP+i*2+1)=controlPointY; 
          }

        //Compute second Control Points from first.
        for(i=0;i<count;i++)
          {
            if(i==count-1)
              {
                P3=g_array_index(dataPoints, struct point, i+1);
                P1_x=(*(fCP+i*2));
                P1_y=(*(fCP+i*2+1));

                controlPointX=(P3.x+P1_x)/2.0;
                controlPointY=(P3.y+P1_y)/2.0;

                *(sCP+count*2-2)=controlPointX;
                *(sCP+count*2-1)=controlPointY;
              }
            else
              {
                P3=g_array_index(dataPoints, struct point, i+1);                
                P1_x=(*(fCP+(i+1)*2));
                P1_y=(*(fCP+(i+1)*2+1));

                controlPointX=2.0*P3.x-P1_x;
                controlPointY=2.0*P3.y-P1_y;

                *(sCP+i*2)=controlPointX;
                *(sCP+i*2+1)=controlPointY;
              }

          }

        controlPoints=g_array_new(FALSE, FALSE, sizeof(struct controls));
        struct controls cp;
        for(i=0;i<count;i++)
          {
            cp.x1=(*(fCP+i*2));
            cp.y1=(*(fCP+i*2+1));
            cp.x2=(*(sCP+i*2));
            cp.y2=(*(sCP+i*2+1));
            g_array_append_val(controlPoints, cp);
          }

        g_free(rhs);
        g_free(a);
        g_free(b);
        g_free(c);
     }

    if(fCP!=NULL) g_free(fCP);
    if(sCP!=NULL) g_free(sCP);

    return controlPoints;
  }



