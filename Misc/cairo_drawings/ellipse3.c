
/* 
    Test rotating a 2d drawing in 3d. Try Euler angles and quaternions for comparison. Animate
a hi tech stealth plane.
     
    gcc -Wall ellipse3.c -o ellipse3 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<stdlib.h>
#include<math.h>

struct point{
  gdouble x;
  gdouble y;
}points;

struct controls{
  gdouble x1;
  gdouble y1;
  gdouble x2;
  gdouble y2;
}controls;

//The starting reference plane drawing.
gdouble plane_coords[11][2]={  
      {0.0, -3.0},  
      {-3.0, 0.0},  
      {-1.0, 0.0}, 
      {-1.5, 2.0},
      {-0.5, 2.0},
      {0.0, 1.0},
      {0.5, 2.0},
      {1.5, 2.0},
      {1.0, 0.0},
      {3.0, 0.0},
      {0.0, -3.0},   
      };
//The rotated plane drawing.
static gdouble plane_rotated[11][2];

//The starting reference circles. 36 points for the circle. 37 to close it with smoothing.
static gdouble circle1[37][3];
static gdouble circle2[37][3];
static gdouble circle3[37][3];
//Rotated circles.
static GArray *circle_r1=NULL;
static GArray *circle_r2=NULL;
static GArray *circle_r3=NULL;
//Reference circle for smoothing. The same as circle3.
static GArray *circle_ref=NULL;

//Combo ids for drawing options.
static gint rotate=0;
static gint angle=0;
static gint line_id=0;

//Tick id for animation frame clock.
static guint tick_id=0;

//The rotations.
static gdouble pitch=10.0;
static gdouble roll=10.0;
static gdouble yaw=10.0;

static void rotate_drawing(GtkComboBox *combo, gpointer data);
static void rotation_angle(GtkComboBox *combo, gpointer data);
static void change_line(GtkComboBox *combo, gpointer data);
static void set_rotation_pitch(GtkWidget *button, gpointer *rotations);
static void set_rotation_roll(GtkWidget *button, gpointer *rotations);
static void set_rotation_yaw(GtkWidget *button, gpointer *rotations);
static void euler_rotation();
static void quaternion_rotation();
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data);
static gboolean draw_ellipses(GtkWidget *da, cairo_t *cr, gpointer data);
static void draw_lines_with_rotation(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_smooth_with_rotation(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_plane(cairo_t *cr, gdouble w1, gdouble h1, gboolean with_euler);
static GArray* control_points_from_coords2(const GArray *dataPoints);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Ellipses");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    //Initialize reference circles.
    gint i=0;
    gdouble x=0;
    gdouble y=0;
    struct point p1;
    circle_ref=g_array_sized_new(FALSE, TRUE, sizeof(struct point), 37);
    for(i=0;i<37;i++)
      {
        x=4.0*cos(10.0*(gdouble)i*G_PI/180.0);
        y=4.0*sin(10.0*(gdouble)i*G_PI/180.0);

        circle1[i][0]=0.0;
        circle1[i][1]=x;
        circle1[i][2]=y;

        circle2[i][0]=x;
        circle2[i][1]=0.0;
        circle2[i][2]=y;

        circle3[i][0]=x;
        circle3[i][1]=y;
        circle3[i][2]=0.0;
        p1.x=x;
        p1.y=y;
        g_array_append_val(circle_ref, p1);
      }
    //Initialize rotated circles.
    circle_r1=g_array_sized_new(FALSE, TRUE, sizeof(struct point), 37);
    g_array_set_size(circle_r1, 37);
    circle_r2=g_array_sized_new(FALSE, TRUE, sizeof(struct point), 37);
    g_array_set_size(circle_r2, 37);
    circle_r3=g_array_sized_new(FALSE, TRUE, sizeof(struct point), 37);
    g_array_set_size(circle_r3, 37);

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);
    g_signal_connect(da, "draw", G_CALLBACK(draw_ellipses), NULL);

    GtkWidget *combo_rotate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_rotate, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 0, "1", "No Rotate");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 1, "2", "Animate Pitch, Roll and Yaw");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_rotate), 0);
    g_signal_connect(combo_rotate, "changed", G_CALLBACK(rotate_drawing), da);

    GtkWidget *combo_angle=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_angle, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_angle), 0, "1", "Euler Angles");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_angle), 1, "2", "Quaternions");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_angle), 0);
    g_signal_connect(combo_angle, "changed", G_CALLBACK(rotation_angle), da);

    GtkWidget *combo_line=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_line, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_line), 0, "1", "Draw Lines");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_line), 1, "2", "Draw Curves");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_line), 0);
    g_signal_connect(combo_line, "changed", G_CALLBACK(change_line), da);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *pitch_entry=gtk_entry_new();
    gtk_widget_set_hexpand(pitch_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(pitch_entry), "10");

    GtkWidget *pitch_button=gtk_button_new_with_label("Update Pitch");
    gtk_widget_set_hexpand(pitch_button, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *roll_entry=gtk_entry_new();
    gtk_widget_set_hexpand(roll_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(roll_entry), "10");

    GtkWidget *roll_button=gtk_button_new_with_label("Update Roll");
    gtk_widget_set_hexpand(roll_button, TRUE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);

    GtkWidget *yaw_entry=gtk_entry_new();
    gtk_widget_set_hexpand(yaw_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(yaw_entry), "10");

    GtkWidget *yaw_button=gtk_button_new_with_label("Update Yaw");
    gtk_widget_set_hexpand(yaw_button, TRUE);

    gpointer rotations[]={pitch_entry, roll_entry, yaw_entry, da};
    g_signal_connect(pitch_button, "clicked", G_CALLBACK(set_rotation_pitch), rotations);
    g_signal_connect(roll_button, "clicked", G_CALLBACK(set_rotation_roll), rotations);
    g_signal_connect(yaw_button, "clicked", G_CALLBACK(set_rotation_yaw), rotations);

    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_rotate, 0, 0, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_angle, 0, 1, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_line, 0, 2, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_entry, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_button, 0, 4, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 0, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_entry, 1, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_button, 0, 6, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 0, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_entry, 1, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_button, 0, 8, 2, 1);

    GtkWidget *scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll, TRUE);
    gtk_widget_set_vexpand(scroll, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll), grid1);
   
    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), scroll, TRUE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 300);
   
    gtk_container_add(GTK_CONTAINER(window), paned1);

    gtk_widget_show_all(window);

    gtk_main();

    g_array_free(circle_r1, TRUE);
    g_array_free(circle_r2, TRUE);
    g_array_free(circle_r3, TRUE);

    return 0;  
  }
static void rotate_drawing(GtkComboBox *combo, gpointer data)
  {
    rotate=gtk_combo_box_get_active(combo);
    //Reset rotations for animation.
    pitch=10.0;
    roll=10.0;
    yaw=10.0;

    if(rotate==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(data), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(data));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(data), (GtkTickCallback)animate_drawing, NULL, NULL);
          }
      }
    
  }
static void rotation_angle(GtkComboBox *combo, gpointer data)
  {
    //Set if drawing is with Euler angles or quaternions.
    angle=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void change_line(GtkComboBox *combo, gpointer data)
  {
    line_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_rotation_pitch(GtkWidget *button, gpointer *rotations)
  {
    g_print("Set Pitch\n");
    gdouble x=strtod(gtk_entry_get_text(GTK_ENTRY(rotations[0])), NULL);

    if(x>=0.0&&x<=359.0)
      {
        pitch=x;
      }
    else g_print("Pitch range 0<=x<=359\n");

    gtk_widget_queue_draw(GTK_WIDGET(rotations[3]));
  }
static void set_rotation_roll(GtkWidget *button, gpointer *rotations)
  {
    g_print("Set Roll\n");
    gdouble y=strtod(gtk_entry_get_text(GTK_ENTRY(rotations[1])), NULL);
   
    if(y>=0.0&&y<=359.0)
      {
        roll=y;
      }
    else g_print("Roll range 0.0<=y<=359.0\n");

    gtk_widget_queue_draw(GTK_WIDGET(rotations[3]));
  }
static void set_rotation_yaw(GtkWidget *button, gpointer *rotations)
  {
    g_print("Set Yaw\n");
    gdouble z=strtod(gtk_entry_get_text(GTK_ENTRY(rotations[2])), NULL);
   
    if(z>=0.0&&z<=359.0)
      {
        yaw=z;
      }
    else g_print("Yaw range 0.0<=z<=359.0\n");

    gtk_widget_queue_draw(GTK_WIDGET(rotations[3]));
  }
static void euler_rotation()
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → rotation matrix
    */
    gint i=0;
    gdouble pitch_r=(pitch*G_PI/180.0);
    gdouble roll_r=(roll*G_PI/180.0);
    gdouble yaw_r=(yaw*G_PI/180.0);
    gdouble t1=0;
    gdouble t2=0;
    gdouble t3=0;
    gdouble t4=0;
    gdouble t5=0;
    gdouble t6=0;
    struct point *p1=&g_array_index(circle_r1, struct point, 0);
    struct point *p2=&g_array_index(circle_r2, struct point, 0);
    struct point *p3=&g_array_index(circle_r3, struct point, 0);

    t1=cos(roll_r)*cos(yaw_r);
    t2=-cos(pitch_r)*sin(yaw_r)+sin(pitch_r)*sin(roll_r)*cos(yaw_r);
    t3=sin(pitch_r)*sin(yaw_r)+cos(pitch_r)*sin(roll_r)*cos(yaw_r);
    t4=cos(roll_r)*sin(yaw_r);
    t5=cos(pitch_r)*cos(yaw_r)+sin(pitch_r)*sin(roll_r)*cos(yaw_r);
    t6=-sin(pitch_r)*cos(yaw_r)+cos(pitch_r)*sin(roll_r)*sin(yaw_r);

    for(i=0;i<11;i++)
      {
        plane_rotated[i][0]=(plane_coords[i][0]*t1)+(plane_coords[i][1]*t2);
        plane_rotated[i][1]=(plane_coords[i][0]*t4)+(plane_coords[i][1]*t5);
      }

    for(i=0;i<37;i++)
      {
        (*p1).x=(circle1[i][1]*t2) + (circle1[i][2]*t3);
        (*p1).y=(circle1[i][1]*t5) + (circle1[i][2]*t6);        
        p1++;
      }

    for(i=0;i<37;i++)
      {
        (*p2).x=(circle2[i][0]*t1) + (circle2[i][2]*t3);
        (*p2).y=(circle2[i][0]*t4) + (circle2[i][2]*t6);
        p2++;
      }
    
    for(i=0;i<37;i++)
      {
        (*p3).x=(circle3[i][0]*t1) + (circle3[i][1]*t2);
        (*p3).y=(circle3[i][0]*t4) + (circle3[i][1]*t5);
        p3++;
      }
  }
static void quaternion_rotation()
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
    */
    gint i=0;
    gdouble pitch_r=(pitch*G_PI/180.0);
    gdouble roll_r=(roll*G_PI/180.0);
    gdouble yaw_r=(yaw*G_PI/180.0);
    gdouble qi=0;
    gdouble qj=0;
    gdouble qk=0;
    gdouble qr=0;
    gdouble r1=0;
    gdouble r2=0;
    gdouble r3=0;
    gdouble r4=0;
    gdouble r5=0;
    gdouble r6=0;
    struct point *p1=&g_array_index(circle_r1, struct point, 0);
    struct point *p2=&g_array_index(circle_r2, struct point, 0);
    struct point *p3=&g_array_index(circle_r3, struct point, 0);

    qi=sin(pitch_r/2.0)*cos(roll_r/2.0)*cos(yaw_r/2.0)-cos(pitch_r/2.0)*sin(roll_r/2.0)*sin(yaw_r/2.0);
    qj=cos(pitch_r/2.0)*sin(roll_r/2.0)*cos(yaw_r/2.0)+sin(pitch_r/2.0)*cos(roll_r/2.0)*sin(yaw_r/2.0);
    qk=cos(pitch_r/2.0)*cos(roll_r/2.0)*sin(yaw_r/2.0)-sin(pitch_r/2.0)*sin(roll_r/2.0)*cos(yaw_r/2.0);
    qr=cos(pitch_r/2.0)*cos(roll_r/2.0)*cos(yaw_r/2.0)+sin(pitch_r/2.0)*sin(roll_r/2.0)*sin(yaw_r/2.0);

    r1=1-2.0*qj*qj-2.0*qk*qk;
    r2=2.0*(qi*qj-qk*qr);
    r3=2.0*(qi*qk+qj*qr);
    r4=2.0*(qi*qj+qk*qr);
    r5=1.0-2.0*qi*qi-2.0*qk*qk;
    r6=2.0*(qj*qk-qi*qr);
    
    for(i=0;i<11;i++)
      {
        plane_rotated[i][0]=(plane_coords[i][0]*r1)+(plane_coords[i][1]*r2);
        plane_rotated[i][1]=(plane_coords[i][0]*r4)+(plane_coords[i][1]*r5);
      }

    for(i=0;i<37;i++)
      {
        (*p1).x=(circle1[i][1]*r2) + (circle1[i][2]*r3);
        (*p1).y=(circle1[i][1]*r5) + (circle1[i][2]*r6);        
        p1++;
      }

    for(i=0;i<37;i++)
      {
        (*p2).x=(circle2[i][0]*r1) + (circle2[i][2]*r3);
        (*p2).y=(circle2[i][0]*r4) + (circle2[i][2]*r6);
        p2++;
      }
    
    for(i=0;i<37;i++)
      {
        (*p3).x=(circle3[i][0]*r1) + (circle3[i][1]*r2);
        (*p3).y=(circle3[i][0]*r4) + (circle3[i][1]*r5);
        p3++;
      }

  }
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data)
  {
    //Apply angles of rotation for animation.    
    if(pitch>359.0) pitch=0.0;
    else pitch+=1.0;
    if(roll>359.0) roll=0.0;
    else roll+=1.0;
    if(yaw>359.0) yaw=0.0;
    else yaw+=1.0;

    gtk_widget_queue_draw(drawing);
    return G_SOURCE_CONTINUE;
  }
//The top drawing function.
static gboolean draw_ellipses(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    GTimer *timer=g_timer_new();
    gint i=0;
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;

    //Paint background.
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_paint(cr);

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 1.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Draw in the center
    cairo_translate(cr, width/2.0, height/2.0);
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND); 

    //Draw reference ellipse.
    cairo_set_line_width(cr, 15.0);
    cairo_set_source_rgba(cr, 0.5, 0.5, 1.0, 1.0);
    if(line_id==0)
      {
        cairo_move_to(cr, w1*circle3[0][0], h1*circle3[0][1]);
        for(i=1;i<36;i++)
          {
            cairo_line_to(cr, w1*circle3[i][0], h1*circle3[i][1]);
            cairo_stroke_preserve(cr);
          }
        cairo_close_path(cr);
        cairo_stroke(cr);
      }
    else
      {
        struct point d1;
        struct point d2;
        struct controls c1;
        GArray *control1=control_points_from_coords2(circle_ref);
        gint length=circle_ref->len-1;
        for(i=0;i<length;i++)
          {
            d1=g_array_index(circle_ref, struct point, i);
            d2=g_array_index(circle_ref, struct point, i+1);
            c1=g_array_index(control1, struct controls, i);
            cairo_move_to(cr, w1*d1.x, h1*d1.y);
            cairo_curve_to(cr, w1*c1.x1, h1*c1.y1, w1*c1.x2, h1*c1.y2, w1*d2.x, h1*d2.y);
            cairo_stroke(cr);
          }
        g_array_free(control1, TRUE);
      }

    cairo_set_line_width(cr, 5.0);

    //Calculate the current rotations for the plane and circles.
    if(angle==0) euler_rotation();
    else quaternion_rotation();

    if(line_id==0) draw_lines_with_rotation(cr, w1, h1); 
    else draw_smooth_with_rotation(cr, w1, h1);  
          
    g_print("Time %f Pitch %i Roll %i Yaw %i\n", g_timer_elapsed(timer, NULL), (gint)pitch%360, (gint)roll%360, (gint)yaw%360);
    g_timer_destroy(timer);
    return FALSE;
  }
static void draw_lines_with_rotation(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;
    cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
    struct point *p1=&g_array_index(circle_r1, struct point, 0);
    cairo_move_to(cr, w1*((*p1).x), h1*((*p1).y));
    for(i=1;i<37;i++)
      {
        p1++;
        cairo_line_to(cr, w1*((*p1).x), h1*((*p1).y));
        cairo_stroke(cr);
        cairo_move_to(cr, w1*((*p1).x), h1*((*p1).y));
      }

    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    p1=&g_array_index(circle_r2, struct point, 0);
    cairo_move_to(cr, w1*((*p1).x), h1*((*p1).y));
    for(i=1;i<37;i++)
      {
        p1++;
        cairo_line_to(cr, w1*((*p1).x), h1*((*p1).y));
        cairo_stroke(cr);
        cairo_move_to(cr, w1*((*p1).x), h1*((*p1).y));
      }

    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    p1=&g_array_index(circle_r3, struct point, 0);
    cairo_move_to(cr, w1*((*p1).x), h1*((*p1).y));
    for(i=1;i<37;i++)
      {
        p1++;
        cairo_line_to(cr, w1*((*p1).x), h1*((*p1).y));
        cairo_stroke(cr);
        cairo_move_to(cr, w1*((*p1).x), h1*((*p1).y));
      }
        
    cairo_set_source_rgba(cr, 0.8, 0.0, 0.8, 0.8);
    cairo_set_line_width(cr, 4.0);
    draw_plane(cr, w1, h1, TRUE);
  }
static void draw_smooth_with_rotation(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;
    struct point d1;
    struct point d2;
    struct controls c1; 
    gint length=0;

    cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
    GArray *control1=control_points_from_coords2(circle_r1);
    length=circle_r1->len-1;
    for(i=0;i<length;i++)
      {
        d1=g_array_index(circle_r1, struct point, i);
        d2=g_array_index(circle_r1, struct point, i+1);
        c1=g_array_index(control1, struct controls, i);
        cairo_move_to(cr, w1*d1.x, h1*d1.y);
        cairo_curve_to(cr, w1*c1.x1, h1*c1.y1, w1*c1.x2, h1*c1.y2, w1*d2.x, h1*d2.y);
        cairo_stroke(cr);
      }
    g_array_free(control1, TRUE);

    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    GArray *control2=control_points_from_coords2(circle_r2);
    length=circle_r2->len-1;
    for(i=0;i<length;i++)
      {
        d1=g_array_index(circle_r2, struct point, i);
        d2=g_array_index(circle_r2, struct point, i+1);
        c1=g_array_index(control2, struct controls, i);
        cairo_move_to(cr, w1*d1.x, h1*d1.y);
        cairo_curve_to(cr, w1*c1.x1, h1*c1.y1, w1*c1.x2, h1*c1.y2, w1*d2.x, h1*d2.y);
        cairo_stroke(cr);
      }
    g_array_free(control2, TRUE);
    
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    GArray *control3=control_points_from_coords2(circle_r3);
    length=circle_r3->len-1;
    for(i=0;i<length;i++)
      {
        d1=g_array_index(circle_r3, struct point, i);
        d2=g_array_index(circle_r3, struct point, i+1);
        c1=g_array_index(control3, struct controls, i);
        cairo_move_to(cr, w1*d1.x, h1*d1.y);
        cairo_curve_to(cr, w1*c1.x1, h1*c1.y1, w1*c1.x2, h1*c1.y2, w1*d2.x, h1*d2.y);
        cairo_stroke(cr);
      }
    g_array_free(control3, TRUE);
            
    cairo_set_source_rgba(cr, 0.8, 0.0, 0.8, 0.8);
    cairo_set_line_width(cr, 4.0);
    draw_plane(cr, w1, h1, TRUE);
  }
static void draw_plane(cairo_t *cr, gdouble w1, gdouble h1, gboolean with_euler)
  {
    gdouble (*plane)[11][2];

    if(with_euler) plane=&plane_rotated;
    else plane=&plane_coords;

    cairo_move_to(cr, (*plane)[0][0]*w1, (*plane)[0][1]*h1);
    cairo_line_to(cr, (*plane)[1][0]*w1, (*plane)[1][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, (*plane)[2][0]*w1, (*plane)[2][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, (*plane)[3][0]*w1, (*plane)[3][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, (*plane)[4][0]*w1, (*plane)[4][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, (*plane)[5][0]*w1, (*plane)[5][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, (*plane)[6][0]*w1, (*plane)[6][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, (*plane)[7][0]*w1, (*plane)[7][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, (*plane)[8][0]*w1, (*plane)[8][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, (*plane)[9][0]*w1, (*plane)[9][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, (*plane)[10][0]*w1, (*plane)[10][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_fill_preserve(cr);
    cairo_stroke(cr);
  }
/*
    This is some exellent work done by Ramsundar Shandilya. Note the following for the original work
    and the rational behind it.
    
    https://medium.com/@ramshandilya/draw-smooth-curves-through-a-set-of-points-in-ios-34f6d73c8f9

    https://github.com/Ramshandilya/Bezier

    The MIT License (MIT)

    Copyright (c) 2015 Ramsundar Shandilya

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
  
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

    This is a translation of the original Swift code to C. It makes the function easy to use with GTK+,
    cairo and glib.
*/
static GArray* control_points_from_coords2(const GArray *dataPoints)
  {  
    gint i=0;
    GArray *controlPoints=NULL;      
    //Number of Segments
    gint count=0;
    if(dataPoints!=NULL) count=dataPoints->len-1;
    gdouble *fCP=NULL;
    gdouble *sCP=NULL;

    if(count>0)
      {
        fCP=(gdouble*)g_malloc(2*count*sizeof(gdouble));
        sCP=(gdouble*)g_malloc(2*count*sizeof(gdouble));
      }
        
    //P0, P1, P2, P3 are the points for each segment, where P0 & P3 are the knots and P1, P2 are the control points.
    if(count<1||dataPoints==NULL)
      {
        //Return NULL.
        controlPoints=NULL;
        g_print("Can't get control points from coordinates. NULL returned.\n");
      }
    else if(count==1)
      {
        struct point P0=g_array_index(dataPoints, struct point, 0);
        struct point P3=g_array_index(dataPoints, struct point, 1);

        //Calculate First Control Point
        //3P1 = 2P0 + P3
        struct point P1;
        P1.x=(2.0*P0.x+P3.x)/3.0;
        P1.y=(2.0*P0.y+P3.y)/3.0;

        *(fCP)=P1.x;
        *(fCP+1)=P1.y;

        //Calculate second Control Point
        //P2 = 2P1 - P0
        struct point P2;
        P2.x=(2.0*P1.x-P0.x);
        P2.y=(2.0*P1.y-P0.x);

        *(sCP)=P2.x;
        *(sCP+1)=P2.y;      
      }
    else
      {
        gdouble *rhs=(gdouble*)g_malloc(2*count*sizeof(gdouble));
        gdouble *a=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *b=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *c=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble rhsValueX=0;
        gdouble rhsValueY=0;
        struct point P0;
        struct point P3;        
        gdouble m=0;
        gdouble b1=0;
        gdouble r2x=0;
        gdouble r2y=0;
        gdouble P1_x=0;
        gdouble P1_y=0;
   
        for(i=0;i<count;i++)
          {
            P0=g_array_index(dataPoints, struct point, i);
            P3=g_array_index(dataPoints, struct point, i+1);

            if(i==0)
              {
                *(a)=0.0;
                *(b)=2.0;
                *(c)=1.0;

                //rhs for first segment
                rhsValueX=P0.x+2.0*P3.x;
                rhsValueY=P0.y+2.0*P3.y;
              }
            else if(i==count-1)
              {
                *(a+i)=2.0;
                *(b+i)=7.0;
                *(c+i)=0.0;

                //rhs for last segment
                rhsValueX=8.0*P0.x+P3.x;
                rhsValueY=8.0*P0.y+P3.y;
              }
            else
              {
                *(a+i)=1.0;
                *(b+i)=4.0;
                *(c+i)=1.0;

                rhsValueX=4.0*P0.x+2.0*P3.x;
                rhsValueY=4.0*P0.y+2.0*P3.y;
              }
            *(rhs+i*2)=rhsValueX;
            *(rhs+i*2+1)=rhsValueY;
          }

        //Solve Ax=B. Use Tridiagonal matrix algorithm a.k.a Thomas Algorithm
        for(i=1;i<count;i++)
          {
            m=(*(a+i))/(*(b+i-1));

            b1=(*(b+i))-m*(*(c+i-1));
            *(b+i)=b1;

            r2x=(*(rhs+i*2))-m*(*(rhs+(i-1)*2));
            r2y=(*(rhs+i*2+1))-m*(*(rhs+(i-1)*2+1));

            *(rhs+i*2)=r2x;
            *(rhs+i*2+1)=r2y;
          }

        //Get First Control Points
        
        //Last control Point
        gdouble lastControlPointX=(*(rhs+2*count-2))/(*(b+count-1));
        gdouble lastControlPointY=(*(rhs+2*count-1))/(*(b+count-1));

        *(fCP+2*count-2)=lastControlPointX;
        *(fCP+2*count-1)=lastControlPointY;

        gdouble controlPointX=0;
        gdouble controlPointY=0;

        for(i=count-2;i>=0;--i)
          {
            controlPointX=(*(rhs+i*2)-(*(c+i))*(*(fCP+(i+1)*2)))/(*(b+i));
            controlPointY=(*(rhs+i*2+1)-(*(c+i))*(*(fCP+(i+1)*2+1)))/(*(b+i));

             *(fCP+i*2)=controlPointX;
             *(fCP+i*2+1)=controlPointY; 
          }

        //Compute second Control Points from first.
        for(i=0;i<count;i++)
          {
            if(i==count-1)
              {
                P3=g_array_index(dataPoints, struct point, i+1);
                P1_x=(*(fCP+i*2));
                P1_y=(*(fCP+i*2+1));

                controlPointX=(P3.x+P1_x)/2.0;
                controlPointY=(P3.y+P1_y)/2.0;

                *(sCP+count*2-2)=controlPointX;
                *(sCP+count*2-1)=controlPointY;
              }
            else
              {
                P3=g_array_index(dataPoints, struct point, i+1);                
                P1_x=(*(fCP+(i+1)*2));
                P1_y=(*(fCP+(i+1)*2+1));

                controlPointX=2.0*P3.x-P1_x;
                controlPointY=2.0*P3.y-P1_y;

                *(sCP+i*2)=controlPointX;
                *(sCP+i*2+1)=controlPointY;
              }

          }

        controlPoints=g_array_new(FALSE, FALSE, sizeof(struct controls));
        struct controls cp;
        for(i=0;i<count;i++)
          {
            cp.x1=(*(fCP+i*2));
            cp.y1=(*(fCP+i*2+1));
            cp.x2=(*(sCP+i*2));
            cp.y2=(*(sCP+i*2+1));
            g_array_append_val(controlPoints, cp);
          }

        g_free(rhs);
        g_free(a);
        g_free(b);
        g_free(c);
     }

    if(fCP!=NULL) g_free(fCP);
    if(sCP!=NULL) g_free(sCP);

    return controlPoints;
  }


