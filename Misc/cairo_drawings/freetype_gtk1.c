/*
    Start looking at the freetype library. Rotate a glyph in 3d. It looks like GTK doesn't
like cairo_surface_map_to_image() and doesn't return just a drawing area surface pointer.
A new surface with cairo_image_surface_create() works better.
    Test both freetype and cairo for 3d rotation of text.

    gcc -Wall freetype_gtk1.c -o freetype_gtk1 -I/usr/include/freetype2 -L/usr/local/lib `pkg-config --cflags --libs gtk+-3.0` -lfreetype -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<stdint.h>
#include<ft2build.h>
#include FT_FREETYPE_H

//A reference triangle t1 and a rotated triangle t2.
gdouble t1[3][3]={  
      {0.0, -1.0, 0.0},  
      {-1.0, 1.0, 0.0},  
      {1.0, 1.0, 0.0}, 
      };
gdouble t2[3][3]={  
      {0.0, -1.0, 0.0},  
      {-1.0, 1.0, 0.0},  
      {1.0, 1.0, 0.0}, 
      };

char *text="Freetype";
gdouble pitch=0;
gdouble roll=0;
gdouble yaw=0;
gint drawing_id=0;

static void change_drawing(GtkComboBox *combo, gpointer data);
static void set_rotation_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, GtkWidget *da);
static void set_rotation_roll(GtkRange *range, GtkScrollType scroll, gdouble value, GtkWidget *da);
static void set_rotation_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, GtkWidget *da);
static gboolean draw_main(GtkWidget *da, cairo_t *cr, FT_Face face);
static void draw_glyph1(GtkWidget *da, cairo_t *cr, FT_Face face);
static void draw_glyph2(GtkWidget *da, cairo_t *cr, FT_Face face);
static void draw_glyph3(GtkWidget *da, cairo_t *cr, FT_Face face);
static void draw_glyph4(GtkWidget *da, cairo_t *cr);
static void quaternion_rotation(gdouble yaw_r, gdouble roll_r, gdouble pitch_r, gdouble qrs[9]);
static void rotate_triangle(gdouble qrs[9]);

int main(int argc, char **argv)
 {
   FT_Library  library; 
   FT_Error error;
   FT_Face face;
   char *font="/usr/share/fonts/truetype/freefont/FreeSans.ttf";

   gtk_init(&argc, &argv);

   error=FT_Init_FreeType(&library);
   if(error) printf("Init_FreeType Error\n");

   error=FT_New_Face(library, font, 0, &face );
   if(error) printf("FT_New_Face Error.\n");

   error=FT_Set_Char_Size(face, 200*64, 200*64, 72, 72);
   if(error) printf("FT_Set_Char_Size Error.\n"); 

   GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
   gtk_window_set_title(GTK_WINDOW(window), "Freetype");
   gtk_window_set_default_size(GTK_WINDOW(window), 800, 500);
   gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
   g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

   GtkWidget *da=gtk_drawing_area_new();
   gtk_widget_set_hexpand(da, TRUE);
   gtk_widget_set_vexpand(da, TRUE);
   g_signal_connect(da, "draw", G_CALLBACK(draw_main), face);

   GtkWidget *combo_drawing=gtk_combo_box_text_new();
   gtk_widget_set_hexpand(combo_drawing, TRUE);
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "0", "Glyph1");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "1", "Problem Glyph2");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 2, "2", "Good Glyph3");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 3, "3", "Cairo Glyph");
   gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
   g_signal_connect(combo_drawing, "changed", G_CALLBACK(change_drawing), da);  

   GtkWidget *pitch_label=gtk_label_new("Pitch");
   gtk_widget_set_hexpand(pitch_label, TRUE);

   GtkWidget *roll_label=gtk_label_new("Roll");
   gtk_widget_set_hexpand(roll_label, TRUE);

   GtkWidget *yaw_label=gtk_label_new("Yaw");
   gtk_widget_set_hexpand(yaw_label, TRUE);

   GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
   gtk_widget_set_vexpand(pitch_slider, TRUE);

   GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
   gtk_widget_set_vexpand(roll_slider, TRUE);

   GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
   gtk_widget_set_vexpand(yaw_slider, TRUE);
    
   g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_rotation_pitch), da);
   g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_rotation_roll), da);
   g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_rotation_yaw), da);

   GtkWidget *grid1=gtk_grid_new();
   gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
   gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
   gtk_grid_attach(GTK_GRID(grid1), combo_drawing, 0, 0, 3, 1);
   gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 1, 1, 1);
   gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 1, 1, 1);
   gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 1, 1, 1);
   gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 2, 1, 1);
   gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 2, 1, 1);
   gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 2, 1, 1);
   GtkWidget *scroll=gtk_scrolled_window_new(NULL, NULL);
   gtk_widget_set_hexpand(scroll, TRUE);
   gtk_widget_set_vexpand(scroll, TRUE);
   gtk_container_add(GTK_CONTAINER(scroll), grid1);
   
   GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
   gtk_paned_pack1(GTK_PANED(paned1), scroll, TRUE, TRUE);
   gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
   gtk_paned_set_position(GTK_PANED(paned1), 300);
   
   gtk_container_add(GTK_CONTAINER(window), paned1);

   gtk_widget_show_all(window);

   gtk_main();

   FT_Done_Face(face);
   FT_Done_FreeType(library);

   return 0;  
 }
static void change_drawing(GtkComboBox *combo, gpointer data)
  {
    drawing_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_rotation_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, GtkWidget *da)
  {
    if(value>360.0) pitch=360.0;
    else if(value<0.0) pitch=0.0;
    else pitch=value; 

    gtk_widget_queue_draw(da);
  }
static void set_rotation_roll(GtkRange *range, GtkScrollType scroll, gdouble value, GtkWidget *da)
  {
    if(value>360.0) roll=360.0;
    else if(value<0.0) roll=0.0;
    else roll=value; 

    gtk_widget_queue_draw(da);
  }
static void set_rotation_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, GtkWidget *da)
  {
    if(value>360.0) yaw=360.0;
    else if(value<0.0) yaw=0.0;
    else yaw=value; 

    gtk_widget_queue_draw(da);
  }
static gboolean draw_main(GtkWidget *da, cairo_t *cr, FT_Face face)
 {
   GTimer *timer=g_timer_new();
   if(drawing_id==0) draw_glyph1(da, cr, face);
   else if(drawing_id==1) draw_glyph2(da, cr, face);
   else if(drawing_id==2) draw_glyph3(da, cr, face);
   else draw_glyph4(da, cr);
   g_print("Timer %f\n", g_timer_elapsed(timer, NULL));
   g_timer_destroy(timer);
   return FALSE;
 }
static void draw_glyph1(GtkWidget *da, cairo_t *cr, FT_Face face)
 {
   /*
       Rotate the individual pixels after rotating them in this drawing.
   */
   gint i=0;
   gint j=0;
   gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
   gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
   gdouble w1=1.0*width/10.0;
   gdouble h1=1.0*height/10.0;

   cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
   cairo_paint(cr);

   //Cartesian coordinates for drawing.
   cairo_set_line_width(cr, 1.0);
   cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
   cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
   cairo_stroke(cr);
   cairo_move_to(cr, 1.0*w1, 5.0*h1);
   cairo_line_to(cr, 9.0*w1, 5.0*h1);
   cairo_stroke(cr);
   cairo_move_to(cr, 5.0*w1, 1.0*h1);
   cairo_line_to(cr, 5.0*w1, 9.0*h1);
   cairo_stroke(cr);

   gdouble qrs[9];
   gdouble pitch_r=(pitch*G_PI/180.0);
   gdouble roll_r=(roll*G_PI/180.0);
   gdouble yaw_r=(yaw*G_PI/180.0);
   quaternion_rotation(yaw_r, roll_r, pitch_r, qrs); 

   //Render a single glyph.
   FT_Error error; 
   error=FT_Load_Char(face, text[2], FT_LOAD_RENDER);
   if(error) g_print("FT_Load_Char error.\n");
   gint b_rows=face->glyph->bitmap.rows;
   gint b_width=face->glyph->bitmap.width;

   //Make rows and columns for the surface even numbers to make finding the center easier.
   gint s_width=0;
   gint s_height=0;
   if(b_width%2==0) s_width=b_width+120;
   else s_width=b_width+101;
   if(b_rows%2==0) s_height=b_rows+120;
   else s_height=b_rows+101;

   cairo_surface_t *surface2=cairo_image_surface_create(CAIRO_FORMAT_ARGB32, s_width, s_height);
   cairo_status_t status=cairo_surface_status(surface2);
   if(status) g_print("Get Surface: %s\n", cairo_status_to_string(status));

   s_width=cairo_image_surface_get_width(surface2);
   s_height=cairo_image_surface_get_height(surface2);

   //Draw a rectangle on the glyph surface.
   cairo_t *cr2=cairo_create(surface2);
   cairo_set_line_width(cr2, 4);
   cairo_set_source_rgba(cr2, 0.0, 1.0, 0.0, 1.0);
   cairo_rectangle(cr2, 0.0, 0.0, s_width, s_height);
   cairo_stroke(cr2);

   //Reference triangle.
   cairo_set_source_rgba(cr2, 0.0, 1.0, 1.0, 1.0);
   rotate_triangle(qrs);
   cairo_translate(cr2, s_width/2, s_height/2);
   cairo_move_to(cr2, 100*t2[0][0], 100*t2[0][1]);
   cairo_line_to(cr2, 100*t2[1][0], 100*t2[1][1]);
   cairo_stroke_preserve(cr2);
   cairo_line_to(cr2, 100*t2[2][0], 100*t2[2][1]);
   cairo_stroke_preserve(cr2);
   cairo_close_path(cr2);
   cairo_stroke(cr2);
   cairo_translate(cr2, -s_width/2, -s_height/2);
  
   //Surface is in 4 byte pixels. Cast to a 32 bit int to line up freetype and cairo pointers.
   uint32_t *s=(uint32_t*)cairo_image_surface_get_data(surface2);
   uint32_t *p1=s;
   //FT_PIXEL_MODE_GRAY=2 is default or 8-bit pixel for glyph.
   unsigned char *p2=face->glyph->bitmap.buffer;
   
   p1=s+60*s_width+60;
   gint offset=s_width-b_width;
   gdouble x1=0.0;
   gdouble y1=0.0;
   gint x2=0;
   gint y2=0;
   //Get i and j "rectangle" up and back to center at origin. Then rotate these values and shift back.
   gint x_shift=b_width/2;
   gint y_shift=b_rows/2;
   for(i=0;i<b_rows;i++)
     {
       for(j=0;j<b_width;j++)
         {  
           y1=(((gdouble)(j-x_shift)*qrs[0]+(gdouble)x_shift)+((gdouble)(i-y_shift)*qrs[1]));
           x1=(((gdouble)(j-x_shift)*qrs[3])+((gdouble)(i-y_shift)*qrs[4]+(gdouble)y_shift));
           y2=(gint)round(y1);
           x2=(gint)round(x1); 
           *(p1+((x2*b_width)+(x2*offset)+y2))=0xff000000+(uint32_t)(*p2);
           p2++; 
         }                      
     }
   
   cairo_surface_mark_dirty(surface2);
   cairo_translate(cr, width/2.0-s_width/2.0, height/2.0-s_height/2.0);
   cairo_set_source_surface(cr, surface2, 0.0, 0.0);
   cairo_paint(cr);
   //cairo_surface_write_to_png(surface2, "image.png");
   cairo_destroy(cr2);
   cairo_surface_destroy(surface2);
 }
static void draw_glyph2(GtkWidget *da, cairo_t *cr, FT_Face face)
 {
   /*
       Problem with getting a drawing area pointer in this drawing.
   */
   gint i=0;
   gint j=0;
   gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
   gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
   gdouble w1=1.0*width/10.0;
   gdouble h1=1.0*height/10.0;

   cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
   cairo_paint(cr);

   //Cartesian coordinates for drawing.
   cairo_set_line_width(cr, 1.0);
   cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
   cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
   cairo_stroke(cr);
   cairo_move_to(cr, 1.0*w1, 5.0*h1);
   cairo_line_to(cr, 9.0*w1, 5.0*h1);
   cairo_stroke(cr);
   cairo_move_to(cr, 5.0*w1, 1.0*h1);
   cairo_line_to(cr, 5.0*w1, 9.0*h1);
   cairo_stroke(cr); 

   //Render a single glyph.
   FT_Error error; 
   error=FT_Load_Char(face, text[2], FT_LOAD_RENDER);
   if(error) g_print("FT_Load_Char error.\n");
   gint b_rows=face->glyph->bitmap.rows;
   gint b_width=face->glyph->bitmap.width;

   /*
      Tried sizing a surface but couldn't get it to work with GTK. GTK seems not to
      work well with cairo_surface_map_to_image().
      cairo_rectangle_int_t rect={.x=200, .y=200, .width=b_width, .height=b_rows};
      Also tried gdk_window_create_similar_image_surface() which fails.
   */
   cairo_surface_t *surface2=cairo_surface_map_to_image(cairo_get_target(cr), NULL);
   cairo_status_t status=cairo_surface_status(surface2);
   if(status) g_print("Get Surface: %s\n", cairo_status_to_string(status));

   cairo_surface_flush(surface2);
   gint s_width=cairo_image_surface_get_width(surface2);
   gint s_height=cairo_image_surface_get_height(surface2);
  
   //Surface is in 4 byte pixels. Cast to a 32 bit int to line up freetype and cairo pointers.
   uint32_t *s=(uint32_t*)cairo_image_surface_get_data(surface2);
   uint32_t *p1=s;
   //FT_PIXEL_MODE_GRAY=2 is default or 8-bit pixel for glyph.
   unsigned char *p2=face->glyph->bitmap.buffer;

   gdouble qrs[9];
   gdouble pitch_r=(pitch*G_PI/180.0);
   gdouble roll_r=(roll*G_PI/180.0);
   gdouble yaw_r=(yaw*G_PI/180.0);
   quaternion_rotation(yaw_r, roll_r, pitch_r, qrs);

   //Adjust for odd even rows. Move to center of drawing.
   /* 
   Fourth quadrant.
   if(s_height%2==0) p1=s+(s_width*s_height)/2+s_width/2;
   else p1=s+(s_width*s_height)/2;
   */
   if(s_height%2==0) p1=s+(s_width*s_height)/2+s_width/2-b_width/2-(b_rows/2*s_width);
   else p1=s+(s_width*s_height)/2-b_width/2-(b_rows/2*s_width);
   gint offset=s_width-b_width;
   gdouble x1=0.0;
   gdouble y1=0.0;
   gint x2=0;
   gint y2=0;
   //Get i and j "rectangle" up and back to center at origin. Then rotate these values and shift back.
   gint x_shift=b_width/2;
   gint y_shift=b_rows/2;
   for(i=0;i<b_rows;i++)
     {
       for(j=0;j<b_width;j++)
         {  
           y1=(((gdouble)(j-x_shift)*qrs[0]+(gdouble)x_shift)+((gdouble)(i-y_shift)*qrs[1]));
           x1=(((gdouble)(j-x_shift)*qrs[3])+((gdouble)(i-y_shift)*qrs[4]+(gdouble)y_shift));
           y2=(gint)round(y1);
           x2=(gint)round(x1); 
           *(p1+((x2*b_width)+(x2*offset)+y2))=0xff000000+(uint32_t)(*p2);
           p2++; 
         }                      
     }
   
   cairo_surface_mark_dirty(surface2);
   //cairo_surface_write_to_png(surface2, "image.png");
   cairo_surface_unmap_image(cairo_get_target(cr), surface2);
 }
static void draw_glyph3(GtkWidget *da, cairo_t *cr, FT_Face face)
 {
   /*
       A good looking glyph that matches with Freetype's and Cairo's rotation. Careful of the 
   rotations in the freetype and cairo matrices! It took a while to figure out how to match
   them. 
   */
   gint i=0;
   gint j=0;
   gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
   gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
   gdouble w1=1.0*width/10.0;
   gdouble h1=1.0*height/10.0;

   cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
   cairo_paint(cr);

   //Cartesian coordinates for drawing.
   cairo_set_line_width(cr, 1.0);
   cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
   cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
   cairo_stroke(cr);
   cairo_move_to(cr, 1.0*w1, 5.0*h1);
   cairo_line_to(cr, 9.0*w1, 5.0*h1);
   cairo_stroke(cr);
   cairo_move_to(cr, 5.0*w1, 1.0*h1);
   cairo_line_to(cr, 5.0*w1, 9.0*h1);
   cairo_stroke(cr); 

   gdouble qrs[9];
   gdouble pitch_r=(pitch*G_PI/180.0);
   gdouble roll_r=(roll*G_PI/180.0);
   gdouble yaw_r=(yaw*G_PI/180.0);
   quaternion_rotation(yaw_r, roll_r, pitch_r, qrs);

   //Render a single glyph.
   FT_Error error; 
   FT_Matrix matrix;
   FT_Vector pen; 
   pen.x=0.0;
   pen.y=0.0; 
   matrix.xx=(FT_Fixed)(qrs[0]*0x10000L);
   matrix.xy=-(FT_Fixed)(qrs[1]*0x10000L);
   matrix.yx=-(FT_Fixed)(qrs[3]*0x10000L);
   matrix.yy=(FT_Fixed)(qrs[4]*0x10000L);
   FT_Set_Transform(face, &matrix, &pen);
   error=FT_Load_Char(face, text[2], FT_LOAD_RENDER);
   if(error) g_print("FT_Load_Char error.\n");
   gint b_rows=face->glyph->bitmap.rows;
   gint b_width=face->glyph->bitmap.width;

   //Get some bounding boxes.
   gint max1=0;
   gint max2=0;
   if(b_rows>b_width) max1=sqrt(b_rows*b_rows+b_rows*b_rows);
   else max1=sqrt(b_width*b_width+b_width*b_width);
   max2=sqrt(max1*max1+max1*max1);

   //Make rows and columns for the surface even numbers to make finding the center easier.
   gint s_width=0;
   gint s_height=0;
   if(max2%2==0)
     {
       s_width=max2;
       s_height=max2;
     }
   else
     {
       s_width=max2+1;
       s_height=max2+1;
     }
   
   cairo_surface_t *surface2=cairo_image_surface_create(CAIRO_FORMAT_ARGB32, s_width, s_height);
   cairo_status_t status=cairo_surface_status(surface2);
   if(status) g_print("Get Surface: %s\n", cairo_status_to_string(status));

   s_width=cairo_image_surface_get_width(surface2);
   s_height=cairo_image_surface_get_height(surface2);

   //Draw a rectangle around the glyph surface with the same rotation.
   cairo_matrix_t matrix1;
   cairo_t *cr2=cairo_create(surface2);
   cairo_set_line_width(cr2, 4);
   cairo_set_source_rgba(cr2, 0.0, 1.0, 0.0, 1.0);
   cairo_paint(cr2);

   //Reference triangle.
   cairo_set_source_rgba(cr2, 0.0, 1.0, 1.0, 1.0);
   rotate_triangle(qrs);
   cairo_translate(cr2, s_width/2, s_height/2);
   cairo_move_to(cr2, 100*t2[0][0], 100*t2[0][1]);
   cairo_line_to(cr2, 100*t2[1][0], 100*t2[1][1]);
   cairo_stroke_preserve(cr2);
   cairo_line_to(cr2, 100*t2[2][0], 100*t2[2][1]);
   cairo_stroke_preserve(cr2);
   cairo_close_path(cr2);
   cairo_stroke(cr2);

   //Rotate rectangle with cairo matrix.
   cairo_set_source_rgba(cr2, 1.0, 0.0, 1.0, 1.0);
   cairo_matrix_init(&matrix1, qrs[0], qrs[3], qrs[1], qrs[4], s_width/2.0, s_height/2.0);
   cairo_set_matrix(cr2, &matrix1);
   cairo_rectangle(cr2, -s_width/2.0+20, -s_height/2.0+20, s_width-40.0, s_height-40.0);
   cairo_stroke(cr2);   
  
   //Surface is in 4 byte pixels. Cast to a 32 bit int to line up freetype and cairo pointers.
   uint32_t *s=(uint32_t*)cairo_image_surface_get_data(surface2);
   uint32_t *p1=s;
   //FT_PIXEL_MODE_GRAY=2 is default or 8-bit pixel for glyph.
   unsigned char *p2=face->glyph->bitmap.buffer;
   
   gint offset=s_width-b_width;
   p1=s+s_width*((max2-b_rows)/2)+((max2-b_width)/2);
   for(i=0;i<b_rows;i++)
     {
       for(j=0;j<b_width;j++)
         {  
           if(*p2==0) *p1=0xff00ff00;
           else *p1=0xff000000+(uint32_t)(*p2);
           p1++,p2++; 
         }
       p1+=offset;                      
     }
   
   cairo_surface_mark_dirty(surface2);
   cairo_translate(cr, width/2.0-s_width/2, height/2.0-s_height/2);
   cairo_set_source_surface(cr, surface2, 0.0, 0.0);
   cairo_paint(cr);
   //cairo_surface_write_to_png(surface2, "image.png");
   cairo_destroy(cr2);
   cairo_surface_destroy(surface2);
 }
static void draw_glyph4(GtkWidget *da, cairo_t *cr)
 {
   //The cairo version.
   gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
   gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
   gdouble w1=1.0*width/10.0;
   gdouble h1=1.0*height/10.0;

   cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
   cairo_paint(cr);

   //Cartesian coordinates for drawing.
   cairo_set_line_width(cr, 1.0);
   cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
   cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
   cairo_stroke(cr);
   cairo_move_to(cr, 1.0*w1, 5.0*h1);
   cairo_line_to(cr, 9.0*w1, 5.0*h1);
   cairo_stroke(cr);
   cairo_move_to(cr, 5.0*w1, 1.0*h1);
   cairo_line_to(cr, 5.0*w1, 9.0*h1);
   cairo_stroke(cr); 

   gdouble qrs[9];
   gdouble pitch_r=(pitch*G_PI/180.0);
   gdouble roll_r=(roll*G_PI/180.0);
   gdouble yaw_r=(yaw*G_PI/180.0);
   quaternion_rotation(yaw_r, roll_r, pitch_r, qrs);

   //Reference triangle.
   cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
   cairo_set_line_width(cr, 6.0);
   rotate_triangle(qrs);
   cairo_translate(cr, width/2, height/2);
   cairo_move_to(cr, 100*t2[0][0], 100*t2[0][1]);
   cairo_line_to(cr, 100*t2[1][0], 100*t2[1][1]);
   cairo_stroke_preserve(cr);
   cairo_line_to(cr, 100*t2[2][0], 100*t2[2][1]);
   cairo_stroke_preserve(cr);
   cairo_close_path(cr);
   cairo_stroke(cr);

   //Rotate rectangle with cairo matrix.
   cairo_matrix_t matrix1;
   cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
   cairo_matrix_init(&matrix1, qrs[0], qrs[3], qrs[1], qrs[4], 0.0, 0.0);
   cairo_transform(cr, &matrix1);
   cairo_rectangle(cr, -120.0, -120.0, 240.0, 240.0);
   cairo_stroke(cr);   
  
   cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
   cairo_set_font_size(cr, 120.0);
   cairo_text_extents_t extents;
   cairo_text_extents(cr, "e", &extents);
   cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
   cairo_show_text(cr, "e");   
 }

static void quaternion_rotation(gdouble yaw_r, gdouble roll_r, gdouble pitch_r, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll_r=roll_r/2.0;
    pitch_r=pitch_r/2.0;
    yaw_r=yaw_r/2.0;
    gdouble qi=sin(pitch_r)*cos(roll_r)*cos(yaw_r)-cos(pitch_r)*sin(roll_r)*sin(yaw_r);
    gdouble qj=cos(pitch_r)*sin(roll_r)*cos(yaw_r)+sin(pitch_r)*cos(roll_r)*sin(yaw_r);
    gdouble qk=cos(pitch_r)*cos(roll_r)*sin(yaw_r)-sin(pitch_r)*sin(roll_r)*cos(yaw_r);
    gdouble qr=cos(pitch_r)*cos(roll_r)*cos(yaw_r)+sin(pitch_r)*sin(roll_r)*sin(yaw_r);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
static void rotate_triangle(gdouble qrs[9])
  {
    gint i=0;
    for(i=0;i<3;i++)
      {
        t2[i][0]=(t1[i][0]*qrs[0])+(t1[i][1]*qrs[1]);
        t2[i][1]=(t1[i][0]*qrs[3])+(t1[i][1]*qrs[4]);
      }
  }

