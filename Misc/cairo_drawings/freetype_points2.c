/*
    Draw 3d truetype fonts with font points in cairo. Also draw chars with a blur and a moveable shadow
projection plane. Output drawing to .svg, .pdf and .ps.

    https://www.freetype.org/freetype2/docs/glyphs/glyphs-6.html

    Clean up some while loops in freetype_points2.c and try some blur. Sort the middle quads before
    drawing. This works OK with a mesh with transparency. The out of order draws become more noticeable
    with a mesh with solid colors. Solid color draws could use a lower level depth test.

    For the translating the quadratic bezier points to cubic bezier points for cairo to use.
    https://lists.freedesktop.org/archives/cairo/2009-October/018351.html
    To turn quad(p0, p1, p2) into cubic(q0, q1, q2, q3):
	q0 = p0
	q1 = (p0+2*p1)/3
	q2 = (p2+2*p1)/3
	q3 = p2

    gcc -Wall freetype_points2.c -o freetype_points2 -I/usr/include/freetype2 -L/usr/local/lib `pkg-config --cflags --libs gtk+-3.0` -lfreetype -lm

    Tested on Ubuntu16.04 and GTK3.18 32bit

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<ft2build.h>
#include FT_FREETYPE_H
#include FT_OUTLINE_H
#include FT_BBOX_H
#include<cairo-svg.h>
#include<cairo-pdf.h>
#include<cairo-ps.h>

/*
  A drawing path operation. The R and L on move_to is clockwise and counter clockwise 
  winding of the path.
*/
enum{ 
  MOVE_TO_R,
  MOVE_TO_L,
  LINE_TO,
  CURVE_TO,
  LINE_TO_STROKE,
  CURVE_TO_STROKE
};

/*
   Replace segment in the 3d drawings with segment2. Some wasted space but it cleans up the
   while loops and makes it easier to sort quads for drawing a blur.
*/
struct segment2{
  gint move_id;
  gdouble x1;
  gdouble y1;
  gdouble z1;
  gdouble x2;
  gdouble y2;
  gdouble z2;
  gdouble x3;
  gdouble y3;
  gdouble z3;
}; 

//A quad that can be sorted by z-value.
struct quad{
  struct segment2 a;
  struct segment2 b;
  struct segment2 c;
  struct segment2 d;
};

//Some letters to test.
static char *text="AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
static gint text_index=0;
//Scale the font from original coordinates. scale>0;
static gdouble scale=0.25;
//The test drawing combo.
static gint drawing_id=0;
//The stress combo.
static gint stress_id=0;
//The slider values
static gdouble slider_pitch=0.0;
static gdouble slider_roll=0.0;
static gdouble slider_yaw=0.0;
static gdouble slider_extrude=20.0;
static gdouble slider_scale=1.0;
static gdouble slider_perspective=0.0;
static gboolean shadow_lines=TRUE;
static gdouble slider_pitch2=0.0;
static gdouble slider_roll2=0.0;
static gdouble slider_yaw2=0.0;
static gdouble slider_revolve=0.0;
static gdouble slider_distance=1.0;
static gdouble font_rgba[4]={0.0, 1.0, 0.0, 1.0};
//A reference vector to keep track of the top of the drawing.
static gdouble v1[3]={0.0, 0.0, 1.0};
static gdouble v1_r[3]={0.0, 0.0, 1.0};
//A saved array to speed up animation.
static GArray *path_s=NULL;
//If blur is set.
static gboolean blur=FALSE;
static gboolean timer_set=FALSE;
//A plane to project the char onto and a transformed plane for revolving the plane.
gdouble plane_pts[9]={0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.5, 1.0, -1.0};
gdouble plane_pts_t[9];
//Arrays for several char point arrays. 
GPtrArray *text3d=NULL;
GPtrArray *text2d=NULL;
//Array for char boxes.
GArray *box_size=NULL;

static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data);
//Set values in combos and sliders.
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_extrude(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_letter(GtkComboBox *combo, gpointer data);
static void set_drawing(GtkComboBox *combo, gpointer data);
static void set_stress(GtkComboBox *combo, gpointer data);
static void set_blur(GtkToggleButton *button, gpointer data);
static void set_timer(GtkToggleButton *button, gpointer data);
static void set_shadow_lines(GtkToggleButton *button, gpointer data);
static void set_pitch2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_revolve(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_distance(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void red_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void green_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void blue_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void alpha_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void set_font_color(GtkWidget *button, gpointer data);
static void draw_svg(GtkWidget *widget, gpointer *da_font);
static void draw_pdf(GtkWidget *widget, gpointer *da_font);
static void draw_ps(GtkWidget *widget, gpointer *da_font);
static void draw_surface(cairo_t *cr, gdouble width, gdouble height, gpointer *da_face);

//Starting draw function.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, FT_Face face);
//Draw the char with the bezier points and virtual points.
static void draw_font1(GtkWidget *da, cairo_t *cr, FT_Face face);
//Draw the char 3d.
static void draw_font2(GtkWidget *da, cairo_t *cr, FT_Face face);
//Draw the 2d char with a shadow.
static void draw_font3(GtkWidget *da, cairo_t *cr, FT_Face face);
//Draw a text string.
static void draw_font4(GtkWidget *da, cairo_t *cr, FT_Face face);

//The char drawing functions
static void draw_path_contours_line(cairo_t *cr, GArray *path);
static void draw_path_contours_connect(cairo_t *cr, GArray *path1, GArray *path2);
static void draw_path_side_fill(cairo_t *cr, GArray *path1, GArray *path2, gdouble rgba[4]);
static void draw_path_side_fill_blur(cairo_t *cr, GArray *path1, GArray *path2, gdouble rgba[4]);
static void draw_path_contours_fill(cairo_t *cr, GArray *path);

/*
    Get arrays of char points to use with the drawing functions. The 2d is one copy of the
    path. This is used to draw the char with a movable shadow plane. The 3d function returns
    2 copies of the path that are connected for the 3d char. The end result for 2d is similar
    to 3d since the shadow path is connected to the single path from the 2d function.
*/
static GPtrArray* get_font_paths_2d(FT_Face face, GArray *index, GArray *box_size);
static GPtrArray* get_font_paths_3d(FT_Face face, GArray *index, GArray *box_size);
static GArray* get_font_path(FT_Face face, gint index, gdouble *box_w, gdouble *box_h);
static GArray* get_shadow_path(GArray *path, const gdouble light[3], const gdouble plane_distance, const gdouble qrs_plane[9]);
static FT_Outline get_font_outline(FT_Face face, gint index, gdouble *box_w, gdouble *box_h);

//Transform functions.
static void quaternion_rotation(const gdouble yaw, const gdouble roll, const gdouble pitch, gdouble qrs[9]);
static void transform_letter(GArray *path, const gdouble qrs[9]);
static void matrix_multiply(const gdouble in1[9], const gdouble in2[9], gdouble out[9]);
static void reset_plane_pts(const gdouble qrs_plane[9]);
static void rotate_plane(const gdouble qrs_plane[9]);

//Sort the quads from -z to +z for blur.
int compare_quads(const void *a, const void *b);

int main(int argc, char **argv)
  {
    gint i=0;
    FT_Library library; 
    FT_Error error;
    FT_Face face;
    char *font="/usr/share/fonts/truetype/freefont/FreeSans.ttf";
    //char *font="/usr/share/fonts/truetype/ubuntu-font-family/UbuntuMono-BI.ttf";
    //char *font="/usr/share/fonts/truetype/liberation/LiberationMono-Italic.ttf";

    gtk_init(&argc, &argv);

    error=FT_Init_FreeType(&library);
    if(error) printf("Init_FreeType Error\n");

    error=FT_New_Face(library, font, 0, &face);
    if(error) printf("FT_New_Face Error.\n");

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Freetype");
    gtk_window_set_default_size(GTK_WINDOW(window), 875, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_widget_set_app_paintable(window, TRUE);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);
    g_signal_connect(da, "draw", G_CALLBACK(draw_main), face);

    GtkWidget *combo_letters=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_letters, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 0, "1", "A");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 1, "2", "a");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 2, "3", "B");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 3, "4", "b");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 4, "5", "C");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 5, "6", "c");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 6, "7", "D");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 7, "8", "d");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 8, "9", "E");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 9, "10", "e");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 10, "11", "F");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 11, "12", "f");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 12, "13", "G");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 13, "14", "g");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 14, "15", "H");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 15, "16", "h");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 16, "17", "I");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 17, "18", "i");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 18, "19", "J");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 19, "20", "j");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 20, "21", "K");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 21, "22", "k");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 22, "23", "L");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 23, "24", "l");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 24, "25", "M");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 25, "26", "m");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 26, "27", "N");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 27, "28", "n");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 28, "29", "O");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 29, "30", "o");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 30, "31", "P");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 31, "32", "p");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 32, "33", "Q");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 33, "34", "q");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 34, "35", "R");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 35, "36", "r");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 36, "37", "S");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 37, "38", "s");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 38, "39", "T");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 39, "40", "t");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 40, "41", "U");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 41, "42", "u");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 42, "43", "V");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 43, "44", "v");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 44, "45", "W");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 45, "46", "w");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 46, "47", "X");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 47, "48", "x");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 48, "49", "Y");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 49, "50", "y");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 50, "51", "Z");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 51, "52", "z");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 52, "53", "0");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 53, "54", "1");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 54, "55", "2");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 55, "56", "3");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 56, "57", "4");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 57, "58", "5");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 58, "59", "6");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 59, "60", "7");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 60, "61", "8");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 61, "62", "9");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_letters), 0);
    g_signal_connect(combo_letters, "changed", G_CALLBACK(set_letter), da);

    GtkWidget *combo_drawing=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_drawing, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "1", "Static Draw");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "2", "Rotate Draw Lines");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 2, "3", "Rotate Draw Stencil");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 3, "4", "Rotate Draw Fill");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 4, "5", "Rotate Draw Shadow");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 5, "6", "Text 3d");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
    g_signal_connect(combo_drawing, "changed", G_CALLBACK(set_drawing), da);

    GtkWidget *combo_stress=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_stress, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 0, "1", "No Stress");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 1, "2", "Stress 2x");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 2, "3", "Stress 2y");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 3, "4", "Stress 2z");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_stress), 0);
    g_signal_connect(combo_stress, "changed", G_CALLBACK(set_stress), da);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);

    GtkWidget *scale_label=gtk_label_new("Scale");
    gtk_widget_set_hexpand(scale_label, TRUE);

    GtkWidget *extrude_label=gtk_label_new("Extrude");
    gtk_widget_set_hexpand(extrude_label, TRUE);

    GtkWidget *perspective_label=gtk_label_new(" Perspective");
    gtk_widget_set_hexpand(perspective_label, TRUE);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider, TRUE);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider, TRUE);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider, TRUE);

    GtkWidget *scale_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 2.0, 0.01);
    gtk_widget_set_vexpand(yaw_slider, TRUE);
    gtk_range_set_value(GTK_RANGE(scale_slider), 1.0);

    GtkWidget *extrude_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 20.0, 200.0, 1.0);
    gtk_widget_set_vexpand(extrude_slider, TRUE);

    GtkWidget *perspective_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 100.0, 1.0);
    gtk_widget_set_vexpand(perspective_slider, TRUE);

    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_pitch), da);
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_roll), da);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_yaw), da);
    g_signal_connect(scale_slider, "change-value", G_CALLBACK(set_scale), da);
    g_signal_connect(extrude_slider, "change-value", G_CALLBACK(set_extrude), da);
    g_signal_connect(perspective_slider, "change-value", G_CALLBACK(set_perspective), da);

    GtkWidget *blur_check=gtk_check_button_new_with_label("Set Blur");
    gtk_widget_set_halign(blur_check, GTK_ALIGN_START);
    g_signal_connect(blur_check, "toggled", G_CALLBACK(set_blur), da);

    GtkWidget *timer_check=gtk_check_button_new_with_label("Get Draw Time");
    gtk_widget_set_halign(timer_check, GTK_ALIGN_END);
    g_signal_connect(timer_check, "toggled", G_CALLBACK(set_timer), da);

    GtkWidget *menu1=gtk_menu_new();
    GtkWidget *menu1item1=gtk_menu_item_new_with_label("font1.svg");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item1);
    GtkWidget *menu1item2=gtk_menu_item_new_with_label("font1.pdf");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item2);
    GtkWidget *menu1item3=gtk_menu_item_new_with_label("font1.ps");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item3);
    GtkWidget *title1=gtk_menu_item_new_with_label("Test Output");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(title1), menu1);

    GtkWidget *menu_bar=gtk_menu_bar_new();
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), title1);

    gpointer da_face[2]={da, face};
    g_signal_connect(menu1item1, "activate", G_CALLBACK(draw_svg), da_face);
    g_signal_connect(menu1item2, "activate", G_CALLBACK(draw_pdf), da_face);
    g_signal_connect(menu1item3, "activate", G_CALLBACK(draw_ps), da_face);

    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_letters, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_drawing, 1, 0, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_stress, 1, 1, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale_label, 3, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), extrude_label, 4, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), perspective_label, 5, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale_slider, 3, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), extrude_slider, 4, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), perspective_slider, 5, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), blur_check, 0, 4, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), timer_check, 2, 4, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), menu_bar, 0, 5, 1, 1);

    GtkWidget *shadow_lines_check=gtk_check_button_new_with_label("Set Shadow Lines");
    gtk_widget_set_halign(shadow_lines_check, GTK_ALIGN_START);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(shadow_lines_check), TRUE);
    g_signal_connect(shadow_lines_check, "toggled", G_CALLBACK(set_shadow_lines), da);

    GtkWidget *pitch_label2=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label2, TRUE);

    GtkWidget *roll_label2=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label2, TRUE);

    GtkWidget *yaw_label2=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label2, TRUE);

    GtkWidget *revolve_label=gtk_label_new("Revolve");
    gtk_widget_set_hexpand(revolve_label, TRUE);

    GtkWidget *distance_label=gtk_label_new("Distance");
    gtk_widget_set_hexpand(distance_label, TRUE);

    GtkWidget *pitch_slider2=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider2, TRUE);

    GtkWidget *roll_slider2=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider2, TRUE);

    GtkWidget *yaw_slider2=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider2, TRUE);

    GtkWidget *revolve_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(revolve_slider, TRUE);

    GtkWidget *distance_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 1.0, 0.05);
    gtk_widget_set_vexpand(distance_slider, TRUE);
    gtk_range_set_value(GTK_RANGE(distance_slider), 1.0);

    g_signal_connect(pitch_slider2, "change-value", G_CALLBACK(set_pitch2), da);
    g_signal_connect(roll_slider2, "change-value", G_CALLBACK(set_roll2), da);
    g_signal_connect(yaw_slider2, "change-value", G_CALLBACK(set_yaw2), da);
    g_signal_connect(revolve_slider, "change-value", G_CALLBACK(set_revolve), da);
    g_signal_connect(distance_slider, "change-value", G_CALLBACK(set_distance), da);

    GtkWidget *grid2=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid2), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid2), 8); 
    gtk_grid_attach(GTK_GRID(grid2), shadow_lines_check, 0, 0, 2, 1);
    gtk_grid_attach(GTK_GRID(grid2), pitch_label2, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), roll_label2, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), yaw_label2, 2, 1, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid2), revolve_label, 3, 1, 1, 1);   
    gtk_grid_attach(GTK_GRID(grid2), distance_label, 4, 1, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid2), pitch_slider2, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), roll_slider2, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), yaw_slider2, 2, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), revolve_slider, 3, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), distance_slider, 4, 2, 1, 1);

    GtkAdjustment *red_adj=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *green_adj=gtk_adjustment_new(1.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *blue_adj=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *alpha_adj=gtk_adjustment_new(1.0, 0.0, 1.0, 0.01, 0.0, 0.0);

    GtkWidget *rgba_label=gtk_label_new("RGBA");
    GtkWidget *red_spin=gtk_spin_button_new(red_adj, 0.01, 2);
    g_signal_connect(red_spin, "value-changed", G_CALLBACK(red_spin_changed), NULL);

    GtkWidget *green_spin=gtk_spin_button_new(green_adj, 0.01, 2);
    g_signal_connect(green_spin, "value-changed", G_CALLBACK(green_spin_changed), NULL);

    GtkWidget *blue_spin=gtk_spin_button_new(blue_adj, 0.01, 2);
    g_signal_connect(blue_spin, "value-changed", G_CALLBACK(blue_spin_changed), NULL);

    GtkWidget *alpha_spin=gtk_spin_button_new(alpha_adj, 0.01, 2);
    g_signal_connect(alpha_spin, "value-changed", G_CALLBACK(alpha_spin_changed), NULL);

    GtkWidget *font_color_button=gtk_button_new_with_label("Update Section Color");
    g_signal_connect(font_color_button, "clicked", G_CALLBACK(set_font_color), da);

    GtkWidget *grid3=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid3), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid3), 8);
    gtk_grid_attach(GTK_GRID(grid3), rgba_label, 0, 0, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid3), red_spin, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), green_spin, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), blue_spin, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), alpha_spin, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), font_color_button, 1, 3, 1, 1);    

    GtkWidget *scroll1=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll1, TRUE);
    gtk_widget_set_vexpand(scroll1, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll1), grid1);

    GtkWidget *scroll2=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll2, TRUE);
    gtk_widget_set_vexpand(scroll2, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll2), grid2);

    GtkWidget *scroll3=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll3, TRUE);
    gtk_widget_set_vexpand(scroll3, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll3), grid3);

    GtkWidget *nb_label1=gtk_label_new("Font");
    GtkWidget *nb_label2=gtk_label_new("Shadow Plane"); 
    GtkWidget *nb_label3=gtk_label_new("Options");    
    GtkWidget *notebook=gtk_notebook_new();
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll1, nb_label1);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll2, nb_label2);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll3, nb_label3);

    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), notebook, TRUE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 375);
   
    //Draw background window based on the paned window splitter.
    g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), paned1);

    gtk_container_add(GTK_CONTAINER(window), paned1);

    gtk_widget_show_all(window);

    gtk_main();

    //Clean up.
    if(path_s!=NULL) g_array_free(path_s, TRUE);
    if(text3d!=NULL)
      {
        for(i=0;i<(text3d->len);i++)
          {
            g_array_free(g_ptr_array_index(text3d, i), TRUE);
          }
        g_ptr_array_free(text3d, TRUE);
      }
    if(text2d!=NULL)
      {
        for(i=0;i<(text2d->len);i++)
          {
            g_array_free(g_ptr_array_index(text2d, i), TRUE);
          }
        g_ptr_array_free(text2d, TRUE);
      }
    if(box_size!=NULL) g_array_free(box_size, TRUE);
    FT_Done_Face(face);
    FT_Done_FreeType(library);

    return 0;  
  }
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data)
  {
    //Paint background of drawing area.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr);
    //Paint the background under the grid.
    cairo_set_source_rgba(cr, 0.0, 0.5, 1.0, 0.5);
    gint width=gtk_paned_get_position(GTK_PANED(data));
    gint height=gtk_widget_get_allocated_height(widget);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_rectangle(cr, width, 0.0, 10, height);
    cairo_fill(cr);
    return FALSE;
  }
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_pitch=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_roll=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_yaw=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_scale=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_extrude(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_extrude=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>100.0) slider_perspective=100.0;
    else if(value<0.0) slider_perspective=0.0;
    else slider_perspective=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_letter(GtkComboBox *combo, gpointer data)
  {   
    text_index=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_drawing(GtkComboBox *combo, gpointer data)
  {   
    drawing_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_stress(GtkComboBox *combo, gpointer data)
  {  
    stress_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_blur(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) blur=TRUE;
    else blur=FALSE;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_timer(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) timer_set=TRUE;
    else timer_set=FALSE;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_shadow_lines(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) shadow_lines=TRUE;
    else shadow_lines=FALSE;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_pitch2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_pitch2=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_roll2=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_yaw2=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_revolve(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_revolve=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_distance(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>100.0) slider_distance=100.0;
    else if(value<0.0) slider_distance=0.0;
    else slider_distance=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void red_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    font_rgba[0]=gtk_spin_button_get_value(spin_button);
  }
static void green_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    font_rgba[1]=gtk_spin_button_get_value(spin_button);
  }
static void blue_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    font_rgba[2]=gtk_spin_button_get_value(spin_button);
  }
static void alpha_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    font_rgba[3]=gtk_spin_button_get_value(spin_button);
  }
static void set_font_color(GtkWidget *button, gpointer data)
  {
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void draw_svg(GtkWidget *widget, gpointer *da_font)
  {
    GtkWidget *da=da_font[0];
    gdouble width=(gdouble)gtk_widget_get_allocated_width(GTK_WIDGET(da));
    gdouble height=(gdouble)gtk_widget_get_allocated_height(GTK_WIDGET(da));
    cairo_surface_t *surface=cairo_svg_surface_create("font1.svg", width, height);
    cairo_t *cr=cairo_create(surface);

    draw_surface(cr, width, height, da_font);

    g_print("font1.svg saved\n");
    cairo_destroy(cr);
    cairo_surface_destroy(surface);
  }
static void draw_pdf(GtkWidget *widget, gpointer *da_font)
  {
    GtkWidget *da=da_font[0];
    gdouble width=(gdouble)gtk_widget_get_allocated_width(GTK_WIDGET(da));
    gdouble height=(gdouble)gtk_widget_get_allocated_height(GTK_WIDGET(da));
    cairo_surface_t *surface=cairo_pdf_surface_create("font1.pdf", width, height);
    cairo_t *cr=cairo_create(surface);

    draw_surface(cr, width, height, da_font);

    g_print("font1.pdf saved\n");
    cairo_destroy(cr);
    cairo_surface_destroy(surface);
  }
static void draw_ps(GtkWidget *widget, gpointer *da_font)
  {
    GtkWidget *da=da_font[0];
    gdouble width=(gdouble)gtk_widget_get_allocated_width(GTK_WIDGET(da));
    gdouble height=(gdouble)gtk_widget_get_allocated_height(GTK_WIDGET(da));
    cairo_surface_t *surface=cairo_ps_surface_create("font1.ps", width, height);
    cairo_t *cr=cairo_create(surface);

    draw_surface(cr, width, height, da_font);

    g_print("font1.ps saved\n");
    cairo_destroy(cr);
    cairo_surface_destroy(surface);
  }
static void draw_surface(cairo_t *cr, gdouble width, gdouble height, gpointer *da_font)
  {
    GtkWidget *da=da_font[0];
    FT_Face face=da_font[1];

    if(drawing_id==0) draw_font1(da, cr, face);
    else if(drawing_id==1||drawing_id==2||drawing_id==3) draw_font2(da, cr, face);
    else if(drawing_id==4) draw_font3(da, cr, face);
    else draw_font4(da, cr, face);
  }
static gboolean draw_main(GtkWidget *da, cairo_t *cr, FT_Face face)
  {
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;

    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr);

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 1.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    GTimer *timer=NULL;
    if(timer_set) timer=g_timer_new();

    if(drawing_id==0) draw_font1(da, cr, face);
    else if(drawing_id==1||drawing_id==2||drawing_id==3) draw_font2(da, cr, face);
    else if(drawing_id==4) draw_font3(da, cr, face);
    else draw_font4(da, cr, face);
    
    if(timer!=NULL)
      {
        g_print("Time %f\n", g_timer_elapsed(timer, NULL));
        g_timer_destroy(timer);
      }
    return FALSE;
  }
static void draw_font1(GtkWidget *da, cairo_t *cr, FT_Face face)
  {
    gint i=0;
    gint j=0;
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    
    FT_Error error;
    FT_GlyphSlot slot;
    FT_Outline outline;
    error=FT_Load_Char(face, text[text_index], FT_LOAD_NO_SCALE);
    if(error) g_print("FT_Load_Char error.\n");
    slot=face->glyph;
    outline=slot->outline;

    //Check contours and points.
    //g_print("n_points %i n_contours %i\n", outline.n_points, outline.n_contours);
    //for(i=0;i<outline.n_contours;i++) g_print("contours %i\n", outline.contours[i]);

    FT_Vector *p1=outline.points;
    char *c1=outline.tags;

    //Invert points and letter.
    for(i=0;i<outline.n_points;i++)
      {
        (p1->y)*=-1;
        p1++;
      }   

    //Get a bounding box.
    FT_BBox box;
    gdouble center_x=0.0;
    gdouble center_y=0.0;
    FT_Outline_Get_BBox(&outline, &box);
    //g_print("box %i %i %i %i\n", (int)box.xMin, (int)box.yMin, (int)box.xMax, (int)box.yMax);
    //cairo_set_source_rgba(cr, 0.0, 0.5, 1.0, 1.0);
    //cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
    //cairo_set_line_width(cr, 2.0);
    center_x=((gdouble)box.xMax*scale-(gdouble)box.xMin*scale)/2.0;
    center_y=((gdouble)box.yMax*scale-(gdouble)box.yMin*scale)/2.0;
/*
    //Bounding box.
    cairo_save(cr);
    cairo_translate(cr, width/2.0-center_x, height/2.0-center_y);
    cairo_rectangle(cr, 0.0, 0.0, 2.0*center_x, 2.0*center_y);
    cairo_stroke(cr);
    cairo_restore(cr);
*/

    //Center points.
    p1=outline.points;
    for(i=0;i<outline.n_points;i++)
      {
        (p1->x)=(p1->x)-center_x*1.0/scale-box.xMin;
        (p1->y)=(p1->y)+center_y*1.0/scale-box.yMax;
        p1++;
      }
   
    //Draw from center
    cairo_translate(cr, width/2.0, height/2.0);
    cairo_scale(cr, slider_scale+0.01, slider_scale+0.01);

    //Draw the points.
    p1=outline.points;
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
    cairo_set_line_width(cr, 16.0);
    gint contour_i=0;
    gint contour_n=outline.contours[contour_i];
    gboolean contour_set=FALSE;
    for(i=0;i<outline.n_points;i++)
      {
        if(*c1==FT_CURVE_TAG_ON)
          {
            //g_print("on ");
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
          }
        else if(*c1==FT_CURVE_TAG_CONIC)
          {
            //g_print("off ");
            cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
          }
        else g_print("error ");
        if(i==0||contour_set)
          {
            cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
            contour_set=FALSE;
          }
        //g_print("%i %i %i %i\n", i, (int)(*c1), (int)p1->x, (int)p1->y);
        cairo_move_to(cr, (p1->x)*scale, (p1->y)*scale);
        cairo_line_to(cr, (p1->x)*scale, (p1->y)*scale);
        cairo_stroke(cr);
        p1++;c1++;
        //Advance the contour if needed.
        if(i==contour_n)
          {
            contour_i++;
            contour_n=outline.contours[contour_i];
            contour_set=TRUE;
          }
      }

    //Draw the lines between the points.
    cairo_set_source_rgba(cr, font_rgba[0], font_rgba[1], font_rgba[2], font_rgba[3]);
    cairo_set_line_width(cr, 4.0);
    p1=outline.points;
    c1=outline.tags;
    gboolean draw=FALSE;
    //For converting quadratic bezier point to cubic.
    gdouble q1_x=0.0;
    gdouble q1_y=0.0;
    gdouble q2_x=0.0;
    gdouble q2_y=0.0;
    //Save the last point whether virtual or in the array.
    gdouble last_x=0.0;
    gdouble last_y=0.0;
    //For counting bezier points.
    gint count=0;
    //The virtual drawing points from the bezier points.
    GArray *virtual=g_array_new(FALSE, FALSE, sizeof(gdouble));
    gdouble v_pointx=0.0;
    gdouble v_pointy=0.0;
    //The start of the first contour.
    gdouble c_startx=0.0;
    gdouble c_starty=0.0;
    gint j_start=0;
    //The points in each contour.
    gint contour_points=0;
    //Draw the contours.
    for(i=0;i<outline.n_contours;i++)
      {
        if(i==0) contour_points=outline.contours[i]+1;
        else contour_points=outline.contours[i]-outline.contours[i-1];
        //If the start of the contour is a conic off.
        if(*c1==FT_CURVE_TAG_CONIC)
          {
            if(*(c1+contour_points-1)==FT_CURVE_TAG_CONIC)
              {
                c_startx=((((p1+contour_points-1)->x)+(p1->x))/2.0)*scale;
                c_starty=((((p1+contour_points-1)->y)+(p1->y))/2.0)*scale;
              }
            else
              {
                c_startx=((p1+contour_points-1)->x)*scale;
                c_starty=((p1+contour_points-1)->y)*scale;
              }
            last_x=c_startx;
            last_y=c_starty;
            g_array_append_val(virtual, c_startx);
            g_array_append_val(virtual, c_starty);
            j_start=0;
          }
        else
          {
            last_x=(p1->x)*scale;
            last_y=(p1->y)*scale;
            c_startx=(p1->x)*scale;
            c_starty=(p1->y)*scale;
            j_start=1;
            p1++;c1++;
          }
      
        cairo_move_to(cr, c_startx, c_starty);
       
        for(j=j_start;j<contour_points;j++)
          {  
            if(*c1==FT_CURVE_TAG_CONIC&&!draw)
              {
                count++;
                if(count==2) draw=TRUE;             
              }          
            else if(*c1==FT_CURVE_TAG_ON&&!draw)
              {
                draw=TRUE;
              }
      
            //Draw the contour segments.
            if(draw)
              {       
                if(count==1)
                  {
                    q1_x=(last_x+2.0*((p1-1)->x)*scale)/3.0;
	            q1_y=(last_y+2.0*((p1-1)->y)*scale)/3.0;
                    q2_x=((p1->x)*scale+2.0*((p1-1)->x)*scale)/3.0;
	            q2_y=((p1->y)*scale+2.0*((p1-1)->y)*scale)/3.0;
                    cairo_curve_to(cr, q1_x, q1_y, q2_x, q2_y, (p1->x)*scale, (p1->y)*scale);
                    count=0.0;
                    last_x=(p1->x)*scale;
                    last_y=(p1->y)*scale;
                  }
                else if(count==2)
                  {
                    v_pointx=((((p1-1)->x)+(p1->x))/2)*scale;
                    v_pointy=((((p1-1)->y)+(p1->y))/2)*scale;
                    g_array_append_val(virtual, v_pointx);
                    g_array_append_val(virtual, v_pointy);
                    q1_x=(last_x+2.0*((p1-1)->x)*scale)/3.0;
	            q1_y=(last_y+2.0*((p1-1)->y)*scale)/3.0;
                    q2_x=(v_pointx+2.0*((p1-1)->x)*scale)/3.0;
	            q2_y=(v_pointy+2.0*((p1-1)->y)*scale)/3.0;
                    cairo_curve_to(cr, q1_x, q1_y, q2_x, q2_y, v_pointx, v_pointy);
                    count--;
                    last_x=v_pointx;
                    last_y=v_pointy;
                  }
                else
                  {
                    cairo_line_to(cr, (p1->x)*scale, (p1->y)*scale);
                    count=0;
                    last_x=(p1->x)*scale;
                    last_y=(p1->y)*scale;
                  }
                draw=FALSE;           
              }

            //End drawing the countour.
            if(j==contour_points-1)
              {
                if(count==1)
                  {
                    q1_x=(last_x+2.0*(p1->x)*scale)/3.0;
	            q1_y=(last_y+2.0*(p1->y)*scale)/3.0;
                    q2_x=(c_startx+2.0*(p1->x)*scale)/3.0;
	            q2_y=(c_starty+2.0*(p1->y)*scale)/3.0;
                    cairo_curve_to(cr, q1_x, q1_y, q2_x, q2_y, c_startx, c_starty);
                  }
                else if(count==2)
                  {
                    v_pointx=((((p1-1)->x)+(p1->x))/2)*scale;
                    v_pointy=((((p1-1)->y)+(p1->y))/2)*scale;
                    g_array_append_val(virtual, v_pointx);
                    g_array_append_val(virtual, v_pointy);
                    q1_x=(last_x+2.0*((p1-1)->x)*scale)/3.0;
	            q1_y=(last_y+2.0*((p1-1)->y)*scale)/3.0;
                    q2_x=(v_pointx+2.0*((p1-1)->x)*scale)/3.0;
	            q2_y=(v_pointy+2.0*((p1-1)->y)*scale)/3.0;
                    cairo_curve_to(cr, q1_x, q1_y, q2_x, q2_y, v_pointx, v_pointy);
                    q1_x=(v_pointx+2.0*(p1->x)*scale)/3.0;
	            q1_y=(v_pointy+2.0*(p1->y)*scale)/3.0;
                    q2_x=(c_startx+2.0*(p1->x)*scale)/3.0;
	            q2_y=(c_starty+2.0*(p1->y)*scale)/3.0;
                    cairo_curve_to(cr, q1_x, q1_y, q2_x, q2_y, c_startx, c_starty);
                  }
                else
                  {
                    cairo_line_to(cr, (p1->x)*scale, (p1->y)*scale);
                    cairo_line_to(cr, c_startx, c_starty);
                  }
                 cairo_stroke(cr);
                 count=0.0;
                 draw=FALSE;
               }
            p1++;c1++;
          }
      }

    //Draw the virtual points.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    for(i=0;i<virtual->len;i+=2)
      {
        cairo_move_to(cr, g_array_index(virtual, gdouble, i), g_array_index(virtual, gdouble, i+1));
        cairo_line_to(cr, g_array_index(virtual, gdouble, i), g_array_index(virtual, gdouble, i+1));
        cairo_stroke(cr);
      }

    g_array_free(virtual, TRUE);
  }
static void draw_font2(GtkWidget *da, cairo_t *cr, FT_Face face)
  {
    gint i=0;
    static gint index=-1;
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    static gdouble box_w=0.0;
    static gdouble box_h=0.0;

    //Get a new path from the points if needed.
    if(text_index!=index)
      {
        if(path_s!=NULL) g_array_free(path_s, TRUE);
        index=text_index;
        g_print("New Char\n");
        path_s=get_font_path(face, index, &box_w, &box_h);
      }

    //Make two copies of the path.
    GArray *path1=g_array_sized_new(FALSE, FALSE, sizeof(struct segment2), path_s->len);
    GArray *path2=g_array_sized_new(FALSE, FALSE, sizeof(struct segment2), path_s->len);
    struct segment2 *p1=&g_array_index(path_s, struct segment2, 0);
    struct segment2 *p2=&g_array_index(path1, struct segment2, 0);
    struct segment2 *p3=&g_array_index(path2, struct segment2, 0);
    memcpy(p2, p1, sizeof(struct segment2)*(path_s->len));
    memcpy(p3, p1, sizeof(struct segment2)*(path_s->len));
    g_array_set_size(path1, path_s->len);
    g_array_set_size(path2, path_s->len);

    //Split slider extrude to keep z-axis centered.
    gdouble shift=slider_extrude/2.0;
    for(i=0;i<path1->len;i++)
      {
        (p2->z1)=shift;
        (p2->z2)=shift;
        (p2->z3)=shift;
        (p3->z1)=-shift;
        (p3->z2)=-shift;
        (p3->z3)=-shift;
        p2++;p3++;
      }

    /*
      Apply 3d transforms to the letter. Rotation matrix(qrs), Stress matrix(sm) and a 
      Perspective matrix(pm).
    */
    gdouble qrs[9];
    gdouble sm[9];
    gdouble pm[9];
    gdouble out1[9];
    gdouble out2[9];
    gdouble pitch=slider_pitch*G_PI/180.0;
    gdouble roll=slider_roll*G_PI/180.0;
    gdouble yaw=slider_yaw*G_PI/180.0;
    quaternion_rotation(yaw, roll, pitch, qrs);
    if(stress_id==0)
      {
        sm[0]=1.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=1.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=1.0; 
      }
   else if(stress_id==1)
      {
        sm[0]=2.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=1.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=1.0;        
      }
    else if(stress_id==2)
      {
        sm[0]=1.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=2.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=1.0;
      }
    else
      {
        sm[0]=1.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=1.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=2.0;
      }
    gdouble perspective1=(100.0-slider_perspective)/100.0; 
    pm[0]=perspective1;pm[1]=0.0;pm[2]=0.0;
    pm[3]=0.0;pm[4]=perspective1;pm[5]=0.0;
    pm[6]=0.0;pm[7]=0.0;pm[8]=1.0;
    matrix_multiply(sm, qrs, out1);
    transform_letter(path1, out1);
    matrix_multiply(pm, out1, out2);  
    transform_letter(path2, out2);

    //Draw from the center
    cairo_translate(cr, width/2.0, height/2.0);
    cairo_scale(cr, slider_scale+0.01, slider_scale+0.01);
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);

/*
    //Bounding box.
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    cairo_rectangle(cr, -box_w/2.0, -box_h/2.0, box_w, box_h);
    cairo_stroke(cr);
*/

    //Draw the paths.
    gdouble rgba1[4];
    rgba1[0]=font_rgba[0];rgba1[1]=font_rgba[1];rgba1[2]=font_rgba[2];rgba1[3]=font_rgba[3];
    if(drawing_id==1)
      {
        cairo_set_source_rgba(cr, font_rgba[0], font_rgba[1], font_rgba[2], font_rgba[3]);
        cairo_set_line_width(cr, 4.0);
        draw_path_contours_connect(cr, path1, path2);
        draw_path_contours_line(cr, path2);
        draw_path_contours_line(cr, path1);
      }
    else if(drawing_id==2)
      {
        cairo_set_source_rgba(cr, font_rgba[0], font_rgba[1], font_rgba[2], font_rgba[3]);
        cairo_set_line_width(cr, 4.0);
        draw_path_contours_line(cr, path2);
        draw_path_contours_line(cr, path1);
        cairo_set_line_width(cr, 0.01);
        if(blur) draw_path_side_fill_blur(cr, path1, path2, rgba1);
        else draw_path_side_fill(cr, path1, path2, rgba1);
      }
    else
      {
       if(v1_r[2]>=0.0)
         {
           cairo_set_source_rgba(cr, font_rgba[0], font_rgba[1], font_rgba[2], font_rgba[3]);
           draw_path_contours_fill(cr, path2);
           cairo_set_source_rgba(cr, font_rgba[0], font_rgba[1], font_rgba[2], font_rgba[3]);
           if(blur) draw_path_side_fill_blur(cr, path1, path2, rgba1);               
           else draw_path_side_fill(cr, path1, path2, rgba1);
           cairo_set_source_rgba(cr, font_rgba[0], font_rgba[1], font_rgba[2], font_rgba[3]);
           draw_path_contours_fill(cr, path1);
           cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
           cairo_set_line_width(cr, 4.0);
           draw_path_contours_line(cr, path1);
         }
       else
         {
           cairo_set_source_rgba(cr, font_rgba[0], font_rgba[1], font_rgba[2], font_rgba[3]);
           draw_path_contours_fill(cr, path1);
           cairo_set_source_rgba(cr, font_rgba[0], font_rgba[1], font_rgba[2], font_rgba[3]);
           if(blur) draw_path_side_fill_blur(cr, path1, path2, rgba1);
           else draw_path_side_fill(cr, path1, path2, rgba1);
           cairo_set_source_rgba(cr, font_rgba[0], font_rgba[1], font_rgba[2], font_rgba[3]);
           draw_path_contours_fill(cr, path2);
           cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
           cairo_set_line_width(cr, 4.0);
           draw_path_contours_line(cr, path2);
         }       
      }
   
    g_array_free(path1, TRUE);
    g_array_free(path2, TRUE);
  }
static void draw_font3(GtkWidget *da, cairo_t *cr, FT_Face face)
  {
    static gint index=-1;
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);

    //Get a new path from the points if needed.
    if(text_index!=index)
      {
        if(path_s!=NULL) g_array_free(path_s, TRUE);
        index=text_index;
        g_print("New Char\n");
        path_s=get_font_path(face, index, NULL, NULL);
      }

    //Make a copy of the path.
    GArray *path1=g_array_sized_new(FALSE, FALSE, sizeof(struct segment2), path_s->len);
    struct segment2 *p1=&g_array_index(path_s, struct segment2, 0);
    struct segment2 *p2=&g_array_index(path1, struct segment2, 0);
    memcpy(p2, p1, sizeof(struct segment2)*(path_s->len));
    g_array_set_size(path1, path_s->len);

    /*
      Apply 3d transforms to the letter. Rotation matrix(qrs), Stress matrix(sm) and a 
      Perspective matrix(pm).
    */
    gdouble qrs[9];
    gdouble sm[9];
    gdouble pm[9];
    gdouble out1[9];
    gdouble out2[9];
    gdouble pitch=slider_pitch*G_PI/180.0;
    gdouble roll=slider_roll*G_PI/180.0;
    gdouble yaw=slider_yaw*G_PI/180.0;
    quaternion_rotation(yaw, roll, pitch, qrs);
    if(stress_id==0)
      {
        sm[0]=1.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=1.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=1.0; 
      }
    else if(stress_id==1)
      {
        sm[0]=2.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=1.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=1.0;        
      }
    else if(stress_id==2)
      {
        sm[0]=1.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=2.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=1.0;
      }
    else
      {
        sm[0]=1.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=1.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=2.0;
      }
    gdouble perspective1=(100.0-slider_perspective)/100.0; 
    pm[0]=perspective1;pm[1]=0.0;pm[2]=0.0;
    pm[3]=0.0;pm[4]=perspective1;pm[5]=0.0;
    pm[6]=0.0;pm[7]=0.0;pm[8]=1.0;
    matrix_multiply(sm, qrs, out1);
    matrix_multiply(pm, out1, out2);
    transform_letter(path1, out2);

    //Draw from the center
    cairo_translate(cr, width/2.0, height/2.0);
    cairo_scale(cr, slider_scale+0.01, slider_scale+0.01);
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);

    //Draw the char path.
    cairo_set_source_rgba(cr, font_rgba[0], font_rgba[1], font_rgba[2], font_rgba[3]);
    cairo_set_line_width(cr, 4.0);
    draw_path_contours_fill(cr, path1); 
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    draw_path_contours_line(cr, path1);
    //Draw a shadow. 
    gdouble light[3]; 
    //light[0]=150.0*cos(slider_revolve*2.0*G_PI/360.0);
    //light[1]=150.0*sin(slider_revolve*2.0*G_PI/360.0);
    //light[2]=0.0; 
    light[0]=150.0*sin(G_PI/2.0)*cos(slider_revolve*2.0*G_PI/360.0);
    light[1]=150.0*sin(G_PI/2.0)*sin(slider_revolve*2.0*G_PI/360.0);
    light[2]=150.0*cos(G_PI/2.0); 
    gdouble qrs_plane[9];
    quaternion_rotation(slider_yaw2*G_PI/180.0, slider_roll2*G_PI/180.0, slider_pitch2*G_PI/180.0, qrs_plane);
    GArray *shadow=get_shadow_path(path1, light, slider_distance, qrs_plane);
    gdouble rgba1[4];
    rgba1[0]=font_rgba[0];rgba1[1]=font_rgba[1];rgba1[2]=font_rgba[2];rgba1[3]=font_rgba[3]/5.0;
    if(blur) draw_path_side_fill_blur(cr, path1, shadow, rgba1);  
    cairo_set_line_width(cr, 2.0);
    cairo_set_source_rgba(cr, font_rgba[0], font_rgba[1], font_rgba[2], font_rgba[3]);
    if(shadow_lines) draw_path_contours_connect(cr, path1, shadow);
    cairo_set_source_rgba(cr, font_rgba[0], font_rgba[1], font_rgba[2], font_rgba[3]);
    draw_path_contours_fill(cr, shadow);
     
    g_array_free(path1, TRUE);
    g_array_free(shadow, TRUE);
  }
static void draw_font4(GtkWidget *da, cairo_t *cr, FT_Face face)
  {
    gint i=0;
    gint j=0;
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;

    //Initialize text for 3d.   
    if(text3d==NULL)
      {
        box_size=g_array_new(FALSE, FALSE, sizeof(gdouble));
        GArray *index=g_array_new(FALSE, FALSE, sizeof(gint));
        gint val=37;
        g_array_append_val(index, val);
        val=41;
        g_array_append_val(index, val);
        val=27;
        g_array_append_val(index, val);
        text3d=get_font_paths_3d(face, index, box_size);
        g_array_free(index, TRUE);
      }

    //Initialize text for 2d or shadow chars.
    if(text2d==NULL)
      {
        GArray *index=g_array_new(FALSE, FALSE, sizeof(gint));
        gint val=16;
        g_array_append_val(index, val);
        val=31;
        g_array_append_val(index, val);
        val=0;
        g_array_append_val(index, val);
        text2d=get_font_paths_2d(face, index, box_size);
        g_array_free(index, TRUE);
      }

    //Make a copy to transform and draw the 3d text.
    GArray *path1=NULL;
    GArray *path2=NULL;
    struct segment2 *p1=NULL;
    struct segment2 *p2=NULL;
    GPtrArray *text3d_c=g_ptr_array_new();
    for(i=0;i<(text3d->len); i++)
      {
        path1=(GArray*)g_ptr_array_index(text3d, i);
        path2=g_array_sized_new(FALSE, FALSE, sizeof(struct segment2), path1->len);
        p1=&g_array_index(path1, struct segment2, 0);
        p2=&g_array_index(path2, struct segment2, 0);
        memcpy(p2, p1, sizeof(struct segment2)*(path1->len));
        g_array_set_size(path2, path1->len);
        g_ptr_array_add(text3d_c, (gpointer)path2);
      }

    //A copy of the 2d text.
    GPtrArray *text2d_c=g_ptr_array_new();
    for(i=0;i<(text2d->len); i++)
      {
        path1=(GArray*)g_ptr_array_index(text2d, i);
        path2=g_array_sized_new(FALSE, FALSE, sizeof(struct segment2), path1->len);
        p1=&g_array_index(path1, struct segment2, 0);
        p2=&g_array_index(path2, struct segment2, 0);
        memcpy(p2, p1, sizeof(struct segment2)*(path1->len));
        g_array_set_size(path2, path1->len);
        g_ptr_array_add(text2d_c, (gpointer)path2);
      }

    //Split slider extrude to keep z-axis centered for 3d text.
    gdouble shift=slider_extrude/2.0;
    for(i=0;i<(text3d_c->len); i+=2)
      {
        path1=(GArray*)g_ptr_array_index(text3d_c, i);
        p1=&g_array_index(path1, struct segment2, 0);
        path2=(GArray*)g_ptr_array_index(text3d_c, i+1);
        p2=&g_array_index(path1, struct segment2, 0);
        for(j=0;j<path1->len;j++)
          {
            (p1->z1)=shift;
            (p1->z2)=shift;
            (p1->z3)=shift;
            (p2->z1)=-shift;
            (p2->z2)=-shift;
            (p2->z3)=-shift;
            p1++;p2++;
          } 
       }    

    /*
      Apply 3d transforms to the letters. Rotation matrix(qrs), Stress matrix(sm) and a 
      Perspective matrix(pm).
    */
    gdouble qrs[9];
    gdouble sm[9];
    gdouble pm[9];
    gdouble out1[9];
    gdouble out2[9];
    gdouble pitch=slider_pitch*G_PI/180.0;
    gdouble roll=slider_roll*G_PI/180.0;
    gdouble yaw=slider_yaw*G_PI/180.0;
    quaternion_rotation(yaw, roll, pitch, qrs);
    if(stress_id==0)
      {
        sm[0]=1.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=1.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=1.0; 
      }
    else if(stress_id==1)
      {
        sm[0]=2.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=1.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=1.0;        
      }
    else if(stress_id==2)
      {
        sm[0]=1.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=2.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=1.0;
      }
    else
      {
        sm[0]=1.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=1.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=2.0;
      }
    gdouble perspective1=(100.0-slider_perspective)/100.0; 
    pm[0]=perspective1;pm[1]=0.0;pm[2]=0.0;
    pm[3]=0.0;pm[4]=perspective1;pm[5]=0.0;
    pm[6]=0.0;pm[7]=0.0;pm[8]=1.0;
    matrix_multiply(sm, qrs, out1);
    for(i=0;i<(text3d_c->len); i+=2)
      {
        path1=(GArray*)g_ptr_array_index(text3d_c, i);
        transform_letter(path1, out1);
      }
    matrix_multiply(pm, out1, out2); 
    for(i=1;i<(text3d_c->len); i+=2)
     {
       path2=(GArray*)g_ptr_array_index(text3d_c, i);
       transform_letter(path2, out2);
     }  

    //Draw the 3d text
    //gdouble box_w=0.0;
    //gdouble box_h=0.0;
    cairo_save(cr);
    cairo_translate(cr, 2.0*w1, 3.0*h1);
    cairo_scale(cr, slider_scale+0.01, slider_scale+0.01);
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
    gdouble rgba1[4]={1.0, 1.0, 0.0, 0.7};
    for(i=0;i<(text3d_c->len); i+=2)
     {
       path1=(GArray*)g_ptr_array_index(text3d_c, i);
       path2=(GArray*)g_ptr_array_index(text3d_c, i+1);
       if(v1_r[2]>=0.0)
         {
           cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
           draw_path_contours_fill(cr, path2);
           cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
           if(blur) draw_path_side_fill_blur(cr, path1, path2, rgba1);               
           else draw_path_side_fill(cr, path1, path2, rgba1);
           cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
           draw_path_contours_fill(cr, path1);
           cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
           cairo_set_line_width(cr, 4.0);
           draw_path_contours_line(cr, path1);
         }
       else
         {
           cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
           draw_path_contours_fill(cr, path1);
           cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
           if(blur) draw_path_side_fill_blur(cr, path1, path2, rgba1);
           else draw_path_side_fill(cr, path1, path2, rgba1);
           cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
           draw_path_contours_fill(cr, path2);
           cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
           cairo_set_line_width(cr, 4.0);
           draw_path_contours_line(cr, path2);
         } 
        //Test some boxes.
        //cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        //box_w=g_array_index(box_size, gdouble, i);
        //box_h=g_array_index(box_size, gdouble, i+1);
        //cairo_rectangle(cr, -box_w/2.0, -box_h/2.0, box_w, box_h);
        //cairo_stroke(cr);
        cairo_translate(cr, 3.0*w1, 0.0);
      } 
    cairo_restore(cr);

    //Transform the 2d text.
    pitch=slider_pitch*G_PI/180.0;
    roll=slider_roll*G_PI/180.0;
    yaw=slider_yaw*G_PI/180.0;
    quaternion_rotation(yaw, roll, pitch, qrs);
    if(stress_id==0)
      {
        sm[0]=1.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=1.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=1.0; 
      }
    else if(stress_id==1)
      {
        sm[0]=2.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=1.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=1.0;        
      }
    else if(stress_id==2)
      {
        sm[0]=1.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=2.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=1.0;
      }
    else
      {
        sm[0]=1.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=1.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=2.0;
      }
    perspective1=(100.0-slider_perspective)/200.0; 
    pm[0]=perspective1;pm[1]=0.0;pm[2]=0.0;
    pm[3]=0.0;pm[4]=perspective1;pm[5]=0.0;
    pm[6]=0.0;pm[7]=0.0;pm[8]=1.0;
    matrix_multiply(sm, qrs, out1);
    matrix_multiply(pm, out1, out2);
    for(i=0;i<(text2d_c->len); i++)
     {
       path1=(GArray*)g_ptr_array_index(text2d_c, i);
       transform_letter(path1, out2);
     }  

    cairo_translate(cr, 2.5*w1, 6.0*h1);
    cairo_scale(cr, slider_scale+0.01, slider_scale+0.01);
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);

    //Draw the 2d text.
    cairo_set_line_width(cr, 4.0);
    gdouble rgba2[4]={0.0, 0.0, 1.0, 0.1};
    gdouble light[3]; 
    light[0]=350.0*cos(slider_revolve*2.0*G_PI/360.0);
    light[1]=350.0*sin(slider_revolve*2.0*G_PI/360.0);
    light[2]=0.0; 
    gdouble qrs_plane[9];
    quaternion_rotation(slider_yaw2*G_PI/180.0, slider_roll2*G_PI/180.0, slider_pitch2*G_PI/180.0, qrs_plane);
    for(i=0;i<(text2d_c->len); i++)
      {
        path1=(GArray*)g_ptr_array_index(text2d_c, i);
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.5);
        draw_path_contours_fill(cr, path1); 
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        draw_path_contours_line(cr, path1);  
        GArray *shadow=get_shadow_path(path1, light, slider_distance, qrs_plane);
        if(blur) draw_path_side_fill_blur(cr, path1, shadow, rgba2);  
        cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 0.5);
        draw_path_contours_fill(cr, shadow);
        g_array_free(shadow, TRUE);
        cairo_translate(cr, 2.5*w1, 0.0); 
      }   

    //Clean up copies.
    if(text2d_c!=NULL)
      {
        for(i=0;i<(text2d_c->len);i++)
          {
            g_array_free(g_ptr_array_index(text2d_c, i), TRUE);
          }
        g_ptr_array_free(text2d_c, TRUE);
      }  

    if(text3d_c!=NULL)
      {
        for(i=0;i<(text3d_c->len);i++)
          {
            g_array_free(g_ptr_array_index(text3d_c, i), TRUE);
          }
        g_ptr_array_free(text3d_c, TRUE);
      }  
  }
static void draw_path_contours_line(cairo_t *cr, GArray *path)
 {
   gint i=0;
   struct segment2 *p1=&g_array_index(path, struct segment2, 0);
   for(i=0;i<(path->len);i++)
     {
       if((p1->move_id)==MOVE_TO_R||(p1->move_id)==MOVE_TO_L)
         {
           cairo_move_to(cr, p1->x3, p1->y3);           
         }
       else if((p1->move_id)==LINE_TO)
         {
           cairo_line_to(cr, p1->x3, p1->y3); 
         }
       else if((p1->move_id)==CURVE_TO)
         {
           cairo_curve_to(cr, p1->x1, p1->y1, p1->x2, p1->y2, p1->x3, p1->y3);
         }
       else if((p1->move_id)==LINE_TO_STROKE)
         {
           cairo_line_to(cr, p1->x3, p1->y3);
           cairo_stroke(cr);
         }
       else //((p1->move_id)==CURVE_TO_STROKE)
         {
           cairo_curve_to(cr, p1->x1, p1->y1, p1->x2, p1->y2, p1->x3, p1->y3);
           cairo_stroke(cr); 
         }
       p1++;
     }
 }
static void draw_path_contours_connect(cairo_t *cr, GArray *path1, GArray *path2)
 {
   gint i=0;
   struct segment2 *p1=&g_array_index(path1, struct segment2, 0);
   struct segment2 *p2=&g_array_index(path2, struct segment2, 0);

   for(i=0;i<(path1->len);i++)
     {      
       cairo_move_to(cr, p1->x3, p1->y3);
       cairo_line_to(cr, p2->x3, p2->y3);
       cairo_stroke(cr);        
       p1++;p2++;          
     }
 }
static void draw_path_side_fill(cairo_t *cr, GArray *path1, GArray *path2, gdouble rgba[4])
 {
   gint i=0;
   gint j=0;
   struct segment2 *p1=&g_array_index(path1, struct segment2, 0);
   struct segment2 *p2=&g_array_index(path2, struct segment2, 0);
   for(i=0;i<(path1->len-1);i++)
     {
       //If at the end of a contour move again.
       if((p1+1)->move_id==MOVE_TO_R||(p1+1)->move_id==MOVE_TO_L)
         {
           //Move to next point.
         }
       else
         {
           for(j=0;j<5;j++)
             {
               if(j==0)
                 {
                   cairo_move_to(cr, p1->x3, p1->y3);           
                 }
               else if(j==1&&(((p1+1)->move_id)==LINE_TO||((p1+1)->move_id)==LINE_TO_STROKE))
                 {
                   cairo_line_to(cr, (p1+1)->x3, (p1+1)->y3); 
                 }
               else if(j==1&&(((p1+1)->move_id)==CURVE_TO||((p1+1)->move_id)==CURVE_TO_STROKE))
                 {
                   cairo_curve_to(cr, (p1+1)->x1, (p1+1)->y1, (p1+1)->x2, (p1+1)->y2, (p1+1)->x3, (p1+1)->y3);
                 }
               else if(j==2)
                 {
                   cairo_line_to(cr, (p2+1)->x3, (p2+1)->y3); 
                 }
               else if(j==3&&(((p2+1)->move_id)==LINE_TO||((p2+1)->move_id)==LINE_TO_STROKE))
                 {
                   cairo_line_to(cr, p2->x3, p2->y3); 
                 }
               else if(j==3&&(((p2+1)->move_id)==CURVE_TO||((p2+1)->move_id)==CURVE_TO_STROKE))
                 {
                   cairo_curve_to(cr, (p2+1)->x2, (p2+1)->y2, (p2+1)->x1, (p2+1)->y1, p2->x3, p2->y3);
                 }
               else
                 {
                   cairo_close_path(cr);
                   cairo_fill(cr);
                 } 
             }         
         }
       p1++;p2++;
     }
 }
static void draw_path_side_fill_blur(cairo_t *cr, GArray *path1, GArray *path2, gdouble rgba[4])
 {
   gint i=0;
   gint j=0;
   struct segment2 *p1=&g_array_index(path1, struct segment2, 0);
   struct segment2 *p2=&g_array_index(path2, struct segment2, 0);

   //An array to sort quads.
   struct quad q;
   GArray *quads=g_array_new(FALSE, FALSE, sizeof(struct quad));

   for(i=0;i<(path1->len-1);i++)
     {
       //If at the end of a contour move again.
       if((p1+1)->move_id==MOVE_TO_R||(p1+1)->move_id==MOVE_TO_L)
         {
           //Move to next point.
         }
       else
         {
           for(j=0;j<5;j++)
             {
               if(j==0)
                 {
                   //cairo_move_to(cr, p1->x3, p1->y3);
                   q.a.move_id=MOVE_TO_R;
                   q.a.x1=0.0;q.a.y1=0.0;q.a.z1=0.0;
                   q.a.x2=0.0;q.a.y2=0.0;q.a.z2=0.0;
                   q.a.x3=p1->x3;q.a.y3=p1->y3;q.a.z3=p1->z3;          
                 }
               else if(j==1&&(((p1+1)->move_id)==LINE_TO||((p1+1)->move_id)==LINE_TO_STROKE))
                 {
                   //cairo_line_to(cr, (p1+1)->x3, (p1+1)->y3);
                   q.b.move_id=LINE_TO; 
                   q.b.x1=0.0;q.b.y1=0.0;q.b.z1=0.0;
                   q.b.x2=0.0;q.b.y2=0.0;q.b.z2=0.0;
                   q.b.x3=(p1+1)->x3;q.b.y3=(p1+1)->y3;q.b.z3=(p1+1)->z3; 
                 }
               else if(j==1&&(((p1+1)->move_id)==CURVE_TO||((p1+1)->move_id)==CURVE_TO_STROKE))
                 {
                   //cairo_curve_to(cr, (p1+1)->x1, (p1+1)->y1, (p1+1)->x2, (p1+1)->y2, (p1+1)->x3, (p1+1)->y3);
                   q.b.move_id=CURVE_TO;
                   q.b.x1=(p1+1)->x1;q.b.y1=(p1+1)->y1;q.b.z1=(p1+1)->z1;
                   q.b.x2=(p1+1)->x2;q.b.y2=(p1+1)->y2;q.b.z2=(p1+1)->z2;
                   q.b.x3=(p1+1)->x3;q.b.y3=(p1+1)->y3;q.b.z3=(p1+1)->z3; 
                 }
               else if(j==2)
                 {
                   //cairo_line_to(cr, (p2+1)->x3, (p2+1)->y3);
                   q.c.move_id=LINE_TO;
                   q.c.x1=0.0;q.c.y1=0.0;q.c.z1=0.0;
                   q.c.x2=0.0;q.c.y2=0.0;q.c.z2=0.0;
                   q.c.x3=(p2+1)->x3;q.c.y3=(p2+1)->y3;q.c.z3=(p2+1)->z3; 
                 }
               else if(j==3&&(((p2+1)->move_id)==LINE_TO||((p2+1)->move_id)==LINE_TO_STROKE))
                 {
                   //cairo_line_to(cr, p2->x3, p2->y3); 
                   q.d.move_id=LINE_TO;
                   q.d.x1=0.0;q.d.y1=0.0;q.d.z1=0.0;
                   q.d.x2=0.0;q.d.y2=0.0;q.d.z2=0.0;
                   q.d.x3=p2->x3;q.d.y3=p2->y3;q.d.z3=p2->z3; 
                 }
               else if(j==3&&(((p2+1)->move_id)==CURVE_TO||((p2+1)->move_id)==CURVE_TO_STROKE))
                 {
                   //cairo_curve_to(cr, (p2+1)->x2, (p2+1)->y2, (p2+1)->x1, (p2+1)->y1, p2->x3, p2->y3);
                   q.d.move_id=CURVE_TO;
                   q.d.x1=(p2+1)->x2;q.d.y1=(p2+1)->y2;q.d.z1=(p2+1)->z2;
                   q.d.x2=(p2+1)->x1;q.d.y2=(p2+1)->y1;q.d.z2=(p2+1)->z1;
                   q.d.x3=p2->x3;q.d.y3=p2->y3;q.d.z3=p2->z3; 
                 }
               else
                 {
                   g_array_append_val(quads, q);
                 } 
             }         
         }
       p1++;p2++;
     }

   g_array_sort(quads, compare_quads);

   struct quad *q1=&g_array_index(quads, struct quad, 0);
   for(i=0;i<quads->len;i++)
     {
       cairo_pattern_t *pattern1=cairo_pattern_create_mesh();
       cairo_mesh_pattern_begin_patch(pattern1);
       cairo_mesh_pattern_move_to(pattern1, (q1->a).x3, (q1->a).y3);
       if((q1->b).move_id==LINE_TO)
         {
           cairo_mesh_pattern_line_to(pattern1, (q1->b).x3, (q1->b).y3);
         }
       else
         {
           cairo_mesh_pattern_curve_to(pattern1, (q1->b).x1, (q1->b).y1, (q1->b).x2, (q1->b).y2, (q1->b).x3, (q1->b).y3);
         }
       cairo_mesh_pattern_line_to(pattern1, (q1->c).x3, (q1->c).y3);
       if((q1->d).move_id==LINE_TO)
         {
           cairo_mesh_pattern_line_to(pattern1, (q1->d).x3, (q1->d).y3);
         }
       else
         {
           cairo_mesh_pattern_curve_to(pattern1, (q1->d).x1, (q1->d).y1, (q1->d).x2, (q1->d).y2, (q1->d).x3, (q1->d).y3);
         } 
       cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, rgba[0], rgba[1], rgba[2], rgba[3]);
       cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, rgba[0], rgba[1], rgba[2], rgba[3]);
       cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, 1.0, 1.0, 1.0, 0.0);
       cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, 1.0, 1.0, 1.0, 0.0);
       cairo_mesh_pattern_end_patch(pattern1);
       cairo_set_source(cr, pattern1);

       cairo_move_to(cr, (q1->a).x3, (q1->a).y3);
       if((q1->b).move_id==LINE_TO)
         {
           cairo_line_to(cr, (q1->b).x3, (q1->b).y3);
         }
       else
         {
           cairo_curve_to(cr, (q1->b).x1, (q1->b).y1, (q1->b).x2, (q1->b).y2, (q1->b).x3, (q1->b).y3);
         }
       cairo_line_to(cr, (q1->c).x3, (q1->c).y3);
       if((q1->d).move_id==LINE_TO)
         {
           cairo_line_to(cr, (q1->d).x3, (q1->d).y3);
         }
       else
         {
           cairo_curve_to(cr, (q1->d).x1, (q1->d).y1, (q1->d).x2, (q1->d).y2, (q1->d).x3, (q1->d).y3);
         }
       cairo_close_path(cr);
       cairo_fill(cr);
       cairo_pattern_destroy(pattern1);
       q1++;
     }


   g_array_free(quads, TRUE);
 }
static void draw_path_contours_fill(cairo_t *cr, GArray *path)
 {
   gint i=0;
   struct segment2 *p1=&g_array_index(path, struct segment2, 0);

   for(i=0;i<(path->len);i++)
     {
       if((p1->move_id)==MOVE_TO_R||(p1->move_id)==MOVE_TO_L)
         {
           //For counter clockwise fill. 
           if((p1->move_id)==MOVE_TO_L)
             {
               cairo_set_fill_rule(cr, CAIRO_FILL_RULE_WINDING);
               cairo_new_sub_path(cr);
             }
           cairo_move_to(cr, p1->x3, p1->y3);           
         }
       else if((p1->move_id)==LINE_TO)
         {
           cairo_line_to(cr, p1->x3, p1->y3);
         }
       else if((p1->move_id)==CURVE_TO)
         {
           cairo_curve_to(cr, p1->x1, p1->y1, p1->x2, p1->y2, p1->x3, p1->y3);
         }
       else if((p1->move_id)==LINE_TO_STROKE)
         {
           cairo_line_to(cr, p1->x3, p1->y3);
           cairo_close_path(cr);
         }
       else //((p1->move_id)==CURVE_TO_STROKE)
         {
           cairo_curve_to(cr, p1->x1, p1->y1, p1->x2, p1->y2, p1->x3, p1->y3);
           cairo_close_path(cr);
         }
       p1++;     
     }
   cairo_fill(cr);
 }
static GPtrArray* get_font_paths_2d(FT_Face face, GArray *index, GArray *box_size)
 {
   gint i=0;
   GPtrArray *text2d=g_ptr_array_new();
   GArray *path1=NULL;
   gdouble box_w;
   gdouble box_h;

   for(i=0;i<(index->len);i++)
     {
       path1=get_font_path(face, g_array_index(index, gint, i), &box_w, &box_h);
       g_ptr_array_add(text2d, (gpointer)path1);
       g_array_append_val(box_size, box_w);
       g_array_append_val(box_size, box_h);
     }

   return text2d;
 }
static GPtrArray* get_font_paths_3d(FT_Face face, GArray *index, GArray *box_size)
 {
   gint i=0;
   GPtrArray *text3d=g_ptr_array_new();
   GArray *path1=NULL;
   GArray *path2=NULL;
   struct segment2 *p1=NULL;
   struct segment2 *p2=NULL;
   gdouble box_w;
   gdouble box_h;

   for(i=0;i<(index->len);i++)
     {
       path1=get_font_path(face, g_array_index(index, gint, i), &box_w, &box_h);
       g_array_append_val(box_size, box_w);
       g_array_append_val(box_size, box_h);
       g_ptr_array_add(text3d, (gpointer)path1);
       path2=g_array_sized_new(FALSE, FALSE, sizeof(struct segment2), path1->len);
       p1=&g_array_index(path1, struct segment2, 0);
       p2=&g_array_index(path2, struct segment2, 0);
       memcpy(p2, p1, sizeof(struct segment2)*(path1->len));
       g_array_set_size(path2, path1->len);
       g_ptr_array_add(text3d, (gpointer)path2);
     }

   return text3d;
 }
static GArray* get_font_path(FT_Face face, gint index, gdouble *box_w, gdouble *box_h)
 {
   gint i=0;
   gint j=0;

   FT_Outline outline=get_font_outline(face, index, box_w, box_h);

   GArray *path=NULL;
   struct segment2 seg;
   if(outline.n_points<1)
     {
       goto exit;
     }
   else
     {
       path=g_array_new(FALSE, FALSE, sizeof(struct segment2));
     }

   FT_Vector *p1=outline.points;
   char *c1=outline.tags;

   p1=outline.points;
   gboolean draw=FALSE;
   //For converting quadratic bezier point to cubic.
   gdouble q1_x=0.0;
   gdouble q1_y=0.0;
   gdouble q2_x=0.0;
   gdouble q2_y=0.0;
   //Save the last point whether virtual or in the array.
   gdouble last_x=0.0;
   gdouble last_y=0.0;
   //For counting bezier points.
   gint count=0;
   gdouble v_pointx=0.0;
   gdouble v_pointy=0.0;
   //The start of the first contour.
   gdouble c_startx=0.0;
   gdouble c_starty=0.0;
   gint j_start=0;
   //The points in each contour.
   gint contour_points=0;
   //Draw the contours.
   for(i=0;i<outline.n_contours;i++)
     {
       if(i==0) contour_points=outline.contours[i]+1;
       else contour_points=outline.contours[i]-outline.contours[i-1];
       //If the start of the contour is a conic off.
       if(*c1==FT_CURVE_TAG_CONIC)
         {
           if(*(c1+contour_points-1)==FT_CURVE_TAG_CONIC)
             {
               c_startx=((((p1+contour_points-1)->x)+(p1->x))/2.0)*scale;
               c_starty=((((p1+contour_points-1)->y)+(p1->y))/2.0)*scale;
             }
           else
             {
               c_startx=((p1+contour_points-1)->x)*scale;
               c_starty=((p1+contour_points-1)->y)*scale;
             }
           last_x=c_startx;
           last_y=c_starty;
           j_start=0;
         }
       else
         {
           last_x=(p1->x)*scale;
           last_y=(p1->y)*scale;
           c_startx=(p1->x)*scale;
           c_starty=(p1->y)*scale;
           j_start=1;
           p1++;c1++;
         }
      
       seg.move_id=MOVE_TO_R;seg.x1=0.0;seg.y1=0.0;seg.z1=0.0;
       seg.x2=0.0;seg.y2=0.0;seg.z2=0.0;
       seg.x3=c_startx;seg.y3=c_starty;seg.z3=0.0;
       g_array_append_val(path, seg);
       
       for(j=j_start;j<contour_points;j++)
         {  
           if(*c1==FT_CURVE_TAG_CONIC&&!draw)
             {
               count++;
               if(count==2) draw=TRUE;             
             }          
           else if(*c1==FT_CURVE_TAG_ON&&!draw)
             {
               draw=TRUE;
             }
      
           //Draw the contour segments.
           if(draw)
             {       
               if(count==1)
                 {
                   q1_x=(last_x+2.0*((p1-1)->x)*scale)/3.0;
	           q1_y=(last_y+2.0*((p1-1)->y)*scale)/3.0;
                   q2_x=((p1->x)*scale+2.0*((p1-1)->x)*scale)/3.0;
	           q2_y=((p1->y)*scale+2.0*((p1-1)->y)*scale)/3.0;
                   seg.move_id=CURVE_TO;seg.x1=q1_x;seg.y1=q1_y;seg.z1=0.0;
                   seg.x2=q2_x;seg.y2=q2_y;seg.z2=0.0;
                   seg.x3=(p1->x)*scale;seg.y3=(p1->y)*scale;seg.z3=0.0;
                   g_array_append_val(path, seg);
                   count=0;
                   last_x=(p1->x)*scale;
                   last_y=(p1->y)*scale;
                 }
               else if(count==2)
                 {
                   v_pointx=((((p1-1)->x)+(p1->x))/2)*scale;
                   v_pointy=((((p1-1)->y)+(p1->y))/2)*scale;
                   q1_x=(last_x+2.0*((p1-1)->x)*scale)/3.0;
	           q1_y=(last_y+2.0*((p1-1)->y)*scale)/3.0;
                   q2_x=(v_pointx+2.0*((p1-1)->x)*scale)/3.0;
	           q2_y=(v_pointy+2.0*((p1-1)->y)*scale)/3.0;
                   seg.move_id=CURVE_TO;seg.x1=q1_x;seg.y1=q1_y;seg.z1=0.0;
                   seg.x2=q2_x;seg.y2=q2_y;seg.z2=0.0;
                   seg.x3=v_pointx;seg.y3=v_pointy;seg.z3=0.0;
                   g_array_append_val(path, seg);
                   count--;
                   last_x=v_pointx;
                   last_y=v_pointy;
                 }
               else
                 {
                   seg.move_id=LINE_TO;seg.x1=0.0;seg.y1=0.0;seg.z1=0.0;
                   seg.x2=0.0;seg.y2=0.0;seg.z2=0.0;
                   seg.x3=(p1->x)*scale;seg.y3=(p1->y)*scale;seg.z3=0.0;
                   g_array_append_val(path, seg);
                   count=0;
                   last_x=(p1->x)*scale;
                   last_y=(p1->y)*scale;
                 }
               draw=FALSE;           
             }

           //End drawing the countour.
           if(j==contour_points-1)
             {
               if(count==1)
                 {
                   q1_x=(last_x+2.0*(p1->x)*scale)/3.0;
	           q1_y=(last_y+2.0*(p1->y)*scale)/3.0;
                   q2_x=(c_startx+2.0*(p1->x)*scale)/3.0;
	           q2_y=(c_starty+2.0*(p1->y)*scale)/3.0;
                   seg.move_id=CURVE_TO_STROKE;seg.x1=q1_x;seg.y1=q1_y;seg.z1=0.0;
                   seg.x2=q2_x;seg.y2=q2_y;seg.z2=0.0;
                   seg.x3=c_startx;seg.y3=c_starty;seg.z3=0.0;
                   g_array_append_val(path, seg);
                 }
               else if(count==2)
                 {
                   v_pointx=((((p1-1)->x)+(p1->x))/2)*scale;
                   v_pointy=((((p1-1)->y)+(p1->y))/2)*scale;
                   q1_x=(last_x+2.0*((p1-1)->x)*scale)/3.0;
	           q1_y=(last_y+2.0*((p1-1)->y)*scale)/3.0;
                   q2_x=(v_pointx+2.0*((p1-1)->x)*scale)/3.0;
	           q2_y=(v_pointy+2.0*((p1-1)->y)*scale)/3.0;
                   seg.move_id=CURVE_TO_STROKE;seg.x1=q1_x;seg.y1=q1_y;seg.z1=0.0;                  
                   seg.x2=q2_x;seg.y2=q2_y;seg.z2=0.0;
                   seg.x3=v_pointx;seg.y3=v_pointy;seg.z3=0.0;
                   g_array_append_val(path, seg);
                   q1_x=(v_pointx+2.0*(p1->x)*scale)/3.0;
	           q1_y=(v_pointy+2.0*(p1->y)*scale)/3.0;
                   q2_x=(c_startx+2.0*(p1->x)*scale)/3.0;
	           q2_y=(c_starty+2.0*(p1->y)*scale)/3.0;
                   seg.move_id=CURVE_TO_STROKE;seg.x1=q1_x;seg.y1=q1_y;seg.z1=0.0;                   
                   seg.x2=q2_x;seg.y2=q2_y;seg.z2=0.0;
                   seg.x3=c_startx;seg.y3=c_starty;seg.z3=0.0;
                   g_array_append_val(path, seg);
                 }
               else
                 {
                   seg.move_id=LINE_TO;seg.x1=0.0;seg.y1=0.0;seg.z1=0.0;
                   seg.x2=0.0;seg.y2=0.0;seg.z2=0.0;
                   seg.x3=(p1->x)*scale;seg.y3=(p1->y)*scale;seg.z3=0.0;
                   g_array_append_val(path, seg);
                   seg.move_id=LINE_TO_STROKE;seg.x1=0.0;seg.y1=0.0;seg.z1=0.0;
                   seg.x2=0.0;seg.y2=0.0;seg.z2=0.0;
                   seg.x3=c_startx;seg.y3=c_starty;seg.z3=0.0;
                   g_array_append_val(path, seg);
                 }
                count=0.0;
                draw=FALSE;
              }

           p1++;c1++;
         }
     }

    /*
      Find the contour directions.
      https://stackoverflow.com/questions/1165647/how-to-determine-if-a-list-of-polygon-points-are-in-clockwise-order
      note "57 A minor caveat:" comment.
    */
    struct segment2 *s1=&g_array_index(path, struct segment2, 0);
    struct segment2 *s2=&g_array_index(path, struct segment2, 0);
    struct segment2 *s3=&g_array_index(path, struct segment2, 0);
    gdouble sum=0.0;
    for(i=0;i<(path->len);i++)
      {
        if((s1->move_id)==MOVE_TO_R)
          {
            s2=s1;
            s3=s1;
            sum=0.0;
          }
        else if((s1->move_id)==CURVE_TO_STROKE||(s1->move_id)==LINE_TO_STROKE)
          {
            while(s2!=s1)
              {
                sum+=((((s2+1)->x3)-(s2->x3))*(((s2+1)->y3)+(s2->y3)));
                s2++;
              }
            if(sum>=0.0) s3->move_id=MOVE_TO_L;
            else s3->move_id=MOVE_TO_R;
          }
        s1++;
      }

    exit:
    return path;
  }
static GArray* get_shadow_path(GArray *path, const gdouble light[3], const gdouble plane_distance, const gdouble qrs_plane[9])
 {
    gint i=0;
    gdouble delta_x=0.0;
    gdouble delta_y=0.0;
    gdouble delta_z=0.0;
    //For the normal vector to the plane.
    gdouble v1[3];
    gdouble v2[3];
    gdouble normal[3];
    gdouble unit=1.0;

    GArray *shadow=g_array_sized_new(FALSE, FALSE, sizeof(struct segment2), path->len);
    g_array_set_size(shadow, path->len);
    struct segment2 *p1=&g_array_index(path, struct segment2, 0);
    struct segment2 *p2=&g_array_index(shadow, struct segment2, 0);

    //A reference plane to stretch the shadow on.
    reset_plane_pts(qrs_plane);
    plane_pts_t[0]=plane_pts_t[0]-light[0]*plane_distance;
    plane_pts_t[1]=plane_pts_t[1]-light[1]*plane_distance;
    plane_pts_t[2]=plane_pts_t[2]-light[2]*plane_distance;
    plane_pts_t[3]=plane_pts_t[3]-light[0]*plane_distance;
    plane_pts_t[4]=plane_pts_t[4]-light[1]*plane_distance;
    plane_pts_t[5]=plane_pts_t[5]-light[2]*plane_distance;
    plane_pts_t[6]=plane_pts_t[6]-light[0]*plane_distance;
    plane_pts_t[7]=plane_pts_t[7]-light[1]*plane_distance;
    plane_pts_t[8]=plane_pts_t[8]-light[2]*plane_distance;

    //Get a normal vector.
    v1[0]=plane_pts_t[0]-plane_pts_t[3];
    v1[1]=plane_pts_t[1]-plane_pts_t[4];
    v1[2]=plane_pts_t[2]-plane_pts_t[5];
    v2[0]=plane_pts_t[0]-plane_pts_t[6];
    v2[1]=plane_pts_t[1]-plane_pts_t[7];
    v2[2]=plane_pts_t[2]-plane_pts_t[8];
    //Cross product.
    normal[0]=((v1[1]*v2[2])-(v1[2]*v2[1]));
    normal[1]=((v1[2]*v2[0])-(v1[0]*v2[2]));
    normal[2]=((v1[0]*v2[1])-(v1[1]*v2[0]));
    //For unit normal vector.
    unit=sqrt(normal[0]*normal[0]+normal[1]*normal[1]+normal[2]*normal[2]);
    normal[0]=normal[0]/unit;
    normal[1]=normal[1]/unit;
    normal[2]=normal[2]/unit;

    //From http://paulbourke.net/geometry/pointlineplane/
    gdouble num=(normal[0]*(plane_pts_t[0]-light[0]))+(normal[1]*(plane_pts_t[1]-light[1]))+(normal[2]*(plane_pts_t[2]-light[2]));
    gdouble den=1.0;
    gdouble u=1.0;
    if(num<0.0001&&num>-0.0001)
      {
        for(i=0;i<(path->len);i++)
          {
            (p2->move_id)=(p1->move_id);
            delta_x=light[0]-(p1->x1);
            delta_y=light[1]-(p1->y1); 
            delta_z=light[2]-(p1->z1); 
            (p2->x1)=(p1->x1)-delta_x;
            (p2->y1)=(p1->y1)-delta_y;
            (p2->z1)=(p1->z1)-delta_z;
            delta_x=light[0]-(p1->x2);
            delta_y=light[1]-(p1->y2);
            delta_z=light[2]-(p1->z2);  
            (p2->x2)=(p1->x2)-delta_x;
            (p2->y2)=(p1->y2)-delta_y;
            (p2->z2)=(p1->z2)-delta_z;
            delta_x=light[0]-(p1->x3);
            delta_y=light[1]-(p1->y3); 
            delta_z=light[2]-(p1->z3);  
            (p2->x3)=(p1->x3)-delta_x;
            (p2->y3)=(p1->y3)-delta_y;
            (p2->z3)=(p1->z3)-delta_z;
            p1++;p2++;
          }
      }
    else
      {
        for(i=0;i<(path->len);i++)
          {
            (p2->move_id)=(p1->move_id);
            den=(normal[0]*((p1->x1)-light[0]))+(normal[1]*((p1->y1)-light[1]))+(normal[2]*((p1->z1)-light[2]));
            if(den<1.0&&den>0.0) den=1.0;
            if(den>-1.0&&den<=0.0) den=-1.0;
            u=num/den;
            if(u<1.0) u=1.0; 
            (p2->x1)=light[0]+u*((p1->x1)-light[0]);
            (p2->y1)=light[1]+u*((p1->y1)-light[1]);
            (p2->z1)=light[2]+u*((p1->z1)-light[2]);;         
            den=(normal[0]*((p1->x2)-light[0]))+(normal[1]*((p1->y2)-light[1]))+(normal[2]*((p1->z2)-light[2]));
            if(den<1.0&&den>0.0) den=1.0;
            if(den>-1.0&&den<=0.0) den=-1.0;
            u=num/den;
            if(u<1.0) u=1.0; 
            (p2->x2)=light[0]+u*((p1->x2)-light[0]);
            (p2->y2)=light[1]+u*((p1->y2)-light[1]);
            (p2->z2)=light[2]+u*((p1->z2)-light[2]);; 
            den=(normal[0]*((p1->x3)-light[0]))+(normal[1]*((p1->y3)-light[1]))+(normal[2]*((p1->z3)-light[2]));
            if(den<1.0&&den>0.0) den=1.0;
            if(den>-1.0&&den<=0.0) den=-1.0;
            u=num/den;
            if(u<1.0) u=1.0;
            (p2->x3)=light[0]+u*((p1->x3)-light[0]);
            (p2->y3)=light[1]+u*((p1->y3)-light[1]);
            (p2->z3)=light[2]+u*((p1->z3)-light[2]);
            p1++;p2++;
          }
      }
   
    return shadow;
  }
static FT_Outline get_font_outline(FT_Face face, gint index, gdouble *box_w, gdouble *box_h)
  {
    gint i=0;
    FT_Error error;
    FT_GlyphSlot slot;
    FT_Outline outline;
    error=FT_Load_Char(face, text[index], FT_LOAD_NO_SCALE|FT_LOAD_NO_BITMAP);
    if(error) g_print("FT_Load_Char error.\n");
    slot=face->glyph;
    outline=slot->outline;

    //Invert points and letter.
    FT_Vector *p=outline.points;
    for(i=0;i<outline.n_points;i++)
      {
        (p->y)*=-1;
        p++;
      }

    //Get a bounding box.
    FT_BBox box;
    gdouble center_x=0.0;
    gdouble center_y=0.0;
    FT_Outline_Get_BBox(&outline, &box);
    center_x=((gdouble)box.xMax*scale-(gdouble)box.xMin*scale)/2.0;
    center_y=((gdouble)box.yMax*scale-(gdouble)box.yMin*scale)/2.0;
    if(box_w!=NULL) *box_w=((gdouble)box.xMax*scale-(gdouble)box.xMin*scale);
    if(box_h!=NULL) *box_h=((gdouble)box.yMax*scale-(gdouble)box.yMin*scale);

    //Center points for rotation around the center.
    p=outline.points;
    for(i=0;i<outline.n_points;i++)
      {
        (p->x)=(p->x)-center_x*1.0/scale-box.xMin;
        (p->y)=(p->y)+center_y*1.0/scale-box.yMax;
        p++;
      }

    return outline;
  }
static void quaternion_rotation(const gdouble yaw, const gdouble roll, const gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    gdouble roll1=roll/2.0;
    gdouble pitch1=pitch/2.0;
    gdouble yaw1=yaw/2.0;
    gdouble qi=sin(pitch1)*cos(roll1)*cos(yaw1)-cos(pitch1)*sin(roll1)*sin(yaw1);
    gdouble qj=cos(pitch1)*sin(roll1)*cos(yaw1)+sin(pitch1)*cos(roll1)*sin(yaw1);
    gdouble qk=cos(pitch1)*cos(roll1)*sin(yaw1)-sin(pitch1)*sin(roll1)*cos(yaw1);
    gdouble qr=cos(pitch1)*cos(roll1)*cos(yaw1)+sin(pitch1)*sin(roll1)*sin(yaw1);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
static void transform_letter(GArray *path, const gdouble qrs[9])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0;  
    gdouble cr4=0.0;
    gdouble cr5=0.0;
    gdouble cr6=0.0;   
    gdouble cr7=0.0;
    gdouble cr8=0.0;
    gdouble cr9=0.0;    
    struct segment2 *p1=NULL;

    p1=&g_array_index(path, struct segment2, 0);
    for(i=0;i<(path->len);i++)
      {
        cr1=(p1->x1);
        cr2=(p1->y1);
        cr3=(p1->z1); 
        cr4=(p1->x2);
        cr5=(p1->y2);
        cr6=(p1->z2);  
        cr7=(p1->x3);
        cr8=(p1->y3);
        cr9=(p1->z3);          
        p1->x1=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        p1->y1=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        p1->z1=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        p1->x2=(cr4*qrs[0])+(cr5*qrs[1])+(cr6*qrs[2]);
        p1->y2=(cr4*qrs[3])+(cr5*qrs[4])+(cr6*qrs[5]);
        p1->z2=(cr4*qrs[6])+(cr5*qrs[7])+(cr6*qrs[8]);
        p1->x3=(cr7*qrs[0])+(cr8*qrs[1])+(cr9*qrs[2]);
        p1->y3=(cr7*qrs[3])+(cr8*qrs[4])+(cr9*qrs[5]);
        p1->z3=(cr7*qrs[6])+(cr8*qrs[7])+(cr9*qrs[8]);
        p1++;
      }

    //Transform reference vector.
    cr1=v1[0];
    cr2=v1[1];
    cr3=v1[2];         
    v1_r[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
    v1_r[1]=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
    v1_r[2]=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
  }
static void matrix_multiply(const gdouble in1[9], const gdouble in2[9], gdouble out[9])
  {
    //Multiply two 3x3 matrices.
    out[0]=in1[0]*in2[0]+in1[3]*in2[1]+in1[6]*in2[2];
    out[1]=in1[1]*in2[0]+in1[4]*in2[1]+in1[7]*in2[2];
    out[2]=in1[2]*in2[0]+in1[5]*in2[1]+in1[8]*in2[2];

    out[3]=in1[0]*in2[3]+in1[3]*in2[4]+in1[6]*in2[5];
    out[4]=in1[1]*in2[3]+in1[4]*in2[4]+in1[7]*in2[5];
    out[5]=in1[2]*in2[3]+in1[5]*in2[4]+in1[8]*in2[5];

    out[6]=in1[0]*in2[6]+in1[3]*in2[7]+in1[6]*in2[8];
    out[7]=in1[1]*in2[6]+in1[4]*in2[7]+in1[7]*in2[8];
    out[8]=in1[2]*in2[6]+in1[5]*in2[7]+in1[8]*in2[8];
  }
static void reset_plane_pts(const gdouble qrs_plane[9])
  {
    gint i=0; 

    for(i=0;i<3;i++)
      {
        plane_pts_t[3*i]=plane_pts[3*i];
        plane_pts_t[3*i+1]=plane_pts[3*i+1];
        plane_pts_t[3*i+2]=plane_pts[3*i+2];
      }
    rotate_plane(qrs_plane);
  }
static void rotate_plane(const gdouble qrs_plane[9])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0;   

    for(i=0;i<3;i++)
      {
        cr1=plane_pts_t[3*i];
        cr2=plane_pts_t[3*i+1];
        cr3=plane_pts_t[3*i+2];;         
        plane_pts_t[3*i]=(cr1*qrs_plane[0])+(cr2*qrs_plane[1])+(cr3*qrs_plane[2]);
        plane_pts_t[3*i+1]=(cr1*qrs_plane[3])+(cr2*qrs_plane[4])+(cr3*qrs_plane[5]);
        plane_pts_t[3*i+2]=(cr1*qrs_plane[6])+(cr2*qrs_plane[7])+(cr3*qrs_plane[8]);
      }    
  }
int compare_quads(const void *q1, const void *q2)
  {
    return((((struct quad*)q1)->a.z3+((struct quad*)q1)->b.z3+((struct quad*)q1)->c.z3+((struct quad*)q1)->d.z3) - (((struct quad*)q2)->a.z3+((struct quad*)q2)->b.z3+((struct quad*)q2)->c.z3+((struct quad*)q2)->d.z3));
  }



