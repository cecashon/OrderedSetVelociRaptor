
/*   
    Start testing some ideas for a 3d histogram with perspective. For comparison with histogram_chart1.
This uses a 4x4 matrix where histogram_chart1.c uses a 3x3 matrix. Draw a 3d line, smooth line, points,
cylinders and cubes.
    Test drawing output to .svg, .pdf and .ps. Helpful for outputting and printing a 3d histogram. For 
example drawing a 3d microtiter plate.
    For drawing, there are two arrays for the points to be drawn. One is the original points and the other is the transformed points. For each draw, the original points array gets copied to the transformed points 
array. Then the points are transformed and possibly sorted before being drawn.

    gcc -Wall histogram_chart2.c -o histogram_chart2 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu16.04 and GTK3.18 32-bit.

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#include<cairo-svg.h>
#include<cairo-pdf.h>
#include<cairo-ps.h>

struct point3d{
  gdouble x;
  gdouble y;
  gdouble z;
};

struct color{
  gdouble r;
  gdouble g;
  gdouble b;
  gdouble a;
};

/*
  For drawing a bezier ring. A little wasteful to have a color for each arc to color a cylinder
  but it works.
*/
struct arc{
  struct point3d a1;
  struct point3d a2;
  struct point3d a3;
  struct point3d a4;
  struct color cl;
};

/*
  A definition for sorting cylinders with two rings of 8 before drawing on the chart. If
8 point rings are not good enough then a different struct along with a qsort compare function
needs to be used. Use aligned memory for this.
*/
struct cylinder16{
  struct arc ar1;
  struct arc ar2;
  struct arc ar3;
  struct arc ar4;
  struct arc ar5;
  struct arc ar6;
  struct arc ar7;
  struct arc ar8;
  struct arc ar9;
  struct arc ar10;
  struct arc ar11;
  struct arc ar12;
  struct arc ar13;
  struct arc ar14;
  struct arc ar15;
  struct arc ar16;
};

//For sorting 24 arcs or 2 rings of 12 for the cylinder.
struct cylinder24{
  struct arc ar1;
  struct arc ar2;
  struct arc ar3;
  struct arc ar4;
  struct arc ar5;
  struct arc ar6;
  struct arc ar7;
  struct arc ar8;
  struct arc ar9;
  struct arc ar10;
  struct arc ar11;
  struct arc ar12;
  struct arc ar13;
  struct arc ar14;
  struct arc ar15;
  struct arc ar16;
  struct arc ar17;
  struct arc ar18;
  struct arc ar19;
  struct arc ar20;
  struct arc ar21;
  struct arc ar22;
  struct arc ar23;
  struct arc ar24;
};

//For getting bezier control points for an array of point3d's. Just need the 2d points.
struct controls2d{
  gdouble x1;
  gdouble y1;
  gdouble x2;
  gdouble y2;
};

struct rectangle3d{
  struct point3d b1;
  struct point3d b2;
  struct point3d b3;
  struct point3d b4;
  struct color cl;
};

struct cube3d{
  struct point3d c1;
  struct point3d c2;
  struct point3d c3;
  struct point3d c4;
  struct point3d c5;
  struct point3d c6;
  struct point3d c7;
  struct point3d c8;
  struct color cl;
};

//Basic layout for 2d and 3d charting.
struct histogram_chart{ 
   guint rows;
   guint columns; 
   gdouble outside_square;
   GArray *base;
   GArray *base_t;
   //The z height of the side and back rows.
   gdouble back_height;
   guint back_rows;
   gdouble back_color[4];
   //Reference vectors for x, y and z axis.
   gdouble rx[3];
   gdouble rx_t[3];
   gdouble ry[3];
   gdouble ry_t[3];
   gdouble rz[3];
   gdouble rz_t[3];
   //The three z corners of the chart.
   gdouble c1[3];
   gdouble c1_t[3];
   gdouble c2[3];
   gdouble c2_t[3];
   gdouble c3[3];
   gdouble c3_t[3];
};

//Data point arrays for the chart. Separate the data from the chart. Test various shapes.
struct chart_data{
   gint data_id;
   gdouble max_z;
   GArray *d;
   GArray *d_t;
};

//For the cylinders. The cylinders need aligned memory for the sorts of different block sizes.
struct chart_data_aligned{
   gint data_id;
   gint ring_size;
   gint len;
   gdouble max_z;
   struct arc *d;
   struct arc *d_t;
};

//Position the histogram cube.
enum{ 
  CUBE_UPPER_LEFT,
  CUBE_CENTER
};

//Draw options for histogram cubes.
enum{ 
  SOLID_CUBES,
  GRADIENT_CUBES,
  GRADIENT_CUBES2
};

//Draw options for histogram cylinders.
enum{ 
  SOLID_CYLINDERS,
  GRADIENT_CYLINDERS,
};

//Draw options for data points.
enum{ 
  DRAW_POINTS,
  DRAW_LINES,
  DRAW_SMOOTH
};

//Some settings for the UI.
static gint drawing_id=0;
static gint chart_rows=9;
static gint chart_columns=11;
static gint chart_index=0;
static gdouble zvalue=0.0;
static gdouble chart_rgba[4]={0.0, 0.0, 0.0, 1.0};
static gdouble pitch=0.0;
static gdouble roll=0.0;
static gdouble yaw=0.0;
static gdouble scale=1.0;
static gdouble perspective=0.0;
static guint tick_id=0;
static gboolean frame_rate=FALSE;
static gboolean time_it=FALSE;
static guint draw_signal_id=0;

//The UI part.
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data);
static void set_animate_drawing(GtkComboBox *combo, gpointer *parts);
static void rows_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void columns_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void set_chart(GtkWidget *button, gpointer *parts);
static void index_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void zvalue_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void set_zvalue(GtkWidget *button, gpointer *parts);
static void red_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void green_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void blue_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void set_bar_color(GtkWidget *button, gpointer *parts);
static void set_base_color(GtkWidget *button, gpointer *parts);
static void set_back_color(GtkWidget *button, gpointer *parts);
static void back_height_spin_changed(GtkSpinButton *spin_button, gpointer *parts);
static void back_rows_spin_changed(GtkSpinButton *spin_button, gpointer *parts);
static void set_drawing_id(GtkComboBox *combo, gpointer da);
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void show_frame_rate(GtkToggleButton *check_frame, gpointer data);
static void show_drawing_time(GtkToggleButton *check_time, gpointer data);
static gboolean animate_drawing(GtkWidget *da, GdkFrameClock *frame_clock, gpointer *parts);
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer *parts);
static void draw_svg(GtkWidget *widget, gpointer *parts);
static void draw_pdf(GtkWidget *widget, gpointer *parts);
static void draw_ps(GtkWidget *widget, gpointer *parts);
static void draw_surface(cairo_t *cr, gdouble width, gdouble height, gpointer *parts);

//Initialize a histogram chart with some cubes, cylinders and data to plot.
static struct histogram_chart* initialize_histogram_chart(guint rows, guint columns, gdouble outside_square);
static struct chart_data* initialize_cubes(struct histogram_chart *hc, gdouble inside_square, guint inside_square_position);
static struct chart_data* initialize_spiral(struct histogram_chart *hc, gint points);
static struct chart_data* initialize_sphere(gint points);
static struct chart_data_aligned* initialize_cylinders(struct histogram_chart *hc, gint sections, gdouble radius);

//A new basic chart. This is the bottom checkerboard and sides of the chart.
static struct histogram_chart* histogram_chart_new(guint rows, guint columns, gdouble outside_square);
static void initialize_base(struct histogram_chart *hc, guint rows, guint columns, gdouble outside_square);
static void histogram_chart_set_base_color(struct histogram_chart *hc, guint index, gdouble rgba[4]);
static void histogram_chart_set_back_height(struct histogram_chart *hc, gdouble height);
static void histogram_chart_free(struct histogram_chart *hc);
//A new cube chart. The cube data to draw.
static struct chart_data* chart_cubes_new(struct histogram_chart *hc, gdouble inside_square, guint inside_square_position);
static void cube_chart_set_bar_color(struct chart_data *cd, guint index, gdouble rgba[4]);
static void cube_chart_set_bar_height(struct chart_data *cd, guint index, gdouble height);
static double cube_chart_get_bar_height(struct chart_data *cd, guint index);
static void cube_chart_free(struct chart_data *cd);
//A new cylinder chart. The cylinder data to draw.
static struct chart_data_aligned* chart_cylinders_new(struct histogram_chart *hc, gint sections, gdouble radius);
static void cylinder_chart_set_bar_color(struct chart_data_aligned *cd, guint index, gdouble rgba[4]);
static void cylinder_chart_set_bar_height(struct chart_data_aligned *cd, guint index, gdouble height);
static double cylinder_chart_get_bar_height(struct chart_data_aligned *cd, guint index);
static void cylinder_chart_free(struct chart_data_aligned *cd);
//Draw the basic chart.
static void histogram_chart_draw_base(struct histogram_chart *hc, cairo_t *cr);
static void histogram_chart_draw_numbers_left(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[16]);
static void histogram_chart_draw_numbers_right(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[16]);
static void histogram_chart_draw_numbers_z(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[16]);
static void histogram_chart_draw_back(struct histogram_chart *hc, cairo_t *cr);
static void histogram_chart_draw_side(struct histogram_chart *hc, cairo_t *cr);
static void histogram_chart_reset_base();
//Draw the bars or cubes.
static void cube_chart_draw(struct histogram_chart *hc, struct chart_data *cd, cairo_t *cr, gdouble qrs[16], guint draw_type);
//Line, smooth line and points charts.
static void chart_data_draw(struct histogram_chart *hc, struct chart_data *cd, cairo_t *cr, gdouble qrs[16], gint draw_option);
//Draw the cylinders.
static void chart_data_draw_aligned(struct histogram_chart *hc, struct chart_data_aligned *cd, cairo_t *cr, gdouble qrs[16], gint draw_option);
//Draw the shapes.
static void chart_data_draw_cubes(struct chart_data *cd, cairo_t *cr, gdouble chart_z);
static void chart_data_draw_cubes_grad(struct chart_data *cd, cairo_t *cr, gdouble chart_z);
static void chart_data_draw_cubes_grad2(struct chart_data *cd, cairo_t *cr, gdouble chart_z);
static void chart_data_draw_lines(struct chart_data *cd, cairo_t *cr);
static void chart_data_draw_smooth(struct chart_data *cd, cairo_t *cr);
static void chart_data_draw_points(struct chart_data *cd, cairo_t *cr);
static void chart_data_draw_cylinders(struct chart_data_aligned *cd, cairo_t *cr, gdouble chart_z);
static void chart_data_draw_cylinders_grad(struct chart_data_aligned *cd, cairo_t *cr, gdouble chart_z);
static void chart_data_reset_cubes(struct chart_data *cd);
static void chart_data_reset(struct chart_data *cd);
static void chart_data_reset_aligned(struct chart_data_aligned *cd);
//3d transform functions.
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[16]);
static void histogram_chart_rotate_base(struct histogram_chart *hc, gdouble qrs[16]);
static void chart_data_rotate_cubes(struct chart_data *cd, gdouble qrs[16]);
static void chart_data_rotate(struct chart_data *cd, gdouble qrs[16]);
static void chart_data_rotate_aligned(struct chart_data_aligned *cd, gdouble qrs[16]);
int compare_cubes(const void *a, const void *b);
static void set_projection_matrix(gdouble qrs[16], gdouble angle, gdouble near, gdouble far);
static void matrix_multiply(const gdouble in1[16], const gdouble in2[16], gdouble out[16]);
//Get bezier control points for curve coordinates.
static GArray* control_points_from_coords_3d(const GArray *data_points3d);
//Generate bezier points and coordinates for a ring.
static void initialize_bezier_ring(struct histogram_chart *hc, struct arc **ring, struct arc **ring_t, gint sections, gdouble radius, gdouble max_z);
int compare_cylinders16(const void *a, const void *b);
int compare_cylinders24(const void *a, const void *b);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Histogram Chart3d");
    gtk_window_set_default_size(GTK_WINDOW(window), 875, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_widget_set_app_paintable(window, TRUE);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    /*
      Initialize a histogram chart. The outside_square is the base square size and the inside_square
      is the bar square size. The bar square is smaller than or equal to the base square.
    */
    guint rows=9;
    guint columns=11;
    gdouble outside_square=40.0;
    gdouble inside_square=20.0;
    struct histogram_chart *hc=initialize_histogram_chart(rows, columns, outside_square);

    struct chart_data *cubes=initialize_cubes(hc, inside_square, CUBE_CENTER);

    //Initialize arrays for spirals.
    struct chart_data *spiral=initialize_spiral(hc, rows);

    //Initialize array for sphere points.
    struct chart_data *sphere=initialize_sphere(150);

    /*
       Initialize arrays for cylinders. This is hard coded for 8 or 12 sides. To change from 8 or 12 the
       qsort and the compare functions need to be changed! Use aligned memory for the compare
       function.
    */
    struct chart_data_aligned *cylinders=initialize_cylinders(hc, 8, 12.0);

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);
    gpointer parts[]={da, hc, spiral, sphere, cylinders, cubes};
    draw_signal_id=g_signal_connect(da, "draw", G_CALLBACK(draw_main), parts); 

    GtkWidget *combo_animate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_animate, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_animate), 0, "1", "Draw Chart");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_animate), 1, "2", "Animate Chart");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_animate), 0);
    g_signal_connect(combo_animate, "changed", G_CALLBACK(set_animate_drawing), parts);

    GtkWidget *combo_drawing=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_drawing, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "1", "Histogram Chart3d");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "2", "Histogram Chart3d Grad");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 2, "3", "Histogram Chart3d Grad2");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 3, "4", "Data Chart3d");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 4, "5", "Smooth Data Chart3d");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 5, "6", "Sphere Points");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 6, "7", "Cylinders");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 7, "8", "Cylinders Grad");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
    g_signal_connect(combo_drawing, "changed", G_CALLBACK(set_drawing_id), da);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);

    GtkWidget *scale_label=gtk_label_new("Scale");
    gtk_widget_set_hexpand(scale_label, TRUE);

    GtkWidget *perspective_label=gtk_label_new("Perspective");
    gtk_widget_set_hexpand(perspective_label, TRUE);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider, TRUE);
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_pitch), da);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider, TRUE);
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_roll), da);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider, TRUE);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_yaw), da);  

    GtkWidget *scale_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 2.0, 0.01);
    gtk_widget_set_vexpand(scale_slider, TRUE);
    gtk_range_set_value(GTK_RANGE(scale_slider), 1.0);
    g_signal_connect(scale_slider, "change-value", G_CALLBACK(set_scale), da);

    GtkWidget *perspective_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 1.0, 0.01);
    gtk_widget_set_vexpand(perspective_slider, TRUE);
    g_signal_connect(perspective_slider, "change-value", G_CALLBACK(set_perspective), da);

    GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
    gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
    g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), da);

    GtkWidget *check_time=gtk_check_button_new_with_label("Time Drawing");
    gtk_widget_set_halign(check_time, GTK_ALIGN_CENTER);
    g_signal_connect(check_time, "toggled", G_CALLBACK(show_drawing_time), da);

    GtkWidget *menu1=gtk_menu_new();
    GtkWidget *menu1item1=gtk_menu_item_new_with_label("histo1.svg");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item1);
    GtkWidget *menu1item2=gtk_menu_item_new_with_label("histo1.pdf");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item2);
    GtkWidget *menu1item3=gtk_menu_item_new_with_label("histo1.ps");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item3);
    GtkWidget *title1=gtk_menu_item_new_with_label("Test Output");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(title1), menu1);

    GtkWidget *menu_bar=gtk_menu_bar_new();
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), title1);

    g_signal_connect(menu1item1, "activate", G_CALLBACK(draw_svg), parts);
    g_signal_connect(menu1item2, "activate", G_CALLBACK(draw_pdf), parts);
    g_signal_connect(menu1item3, "activate", G_CALLBACK(draw_ps), parts);
   
    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 8);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_animate, 0, 0, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_drawing, 0, 1, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale_label, 3, 2, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid1), perspective_label, 4, 2, 1, 1);     
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale_slider, 3, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), perspective_slider, 4, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_frame, 0, 4, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_time, 0, 5, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), menu_bar, 0, 6, 4, 1);

    //Second page of notebook. 
    GtkAdjustment *row_adj=gtk_adjustment_new(9.0, 1.0, 20.0, 1.0, 0.0, 0.0);
    GtkAdjustment *column_adj=gtk_adjustment_new(11.0, 1.0, 20.0, 1.0, 0.0, 0.0);
    GtkAdjustment *index_adj=gtk_adjustment_new(0.0, 0.0, 400.0, 1.0, 0.0, 0.0);
    GtkAdjustment *zvalue_adj=gtk_adjustment_new(0.0, 0.0, 200.0, 1.0, 0.0, 0.0);
    GtkAdjustment *red_adj=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *green_adj=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *blue_adj=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);

    GtkWidget *rows_label=gtk_label_new("Rows");
    GtkWidget *rows_spin=gtk_spin_button_new(row_adj, 1, 0);
    g_signal_connect(rows_spin, "value-changed", G_CALLBACK(rows_spin_changed), NULL);

    GtkWidget *columns_label=gtk_label_new("Columns");
    GtkWidget *columns_spin=gtk_spin_button_new(column_adj, 1, 0);
    g_signal_connect(columns_spin, "value-changed", G_CALLBACK(columns_spin_changed), NULL);

    GtkWidget *rc_button=gtk_button_new_with_label("Update RC");
    g_signal_connect(rc_button, "clicked", G_CALLBACK(set_chart), parts);

    GtkWidget *index_label=gtk_label_new("Index");
    GtkWidget *index_spin=gtk_spin_button_new(index_adj, 1, 0);
    g_signal_connect(index_spin, "value-changed", G_CALLBACK(index_spin_changed), NULL);

    GtkWidget *zvalue_label=gtk_label_new("Z Value");
    GtkWidget *zvalue_spin=gtk_spin_button_new(zvalue_adj, 1, 0);
    g_signal_connect(zvalue_spin, "value-changed", G_CALLBACK(zvalue_spin_changed), NULL);

    GtkWidget *zvalue_button=gtk_button_new_with_label("Update Z-value");
    g_signal_connect(zvalue_button, "clicked", G_CALLBACK(set_zvalue), parts);

    GtkWidget *rgb_label=gtk_label_new("RGB");
    GtkWidget *red_spin=gtk_spin_button_new(red_adj, 0.01, 2);
    g_signal_connect(red_spin, "value-changed", G_CALLBACK(red_spin_changed), NULL);

    GtkWidget *green_spin=gtk_spin_button_new(green_adj, 0.01, 2);
    g_signal_connect(green_spin, "value-changed", G_CALLBACK(green_spin_changed), NULL);

    GtkWidget *blue_spin=gtk_spin_button_new(blue_adj, 0.01, 2);
    g_signal_connect(blue_spin, "value-changed", G_CALLBACK(blue_spin_changed), NULL);

    GtkWidget *bar_color_button=gtk_button_new_with_label("Update Bar Color");
    g_signal_connect(bar_color_button, "clicked", G_CALLBACK(set_bar_color), parts);

    GtkWidget *base_color_button=gtk_button_new_with_label("Update Base Color");
    g_signal_connect(base_color_button, "clicked", G_CALLBACK(set_base_color), parts);

    GtkWidget *back_color_button=gtk_button_new_with_label("Update Back Color");
    g_signal_connect(back_color_button, "clicked", G_CALLBACK(set_back_color), parts);

    GtkWidget *grid2=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid2), 8);
    gtk_grid_set_row_spacing(GTK_GRID(grid2), 8);
    gtk_grid_attach(GTK_GRID(grid2), rows_label, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), columns_label, 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), rows_spin, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), columns_spin, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), rc_button, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), index_label, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), zvalue_label, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), index_spin, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), zvalue_spin, 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), zvalue_button, 1, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), rgb_label, 0, 6, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid2), red_spin, 0, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), green_spin, 1, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), blue_spin, 0, 8, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), bar_color_button, 1, 8, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), base_color_button, 1, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), back_color_button, 1, 10, 1, 1);

    GtkAdjustment *back_height_adj=gtk_adjustment_new(200.0, 0.0, 400.0, 10.0, 0.0, 0.0);
    GtkAdjustment *back_rows_adj=gtk_adjustment_new(10.0, 0.0, 20.0, 1.0, 0.0, 0.0);
   
    GtkWidget *back_height_label=gtk_label_new("Back Height");
    GtkWidget *back_height_spin=gtk_spin_button_new(back_height_adj, 100.0, 1);
    g_signal_connect(back_height_spin, "value-changed", G_CALLBACK(back_height_spin_changed), parts);

    GtkWidget *back_rows_label=gtk_label_new("Back Rows");
    GtkWidget *back_rows_spin=gtk_spin_button_new(back_rows_adj, 10.0, 0);
    g_signal_connect(back_rows_spin, "value-changed", G_CALLBACK(back_rows_spin_changed), parts);

    GtkWidget *grid3=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid3), 8);
    gtk_grid_set_row_spacing(GTK_GRID(grid3), 8);
    gtk_grid_attach(GTK_GRID(grid3), back_height_label, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), back_rows_label, 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), back_height_spin, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), back_rows_spin, 1, 1, 1, 1);

    GtkWidget *nb_label1=gtk_label_new("Drawing");
    GtkWidget *nb_label2=gtk_label_new("Options");
    GtkWidget *nb_label3=gtk_label_new("Options2");
    GtkWidget *notebook=gtk_notebook_new();
    gtk_container_set_border_width(GTK_CONTAINER(notebook), 15);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), grid1, nb_label1);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), grid2, nb_label2);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), grid3, nb_label3);

    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), notebook, FALSE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 350);

    //Draw background window based on the paned window splitter.
    g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), paned1);

    gtk_container_add(GTK_CONTAINER(window), paned1);

    gtk_widget_show_all(window);

    gtk_main();

    //Clean up. Free the copied hc pointer in the array since it may have changed.
    histogram_chart_free((struct histogram_chart*)parts[1]);
   
    g_array_free(spiral->d, TRUE);
    g_array_free(spiral->d_t, TRUE);
    g_free(spiral);

    g_array_free(sphere->d, TRUE);
    g_array_free(sphere->d_t, TRUE);
    g_free(sphere);

    //Free copy of cylinder since it might be changed.
    cylinder_chart_free((struct chart_data_aligned*)parts[4]);

    cube_chart_free((struct chart_data*)parts[5]);

    return 0;  
  }
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data)
  {
    //Paint background of drawing area.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr);
    //Paint the background under the grid.
    cairo_set_source_rgba(cr, 0.0, 0.5, 1.0, 0.5);
    gint width=gtk_paned_get_position(GTK_PANED(data));
    gint height=gtk_widget_get_allocated_height(widget);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_rectangle(cr, width, 0.0, 10, height);
    cairo_fill(cr);
    return FALSE;
  }
static void set_animate_drawing(GtkComboBox *combo, gpointer *parts)
  {   
    pitch=0.0;
    roll=0.0;
    yaw=0.0;
 
    if(gtk_combo_box_get_active(combo)==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
          }
      }
    
  }
static void rows_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_rows=gtk_spin_button_get_value_as_int(spin_button);
  }
static void columns_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_columns=gtk_spin_button_get_value_as_int(spin_button);
  }
static void set_chart(GtkWidget *button, gpointer *parts)
  {
    gboolean was_ticking=FALSE;

    //Block the draw and tick when updating the array.
    g_signal_handler_block(parts[0], draw_signal_id);
    if(tick_id!=0)
      {
        gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
        tick_id=0;
        was_ticking=TRUE;
      }

    //Free the previous chart and cylinders.
    if(((struct histogram_chart*)parts[1])!=NULL)
      {
        histogram_chart_free((struct histogram_chart*)parts[1]);
      }

    if(((struct chart_data_aligned*)parts[4])!=NULL)
      {
        cylinder_chart_free((struct chart_data_aligned*)parts[4]);
      }

    if(((struct chart_data*)parts[5])!=NULL)
      {
        cube_chart_free((struct chart_data*)parts[5]);
      }

    //Get a new chart and reinitialize cylinders.    
    parts[1]=initialize_histogram_chart(chart_rows, chart_columns, 40.0);
    parts[4]=initialize_cylinders((struct histogram_chart*)parts[1], 8, 12.0);
    parts[5]=initialize_cubes((struct histogram_chart*)parts[1], 20, CUBE_CENTER);

    g_signal_handler_unblock(parts[0], draw_signal_id);
    if(was_ticking)
      {
        tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
      }

    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static void index_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_index=gtk_spin_button_get_value_as_int(spin_button);
  }
static void zvalue_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    zvalue=gtk_spin_button_get_value(spin_button);
  }
static void set_zvalue(GtkWidget *button, gpointer *parts)
  {
    struct histogram_chart *hc=parts[1];
    gint total=(hc->rows)*(hc->columns)-1;

    if(chart_index<0||chart_index>total)
      {
        g_warning("Index out of range. The bar height wasn't set.\n");
      }
    else
      {
        //Block the draw and tick when updating the array.
        gboolean was_ticking=FALSE;
        g_signal_handler_block(parts[0], draw_signal_id);
        if(tick_id!=0)
          {
            gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
            tick_id=0;
            was_ticking=TRUE;
          }

        cube_chart_set_bar_height((struct chart_data*)parts[5], chart_index, zvalue);
        cylinder_chart_set_bar_height((struct chart_data_aligned*)parts[4], chart_index, zvalue);

        g_signal_handler_unblock(parts[0], draw_signal_id);
        if(was_ticking)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
          }

        gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
      }
  }
static void red_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_rgba[0]=gtk_spin_button_get_value(spin_button);
  }
static void green_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_rgba[1]=gtk_spin_button_get_value(spin_button);
  }
static void blue_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_rgba[2]=gtk_spin_button_get_value(spin_button);
  }
static void set_bar_color(GtkWidget *button, gpointer *parts)
  {
    struct histogram_chart *hc=parts[1];
    gint total=(hc->rows)*(hc->columns)-1;

    if(chart_index<0||chart_index>total)
      {
        g_warning("Index out of range. The bar height wasn't set.\n");
      }
    else
      {
        //Block the draw and tick when updating the array.
        gboolean was_ticking=FALSE;
        g_signal_handler_block(parts[0], draw_signal_id);
        if(tick_id!=0)
          {
            gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
            tick_id=0;
            was_ticking=TRUE;
          }

        cube_chart_set_bar_color((struct chart_data*)parts[5], chart_index, chart_rgba);
        cylinder_chart_set_bar_color((struct chart_data_aligned*)parts[4], chart_index, chart_rgba);

        g_signal_handler_unblock(parts[0], draw_signal_id);
        if(was_ticking)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
          }

        gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
      }
  }
static void set_base_color(GtkWidget *button, gpointer *parts)
  {
    struct histogram_chart *hc=parts[1];
    gint total=(hc->rows)*(hc->columns)-1;

    if(chart_index<0||chart_index>total)
      {
        g_warning("Index out of range. The base height wasn't set.\n");
      }
    else
      {
        //Block the draw and tick when updating the array.
        gboolean was_ticking=FALSE;
        g_signal_handler_block(parts[0], draw_signal_id);
        if(tick_id!=0)
          {
            gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
            tick_id=0;
            was_ticking=TRUE;
          }

        histogram_chart_set_base_color(hc, chart_index, chart_rgba);

        g_signal_handler_unblock(parts[0], draw_signal_id);
        if(was_ticking)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
          }

        gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
      }
  }
static void set_back_color(GtkWidget *button, gpointer *parts)
  {
    struct histogram_chart *hc=parts[1];
    gboolean was_ticking=FALSE;

    g_signal_handler_block(parts[0], draw_signal_id);
    if(tick_id!=0)
      {
        gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
        tick_id=0;
        was_ticking=TRUE;
      }

    //Update back_height and reference points.
    hc->back_color[0]=chart_rgba[0];
    hc->back_color[1]=chart_rgba[1];
    hc->back_color[2]=chart_rgba[2];
    hc->back_color[3]=chart_rgba[3];   

    g_signal_handler_unblock(parts[0], draw_signal_id);
    if(was_ticking)
      {
        tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
      }

    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static void back_height_spin_changed(GtkSpinButton *spin_button, gpointer *parts)
  {
    struct histogram_chart *hc=parts[1];
    gboolean was_ticking=FALSE;

    g_signal_handler_block(parts[0], draw_signal_id);
    if(tick_id!=0)
      {
        gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
        tick_id=0;
        was_ticking=TRUE;
      }

    //Update back_height and reference points.
    hc->back_height=gtk_spin_button_get_value(spin_button);
    hc->c1[2]=(hc->back_height);
    hc->c1_t[2]=(hc->back_height);
    hc->c2[2]=(hc->back_height);
    hc->c2_t[2]=(hc->back_height);
    hc->c3[2]=(hc->back_height);
    hc->c3_t[2]=(hc->back_height);

    g_signal_handler_unblock(parts[0], draw_signal_id);
    if(was_ticking)
      {
        tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
      }

    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static void back_rows_spin_changed(GtkSpinButton *spin_button, gpointer *parts)
  {
    struct histogram_chart *hc=parts[1];
    gboolean was_ticking=FALSE;

    g_signal_handler_block(parts[0], draw_signal_id);
    if(tick_id!=0)
      {
        gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
        tick_id=0;
        was_ticking=TRUE;
      }

    (hc->back_rows)=gtk_spin_button_get_value_as_int(spin_button);

    g_signal_handler_unblock(parts[0], draw_signal_id);
    if(was_ticking)
      {
        tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
      }

    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static void set_drawing_id(GtkComboBox *combo, gpointer da)
  {  
    drawing_id=gtk_combo_box_get_active(combo);

    gtk_widget_queue_draw(GTK_WIDGET(da));
  }
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    pitch=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    roll=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    yaw=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>2.0) scale=2.0;
    else if(value<0.0) scale=0.0;
    else scale=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value<0.0) perspective=0.0;
    else if(value>100.0) perspective=100.0;
    else perspective=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void show_frame_rate(GtkToggleButton *check_frame, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_frame)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_drawing_time(GtkToggleButton *check_time, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_time)) time_it=TRUE;
    else time_it=FALSE;
  }
static gboolean animate_drawing(GtkWidget *da, GdkFrameClock *frame_clock, gpointer *parts)
  {
    gint i=0;
    gdouble z=0.0;

    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    if(pitch>360.0) pitch=0.0;
    else pitch+=0.5;
    if(roll>360.0) roll=0.0;
    else roll+=0.5;
    if(yaw>360.0) yaw=0.0;
    else yaw+=0.5;

    //Change bar heights and reset max.
    if(drawing_id==0||drawing_id==1||drawing_id==2)
      {
        struct chart_data *cd=parts[5];
        cd->max_z=0.0;
        for(i=0;i<(cd->d->len);i++)
          {
            z=cube_chart_get_bar_height(cd, i)+2.0;
            if(z>200.0) cube_chart_set_bar_height(cd, i, 0.0);
            else cube_chart_set_bar_height(cd, i, z);
            if((cd->max_z)<z) (cd->max_z)=z;
          }
       }
    else if(drawing_id==6||drawing_id==7)
      {
        struct chart_data_aligned *cd=parts[4];
        gint cylinders=(cd->len)/((cd->ring_size)*2);
        for(i=0;i<cylinders;i++)
          {
            z=cylinder_chart_get_bar_height(cd, i)+2.0;
            if(z>200.0) cylinder_chart_set_bar_height(cd, i, 0.0);
            else cylinder_chart_set_bar_height(cd, i, z);
            if((cd->max_z)<z) (cd->max_z)=z;
          }
      }
    
    gtk_widget_queue_draw(GTK_WIDGET(da));
    return G_SOURCE_CONTINUE;
  }
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer *parts)
  {  
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;
    struct histogram_chart *hc=parts[1];
    struct chart_data *spiral=parts[2];
    struct chart_data *sphere=parts[3];
    struct chart_data_aligned *cylinders=parts[4];
    struct chart_data *cubes=parts[5];

    //Background.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr); 

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 2.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);
  
    GTimer *timer=NULL; 
    if(time_it==TRUE) timer=g_timer_new();
 
    cairo_translate(cr, width/2.0, height/2.0);
    cairo_scale(cr, scale+0.01, scale+0.01);
    //Keep the lines smooth.
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);        
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_set_line_width(cr, 1.0);

    //Rotation.
    gdouble qrs[16];
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    quaternion_rotation(yaw1, roll1, pitch1, qrs);

    //Perspective.
    gdouble pm[16];
    gdouble out[16];
    gdouble near=1.0;
    gdouble far=1.0+perspective*0.01;
    set_projection_matrix(pm, 90.0, near, far);  
    matrix_multiply(qrs, pm, out); 
   
    //Draw the chart.
    if(drawing_id==0) cube_chart_draw(hc, cubes, cr, out, SOLID_CUBES);
    else if(drawing_id==1) cube_chart_draw(hc, cubes, cr, out, GRADIENT_CUBES);
    else if(drawing_id==2) cube_chart_draw(hc, cubes, cr, out, GRADIENT_CUBES2);
    else if(drawing_id==3) chart_data_draw(hc, spiral, cr, out, DRAW_LINES);
    else if(drawing_id==4) chart_data_draw(hc, spiral, cr, out, DRAW_SMOOTH);
    else if(drawing_id==5) chart_data_draw(hc, sphere, cr, out, DRAW_POINTS);
    else if(drawing_id==6) chart_data_draw_aligned(hc, cylinders, cr, out, SOLID_CYLINDERS);
    else chart_data_draw_aligned(hc, cylinders, cr, out, GRADIENT_CYLINDERS);
     
    if(time_it==TRUE) 
     {
       g_print("Draw Time %f\n", g_timer_elapsed(timer, NULL));
       g_timer_destroy(timer);
     }

    return FALSE;
  }
static void draw_svg(GtkWidget *widget, gpointer *parts)
  {
    gdouble width=(gdouble)gtk_widget_get_allocated_width(GTK_WIDGET(parts[0]));
    gdouble height=(gdouble)gtk_widget_get_allocated_height(GTK_WIDGET(parts[0]));
    cairo_surface_t *surface=cairo_svg_surface_create("histo1.svg", width, height);
    cairo_t *cr=cairo_create(surface);

    draw_surface(cr, width, height, parts);

    g_print("histo1.svg saved\n");
    cairo_destroy(cr);
    cairo_surface_destroy(surface);
  }
static void draw_pdf(GtkWidget *widget, gpointer *parts)
  {
    gdouble width=(gdouble)gtk_widget_get_allocated_width(GTK_WIDGET(parts[0]));
    gdouble height=(gdouble)gtk_widget_get_allocated_height(GTK_WIDGET(parts[0]));
    cairo_surface_t *surface=cairo_pdf_surface_create("histo1.pdf", width, height);
    cairo_t *cr=cairo_create(surface);

    draw_surface(cr, width, height, parts);

    g_print("histo1.pdf saved\n");
    cairo_destroy(cr);
    cairo_surface_destroy(surface);
  }
static void draw_ps(GtkWidget *widget, gpointer *parts)
  {
    gdouble width=(gdouble)gtk_widget_get_allocated_width(GTK_WIDGET(parts[0]));
    gdouble height=(gdouble)gtk_widget_get_allocated_height(GTK_WIDGET(parts[0]));
    cairo_surface_t *surface=cairo_ps_surface_create("histo1.ps", width, height);
    cairo_t *cr=cairo_create(surface);

    draw_surface(cr, width, height, parts);

    g_print("histo1.ps saved\n");
    cairo_destroy(cr);
    cairo_surface_destroy(surface);
  }
static void draw_surface(cairo_t *cr, gdouble width, gdouble height, gpointer *parts)
  {
    struct histogram_chart *hc=parts[1];
    struct chart_data *spiral=parts[2];
    struct chart_data *sphere=parts[3];
    struct chart_data_aligned *cylinders=parts[4];
    struct chart_data *cubes=parts[5];

    //Background.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr); 

    cairo_translate(cr, width/2.0, height/2.0);
    cairo_scale(cr, scale+0.01, scale+0.01);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);        
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_set_line_width(cr, 1.0);

    //Rotation.
    gdouble qrs[16];
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    quaternion_rotation(yaw1, roll1, pitch1, qrs);

    //Perspective.
    gdouble pm[16];
    gdouble out[16];
    gdouble near=1.0;
    gdouble far=1.0+perspective*0.01;
    set_projection_matrix(pm, 90.0, near, far);  
    matrix_multiply(qrs, pm, out);

    //Draw the chart.
    if(drawing_id==0) cube_chart_draw(hc, cubes, cr, out, SOLID_CUBES);
    else if(drawing_id==1) cube_chart_draw(hc, cubes, cr, out, GRADIENT_CUBES);
    else if(drawing_id==2) cube_chart_draw(hc, cubes, cr, out, GRADIENT_CUBES2);
    else if(drawing_id==3) chart_data_draw(hc, spiral, cr, out, DRAW_LINES);
    else if(drawing_id==4) chart_data_draw(hc, spiral, cr, out, DRAW_SMOOTH);
    else if(drawing_id==5) chart_data_draw(hc, sphere, cr, out, DRAW_POINTS);
    else if(drawing_id==6) chart_data_draw_aligned(hc, cylinders, cr, out, SOLID_CYLINDERS);
    else chart_data_draw_aligned(hc, cylinders, cr, out, GRADIENT_CYLINDERS);
  }
static struct histogram_chart* initialize_histogram_chart(guint rows, guint columns, gdouble outside_square)
  {
    struct histogram_chart *hc=histogram_chart_new(rows, columns, outside_square);

    //Reduce the back height. Default back height is the width of rows.
    histogram_chart_set_back_height(hc, hc->back_height/2.0);

    //Set histogram base square colors.
    gint i=0;
    gdouble total=rows*columns;
    gdouble rgba[4]={0.0, 0.0, 0.0, 1.0};
    for(i=0;i<total;i++)
      {
        if(i%2==0)
          {
            rgba[1]=0.0;rgba[2]=1.0;
          }
        else
          {
            rgba[1]=1.0;rgba[2]=1.0;
          }
        histogram_chart_set_base_color(hc, i, rgba);
      }
    
    return hc;
  }
static struct chart_data* initialize_cubes(struct histogram_chart *hc, gdouble inside_square, guint inside_square_position)
  {
    gint i=0;
    gint total=(hc->rows)*(hc->columns);
    gdouble inv=1.0/(gdouble)total;
    gdouble rgba[4]={1.0, 0.0, 1.0, 1.0};
    gdouble rand=0.0;

    struct chart_data *cd=chart_cubes_new(hc, inside_square, inside_square_position);

    cd->max_z=100.0;

    //Set histogram bar colors.
    for(i=0;i<total;i++)
      {
        cube_chart_set_bar_color(cd, i, rgba);
        rgba[1]+=inv;
        rgba[2]-=inv;
      }

    //Set some random histogram bar heights.
    for(i=0;i<total;i++)
      {
        rand=(cd->max_z)*g_random_double();
        cube_chart_set_bar_height(cd, i, rand);
      }

    return cd;
  }
static struct chart_data* initialize_spiral(struct histogram_chart *hc, gint points)
  {
    gint i=0;
    gdouble origin_x=-((gdouble)(hc->columns)/2.0)*(hc->outside_square);
    gdouble origin_y=-((gdouble)(hc->rows)/2.0)*(hc->outside_square);
    gdouble arc1=90.0*G_PI/180.0;
    gdouble arc2=-45.0*G_PI/180.0;
    gdouble move=(hc->outside_square)/2.0;
    gint points1=points*2;
    struct chart_data *spiral=g_new(struct chart_data, 1);

    //The data_id isn't used yet.
    spiral->data_id=0;
    spiral->d=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), points1);
    g_array_set_size(spiral->d, points1);
    spiral->d_t=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), points1);
    g_array_set_size(spiral->d_t, points1);

    struct point3d *p1=&g_array_index(spiral->d, struct point3d, 0);
    for(i=0;i<points1;i++)
      {
        (*p1).x=40.0*sin(arc1*(gdouble)i)*cos(arc2)+move*(gdouble)i+origin_x;
        (*p1).y=40.0*sin(arc1*(gdouble)i)*sin(arc2)+move*(gdouble)i+origin_y;
        (*p1).z=40.0*cos(arc1*(gdouble)i)+move*4.0;
        p1++;
      }

    return spiral;
  }
static struct chart_data* initialize_sphere(gint points)
  {
    gint i=0;    
    gdouble arc1=0.0;
    gdouble arc2=0.0;
    struct chart_data *sphere=g_new(struct chart_data, 1);

    //The data_id isn't used yet.
    sphere->data_id=0;
    sphere->d=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), points);
    g_array_set_size(sphere->d, points);
    sphere->d_t=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), points);
    g_array_set_size(sphere->d_t, points);

    struct point3d *p1=&g_array_index(sphere->d, struct point3d, 0);
    for(i=0;i<points;i++)
      {
        arc1=(360.0*g_random_double())*G_PI/180.0;
        arc2=(180.0*g_random_double())*G_PI/180.0;
        (*p1).x=100.0*sin(arc1)*cos(arc2);
        (*p1).y=100.0*sin(arc1)*sin(arc2);
        (*p1).z=100.0*cos(arc1)+100.0;
        p1++;
      }

    return sphere;
  }
static struct chart_data_aligned* initialize_cylinders(struct histogram_chart *hc, gint sections, gdouble radius)
  {
    gint i=0;
    gint total=(hc->rows)*(hc->columns);
    gdouble inv=1.0/(gdouble)total;
    gdouble rgba[4]={1.0, 0.0, 1.0, 1.0};
    gdouble rand=0.0;

    struct chart_data_aligned *cd=chart_cylinders_new(hc, sections, radius);

    cd->max_z=100.0;

    //Set cylinder colors.
    struct arc *p1=(cd->d);
    for(i=0;i<total;i++)
      {
        cylinder_chart_set_bar_color(cd, i, rgba);       
        rgba[1]+=inv;
        rgba[2]-=inv;
        p1++;
      }

    //Set some random histogram bar heights.
    for(i=0;i<total;i++)
      {
        rand=(cd->max_z)*g_random_double();
        cylinder_chart_set_bar_height(cd, i, rand);
      }

    return cd;
  }

//The chart functions.
static struct histogram_chart* histogram_chart_new(guint rows, guint columns, gdouble outside_square)
  {
    //Check the variables.
    if(rows<1)
      {
        rows=1;
        g_warning("Chart rows set to 1.\n");
      }
    else if(columns<1)
      {
        columns=1;
        g_warning("Chart columns set to 1.\n");
      }
    else if(outside_square<10)
      {
        outside_square=10.0;
        g_warning("Outside square set to 10.0.\n");
      }

    struct histogram_chart *hc=g_new(struct histogram_chart, 1);

    hc->rows=rows;
    hc->columns=columns;
    hc->outside_square=outside_square;
    initialize_base(hc, rows, columns, outside_square);
    hc->back_height=200.0;
    hc->back_rows=10;
    hc->back_color[0]=0.0;
    hc->back_color[1]=0.0;
    hc->back_color[2]=1.0;
    hc->back_color[3]=1.0;
    //Initialize axis reference vectors.
    hc->rx[0]=1.0;
    hc->rx[1]=0.0;
    hc->rx[2]=0.0;
    hc->rx_t[0]=1.0;
    hc->rx_t[1]=0.0;
    hc->rx_t[2]=0.0;
    hc->ry[0]=0.0;
    hc->ry[1]=1.0;
    hc->ry[2]=0.0;
    hc->ry_t[0]=0.0;
    hc->ry_t[1]=1.0;
    hc->ry_t[2]=0.0;
    hc->rz[0]=0.0;
    hc->rz[1]=0.0;
    hc->rz[2]=1.0;
    hc->rz_t[0]=0.0;
    hc->rz_t[1]=0.0;
    hc->rz_t[2]=1.0;
    //Initialize the 3 top corners.
    hc->c1[0]=-(columns*outside_square)/2.0;
    hc->c1[1]=(rows*outside_square)/2.0;
    hc->c1[2]=(hc->back_height);
    hc->c1_t[0]=0.0;
    hc->c1_t[1]=0.0;
    hc->c1_t[2]=0.0;
    hc->c2[0]=-(columns*outside_square)/2.0;
    hc->c2[1]=-(rows*outside_square)/2.0;
    hc->c2[2]=(hc->back_height);
    hc->c2_t[0]=0.0;
    hc->c2_t[1]=1.0;
    hc->c2_t[2]=0.0;
    hc->c3[0]=(columns*outside_square)/2.0;
    hc->c3[1]=-(rows*outside_square)/2.0;
    hc->c3[2]=(hc->back_height);
    hc->c3_t[0]=0.0;
    hc->c3_t[1]=0.0;
    hc->c3_t[2]=1.0;

    return hc;
  }
static void initialize_base(struct histogram_chart *hc, guint rows, guint columns, gdouble outside_square)
  {
    gint i=0;
    gint j=0;
    gint total=rows*columns;
    //Center points.
    gdouble move_x=-(columns*outside_square)/2.0;
    gdouble move_y=-(rows*outside_square)/2.0;

    hc->base=g_array_sized_new(FALSE, TRUE, sizeof(struct rectangle3d), total);
    g_array_set_size(hc->base, total);
    hc->base_t=g_array_sized_new(FALSE, TRUE, sizeof(struct rectangle3d), total);
    g_array_set_size(hc->base_t, total);

    struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, 0);

    for(i=0;i<rows;i++)
      {
        for(j=0;j<columns;j++)
          {
            (p1->b1).x=move_x;
            (p1->b1).y=move_y;
            (p1->b1).z=0.0;
            (p1->b2).x=move_x+outside_square;
            (p1->b2).y=move_y;
            (p1->b2).z=0.0;
            (p1->b3).x=move_x+outside_square;
            (p1->b3).y=move_y+outside_square;
            (p1->b3).z=0.0;
            (p1->b4).x=move_x;
            (p1->b4).y=move_y+outside_square;
            (p1->b4).z=0.0;
            //Initialize color.
            (p1->cl).r=0.0;(p1->cl).g=0.0;(p1->cl).b=1.0;(p1->cl).a=1.0;
            move_x+=outside_square;
            p1++;
          }
        move_x=-(columns*outside_square)/2.0;
        move_y+=outside_square;
      }
  }
static void histogram_chart_set_base_color(struct histogram_chart *hc, guint index, gdouble rgba[4])
  {
    if(index>(hc->base->len)-1)
      {
        g_warning("The base index %i isn't valid. No color was set.\n", index);
      }
    else
      {
        struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, index);
        (p1->cl).r=rgba[0];(p1->cl).g=rgba[1];(p1->cl).b=rgba[2];(p1->cl).a=rgba[3];
      }
  }
static void histogram_chart_set_back_height(struct histogram_chart *hc, gdouble height)
  {
    if(height>=0.0) hc->back_height=height;
    else g_warning("The back height wasn't set. height>=0.\n");
  }
static void histogram_chart_free(struct histogram_chart *hc)
  {
    if((hc->base)!=NULL) g_array_free((hc->base), TRUE);
    if((hc->base_t)!=NULL) g_array_free((hc->base_t), TRUE);
    g_free(hc);
  }
static struct chart_data* chart_cubes_new(struct histogram_chart *hc, gdouble inside_square, guint inside_square_position)
  {
    gint i=0;
    gint j=0;
    gint rows=(hc->rows);
    gint columns=(hc->columns);
    gdouble outside_square=(hc->outside_square);
    gint total=rows*columns;
    gdouble gap=outside_square-inside_square;
    gdouble position=0.0;
    if(inside_square_position==CUBE_CENTER) position=gap/2.0;
    //Center points.
    gdouble move_x=-(columns*(inside_square+gap))/2.0+position;
    gdouble move_y=-(rows*(inside_square+gap))/2.0+position;

    struct chart_data *cubes=g_new(struct chart_data, 1);

    cubes->d=g_array_sized_new(FALSE, TRUE, sizeof(struct cube3d), total);
    g_array_set_size(cubes->d, total);
    cubes->d_t=g_array_sized_new(FALSE, TRUE, sizeof(struct cube3d), total);
    g_array_set_size(cubes->d_t, total);

    cubes->data_id=0;
    cubes->max_z=0.0;
    
    struct cube3d *p1=&g_array_index(cubes->d, struct cube3d, 0);

    for(i=0;i<rows;i++)
      {
        for(j=0;j<columns;j++)
          {
            (p1->c1).x=move_x;
            (p1->c1).y=move_y;
            (p1->c1).z=0.0;
            (p1->c2).x=move_x+inside_square;
            (p1->c2).y=move_y;
            (p1->c2).z=0.0;
            (p1->c3).x=move_x+inside_square;
            (p1->c3).y=move_y+inside_square;
            (p1->c3).z=0.0;
            (p1->c4).x=move_x;
            (p1->c4).y=move_y+inside_square;
            (p1->c4).z=0.0;
            (p1->c5).x=move_x;
            (p1->c5).y=move_y;
            (p1->c5).z=0.0;
            (p1->c6).x=move_x+inside_square;
            (p1->c6).y=move_y;
            (p1->c6).z=0.0;
            (p1->c7).x=move_x+inside_square;
            (p1->c7).y=move_y+inside_square;
            (p1->c7).z=0.0;
            (p1->c8).x=move_x;
            (p1->c8).y=move_y+inside_square;
            (p1->c8).z=0.0;
            //Initialize color to green.
            (p1->cl).r=0.0;(p1->cl).g=1.0;(p1->cl).b=0.0;(p1->cl).a=1.0;
            move_x+=inside_square+gap;
            p1++;
          }
        move_x=-(columns*(inside_square+gap))/2.0+position;
        move_y+=inside_square+gap;
      }

    return cubes;
  }
static void cube_chart_set_bar_color(struct chart_data *cd, guint index, gdouble rgba[4])
  {
    if(index>(cd->d->len)-1)
      {
        g_warning("The cube index %i isn't valid. No color was set.\n", index);
      }
    else
      {
        struct cube3d *p1=&g_array_index(cd->d, struct cube3d, index);
        (p1->cl).r=rgba[0];(p1->cl).g=rgba[1];(p1->cl).b=rgba[2];(p1->cl).a=rgba[3];
      }
  }
static void cube_chart_set_bar_height(struct chart_data *cd, guint index, gdouble height)
  {
    if(index>(cd->d->len)-1)
      {
        g_warning("The cube index %i isn't valid. No height was set.\n", index);
      }
    else
      {
        struct cube3d *p1=&g_array_index(cd->d, struct cube3d, index);
        (p1->c5).z=height;
        (p1->c6).z=height;
        (p1->c7).z=height;
        (p1->c8).z=height;
        if((cd->max_z)<height) (cd->max_z)=height;
      }
  }
static double cube_chart_get_bar_height(struct chart_data *cd, guint index)
  {
    gdouble ret=0.0;

    if(index>(cd->d->len)-1)
      {
        g_warning("The cube index %i isn't valid. The returned height is 0.\n", index);
      }
    else
      {
        struct cube3d *p1=&g_array_index(cd->d, struct cube3d, index);
        ret=(p1->c5).z;
      }

    return ret;
  }
static void cube_chart_free(struct chart_data *cd)
  {
    g_array_free(cd->d, TRUE);
    g_array_free(cd->d_t, TRUE);
    g_free(cd);
  }
static struct chart_data_aligned* chart_cylinders_new(struct histogram_chart *hc, gint sections, gdouble radius)
  {
    if(!(sections==8||sections==12))
      {
        g_warning("Sections can be either 8 or 12. Sections set to 8.\n");
        sections=8;
      }
   
    struct chart_data_aligned *cylinders=g_new(struct chart_data_aligned, 1);
    cylinders->data_id=0;
    cylinders->ring_size=sections;
    cylinders->len=sections*2.0*(hc->rows)*(hc->columns);
    cylinders->max_z=0.0;
    initialize_bezier_ring(hc, &(cylinders->d), &(cylinders->d_t), sections, radius, 0.0);

    return cylinders;
  }
static void cylinder_chart_set_bar_color(struct chart_data_aligned *cd, guint index, gdouble rgba[4])
  {
    gint i=0;
    gint offset=((cd->ring_size)*2);
    gint cylinders=(cd->len)/offset;

    if(index>(cylinders-1))
      {
        g_warning("The cylinder index %i isn't valid. No color was set.\n", index);
      }
    else
      {
        struct arc *p1=(cd->d)+(index*offset);
        for(i=0;i<offset;i++)
          {
            (p1->cl).r=rgba[0];(p1->cl).g=rgba[1];(p1->cl).b=rgba[2];(p1->cl).a=rgba[3];
            p1++;
          }
      }
  }
static void cylinder_chart_set_bar_height(struct chart_data_aligned *cd, guint index, gdouble height)
  {
    gint i=0;
    gint offset=((cd->ring_size)*2);
    gint cylinders=(cd->len)/offset;

    if(index>(cylinders-1))
      {
        g_warning("The cylinder index %i isn't valid. No height was set.\n", index);
      }
    else
      {
        struct arc *p1=(cd->d)+(index*offset)+(cd->ring_size);
        for(i=0;i<(cd->ring_size);i++)
          {
            (p1->a1).z=height;
            (p1->a2).z=height;
            (p1->a3).z=height;
            (p1->a4).z=height;
            if((cd->max_z)<height) (cd->max_z)=height;
            p1++;
          }
      }
  }
static double cylinder_chart_get_bar_height(struct chart_data_aligned *cd, guint index)
  {
    gint offset=((cd->ring_size)*2);
    gint cylinders=(cd->len)/offset;
    gdouble ret=0.0;

    if(index>(cylinders-1))
      {
        g_warning("The cylinder index %i isn't valid. The returned height is 0.\n", index);
      }
    else
      {
        struct arc *p1=(cd->d)+(index*offset)+(cd->ring_size);
        ret=(p1->a1).z;
      }

    return ret;
  }
static void cylinder_chart_free(struct chart_data_aligned *cd)
  {
    free(cd->d);
    free(cd->d_t);
    free(cd);
  }
static void histogram_chart_draw_base(struct histogram_chart *hc, cairo_t *cr)
  {
    gint i=0;
    struct rectangle3d *p1=&g_array_index(hc->base_t, struct rectangle3d, 0);

    for(i=0;i<(hc->base_t->len);i++)
      {
        cairo_set_source_rgba(cr, (p1->cl).r, (p1->cl).g, (p1->cl).b, (p1->cl).a);
        cairo_move_to(cr, (p1->b1).x, (p1->b1).y);
        cairo_line_to(cr, (p1->b2).x, (p1->b2).y);
        cairo_line_to(cr, (p1->b3).x, (p1->b3).y);
        cairo_line_to(cr, (p1->b4).x, (p1->b4).y);
        cairo_close_path(cr);
        cairo_fill(cr);
        p1++;
      }
  }
static void histogram_chart_draw_numbers_left(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[16])
  {
    gint i=0;
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    gdouble font_size=(hc->outside_square)/2.0;
    gdouble cr1=1.0;
    gdouble cr2=1.0;
    gdouble cr3=1.0;
    gdouble w=0.0;

    struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, 0);

    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, font_size);
    for(i=0;i<(hc->columns);i++)
      {
        cairo_save(cr);
        cr1=(p1->b1).x;
        cr2=(p1->b1).y;
        cr3=0.0;
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        cairo_matrix_init(&matrix1, qrs[0]/w, qrs[4]/w, qrs[1]/w, qrs[5]/w, 0.0, 0.0);
        cairo_transform(cr, &matrix1); 
        gchar *string=g_strdup_printf("%i", i);
        cairo_text_extents(cr, string, &extents);
        if(i<10) cairo_move_to(cr, (p1->b1).x+1.3*extents.width, (p1->b1).y-extents.height);
        else cairo_move_to(cr, (p1->b1).x+extents.width/3.0, (p1->b1).y-extents.height);
        cairo_show_text(cr, string);
        g_free(string);
        cairo_restore(cr);
        p1++;
      }

    p1=&g_array_index(hc->base, struct rectangle3d, 0);
    for(i=0;i<(hc->rows);i++)
      {
        cairo_save(cr);
        cr1=(p1->b1).x;
        cr2=(p1->b1).y;
        cr3=0.0;
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        cairo_matrix_init(&matrix1, qrs[0]/w, qrs[4]/w, qrs[1]/w, qrs[5]/w, 0.0, 0.0);
        cairo_transform(cr, &matrix1); 
        gchar *string=g_strdup_printf("%i", i);
        if(i==0) cairo_text_extents(cr, string, &extents);
        if(i==10) cairo_text_extents(cr, string, &extents);
        if(i<10)cairo_move_to(cr, (p1->b1).x-2.6*extents.width, (p1->b1).y+2.0*extents.height);
        else cairo_move_to(cr, (p1->b1).x-1.3*extents.width, (p1->b1).y+2.0*extents.height);
        cairo_show_text(cr, string);
        g_free(string);
        cairo_restore(cr);
        p1+=(hc->columns);
      }
  }
static void histogram_chart_draw_numbers_right(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[16])
  {
    gint i=0;
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    gdouble font_size=(hc->outside_square)/2.0;
    gdouble cr1=1.0;
    gdouble cr2=1.0;
    gdouble cr3=1.0;
    gdouble w=0.0;

    struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, (hc->rows)*(hc->columns)-(hc->columns));

    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, font_size);
    for(i=0;i<(hc->columns);i++)
      {
        cairo_save(cr);
        cr1=(p1->b4).x;
        cr2=(p1->b4).y;
        cr3=0.0;
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        cairo_matrix_init(&matrix1, qrs[0]/w, qrs[4]/w, qrs[1]/w, qrs[5]/w, 0.0, 0.0);
        cairo_transform(cr, &matrix1);
        gchar *string=g_strdup_printf("%i", i);
        if(i==0) cairo_text_extents(cr, string, &extents);
        if(i==10) cairo_text_extents(cr, string, &extents);
        if(i<10) cairo_move_to(cr, (p1->b4).x+1.3*extents.width, (p1->b4).y+2.0*extents.height);
        else cairo_move_to(cr, (p1->b4).x+extents.width/3.0, (p1->b4).y+2.0*extents.height);
        cairo_show_text(cr, string);
        g_free(string);
        cairo_restore(cr);
        p1++;
      }

    p1=&g_array_index(hc->base, struct rectangle3d, hc->columns-1);
    for(i=0;i<(hc->rows);i++)
      {
        cairo_save(cr);
        cr1=(p1->b2).x;
        cr2=(p1->b2).y;
        cr3=0.0;
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        cairo_matrix_init(&matrix1, qrs[0]/w, qrs[4]/w, qrs[1]/w, qrs[5]/w, 0.0, 0.0);
        cairo_transform(cr, &matrix1);
        gchar *string=g_strdup_printf("%i", i);
        if(i==0) cairo_text_extents(cr, string, &extents);
        cairo_move_to(cr, (p1->b2).x+extents.width, (p1->b2).y+2.0*extents.height);
        cairo_show_text(cr, string);
        g_free(string);
        cairo_restore(cr);
        p1+=(hc->columns);
      }
  }
static void histogram_chart_draw_back(struct histogram_chart *hc, cairo_t *cr)
  {
    gint i=0;
    gdouble slope_x1=0.0;
    gdouble slope_x2=0.0;
    gdouble slope_y1=0.0;
    gdouble slope_y2=0.0;
    struct rectangle3d *p1=&g_array_index(hc->base_t, struct rectangle3d, 0);
    struct rectangle3d *p2=&g_array_index(hc->base_t, struct rectangle3d, hc->columns-1);   

    slope_x1=(hc->c2_t[0]-(p1->b1).x);
    slope_y1=(hc->c2_t[1]-(p1->b1).y);
    slope_x2=(hc->c3_t[0]-(p2->b2).x);
    slope_y2=(hc->c3_t[1]-(p2->b2).y);

    cairo_set_source_rgba(cr, (hc->back_color)[0], (hc->back_color)[1], (hc->back_color)[2], (hc->back_color)[3]);
    cairo_set_line_width(cr, 3.0);

    cairo_move_to(cr, (p1->b1).x, (p1->b1).y);
    cairo_line_to(cr, (p1->b1).x+slope_x1, (p1->b1).y+slope_y1);
    cairo_line_to(cr, (p2->b2).x+slope_x2, (p2->b2).y+slope_y2);
    cairo_line_to(cr, (p2->b2).x, (p2->b2).y);
    cairo_stroke(cr);

    slope_x1/=(hc->back_rows);
    slope_y1/=(hc->back_rows);
    slope_x2/=(hc->back_rows);
    slope_y2/=(hc->back_rows);

    for(i=0;i<(hc->back_rows);i++)
      {
        cairo_move_to(cr, (p1->b1).x+(gdouble)(i+1)*slope_x1, (p1->b1).y+(gdouble)(i+1)*slope_y1);
        cairo_line_to(cr, (p2->b2).x+(gdouble)(i+1)*slope_x2, (p2->b2).y+(gdouble)(i+1)*slope_y2); 
        cairo_stroke(cr);
      }

    cairo_set_line_width(cr, 1.0);
  }
static void histogram_chart_draw_numbers_z(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[16])
  {
    gint i=0;
    gdouble slope_x1=0.0;
    gdouble slope_y1=0.0;
    cairo_matrix_t matrix1;
    gdouble font_size=(hc->outside_square)/2.0;
    gdouble r[16];
    gdouble out[16];

    cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, font_size);

    quaternion_rotation(0.0, 0.0, -G_PI/2.0, r);
    matrix_multiply(r, qrs, out);

    struct rectangle3d *p1=&g_array_index(hc->base_t, struct rectangle3d, (hc->columns)-1);

    slope_x1=(hc->c3_t[0]-(p1->b2).x);
    slope_y1=(hc->c3_t[1]-(p1->b2).y);
    slope_x1/=hc->back_rows;
    slope_y1/=hc->back_rows;

    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    for(i=0;i<(hc->back_rows);i++)
      {
        if(hc->rx_t[0]>=0.0)
          {
            cairo_move_to(cr, (p1->b2).x+(gdouble)(i)*slope_x1+8.0, (p1->b2).y+(gdouble)(i)*slope_y1);
          }
        else
          {
            cairo_move_to(cr, (p1->b2).x+(gdouble)(i)*slope_x1-8.0, (p1->b2).y+(gdouble)(i)*slope_y1);
          }
        cairo_save(cr);
        gchar *string=g_strdup_printf("%i", i);
        //Rotate the text in the center but have it drawn at the move to position.
        cairo_matrix_init(&matrix1, out[0], out[4], out[1], out[5], 0.0, 0.0);
        cairo_transform(cr, &matrix1); 
        cairo_show_text(cr, string);
        g_free(string);
        cairo_restore(cr);
      }
  }
static void histogram_chart_draw_side(struct histogram_chart *hc, cairo_t *cr)
  {
    gint i=0;
    gdouble slope_x1=0.0;
    gdouble slope_x2=0.0;
    gdouble slope_y1=0.0;
    gdouble slope_y2=0.0;
    struct rectangle3d *p1=&g_array_index(hc->base_t, struct rectangle3d, 0);
    struct rectangle3d *p2=&g_array_index(hc->base_t, struct rectangle3d, (hc->rows)*(hc->columns)-(hc->columns));  

    slope_x1=(hc->c1_t[0]-(p2->b4).x);
    slope_y1=(hc->c1_t[1]-(p2->b4).y);
    slope_x2=(hc->c2_t[0]-(p1->b1).x);
    slope_y2=(hc->c2_t[1]-(p1->b1).y);

    cairo_set_source_rgba(cr, (hc->back_color)[0], (hc->back_color)[1], (hc->back_color)[2], (hc->back_color)[3]);
    cairo_set_line_width(cr, 3.0);

    cairo_move_to(cr, (p2->b4).x, (p2->b4).y);
    cairo_line_to(cr, (p2->b4).x+slope_x1, (p2->b4).y+slope_y1);
    cairo_line_to(cr, (p1->b1).x+slope_x2, (p1->b1).y+slope_y2);
    cairo_line_to(cr, (p1->b1).x, (p1->b1).y);
    cairo_stroke(cr);

    slope_x1/=hc->back_rows;
    slope_y1/=hc->back_rows;
    slope_x2/=hc->back_rows;
    slope_y2/=hc->back_rows;

    for(i=0;i<(hc->back_rows);i++)
      {
        cairo_move_to(cr, (p2->b4).x+(gdouble)(i+1)*slope_x1, (p2->b4).y+(gdouble)(i+1)*slope_y1);
        cairo_line_to(cr, (p1->b1).x+(gdouble)(i+1)*slope_x2, (p1->b1).y+(gdouble)(i+1)*slope_y2); 
        cairo_stroke(cr);
      }

    cairo_set_line_width(cr, 1.0);
  }
static void histogram_chart_reset_base(struct histogram_chart *hc)
  {
    struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, 0);
    struct rectangle3d *p2=&g_array_index(hc->base_t, struct rectangle3d, 0);
    memcpy(p2, p1, sizeof(struct rectangle3d)*(hc->base->len));
  }
static void chart_data_reset_cubes(struct chart_data *cd)
  {
    struct cube3d *p1=&g_array_index(cd->d, struct cube3d, 0);
    struct cube3d *p2=&g_array_index(cd->d_t, struct cube3d, 0);
    memcpy(p2, p1, sizeof(struct cube3d)*(cd->d->len));
  }
static void chart_data_reset(struct chart_data *cd)
  {   
    struct point3d *p1=&g_array_index(cd->d, struct point3d, 0);
    struct point3d *p2=&g_array_index(cd->d_t, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(cd->d->len));
  }
static void chart_data_reset_aligned(struct chart_data_aligned *cd)
  {
    struct arc *p1=(struct arc*)(cd->d);
    struct arc *p2=(struct arc*)(cd->d_t);
    memcpy(p2, p1, sizeof(struct arc)*(cd->len));
  }
static void cube_chart_draw(struct histogram_chart *hc, struct chart_data *cd, cairo_t *cr, gdouble qrs[16], guint draw_type)
  {
    histogram_chart_reset_base(hc);
    histogram_chart_rotate_base(hc, qrs);
    chart_data_reset_cubes(cd);    
    chart_data_rotate_cubes(cd, qrs);

    if(hc->rz_t[2]>0.0)
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_numbers_z(hc, cr, qrs);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_numbers_left(hc, cr, qrs);
        histogram_chart_draw_numbers_right(hc, cr, qrs);
        histogram_chart_draw_base(hc, cr);    
        g_array_sort(cd->d_t, compare_cubes);
        if(draw_type==SOLID_CUBES) chart_data_draw_cubes(cd, cr, hc->rz_t[2]);
        else if(draw_type==GRADIENT_CUBES) chart_data_draw_cubes_grad(cd, cr, hc->rz_t[2]);
        else chart_data_draw_cubes_grad2(cd, cr, hc->rz_t[2]);
        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_numbers_z(hc, cr, qrs);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
      }
    else
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_numbers_z(hc, cr, qrs);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr); 
        g_array_sort(cd->d_t, compare_cubes);
        if(draw_type==SOLID_CUBES) chart_data_draw_cubes(cd, cr, hc->rz_t[2]);
        else if(draw_type==GRADIENT_CUBES) chart_data_draw_cubes_grad(cd, cr, hc->rz_t[2]);
        else chart_data_draw_cubes_grad2(cd, cr, hc->rz_t[2]);
        histogram_chart_draw_back(hc, cr);
        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_numbers_z(hc, cr, qrs);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_base(hc, cr);        
        histogram_chart_draw_numbers_left(hc, cr, qrs);
        histogram_chart_draw_numbers_right(hc, cr, qrs);  
      }
  }
static void chart_data_draw(struct histogram_chart *hc, struct chart_data *cd, cairo_t *cr, gdouble qrs[16], gint draw_option)
  {
    histogram_chart_reset_base(hc);
    histogram_chart_rotate_base(hc, qrs);    
    chart_data_reset(cd);    
    chart_data_rotate(cd, qrs);

    if(hc->rz_t[2]>0.0)
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_numbers_z(hc, cr, qrs);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_numbers_left(hc, cr, qrs);
        histogram_chart_draw_numbers_right(hc, cr, qrs);
        histogram_chart_draw_base(hc, cr);

        //Draw data.
        cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
        cairo_set_line_width(cr, 10.0);
        if(draw_option==DRAW_LINES)
          {
            chart_data_draw_lines(cd, cr);
            cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
            chart_data_draw_points(cd, cr);
          }
        else if(draw_option==DRAW_SMOOTH)
          {
            chart_data_draw_smooth(cd, cr);
            cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
            chart_data_draw_points(cd, cr);
          }
        else
          {
            chart_data_draw_points(cd, cr);
          }
       
        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_numbers_z(hc, cr, qrs);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
      }
    else
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_numbers_z(hc, cr, qrs);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr); 

        //Draw data.
        cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
        cairo_set_line_width(cr, 10.0);
        if(draw_option==DRAW_LINES)
          {
            chart_data_draw_lines(cd, cr);
            cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
            chart_data_draw_points(cd, cr);
          }
        else if(draw_option==DRAW_SMOOTH)
          {
            chart_data_draw_smooth(cd, cr);
            cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
            chart_data_draw_points(cd, cr);
          }
        else
          {
            chart_data_draw_points(cd, cr);
          }

        histogram_chart_draw_back(hc, cr);
        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_numbers_z(hc, cr, qrs);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_base(hc, cr);        
        histogram_chart_draw_numbers_left(hc, cr, qrs);
        histogram_chart_draw_numbers_right(hc, cr, qrs);  
      }
  }
static void chart_data_draw_aligned(struct histogram_chart *hc, struct chart_data_aligned *cd, cairo_t *cr, gdouble qrs[16], gint draw_option)
  {
    histogram_chart_reset_base(hc);
    histogram_chart_rotate_base(hc, qrs);    
    chart_data_reset_aligned(cd);    
    chart_data_rotate_aligned(cd, qrs);

    if(hc->rz_t[2]>0.0)
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_numbers_z(hc, cr, qrs);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_numbers_left(hc, cr, qrs);
        histogram_chart_draw_numbers_right(hc, cr, qrs);
        histogram_chart_draw_base(hc, cr);

        //Draw data.
        cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);        
        cairo_set_line_width(cr, 1.0);
        if(cd->ring_size==8) qsort(cd->d_t, ((cd->len)/16), sizeof(struct cylinder16), compare_cylinders16);
        else qsort(cd->d_t, ((cd->len)/24), sizeof(struct cylinder24), compare_cylinders24);
        if(draw_option==SOLID_CYLINDERS) chart_data_draw_cylinders(cd, cr, hc->rz_t[2]);
        else chart_data_draw_cylinders_grad(cd, cr, hc->rz_t[2]);
       
        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_numbers_z(hc, cr, qrs);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
      }
    else
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_numbers_z(hc, cr, qrs);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr); 

        //Draw data.
        cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
        cairo_set_line_width(cr, 10.0);
        cairo_set_line_width(cr, 2.0);
        if(cd->ring_size==8)qsort(cd->d_t, ((cd->len)/16), sizeof(struct cylinder16), compare_cylinders16);
        else qsort(cd->d_t, ((cd->len)/24), sizeof(struct cylinder24), compare_cylinders24);
        if(draw_option==SOLID_CYLINDERS) chart_data_draw_cylinders(cd, cr, hc->rz_t[2]);
        else chart_data_draw_cylinders_grad(cd, cr, hc->rz_t[2]);

        histogram_chart_draw_back(hc, cr);
        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_numbers_z(hc, cr, qrs);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_base(hc, cr);        
        histogram_chart_draw_numbers_left(hc, cr, qrs);
        histogram_chart_draw_numbers_right(hc, cr, qrs);  
      }
  }
static void chart_data_draw_cubes(struct chart_data *cd, cairo_t *cr, gdouble chart_z)
  {
    gint i=0;
    struct cube3d *p1=&g_array_index(cd->d_t, struct cube3d, 0);

    for(i=0;i<(cd->d_t->len);i++)
      {
        cairo_set_source_rgba(cr, (p1->cl).r, (p1->cl).g, (p1->cl).b, (p1->cl).a);
        //Bottom of cube.
        cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
        cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
        cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
        cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        //Sides of cube
        cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
        cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
        cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
        cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        cairo_move_to(cr, (p1->c2).x, (p1->c2).y);
        cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
        cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
        cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        cairo_move_to(cr, (p1->c3).x, (p1->c3).y);
        cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
        cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
        cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        cairo_move_to(cr, (p1->c4).x, (p1->c4).y);
        cairo_line_to(cr, (p1->c1).x, (p1->c1).y);
        cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
        cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        //Top of cube
        cairo_move_to(cr, (p1->c5).x, (p1->c5).y);
        cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
        cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
        cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
        cairo_close_path(cr);
        if(chart_z>0.0)
          {
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else
          {
            cairo_fill(cr);
          }
        p1++;
      }
  }
static void chart_data_draw_cubes_grad(struct chart_data *cd, cairo_t *cr, gdouble chart_z)
  {
    /*
        These cubes have been sorted at this point so need to calculate the distance
        from the top and bottom square.
    */
    gint i=0;
    struct cube3d *p1=&g_array_index(cd->d_t, struct cube3d, 0);
    gdouble distance=0.0;
    gdouble max_val=(cd->max_z);
    gdouble dm=0.0;
    gdouble bottom_color[4]={1.0, 0.0, 1.0, 1.0};
    cairo_pattern_t *pattern1=NULL;
    gdouble a[3]={0.0, 0.0, 0.0};
    gdouble b[3]={0.0, 0.0, 0.0};
    gdouble c[3]={0.0, 0.0, 0.0}; 

    for(i=0;i<(cd->d_t->len);i++)
      {
        //Bottom of cube.
        cairo_set_source_rgba(cr, bottom_color[0], bottom_color[1], bottom_color[2], bottom_color[3]);
        cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
        cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
        cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
        cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        //Sides of cube.
        distance=sqrt(((p1->c5).x-(p1->c1).x)*((p1->c5).x-(p1->c1).x)+((p1->c5).y-(p1->c1).y)*((p1->c5).y-(p1->c1).y)+((p1->c5).z-(p1->c1).z)*((p1->c5).z-(p1->c1).z));
        dm=distance/max_val;

        /*
          Cross product. Just need to know if the z value is positive or negative before drawing
          the side gradient of the cube for solid colors.
        */
        a[0]=(p1->c2).x-(p1->c1).x;
        a[1]=(p1->c2).y-(p1->c1).y;
        //a[2]=(p1->c5).z-(p1->c1).z;
        b[0]=(p1->c5).x-(p1->c1).x;
        b[1]=(p1->c5).y-(p1->c1).y;
        //b[2]=(p1->c4).z-(p1->c1).z;
        //c[0]=a[1]*b[2]-a[2]*b[1];
        //c[1]=a[2]*b[0]-a[0]*b[2];
        c[2]=a[0]*b[1]-a[1]*b[0];
        if(c[2]>0.0)
          {
            pattern1=cairo_pattern_create_linear((p1->c1).x, (p1->c1).y, (p1->c5).x, (p1->c5).y); 
            cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 1.0, 0.0, 1.0, 1.0); 
            cairo_pattern_add_color_stop_rgba(pattern1, 1.0, 1.0, dm, 1.0-dm, 1.0);
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
            cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
            cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
            cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        a[0]=(p1->c3).x-(p1->c2).x;
        a[1]=(p1->c3).y-(p1->c2).y;
        b[0]=(p1->c6).x-(p1->c2).x;
        b[1]=(p1->c6).y-(p1->c2).y;
        c[2]=a[0]*b[1]-a[1]*b[0];       
        if(c[2]>0.0)
          {
            pattern1=cairo_pattern_create_linear((p1->c2).x, (p1->c2).y, (p1->c6).x, (p1->c6).y); 
            cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 1.0, 0.0, 1.0, 1.0); 
            cairo_pattern_add_color_stop_rgba(pattern1, 1.0, 1.0, dm, 1.0-dm, 1.0);
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->c2).x, (p1->c2).y);
            cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
            cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
            cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        a[0]=(p1->c4).x-(p1->c3).x;
        a[1]=(p1->c4).y-(p1->c3).y;
        b[0]=(p1->c7).x-(p1->c3).x;
        b[1]=(p1->c7).y-(p1->c3).y;
        c[2]=a[0]*b[1]-a[1]*b[0];
        if(c[2]>0.0)
          {
            pattern1=cairo_pattern_create_linear((p1->c3).x, (p1->c3).y, (p1->c7).x, (p1->c7).y); 
            cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 1.0, 0.0, 1.0, 1.0); 
            cairo_pattern_add_color_stop_rgba(pattern1, 1.0, 1.0, dm, 1.0-dm, 1.0);
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->c3).x, (p1->c3).y);
            cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
            cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
            cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        a[0]=(p1->c1).x-(p1->c4).x;
        a[1]=(p1->c1).y-(p1->c4).y;
        b[0]=(p1->c8).x-(p1->c4).x;
        b[1]=(p1->c8).y-(p1->c4).y;
        c[2]=a[0]*b[1]-a[1]*b[0];
        if(c[2]>0.0)
          {
            pattern1=cairo_pattern_create_linear((p1->c4).x, (p1->c4).y, (p1->c8).x, (p1->c8).y); 
            cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 1.0, 0.0, 1.0, 1.0); 
            cairo_pattern_add_color_stop_rgba(pattern1, 1.0, 1.0, dm, 1.0-dm, 1.0); 
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->c4).x, (p1->c4).y);
            cairo_line_to(cr, (p1->c1).x, (p1->c1).y);
            cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
            cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        //Top of cube
        cairo_set_source_rgba(cr, 1.0, dm, 1.0-dm, 1.0); 
        cairo_move_to(cr, (p1->c5).x, (p1->c5).y);
        cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
        cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
        cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
        cairo_close_path(cr);
        if(chart_z>0.0)
          {
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else
          {
            cairo_fill(cr);
          }
        p1++;
      }
  }
static void chart_data_draw_cubes_grad2(struct chart_data *cd, cairo_t *cr, gdouble chart_z)
  {
    //Try a 3 color gradient.
    gint i=0;
    struct cube3d *p1=&g_array_index(cd->d_t, struct cube3d, 0);
    gdouble distance=0.0;
    gdouble max_val=(cd->max_z);
    gdouble dm=0.0;
    gdouble bottom_color[4]={0.0, 0.0, 1.0, 1.0};
    cairo_pattern_t *pattern1=NULL;
    gdouble a[3]={0.0, 0.0, 0.0};
    gdouble b[3]={0.0, 0.0, 0.0};
    gdouble c[3]={0.0, 0.0, 0.0}; 

    for(i=0;i<(cd->d_t->len);i++)
      {
        //Bottom of cube.
        cairo_set_source_rgba(cr, bottom_color[0], bottom_color[1], bottom_color[2], bottom_color[3]);
        cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
        cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
        cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
        cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        //Sides of cube.
        distance=sqrt(((p1->c5).x-(p1->c1).x)*((p1->c5).x-(p1->c1).x)+((p1->c5).y-(p1->c1).y)*((p1->c5).y-(p1->c1).y)+((p1->c5).z-(p1->c1).z)*((p1->c5).z-(p1->c1).z));
        dm=distance/max_val;

        a[0]=(p1->c2).x-(p1->c1).x;
        a[1]=(p1->c2).y-(p1->c1).y;
        b[0]=(p1->c5).x-(p1->c1).x;
        b[1]=(p1->c5).y-(p1->c1).y;
        c[2]=a[0]*b[1]-a[1]*b[0];
        if(c[2]>0.0)
          {
            pattern1=cairo_pattern_create_linear((p1->c1).x, (p1->c1).y, (p1->c5).x, (p1->c5).y); 
            if(dm<=0.5)
              { 
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 0.0, 0.0, 1.0, 1.0); 
                cairo_pattern_add_color_stop_rgba(pattern1, 1.0, 0.0, 2.0*dm, 1.0-(2.0*dm), 1.0);
              }
            else
              {
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 0.0, 0.0, 1.0, 1.0); 
                cairo_pattern_add_color_stop_rgba(pattern1, 0.5, 0.0, 1.0, 0.0, 1.0);
                cairo_pattern_add_color_stop_rgba(pattern1, dm, dm, 1.0, 0.0, 1.0);
              } 
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
            cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
            cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
            cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        a[0]=(p1->c3).x-(p1->c2).x;
        a[1]=(p1->c3).y-(p1->c2).y;
        b[0]=(p1->c6).x-(p1->c2).x;
        b[1]=(p1->c6).y-(p1->c2).y;
        c[2]=a[0]*b[1]-a[1]*b[0];       
        if(c[2]>0.0)
          {
            pattern1=cairo_pattern_create_linear((p1->c2).x, (p1->c2).y, (p1->c6).x, (p1->c6).y); 
            if(dm<=0.5)
              { 
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 0.0, 0.0, 1.0, 1.0); 
                cairo_pattern_add_color_stop_rgba(pattern1, 1.0, 0.0, 2.0*dm, 1.0-(2.0*dm), 1.0);
              }
            else
              {
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 0.0, 0.0, 1.0, 1.0); 
                cairo_pattern_add_color_stop_rgba(pattern1, 0.5, 0.0, 1.0, 0.0, 1.0);
                cairo_pattern_add_color_stop_rgba(pattern1, dm, dm, 1.0, 0.0, 1.0);
              }   
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->c2).x, (p1->c2).y);
            cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
            cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
            cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        a[0]=(p1->c4).x-(p1->c3).x;
        a[1]=(p1->c4).y-(p1->c3).y;
        b[0]=(p1->c7).x-(p1->c3).x;
        b[1]=(p1->c7).y-(p1->c3).y;
        c[2]=a[0]*b[1]-a[1]*b[0];
        if(c[2]>0.0)
          {
            pattern1=cairo_pattern_create_linear((p1->c3).x, (p1->c3).y, (p1->c7).x, (p1->c7).y);
            if(dm<=0.5)
              { 
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 0.0, 0.0, 1.0, 1.0); 
                cairo_pattern_add_color_stop_rgba(pattern1, 1.0, 0.0, 2.0*dm, 1.0-(2.0*dm), 1.0);
              }
            else
              {
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 0.0, 0.0, 1.0, 1.0); 
                cairo_pattern_add_color_stop_rgba(pattern1, 0.5, 0.0, 1.0, 0.0, 1.0);
                cairo_pattern_add_color_stop_rgba(pattern1, dm, dm, 1.0, 0.0, 1.0);
              }   
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->c3).x, (p1->c3).y);
            cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
            cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
            cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        a[0]=(p1->c1).x-(p1->c4).x;
        a[1]=(p1->c1).y-(p1->c4).y;
        b[0]=(p1->c8).x-(p1->c4).x;
        b[1]=(p1->c8).y-(p1->c4).y;
        c[2]=a[0]*b[1]-a[1]*b[0];
        if(c[2]>0.0)
          {
            pattern1=cairo_pattern_create_linear((p1->c4).x, (p1->c4).y, (p1->c8).x, (p1->c8).y);  
            if(dm<=0.5)
              { 
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 0.0, 0.0, 1.0, 1.0); 
                cairo_pattern_add_color_stop_rgba(pattern1, 1.0, 0.0, 2.0*dm, 1.0-(2.0*dm), 1.0);
              }
            else
              {
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 0.0, 0.0, 1.0, 1.0); 
                cairo_pattern_add_color_stop_rgba(pattern1, 0.5, 0.0, 1.0, 0.0, 1.0);
                cairo_pattern_add_color_stop_rgba(pattern1, dm, dm, 1.0, 0.0, 1.0);
              }   
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->c4).x, (p1->c4).y);
            cairo_line_to(cr, (p1->c1).x, (p1->c1).y);
            cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
            cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        //Top of cube
        if(dm<=0.5) cairo_set_source_rgba(cr, 0.0, 2.0*dm, 1.0-(2.0*dm), 1.0);
        else cairo_set_source_rgba(cr, dm, 1.0, 0.0, 1.0);
        cairo_move_to(cr, (p1->c5).x, (p1->c5).y);
        cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
        cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
        cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
        cairo_close_path(cr);
        if(chart_z>0.0)
          {
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else
          {
            cairo_fill(cr);
          }
        p1++;
      }
  }
static void chart_data_draw_lines(struct chart_data *cd, cairo_t *cr)
  {
    gint i=0;
    struct point3d *p1=&g_array_index(cd->d_t, struct point3d, 0);

    cairo_move_to(cr, p1->x, p1->y);
    p1++;
    for(i=1;i<(cd->d_t->len);i++)
      {       
        cairo_line_to(cr, p1->x, p1->y);  
        p1++;
      }
    cairo_stroke(cr);
  }
static void chart_data_draw_smooth(struct chart_data *cd, cairo_t *cr)
  {
    gint i=0;
    struct controls2d *c1=NULL;
    GArray *control1=control_points_from_coords_3d(cd->d_t);

    struct point3d *p1=&g_array_index(cd->d_t, struct point3d, 0);

    c1=&g_array_index(control1, struct controls2d, 0);
    cairo_move_to(cr, p1->x, p1->y);
    for(i=1;i<(cd->d_t->len);i++)
      {
        p1++;
        cairo_curve_to(cr, c1->x1, c1->y1, c1->x2, c1->y2, p1->x, p1->y);
        c1++; 
      }
    cairo_stroke(cr);

    if(control1!=NULL) g_array_free(control1, TRUE);
  }
static void chart_data_draw_points(struct chart_data *cd, cairo_t *cr)
  {
    gint i=0;
    struct point3d *p1=&g_array_index(cd->d_t, struct point3d, 0);

    for(i=0;i<(cd->d_t->len);i++)
      {
        cairo_move_to(cr, p1->x, p1->y);
        cairo_line_to(cr, p1->x, p1->y);
        cairo_stroke(cr); 
        p1++;
      }
  }
static void chart_data_draw_cylinders(struct chart_data_aligned *cd, cairo_t *cr, gdouble chart_z)
  {
    gint i=0;
    gint j=0;
    gint cylinders=(cd->len)/((cd->ring_size)*2.0);
    struct arc *p1=(struct arc*)(cd->d_t);
    struct arc *p2=(struct arc*)(cd->d_t);

    for(i=0;i<cylinders;i++)
      { 
        //Bottom of cylinder.
        cairo_set_source_rgba(cr, (p1->cl).r, (p1->cl).g, (p1->cl).b, (p1->cl).a);
        cairo_move_to(cr, (p1->a1).x, (p1->a1).y); 
        for(j=0;j<(cd->ring_size);j++)
          { 
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            p1++;
          }
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        //The sides of the cylinder.
        cairo_set_source_rgba(cr, (p1->cl).r, (p1->cl).g, (p1->cl).b, (p1->cl).a);
        p2=p1-(cd->ring_size); 
        for(j=0;j<(cd->ring_size);j++)
          { 
            cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
            cairo_line_to(cr, (p2->a1).x, (p2->a1).y);
            cairo_curve_to(cr, (p2->a2).x, (p2->a2).y, (p2->a3).x, (p2->a3).y, (p2->a4).x, (p2->a4).y);
            cairo_line_to(cr, (p1->a4).x, (p1->a4).y);
            cairo_curve_to(cr, (p1->a3).x, (p1->a3).y, (p1->a2).x, (p1->a2).y, (p1->a1).x, (p1->a1).y);
            cairo_fill(cr);
            p1++;p2++;
          }
        
        //Top of cylinder.
        cairo_move_to(cr, (p2->a1).x, (p2->a1).y); 
        for(j=0;j<(cd->ring_size);j++)
          { 
            cairo_curve_to(cr, (p2->a2).x, (p2->a2).y, (p2->a3).x, (p2->a3).y, (p2->a4).x, (p2->a4).y);
            p2++;
          }
        if(chart_z>0.0)
          {
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else
          {
            cairo_fill(cr);
          }
      }
  }
static void chart_data_draw_cylinders_grad(struct chart_data_aligned *cd, cairo_t *cr, gdouble chart_z)
  {
    gint i=0;
    gint j=0;
    gdouble dm=0.0;
    gdouble distance=0.0;
    gint cylinders=(cd->len)/((cd->ring_size)*2.0);
    struct arc *p1=(struct arc*)(cd->d_t);
    struct arc *p2=(struct arc*)(cd->d_t);
    cairo_pattern_t *pattern1=NULL;

    for(i=0;i<cylinders;i++)
      { 
        //Bottom of cylinder.
        cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
        cairo_move_to(cr, (p1->a1).x, (p1->a1).y); 
        for(j=0;j<(cd->ring_size);j++)
          { 
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            p1++;
          }
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);

        //The sides of the cylinder.
        p2=p1-(cd->ring_size); 
        distance=sqrt(((p1->a1).x-(p2->a1).x)*((p1->a1).x-(p2->a1).x)+((p1->a1).y-(p2->a1).y)*((p1->a1).y-(p2->a1).y)+((p1->a1).z-(p2->a1).z)*((p1->a1).z-(p2->a1).z));
        dm=distance/(cd->max_z);
        for(j=0;j<(cd->ring_size);j++)
          { 
            pattern1=cairo_pattern_create_linear((p2->a1).x, (p2->a1).y, (p1->a1).x, (p1->a1).y); 
            cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 1.0, 0.0, 1.0, 1.0); 
            cairo_pattern_add_color_stop_rgba(pattern1, 1.0, 1.0, dm, 1.0-dm, 1.0);
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
            cairo_line_to(cr, (p2->a1).x, (p2->a1).y);
            cairo_curve_to(cr, (p2->a2).x, (p2->a2).y, (p2->a3).x, (p2->a3).y, (p2->a4).x, (p2->a4).y);
            cairo_line_to(cr, (p1->a4).x, (p1->a4).y);
            cairo_curve_to(cr, (p1->a3).x, (p1->a3).y, (p1->a2).x, (p1->a2).y, (p1->a1).x, (p1->a1).y);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
            p1++;p2++;
          }
        
        //Top of cylinder.
        cairo_set_source_rgba(cr, 1.0, dm, 1.0-dm, 1.0);  
        cairo_move_to(cr, (p2->a1).x, (p2->a1).y); 
        for(j=0;j<(cd->ring_size);j++)
          { 
            cairo_curve_to(cr, (p2->a2).x, (p2->a2).y, (p2->a3).x, (p2->a3).y, (p2->a4).x, (p2->a4).y);
            p2++;
          }
        if(chart_z>0.0)
          {
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else
          {
            cairo_fill(cr);
          }
      }
  }
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[16])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);

    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=0.0;

    qrs[4]=2.0*(qi*qj+qk*qr);
    qrs[5]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[6]=2.0*(qj*qk-qi*qr);
    qrs[7]=0.0;

    qrs[8]=2.0*(qi*qk-qj*qr);
    qrs[9]=2.0*(qj*qk+qi*qr);
    qrs[10]=1.0-2.0*(qi*qi+qj*qj);
    qrs[11]=0.0;

    qrs[12]=0.0;
    qrs[13]=0.0;
    qrs[14]=0.0;
    qrs[15]=1.0;
  }
static void histogram_chart_rotate_base(struct histogram_chart *hc, gdouble qrs[16])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0; 
    gdouble w=0.0;  
    struct rectangle3d *p1=&g_array_index(hc->base_t, struct rectangle3d, 0);

    for(i=0;i<(hc->base_t->len);i++)
      {
        cr1=(p1->b1).x;
        cr2=(p1->b1).y;
        cr3=(p1->b1).z;         
        (p1->b1).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->b1).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->b1).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        ((p1->b1).x)/=w;
        ((p1->b1).y)/=w;
        ((p1->b1).z)/=w;
        cr1=(p1->b2).x;
        cr2=(p1->b2).y;
        cr3=(p1->b2).z;         
        (p1->b2).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->b2).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->b2).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        ((p1->b2).x)/=w;
        ((p1->b2).y)/=w;
        ((p1->b2).z)/=w;
        cr1=(p1->b3).x;
        cr2=(p1->b3).y;
        cr3=(p1->b3).z;         
        (p1->b3).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->b3).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->b3).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        ((p1->b3).x)/=w;
        ((p1->b3).y)/=w;
        ((p1->b3).z)/=w;
        cr1=(p1->b4).x;
        cr2=(p1->b4).y;
        cr3=(p1->b4).z;         
        (p1->b4).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->b4).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->b4).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        ((p1->b4).x)/=w;
        ((p1->b4).y)/=w;
        ((p1->b4).z)/=w;
        p1++;
      }  

    //Rotate reference vector.
    cr1=hc->rx[0];
    cr2=hc->rx[1];
    cr3=hc->rx[2];          
    hc->rx_t[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
    hc->rx_t[1]=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6]);
    hc->rx_t[2]=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10]);

    cr1=hc->ry[0];
    cr2=hc->ry[1];
    cr3=hc->ry[2];          
    hc->ry_t[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
    hc->ry_t[1]=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6]);
    hc->ry_t[2]=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10]); 

    cr1=hc->rz[0];
    cr2=hc->rz[1];
    cr3=hc->rz[2];          
    hc->rz_t[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
    hc->rz_t[1]=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6]);
    hc->rz_t[2]=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10]); 

    //Rotate top points.
    cr1=hc->c1[0];
    cr2=hc->c1[1];
    cr3=hc->c1[2];          
    hc->c1_t[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
    hc->c1_t[1]=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
    hc->c1_t[2]=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
    w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
    (hc->c1_t[0])/=w;
    (hc->c1_t[1])/=w;
    (hc->c1_t[2])/=w;

    cr1=hc->c2[0];
    cr2=hc->c2[1];
    cr3=hc->c2[2];          
    hc->c2_t[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
    hc->c2_t[1]=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
    hc->c2_t[2]=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
    w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
    (hc->c2_t[0])/=w;
    (hc->c2_t[1])/=w;
    (hc->c2_t[2])/=w; 

    cr1=hc->c3[0];
    cr2=hc->c3[1];
    cr3=hc->c3[2];          
    hc->c3_t[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
    hc->c3_t[1]=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
    hc->c3_t[2]=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
    w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
    (hc->c3_t[0])/=w;
    (hc->c3_t[1])/=w;
    (hc->c3_t[2])/=w; 
  }
static void chart_data_rotate_cubes(struct chart_data *cd, gdouble qrs[16])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0; 
    gdouble w=0.0;  
    struct cube3d *p1=&g_array_index(cd->d_t, struct cube3d, 0);
    
    for(i=0;i<(cd->d_t->len);i++)
      {
        cr1=(p1->c1).x;
        cr2=(p1->c1).y;
        cr3=(p1->c1).z;         
        (p1->c1).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->c1).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->c1).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        ((p1->c1).x)/=w;
        ((p1->c1).y)/=w;
        ((p1->c1).z)/=w;
        cr1=(p1->c2).x;
        cr2=(p1->c2).y;
        cr3=(p1->c2).z;         
        (p1->c2).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->c2).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->c2).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        ((p1->c2).x)/=w;
        ((p1->c2).y)/=w;
        ((p1->c2).z)/=w;
        cr1=(p1->c3).x;
        cr2=(p1->c3).y;
        cr3=(p1->c3).z;         
        (p1->c3).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->c3).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->c3).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        ((p1->c3).x)/=w;
        ((p1->c3).y)/=w;
        ((p1->c3).z)/=w;
        cr1=(p1->c4).x;
        cr2=(p1->c4).y;
        cr3=(p1->c4).z;         
        (p1->c4).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->c4).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->c4).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        ((p1->c4).x)/=w;
        ((p1->c4).y)/=w;
        ((p1->c4).z)/=w;
        cr1=(p1->c5).x;
        cr2=(p1->c5).y;
        cr3=(p1->c5).z;         
        (p1->c5).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->c5).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->c5).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        ((p1->c5).x)/=w;
        ((p1->c5).y)/=w;
        ((p1->c5).z)/=w;
        cr1=(p1->c6).x;
        cr2=(p1->c6).y;
        cr3=(p1->c6).z;         
        (p1->c6).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->c6).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->c6).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        ((p1->c6).x)/=w;
        ((p1->c6).y)/=w;
        ((p1->c6).z)/=w;
        cr1=(p1->c7).x;
        cr2=(p1->c7).y;
        cr3=(p1->c7).z;         
        (p1->c7).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->c7).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->c7).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        ((p1->c7).x)/=w;
        ((p1->c7).y)/=w;
        ((p1->c7).z)/=w;
        cr1=(p1->c8).x;
        cr2=(p1->c8).y;
        cr3=(p1->c8).z;         
        (p1->c8).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->c8).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->c8).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        ((p1->c8).x)/=w;
        ((p1->c8).y)/=w;
        ((p1->c8).z)/=w;
        p1++;
      } 
  }
static void chart_data_rotate(struct chart_data *cd, gdouble qrs[16])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0; 
    gdouble w=0.0;  

    struct point3d *p1=&g_array_index(cd->d_t, struct point3d, 0);    
    for(i=0;i<(cd->d_t->len);i++)
      {
        cr1=(p1->x);
        cr2=(p1->y);
        cr3=(p1->z);         
        (p1->x)=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->y)=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->z)=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        (p1->x)/=w;
        (p1->y)/=w;
        (p1->z)/=w;
        p1++;
      }    
  }
static void chart_data_rotate_aligned(struct chart_data_aligned *cd, gdouble qrs[16])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0; 
    gdouble w=0.0;  

    struct arc *p1=(struct arc*)(cd->d_t);
    for(i=0;i<(cd->len);i++)
      {
        cr1=(p1->a1).x;
        cr2=(p1->a1).y;
        cr3=(p1->a1).z;         
        (p1->a1).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->a1).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->a1).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        ((p1->a1).x)/=w;
        ((p1->a1).y)/=w;
        ((p1->a1).z)/=w;
        cr1=(p1->a2).x;
        cr2=(p1->a2).y;
        cr3=(p1->a2).z;         
        (p1->a2).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->a2).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->a2).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        ((p1->a2).x)/=w;
        ((p1->a2).y)/=w;
        ((p1->a2).z)/=w;
        cr1=(p1->a3).x;
        cr2=(p1->a3).y;
        cr3=(p1->a3).z;         
        (p1->a3).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->a3).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->a3).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        ((p1->a3).x)/=w;
        ((p1->a3).y)/=w;
        ((p1->a3).z)/=w;
        cr1=(p1->a4).x;
        cr2=(p1->a4).y;
        cr3=(p1->a4).z;         
        (p1->a4).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->a4).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->a4).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        ((p1->a4).x)/=w;
        ((p1->a4).y)/=w;
        ((p1->a4).z)/=w;
        p1++;
     }
  }
int compare_cubes(const void *a, const void *b)
  {
    return ((((struct cube3d*)a)->c1.z)-(((struct cube3d*)b)->c1.z));
  }
static void set_projection_matrix(gdouble qrs[16], gdouble angle, gdouble near, gdouble far)
  {
    gdouble scale=1.0/tan(angle*0.5*G_PI/180.0);

    qrs[0]=scale;qrs[1]=0.0;qrs[2]=0.0;qrs[3]=0.0;
    qrs[4]=0.0;qrs[5]=scale;qrs[6]=0.0;qrs[7]=0.0;
    qrs[8]=0.0;qrs[9]=0.0;qrs[10]=near/far;qrs[11]=0.0;
    qrs[12]=0.0;qrs[13]=0.0;qrs[14]=-(1.0-near/far);qrs[15]=1.0;
  }
static void matrix_multiply(const gdouble in1[16], const gdouble in2[16], gdouble out[16])
  {
    out[0]=in1[0]*in2[0]+in1[4]*in2[1]+in1[8]*in2[2]+in1[12]*in2[3];
    out[1]=in1[1]*in2[0]+in1[5]*in2[1]+in1[9]*in2[2]+in1[13]*in2[3];
    out[2]=in1[2]*in2[0]+in1[6]*in2[1]+in1[10]*in2[2]+in1[14]*in2[3];
    out[3]=in1[3]*in2[0]+in1[7]*in2[1]+in1[11]*in2[2]+in1[15]*in2[3];

    out[4]=in1[0]*in2[4]+in1[4]*in2[5]+in1[8]*in2[6]+in1[12]*in2[7];
    out[5]=in1[1]*in2[4]+in1[5]*in2[5]+in1[9]*in2[6]+in1[13]*in2[7];
    out[6]=in1[2]*in2[4]+in1[6]*in2[5]+in1[10]*in2[6]+in1[14]*in2[7];
    out[7]=in1[3]*in2[4]+in1[7]*in2[5]+in1[11]*in2[6]+in1[15]*in2[7];

    out[8]=in1[0]*in2[8]+in1[4]*in2[9]+in1[8]*in2[10]+in1[12]*in2[11];
    out[9]=in1[1]*in2[8]+in1[5]*in2[9]+in1[9]*in2[10]+in1[13]*in2[11];
    out[10]=in1[2]*in2[8]+in1[6]*in2[9]+in1[10]*in2[10]+in1[14]*in2[11];
    out[11]=in1[3]*in2[8]+in1[7]*in2[9]+in1[11]*in2[10]+in1[15]*in2[11];

    out[12]=in1[0]*in2[12]+in1[4]*in2[13]+in1[8]*in2[14]+in1[12]*in2[15];
    out[13]=in1[1]*in2[12]+in1[5]*in2[13]+in1[9]*in2[14]+in1[13]*in2[15];
    out[14]=in1[2]*in2[12]+in1[6]*in2[13]+in1[10]*in2[14]+in1[14]*in2[15];
    out[15]=in1[3]*in2[12]+in1[7]*in2[13]+in1[11]*in2[14]+in1[15]*in2[15];
  }
/*
    This is some exellent work done by Ramsundar Shandilya. Note the following for the original work
    and the rational behind it.
    
    https://medium.com/@ramshandilya/draw-smooth-curves-through-a-set-of-points-in-ios-34f6d73c8f9

    https://github.com/Ramshandilya/Bezier

    The MIT License (MIT)

    Copyright (c) 2015 Ramsundar Shandilya

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
  
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

    This is a translation of the original Swift code to C. It makes the function easy to use with GTK+,
    cairo and glib.

    For arrays with 3d points. Just calculate x and y.
*/
static GArray* control_points_from_coords_3d(const GArray *data_points3d)
  {  
    gint i=0;
    GArray *control_points=NULL;      
    //Number of Segments
    gint count=0;
    if(data_points3d!=NULL) count=data_points3d->len-1;
    gdouble *fCP=NULL;
    gdouble *sCP=NULL;

    if(count>0)
      {
        fCP=(gdouble*)g_malloc(2*count*sizeof(gdouble));
        sCP=(gdouble*)g_malloc(2*count*sizeof(gdouble));
      }
        
    //P0, P1, P2, P3 are the points for each segment, where P0 & P3 are the knots and P1, P2 are the control points.
    if(count<1||data_points3d==NULL)
      {
        //Return NULL.
        control_points=NULL;
        g_warning("Can't get control points from coordinates. NULL returned.\n");
      }
    else if(count==1)
      {
        struct point3d P0=g_array_index(data_points3d, struct point3d, 0);
        struct point3d P3=g_array_index(data_points3d, struct point3d, 1);

        //Calculate First Control Point
        //3P1 = 2P0 + P3
        struct point3d P1;
        P1.x=(2.0*P0.x+P3.x)/3.0;
        P1.y=(2.0*P0.y+P3.y)/3.0;

        *(fCP)=P1.x;
        *(fCP+1)=P1.y;

        //Calculate second Control Point
        //P2 = 2P1 - P0
        struct point3d P2;
        P2.x=(2.0*P1.x-P0.x);
        P2.y=(2.0*P1.y-P0.x);

        *(sCP)=P2.x;
        *(sCP+1)=P2.y;     
      }
    else
      {
        gdouble *rhs=(gdouble*)g_malloc(2*count*sizeof(gdouble));
        gdouble *a=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *b=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *c=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble rhsValueX=0;
        gdouble rhsValueY=0;
        struct point3d P0;
        struct point3d P3;        
        gdouble m=0;
        gdouble b1=0;
        gdouble r2x=0;
        gdouble r2y=0;
        gdouble P1_x=0;
        gdouble P1_y=0;
   
        for(i=0;i<count;i++)
          {
            P0=g_array_index(data_points3d, struct point3d, i);
            P3=g_array_index(data_points3d, struct point3d, i+1);

            if(i==0)
              {
                *(a)=0.0;
                *(b)=2.0;
                *(c)=1.0;

                //rhs for first segment
                rhsValueX=P0.x+2.0*P3.x;
                rhsValueY=P0.y+2.0*P3.y;
              }
            else if(i==count-1)
              {
                *(a+i)=2.0;
                *(b+i)=7.0;
                *(c+i)=0.0;

                //rhs for last segment
                rhsValueX=8.0*P0.x+P3.x;
                rhsValueY=8.0*P0.y+P3.y;
              }
            else
              {
                *(a+i)=1.0;
                *(b+i)=4.0;
                *(c+i)=1.0;

                rhsValueX=4.0*P0.x+2.0*P3.x;
                rhsValueY=4.0*P0.y+2.0*P3.y;
              }
            *(rhs+i*2)=rhsValueX;
            *(rhs+i*2+1)=rhsValueY;
          }

        //Solve Ax=B. Use Tridiagonal matrix algorithm a.k.a Thomas Algorithm
        for(i=1;i<count;i++)
          {
            m=(*(a+i))/(*(b+i-1));

            b1=(*(b+i))-m*(*(c+i-1));
            *(b+i)=b1;

            r2x=(*(rhs+i*2))-m*(*(rhs+(i-1)*2));
            r2y=(*(rhs+i*2+1))-m*(*(rhs+(i-1)*2+1));

            *(rhs+i*2)=r2x;
            *(rhs+i*2+1)=r2y;
          }
        
        //Last control Point
        gdouble lastControlPointX=(*(rhs+2*count-2))/(*(b+count-1));
        gdouble lastControlPointY=(*(rhs+2*count-1))/(*(b+count-1));

        *(fCP+2*count-2)=lastControlPointX;
        *(fCP+2*count-1)=lastControlPointY;

        gdouble controlPointX=0;
        gdouble controlPointY=0;

        for(i=count-2;i>=0;--i)
          {
            controlPointX=(*(rhs+i*2)-(*(c+i))*(*(fCP+(i+1)*2)))/(*(b+i));
            controlPointY=(*(rhs+i*2+1)-(*(c+i))*(*(fCP+(i+1)*2+1)))/(*(b+i));

             *(fCP+i*2)=controlPointX;
             *(fCP+i*2+1)=controlPointY; 
          }

        //Compute second Control Points from first.
        for(i=0;i<count;i++)
          {
            if(i==count-1)
              {
                P3=g_array_index(data_points3d, struct point3d, i+1);
                P1_x=(*(fCP+i*2));
                P1_y=(*(fCP+i*2+1));

                controlPointX=(P3.x+P1_x)/2.0;
                controlPointY=(P3.y+P1_y)/2.0;

                *(sCP+count*2-2)=controlPointX;
                *(sCP+count*2-1)=controlPointY;
              }
            else
              {
                P3=g_array_index(data_points3d, struct point3d, i+1);                
                P1_x=(*(fCP+(i+1)*2));
                P1_y=(*(fCP+(i+1)*2+1));

                controlPointX=2.0*P3.x-P1_x;
                controlPointY=2.0*P3.y-P1_y;

                *(sCP+i*2)=controlPointX;
                *(sCP+i*2+1)=controlPointY;
              }

          }

        control_points=g_array_sized_new(FALSE, FALSE, sizeof(struct controls2d), count);
        g_array_set_size(control_points, count);
        struct controls2d *cp=&g_array_index(control_points, struct controls2d, 0);;
        for(i=0;i<count;i++)
          {
            (cp->x1)=(*(fCP+i*2));
            (cp->y1)=(*(fCP+i*2+1));
            (cp->x2)=(*(sCP+i*2));
            (cp->y2)=(*(sCP+i*2+1));
            cp++;
          }

        g_free(rhs);
        g_free(a);
        g_free(b);
        g_free(c);
     }

    if(fCP!=NULL) g_free(fCP);
    if(sCP!=NULL) g_free(sCP);

    return control_points;
  }
static void initialize_bezier_ring(struct histogram_chart *hc, struct arc **ring, struct arc **ring_t, gint sections, gdouble radius, gdouble max_z)
  {
    /*
         Bezier control points. 
         https://en.wikipedia.org/wiki/Composite_B%C3%A9zier_curve#Approximating_circular_arcs 

         Get the points in one arc of the ring and rotate them for the other points in the ring.
      Make one complete ring and copy that to make all the cylinders. Need points for each cylinder
      since the perspective matrix will change individual points in each cylinder. Non affine.
    */  
    gint i=0;
    gint j=0;
    gint k=0;
    //Translate x and y points for each ring.
    gdouble move_x=-((hc->columns)*(hc->outside_square))/2.0+(hc->outside_square)/2.0;
    gdouble move_y=-((hc->rows)*(hc->outside_square))/2.0+(hc->outside_square)/2.0;
    //Get one section arc.
    gdouble arc_top=-(2.0*G_PI/sections)/2.0;
    gdouble arc_offset=-2.0*G_PI/sections;
    gdouble start_x=radius*cos(arc_top);
    gdouble start_y=radius*sin(arc_top);
    gdouble end_x=start_x;
    gdouble end_y=-start_y;
    gdouble c1_x=(4.0*radius-start_x)/3.0;
    gdouble c1_y=((radius-start_x)*(3.0*radius-start_x))/(3.0*start_y);
    gdouble c2_x=c1_x;
    gdouble c2_y=-c1_y;
    gdouble offset1=0.0;
    gdouble offset2=0.0;

    //Just get one ring.
    GArray *one_ring=g_array_sized_new(FALSE, FALSE, sizeof(struct arc), sections);
    g_array_set_size(one_ring, sections);

    *ring=(struct arc*)aligned_alloc(8, sizeof(struct arc)*(hc->rows)*(hc->columns)*sections*2);
    *ring_t=(struct arc*)aligned_alloc(8, sizeof(struct arc)*(hc->rows)*(hc->columns)*sections*2);
    if((one_ring->data)==NULL||*ring==NULL||*ring_t==NULL)
      {
        g_warning("Cylinder allocation error in initialize_bezier_ring().\n");
        goto end;
      }

    struct arc *p1=&g_array_index(one_ring, struct arc, 0);
    struct arc *p2=&g_array_index(one_ring, struct arc, 1);

    //Rotate the points in the section to align with the zero point on the unit circle.
    offset1=cos(arc_top);
    offset2=sin(arc_top);
    (p1->a4).x=start_x*offset1-start_y*offset2;
    (p1->a4).y=start_x*offset2+start_y*offset1;
    (p1->a4).z=0.0;
    (p1->a3).x=c1_x*offset1-c1_y*offset2;
    (p1->a3).y=c1_x*offset2+c1_y*offset1;
    (p1->a3).z=0.0;
    (p1->a2).x=c2_x*offset1-c2_y*offset2;
    (p1->a2).y=c2_x*offset2+c2_y*offset1;
    (p1->a2).z=0.0;
    (p1->a1).x=end_x*offset1-end_y*offset2;
    (p1->a1).y=end_x*offset2+end_y*offset1;
    (p1->a1).z=0.0;

    //Get a single ring.
    offset1=cos(arc_offset);
    offset2=sin(arc_offset);
    for(i=1;i<sections;i++)
      {
        (p2->a1).x=(p1->a1).x*offset1-(p1->a1).y*offset2;
        (p2->a1).y=(p1->a1).x*offset2+(p1->a1).y*offset1;
        (p2->a1).z=0.0;
        (p2->a2).x=(p1->a2).x*offset1-(p1->a2).y*offset2;
        (p2->a2).y=(p1->a2).x*offset2+(p1->a2).y*offset1;
        (p2->a2).z=0.0;
        (p2->a3).x=(p1->a3).x*offset1-(p1->a3).y*offset2;
        (p2->a3).y=(p1->a3).x*offset2+(p1->a3).y*offset1;
        (p2->a3).z=0.0;
        (p2->a4).x=(p1->a4).x*offset1-(p1->a4).y*offset2;
        (p2->a4).y=(p1->a4).x*offset2+(p1->a4).y*offset1;
        (p2->a4).z=0.0;
        p1++;p2++;
      }

    //Fill arrays to be graphed.
    struct arc *p3=(*ring);
    for(i=0;i<(hc->rows);i++)
      {
        for(j=0;j<(hc->columns);j++)
          {
            p1=&g_array_index(one_ring, struct arc, 0);
            for(k=0;k<(one_ring->len);k++)
              {
                //Bottom circle.
                (p3->a1).x=(p1->a1).x+move_x;
                (p3->a1).y=(p1->a1).y+move_y;
                (p3->a1).z=(p1->a1).z;
                (p3->a2).x=(p1->a2).x+move_x;
                (p3->a2).y=(p1->a2).y+move_y;
                (p3->a2).z=(p1->a2).z;
                (p3->a3).x=(p1->a3).x+move_x;
                (p3->a3).y=(p1->a3).y+move_y;
                (p3->a3).z=(p1->a3).z;
                (p3->a4).x=(p1->a4).x+move_x;
                (p3->a4).y=(p1->a4).y+move_y;
                (p3->a4).z=(p1->a4).z;
                (p3->a4).z=(p1->a4).z;
                (p3->cl).r=0.0;
                (p3->cl).g=1.0;
                (p3->cl).b=0.0;
                (p3->cl).a=1.0;
                p1++;p3++;
              }
            p1=&g_array_index(one_ring, struct arc, 0);
            for(k=0;k<(one_ring->len);k++)
              {
                //Top circle.
                (p3->a1).x=(p1->a1).x+move_x;
                (p3->a1).y=(p1->a1).y+move_y;
                (p3->a1).z=(p1->a1).z;
                (p3->a2).x=(p1->a2).x+move_x;
                (p3->a2).y=(p1->a2).y+move_y;
                (p3->a2).z=(p1->a2).z;
                (p3->a3).x=(p1->a3).x+move_x;
                (p3->a3).y=(p1->a3).y+move_y;
                (p3->a3).z=(p1->a3).z;
                (p3->a4).x=(p1->a4).x+move_x;
                (p3->a4).y=(p1->a4).y+move_y;
                (p3->a4).z=(p1->a4).z;
                (p3->cl).r=0.0;
                (p3->cl).g=1.0;
                (p3->cl).b=0.0;
                (p3->cl).a=1.0;
                p1++;p3++;
              }
             move_x+=(hc->outside_square);
           }
         move_x-=(hc->outside_square)*(hc->columns);
         move_y+=(hc->outside_square);
       }
       
    g_array_free(one_ring, TRUE);

    end:
    return;
  }
int compare_cylinders16(const void *a, const void *b)
  {
    return (((((struct cylinder16*)a)->ar1).a1.z)-((((struct cylinder16*)b)->ar1).a1.z));
  }
int compare_cylinders24(const void *a, const void *b)
  {
    return (((((struct cylinder24*)a)->ar1).a1.z)-((((struct cylinder24*)b)->ar1).a1.z));
  }



