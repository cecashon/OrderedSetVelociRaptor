/*
    From Python Playground and boids.py
    https://github.com/electronut/pp
    by Mahesh Venkitachalam

    Try a C version with cairo and GTK+. Add depth or z direction for 3d vectors. Sort the 
vectors from back to front before drawing. Test a couple different methods for getting rotations
from a velocity vector and apply it to a cube.

    There is a problem with getting rotations from a velocity vector. You can get 2 out of 3 
rotations. Take a look at velocity1.c to get a better understanding of the problem.

    gcc -Wall boids2.c -o boids2 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu20.04 and GTK3.24

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<stdlib.h>

struct boid3d{
  guint shape_id;
  gdouble x;
  gdouble y;
  gdouble z;
  gdouble vx;
  gdouble vy;
  gdouble vz;
  gdouble tail_color[4];
};
GArray *boids=NULL;

struct velocity3d{
  gdouble vx;
  gdouble vy;
  gdouble vz;
};

struct quad{
  gint id;
  gdouble z;
};

//For adding more drawings.
enum{ 
  ARROW
}; 

//Reference cube.
gdouble cube[24]={0.0, 20.0, 20.0,   0.0, 20.0, -20.0,   0.0, -20.0, -20.0,   0.0, -20.0, 20.0,
                  -40.0, 20.0, 20.0,   -40.0, 20.0, -20.0,   -40.0, -20.0, -20.0,   -40.0, -20.0, 20.0};
gdouble cube_rot[24];

//Starting boids.
gint num_boids=80;
//max magnitude of velocities calculated by "rules"
gdouble max_rule_val=0.03;
//max maginitude of final velocity
gdouble max_vel=2.0;

gdouble cur_width=500;
gdouble cur_height=500;

static guint tick_id=0;
static guint drawing_id=0;

//For the performance check boxes.
static gboolean frame_rate=FALSE;
static gboolean draw_time=FALSE;
static gboolean rules_time=FALSE;

static void show_frame_rate(GtkToggleButton *button, gpointer data);
static void show_draw_time(GtkToggleButton *button, gpointer data);
static void show_rules_time(GtkToggleButton *button, gpointer data);
static void change_drawing(GtkComboBox *combo, gpointer data);
//Draw the main window transparent blue.
static gboolean draw_main_window(GtkWidget *window, cairo_t *cr, gpointer data);
static void initialize_boids();
static void apply_rules();
static void apply_bc();
static gboolean draw_boids(GtkWidget *da, cairo_t *cr, gpointer data);
static void scatter_boids(GtkWidget *button, gpointer data);
static void add_boid(GtkWidget *button, gpointer data);
static gboolean start_draw_boids(GtkWidget *da, GdkFrameClock *frame_clock, gpointer data);
gint compare_boids_z(const void *a, const void *b);
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9]);
static void rotate_cube(gdouble qrs[9]);
static void rotate_cube2(gdouble qrs[9]);
static void get_vector_rotations(gdouble v[3], gdouble qrs[9]);
static void draw_cube(cairo_t *cr, gdouble scale);
int compare_quads(const void *a, const void *b);
static void exit_program(GtkWidget *widget, GtkWidget *da);

int main(int argc, char **argv)
 {
   gtk_init(&argc, &argv);

   GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
   gtk_window_set_title(GTK_WINDOW(window), "Boids");
   gtk_window_set_default_size(GTK_WINDOW(window), 500, 500);
   gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
   gtk_widget_set_app_paintable(window, TRUE);
   //Try to set transparency of main window.
   GdkScreen *screen=gtk_widget_get_screen(window); 
   if(gdk_screen_is_composited(screen))
     {
       GdkVisual *visual=gdk_screen_get_rgba_visual(screen);
       gtk_widget_set_visual(window, visual);
     }
   else
     {
       g_print("Can't set window transparency.\n");
     } 
   g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), NULL);

   initialize_boids();

   GtkWidget *da=gtk_drawing_area_new();
   gtk_widget_set_hexpand(da, TRUE);
   gtk_widget_set_vexpand(da, TRUE);
   g_signal_connect(da, "draw", G_CALLBACK(draw_boids), NULL);
   g_signal_connect(window, "destroy", G_CALLBACK(exit_program), da);

   GtkWidget *button1=gtk_button_new_with_label("Scatter");
   gtk_widget_set_hexpand(button1, TRUE);
   g_signal_connect(button1, "clicked", G_CALLBACK(scatter_boids), NULL);

   GtkWidget *button2=gtk_button_new_with_label("Add");
   gtk_widget_set_hexpand(button2, TRUE);
   g_signal_connect(button2, "clicked", G_CALLBACK(add_boid), NULL);

   GtkWidget *combo_drawing=gtk_combo_box_text_new();
   gtk_widget_set_hexpand(combo_drawing, TRUE);
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "0", "Arrow");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "1", "Circle Arrow");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 2, "2", "Cube Arrow"); 
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 3, "3", "Cube Arrow2");     
   gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
   g_signal_connect(combo_drawing, "changed", G_CALLBACK(change_drawing), NULL);

   //Some checks for performance.
   GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
   gtk_widget_set_hexpand(check_frame, TRUE);
   gtk_widget_set_name(check_frame, "check_frame");
   g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), NULL);

   GtkWidget *check_draw=gtk_check_button_new_with_label("Show Draw Time");
   gtk_widget_set_hexpand(check_draw, TRUE);
   gtk_widget_set_name(check_draw, "check_draw");
   g_signal_connect(check_draw, "toggled", G_CALLBACK(show_draw_time), NULL);  

   GtkWidget *check_rules=gtk_check_button_new_with_label("Show Rules Time");
   gtk_widget_set_hexpand(check_rules, TRUE);
   gtk_widget_set_name(check_rules, "check_rules");
   g_signal_connect(check_rules, "toggled", G_CALLBACK(show_rules_time), NULL); 

   GtkWidget *grid=gtk_grid_new();
   gtk_grid_set_column_homogeneous(GTK_GRID(grid), TRUE);
   gtk_grid_attach(GTK_GRID(grid), da, 0, 0, 3, 1);
   gtk_grid_attach(GTK_GRID(grid), combo_drawing, 0, 1, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), button2, 0, 2, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), button1, 1, 1, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), check_frame, 1, 2, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), check_draw, 2, 1, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), check_rules, 2, 2, 1, 1);
   
   gtk_container_add(GTK_CONTAINER(window), grid);

   tick_id=gtk_widget_add_tick_callback(da, (GtkTickCallback)start_draw_boids, NULL, NULL);
   
   gchar *css_string=g_strdup("#check_frame{background: rgba(0,255,255,1.0);} #check_draw{background: rgba(0,255,255,1.0);}  #check_rules{background: rgba(0,255,255,1.0);}");
   GError *css_error=NULL;
   GtkCssProvider *provider=gtk_css_provider_new();
   gtk_css_provider_load_from_data(provider, css_string, -1, &css_error);
   gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
   if(css_error!=NULL)
     {
       g_print("CSS loader error %s\n", css_error->message);
       g_error_free(css_error);
     }
   g_object_unref(provider);
   g_free(css_string);

   gtk_widget_show_all(window);

   gtk_main();

   g_array_free(boids, TRUE);

   return 0;  
 }
static void show_frame_rate(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_draw_time(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) draw_time=TRUE;
    else draw_time=FALSE;
  }
static void show_rules_time(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) rules_time=TRUE;
    else rules_time=FALSE;
  }
static void change_drawing(GtkComboBox *combo, gpointer data)
  {
    drawing_id=gtk_combo_box_get_active(combo);
  }
static gboolean draw_main_window(GtkWidget *window, cairo_t *cr, gpointer data)
  {
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.3);
    cairo_paint(cr);

    return FALSE;
  }
static void initialize_boids()
  {
    gint i=0;
    gdouble angle1=0;
    gdouble angle2=0;
    boids=g_array_sized_new(FALSE, FALSE, sizeof(struct boid3d), num_boids);
    g_array_set_size(boids, num_boids);
    struct boid3d *p1=&g_array_index(boids, struct boid3d, 0);

    for(i=0;i<num_boids;i++)
      {
        (*p1).shape_id=ARROW;
        angle1=2.0*G_PI*g_random_double();
        angle2=g_random_double_range(-G_PI/2.0, G_PI/2.0);
        //Position.
        (*p1).x=250.0+10.0*sin(angle2)*cos(angle1);
        (*p1).y=250.0+10.0*sin(angle2)*sin(angle1);
        (*p1).z=10.0*cos(angle2);
        //Unit velocity vector.
        (*p1).vx=sin(angle2)*cos(angle1);
        (*p1).vy=sin(angle2)*sin(angle1);
        (*p1).vz=cos(angle2);
        (*p1).tail_color[0]=0.0;
        (*p1).tail_color[1]=1.0;  
        (*p1).tail_color[2]=1.0;  
        (*p1).tail_color[3]=1.0;     
        p1++;
      }
  }
static void apply_rules()
  {
    gint i=0;
    gint j=0;
    gdouble temp1=0.0;
    gdouble temp2=0.0;
    gdouble temp3=0.0;
    gdouble temp4=0.0;
    gint counter=0;
    gdouble x1=0;
    gdouble x2=0;
    gdouble y1=0;
    gdouble y2=0;
    gdouble z1=0.0;
    gdouble z2=0.0;
    gdouble mag=0.0;
    GArray *vel=g_array_sized_new(FALSE, FALSE, sizeof(struct velocity3d), boids->len);
    g_array_set_size(vel, boids->len);
    GArray *D=g_array_sized_new(FALSE, FALSE, sizeof(gboolean), (boids->len)*(boids->len));
    g_array_set_size(D, (boids->len)*(boids->len));
    GArray *sum=g_array_sized_new(FALSE, FALSE, sizeof(gint), boids->len);
    g_array_set_size(sum, boids->len);
    GArray *vel2_3=g_array_sized_new(FALSE, FALSE, sizeof(struct velocity3d), boids->len);
    g_array_set_size(sum, boids->len);
    struct velocity3d *v1=&g_array_index(vel, struct velocity3d, 0);
    struct boid3d *p1=&g_array_index(boids, struct boid3d, 0);
    gboolean *d1=&g_array_index(D, gboolean, 0);
    gint *s1=&g_array_index(sum, gint, 0);
    struct velocity3d *v2=&g_array_index(vel2_3, struct velocity3d, 0);

    //D = self.distMatrix < 25.0
    for(i=0;i<boids->len;i++)
      {
        x1=(p1+i)->x;
        y1=(p1+i)->y;
        z1=(p1+i)->z;
        counter=0;
        for(j=0;j<boids->len;j++)
          {
            x2=(p1+j)->x;
            y2=(p1+j)->y;
            z2=(p1+j)->z;
            temp1=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)+(z2-z1)*(z2-z1));
            //If the boid is bouncing off the front or back don't group.
            if( ((p1+i)->z>95.0) || ((p1+i)->z<-95.0) ) *d1=FALSE;
            else if(temp1<15.0)
              {
                counter++;
                *d1=TRUE;
              }
            else *d1=FALSE;
            d1++; 
          } 
        *(s1+i)=counter;     
      }

    //vel = self.pos*D.sum(axis=1).reshape(self.N, 1) - D.dot(self.pos)
    d1=&g_array_index(D, gboolean, 0);
    for(i=0;i<boids->len;i++)
      {
        temp2=0.0;
        temp3=0.0;
        temp4=0.0;
        for(j=0;j<boids->len;j++)
          {
            temp2+=(gdouble)(*d1)*((p1+j)->x);
            temp3+=(gdouble)(*d1)*((p1+j)->y);
            temp4+=(gdouble)(*d1)*((p1+j)->z);
            d1++;
          }
        (*v1).vx=((p1+i)->x)*(gdouble)(*(s1+i))-temp2;
        (*v1).vy=((p1+i)->y)*(gdouble)(*(s1+i))-temp3;
        (*v1).vz=((p1+i)->z)*(gdouble)(*(s1+i))-temp4;
        v1++;
      }

    //self.limit(vel, self.maxRuleVal)
    v1=&g_array_index(vel, struct velocity3d, 0);
    for(i=0;i<boids->len;i++)
      {
        mag=sqrt((v1->vx)*(v1->vx)+(v1->vy)*(v1->vy)+(v1->vz)*(v1->vz));
        if(mag>max_rule_val)
          {
            (*v1).vx=(v1->vx)*max_rule_val/mag;
            (*v1).vy=(v1->vy)*max_rule_val/mag;
            (*v1).vz=(v1->vz)*max_rule_val/mag;
          }
        v1++;
      }

    //D = self.distMatrix < 50.0
    d1=&g_array_index(D, gboolean, 0);
    for(i=0;i<boids->len;i++)
      {
        x1=(p1+i)->x;
        y1=(p1+i)->y;
        z1=(p1+i)->z;
        for(j=0;j<boids->len;j++)
          {
            x2=(p1+j)->x;
            y2=(p1+j)->y;
            z2=(p1+j)->z;
            temp1=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)+(z2-z1)*(z2-z1));
            //If the boid is bouncing off the front or back don't group.
            if( ((p1+i)->z>95.0) || ((p1+i)->z<-95.0) ) *d1=FALSE;
            else if(temp1<50.0)
              {
                *d1=TRUE;
              }
            else *d1=FALSE;
            d1++;
          }    
      }

    //vel2 = D.dot(self.vel)
    d1=&g_array_index(D, gboolean, 0);
    for(i=0;i<boids->len;i++)
      {
        temp2=0.0;
        temp3=0.0;
        temp4=0.0;
        for(j=0;j<boids->len;j++)
          {
            temp2+=(gdouble)(*d1)*((p1+j)->vx);
            temp3+=(gdouble)(*d1)*((p1+j)->vy);
            temp4+=(gdouble)(*d1)*((p1+j)->vz);
            d1++;
          }
        (*v2).vx=temp2;
        (*v2).vy=temp3;
        (*v2).vz=temp4;
        v2++;
      }

    //self.limit(vel2, self.maxRuleVel)
    v1=&g_array_index(vel, struct velocity3d, 0);
    v2=&g_array_index(vel2_3, struct velocity3d, 0);
    for(i=0;i<boids->len;i++)
      {
        mag=sqrt((v2->vx)*(v2->vx)+(v2->vy)*(v2->vy)+(v2->vz)*(v2->vz));
        if(mag>max_rule_val)
          {
            (*v2).vx=(v2->vx)*max_rule_val/mag;
            (*v2).vy=(v2->vy)*max_rule_val/mag;
            (*v2).vz=(v2->vz)*max_rule_val/mag;
          }
        v1++;v2++;
      }

    //vel += vel2
    v1=&g_array_index(vel, struct velocity3d, 0);
    v2=&g_array_index(vel2_3, struct velocity3d, 0);
    for(i=0;i<boids->len;i++)
      {
        (*v1).vx+=(v2->vx);
        (*v1).vy+=(v2->vy);
        (*v1).vz+=(v2->vz);
        v1++;v2++;
      }

    //vel3 = D.dot(self.pos) - self.pos
    p1=&g_array_index(boids, struct boid3d, 0);
    v2=&g_array_index(vel2_3, struct velocity3d, 0);
    d1=&g_array_index(D, gboolean, 0);
    for(i=0;i<boids->len;i++)
      {
        temp2=0.0;
        temp3=0.0;
        temp4=0.0;
        for(j=0;j<boids->len;j++)
          {
            temp2+=(gdouble)(*d1)*((p1+j)->x);
            temp3+=(gdouble)(*d1)*((p1+j)->y);
            temp4+=(gdouble)(*d1)*((p1+j)->z);
            d1++;
          }
        (*v2).vx=temp2-((p1+i)->x);
        (*v2).vy=temp3-((p1+i)->y);
        (*v2).vz=temp4-((p1+i)->z);
        v2++;
      }

    //self.limit(vel3, self.maxRuleVel)
    v2=&g_array_index(vel2_3, struct velocity3d, 0);
    for(i=0;i<boids->len;i++)
      {
        mag=sqrt((v2->vx)*(v2->vx)+(v2->vy)*(v2->vy)+(v2->vz)*(v2->vz));
        if(mag>max_rule_val)
          {
            (*v2).vx=(v2->vx)*max_rule_val/mag;
            (*v2).vy=(v2->vy)*max_rule_val/mag;
            (*v2).vz=(v2->vz)*max_rule_val/mag;
          }
        v2++;
      }

    //vel += vel3
    v1=&g_array_index(vel, struct velocity3d, 0);
    v2=&g_array_index(vel2_3, struct velocity3d, 0);
    for(i=0;i<boids->len;i++)
      {
        (*v1).vx+=(v2->vx);
        (*v1).vy+=(v2->vx);
        (*v1).vz+=(v2->vz);
        v1++;v2++;
      }

    //self.vel += self.applyRules()
    p1=&g_array_index(boids, struct boid3d, 0);
    v1=&g_array_index(vel, struct velocity3d, 0);
    for(i=0;i<boids->len;i++)
      {
        (*p1).vx+=v1->vx;
        (*p1).vy+=v1->vy;
        (*p1).vz+=v1->vz;
        p1++;v1++;
      }

    //self.limit(self.vel, self.maxVel)
    p1=&g_array_index(boids, struct boid3d, 0);
    for(i=0;i<boids->len;i++)
      {
        mag=sqrt((p1->vx)*(p1->vx)+(p1->vy)*(p1->vy)+(p1->vz)*(p1->vz));
        if(mag>max_vel)
          {
            (*p1).vx=(p1->vx)*max_vel/mag;
            (*p1).vy=(p1->vy)*max_vel/mag;
            (*p1).vz=(p1->vz)*max_vel/mag;
          }
        p1++;
      }

    //self.pos += self.vel
    p1=&g_array_index(boids, struct boid3d, 0);
    for(i=0;i<boids->len;i++)
      {
        (*p1).x+=(p1->vx);
        (*p1).y+=(p1->vy);
        (*p1).z+=(p1->vz);
        p1++;
      }
    
    g_array_free(vel, TRUE);
    g_array_free(D, TRUE);
    g_array_free(sum, TRUE);
    g_array_free(vel2_3, TRUE);    
  }
static void apply_bc()
  {
    gint i=0;
    gdouble delta_r=2.0;
    gdouble w1=cur_width;
    gdouble h1=cur_height;
    struct boid3d *p1=&g_array_index(boids, struct boid3d, 0);

    for(i=0;i<boids->len;i++)
      {
        //Boundries.
        if((p1->x)>(w1+delta_r)) (*p1).x+=-w1-delta_r;            
        if((p1->x)<(-w1-delta_r)) (*p1).x+=w1+delta_r;           
        if((p1->y)>(h1+delta_r)) (*p1).y+=-h1-delta_r;            
        if((p1->y)<(-h1-delta_r)) (*p1).y+=h1+delta_r;
        //Bounce the boids off the front and back.
        if((p1->z)>(100.0+delta_r))
          {
            (*p1).vy=-(p1->vy);
            (*p1).vz=-(p1->vz);
          }
        if((p1->z)<(-100.0-delta_r))
          {
            (*p1).vy=-(p1->vy);
            (*p1).vz=-(p1->vz);
          }             
        p1++;
      }
  }
static void scatter_boids(GtkWidget *button, gpointer data)
  {
    gint i=0;
    gdouble angle1=0.0;
    gdouble angle2=0.0;
    struct boid3d *p1=&g_array_index(boids, struct boid3d, 0);

    for(i=0;i<boids->len;i++)
      {        
        angle1=2.0*G_PI*g_random_double();
        angle2=g_random_double_range(-G_PI/2.0, G_PI/2.0);
        //Position.
        (*p1).x=cur_width*sin(angle2)*cos(angle1);
        (*p1).y=cur_height*sin(angle2)*sin(angle1);
        (*p1).z=100.0*cos(angle2);
        //Unit velocity vector.
        (*p1).vx=sin(angle2)*cos(angle1);
        (*p1).vy=sin(angle2)*sin(angle1);
        (*p1).vz=cos(angle2);   
        p1++;
      }
  }
static void add_boid(GtkWidget *button, gpointer data)
  {
    gdouble angle1=0.0;
    gdouble angle2=0.0;
    struct boid3d b;

    b.shape_id=ARROW;
    b.x=g_random_double_range(0.0, cur_width);
    b.y=g_random_double_range(0.0, cur_height);
    b.z=g_random_double_range(-100.0, 100);
    angle1=2.0*G_PI*g_random_double();
    angle2=g_random_double_range(-G_PI/2.0, G_PI/2.0);
    b.vx=sin(angle2)*cos(angle1);
    b.vy=sin(angle2)*sin(angle1);
    b.vz=cos(angle2);
    b.tail_color[0]=0.0;
    b.tail_color[1]=0.0;
    b.tail_color[2]=0.0;
    b.tail_color[3]=1.0;      
    g_array_append_val(boids, b);
  }
static gboolean start_draw_boids(GtkWidget *da, GdkFrameClock *frame_clock, gpointer data)
  {
    if(frame_rate)
      {
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);       
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start=gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    if(rules_time)
      {
        GTimer *timer=g_timer_new();
        apply_rules();
        apply_bc();
        g_print("Rules Time %f\n", g_timer_elapsed(timer, NULL));
        g_timer_destroy(timer);
      }
    else
      {
        apply_rules();
        apply_bc();
      }

    gtk_widget_queue_draw(da);
    return G_SOURCE_CONTINUE;
  }
static gboolean draw_boids(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    gint i=0;
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    cur_width=width;
    cur_height=height;

    GTimer *timer=NULL;
    if(draw_time)
      {
        timer=g_timer_new();
      }

    //Paint transparent.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.0);
    cairo_paint(cr);

    //Sort the boids based on z-value. Draw the small or distant boids in the back.
    g_array_sort(boids, compare_boids_z);

    gdouble scale=0.0;
    struct boid3d *p1=&g_array_index(boids, struct boid3d, 0);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
    cairo_set_line_width(cr, 5.0);
    //cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
    for(i=0;i<boids->len;i++)
      {
        //Scale based on Z.
        scale=(150.0+(p1->z))/200.0;

        //Draw a circle
        if(drawing_id==1)
          {
            cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 0.6);
            cairo_arc(cr, p1->x, p1->y, scale*20.0, 0.0, 2.0*G_PI);
            cairo_fill(cr);
          }

        /*
          Draw a cube. Try different methods for getting the rotations from the velocity
          vector. For drawing_id==2, use a reference vector to rotate the cube so the red
          line stays on top. For example, if you have a pen pointing up, normal to a table,
          roll the pen at this point so the clip on the pen faces up.
        */
        if((drawing_id==2||drawing_id==3)&&((p1->vz)>=0.0))
          {
            cairo_save(cr);
            if(drawing_id==2)
              {
                cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
                gdouble yaw=atan((p1->vy)/(p1->vx));
                gdouble pitch=atan((p1->vz)/sqrt((p1->vx)*(p1->vx)+(p1->vy)*(p1->vy)));
                gdouble qrs[9];
                if((p1->vx)>0.0) quaternion_rotation(yaw, 0.0, pitch, qrs);
                else quaternion_rotation(yaw+G_PI, 0.0, pitch+G_PI, qrs);
                rotate_cube(qrs);

                //Add a small amount of roll to the cube so you can see the ends.
                quaternion_rotation(0.0, G_PI/12.0, 0.0, qrs);                 
                rotate_cube2(qrs);

                cairo_set_line_width(cr, 2.0);
                cairo_translate(cr, p1->x, p1->y);
                draw_cube(cr, scale);
              }
            if(drawing_id==3)
              {
                cairo_set_line_width(cr, 2.0);
                gdouble v[3];
                gdouble qrs[9];
                v[0]=p1->vx;
                v[1]=p1->vy;
                v[2]=p1->vz;
                get_vector_rotations(v, qrs);
                rotate_cube(qrs);
                cairo_translate(cr, p1->x, p1->y);
                draw_cube(cr, scale);
                cairo_set_line_width(cr, 5.0);                
              }
            cairo_restore(cr);
          }
 
        //Draw the arrow.
        cairo_save(cr);
        cairo_set_line_width(cr, 5.0);
        cairo_set_source_rgba(cr, p1->tail_color[0], p1->tail_color[1], p1->tail_color[2], p1->tail_color[3]);
        cairo_move_to(cr, p1->x, p1->y);
        cairo_line_to(cr, (p1->x)+15.0*scale*(p1->vx), p1->y+15.0*scale*(p1->vy));
        cairo_stroke(cr);
        //The yellow arrow head.
        cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
        cairo_translate(cr, (p1->x)+15.0*scale*(p1->vx), p1->y+15.0*scale*(p1->vy));
        cairo_move_to(cr, 0.0, 0.0);
        cairo_line_to(cr, 20.0*scale*(p1->vx), 20.0*scale*(p1->vy));        
        cairo_rotate(cr, G_PI/2.0);
        cairo_line_to(cr, 3.0*scale*(p1->vx), 3.0*scale*(p1->vy));
        cairo_line_to(cr, -3.0*scale*(p1->vx), -3.0*scale*(p1->vy));
        cairo_rotate(cr, -G_PI/2.0);
        cairo_line_to(cr, 20.0*scale*(p1->vx), 20.0*scale*(p1->vy)); 
        cairo_close_path(cr);
        cairo_fill(cr);
        cairo_restore(cr); 

        if((drawing_id==2||drawing_id==3)&&((p1->vz)<0.0))
          {
            cairo_save(cr);            
            if(drawing_id==2)
              {
                cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
                gdouble yaw=atan((p1->vy)/(p1->vx));
                gdouble pitch=atan((p1->vz)/sqrt((p1->vx)*(p1->vx)+(p1->vy)*(p1->vy)));
                gdouble qrs[9];
                if((p1->vx)>0.0) quaternion_rotation(yaw, 0.0, pitch, qrs);
                else quaternion_rotation(yaw+G_PI, 0.0, pitch+G_PI, qrs);
                rotate_cube(qrs);

                //Add a small amount of roll to the cube so you can see the ends.
                quaternion_rotation(0.0, G_PI/12.0, 0.0, qrs);                 
                rotate_cube2(qrs);

                cairo_set_line_width(cr, 2.0);
                cairo_translate(cr, p1->x, p1->y);
                draw_cube(cr, scale);
              }
            if(drawing_id==3)
              {
                cairo_set_line_width(cr, 2.0);
                gdouble v[3];
                gdouble qrs[9];
                v[0]=p1->vx;
                v[1]=p1->vy;
                v[2]=p1->vz;
                get_vector_rotations(v, qrs);
                rotate_cube(qrs);
                cairo_translate(cr, p1->x, p1->y);
                draw_cube(cr, scale);
              }
            
            cairo_restore(cr);
          }    
        p1++;
      }

    if(timer!=NULL)
      {
        g_print("Draw Time %f\n", g_timer_elapsed(timer, NULL));
        g_timer_destroy(timer);
      }

    return FALSE;
  }
int compare_boids_z(const void *a, const void *b)
  {
    return(  (((struct boid3d*)a)->z) - (((struct boid3d*)b)->z) );
  }
static void rotate_cube(gdouble qrs[9])
  {
    gint i=0;
    for(i=0;i<8;i++)
      {        
        cube_rot[3*i]=(cube[3*i]*qrs[0])+(cube[3*i+1]*qrs[1])+(cube[3*i+2]*qrs[2]);
        cube_rot[3*i+1]=(cube[3*i]*qrs[3])+(cube[3*i+1]*qrs[4])+(cube[3*i+2]*qrs[5]);
        cube_rot[3*i+2]=(cube[3*i]*qrs[6])+(cube[3*i+1]*qrs[7])+(cube[3*i+2]*qrs[8]);
      }
  }
static void rotate_cube2(gdouble qrs[9])
  {
    gint i=0;
    gdouble x=0.0;
    gdouble y=0.0;
    gdouble z=0.0;

    for(i=0;i<8;i++)
      {   
        x=cube_rot[3*i];
        y=cube_rot[3*i+1]; 
        z=cube_rot[3*i+2];    
        cube_rot[3*i]=(x*qrs[0])+(y*qrs[1])+(z*qrs[2]);
        cube_rot[3*i+1]=(x*qrs[3])+(y*qrs[4])+(z*qrs[5]);
        cube_rot[3*i+2]=(x*qrs[6])+(y*qrs[7])+(z*qrs[8]);
      }
  }
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
static void get_vector_rotations(gdouble v[3], gdouble qrs[9])
  {
    //https://stackoverflow.com/questions/24705225/opengl-rotation-from-velocity-vector
    gdouble bx[3];
    gdouble by[3];
    gdouble bz[3];
    gdouble mag=sqrt(v[0]*v[0]+v[1]*v[1]+v[2]+v[2]);

    if(mag>1.0)
      {
        bx[0]=v[0]/mag;
        bx[1]=v[1]/mag;
        bx[2]=v[2]/mag;
      }
    else
      {
        bx[0]=v[0];
        bx[1]=v[1];
        bx[2]=v[2];
      }

    if((fabs(v[2])<fabs(v[0]))&&(fabs(v[2])<fabs(v[1])))
      {
        mag=sqrt(v[1]*v[1]+v[0]*v[0]);
        if(mag>1.0)
          {
            by[0]=v[1]/mag;
            by[1]=-v[0]/mag;
            by[2]=0.0;
          }
        else
          {
            by[0]=v[1];
            by[1]=-v[0];
            by[2]=0.0;
          }
      }
    else
      {
        mag=sqrt(v[2]*v[2]+v[0]*v[0]);
        if(mag>1.0)
          {
            by[0]=v[2]/mag;
            by[1]=0.0;
            by[2]=-v[0]/mag;
          }
        else
          {
            by[0]=v[2];
            by[1]=0.0;
            by[2]=-v[0];
          }
      }

    bz[0]=by[1]*bx[2]-by[2]*bx[1];
    bz[1]=by[2]*bx[0]-by[0]*bx[2];
    bz[2]=by[0]*bx[1]-by[1]*bx[0];

    qrs[0]=bx[0]; qrs[1]=by[0]; qrs[2]=bz[0];
    qrs[3]=bx[1]; qrs[4]=by[1]; qrs[5]=bz[1];
    qrs[6]=bx[2]; qrs[7]=by[2]; qrs[8]=bz[2];
  }
static void draw_cube(cairo_t *cr, gdouble scale)
  {
    gint i=0;
    //An array to sort quads.
    struct quad *quads_start=g_malloc(6*sizeof(struct quad));
    struct quad *quads=quads_start;
    
    (*quads).id=0;
    (*quads).z=cube_rot[2]+cube_rot[5]+cube_rot[8]+cube_rot[11];
    quads++;
    (*quads).id=1;
    (*quads).z=cube_rot[14]+cube_rot[17]+cube_rot[20]+cube_rot[23];
    quads++;
    (*quads).id=2;
    (*quads).z=cube_rot[2]+cube_rot[5]+cube_rot[17]+cube_rot[14];
    quads++;
    (*quads).id=3;
    (*quads).z=cube_rot[5]+cube_rot[8]+cube_rot[20]+cube_rot[17];
    quads++;
    (*quads).id=4;
    (*quads).z=cube_rot[8]+cube_rot[11]+cube_rot[23]+cube_rot[20];
    quads++;
    (*quads).id=5;
    (*quads).z=cube_rot[11]+cube_rot[2]+cube_rot[14]+cube_rot[23];
    quads++;

    //Sort array based on z values.
    //qsort(quads_start, 6, sizeof(struct quad), compare_quads);

    quads=quads_start;
    for(i=0;i<6;i++)
      {
        if((*quads).id==0)
          {
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.8);
            cairo_move_to(cr, cube_rot[0]*scale, cube_rot[1]*scale);
            cairo_line_to(cr, cube_rot[3]*scale, cube_rot[4]*scale);
            cairo_line_to(cr, cube_rot[6]*scale, cube_rot[7]*scale);
            cairo_line_to(cr, cube_rot[9]*scale, cube_rot[10]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else if((*quads).id==1)
          {
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.8);
            cairo_move_to(cr, cube_rot[12]*scale, cube_rot[13]*scale);
            cairo_line_to(cr, cube_rot[15]*scale, cube_rot[16]*scale);
            cairo_line_to(cr, cube_rot[18]*scale, cube_rot[19]*scale);
            cairo_line_to(cr, cube_rot[21]*scale, cube_rot[22]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else if((*quads).id==2)
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, cube_rot[0]*scale, cube_rot[1]*scale);
            cairo_line_to(cr, cube_rot[3]*scale, cube_rot[4]*scale);
            cairo_line_to(cr, cube_rot[15]*scale, cube_rot[16]*scale);
            cairo_line_to(cr, cube_rot[12]*scale, cube_rot[13]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else if((*quads).id==3)
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, cube_rot[3]*scale, cube_rot[4]*scale);
            cairo_line_to(cr, cube_rot[6]*scale, cube_rot[7]*scale);
            cairo_line_to(cr, cube_rot[18]*scale, cube_rot[19]*scale);
            cairo_line_to(cr, cube_rot[15]*scale, cube_rot[16]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        //Add a reference line.
        else if((*quads).id==4)
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, cube_rot[6]*scale, cube_rot[7]*scale);
            cairo_line_to(cr, cube_rot[9]*scale, cube_rot[10]*scale);
            cairo_line_to(cr, cube_rot[21]*scale, cube_rot[22]*scale);
            cairo_line_to(cr, cube_rot[18]*scale, cube_rot[19]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
            cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
            cairo_move_to(cr, ((cube_rot[6]*scale)+(cube_rot[9]*scale))/2.0, ((cube_rot[7]*scale)+(cube_rot[10]*scale))/2.0);
            cairo_line_to(cr, ((cube_rot[21]*scale)+(cube_rot[18]*scale))/2.0, ((cube_rot[22]*scale)+(cube_rot[19]*scale))/2.0);
            cairo_stroke(cr);
          }
        else
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, cube_rot[9]*scale, cube_rot[10]*scale);
            cairo_line_to(cr, cube_rot[0]*scale, cube_rot[1]*scale);
            cairo_line_to(cr, cube_rot[12]*scale, cube_rot[13]*scale);
            cairo_line_to(cr, cube_rot[21]*scale, cube_rot[22]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        quads++;
        }

    g_free(quads_start);  
  }
int compare_quads(const void *a, const void *b)
  {
    return(  (((struct quad*)a)->z) - (((struct quad*)b)->z) );
  }
static void exit_program(GtkWidget *widget, GtkWidget *da)
  {
    if(tick_id!=0) gtk_widget_remove_tick_callback(da, tick_id);
    gtk_main_quit();
  }








