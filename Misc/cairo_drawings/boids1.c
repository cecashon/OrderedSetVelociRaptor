/*
    From Python Playground and boids.py
    https://github.com/electronut/pp
    by Mahesh Venkitachalam

    Try a C version with cairo and GTK+. Might need some more work.

    gcc -Wall boids1.c -o boids1 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu20.04 and GTK3.24

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>

struct boid{
  gdouble x;
  gdouble y;
  gdouble vx;
  gdouble vy;
};
GArray *boids=NULL;

struct velocity{
  gdouble vx;
  gdouble vy;
};

//Start with 100 boids.
gint num_boids=100;
//max magnitude of velocities calculated by "rules"
gdouble max_rule_val=0.03;
//max maginitude of final velocity
gdouble max_vel=2.0;

gdouble cur_width=500;
gdouble cur_height=500;

static guint tick_id=0;

static void initialize_boids();
static void apply_rules();
static void apply_bc();
static gboolean draw_boids(GtkWidget *da, cairo_t *cr, gpointer data);
static void scatter_boids(GtkWidget *button, gpointer data);
static void add_boid(GtkWidget *button, gpointer data);
static gboolean start_draw_boids(GtkWidget *da, GdkFrameClock *frame_clock, gpointer data);
static void exit_program(GtkWidget *widget, GtkWidget *da);

int main(int argc, char **argv)
 {
   gtk_init(&argc, &argv);

   GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
   gtk_window_set_title(GTK_WINDOW(window), "Boids");
   gtk_window_set_default_size(GTK_WINDOW(window), 500, 500);
   gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);

   initialize_boids();

   GtkWidget *da=gtk_drawing_area_new();
   gtk_widget_set_hexpand(da, TRUE);
   gtk_widget_set_vexpand(da, TRUE);
   g_signal_connect(da, "draw", G_CALLBACK(draw_boids), NULL);
   g_signal_connect(window, "destroy", G_CALLBACK(exit_program), da);

   GtkWidget *button1=gtk_button_new_with_label("Scatter Boids");
   gtk_widget_set_hexpand(button1, TRUE);
   g_signal_connect(button1, "clicked", G_CALLBACK(scatter_boids), NULL);

   GtkWidget *button2=gtk_button_new_with_label("Add Boid");
   gtk_widget_set_hexpand(button2, TRUE);
   g_signal_connect(button2, "clicked", G_CALLBACK(add_boid), NULL);

   GtkWidget *grid=gtk_grid_new();
   gtk_grid_attach(GTK_GRID(grid), da, 0, 0, 2, 1);
   gtk_grid_attach(GTK_GRID(grid), button1, 0, 1, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), button2, 1, 1, 1, 1);
   
   gtk_container_add(GTK_CONTAINER(window), grid);

   tick_id=gtk_widget_add_tick_callback(da, (GtkTickCallback)start_draw_boids, NULL, NULL);

   gtk_widget_show_all(window);

   gtk_main();

   g_array_free(boids, TRUE);

   return 0;  
 }
static void initialize_boids()
  {
    gint i=0;
    gdouble angle=0;
    boids=g_array_sized_new(FALSE, FALSE, sizeof(struct boid), num_boids);
    g_array_set_size(boids, num_boids);
    struct boid *p1=&g_array_index(boids, struct boid, 0);

    for(i=0;i<num_boids;i++)
      {
        (*p1).x=250.0+10.0*g_random_double_range(-1.0, 1.0);
        (*p1).y=250.0+10.0*g_random_double_range(-1.0, 1.0);
        angle=2.0*G_PI*g_random_double();
        (*p1).vx=cos(angle); 
        (*p1).vy=sin(angle);    
        p1++;
      }
  }
static void apply_rules()
  {
    gint i=0;
    gint j=0;
    gdouble temp1=0.0;
    gdouble temp2=0.0;
    gdouble temp3=0.0;
    gint counter=0;
    gdouble x1=0;
    gdouble x2=0;
    gdouble y1=0;
    gdouble y2=0;
    gdouble mag=0.0;
    GArray *vel=g_array_sized_new(FALSE, FALSE, sizeof(struct velocity), boids->len);
    g_array_set_size(vel, boids->len);
    GArray *D=g_array_sized_new(FALSE, FALSE, sizeof(gboolean), (boids->len)*(boids->len));
    g_array_set_size(D, (boids->len)*(boids->len));
    GArray *sum=g_array_sized_new(FALSE, FALSE, sizeof(gint), boids->len);
    g_array_set_size(sum, boids->len);
    GArray *vel2_3=g_array_sized_new(FALSE, FALSE, sizeof(struct velocity), boids->len);
    g_array_set_size(sum, boids->len);
    struct velocity *v1=&g_array_index(vel, struct velocity, 0);
    struct boid *p1=&g_array_index(boids, struct boid, 0);
    gboolean *d1=&g_array_index(D, gboolean, 0);
    gint *s1=&g_array_index(sum, gint, 0);
    struct velocity *v2=&g_array_index(vel2_3, struct velocity, 0);

    //D = self.distMatrix < 25.0
    for(i=0;i<boids->len;i++)
      {
        x1=(p1+i)->x;
        y1=(p1+i)->y;
        counter=0;
        for(j=0;j<boids->len;j++)
          {
            x2=(p1+j)->x;
            y2=(p1+j)->y;
            temp1=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
            if(temp1<15.0)
              {
                counter++;
                *d1=TRUE;
              }
            else *d1=FALSE;
            d1++; 
          } 
        *(s1+i)=counter;     
      }

    //vel = self.pos*D.sum(axis=1).reshape(self.N, 1) - D.dot(self.pos)
    d1=&g_array_index(D, gboolean, 0);
    for(i=0;i<boids->len;i++)
      {
        temp2=0.0;
        temp3=0.0;
        for(j=0;j<boids->len;j++)
          {
            temp2+=(gdouble)(*d1)*((p1+j)->x);
            temp3+=(gdouble)(*d1)*((p1+j)->y);
            d1++;
          }
        (*v1).vx=((p1+i)->x)*(gdouble)(*(s1+i))-temp2;
        (*v1).vy=((p1+i)->y)*(gdouble)(*(s1+i))-temp3;
        v1++;
      }

    //self.limit(vel, self.maxRuleVal)
    v1=&g_array_index(vel, struct velocity, 0);
    for(i=0;i<boids->len;i++)
      {
        mag=sqrt((v1->vx)*(v1->vx)+(v1->vy)*(v1->vy));
        if(mag>max_rule_val)
          {
            (*v1).vx=(v1->vx)*max_rule_val/mag;
            (*v1).vy=(v1->vy)*max_rule_val/mag;
          }
        v1++;
      }

    //D = self.distMatrix < 50.0
    d1=&g_array_index(D, gboolean, 0);
    for(i=0;i<boids->len;i++)
      {
        x1=(p1+i)->x;
        y1=(p1+i)->y;
        for(j=0;j<boids->len;j++)
          {
            x2=(p1+j)->x;
            y2=(p1+j)->y;
            temp1=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
            if(temp1<50.0)
              {
                *d1=TRUE;
              }
            else *d1=FALSE;
            d1++;
          }    
      }

    //vel2 = D.dot(self.vel)
    d1=&g_array_index(D, gboolean, 0);
    for(i=0;i<boids->len;i++)
      {
        temp2=0.0;
        temp3=0.0;
        for(j=0;j<boids->len;j++)
          {
            temp2+=(gdouble)(*d1)*((p1+j)->vx);
            temp3+=(gdouble)(*d1)*((p1+j)->vy);
            d1++;
          }
        (*v2).vx=temp2;
        (*v2).vy=temp3;
        v2++;
      }

    //self.limit(vel2, self.maxRuleVel)
    v1=&g_array_index(vel, struct velocity, 0);
    v2=&g_array_index(vel2_3, struct velocity, 0);
    for(i=0;i<boids->len;i++)
      {
        mag=sqrt((v2->vx)*(v2->vx)+(v2->vy)*(v2->vy));
        if(mag>max_rule_val)
          {
            (*v2).vx=(v2->vx)*max_rule_val/mag;
            (*v2).vy=(v2->vy)*max_rule_val/mag;
          }
        v1++;v2++;
      }

    //vel += vel2
    v1=&g_array_index(vel, struct velocity, 0);
    v2=&g_array_index(vel2_3, struct velocity, 0);
    for(i=0;i<boids->len;i++)
      {
        (*v1).vx+=(v2->vx);
        (*v1).vy+=(v2->vy);
        v1++;v2++;
      }

    //vel3 = D.dot(self.pos) - self.pos
    p1=&g_array_index(boids, struct boid, 0);
    v2=&g_array_index(vel2_3, struct velocity, 0);
    d1=&g_array_index(D, gboolean, 0);
    for(i=0;i<boids->len;i++)
      {
        temp2=0.0;
        temp3=0.0;
        for(j=0;j<boids->len;j++)
          {
            temp2+=(gdouble)(*d1)*((p1+j)->x);
            temp3+=(gdouble)(*d1)*((p1+j)->y);
            d1++;
          }
        (*v2).vx=temp2-((p1+i)->x);
        (*v2).vy=temp3-((p1+i)->y);
        v2++;
      }

    //self.limit(vel3, self.maxRuleVel)
    v2=&g_array_index(vel2_3, struct velocity, 0);
    for(i=0;i<boids->len;i++)
      {
        mag=sqrt((v2->vx)*(v2->vx)+(v2->vy)*(v2->vy));
        if(mag>max_rule_val)
          {
            (*v2).vx=(v2->vx)*max_rule_val/mag;
            (*v2).vy=(v2->vy)*max_rule_val/mag;
          }
        v2++;
      }

    //vel += vel3
    v1=&g_array_index(vel, struct velocity, 0);
    v2=&g_array_index(vel2_3, struct velocity, 0);
    for(i=0;i<boids->len;i++)
      {
        (*v1).vx+=(v2->vx);
        (*v1).vy+=(v2->vx);
        v1++;v2++;
      }

    //self.vel += self.applyRules()
    p1=&g_array_index(boids, struct boid, 0);
    v1=&g_array_index(vel, struct velocity, 0);
    for(i=0;i<boids->len;i++)
      {
        (*p1).vx+=v1->vx;
        (*p1).vy+=v1->vy;
        p1++;v1++;
      }

    //self.limit(self.vel, self.maxVel)
    p1=&g_array_index(boids, struct boid, 0);
    for(i=0;i<boids->len;i++)
      {
        mag=sqrt((p1->vx)*(p1->vx)+(p1->vy)*(p1->vy));
        if(mag>max_vel)
          {
            (*p1).vx=(p1->vx)*max_vel/mag;
            (*p1).vy=(p1->vy)*max_vel/mag;
          }
        p1++;
      }

    //self.pos += self.vel
    p1=&g_array_index(boids, struct boid, 0);
    for(i=0;i<boids->len;i++)
      {
        (*p1).x+=(p1->vx);
        (*p1).y+=(p1->vy);
        p1++;
      }
    
    g_array_free(vel, TRUE);
    g_array_free(D, TRUE);
    g_array_free(sum, TRUE);
    g_array_free(vel2_3, TRUE);    
  }
static void apply_bc()
  {
    gint i=0;
    gdouble delta_r=2.0;
    gdouble w1=cur_width;
    gdouble h1=cur_height;
    struct boid *p1=&g_array_index(boids, struct boid, 0);

    for(i=0;i<boids->len;i++)
      {
        //Boundries.
        if((p1->x)>(w1+delta_r)) (*p1).x+=-w1-delta_r;
        if((p1->x)<(-w1-delta_r)) (*p1).x+=w1+delta_r;
        if((p1->y)>(h1+delta_r)) (*p1).y+=-h1-delta_r;
        if((p1->y)<(-h1-delta_r)) (*p1).y+=h1+delta_r;            
        p1++;
      }
  }
static void scatter_boids(GtkWidget *button, gpointer data)
  {
    gint i=0;
    gdouble angle=0.0;
    struct boid *p1=&g_array_index(boids, struct boid, 0);

    for(i=0;i<boids->len;i++)
      {
        (*p1).x=g_random_double_range(0.0, cur_width);
        (*p1).y=g_random_double_range(0.0, cur_height);
        angle=2.0*G_PI*g_random_double();
        (*p1).vx=cos(angle); 
        (*p1).vy=sin(angle);       
        p1++;
      }
  }
static void add_boid(GtkWidget *button, gpointer data)
  {
    gdouble angle=0.0;
    struct boid b;

    b.x=g_random_double_range(0.0, cur_width);
    b.y=g_random_double_range(0.0, cur_height);
    angle=2.0*G_PI*g_random_double();
    b.vx=cos(angle); 
    b.vy=sin(angle);       
    g_array_append_val(boids, b);  
  }
static gboolean start_draw_boids(GtkWidget *da, GdkFrameClock *frame_clock, gpointer data)
  {
    gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);       
    gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
    gint64 start=gdk_frame_clock_get_history_start(frame_clock);
    gint64 history_len=frame-start;
    GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
    gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
    g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));

    GTimer *timer=g_timer_new();
    apply_rules();
    apply_bc();
    g_print("Time %f\n", g_timer_elapsed(timer, NULL));
    g_timer_destroy(timer);

    gtk_widget_queue_draw(da);
    return G_SOURCE_CONTINUE;
  }
static gboolean draw_boids(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    gint i=0;
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    cur_width=width;
    cur_height=height;

    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    cairo_paint(cr);
   
    //Layout axis for drawing.
    cairo_set_line_width(cr, 1.0);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_rectangle(cr, width/10.0, height/10.0, 8.0*width/10.0, 8.0*height/10.0);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*width/10.0, 5.0*height/10.0);
    cairo_line_to(cr, 9.0*width/10.0, 5.0*height/10.0);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*width/10.0, 1.0*height/10.0);
    cairo_line_to(cr, 5.0*width/10.0, 9.0*height/10.0);
    cairo_stroke(cr);

    struct boid *p1=&g_array_index(boids, struct boid, 0);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
    cairo_set_line_width(cr, 10.0);
    //Draw the first boid yellow.
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_move_to(cr, p1->x, p1->y);
    cairo_line_to(cr, (p1->x)+5.0*(p1->vx), p1->y+5.0*(p1->vy));
    cairo_stroke(cr);
    p1++;
    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
    cairo_set_line_width(cr, 10.0); 
    for(i=1;i<num_boids;i++)
      {
        cairo_move_to(cr, p1->x, p1->y);
        cairo_line_to(cr, (p1->x)+5.0*(p1->vx), p1->y+5.0*(p1->vy));
        cairo_stroke(cr);     
        p1++;
      }
    //Draw the added boids.
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    for(i=num_boids;i<boids->len;i++)
      {
        cairo_move_to(cr, p1->x, p1->y);
        cairo_line_to(cr, (p1->x)+5.0*(p1->vx), p1->y+5.0*(p1->vy));
        cairo_stroke(cr);     
        p1++;
      }

    return FALSE;
  }
static void exit_program(GtkWidget *widget, GtkWidget *da)
  {
    if(tick_id!=0) gtk_widget_remove_tick_callback(da, tick_id);
    gtk_main_quit();
  }








