
/* 
    Test drawing a cube combining 3d rotations, perspective and a stress matrix with cairo.

    gcc -Wall perspective1.c -o perspective1 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<stdlib.h>

//Sort cube faces from back to front.
struct quad{
  gint id;
  gdouble z;
};

//Reference cube and a transformed cube.
gdouble cube[24]={20.0, 20.0, 20.0,   20.0, 20.0, -20.0,   20.0, -20.0, -20.0,   20.0, -20.0, 20.0,
                  -20.0, 20.0, 20.0,   -20.0, 20.0, -20.0,   -20.0, -20.0, -20.0,   -20.0, -20.0, 20.0};
gdouble cube_t[24];

//Some lines across one face of the cube.
gdouble lines[54]={-20.0, -20.0, 16.0,  -20.0, -20.0, 12.0,  -20.0, -20.0, 8.0,  -20.0, -20.0, 4.0,
                  -20.0, -20.0, 0.0,  -20.0, -20.0, -4.0,  -20.0, -20.0, -8.0,  -20.0, -20.0, -12.0,
                  -20.0, -20.0, -16.0,
                  20.0, -20.0, 16.0,  20.0, -20.0, 12.0,  20.0, -20.0, 8.0,  20.0, -20.0, 4.0,
                  20.0, -20.0, 0.0,  20.0, -20.0, -4.0,  20.0, -20.0, -8.0,  20.0, -20.0, -12.0,
                  20.0, -20.0, -16.0,
                  };
gdouble lines_t[54];

//A reference vector. Used for centering the object under perspective.
gdouble v1[3]={0.0, 0.0, -20.0};
gdouble v1_t[3];

//Combo ids for the perspective and stress.
static gint perspective_id=0;
static gint stress_id=0;

//The combo rotation slider values.
static gdouble combo_pitch=0.0;
static gdouble combo_roll=0.0;
static gdouble combo_yaw=0.0;
static gdouble combo_perspective=0.0;

//Tick id for animation frame clock.
static guint tick_id=0;

//For the performance check boxes.
static gboolean frame_rate=FALSE;
static gboolean function_time=FALSE;

//Draw the main window transparent.
static gboolean draw_main_window(GtkWidget *window, cairo_t *cr, gpointer data);
//UI functions for sliders and combo boxes.
static void show_frame_rate(GtkToggleButton *button, gpointer data);
static void show_function_time(GtkToggleButton *button, gpointer data);
static void rotate_drawing(GtkComboBox *combo, gpointer data);
static void set_perspective_id(GtkComboBox *combo, gpointer data);
static void set_stress_id(GtkComboBox *combo, gpointer data);
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data);
//The start of drawing. Get the combo_id's to draw.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer data);
/*
  Some matrix functions for drawing. Reset points, stress matrix, perspective vector, rotation matrix is
  the order of the operations.
*/
static void reset_draw_cube();
static void change_stress(gdouble sm[9]);
static void change_perspective1();
static void change_perspective2();
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9]);
static void rotate_cube(gdouble qrs[9]);
//After the cube points are transformed, draw it.
static void draw_cube(cairo_t *cr, gdouble scale);
int compare_quads(const void *a, const void *b);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Perspective1");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    gtk_widget_set_app_paintable(window, TRUE);
    //Try to set transparency of main window.
    if(gtk_widget_is_composited(window))
      {
        GdkScreen *screen=gtk_widget_get_screen(window);  
        GdkVisual *visual=gdk_screen_get_rgba_visual(screen);
        gtk_widget_set_visual(window, visual);
      }
    else g_print("Can't set window transparency.\n");

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);

    GtkWidget *combo_rotate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_rotate, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 0, "1", "Draw Cube");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 1, "2", "Animate Cube");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_rotate), 0);

    GtkWidget *combo_perspective=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_perspective, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_perspective), 0, "1", "Perspective1");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_perspective), 1, "2", "Perspective2");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_perspective), 0);

    GtkWidget *combo_stress=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_stress, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 0, "1", "No Stress");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 1, "2", "Stress Matrix1");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 2, "3", "Stress Matrix2");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_stress), 0);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);

    GtkWidget *perspective_label=gtk_label_new("Perspective");
    gtk_widget_set_hexpand(perspective_label, TRUE);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider, TRUE);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider, TRUE);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider, TRUE);

    GtkWidget *perspective_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 100.0, 1.0);
    gtk_widget_set_vexpand(perspective_slider, TRUE);

    g_signal_connect(da, "draw", G_CALLBACK(draw_main), NULL);
    g_signal_connect(combo_rotate, "changed", G_CALLBACK(rotate_drawing), da);
    g_signal_connect(combo_perspective, "changed", G_CALLBACK(set_perspective_id), da);
    g_signal_connect(combo_stress, "changed", G_CALLBACK(set_stress_id), da);
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_pitch), da);
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_roll), da);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_yaw), da);
    g_signal_connect(perspective_slider, "change-value", G_CALLBACK(set_perspective), da);

    //Some checks for performance.
    GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
    gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
    g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), NULL);

    GtkWidget *check_functions=gtk_check_button_new_with_label("Show Function Time");
    gtk_widget_set_halign(check_functions, GTK_ALIGN_CENTER);
    g_signal_connect(check_functions, "toggled", G_CALLBACK(show_function_time), NULL);

    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_rotate, 0, 0, 6, 1); 
    gtk_grid_attach(GTK_GRID(grid1), combo_perspective, 0, 1, 6, 1); 
    gtk_grid_attach(GTK_GRID(grid1), combo_stress, 0, 2, 6, 1);         
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 3, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid1), perspective_label, 3, 3, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), perspective_slider, 3, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_frame, 0, 5, 6, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_functions, 0, 6, 6, 1);

    GtkWidget *scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll, TRUE);
    gtk_widget_set_vexpand(scroll, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll), grid1);
   
    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), scroll, TRUE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 250);

    g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), paned1);
   
    gtk_container_add(GTK_CONTAINER(window), paned1);
    
    gtk_widget_show_all(window);

    gtk_main();

    return 0;  
  }
static gboolean draw_main_window(GtkWidget *window, cairo_t *cr, gpointer data)
  {
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.3);
    cairo_paint(cr);

    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
    gint width=gtk_paned_get_position(GTK_PANED(data));
    gint height=gtk_widget_get_allocated_height(window);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_rectangle(cr, width, 0.0, 10, height);
    cairo_fill(cr);

    return FALSE;
  }
static void show_frame_rate(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_function_time(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) function_time=TRUE;
    else function_time=FALSE;
  }
static void rotate_drawing(GtkComboBox *combo, gpointer data)
  {   
    combo_pitch=0.0;
    combo_roll=0.0;
    combo_yaw=0.0;
    combo_perspective=0.0;
 
    if(gtk_combo_box_get_active(combo)==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(data), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(data));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(data), (GtkTickCallback)animate_drawing, NULL, NULL);
          }
      }
    
  }
static void set_perspective_id(GtkComboBox *combo, gpointer data)
  {  
    perspective_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_stress_id(GtkComboBox *combo, gpointer data)
  {  
    stress_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_pitch=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_roll=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_yaw=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>100.0) combo_perspective=100.0;
    else if(value<0.0) combo_perspective=0.0;
    else combo_perspective=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data)
  {
    static gboolean up=TRUE;

    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    if(combo_pitch>360.0) combo_pitch=0.0;
    else combo_pitch+=0.5;
    if(combo_roll>360.0) combo_roll=0.0;
    else combo_roll+=0.5;
    if(combo_yaw>360.0) combo_yaw=0.0;
    else combo_yaw+=0.5;

    if(combo_perspective>100.0) up=FALSE;
    else if(combo_perspective<0.0) up=TRUE; 
    if(up) combo_perspective+=0.5;
    else combo_perspective-=0.5;

    gtk_widget_queue_draw(GTK_WIDGET(drawing));
    return G_SOURCE_CONTINUE;
  }
//The top drawing function.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;

    //Paint background transparent.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.0);
    cairo_paint(cr);

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 3.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Draw in the center
    cairo_translate(cr, width/2.0, height/2.0);
    //Sometimes there are small extra line fragments if CAIRO_LINE_JOIN_MITER is used.
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    gdouble pitch=combo_pitch*G_PI/180.0;
    gdouble roll=combo_roll*G_PI/180.0;
    gdouble yaw=combo_yaw*G_PI/180.0;
    gdouble qrs[9];      
 
    //Time the transforms.
    GTimer *timer1=g_timer_new();
    //Reset to initial values.
    reset_draw_cube();
    //Get a rotation matrix.
    quaternion_rotation(yaw, roll, pitch, qrs);
    //Remember matrix multiplication isn't commutative or the order of matrix multiplication matters.
    if(stress_id==1)
      {
        //A simple stress matrix.
        gdouble sm[9]={2.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0};
        change_stress(sm);
      }
    if(stress_id==2)
      {
        gdouble sm[9]={1.0, 0.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0, 1.0};
        change_stress(sm);
      }
    if(perspective_id==0) change_perspective1();
    if(perspective_id==1) change_perspective2();
    rotate_cube(qrs);
    if(function_time) g_print("Transform time %f\n", g_timer_elapsed(timer1, NULL));

    //Time the drawing.
    g_timer_start(timer1);
    draw_cube(cr, 4.0);
    if(function_time) g_print("     Draw time %f\n", g_timer_elapsed(timer1, NULL));
    g_timer_destroy(timer1);
     
    return FALSE;
  }
static void reset_draw_cube()
  {
    //Start with a new copy of the original cube, lines and reference vector.
    gint i=0;
    for(i=0;i<8;i++)
      {        
        cube_t[3*i]=cube[3*i];
        cube_t[3*i+1]=cube[3*i+1];
        cube_t[3*i+2]=cube[3*i+2];
      }

    for(i=0;i<18;i++)
      {        
        lines_t[3*i]=lines[3*i];
        lines_t[3*i+1]=lines[3*i+1];
        lines_t[3*i+2]=lines[3*i+2];
      }

    v1_t[0]=v1[0];
    v1_t[1]=v1[1];
    v1_t[2]=v1[2];
  }
static void change_stress(gdouble sm[9])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0;
    
    for(i=0;i<8;i++)
      { 
        cr1=cube_t[3*i];
        cr2=cube_t[3*i+1];
        cr3=cube_t[3*i+2];      
        cube_t[3*i]=(cr1*sm[0])+(cr2*sm[1])+(cr3*sm[2]);
        cube_t[3*i+1]=(cr1*sm[3])+(cr2*sm[4])+(cr3*sm[5]);
        cube_t[3*i+2]=(cr1*sm[6])+(cr2*sm[7])+(cr3*sm[8]);
      }

    for(i=0;i<18;i++)
      {   
        cr1=lines_t[3*i];
        cr2=lines_t[3*i+1];
        cr3=lines_t[3*i+2];      
        lines_t[3*i]=(cr1*sm[0])+(cr2*sm[1])+(cr3*sm[2]);
        lines_t[3*i+1]=(cr1*sm[3])+(cr2*sm[4])+(cr3*sm[5]);
        lines_t[3*i+2]=(cr1*sm[6])+(cr2*sm[7])+(cr3*sm[8]);
      }
  }
static void change_perspective1()
  {
    gint i=0;
    gdouble perspective1=combo_perspective/2000.0;
    gdouble perspective2=0.0;

    //Center the perspective object.
    perspective2=(1.0+(perspective1*v1[2])); 
    gdouble translate_z=v1_t[2]-v1_t[2]*perspective2;

    for(i=0;i<8;i++)
      {   
        perspective2=(1.0+(perspective1*cube[3*i+2]));     
        cube_t[3*i]=cube_t[3*i]*perspective2;
        cube_t[3*i+1]=cube_t[3*i+1]*perspective2;
        cube_t[3*i+2]=cube_t[3*i+2]*perspective2+translate_z;
      }
    
    for(i=0;i<18;i++)
      {   
        perspective2=(1.0+(perspective1*lines[3*i+2]));     
        lines_t[3*i]=lines_t[3*i]*perspective2;
        lines_t[3*i+1]=lines_t[3*i+1]*perspective2;
        lines_t[3*i+2]=lines_t[3*i+2]*perspective2+translate_z;
      }

    perspective2=(1.0+(perspective1*v1_t[2]));
    v1_t[0]=v1_t[0]*perspective2;
    v1_t[1]=v1_t[1]*perspective2;
    v1_t[2]=v1_t[2]*perspective2+translate_z;
  }
static void change_perspective2()
  {
    gint i=0;
    gdouble perspective1=combo_perspective/2000.0;
    gdouble perspective2=0.0;

    perspective2=(1.0+(perspective1*v1[2])*3.0); 
    gdouble translate_z=v1_t[2]-v1_t[2]*perspective2;

    for(i=0;i<8;i++)
      {   
        perspective2=(1.0+(perspective1*cube[3*i+2]*3.0));     
        cube_t[3*i]=cube_t[3*i]*perspective2;
        cube_t[3*i+1]=cube_t[3*i+1]*perspective2;
        cube_t[3*i+2]=cube_t[3*i+2]*perspective2+translate_z;
      }

    for(i=0;i<18;i++)
      {   
        perspective2=(1.0+(perspective1*lines[3*i+2]*3.0));     
        lines_t[3*i]=lines_t[3*i]*perspective2;
        lines_t[3*i+1]=lines_t[3*i+1]*perspective2;
        lines_t[3*i+2]=lines_t[3*i+2]*perspective2+translate_z;
      }

    perspective2=(1.0+(perspective1*v1_t[2]));
    v1_t[0]=v1_t[0]*perspective2;
    v1_t[1]=v1_t[1]*perspective2;
    v1_t[2]=v1_t[2]*perspective2+translate_z;
  }
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
static void rotate_cube(gdouble qrs[9])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0;

    for(i=0;i<8;i++)
      { 
        cr1=cube_t[3*i];
        cr2=cube_t[3*i+1];
        cr3=cube_t[3*i+2];         
        cube_t[3*i]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        cube_t[3*i+1]=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        cube_t[3*i+2]=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
      }

    for(i=0;i<18;i++)
      {   
        cr1=lines_t[3*i];
        cr2=lines_t[3*i+1];
        cr3=lines_t[3*i+2];       
        lines_t[3*i]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        lines_t[3*i+1]=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        lines_t[3*i+2]=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
      }

    cr1=v1_t[0];
    cr2=v1_t[1];
    cr3=v1_t[2]; 
    v1_t[0]=cr1*qrs[0]+cr2*qrs[1]+cr3*qrs[2];
    v1_t[1]=cr1*qrs[3]+cr2*qrs[4]+cr3*qrs[5];
    v1_t[2]=cr1*qrs[6]+cr2*qrs[7]+cr3*qrs[8];
  }
static void draw_cube(cairo_t *cr, gdouble scale)
  {
    gint i=0;
    gint j=0;
    gdouble slope_x1=0.0;
    gdouble slope_y1=0.0;
    gdouble slope_x2=0.0;
    gdouble slope_y2=0.0;
    gdouble x1=0.0;
    gdouble y1=0.0;
    gdouble x2=0.0;
    gdouble y2=0.0;
    //An array to sort quads.
    struct quad *quads_start=g_malloc(6*sizeof(struct quad));
    struct quad *quads=quads_start;
    
    (*quads).id=0;
    (*quads).z=cube_t[2]+cube_t[5]+cube_t[8]+cube_t[11];
    quads++;
    (*quads).id=1;
    (*quads).z=cube_t[14]+cube_t[17]+cube_t[20]+cube_t[23];
    quads++;
    (*quads).id=2;
    (*quads).z=cube_t[2]+cube_t[5]+cube_t[17]+cube_t[14];
    quads++;
    (*quads).id=3;
    (*quads).z=cube_t[5]+cube_t[8]+cube_t[20]+cube_t[17];
    quads++;
    (*quads).id=4;
    (*quads).z=cube_t[8]+cube_t[11]+cube_t[23]+cube_t[20];
    quads++;
    (*quads).id=5;
    (*quads).z=cube_t[11]+cube_t[2]+cube_t[14]+cube_t[23];
    quads++;

    //Sort array based on z values.
    qsort(quads_start, 6, sizeof(struct quad), compare_quads);

    //Just draw the top three cube planes.
    quads=quads_start;
    for(i=0;i<6;i++)
      {
        if((*quads).id==0)
          {
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.5);
            cairo_move_to(cr, cube_t[0]*scale, cube_t[1]*scale);
            cairo_line_to(cr, cube_t[3]*scale, cube_t[4]*scale);
            cairo_line_to(cr, cube_t[6]*scale, cube_t[7]*scale);
            cairo_line_to(cr, cube_t[9]*scale, cube_t[10]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else if((*quads).id==1)
          {
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.5);
            cairo_move_to(cr, cube_t[12]*scale, cube_t[13]*scale);
            cairo_line_to(cr, cube_t[15]*scale, cube_t[16]*scale);
            cairo_line_to(cr, cube_t[18]*scale, cube_t[19]*scale);
            cairo_line_to(cr, cube_t[21]*scale, cube_t[22]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else if((*quads).id==2)
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, cube_t[0]*scale, cube_t[1]*scale);
            cairo_line_to(cr, cube_t[3]*scale, cube_t[4]*scale);
            cairo_line_to(cr, cube_t[15]*scale, cube_t[16]*scale);
            cairo_line_to(cr, cube_t[12]*scale, cube_t[13]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);

            //Draw perspective lines.
            cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
            slope_x1=(cube_t[3]-cube_t[0])/10.0;
            slope_y1=(cube_t[4]-cube_t[1])/10.0;
            slope_x2=(cube_t[15]-cube_t[12])/10.0;
            slope_y2=(cube_t[16]-cube_t[13])/10.0;             
            for(j=1;j<10;j++)
              {
                x1=slope_x1*(gdouble)(j);
                y1=slope_y1*(gdouble)(j);
                x2=slope_x2*(gdouble)(j);
                y2=slope_y2*(gdouble)(j);
                cairo_move_to(cr, (cube_t[0]+x1)*scale, (cube_t[1]+y1)*scale);
                cairo_line_to(cr, (cube_t[12]+x2)*scale, (cube_t[13]+y2)*scale);
                cairo_stroke(cr);
              }

          }
        else if((*quads).id==3)
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, cube_t[3]*scale, cube_t[4]*scale);
            cairo_line_to(cr, cube_t[6]*scale, cube_t[7]*scale);
            cairo_line_to(cr, cube_t[18]*scale, cube_t[19]*scale);
            cairo_line_to(cr, cube_t[15]*scale, cube_t[16]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else if((*quads).id==4)
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, cube_t[6]*scale, cube_t[7]*scale);
            cairo_line_to(cr, cube_t[9]*scale, cube_t[10]*scale);
            cairo_line_to(cr, cube_t[21]*scale, cube_t[22]*scale);
            cairo_line_to(cr, cube_t[18]*scale, cube_t[19]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
            //Draw a few more lines for perspective.
            cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
            for(j=0;j<9;j++)
              {
                cairo_move_to(cr, lines_t[3*j]*scale, lines_t[3*j+1]*scale);
                cairo_line_to(cr, lines_t[3*j+27]*scale, lines_t[3*j+28]*scale);
                cairo_stroke(cr);
              }

            cairo_move_to(cr, lines_t[0]*scale, lines_t[1]*scale);
            for(j=1;j<9;j++)
              {
                cairo_line_to(cr, lines_t[3*j]*scale, lines_t[3*j+1]*scale);
              }
            cairo_stroke(cr);

            cairo_move_to(cr, lines_t[27]*scale, lines_t[28]*scale);
            for(j=1;j<9;j++)
              {
                cairo_line_to(cr, lines_t[3*j+27]*scale, lines_t[3*j+28]*scale);
              }
            cairo_stroke(cr);
          }
        else
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, cube_t[9]*scale, cube_t[10]*scale);
            cairo_line_to(cr, cube_t[0]*scale, cube_t[1]*scale);
            cairo_line_to(cr, cube_t[12]*scale, cube_t[13]*scale);
            cairo_line_to(cr, cube_t[21]*scale, cube_t[22]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        quads++;
        }

    g_free(quads_start);  
  }
int compare_quads(const void *a, const void *b)
  {
    return(  (((struct quad*)a)->z) - (((struct quad*)b)->z) );
  }
