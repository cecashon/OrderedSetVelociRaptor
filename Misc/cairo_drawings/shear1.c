
/*
    Test shearing a linear gradient.

    gcc -Wall shear1.c -o shear1 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu18.04 and GTK3.22
*/

#include<gtk/gtk.h>
#include<math.h>

static gdouble shear_x=0.0;
static gdouble shear_y=0.0;
static gdouble shear_x2=0.0;
static gdouble shear_y2=0.0;

static void shear_x_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void shear_y_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void shear_x2_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void shear_y2_spin_changed(GtkSpinButton *spin_button, gpointer data);
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer data);
static void intersection(double A1, double A2, double B1, double B2, double C1, double C2, double D1, double D2, double *out_x, double *out_y) ;

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Shear Test");
    gtk_window_set_default_size(GTK_WINDOW(window), 600, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);
    g_signal_connect(da, "draw", G_CALLBACK(draw_main), NULL);

    GtkAdjustment *shear_x_adj=gtk_adjustment_new(0.0, -300.0, 300.0, 10.0, 0.0, 0.0);
    GtkAdjustment *shear_y_adj=gtk_adjustment_new(0.0, -300.0, 300.0, 10.0, 0.0, 0.0);

    GtkWidget *shear_x_label=gtk_label_new("Shear X");
    GtkWidget *shear_x_spin=gtk_spin_button_new(shear_x_adj, 1, 0);
    g_signal_connect(shear_x_spin, "value-changed", G_CALLBACK(shear_x_spin_changed), da);

    GtkWidget *shear_y_label=gtk_label_new("Shear Y");
    GtkWidget *shear_y_spin=gtk_spin_button_new(shear_y_adj, 1, 0);
    g_signal_connect(shear_y_spin, "value-changed", G_CALLBACK(shear_y_spin_changed), da);

    GtkAdjustment *shear_x2_adj=gtk_adjustment_new(0.0, -200.0, 200.0, 10.0, 0.0, 0.0);
    GtkAdjustment *shear_y2_adj=gtk_adjustment_new(0.0, -200.0, 200.0, 10.0, 0.0, 0.0);

    GtkWidget *shear_x2_label=gtk_label_new("Shear X2");
    GtkWidget *shear_x2_spin=gtk_spin_button_new(shear_x2_adj, 1, 0);
    g_signal_connect(shear_x2_spin, "value-changed", G_CALLBACK(shear_x2_spin_changed), da);

    GtkWidget *shear_y2_label=gtk_label_new("Skear Y2");
    GtkWidget *shear_y2_spin=gtk_spin_button_new(shear_y2_adj, 1, 0);
    g_signal_connect(shear_y2_spin, "value-changed", G_CALLBACK(shear_y2_spin_changed), da);

    GtkWidget *grid=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid), da, 0, 0, 4, 1);
    gtk_grid_attach(GTK_GRID(grid), shear_x_label, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), shear_y_label, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), shear_x2_label, 2, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), shear_y2_label, 3, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), shear_x_spin, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), shear_y_spin, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), shear_x2_spin, 2, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), shear_y2_spin, 3, 2, 1, 1);
      
    gtk_container_add(GTK_CONTAINER(window), grid);

    gtk_widget_show_all(window);

    gtk_main();

    return 0;  
  }
static void shear_x_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    shear_x=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void shear_y_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    shear_y=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void shear_x2_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    shear_x2=gtk_spin_button_get_value(spin_button)/100.0;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void shear_y2_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    shear_y2=gtk_spin_button_get_value(spin_button)/100.0;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    cairo_translate(cr, 150.0, 50.0);
    cairo_scale(cr, 0.5, 0.5);

    //Paint the background.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr);

    //A rectangle.
    double r[8]={100.0, 50.0, 500.0, 50.0, 500.0, 200.0, 100.0, 200.0};

    //Slope of perpendicular line.
    double m_x=-1.0*(r[0]-(r[6]+shear_x));
    double m_y=r[1]-(r[7]);
    
    //Find intersection.
    double A1=r[2];
    double A2=r[3]+shear_y;
    double B1=r[4]+shear_x;
    double B2=r[5]+shear_y;
    double C1=r[0];
    double C2=r[1];
    double D1=r[0]+m_y;
    double D2=r[1]+m_x;
    double out_x=0.0;
    double out_y=0.0;
    intersection(A1, A2, B1, B2, C1, C2, D1, D2, &out_x, &out_y);

    cairo_pattern_t *pattern1=cairo_pattern_create_linear(r[0], r[1], out_x, out_y);
    cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 0.0, 0.0, 1.0, 1.0); 
    cairo_pattern_add_color_stop_rgba(pattern1, 1.0, 1.0, 1.0, 0.0, 1.0);
    cairo_set_source(cr, pattern1); 

    cairo_move_to(cr, r[0], r[1]);
    cairo_line_to(cr, r[2], r[3]+shear_y);
    cairo_line_to(cr, r[4]+shear_x, r[5]+shear_y);
    cairo_line_to(cr, r[6]+shear_x, r[7]);
    cairo_close_path(cr);
    cairo_fill(cr);

    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_move_to(cr, r[0], r[1]);
    cairo_line_to(cr, out_x, out_y); 
    cairo_stroke(cr);
    
    //Skew matrix.
    cairo_translate(cr, 0.0, 400.0);
    cairo_matrix_t matrix;
    cairo_matrix_init(&matrix, 1.0, shear_y2, shear_x2, 1.0, 0.0, 0.0);
    cairo_transform(cr, &matrix);

    cairo_pattern_t *pattern2=cairo_pattern_create_linear(r[0], r[1], r[2], r[3]);
    cairo_pattern_add_color_stop_rgba(pattern2, 0.0, 0.0, 0.0, 1.0, 1.0); 
    cairo_pattern_add_color_stop_rgba(pattern2, 1.0, 1.0, 1.0, 0.0, 1.0);
    cairo_set_source(cr, pattern2); 
 
    cairo_move_to(cr, r[0], r[1]);
    cairo_line_to(cr, r[2], r[3]);
    cairo_line_to(cr, r[4], r[5]);
    cairo_line_to(cr, r[6], r[7]);
    cairo_close_path(cr);
    cairo_fill(cr);
    
    cairo_pattern_destroy(pattern1);
    cairo_pattern_destroy(pattern2);

    return FALSE;
  }
static void intersection(double A1, double A2, double B1, double B2, double C1, double C2, double D1, double D2, double *out_x, double *out_y) 
  { 
    //line AB 
    double a1=B2-A2; 
    double b1=A1-B1; 
    double c1=a1*A1+b1*A2; 
  
    //line CD
    double a2=D2-C2; 
    double b2=C1-D1; 
    double c2=a2*C1+b2*C2; 
  
    double determinant=a1*b2-a2*b1; 
  
    if(determinant==0) 
     { 
       printf("det=0\n");
     } 
    else
     { 
       *out_x=(b2*c1-b1*c2)/determinant; 
       *out_y=(a1*c2-a2*c1)/determinant; 
     } 
  }
















