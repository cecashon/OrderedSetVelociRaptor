

--A variety of cairo drawings. Some of them are the progression of a drawing to a widget. A few others are looking at the possibility of drawing in 3d with cairo. Some... just for fun.

spring2.c

![ScreenShot](/Misc/cairo_drawings/spring2.png)

![ScreenShot](/Misc/cairo_drawings/spring3.png)

freetype_points2.c

![ScreenShot](/Misc/cairo_drawings/freetype.png)

shadow1.c

![ScreenShot](/Misc/cairo_drawings/shadow.png)

