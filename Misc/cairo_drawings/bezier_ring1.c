
/*   
    Test drawing bezier rings with different section arc lengths. Try a flying saucer, get some
cross product normals and explode the flying saucer. Draw a 2d and 3d pie chart. Explode the
3d pie chart.
    For drawing the 3d pie chart use symetry to draw the outside ring quads. A reference vector
for the top and bottom. Painters algorithm for ordering the inside drawing quads. The movement
of the pie pieces outward uses a couple more reference vectors. This looks to work for the 2d and
3d pie chart along with the animated movement of the pieces.
    Move pie chart3d to pie_chart1.c.

    gcc -Wall bezier_ring1.c -o bezier_ring1 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<string.h>
#include<math.h>

struct point3d{
  gdouble x;
  gdouble y;
  gdouble z;
};

struct arc{
  struct point3d a1;
  struct point3d a2;
  struct point3d a3;
  struct point3d a4;
};

//A quad that can be sorted by z-value.
struct quad{
  struct point3d a;
  struct point3d b;
  struct point3d c;
  struct point3d d;
  gdouble color[3];
  gint section_id;
};

struct section{
  gint section_id;
  gint start;
  gint end;
  gdouble color[3];
};

//Test arrays.
static GArray *ring1=NULL;
static GArray *ring1_t=NULL;
static GArray *ring2=NULL;
static GArray *ring2_t=NULL;
static GArray *ring3=NULL;
static GArray *ring3_t=NULL;
static GPtrArray *flying_saucer1=NULL;
static GArray *cross_products=NULL;
//For the pie charts.
static GArray *pie_chart1=NULL;
static GArray *pie_chart1_t=NULL;
static GArray *pie_chart2=NULL;
static GArray *pie_chart2_t=NULL;
//Array of vectors to move the pie sections along.
static GArray *v_pie=NULL;
static GArray *v_pie_t=NULL;
//Array for the location of the pie chart sections.
static GArray *sections=NULL;

//Reference vector for top of the ring or pie chart.
static gdouble top[3]={0.0, 0.0, 1.0};
static gdouble top_t[3]={0.0, 0.0, 1.0};
//Multiplier for pie section vectors.
static gdouble pie_explode=20.0;
//The depth of the pie chart from (0,0,0). The total z distance in 2*depth.
static gdouble depth=20.0;

static gint drawing_id=0;
static gdouble pitch=0.0;
static gdouble roll=0.0;
static gdouble yaw=0.0;
static gint explode=0;
static gint seconds=-1;
static guint tick_id=0;
static gboolean frame_rate=FALSE;
static gboolean time_it=FALSE;

static void set_animate_drawing(GtkComboBox *combo, gpointer data);
static void set_drawing_id(GtkComboBox *combo, gpointer data);
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void show_frame_rate(GtkToggleButton *check_frame, gpointer data);
static void show_drawing_time(GtkToggleButton *check_time, gpointer data);

static void initialize_bezier_ring(GArray **ring, GArray **ring_t, gint sections, gdouble radius, gdouble z);

static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data);
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer data);
static void draw_rings(GtkWidget *da, cairo_t *cr);
static void draw_flying_saucer1(GtkWidget *da, cairo_t *cr);
static void draw_pie_chart1(GtkWidget *da, cairo_t *cr);
static void draw_pie_chart2(GtkWidget *da, cairo_t *cr);
static void draw_pie_chart3(GtkWidget *da, cairo_t *cr);

static void reset_ring(GArray *ring, GArray *ring_t);
static void reset_flying_saucer1();
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9]);
static void rotate_reference_vector(gdouble qrs[9]);
static void rotate_pie_reference_vectors(gdouble qrs[9]);
static void rotate_ring(GArray *ring, gdouble qrs[9]);
static void rotate_flying_saucer1(gdouble qrs[9]);
static void get_cross_products_flying_saucer1();
static void explode_flying_saucer1();
int compare_quads(const void *q1, const void *q2);

int main(int argc, char **argv)
 {
   gtk_init(&argc, &argv);

   GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
   gtk_window_set_title(GTK_WINDOW(window), "Bezier Rings");
   gtk_window_set_default_size(GTK_WINDOW(window), 700, 500);
   gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
   g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

   initialize_bezier_ring(&ring1, &ring1_t, 16, 150.0, 0.0);
   initialize_bezier_ring(&ring2, &ring2_t, 8, 125.0, 50.0);
   initialize_bezier_ring(&ring3, &ring3_t, 4, 100.0, -50.0);

   //Initialize a flying saucer and cross products for normal vectors.
   gint i=0;
   flying_saucer1=g_ptr_array_new();
   GArray *p1=NULL;
   GArray *p1_t=NULL;   
   initialize_bezier_ring(&p1, &p1_t, 8, 50.0, -50.0);
   g_ptr_array_add(flying_saucer1, (gpointer)p1);
   g_ptr_array_add(flying_saucer1, (gpointer)p1_t);   
   initialize_bezier_ring(&p1, &p1_t, 8, 100.0, -25.0);
   g_ptr_array_add(flying_saucer1, (gpointer)p1);
   g_ptr_array_add(flying_saucer1, (gpointer)p1_t);   
   initialize_bezier_ring(&p1, &p1_t, 8, 150.0, 0.0);
   g_ptr_array_add(flying_saucer1, (gpointer)p1);
   g_ptr_array_add(flying_saucer1, (gpointer)p1_t);
   initialize_bezier_ring(&p1, &p1_t, 8, 100.0, 25.0);
   g_ptr_array_add(flying_saucer1, (gpointer)p1);
   g_ptr_array_add(flying_saucer1, (gpointer)p1_t);
   initialize_bezier_ring(&p1, &p1_t, 8, 50.0, 50.0);
   g_ptr_array_add(flying_saucer1, (gpointer)p1);
   g_ptr_array_add(flying_saucer1, (gpointer)p1_t);
   cross_products=g_array_sized_new(FALSE, FALSE, sizeof(struct point3d), 32);
   g_array_set_size(cross_products, 32);  

   //Initialize a pie chart with 60 pieces. This needs to go into a pie chart object.
   initialize_bezier_ring(&pie_chart1, &pie_chart1_t, 60, 150.0, depth);
   initialize_bezier_ring(&pie_chart2, &pie_chart2_t, 60, 150.0, -depth);
   //Place reference vectors in the center of each piece.
   struct point3d v1;
   v_pie=g_array_new(FALSE, FALSE, sizeof(struct point3d));
   v_pie_t=g_array_new(FALSE, FALSE, sizeof(struct point3d));
   v1.x=cos(5.0*G_PI/30.0);v1.y=-sin(5.0*G_PI/30.0);v1.z=0.0;
   g_array_append_val(v_pie, v1);
   g_array_append_val(v_pie_t, v1);
   v1.x=cos(25.0*G_PI/30.0);v1.y=-sin(25.0*G_PI/30.0);v1.z=0.0;
   g_array_append_val(v_pie, v1);
   g_array_append_val(v_pie_t, v1);
   v1.x=cos(50.0*G_PI/30.0);v1.y=-sin(50.0*G_PI/30.0);v1.z=0.0;
   g_array_append_val(v_pie, v1);
   g_array_append_val(v_pie_t, v1);
   //The pie chart sections
   struct section s1;
   sections=g_array_new(FALSE, FALSE, sizeof(struct section));
   s1.section_id=0;s1.start=0;s1.end=9;s1.color[0]=1.0;s1.color[1]=0.0;s1.color[2]=0.0;
   g_array_append_val(sections, s1);
   s1.section_id=1;s1.start=10;s1.end=39;s1.color[0]=0.0;s1.color[1]=1.0;s1.color[2]=0.0;
   g_array_append_val(sections, s1);
   s1.section_id=2;s1.start=40;s1.end=59;s1.color[0]=0.0;s1.color[1]=0.0;s1.color[2]=1.0;
   g_array_append_val(sections, s1);

   GtkWidget *da=gtk_drawing_area_new();
   gtk_widget_set_hexpand(da, TRUE);
   gtk_widget_set_vexpand(da, TRUE);
   g_signal_connect(da, "draw", G_CALLBACK(draw_main), NULL); 

   GtkWidget *combo_animate=gtk_combo_box_text_new();
   gtk_widget_set_hexpand(combo_animate, TRUE);
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_animate), 0, "1", "Draw Shape");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_animate), 1, "2", "Animate Shape");
   gtk_combo_box_set_active(GTK_COMBO_BOX(combo_animate), 0);
   g_signal_connect(combo_animate, "changed", G_CALLBACK(set_animate_drawing), da);

   GtkWidget *combo_drawing=gtk_combo_box_text_new();
   gtk_widget_set_hexpand(combo_drawing, TRUE);
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "1", "Bezier Rings");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "2", "Flying Saucer");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 2, "3", "Pie Chart 2d");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 3, "4", "Pie Chart 3d");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 4, "5", "Pie Chart 3d_2");
   gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
   g_signal_connect(combo_drawing, "changed", G_CALLBACK(set_drawing_id), da);

   GtkWidget *pitch_label=gtk_label_new("Pitch");
   gtk_widget_set_hexpand(pitch_label, TRUE);

   GtkWidget *roll_label=gtk_label_new("Roll");
   gtk_widget_set_hexpand(roll_label, TRUE);

   GtkWidget *yaw_label=gtk_label_new("Yaw");
   gtk_widget_set_hexpand(yaw_label, TRUE);

   GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
   gtk_widget_set_vexpand(pitch_slider, TRUE);
   g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_pitch), da);

   GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
   gtk_widget_set_vexpand(roll_slider, TRUE);
   g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_roll), da);

   GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
   gtk_widget_set_vexpand(yaw_slider, TRUE);
   g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_yaw), da);  

   GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
   gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
   g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), NULL);

   GtkWidget *check_time=gtk_check_button_new_with_label("Time Drawing");
   gtk_widget_set_halign(check_time, GTK_ALIGN_CENTER);
   g_signal_connect(check_time, "toggled", G_CALLBACK(show_drawing_time), NULL);
   
   GtkWidget *grid=gtk_grid_new();
   gtk_container_set_border_width(GTK_CONTAINER(grid), 15);
   gtk_grid_set_row_spacing(GTK_GRID(grid), 10);
   gtk_grid_attach(GTK_GRID(grid), combo_animate, 0, 0, 3, 1);
   gtk_grid_attach(GTK_GRID(grid), combo_drawing, 0, 1, 3, 1);
   gtk_grid_attach(GTK_GRID(grid), pitch_label, 0, 2, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), roll_label, 1, 2, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), yaw_label, 2, 2, 1, 1);    
   gtk_grid_attach(GTK_GRID(grid), pitch_slider, 0, 3, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), roll_slider, 1, 3, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), yaw_slider, 2, 3, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), check_frame, 0, 4, 3, 1);
   gtk_grid_attach(GTK_GRID(grid), check_time, 0, 5, 3, 1);

   GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
   gtk_paned_pack1(GTK_PANED(paned1), grid, FALSE, TRUE);
   gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
   gtk_paned_set_position(GTK_PANED(paned1), 200);
   
   gtk_container_add(GTK_CONTAINER(window), paned1);

   gtk_widget_show_all(window);

   gtk_main();

   //Clean up.
   g_array_free(ring1, TRUE);
   g_array_free(ring1_t, TRUE);
   g_array_free(ring2, TRUE);
   g_array_free(ring2_t, TRUE);
   g_array_free(ring3, TRUE);
   g_array_free(ring3_t, TRUE);

   if(flying_saucer1!=NULL)
      {
        for(i=0;i<(flying_saucer1->len);i++)
          {
            g_array_free((GArray*)g_ptr_array_index(flying_saucer1, i), TRUE);
          }
        g_ptr_array_free(flying_saucer1, TRUE);
      } 
   g_array_free(cross_products, TRUE); 

   g_array_free(pie_chart1, TRUE);
   g_array_free(pie_chart1_t, TRUE);
   g_array_free(pie_chart2, TRUE);
   g_array_free(pie_chart2_t, TRUE);
   g_array_free(v_pie, TRUE);
   g_array_free(v_pie_t, TRUE);
   g_array_free(sections, TRUE);

   return 0;  
 }
static void set_animate_drawing(GtkComboBox *combo, gpointer data)
  {   
    pitch=0.0;
    roll=0.0;
    yaw=0.0;
 
    if(gtk_combo_box_get_active(combo)==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(data), tick_id);
        tick_id=0;
        explode=0;
        seconds=-1;
        pie_explode=20.0;
        gtk_widget_queue_draw(GTK_WIDGET(data));
      }
    else
      {
        if(tick_id==0)
          {
            pie_explode=0.0;
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(data), (GtkTickCallback)animate_drawing, NULL, NULL);
          }
      }
    
  }
static void set_drawing_id(GtkComboBox *combo, gpointer data)
  {  
    drawing_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    pitch=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    roll=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    yaw=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void show_frame_rate(GtkToggleButton *check_frame, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_frame)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_drawing_time(GtkToggleButton *check_time, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_time)) time_it=TRUE;
    else time_it=FALSE;
  }
static void initialize_bezier_ring(GArray **ring, GArray **ring_t, gint sections, gdouble radius, gdouble z)
  {
    /*
         Bezier control points. 
         https://en.wikipedia.org/wiki/Composite_B%C3%A9zier_curve#Approximating_circular_arcs 

         Get the points in one arc of the ring and rotate them for the other points in the ring.
    */  
    gint i=0;
    //Get one section arc.
    gdouble arc_top=-(2.0*G_PI/sections)/2.0;
    gdouble arc_offset=-2.0*G_PI/sections;
    gdouble start_x=radius*cos(arc_top);
    gdouble start_y=radius*sin(arc_top);
    gdouble end_x=start_x;
    gdouble end_y=-start_y;
    gdouble c1_x=(4.0*radius-start_x)/3.0;
    gdouble c1_y=((radius-start_x)*(3.0*radius-start_x))/(3.0*start_y);
    gdouble c2_x=c1_x;
    gdouble c2_y=-c1_y;
    gdouble offset1=0.0;
    gdouble offset2=0.0;

    *ring=g_array_sized_new(FALSE, FALSE, sizeof(struct arc), sections);
    g_array_set_size(*ring, sections);
    *ring_t=g_array_sized_new(FALSE, FALSE, sizeof(struct arc), sections);
    g_array_set_size(*ring_t, sections);

    struct arc *p1=&g_array_index(*ring, struct arc, 0);
    struct arc *p2=&g_array_index(*ring, struct arc, 1);

    //Rotate the points in the section to align with the zero point on the unit circle.
    offset1=cos(arc_top);
    offset2=sin(arc_top);
    (p1->a4).x=start_x*offset1-start_y*offset2;
    (p1->a4).y=start_x*offset2+start_y*offset1;
    (p1->a4).z=z;
    (p1->a3).x=c1_x*offset1-c1_y*offset2;
    (p1->a3).y=c1_x*offset2+c1_y*offset1;
    (p1->a3).z=z;
    (p1->a2).x=c2_x*offset1-c2_y*offset2;
    (p1->a2).y=c2_x*offset2+c2_y*offset1;
    (p1->a2).z=z;
    (p1->a1).x=end_x*offset1-end_y*offset2;
    (p1->a1).y=end_x*offset2+end_y*offset1;
    (p1->a1).z=z;

    offset1=cos(arc_offset);
    offset2=sin(arc_offset);
    for(i=1;i<sections;i++)
      {
        (p2->a1).x=(p1->a1).x*offset1-(p1->a1).y*offset2;
        (p2->a1).y=(p1->a1).x*offset2+(p1->a1).y*offset1;
        (p2->a1).z=z;
        (p2->a2).x=(p1->a2).x*offset1-(p1->a2).y*offset2;
        (p2->a2).y=(p1->a2).x*offset2+(p1->a2).y*offset1;
        (p2->a2).z=z;
        (p2->a3).x=(p1->a3).x*offset1-(p1->a3).y*offset2;
        (p2->a3).y=(p1->a3).x*offset2+(p1->a3).y*offset1;
        (p2->a3).z=z;
        (p2->a4).x=(p1->a4).x*offset1-(p1->a4).y*offset2;
        (p2->a4).y=(p1->a4).x*offset2+(p1->a4).y*offset1;
        (p2->a4).z=z;
        p1++;p2++;
      }
  }
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data)
  {
    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    if(pitch>360.0) pitch=0.0;
    else pitch+=0.5;
    if(roll>360.0) roll=0.0;
    else roll+=0.5;
    if(yaw>360.0) yaw=0.0;
    else yaw+=0.5;
    if(explode>1000) explode=0;
    else explode++;
    if(seconds>59) seconds=0;
    else seconds++;
    if(pie_explode>100.0) pie_explode=0.0;
    else pie_explode+=0.5;

    gtk_widget_queue_draw(GTK_WIDGET(drawing));
    return G_SOURCE_CONTINUE;
  }
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    if(drawing_id==0) draw_rings(da, cr);
    else if(drawing_id==1) draw_flying_saucer1(da, cr);
    else if(drawing_id==2) draw_pie_chart1(da, cr);
    else if(drawing_id==3) draw_pie_chart2(da, cr);
    else draw_pie_chart3(da, cr);
    return FALSE;
  }
static void draw_rings(GtkWidget *da, cairo_t *cr)
  {
    gint i=0;
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;
    GTimer *timer=NULL;
 
    if(time_it==TRUE) timer=g_timer_new();

    //Background.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr); 

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 2.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Draw in the center
    cairo_translate(cr, width/2.0, height/2.0);
    cairo_set_line_cap (cr, CAIRO_LINE_CAP_ROUND);
    cairo_set_line_width(cr, 6.0); 
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    gdouble qrs[9]; 
    quaternion_rotation(yaw1, roll1, pitch1, qrs);
    reset_ring(ring1, ring1_t);
    reset_ring(ring2, ring2_t); 
    reset_ring(ring3, ring3_t);  
    rotate_ring(ring1_t, qrs); 
    rotate_ring(ring2_t, qrs); 
    rotate_ring(ring3_t, qrs);    

    //Draw the ring.
    struct arc *p1=&g_array_index(ring1_t, struct arc, 0);
    cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
    cairo_move_to(cr, (p1->a1).x, (p1->a1).y); 
    for(i=0;i<ring1->len;i++)
      {
        cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
        p1++;
      }
    cairo_stroke(cr);

    p1=&g_array_index(ring2_t, struct arc, 0);
    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
    cairo_move_to(cr, (p1->a1).x, (p1->a1).y); 
    for(i=0;i<ring2->len;i++)
      {
        cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
        p1++;
      }
    cairo_stroke(cr);

    //cairo_scale(cr, 1.5, 1.0);
    p1=&g_array_index(ring3_t, struct arc, 0);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_move_to(cr, (p1->a1).x, (p1->a1).y); 
    for(i=0;i<ring3->len;i++)
      {
        cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
        p1++;
      }
    cairo_stroke(cr);


    if(time_it==TRUE) 
      {
        g_print("Draw Time %f\n", g_timer_elapsed(timer, NULL));
        g_timer_destroy(timer);
      }

  }
static void draw_flying_saucer1(GtkWidget *da, cairo_t *cr)
  {
    gint i=0;
    gint j=0;
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;
    GTimer *timer=NULL;
 
    if(time_it==TRUE) timer=g_timer_new();

    //Background.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr); 

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 2.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Draw in the center
    cairo_translate(cr, width/2.0, height/2.0);
    //cairo_scale(cr, 0.25, 0.25);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    gdouble qrs[9]; 
    quaternion_rotation(yaw1, roll1, pitch1, qrs);
    reset_flying_saucer1(); 
    rotate_flying_saucer1(qrs);
    get_cross_products_flying_saucer1(); 
    if(explode!=0) explode_flying_saucer1();

    GArray *p1=NULL;
    GArray *p2=NULL;
    struct arc *c1=NULL;
    struct arc *c2=NULL;
    struct point3d *cp=&g_array_index(cross_products, struct point3d, 0);

    //Base of the saucer.
    p1=(GArray*)g_ptr_array_index(flying_saucer1, 1);
    c1=&g_array_index(p1, struct arc, 0);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 0.5);
    cairo_move_to(cr, (c1->a1).x, (c1->a1).y); 
    for(i=0;i<(p1->len);i++)
      {
        cairo_curve_to(cr, (c1->a2).x, (c1->a2).y, (c1->a3).x, (c1->a3).y, (c1->a4).x, (c1->a4).y);        
        c1++;
      }
    cairo_fill(cr);

    //Middle of the saucer.
    cairo_set_line_width(cr, 3.0); 
    for(i=1;i<(flying_saucer1->len)-2;i+=2)
      {
        p1=(GArray*)g_ptr_array_index(flying_saucer1, i);
        p2=(GArray*)g_ptr_array_index(flying_saucer1, i+2);
        c1=&g_array_index(p1, struct arc, 0);
        c2=&g_array_index(p2, struct arc, 0);
        for(j=0;j<(p1->len);j++)
          {
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_move_to(cr, (c1->a1).x, (c1->a1).y); 
            cairo_curve_to(cr, (c1->a2).x, (c1->a2).y, (c1->a3).x, (c1->a3).y, (c1->a4).x, (c1->a4).y);
            cairo_line_to(cr, (c2->a4).x, (c2->a4).y); 
            cairo_curve_to(cr, (c2->a3).x, (c2->a3).y, (c2->a2).x, (c2->a2).y, (c2->a1).x, (c2->a1).y);
            cairo_stroke_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 0.5);
            cairo_fill(cr);
            //Some normals to the quads from the cross products.
            cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
            cairo_move_to(cr, (c1->a1).x, (c1->a1).y);
            cairo_line_to(cr, (c1->a1).x+10.0*(cp->x), (c1->a1).y+10.0*(cp->y));
            cairo_stroke(cr);
            c1++;c2++;cp++;
          }
      }

    //Top of the saucer.
    p1=(GArray*)g_ptr_array_index(flying_saucer1, 9);
    c1=&g_array_index(p1, struct arc, 0);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 0.5);
    cairo_move_to(cr, (c1->a1).x, (c1->a1).y); 
    for(i=0;i<(p1->len);i++)
      {
        cairo_curve_to(cr, (c1->a2).x, (c1->a2).y, (c1->a3).x, (c1->a3).y, (c1->a4).x, (c1->a4).y);        
        c1++;
      }
    cairo_fill(cr);

    if(time_it==TRUE) 
     {
       g_print("Draw Time %f\n", g_timer_elapsed(timer, NULL));
       g_timer_destroy(timer);
     }
  }
static void draw_pie_chart1(GtkWidget *da, cairo_t *cr)
  {
    gint i=0;
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;
    GTimer *timer=NULL;
 
    if(time_it==TRUE) timer=g_timer_new();

    //Background.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr); 

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 2.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Draw in the center
    cairo_translate(cr, width/2.0, height/2.0);
    cairo_set_line_cap (cr, CAIRO_LINE_CAP_ROUND);
    cairo_set_line_width(cr, 6.0); 
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    gdouble qrs[9]; 
    quaternion_rotation(yaw1, roll1, pitch1, qrs);
    reset_ring(pie_chart1, pie_chart1_t);    
    rotate_ring(pie_chart1_t, qrs); 
    rotate_reference_vector(qrs); 
      
    //Draw the pie chart. 60 arcs or pieces of pie.
    struct arc *p1=&g_array_index(pie_chart1_t, struct arc, 0);
    if(seconds==-1)
      {
        cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
        cairo_move_to(cr, 20.0*top_t[0], 20.0*top_t[1]);
        cairo_line_to(cr, (p1->a1).x, (p1->a1).y); 
        for(i=0;i<10;i++)
          {
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            p1++;
          }
        cairo_close_path(cr);
        cairo_fill(cr);

        cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
        cairo_move_to(cr, 20.0*top_t[0], 20.0*top_t[1]);
        cairo_line_to(cr, (p1->a1).x, (p1->a1).y);  
        for(i=0;i<30;i++)
          {
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            p1++;
          }
        cairo_close_path(cr);
        cairo_fill(cr);

        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        cairo_move_to(cr, 20.0*top_t[0], 20.0*top_t[1]);
        cairo_line_to(cr, (p1->a1).x, (p1->a1).y); 
        for(i=0;i<20;i++)
          {
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            p1++;
          }
        cairo_close_path(cr);
        cairo_fill(cr);
      }
    //Draw the seconds
    else
      {
        gint diff=(pie_chart1->len)-seconds;
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        cairo_move_to(cr, 20.0*top_t[0], 20.0*top_t[1]);
        cairo_line_to(cr, (p1->a1).x, (p1->a1).y); 
        for(i=0;i<seconds;i++)
          {
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            p1++;
          }
        cairo_close_path(cr);
        cairo_fill(cr);

        cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
        cairo_move_to(cr, 20.0*top_t[0], 20.0*top_t[1]);
        cairo_line_to(cr, (p1->a1).x, (p1->a1).y); 
        for(i=0;i<diff;i++)
          {
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            p1++;
          }
        cairo_close_path(cr);
        cairo_fill(cr);
      }

    if(time_it==TRUE) 
      {
        g_print("Draw Time %f\n", g_timer_elapsed(timer, NULL));
        g_timer_destroy(timer);
      }

  }
static void draw_pie_chart2(GtkWidget *da, cairo_t *cr)
  {
    gint i=0;
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;
    GTimer *timer=NULL;
 
    if(time_it==TRUE) timer=g_timer_new();

    //Background.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr); 

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 2.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Draw in the center
    cairo_translate(cr, width/2.0, height/2.0);
    cairo_set_line_cap (cr, CAIRO_LINE_CAP_ROUND);
    cairo_set_line_width(cr, 2.0); 
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    gdouble qrs[9]; 
    quaternion_rotation(yaw1, roll1, pitch1, qrs);
    reset_ring(pie_chart1, pie_chart1_t); 
    reset_ring(pie_chart2, pie_chart2_t);     
    rotate_ring(pie_chart1_t, qrs);
    rotate_ring(pie_chart2_t, qrs); 
    rotate_reference_vector(qrs); 
      
    //Draw the pie chart. 60 arcs or pieces of pie.
    struct arc *p1=NULL;
    struct arc *p2=NULL;

    if(top_t[2]>0.0)
      {
        p1=&g_array_index(pie_chart1_t, struct arc, 0);
        p2=&g_array_index(pie_chart2_t, struct arc, 0);
      }
    else
      {
        p1=&g_array_index(pie_chart2_t, struct arc, 0);
        p2=&g_array_index(pie_chart1_t, struct arc, 0);
      }

    //Draw back ring.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    if(top_t[2]>0.0) p2=&g_array_index(pie_chart2_t, struct arc, 0);
    else p2=&g_array_index(pie_chart1_t, struct arc, 0);
    cairo_move_to(cr, (p2->a1).x, (p2->a1).y); 
    for(i=0;i<60;i++)
      {
        cairo_curve_to(cr, (p2->a2).x, (p2->a2).y, (p2->a3).x, (p2->a3).y, (p2->a4).x, (p2->a4).y);
        p2++;
      }
    cairo_close_path(cr);
    cairo_stroke(cr);

/*
    //Draw the back wedges. This would be needed if using transparent colors.
    cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
    cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
    if(top_t[2]>0.0) p2=&g_array_index(pie_chart2_t, struct arc, 0);
    else p2=&g_array_index(pie_chart1_t, struct arc, 0);
    if(top_t[2]>0.0) cairo_move_to(cr, -20.0*top_t[0], -20.0*top_t[1]);
    else cairo_move_to(cr, 20.0*top_t[0], 20.0*top_t[1]);
    cairo_line_to(cr, (p2->a1).x, (p2->a1).y); 
    for(i=0;i<10;i++)
      {
        cairo_curve_to(cr, (p2->a2).x, (p2->a2).y, (p2->a3).x, (p2->a3).y, (p2->a4).x, (p2->a4).y);
        p2++;
      }
    cairo_close_path(cr);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    if(top_t[2]>0.0) cairo_move_to(cr, -20.0*top_t[0], -20.0*top_t[1]);
    else cairo_move_to(cr, 20.0*top_t[0], 20.0*top_t[1]);
    cairo_line_to(cr, (p2->a1).x, (p2->a1).y);  
    for(i=0;i<30;i++)
      {
        cairo_curve_to(cr, (p2->a2).x, (p2->a2).y, (p2->a3).x, (p2->a3).y, (p2->a4).x, (p2->a4).y);
        p2++;
      }
    cairo_close_path(cr);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    if(top_t[2]>0.0) cairo_move_to(cr, -20.0*top_t[0], -20.0*top_t[1]);
    else cairo_move_to(cr, 20.0*top_t[0], 20.0*top_t[1]);
    cairo_line_to(cr, (p2->a1).x, (p2->a1).y); 
    for(i=0;i<20;i++)
      {
        cairo_curve_to(cr, (p2->a2).x, (p2->a2).y, (p2->a3).x, (p2->a3).y, (p2->a4).x, (p2->a4).y);
        p2++;
      }
    cairo_close_path(cr);
    cairo_fill(cr);
*/    
    //Draw the sides of the pie chart.
    if(top_t[2]>0.0) p2=&g_array_index(pie_chart2_t, struct arc, 0);
    else p2=&g_array_index(pie_chart1_t, struct arc, 0);
    cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0); 
    cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
    for(i=0;i<10;i++)
      {
        if((p1->a1).z>0.0)
          {
            cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
            cairo_curve_to(cr, (p2->a3).x, (p2->a3).y, (p2->a2).x, (p2->a2).y, (p2->a1).x, (p2->a1).y);
            cairo_close_path(cr);
            cairo_fill(cr);
          }
        p1++;p2++;
      }
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    for(i=0;i<30;i++)
      {
        if((p1->a1).z>0.0)
          {
            cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
            cairo_curve_to(cr, (p2->a3).x, (p2->a3).y, (p2->a2).x, (p2->a2).y, (p2->a1).x, (p2->a1).y);
            cairo_close_path(cr);
            cairo_fill(cr);
          }
        p1++;p2++;
      }
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    for(i=0;i<20;i++)
      {
        if((p1->a1).z>0.0)
          {
            cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
            cairo_curve_to(cr, (p2->a3).x, (p2->a3).y, (p2->a2).x, (p2->a2).y, (p2->a1).x, (p2->a1).y);
            cairo_close_path(cr);
            cairo_fill(cr);
          }
        p1++;p2++;
      }
    
    //Draw the top of pie chart.
    if(top_t[2]>0.0) p1=&g_array_index(pie_chart1_t, struct arc, 0);
    else p1=&g_array_index(pie_chart2_t, struct arc, 0);  
    cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
    if(top_t[2]>0.0) cairo_move_to(cr, 20.0*top_t[0], 20.0*top_t[1]);
    else cairo_move_to(cr, -20.0*top_t[0], -20.0*top_t[1]);
    cairo_line_to(cr, (p1->a1).x, (p1->a1).y); 
    for(i=0;i<10;i++)
      {
        cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
        p1++;
      }
    cairo_close_path(cr);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    if(top_t[2]>0.0) cairo_move_to(cr, 20.0*top_t[0], 20.0*top_t[1]);
    else cairo_move_to(cr, -20.0*top_t[0], -20.0*top_t[1]);
    cairo_line_to(cr, (p1->a1).x, (p1->a1).y);  
    for(i=0;i<30;i++)
      {
        cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
        p1++;
      }
    cairo_close_path(cr);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    if(top_t[2]>0.0) cairo_move_to(cr, 20.0*top_t[0], 20.0*top_t[1]);
    else cairo_move_to(cr, -20.0*top_t[0], -20.0*top_t[1]);
    cairo_line_to(cr, (p1->a1).x, (p1->a1).y); 
    for(i=0;i<20;i++)
      {
        cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
        p1++;
      }
    cairo_close_path(cr);
    cairo_fill(cr);

    //Draw front ring.
    cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    if(top_t[2]>0.0) p1=&g_array_index(pie_chart1_t, struct arc, 0);
    else p1=&g_array_index(pie_chart2_t, struct arc, 0); 
    cairo_move_to(cr, (p1->a1).x, (p1->a1).y); 
    for(i=0;i<60;i++)
      {
        cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
        p1++;
      }
    cairo_close_path(cr);
    cairo_stroke(cr);

    //Draw the text on the top.
    if(top_t[2]>0.0)
      {
        cairo_matrix_t matrix1;
        cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
        cairo_set_font_size(cr, 20.0);
        cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
        cairo_matrix_init(&matrix1, qrs[0], qrs[3], qrs[1], qrs[4], 20.0*top_t[0], 20.0*top_t[1]);
        cairo_transform(cr, &matrix1);         
        gchar *string1=g_strdup("10 minutes");
        cairo_move_to(cr, 20.0, -10.0);
        cairo_show_text(cr, string1);
        g_free(string1);
        gchar *string2=g_strdup("30 minutes");
        cairo_move_to(cr, -120.0, -40.0);
        cairo_show_text(cr, string2);
        g_free(string2);
        gchar *string3=g_strdup("20 minutes");
        cairo_move_to(cr, -10.0, 80.0);
        cairo_show_text(cr, string3);
        g_free(string3);
      }

    if(time_it==TRUE) 
      {
        g_print("Draw Time %f\n", g_timer_elapsed(timer, NULL));
        g_timer_destroy(timer);
      }

  }
static void draw_pie_chart3(GtkWidget *da, cairo_t *cr)
  {
    //An eploding pie chart.
    gint i=0;
    gint j=0;
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;
    GTimer *timer=NULL;
 
    if(time_it==TRUE) timer=g_timer_new();

    //Background.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr); 

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 2.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Draw in the center
    cairo_translate(cr, width/2.0, height/2.0);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
    cairo_set_line_width(cr, 2.0); 
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    gdouble qrs[9]; 
    quaternion_rotation(yaw1, roll1, pitch1, qrs);
    reset_ring(pie_chart1, pie_chart1_t); 
    reset_ring(pie_chart2, pie_chart2_t);     
    rotate_ring(pie_chart1_t, qrs);
    rotate_ring(pie_chart2_t, qrs); 
    rotate_reference_vector(qrs); 
    rotate_pie_reference_vectors(qrs); 
      
    //Draw the pie chart. 60 arcs or pieces of pie.
    cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
    struct arc *p1=NULL;
    struct arc *p2=NULL;

    //An array to sort inside quads.
    struct quad q;
    GArray *quads=g_array_new(FALSE, FALSE, sizeof(struct quad));
     
    //Get the inside quad points.
    if(top_t[2]>0.0)
      {
        p1=&g_array_index(pie_chart1_t, struct arc, 0);
        p2=&g_array_index(pie_chart2_t, struct arc, 0);
      }
    else
      {
        p1=&g_array_index(pie_chart2_t, struct arc, 0);
        p2=&g_array_index(pie_chart1_t, struct arc, 0);
      } 

    struct section *s1=&g_array_index(sections, struct section, 0);
    gint offset=0;
    for(i=0;i<(sections->len);i++)
      {
        q.color[0]=(s1->color)[0];q.color[1]=(s1->color)[1];q.color[2]=(s1->color)[2]; 
        if(top_t[2]>0.0)
          {
            q.a.x=depth*top_t[0];q.a.y=depth*top_t[1];q.a.z=depth*top_t[2];
          }
        else
          {
            q.a.x=-depth*top_t[0];q.a.y=-depth*top_t[1];q.a.z=-depth*top_t[2];
          }
        q.b.x=(p1->a1).x;q.b.y=(p1->a1).y;q.b.z=(p1->a1).z;
        q.c.x=(p2->a1).x;q.c.y=(p2->a1).y;q.c.z=(p2->a1).z;
        if(top_t[2]>0.0)
          {
            q.d.x=-depth*top_t[0];q.d.y=-depth*top_t[1];q.d.z=-depth*top_t[2];
          }
        else
          {
            q.d.x=depth*top_t[0];q.d.y=depth*top_t[1];q.d.z=depth*top_t[2];
          }
        q.section_id=(s1->section_id);
        g_array_append_val(quads, q);

        //Second side in pie section.
        offset=(s1->end)-(s1->start);
        p1+=offset;p2+=offset; 
        q.color[0]=(s1->color)[0];q.color[1]=(s1->color)[1];q.color[2]=(s1->color)[2];  
        if(top_t[2]>0.0)
          {
            q.a.x=depth*top_t[0];q.a.y=depth*top_t[1];q.a.z=depth*top_t[2];
          }
        else
          {
            q.a.x=-depth*top_t[0];q.a.y=-depth*top_t[1];q.a.z=-depth*top_t[2];
          }
        q.b.x=(p1->a4).x;q.b.y=(p1->a4).y;q.b.z=(p1->a4).z;
        q.c.x=(p2->a4).x;q.c.y=(p2->a4).y;q.c.z=(p2->a4).z;
        if(top_t[2]>0.0)
          {
            q.d.x=-depth*top_t[0];q.d.y=-depth*top_t[1];q.d.z=-depth*top_t[2];
          }
        else
          {
            q.d.x=depth*top_t[0];q.d.y=depth*top_t[1];q.d.z=depth*top_t[2];
          }
        q.section_id=(s1->section_id);
        g_array_append_val(quads, q);
        p1++;p2++;s1++;
      }

    //Sort and draw the inside quads.
    g_array_sort(quads, compare_quads);
    struct quad *q1=&g_array_index(quads, struct quad, 0);
    struct point3d *pt=&g_array_index(v_pie_t, struct point3d, 0);
    for(i=0;i<(quads->len);i++)
      {
        cairo_save(cr);
        //Some kinda crummy pointer arithmetic to get the translation.
        cairo_translate(cr, pie_explode*((pt+(q1->section_id))->x), pie_explode*((pt+(q1->section_id))->y));        
        cairo_set_source_rgb(cr, (q1->color)[0], (q1->color)[1], (q1->color)[2]); 
        cairo_move_to(cr, (q1->a).x, (q1->a).y);
        cairo_line_to(cr, (q1->b).x, (q1->b).y);
        cairo_line_to(cr, (q1->c).x, (q1->c).y);
        cairo_line_to(cr, (q1->d).x, (q1->d).y);
        cairo_close_path(cr);
        cairo_fill(cr);
        cairo_restore(cr);
        q1++;
      }
    g_array_free(quads, TRUE);

    //Draw circular outside sides of the pieces of the pie chart.
    if(top_t[2]>0.0)
      {
        p1=&g_array_index(pie_chart1_t, struct arc, 0);
        p2=&g_array_index(pie_chart2_t, struct arc, 0);
      }
    else
      {
        p1=&g_array_index(pie_chart2_t, struct arc, 0);
        p2=&g_array_index(pie_chart1_t, struct arc, 0);
      } 
    s1=&g_array_index(sections, struct section, 0);
    pt=&g_array_index(v_pie_t, struct point3d, 0);

    for(i=0;i<(sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;
        cairo_save(cr);
        cairo_translate(cr, pie_explode*(pt->x), pie_explode*(pt->y));
        for(j=0;j<offset;j++)
          {
            if((p1->a1).z>0.0)
              {
                cairo_set_source_rgb(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2]); 
                cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
                cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
                cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
                cairo_curve_to(cr, (p2->a3).x, (p2->a3).y, (p2->a2).x, (p2->a2).y, (p2->a1).x, (p2->a1).y);
                cairo_close_path(cr);
                cairo_fill(cr);
              }
            p1++;p2++;
          }
        cairo_restore(cr);
        s1++;pt++;
      }

    //Draw the top of pie chart.
    cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT);
    if(top_t[2]>0.0) p1=&g_array_index(pie_chart1_t, struct arc, 0);
    else p1=&g_array_index(pie_chart2_t, struct arc, 0); 
    s1=&g_array_index(sections, struct section, 0);
    pt=&g_array_index(v_pie_t, struct point3d, 0);

    for(i=0;i<(sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;    
        cairo_save(cr);
        cairo_translate(cr, pie_explode*(pt->x), pie_explode*(pt->y)); 
        cairo_set_source_rgb(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2]);
        if(top_t[2]>0.0) cairo_move_to(cr, depth*top_t[0], depth*top_t[1]);
        else cairo_move_to(cr, -depth*top_t[0], -depth*top_t[1]);
        cairo_line_to(cr, (p1->a1).x, (p1->a1).y); 
        for(j=0;j<offset;j++)
          {
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            p1++;
          }
        cairo_close_path(cr);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke(cr);
        cairo_restore(cr);
        s1++;pt++;
      }

    if(time_it==TRUE) 
      {
        g_print("Draw Time %f\n", g_timer_elapsed(timer, NULL));
        g_timer_destroy(timer);
      }

  }

static void reset_ring(GArray *ring, GArray *ring_t)
  {
    struct arc *p1=&g_array_index(ring, struct arc, 0);
    struct arc *p2=&g_array_index(ring_t, struct arc, 0);
    memcpy(p2, p1, sizeof(struct arc)*(ring->len));
  }
static void reset_flying_saucer1()
  {
    gint i=0;
    GArray *p1=NULL;
    GArray *p2=NULL;
    struct arc *a1=NULL;
    struct arc *a2=NULL;
    for(i=0;i<(flying_saucer1->len);i+=2)
      {
        p1=(GArray*)g_ptr_array_index(flying_saucer1, i);
        p2=(GArray*)g_ptr_array_index(flying_saucer1, i+1);
        a1=&g_array_index(p1, struct arc, 0);
        a2=&g_array_index(p2, struct arc, 0);
        memcpy(a2, a1, sizeof(struct arc)*(p1->len));
      }
  }
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
static void rotate_reference_vector(gdouble qrs[9])
  {
    //Rotate the top[] reference vector.
    gdouble cr1=top[0];
    gdouble cr2=top[1];
    gdouble cr3=top[2];         
    top_t[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
    top_t[1]=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
    top_t[2]=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);  
  }
static void rotate_pie_reference_vectors(gdouble qrs[9])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0; 
    struct point3d *p1=&g_array_index(v_pie, struct point3d, 0);
    struct point3d *p2=&g_array_index(v_pie_t, struct point3d, 0);

    for(i=0;i<(v_pie->len);i++)
      {
        cr1=(p1->x);
        cr2=(p1->y);
        cr3=(p1->z);          
        (p2->x)=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p2->y)=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p2->z)=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]); 
        p1++;p2++;
      } 
  }
static void rotate_ring(GArray *ring, gdouble qrs[9])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0;   
    struct arc *p1=&g_array_index(ring, struct arc, 0);

    for(i=0;i<(ring->len);i++)
      {
        cr1=(p1->a1).x;
        cr2=(p1->a1).y;
        cr3=(p1->a1).z;         
        (p1->a1).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p1->a1).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p1->a1).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p1->a2).x;
        cr2=(p1->a2).y;
        cr3=(p1->a2).z;         
        (p1->a2).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p1->a2).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p1->a2).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p1->a3).x;
        cr2=(p1->a3).y;
        cr3=(p1->a3).z;         
        (p1->a3).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p1->a3).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p1->a3).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p1->a4).x;
        cr2=(p1->a4).y;
        cr3=(p1->a4).z;         
        (p1->a4).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p1->a4).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p1->a4).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        p1++;
      }  
  }
static void rotate_flying_saucer1(gdouble qrs[9])
  {
    gint i=0;
    gint j=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0;   

    GArray *p1=NULL;
    struct arc *p2=NULL;
    for(i=1;i<(flying_saucer1->len);i+=2)
      {
        p1=(GArray*)g_ptr_array_index(flying_saucer1, i);
        p2=&g_array_index(p1, struct arc, 0);
        for(j=0;j<(p1->len);j++)
          {
            cr1=(p2->a1).x;
            cr2=(p2->a1).y;
            cr3=(p2->a1).z;         
            (p2->a1).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
            (p2->a1).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
            (p2->a1).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
            cr1=(p2->a2).x;
            cr2=(p2->a2).y;
            cr3=(p2->a2).z;         
            (p2->a2).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
            (p2->a2).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
            (p2->a2).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
            cr1=(p2->a3).x;
            cr2=(p2->a3).y;
            cr3=(p2->a3).z;         
            (p2->a3).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
            (p2->a3).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
            (p2->a3).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
            cr1=(p2->a4).x;
            cr2=(p2->a4).y;
            cr3=(p2->a4).z;         
            (p2->a4).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
            (p2->a4).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
            (p2->a4).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
            p2++;
          } 
      }   
  }
static void get_cross_products_flying_saucer1()
  {
    gint i=0;
    gint j=0;
    gdouble length=0.0;
    gdouble a[3];
    gdouble b[3];
    struct point3d *cp=&g_array_index(cross_products, struct point3d, 0);
    GArray *p1=NULL;
    GArray *p2=NULL;
    struct arc *c1=NULL;
    struct arc *c2=NULL;

    for(i=1;i<(flying_saucer1->len)-2;i+=2)
      {
        p1=(GArray*)g_ptr_array_index(flying_saucer1, i);
        p2=(GArray*)g_ptr_array_index(flying_saucer1, i+2);
        c1=&g_array_index(p1, struct arc, 0);
        c2=&g_array_index(p2, struct arc, 0);
        for(j=0;j<(p1->len);j++)
          {
            a[0]=(c2->a1).x-(c1->a1).x;
            a[1]=(c2->a1).y-(c1->a1).y;
            a[2]=(c2->a1).z-(c1->a1).z;
            b[0]=(c1->a4).x-(c1->a1).x;
            b[1]=(c1->a4).y-(c1->a1).y;
            b[2]=(c1->a4).z-(c1->a1).z;
            (cp->x)=a[1]*b[2]-a[2]*b[1];
            (cp->y)=a[2]*b[0]-a[0]*b[2];
            (cp->z)=a[0]*b[1]-a[1]*b[0]; 
            length=sqrt((cp->x)*(cp->x)+(cp->y)*(cp->y)+(cp->z)*(cp->z));    
            (cp->x)/=length;
            (cp->y)/=length;
            (cp->z)/=length;
            cp++;c1++;c2++;
          }
      }
  }
static void explode_flying_saucer1()
  {
    gint i=0;
    gint j=0;
    gdouble e1=0.0;
    gdouble e2=0.0;
    struct point3d *cp=&g_array_index(cross_products, struct point3d, 0);
    GArray *p1=NULL;
    GArray *p2=NULL;
    struct arc *c1=NULL;
    struct arc *c2=NULL;

    for(i=1;i<(flying_saucer1->len)-2;i+=2)
      {
        p1=(GArray*)g_ptr_array_index(flying_saucer1, i);
        p2=(GArray*)g_ptr_array_index(flying_saucer1, i+2);
        c1=&g_array_index(p1, struct arc, 0);
        c2=&g_array_index(p2, struct arc, 0);
        for(j=0;j<(p1->len);j++)
          {
            e1=((gdouble)explode/10.0*(cp->x));
            e2=((gdouble)explode/10.0*(cp->y));
            (c1->a1).x+=e1;
            (c1->a1).y+=e2;
            (c1->a2).x+=e1;
            (c1->a2).y+=e2;
            (c1->a3).x+=e1;
            (c1->a3).y+=e2;
            (c1->a4).x+=e1;
            (c1->a4).y+=e2;          
            (c2->a1).x+=e1;
            (c2->a1).y+=e2;
            (c2->a2).x+=e1;
            (c2->a2).y+=e2;
            (c2->a3).x+=e1;
            (c2->a3).y+=e2;
            (c2->a4).x+=e1;
            (c2->a4).y+=e2; 
            cp++;c1++;c2++;
          }
      }
  }
int compare_quads(const void *q1, const void *q2)
  {
    return((((struct quad*)q1)->a.z+((struct quad*)q1)->b.z+((struct quad*)q1)->c.z+((struct quad*)q1)->d.z) - (((struct quad*)q2)->a.z+((struct quad*)q2)->b.z+((struct quad*)q2)->c.z+((struct quad*)q2)->d.z));
  }

