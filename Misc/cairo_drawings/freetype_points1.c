/*
    Try drawing 3d fonts with font points in cairo. Needs some work. Doesn't have

    "If the last point is a conic ‘off’ point itself, start the contour with the virtual ‘on’ point between the last and first point of the contour." 

    https://www.freetype.org/freetype2/docs/glyphs/glyphs-6.html

    For the translating the quadratic bezier points to cubic bezier points for cairo to use.
    https://lists.freedesktop.org/archives/cairo/2009-October/018351.html
    To turn quad(p0, p1, p2) into cubic(q0, q1, q2, q3):
	q0 = p0
	q1 = (p0+2*p1)/3
	q2 = (p2+2*p1)/3
	q3 = p2

    gcc -Wall freetype_points1.c -o freetype_points1 -I/usr/include/freetype2 -L/usr/local/lib `pkg-config --cflags --libs gtk+-3.0` -lfreetype -lm

    Tested on Ubuntu16.04 and GTK3.18 32bit

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<stdint.h>
#include<stdlib.h>
#include<ft2build.h>
#include FT_FREETYPE_H
#include FT_OUTLINE_H
#include FT_BBOX_H

/*
  A drawing path operation. The R and L on move_to is clockwise and counter clockwise 
  winding of the path
*/
enum{ 
  MOVE_TO_R,
  MOVE_TO_L,
  LINE_TO,
  CURVE_TO,
  LINE_TO_STROKE,
  CURVE_TO_STROKE
};

//The path operation and 3d point on the character.
struct segment{
  gint move_id;
  gdouble x;
  gdouble y;
  gdouble z;
}; 

//Some letters to test.
static char *text="AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
static gint text_index=0;
//Scale the font from original coordinates. scale>0;
static gdouble scale=0.25;
//The test drawing combo.
static gint drawing_id=0;
//The stress combo.
static gint stress_id=0;
//The slider values
static gdouble slider_pitch=0.0;
static gdouble slider_roll=0.0;
static gdouble slider_yaw=0.0;
static gdouble slider_extrude=20.0;
static gdouble slider_perspective=0.0;
//A reference vector to keep track of the top of the drawing.
static gdouble v1[3]={0.0, 0.0, 1.0};
static gdouble v1_r[3]={0.0, 0.0, 1.0};
//A saved array to speed up animation.
static GArray *path_s=NULL;

//Set values in combos and sliders.
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_extrude(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_letter(GtkComboBox *combo, gpointer data);
static void set_drawing(GtkComboBox *combo, gpointer data);
static void set_stress(GtkComboBox *combo, gpointer data);
//Start drawing.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, FT_Face face);
//Draw the char with the bezier points and virtual points.
static void draw_font(GtkWidget *da, cairo_t *cr, FT_Face face);
//Draw the char 3d.
static void draw_font2(GtkWidget *da, cairo_t *cr, FT_Face face);
static void draw_path_contours_line(cairo_t *cr, GArray *path);
static void draw_path_contours_connect(cairo_t *cr, GArray *path1, GArray *path2);
static void draw_path_side_fill(cairo_t *cr, GArray *path1, GArray *path2);
static void draw_path_contours_fill(cairo_t *cr, GArray *path);
//Load an array of points that can be transformed easily.
static GArray* get_font_path(FT_Face face, gint index);
static FT_Outline get_font_outline(FT_Face face, gint index);
//Try a couple of 3d transforms.
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9]);
static void transform_letter(GArray *path, gdouble qrs[9]);
static void matrix_multiply(gdouble qrs1[9], gdouble qrs2[9]);

int main(int argc, char **argv)
 {
   FT_Library library; 
   FT_Error error;
   FT_Face face;
   char *font="/usr/share/fonts/truetype/freefont/FreeSans.ttf";

   gtk_init(&argc, &argv);

   error=FT_Init_FreeType(&library);
   if(error) printf("Init_FreeType Error\n");

   error=FT_New_Face(library, font, 0, &face );
   if(error) printf("FT_New_Face Error.\n");

   error=FT_Set_Char_Size(face, 200*64, 200*64, 72, 72);
   if(error) printf("FT_Set_Char_Size Error.\n");

   GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
   gtk_window_set_title(GTK_WINDOW(window), "Freetype");
   gtk_window_set_default_size(GTK_WINDOW(window), 800, 550);
   gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
   g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

   GtkWidget *da=gtk_drawing_area_new();
   gtk_widget_set_hexpand(da, TRUE);
   gtk_widget_set_vexpand(da, TRUE);
   g_signal_connect(da, "draw", G_CALLBACK(draw_main), face);

   GtkWidget *combo_letters=gtk_combo_box_text_new();
   gtk_widget_set_hexpand(combo_letters, TRUE);
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 0, "1", "A");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 1, "2", "a");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 2, "3", "B");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 3, "4", "b");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 4, "5", "C");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 5, "6", "c");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 6, "7", "D");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 7, "8", "d");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 8, "9", "E");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 9, "10", "e");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 10, "11", "F");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 11, "12", "f");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 12, "13", "G");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 13, "14", "g");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 14, "15", "H");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 15, "16", "h");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 16, "17", "I");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 17, "18", "i");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 18, "19", "J");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 19, "20", "j");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 20, "21", "K");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 21, "22", "k");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 22, "23", "L");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 23, "24", "l");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 24, "25", "M");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 25, "26", "m");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 26, "27", "N");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 27, "28", "n");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 28, "29", "O");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 29, "30", "o");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 30, "31", "P");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 31, "32", "p");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 32, "33", "Q");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 33, "34", "q");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 34, "35", "R");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 35, "36", "r");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 36, "37", "S");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 37, "38", "s");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 38, "39", "T");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 39, "40", "t");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 40, "41", "U");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 41, "42", "u");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 42, "43", "V");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 43, "44", "v");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 44, "45", "W");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 45, "46", "w");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 46, "47", "X");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 47, "48", "x");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 48, "49", "Y");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 49, "50", "y");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 50, "51", "Z");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_letters), 51, "52", "z");
   gtk_combo_box_set_active(GTK_COMBO_BOX(combo_letters), 0);
   g_signal_connect(combo_letters, "changed", G_CALLBACK(set_letter), da);

   GtkWidget *combo_drawing=gtk_combo_box_text_new();
   gtk_widget_set_hexpand(combo_drawing, TRUE);
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "1", "Static Draw");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "2", "Rotate Draw Lines");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 2, "3", "Rotate Draw Stencil");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 3, "4", "Rotate Draw Fill");
   gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
   g_signal_connect(combo_drawing, "changed", G_CALLBACK(set_drawing), da);

   GtkWidget *combo_stress=gtk_combo_box_text_new();
   gtk_widget_set_hexpand(combo_stress, TRUE);
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 0, "1", "No Stress");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 1, "2", "Stress 2x");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 2, "3", "Stress 2y");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 3, "4", "Stress 2z");
   gtk_combo_box_set_active(GTK_COMBO_BOX(combo_stress), 0);
   g_signal_connect(combo_stress, "changed", G_CALLBACK(set_stress), da);

   GtkWidget *pitch_label=gtk_label_new("Pitch");
   gtk_widget_set_hexpand(pitch_label, TRUE);

   GtkWidget *roll_label=gtk_label_new("Roll");
   gtk_widget_set_hexpand(roll_label, TRUE);

   GtkWidget *yaw_label=gtk_label_new("Yaw");
   gtk_widget_set_hexpand(yaw_label, TRUE);

   GtkWidget *extrude_label=gtk_label_new("Extrude");
   gtk_widget_set_hexpand(extrude_label, TRUE);

   GtkWidget *perspective_label=gtk_label_new("Perspective");
   gtk_widget_set_hexpand(perspective_label, TRUE);

   GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
   gtk_widget_set_vexpand(pitch_slider, TRUE);

   GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
   gtk_widget_set_vexpand(roll_slider, TRUE);

   GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
   gtk_widget_set_vexpand(yaw_slider, TRUE);

   GtkWidget *extrude_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 20.0, 100.0, 1.0);
   gtk_widget_set_vexpand(extrude_slider, TRUE);

   GtkWidget *perspective_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 100.0, 1.0);
   gtk_widget_set_vexpand(perspective_slider, TRUE);

   g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_pitch), da);
   g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_roll), da);
   g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_yaw), da);
   g_signal_connect(extrude_slider, "change-value", G_CALLBACK(set_extrude), da);
   g_signal_connect(perspective_slider, "change-value", G_CALLBACK(set_perspective), da);

   GtkWidget *grid1=gtk_grid_new();
   gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
   gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
   gtk_grid_attach(GTK_GRID(grid1), combo_letters, 0, 0, 1, 1);
   gtk_grid_attach(GTK_GRID(grid1), combo_drawing, 1, 0, 4, 1);
   gtk_grid_attach(GTK_GRID(grid1), combo_stress, 1, 1, 4, 1);
   gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 2, 1, 1);
   gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 2, 1, 1);
   gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 2, 1, 1);
   gtk_grid_attach(GTK_GRID(grid1), extrude_label, 3, 2, 1, 1);
   gtk_grid_attach(GTK_GRID(grid1), perspective_label, 4, 2, 1, 1);
   gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 3, 1, 1);
   gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 3, 1, 1);
   gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 3, 1, 1);
   gtk_grid_attach(GTK_GRID(grid1), extrude_slider, 3, 3, 1, 1);
   gtk_grid_attach(GTK_GRID(grid1), perspective_slider, 4, 3, 1, 1);

   GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
   gtk_paned_pack1(GTK_PANED(paned1), grid1, TRUE, TRUE);
   gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
   gtk_paned_set_position(GTK_PANED(paned1), 325);
   
   gtk_container_add(GTK_CONTAINER(window), paned1);

   gtk_widget_show_all(window);

   gtk_main();

   if(path_s!=NULL) g_array_free(path_s, TRUE);
   FT_Done_Face(face);
   FT_Done_FreeType(library);

   return 0;  
 }
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_pitch=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_roll=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_yaw=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_extrude(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_extrude=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>100.0) slider_perspective=100.0;
    else if(value<0.0) slider_perspective=0.0;
    else slider_perspective=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_letter(GtkComboBox *combo, gpointer data)
  {   
    text_index=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_drawing(GtkComboBox *combo, gpointer data)
  {   
    drawing_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_stress(GtkComboBox *combo, gpointer data)
  {  
    stress_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static gboolean draw_main(GtkWidget *da, cairo_t *cr, FT_Face face)
  {
    GTimer *timer=g_timer_new();
    if(drawing_id==0) draw_font(da, cr, face);
    else draw_font2(da, cr, face);
    g_print("Time %f\n", g_timer_elapsed(timer, NULL));
    g_timer_destroy(timer);
    return FALSE;
  }
static void draw_font(GtkWidget *da, cairo_t *cr, FT_Face face)
 {
   gint i=0;
   gint j=0;
   gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
   gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
   gdouble w1=1.0*width/10.0;
   gdouble h1=1.0*height/10.0;

   cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
   cairo_paint(cr);

   //Cartesian coordinates for drawing.
   cairo_set_line_width(cr, 1.0);
   cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
   cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
   cairo_stroke(cr);
   cairo_move_to(cr, 1.0*w1, 5.0*h1);
   cairo_line_to(cr, 9.0*w1, 5.0*h1);
   cairo_stroke(cr);
   cairo_move_to(cr, 5.0*w1, 1.0*h1);
   cairo_line_to(cr, 5.0*w1, 9.0*h1);
   cairo_stroke(cr);

   FT_Error error;
   FT_GlyphSlot slot;
   FT_Outline outline;
   error=FT_Load_Char(face, text[text_index], FT_LOAD_NO_SCALE);
   if(error) g_print("FT_Load_Char error.\n");
   slot=face->glyph;
   outline=slot->outline;

   //Check contours and points.
   //g_print("n_points %i n_contours %i\n", outline.n_points, outline.n_contours);
   //for(i=0;i<outline.n_contours;i++) g_print("contours %i\n", outline.contours[i]);

   FT_Vector *p1=outline.points;
   char *c1=outline.tags;

   //Invert points and letter.
   for(i=0;i<outline.n_points;i++)
     {
       (p1->y)*=-1;
       p1++;
     }   

   //Get a bounding box.
   FT_BBox box;
   gdouble center_x=0.0;
   gdouble center_y=0.0;
   FT_Outline_Get_BBox(&outline, &box);
   //g_print("box %i %i %i %i\n", (int)box.xMin, (int)box.yMin, (int)box.xMax, (int)box.yMax);
   cairo_set_source_rgba(cr, 0.0, 0.5, 1.0, 1.0);
   cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
   cairo_set_line_width(cr, 2.0);
   center_x=((gdouble)box.xMax*scale-(gdouble)box.xMin*scale)/2.0;
   center_y=((gdouble)box.yMax*scale-(gdouble)box.yMin*scale)/2.0;
   cairo_save(cr);
   cairo_translate(cr, width/2.0-center_x, height/2.0-center_y);
   cairo_rectangle(cr, 0.0, 0.0, 2.0*center_x, 2.0*center_y);
   cairo_stroke(cr);
   cairo_restore(cr);

   //Center points.
   p1=outline.points;
   for(i=0;i<outline.n_points;i++)
     {
       (p1->x)=(p1->x)-center_x*1.0/scale-box.xMin;
       (p1->y)=(p1->y)+center_y*1.0/scale-box.yMax;
       p1++;
     }
   
   //Draw from center
   cairo_translate(cr, width/2.0, height/2.0);

   //Draw the points.
   p1=outline.points;
   cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
   cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
   cairo_set_line_width(cr, 16.0);
   gint contour_i=0;
   gint contour_n=outline.contours[contour_i];
   gboolean contour_set=FALSE;
   for(i=0;i<outline.n_points;i++)
     {
       if(*c1==FT_CURVE_TAG_ON)
         {
           //g_print("on ");
           cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
         }
       else if(*c1==FT_CURVE_TAG_CONIC)
         {
           //g_print("off ");
           cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
         }
       else g_print("error ");
       if(i==0||contour_set)
         {
           cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
           contour_set=FALSE;
         }
       //g_print("%i %i %i %i\n", i, (int)(*c1), (int)p1->x, (int)p1->y);
       cairo_move_to(cr, (p1->x)*scale, (p1->y)*scale);
       cairo_line_to(cr, (p1->x)*scale, (p1->y)*scale);
       cairo_stroke(cr);
       p1++;c1++;
       //Advance the contour if needed.
       if(i==contour_n)
         {
           contour_i++;
           contour_n=outline.contours[contour_i];
           contour_set=TRUE;
         }
     }

   //Draw the lines between the points.
   cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
   cairo_set_line_width(cr, 4.0);
   p1=outline.points;
   c1=outline.tags;
   gboolean draw=FALSE;
   //For converting quadratic bezier point to cubic.
   gdouble q1_x=0.0;
   gdouble q1_y=0.0;
   gdouble q2_x=0.0;
   gdouble q2_y=0.0;
   //Save the last point whether virtual or in the array.
   gdouble last_x=0.0;
   gdouble last_y=0.0;
   //For counting bezier points.
   gint count=0;
   //The virtual drawing points from the bezier points.
   GArray *virtual=g_array_new(FALSE, FALSE, sizeof(gdouble));
   gdouble v_pointx=0.0;
   gdouble v_pointy=0.0;
   //The start of the first contour.
   gdouble c_startx=0.0;
   gdouble c_starty=0.0;
   gint j_start=0;
   //The points in each contour.
   gint contour_points=0;
   //Draw the contours.
   for(i=0;i<outline.n_contours;i++)
     {
       if(i==0) contour_points=outline.contours[i]+1;
       else contour_points=outline.contours[i]-outline.contours[i-1];
       //If the start of the contour is a conic off.
       if(*c1==FT_CURVE_TAG_CONIC)
         {
           //g_print("is off %f %f %f %f %i\n", (double)((p1+contour_points-1)->x), (double)((p1+contour_points-1)->y), (double)p1->y, (double)p1->y, contour_points);
           c_startx=((((p1+contour_points-1)->x)+(p1->x))/2.0)*scale;
           c_starty=((((p1+contour_points-1)->y)+(p1->y))/2.0)*scale;
           last_x=c_startx;
           last_y=c_starty;
           g_array_append_val(virtual, c_startx);
           g_array_append_val(virtual, c_starty);
           j_start=0;
         }
       else
         {
           last_x=(p1->x)*scale;
           last_y=(p1->y)*scale;
           c_startx=(p1->x)*scale;
           c_starty=(p1->y)*scale;
           j_start=1;
           p1++;c1++;
         }
      
       cairo_move_to(cr, c_startx, c_starty);
       
       for(j=j_start;j<contour_points;j++)
         {  
           if(*c1==FT_CURVE_TAG_CONIC&&!draw)
             {
               count++;
               if(count==2) draw=TRUE;             
             }          
           else if(*c1==FT_CURVE_TAG_ON&&!draw)
             {
               draw=TRUE;
             }
      
           //Draw the contour segments.
           if(draw)
             {       
               if(count==1)
                 {
                   q1_x=(last_x+2.0*((p1-1)->x)*scale)/3.0;
	           q1_y=(last_y+2.0*((p1-1)->y)*scale)/3.0;
                   q2_x=((p1->x)*scale+2.0*((p1-1)->x)*scale)/3.0;
	           q2_y=((p1->y)*scale+2.0*((p1-1)->y)*scale)/3.0;
                   cairo_curve_to(cr, q1_x, q1_y, q2_x, q2_y, (p1->x)*scale, (p1->y)*scale);
                   count=0.0;
                   last_x=(p1->x)*scale;
                   last_y=(p1->y)*scale;
                 }
               else if(count==2)
                 {
                   v_pointx=((((p1-1)->x)+(p1->x))/2)*scale;
                   v_pointy=((((p1-1)->y)+(p1->y))/2)*scale;
                   g_array_append_val(virtual, v_pointx);
                   g_array_append_val(virtual, v_pointy);
                   q1_x=(last_x+2.0*((p1-1)->x)*scale)/3.0;
	           q1_y=(last_y+2.0*((p1-1)->y)*scale)/3.0;
                   q2_x=(v_pointx+2.0*((p1-1)->x)*scale)/3.0;
	           q2_y=(v_pointy+2.0*((p1-1)->y)*scale)/3.0;
                   cairo_curve_to(cr, q1_x, q1_y, q2_x, q2_y, v_pointx, v_pointy);
                   count--;
                   last_x=v_pointx;
                   last_y=v_pointy;
                 }
               else
                 {
                   cairo_line_to(cr, (p1->x)*scale, (p1->y)*scale);
                   count=0;
                   last_x=(p1->x)*scale;
                   last_y=(p1->y)*scale;
                 }
               draw=FALSE;           
             }

           //End drawing the countour.
           if(j==contour_points-1)
             {
               if(count==1)
                 {
                   q1_x=(last_x+2.0*(p1->x)*scale)/3.0;
	           q1_y=(last_y+2.0*(p1->y)*scale)/3.0;
                   q2_x=(c_startx+2.0*(p1->x)*scale)/3.0;
	           q2_y=(c_starty+2.0*(p1->y)*scale)/3.0;
                   cairo_curve_to(cr, q1_x, q1_y, q2_x, q2_y, c_startx, c_starty);
                 }
               else if(count==2)
                 {
                   v_pointx=((((p1-1)->x)+(p1->x))/2)*scale;
                   v_pointy=((((p1-1)->y)+(p1->y))/2)*scale;
                   g_array_append_val(virtual, v_pointx);
                   g_array_append_val(virtual, v_pointy);
                   q1_x=(last_x+2.0*((p1-1)->x)*scale)/3.0;
	           q1_y=(last_y+2.0*((p1-1)->y)*scale)/3.0;
                   q2_x=(v_pointx+2.0*((p1-1)->x)*scale)/3.0;
	           q2_y=(v_pointy+2.0*((p1-1)->y)*scale)/3.0;
                   cairo_curve_to(cr, q1_x, q1_y, q2_x, q2_y, v_pointx, v_pointy);
                   q1_x=(v_pointx+2.0*(p1->x)*scale)/3.0;
	           q1_y=(v_pointy+2.0*(p1->y)*scale)/3.0;
                   q2_x=(c_startx+2.0*(p1->x)*scale)/3.0;
	           q2_y=(c_starty+2.0*(p1->y)*scale)/3.0;
                   cairo_curve_to(cr, q1_x, q1_y, q2_x, q2_y, c_startx, c_starty);
                 }
               else
                 {
                   cairo_line_to(cr, (p1->x)*scale, (p1->y)*scale);
                   cairo_line_to(cr, c_startx, c_starty);
                 }
                cairo_stroke(cr);
                count=0.0;
                draw=FALSE;
              }

           p1++;c1++;
         }
     }

   //Draw the virtual points.
   cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
   for(i=0;i<virtual->len;i+=2)
     {
       cairo_move_to(cr, g_array_index(virtual, gdouble, i), g_array_index(virtual, gdouble, i+1));
       cairo_line_to(cr, g_array_index(virtual, gdouble, i), g_array_index(virtual, gdouble, i+1));
       cairo_stroke(cr);
     }

   g_array_free(virtual, TRUE);
 }
static void draw_font2(GtkWidget *da, cairo_t *cr, FT_Face face)
 {
   gint i=0;
   static gint index=-1;
   gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
   gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
   gdouble w1=1.0*width/10.0;
   gdouble h1=1.0*height/10.0;

   cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
   cairo_paint(cr);

   //Cartesian coordinates for drawing.
   cairo_set_line_width(cr, 1.0);
   cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
   cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
   cairo_stroke(cr);
   cairo_move_to(cr, 1.0*w1, 5.0*h1);
   cairo_line_to(cr, 9.0*w1, 5.0*h1);
   cairo_stroke(cr);
   cairo_move_to(cr, 5.0*w1, 1.0*h1);
   cairo_line_to(cr, 5.0*w1, 9.0*h1);
   cairo_stroke(cr);

   //Get a new path from the points if needed.
   if(text_index!=index)
     {
       if(path_s!=NULL) g_array_free(path_s, TRUE);
       index=text_index;
       g_print("New Char\n");
       path_s=get_font_path(face, index);
     }

   //Make two copies of the path.
   GArray *path1=g_array_sized_new(FALSE, FALSE, sizeof(struct segment), path_s->len);
   GArray *path2=g_array_sized_new(FALSE, FALSE, sizeof(struct segment), path_s->len);
   struct segment *p1=&g_array_index(path_s, struct segment, 0);
   struct segment *p2=&g_array_index(path1, struct segment, 0);
   struct segment *p3=&g_array_index(path2, struct segment, 0);
   memcpy(p2, p1, sizeof(struct segment)*(path_s->len));
   memcpy(p3, p1, sizeof(struct segment)*(path_s->len));
   g_array_set_size(path1, path_s->len);
   g_array_set_size(path2, path_s->len);

   //Split slider extrude to keep z-axis centered.
   gdouble shift=slider_extrude/2.0;
   for(i=0;i<path1->len;i++)
     {
       (p2->z)=shift;
       (p3->z)=-shift;
       p2++;p3++;
     }

   /*
     Apply 3d transforms to the letter. Rotation matrix(qrs), Stress matrix(sm) and a 
     Perspective matrix(pm).
   */
   gdouble qrs[9];
   gdouble sm[9];
   gdouble pm[9];
   gdouble pitch=slider_pitch*G_PI/180.0;
   gdouble roll=slider_roll*G_PI/180.0;
   gdouble yaw=slider_yaw*G_PI/180.0;
   quaternion_rotation(yaw, roll, pitch, qrs);
   if(stress_id==0)
     {
        sm[0]=1.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=1.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=1.0; 
     }
   else if(stress_id==1)
      {
        sm[0]=2.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=1.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=1.0;        
      }
    else if(stress_id==2)
      {
        sm[0]=1.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=2.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=1.0;
      }
    else
      {
        sm[0]=1.0;sm[1]=0.0;sm[2]=0.0;
        sm[3]=0.0;sm[4]=1.0;sm[5]=0.0;
        sm[6]=0.0;sm[7]=0.0;sm[8]=2.0;
      }
   gdouble perspective1=(100.0-slider_perspective)/100.0; 
   pm[0]=perspective1;pm[1]=0.0;pm[2]=0.0;
   pm[3]=0.0;pm[4]=perspective1;pm[5]=0.0;
   pm[6]=0.0;pm[7]=0.0;pm[8]=1.0;
   matrix_multiply(qrs, sm);
   transform_letter(path1, qrs);
   matrix_multiply(qrs, pm);   
   transform_letter(path2, qrs);

   //Draw from the center
   cairo_translate(cr, width/2.0, height/2.0);
   cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);

   //Draw the paths.
   if(drawing_id==1)
     {
       cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
       cairo_set_line_width(cr, 4.0);
       draw_path_contours_connect(cr, path1, path2);
       draw_path_contours_line(cr, path2);
       draw_path_contours_line(cr, path1);
     }
   else if(drawing_id==2)
     {
       cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
       cairo_set_line_width(cr, 4.0);
       draw_path_contours_line(cr, path2);
       draw_path_contours_line(cr, path1);
       cairo_set_line_width(cr, 0.01);
       draw_path_side_fill(cr, path1, path2);
     }
   else
     {
       if(v1_r[2]>=0.0)
         {
           cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
           cairo_set_line_width(cr, 0.01);
           draw_path_contours_fill(cr, path2);
           cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
           draw_path_side_fill(cr, path1, path2);
           cairo_set_operator(cr, CAIRO_OPERATOR_SATURATE);
           cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
           draw_path_contours_fill(cr, path1);
           cairo_set_operator(cr, CAIRO_OPERATOR_OVER);
           cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
           cairo_set_line_width(cr, 4.0);
           draw_path_contours_line(cr, path1);
         }
       else
         {
           cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
           cairo_set_line_width(cr, 0.01);
           draw_path_contours_fill(cr, path1);
           cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
           draw_path_side_fill(cr, path1, path2);
           cairo_set_operator(cr, CAIRO_OPERATOR_SATURATE);
           cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
           draw_path_contours_fill(cr, path2);
           cairo_set_operator(cr, CAIRO_OPERATOR_OVER);
           cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
           cairo_set_line_width(cr, 4.0);
           draw_path_contours_line(cr, path2);
         }       
     }
   
   g_array_free(path1, TRUE);
   g_array_free(path2, TRUE);
 }
static void draw_path_contours_line(cairo_t *cr, GArray *path)
 {
   gint i=0;
   gdouble c1=0.0;
   gdouble c2=0.0;
   gdouble c3=0.0;
   gdouble c4=0.0;
   struct segment *p1=&g_array_index(path, struct segment, 0);
   while(i<path->len)
     {
       if((p1->move_id)==MOVE_TO_R||(p1->move_id)==MOVE_TO_L)
         {
           cairo_move_to(cr, p1->x, p1->y);
           p1++;i++;           
         }
       else if((p1->move_id)==LINE_TO)
         {
           cairo_line_to(cr, p1->x, p1->y);
           p1++;i++; 
         }
       else if((p1->move_id)==CURVE_TO)
         {
           c1=p1->x;c2=p1->y;
           p1++;i++; 
           c3=p1->x;c4=p1->y;
           p1++;i++; 
           cairo_curve_to(cr, c1, c2, c3, c4, p1->x, p1->y);
           p1++;i++; 
         }
       else if((p1->move_id)==LINE_TO_STROKE)
         {
           cairo_line_to(cr, p1->x, p1->y);
           cairo_stroke(cr);
           p1++;i++; 
         }
       else if((p1->move_id)==CURVE_TO_STROKE)
         {
           c1=p1->x;c2=p1->y;
           p1++;i++; 
           c3=p1->x;c4=p1->y;
           p1++;i++; 
           cairo_curve_to(cr, c1, c2, c3, c4, p1->x, p1->y);
           cairo_stroke(cr);
           p1++;i++; 
         }
       else g_print("Error\n");
     
     }
 }
static void draw_path_contours_connect(cairo_t *cr, GArray *path1, GArray *path2)
 {
   gint i=0;
   struct segment *p1=&g_array_index(path1, struct segment, 0);
   struct segment *p2=&g_array_index(path2, struct segment, 0);
   while(i<path1->len)
     {
       if( ((p1->move_id)==CURVE_TO_STROKE)||((p1->move_id)==CURVE_TO ))
         {
           p1+=2;p2+=2;i+=2;
           cairo_move_to(cr, p1->x, p1->y);
           cairo_line_to(cr, p2->x, p2->y);
           cairo_stroke(cr);
           p1++;p2++;i++;
         }
       else
         {
           cairo_move_to(cr, p1->x, p1->y);
           cairo_line_to(cr, p2->x, p2->y);
           cairo_stroke(cr);
           p1++;p2++;i++;
         }           
     }
 }
static void draw_path_side_fill(cairo_t *cr, GArray *path1, GArray *path2)
 {
   gint i=0;
   gint count=0;
   gboolean jump3=FALSE;
   gdouble c1=0.0;
   gdouble c2=0.0;
   gdouble c3=0.0;
   gdouble c4=0.0;
   struct segment *p1=&g_array_index(path1, struct segment, 0);
   struct segment *p2=&g_array_index(path2, struct segment, 0);
   while(i<(path1->len-1))
     {
       //If at the end of a contour move again.
       if((p1+1)->move_id==MOVE_TO_R||(p1+1)->move_id==MOVE_TO_L)
         {
           p1++;p2++;i++;
         }

       if(count==0)
         {
           cairo_move_to(cr, p1->x, p1->y);
           count++;           
         }
       else if(count==1&&(((p1+1)->move_id)==LINE_TO||((p1+1)->move_id)==LINE_TO_STROKE))
         {
           cairo_line_to(cr, (p1+1)->x, (p1+1)->y);
           count++; 
         }
       else if(count==1&&(((p1+1)->move_id)==CURVE_TO||((p1+1)->move_id)==CURVE_TO_STROKE))
         {
           c1=(p1+1)->x;c2=(p1+1)->y;
           c3=(p1+2)->x;c4=(p1+2)->y; 
           cairo_curve_to(cr, c1, c2, c3, c4, (p1+3)->x, (p1+3)->y);
           jump3=TRUE;
           count++; 
         }
       else if(count==2&&jump3)
         {
           cairo_line_to(cr, (p2+3)->x, (p2+3)->y);
           count++; 
         }
       else if(count==2)
         {
           cairo_line_to(cr, (p2+1)->x, (p2+1)->y);
           count++; 
         }
       else if(count==3&&(((p2+1)->move_id)==LINE_TO||((p2+1)->move_id)==LINE_TO_STROKE))
         {
           cairo_line_to(cr, p2->x, p2->y);
           count++; 
         }
       else if(count==3&&jump3&&(((p2+1)->move_id)==CURVE_TO||((p2+1)->move_id)==CURVE_TO_STROKE))
         {
           c1=(p2+2)->x;c2=(p2+2)->y;
           c3=(p2+1)->x;c4=(p2+1)->y; 
           cairo_curve_to(cr, c1, c2, c3, c4, p2->x, p2->y);
           count++; 
         }
       else if(count==4)
         {
           cairo_close_path(cr);
           cairo_fill(cr);
           count=0;
           if(jump3)
             {
               p1+=3;p2+=3;i+=3;
             }
           else
             {
               p1++;p2++;i++;
             }
           jump3=FALSE;
         }
       else
         {
           g_print("Error Count %i\n", count);
           p1++;p2++;i++;
         }
     
     }
 }
static void draw_path_contours_fill(cairo_t *cr, GArray *path)
 {
   gint i=0;
   gdouble c1=0.0;
   gdouble c2=0.0;
   gdouble c3=0.0;
   gdouble c4=0.0;
   struct segment *p1=&g_array_index(path, struct segment, 0);
   while(i<path->len)
     {
       if((p1->move_id)==MOVE_TO_R||(p1->move_id)==MOVE_TO_L)
         {
           //For counter clockwise fill. 
           if((p1->move_id)==MOVE_TO_L)
             {
               cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
             }
           cairo_move_to(cr, p1->x, p1->y);
           p1++;i++;           
         }
       else if((p1->move_id)==LINE_TO)
         {
           cairo_line_to(cr, p1->x, p1->y);
           p1++;i++; 
         }
       else if((p1->move_id)==CURVE_TO)
         {
           c1=p1->x;c2=p1->y;
           p1++;i++; 
           c3=p1->x;c4=p1->y;
           p1++;i++; 
           cairo_curve_to(cr, c1, c2, c3, c4, p1->x, p1->y);
           p1++;i++; 
         }
       else if((p1->move_id)==LINE_TO_STROKE)
         {
           cairo_line_to(cr, p1->x, p1->y);
           cairo_close_path(cr);
           cairo_fill(cr);
           p1++;i++; 
         }
       else if((p1->move_id)==CURVE_TO_STROKE)
         {
           c1=p1->x;c2=p1->y;
           p1++;i++; 
           c3=p1->x;c4=p1->y;
           p1++;i++; 
           cairo_curve_to(cr, c1, c2, c3, c4, p1->x, p1->y);
           cairo_close_path(cr);
           cairo_fill(cr);
           p1++;i++; 
         }
       else g_print("Error\n");     
     }
 }
static FT_Outline get_font_outline(FT_Face face, gint index)
 {
   gint i=0;
   FT_Error error;
   FT_GlyphSlot slot;
   FT_Outline outline;
   error=FT_Load_Char(face, text[index], FT_LOAD_NO_SCALE);
   if(error) g_print("FT_Load_Char error.\n");
   slot=face->glyph;
   outline=slot->outline;

   //Invert points and letter.
   FT_Vector *p=outline.points;
   for(i=0;i<outline.n_points;i++)
     {
       (p->y)*=-1;
       p++;
     }

   //Get a bounding box.
   FT_BBox box;
   gdouble center_x=0.0;
   gdouble center_y=0.0;
   FT_Outline_Get_BBox(&outline, &box);
   center_x=((gdouble)box.xMax*scale-(gdouble)box.xMin*scale)/2.0;
   center_y=((gdouble)box.yMax*scale-(gdouble)box.yMin*scale)/2.0;

   //Center points for rotation around the center.
   p=outline.points;
   for(i=0;i<outline.n_points;i++)
     {
       (p->x)=(p->x)-center_x*1.0/scale-box.xMin;
       (p->y)=(p->y)+center_y*1.0/scale-box.yMax;
       p++;
     }

   return outline;
 }
static GArray* get_font_path(FT_Face face, gint index)
 {
   gint i=0;
   gint j=0;

   FT_Outline outline=get_font_outline(face, index);

   GArray *path=NULL;
   struct segment seg;
   if(outline.n_points<1)
     {
       goto exit;
     }
   else
     {
       path=g_array_new(FALSE, FALSE, sizeof(struct segment));
     }

   FT_Vector *p1=outline.points;
   char *c1=outline.tags;

   p1=outline.points;
   gboolean draw=FALSE;
   //For converting quadratic bezier point to cubic.
   gdouble q1_x=0.0;
   gdouble q1_y=0.0;
   gdouble q2_x=0.0;
   gdouble q2_y=0.0;
   //Save the last point whether virtual or in the array.
   gdouble last_x=0.0;
   gdouble last_y=0.0;
   //For counting bezier points.
   gint count=0;
   gdouble v_pointx=0.0;
   gdouble v_pointy=0.0;
   //The start of the first contour.
   gdouble c_startx=0.0;
   gdouble c_starty=0.0;
   gint j_start=0;
   //The points in each contour.
   gint contour_points=0;
   //Draw the contours.
   for(i=0;i<outline.n_contours;i++)
     {
       if(i==0) contour_points=outline.contours[i]+1;
       else contour_points=outline.contours[i]-outline.contours[i-1];
       //If the start of the contour is a conic off.
       if(*c1==FT_CURVE_TAG_CONIC)
         {
           //g_print("is off %f %f %f %f %i\n", (double)((p1+contour_points-1)->x), (double)((p1+contour_points-1)->y), (double)p1->y, (double)p1->y, contour_points);
           c_startx=((((p1+contour_points-1)->x)+(p1->x))/2.0)*scale;
           c_starty=((((p1+contour_points-1)->y)+(p1->y))/2.0)*scale;
           last_x=c_startx;
           last_y=c_starty;
           //g_array_append_val(virtual, c_startx);
           //g_array_append_val(virtual, c_starty);
           j_start=0;
         }
       else
         {
           last_x=(p1->x)*scale;
           last_y=(p1->y)*scale;
           c_startx=(p1->x)*scale;
           c_starty=(p1->y)*scale;
           j_start=1;
           p1++;c1++;
         }
      
       //cairo_move_to(cr, c_startx, c_starty);
       seg.move_id=MOVE_TO_R;seg.x=c_startx;seg.y=c_starty;seg.z=0.0;
       g_array_append_val(path, seg);
       
       for(j=j_start;j<contour_points;j++)
         {  
           if(*c1==FT_CURVE_TAG_CONIC&&!draw)
             {
               count++;
               if(count==2) draw=TRUE;             
             }          
           else if(*c1==FT_CURVE_TAG_ON&&!draw)
             {
               draw=TRUE;
             }
      
           //Draw the contour segments.
           if(draw)
             {       
               if(count==1)
                 {
                   q1_x=(last_x+2.0*((p1-1)->x)*scale)/3.0;
	           q1_y=(last_y+2.0*((p1-1)->y)*scale)/3.0;
                   q2_x=((p1->x)*scale+2.0*((p1-1)->x)*scale)/3.0;
	           q2_y=((p1->y)*scale+2.0*((p1-1)->y)*scale)/3.0;
                   //cairo_curve_to(cr, q1_x, q1_y, q2_x, q2_y, (p1->x)*scale, (p1->y)*scale);
                   seg.move_id=CURVE_TO;seg.x=q1_x;seg.y=q1_y;seg.z=0.0;
                   g_array_append_val(path, seg);
                   seg.move_id=CURVE_TO;seg.x=q2_x;seg.y=q2_y;seg.z=0.0;
                   g_array_append_val(path, seg);
                   seg.move_id=CURVE_TO;seg.x=(p1->x)*scale;seg.y=(p1->y)*scale;seg.z=0.0;
                   g_array_append_val(path, seg);
                   count=0.0;
                   last_x=(p1->x)*scale;
                   last_y=(p1->y)*scale;
                 }
               else if(count==2)
                 {
                   v_pointx=((((p1-1)->x)+(p1->x))/2)*scale;
                   v_pointy=((((p1-1)->y)+(p1->y))/2)*scale;
                   //g_array_append_val(virtual, v_pointx);
                   //g_array_append_val(virtual, v_pointy);
                   q1_x=(last_x+2.0*((p1-1)->x)*scale)/3.0;
	           q1_y=(last_y+2.0*((p1-1)->y)*scale)/3.0;
                   q2_x=(v_pointx+2.0*((p1-1)->x)*scale)/3.0;
	           q2_y=(v_pointy+2.0*((p1-1)->y)*scale)/3.0;
                   //cairo_curve_to(cr, q1_x, q1_y, q2_x, q2_y, v_pointx, v_pointy);
                   seg.move_id=CURVE_TO;seg.x=q1_x;seg.y=q1_y;seg.z=0.0;
                   g_array_append_val(path, seg);
                   seg.move_id=CURVE_TO;seg.x=q2_x;seg.y=q2_y;seg.z=0.0;
                   g_array_append_val(path, seg);
                   seg.move_id=CURVE_TO;seg.x=v_pointx;seg.y=v_pointy;seg.z=0.0;
                   g_array_append_val(path, seg);
                   count--;
                   last_x=v_pointx;
                   last_y=v_pointy;
                 }
               else
                 {
                   //cairo_line_to(cr, (p1->x)*scale, (p1->y)*scale);
                   seg.move_id=LINE_TO;seg.x=(p1->x)*scale;seg.y=(p1->y)*scale;seg.z=0.0;
                   g_array_append_val(path, seg);
                   count=0;
                   last_x=(p1->x)*scale;
                   last_y=(p1->y)*scale;
                 }
               draw=FALSE;           
             }

           //End drawing the countour.
           if(j==contour_points-1)
             {
               if(count==1)
                 {
                   q1_x=(last_x+2.0*(p1->x)*scale)/3.0;
	           q1_y=(last_y+2.0*(p1->y)*scale)/3.0;
                   q2_x=(c_startx+2.0*(p1->x)*scale)/3.0;
	           q2_y=(c_starty+2.0*(p1->y)*scale)/3.0;
                   //cairo_curve_to(cr, q1_x, q1_y, q2_x, q2_y, c_startx, c_starty);
                   seg.move_id=CURVE_TO_STROKE;seg.x=q1_x;seg.y=q1_y;seg.z=0.0;
                   g_array_append_val(path, seg);
                   seg.move_id=CURVE_TO_STROKE;seg.x=q2_x;seg.y=q2_y;seg.z=0.0;
                   g_array_append_val(path, seg);
                   seg.move_id=CURVE_TO_STROKE;seg.x=c_startx;seg.y=c_starty;seg.z=0.0;
                   g_array_append_val(path, seg);
                 }
               else if(count==2)
                 {
                   v_pointx=((((p1-1)->x)+(p1->x))/2)*scale;
                   v_pointy=((((p1-1)->y)+(p1->y))/2)*scale;
                   //g_array_append_val(virtual, v_pointx);
                   //g_array_append_val(virtual, v_pointy);
                   q1_x=(last_x+2.0*((p1-1)->x)*scale)/3.0;
	           q1_y=(last_y+2.0*((p1-1)->y)*scale)/3.0;
                   q2_x=(v_pointx+2.0*((p1-1)->x)*scale)/3.0;
	           q2_y=(v_pointy+2.0*((p1-1)->y)*scale)/3.0;
                   //cairo_curve_to(cr, q1_x, q1_y, q2_x, q2_y, v_pointx, v_pointy);
                   seg.move_id=CURVE_TO_STROKE;seg.x=q1_x;seg.y=q1_y;seg.z=0.0;
                   g_array_append_val(path, seg);
                   seg.move_id=CURVE_TO_STROKE;seg.x=q2_x;seg.y=q2_y;seg.z=0.0;
                   g_array_append_val(path, seg);
                   seg.move_id=CURVE_TO_STROKE;seg.x=v_pointx;seg.y=v_pointy;seg.z=0.0;
                   g_array_append_val(path, seg);
                   q1_x=(v_pointx+2.0*(p1->x)*scale)/3.0;
	           q1_y=(v_pointy+2.0*(p1->y)*scale)/3.0;
                   q2_x=(c_startx+2.0*(p1->x)*scale)/3.0;
	           q2_y=(c_starty+2.0*(p1->y)*scale)/3.0;
                   //cairo_curve_to(cr, q1_x, q1_y, q2_x, q2_y, c_startx, c_starty);
                   seg.move_id=CURVE_TO_STROKE;seg.x=q1_x;seg.y=q1_y;seg.z=0.0;
                   g_array_append_val(path, seg);
                   seg.move_id=CURVE_TO_STROKE;seg.x=q2_x;seg.y=q2_y;seg.z=0.0;
                   g_array_append_val(path, seg);
                   seg.move_id=CURVE_TO_STROKE;seg.x=c_startx;seg.y=c_starty;seg.z=0.0;
                   g_array_append_val(path, seg);
                 }
               else
                 {
                   //cairo_line_to(cr, (p1->x)*scale, (p1->y)*scale);
                   seg.move_id=LINE_TO;seg.x=(p1->x)*scale;seg.y=(p1->y)*scale;seg.z=0.0;
                   g_array_append_val(path, seg);
                   //cairo_line_to(cr, c_startx, c_starty);
                   seg.move_id=LINE_TO_STROKE;seg.x=c_startx;seg.y=c_starty;seg.z=0.0;
                   g_array_append_val(path, seg);
                 }
                //cairo_stroke(cr);
                count=0.0;
                draw=FALSE;
              }

           p1++;c1++;
         }
     }

    /*
      Find the contour directions.
      https://stackoverflow.com/questions/1165647/how-to-determine-if-a-list-of-polygon-points-are-in-clockwise-order
      note "57 A minor caveat:" comment.
    */
    struct segment *s1=&g_array_index(path, struct segment, 0);
    struct segment *s2=&g_array_index(path, struct segment, 0);
    struct segment *s3=&g_array_index(path, struct segment, 0);
    gdouble sum=0.0;
    for(i=0;i<(path->len);i++)
      {
        if((s1->move_id)==MOVE_TO_R)
          {
            s2=s1;
            s3=s1;
            sum=0.0;
          }
        else if((s1->move_id)==CURVE_TO_STROKE||(s1->move_id)==LINE_TO_STROKE)
          {
            while(s2!=s1)
              {
                sum+=((((s2+1)->x)-(s2->x))*(((s2+1)->y)+(s2->y)));
                s2++;
              }
            if(sum>=0.0) s3->move_id=MOVE_TO_L;
            else s3->move_id=MOVE_TO_R;
          }
        s1++;
      }

    exit:
    return path;
  }
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
static void transform_letter(GArray *path, gdouble qrs[9])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0;   
    struct segment *p1=NULL;

    p1=&g_array_index(path, struct segment, 0);
    for(i=0;i<(path->len);i++)
      {
        cr1=(p1->x);
        cr2=(p1->y);
        cr3=(p1->z);         
        p1->x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        p1->y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        p1->z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        p1++;
      }

    //Transform reference vector.
    cr1=v1[0];
    cr2=v1[1];
    cr3=v1[2];         
    v1_r[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
    v1_r[1]=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
    v1_r[2]=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
  }
static void matrix_multiply(gdouble qrs1[9], gdouble qrs2[9])
  {
    //Multiply two 3x3 matrices.
    gdouble m1=qrs1[0];
    gdouble m2=qrs1[1];
    gdouble m3=qrs1[2];
    gdouble m4=qrs1[3];
    gdouble m5=qrs1[4];
    gdouble m6=qrs1[5];
    gdouble m7=qrs1[6];
    gdouble m8=qrs1[7];
    gdouble m9=qrs1[8];

    qrs1[0]=m1*qrs2[0]+m4*qrs2[1]+m7*qrs2[2];
    qrs1[1]=m2*qrs2[0]+m5*qrs2[1]+m8*qrs2[2];
    qrs1[2]=m3*qrs2[0]+m6*qrs2[1]+m9*qrs2[2];

    qrs1[3]=m1*qrs2[3]+m4*qrs2[4]+m7*qrs2[5];
    qrs1[4]=m2*qrs2[3]+m5*qrs2[4]+m8*qrs2[5];
    qrs1[5]=m3*qrs2[3]+m6*qrs2[4]+m9*qrs2[5];

    qrs1[6]=m1*qrs2[6]+m4*qrs2[7]+m7*qrs2[8];
    qrs1[7]=m2*qrs2[6]+m5*qrs2[7]+m8*qrs2[8];
    qrs1[8]=m3*qrs2[6]+m6*qrs2[7]+m9*qrs2[8];
  }




