
/* 
    Test rotating a 3d spring, cylinder, cube, checkerboard, ball, wire ball, funnel,
gem, torus, twist, pyramid, cone and text ring with cairo. Need a teapot. There
is also a ..OpenGL/sphereGL.c that can compare drawing times of the 3d spheres. 
The basic idea is to initialize shape coordinates, get quaternions for rotation, 
rotate and maybe revolve the shape and draw.
    Cairo can draw 3d but there are some problems such as having the equivalent of
glEnable(GL_DEPTH_TEST) for drawing. For some shapes you can check the z-coordinate values in cairo.
Another thing to try is to order the planes(painters algorithm) before drawing like in the pyramid
or cone drawings.
    Drawing text in 3d is possible with cairo. The cubes have text that can be rotated in 3d. 
    The testing was done with the xlib backend. I think that the cairo gl backend might do automatic
depth testing but I haven't tested this out yet.
    For spring2 a shape cache was added so that you can draw more than one shape at once and rotate the
drawings individually.
    The testing was done with an older netbook with an Intel Atom CPU N270 1.60GHz × 2 logical cores.
Drawing four shapes at once is a little slow but drawing single shapes work well.
     
    gcc -Wall spring2.c -o spring2 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<stdlib.h>

struct point{
  gdouble x;
  gdouble y;
};

struct point3d{
  gdouble x;
  gdouble y;
  gdouble z;
};

//Bezier points for smoothing.
struct controls{
  gdouble x1;
  gdouble y1;
  gdouble x2;
  gdouble y2;
};

//For sorting drawing planes to draw in order from -z to +z. Used in the pyramid.
struct plane_order{
  gint id;
  gdouble value;
};

//For sorting planes in the cone drawing. Sort quad and triangle planes.
struct quad_plane{
  gdouble x1;
  gdouble y1;
  gdouble z1;
  gdouble x2;
  gdouble y2;
  gdouble z2;
  gdouble x3;
  gdouble y3;
  gdouble z3;
  gdouble x4;
  gdouble y4;
  gdouble z4;
  gdouble r;
  gdouble g;
  gdouble b;
  gdouble a;
};

struct quad_plane_mesh{
  gdouble x1;
  gdouble y1;
  gdouble z1;
  gdouble x2;
  gdouble y2;
  gdouble z2;
  gdouble x3;
  gdouble y3;
  gdouble z3;
  gdouble x4;
  gdouble y4;
  gdouble z4;
  gdouble c1[4];
  gdouble c2[4];
  gdouble c3[4];
  gdouble c4[4];
};

//The data arrays for the shape(*s) and shape rotated(*sr).
struct shape{
  gint cache_id;
  GArray *s;
  GArray *sr;
  gint rings;
  gint points_per_ring;
};

//These are the shape cache_id values or basic shapes.
enum{ 
  SPRING, 
  CYLINDER, 
  CUBE,
  CHECKERBOARD,
  BALL,
  FUNNEL,
  GEM,
  TORUS,
  TWIST,
  PYRAMID,
  CONE,
  TRIANGLE,
  TEXT_RING
}; 

//String representations for the enumumeration.
const char *shape_names[13]={"Spring", "Cylinder", "Cube", "Checkerboard", "Ball", "Funnel", "Gem", "Torus", "Twist", "Pyramid", "Cone", "Triangle", "Text Ring"}; 

//The array to hold the shapes in.
GPtrArray *shape_cache=NULL;

//A spherical point to revolve or rotate on a spherical path with quaternions.
static gdouble revolve[3]={1.0, 0.0, 0.0};
static gdouble revolve_r[3]={1.0, 0.0, 0.0};

//Combo ids for drawing options.
static gint rotate=0;
static gint drawing_id=0;
static gint initialize_id=0;

//Tick id for animation frame clock.
static guint tick_id=0;

//Draw a segment a different color in the spring.
static gdouble segment=0.0;

//Compress the spring.
static gdouble compress=1.0;

//Scale the cube.
static gdouble scale_cube=1.0;

//For the performance check boxes.
static gboolean frame_rate=FALSE;
static gboolean function_time=FALSE;

//Initialization functions for some basic shapes.
static void initialize_shape(gint cache_id, gint rings, gint points_per_ring);
static void initialize_spring();
static void initialize_cylinder(gint points_per_ring);
static void initialize_cube();
static void initialize_checkerboard();
static void initialize_ball(gint rings, gint points_per_ring);
static void initialize_funnel(gint rings, gint points_per_ring);
static void initialize_gem();
static void initialize_torus(gint rings, gint points_per_ring);
static void initialize_twist();
static void initialize_triangle();
static void initialize_pyramid();
static void initialize_cone(gint rings, gint points_per_ring);
static void initialize_text_ring(gint points_per_ring);
static void clear_shape_cache();
//UI functions for sliders and combo boxes.
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data);
static void show_frame_rate(GtkToggleButton *button, gpointer data);
static void show_function_time(GtkToggleButton *button, gpointer data);
static void rotate_drawing(GtkComboBox *combo, gpointer *rotations);
static void change_drawing(GtkComboBox *combo, gpointer data);
static void set_rotation_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations);
static void set_rotation_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations);
static void set_rotation_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations);
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer *rotations);
//The start of drawing.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer *rotations);
//Rotation functions.
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9]);
static void matrix_multiply_rotations(gdouble qrs1[9], gdouble qrs2[9], gdouble qrs3[9]);
static void revolve_standard_point(gdouble qrs[9]);
static void rotate_cache_shapes(gdouble qrs[9]);
static void rotate_cache_shape_index(gdouble qrs[9], gint index);
static void rotate_shape(const struct shape *s1, gdouble qrs[9]);
static void rotate_shape_xy(const struct shape *s1, gdouble qrs[9]);
static void rotate_shape_xyz(const struct shape *s1, gdouble qrs[9]);
static const struct shape* get_drawing_from_shape_cache(gint cache_id, gint rings, gint points_per_ring);
static void print_shapes_in_cache();
//Draw the shapes.
static void draw_spring(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_cylinder(cairo_t *cr, gdouble w1, gdouble h1, gint points_per_ring);
static void draw_cube(cairo_t *cr, gdouble w1, gdouble h1, gdouble qrs[9]);
static void draw_transparent_cube(cairo_t *cr, gdouble w1, gdouble h1, gpointer *rotations);
static void draw_checkerboard(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_ball(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
static void draw_ball_mesh(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
static void draw_wire_ball(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_wire_ball2(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_wire_funnel(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
static void draw_gem(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_funnel(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
static void draw_wire_torus(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
static void draw_torus(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
static void draw_wire_twist(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_twist(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_triangle(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_pyramid(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_cone(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
static void draw_cone_shade(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
static void draw_text_ring(cairo_t *cr, gdouble w1, gdouble h1, gint points_per_ring, gpointer *rotations);
//For comparing z values of planes. Painters algorithm.
int compare_planes(const void *a, const void *b);
int compare_quads(const void *a, const void *b);
int compare_quads_mesh(const void *a, const void *b);
//Get bezier control points for curve coordinates.
static GArray* control_points_from_coords2(const GArray *dataPoints);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Spring2");
    gtk_window_set_default_size(GTK_WINDOW(window), 875, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_widget_set_app_paintable(window, TRUE);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    //Initialize the shape cache and first drawing.
    shape_cache=g_ptr_array_new();
    initialize_spring();
    print_shapes_in_cache();

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);

    GtkWidget *combo_rotate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_rotate, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 0, "1", "Draw Shape");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 1, "2", "Animate Rotation");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_rotate), 0);

    /*
      The drawing_id is the first column and the initialize_id is the second column. The initialize_id
    is so that you can initialize and draw many different shapes. From 0-12 here cache_id
    is equal to initialize_id.
    */
    GtkWidget *combo_drawing=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_drawing, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "0", "Spring");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "1", "Cylinder");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 2, "2", "Cube");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 3, "2", "Transparent Cube");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 4, "3", "Checkerboard");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 5, "4", "Ball");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 6, "4", "Wire Ball");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 7, "4", "Wire Ball2");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 8, "4", "No Anti Alias Wire Ball2");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 9, "5", "Wire Funnel");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 10, "5", "Funnel");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 11, "6", "Gem");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 12, "7", "Wire Torus");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 13, "7", "Torus");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 14, "7", "4-Torus");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 15, "8", "Wire Twist");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 16, "8", "Twist");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 17, "11", "Triangle Shadow & Light");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 18, "9", "Pyramid");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 19, "10", "Cone");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 20, "10", "Cone Shade");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 21, "12", "Text Ring");
    //Draw several shapes.
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 22, "14", "Ball, Gem, Twist and Cone");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 23, "14", "Ball, Gem, Twist and Cone2");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 24, "15", "Revolve and Rotate Ball");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 25, "16", "Planets");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 26, "17", "Ball and Cone");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
    g_signal_connect(combo_drawing, "changed", G_CALLBACK(change_drawing), da);  

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);
    
    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider, TRUE);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider, TRUE);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider, TRUE);

    //Set initial rotations. Yaw, roll and pitch.
    gdouble plane_r[3]={0.0, 0.0, 0.0};

    //Setup callbacks.
    gpointer rotations[]={plane_r, da};
    g_signal_connect(da, "draw", G_CALLBACK(draw_main), rotations);
    g_signal_connect(combo_rotate, "changed", G_CALLBACK(rotate_drawing), rotations);
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_rotation_pitch), rotations);   
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_rotation_roll), rotations);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_rotation_yaw), rotations);

    //Some checks for performance.
    GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
    gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
    g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), NULL);

    GtkWidget *check_functions=gtk_check_button_new_with_label("Show Function Time");
    gtk_widget_set_halign(check_functions, GTK_ALIGN_CENTER);
    g_signal_connect(check_functions, "toggled", G_CALLBACK(show_function_time), NULL);

    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_rotate, 0, 0, 3, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_drawing, 0, 1, 3, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_frame, 0, 4, 3, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_functions, 0, 5, 3, 1);

    GtkWidget *scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll, TRUE);
    gtk_widget_set_vexpand(scroll, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll), grid1);
   
    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), scroll, TRUE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 350);
   
    gtk_container_add(GTK_CONTAINER(window), paned1);

    //Draw background window based on the paned window splitter.
    g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), paned1);

    gtk_widget_show_all(window);

    gtk_main();

    //Clean up shape cache.
    clear_shape_cache();
    g_ptr_array_free(shape_cache, TRUE);

    return 0;  
  }
static void initialize_shape(gint cache_id, gint rings, gint points_per_ring)
  {
    switch(cache_id)
      {
        case SPRING:
          initialize_spring();
          break;
        case CYLINDER:
          initialize_cylinder(points_per_ring);
          break;
        case CUBE:
          initialize_cube();
          break;
        case CHECKERBOARD:
          initialize_checkerboard();
          break;
        case BALL:
          initialize_ball(rings, points_per_ring);
          break;
        case FUNNEL:
          initialize_funnel(rings, points_per_ring);
          break;
        case GEM:
          initialize_gem();
          break;
        case TORUS:
          initialize_torus(rings, points_per_ring);
          break;
        case TWIST:
          initialize_twist();
          break;
        case PYRAMID:
          initialize_pyramid();
          break;
        case CONE:
          initialize_cone(rings, points_per_ring);
          break;
        case TRIANGLE:
          initialize_triangle();
          break;
        case TEXT_RING:
          initialize_text_ring(points_per_ring);
          break;
        default:
          g_print("Warning: Undefined Shape!\n");
      }
  }
static void initialize_spring()
  {
    gint i=0;
    GArray *spring=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 80);
    g_array_set_size(spring, 80);
    GArray *spring_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point), 80);
    g_array_set_size(spring_rotated, 80);

    struct point3d *p1=&g_array_index(spring, struct point3d, 0);
    for(i=0;i<80;i++)
      {
        (*p1).x=cos(45.0*(gdouble)i*G_PI/180.0);
        (*p1).y=sin(45.0*(gdouble)i*G_PI/180.0);
        (*p1).z=(gdouble)i/20.0;
        p1++;
      }

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=SPRING;
    p->s=spring;
    p->sr=spring_rotated;
    p->rings=0.0;
    p->points_per_ring=0.0;
    g_ptr_array_add(shape_cache, p);
  }
static void initialize_cylinder(gint points_per_ring)
  {
    //Only 2 rings in the cylinder.
    gint i=0;
    gint j=0;

    if(points_per_ring<10||points_per_ring>50)
      {
        points_per_ring=16;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=16.\n");
      }

    gint points=2*(points_per_ring+1);
    GArray *cylinder=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), points);
    g_array_set_size(cylinder, points);    
    GArray *cylinder_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point), points);
    g_array_set_size(cylinder_rotated, points);
    
    struct point3d *p1=&g_array_index(cylinder, struct point3d, 0);
    gdouble arc1=360.0/(gdouble)(points_per_ring);
    gdouble z_value=2.0;
    for(i=0;i<2;i++)
      {
        for(j=0;j<points_per_ring+1;j++)
          {
            (*p1).x=2.0*cos(arc1*(gdouble)j*G_PI/180.0);
            (*p1).y=2.0*sin(arc1*(gdouble)j*G_PI/180.0);
            (*p1).z=z_value;
            p1++;
          }
        z_value=-2.0;
      }

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=CYLINDER;
    p->s=cylinder;
    p->sr=cylinder_rotated;
    p->rings=0.0;
    p->points_per_ring=points_per_ring;
    g_ptr_array_add(shape_cache, p);
  }
static void initialize_cube()
  {
    GArray *cube=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 8);
    g_array_set_size(cube, 8);
    GArray *cube_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 8);
    g_array_set_size(cube_rotated, 8);

    struct point3d *p1=&g_array_index(cube, struct point3d, 0);
    (*p1).x=2.0;
    (*p1).y=2.0;
    (*p1).z=2.0;
    (*(p1+1)).x=2.0;
    (*(p1+1)).y=-2.0;
    (*(p1+1)).z=2.0;
    (*(p1+2)).x=-2.0;
    (*(p1+2)).y=-2.0;
    (*(p1+2)).z=2.0;
    (*(p1+3)).x=-2.0;
    (*(p1+3)).y=2.0;
    (*(p1+3)).z=2.0;
    (*(p1+4)).x=2.0;
    (*(p1+4)).y=2.0;
    (*(p1+4)).z=-2.0;
    (*(p1+5)).x=2.0;
    (*(p1+5)).y=-2.0;
    (*(p1+5)).z=-2.0;
    (*(p1+6)).x=-2.0;
    (*(p1+6)).y=-2.0;
    (*(p1+6)).z=-2.0;
    (*(p1+7)).x=-2.0;
    (*(p1+7)).y=2.0;
    (*(p1+7)).z=-2.0;

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=CUBE;
    p->s=cube;
    p->sr=cube_rotated;
    p->rings=0.0;
    p->points_per_ring=0.0;
    g_ptr_array_add(shape_cache, p);
  }
static void initialize_checkerboard()
  {
    gint i=0;
    gint j=0;
    GArray *checkerboard=g_array_sized_new(FALSE, TRUE, sizeof(struct point), 81);
    g_array_set_size(checkerboard, 81);
    GArray *checkerboard_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point), 81);
    g_array_set_size(checkerboard_rotated, 81);

    struct point *p1=&g_array_index(checkerboard, struct point, 0);
    for(i=0;i<9;i++)
      {
        for(j=0;j<9;j++)
          {
            (*p1).x=(gdouble)j-4.0;
            (*p1).y=(gdouble)i-4.0;
            p1++;
          }
      }

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=CHECKERBOARD;
    p->s=checkerboard;
    p->sr=checkerboard_rotated;
    p->rings=0.0;
    p->points_per_ring=0.0;
    g_ptr_array_add(shape_cache, p);
  }
static void initialize_ball(gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    if(rings<5||rings>50)
      {
        rings=5;
        g_print("Range for rings is 5<=x<=50. Set default rings=5.\n");
      }
    if(points_per_ring<10||points_per_ring>50)
      {
        points_per_ring=16;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=16.\n");
      }
   
    gint array_size=rings*(points_per_ring+1)+2;
    struct point3d *p1=NULL;

    GArray *ball=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(ball, array_size);
    p1=&g_array_index(ball, struct point3d, 0);
          
    GArray *ball_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(ball_rotated, array_size);
          
    gint ring_points=points_per_ring+1;
    gdouble arc1=180.0/(gdouble)(rings+1);
    gdouble arc2=360.0/(gdouble)(points_per_ring);
    for(i=0;i<rings;i++)
      {
        for(j=0;j<ring_points;j++)
          {
            (*p1).x=3.0*sin(arc1*(gdouble)(i+1)*G_PI/180.0)*cos(arc2*(gdouble)j*G_PI/180.0);
            (*p1).y=3.0*sin(arc1*(gdouble)(i+1)*G_PI/180.0)*sin(arc2*(gdouble)j*G_PI/180.0);
            (*p1).z=3.0*cos(arc1*(gdouble)(i+1)*G_PI/180.0);
            p1++;
          }
      }
    //Top.
    (*p1).x=0.0;
    (*p1).y=0.0;
    (*p1).z=3.0;
    p1++;
    //Bottom.
    (*p1).x=0.0;
    (*p1).y=0.0;
    (*p1).z=-3.0;
    
    struct shape *p=g_new(struct shape, 1);
    p->cache_id=BALL;
    p->s=ball;
    p->sr=ball_rotated;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    g_ptr_array_add(shape_cache, p);
  }
void initialize_funnel(gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    if(rings<10||rings>50)
      {
        rings=10;
        g_print("Range for rings is 10<=x<=50. Set default rings=10.\n");
      }
    if(points_per_ring<10||points_per_ring>50)
      {
        points_per_ring=12;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=12.\n");
      }
   
    gint array_size=rings*points_per_ring;
   
    GArray *funnel=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(funnel, array_size);
    struct point3d *p1=&g_array_index(funnel, struct point3d, 0);
          
    GArray *funnel_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(funnel_rotated, array_size);

    gdouble radius=3.0;
    gdouble z=3.0;
    gdouble z_step=6.0/rings;
    gdouble arc1=360.0/(gdouble)(points_per_ring);
    for(i=0;i<rings;i++)
      {
        for(j=0;j<points_per_ring;j++)
          {
            (*p1).x=radius*cos(arc1*(gdouble)j*G_PI/180.0);
            (*p1).y=radius*sin(arc1*(gdouble)j*G_PI/180.0);
            (*p1).z=z;
            p1++;
          }
        z-=z_step;
        radius/=1.3;
      }
    
    struct shape *p=g_new(struct shape, 1);
    p->cache_id=FUNNEL;
    p->s=funnel;
    p->sr=funnel_rotated;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
static void initialize_gem()
  {
    gint i=0;
    gint j=0;
   
    GArray *gem=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 27);
    g_array_set_size(gem, 27);
    struct point3d *p1=&g_array_index(gem, struct point3d, 0);
          
    GArray *gem_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 27);
    g_array_set_size(gem_rotated, 27);
          
    for(i=0;i<2;i++)
      {
        for(j=0;j<13;j++)
          {
            (*p1).x=(2.0+(gdouble)i)*cos(30.0*(gdouble)(j)*G_PI/180.0);
            (*p1).y=(2.0+(gdouble)i)*sin(30.0*(gdouble)(j)*G_PI/180.0);
            (*p1).z=0.0-(gdouble)i;                        
            p1++;
          }
      }
    //Bottom of the gem.
    (*p1).x=0.0;
    (*p1).y=0.0;
    (*p1).z=-3.0;

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=GEM;
    p->s=gem;
    p->sr=gem_rotated;
    p->rings=0.0;
    p->points_per_ring=0.0;
    g_ptr_array_add(shape_cache, p);
    
  }
static void initialize_torus(gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    if(rings<5||rings>50)
      {
        rings=12;
        g_print("Range for rings_torus is 5<=x<=50. Set default rings_torus=12.\n");
      }
    if(points_per_ring<5||points_per_ring>50)
      {
        points_per_ring=16;
        g_print("Range for points_per_ring_torus is 5<=x<=50. Set default points_per_ring_torus=16.\n");
      }
   
    gint array_size=rings*(points_per_ring+1);
    struct point3d *p1=NULL;
    struct point3d *p2=NULL;

    GArray *torus=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(torus, array_size);
    p1=&g_array_index(torus, struct point3d, 0);
          
    GArray *torus_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(torus_rotated, array_size);
    
    //Draw one ring and then rotate it with quaternions around another ring.      
    gint ring_points=points_per_ring+1;
    gdouble arc1=360.0/(gdouble)(points_per_ring);
    for(i=0;i<ring_points;i++)
      {
        (*p1).x=cos(arc1*(gdouble)(i)*G_PI/180.0)+2.0;
        (*p1).y=sin(arc1*(gdouble)(i)*G_PI/180.0);
        (*p1).z=0.0;
        p1++;
      }

    //Rotate the one ring.
    p1=&g_array_index(torus, struct point3d, 0);
    p2=&g_array_index(torus, struct point3d, points_per_ring+1);
    gdouble arc2=360.0/(gdouble)(rings);
    gdouble qrs[9];
    for(i=1;i<rings;i++)
      {
        quaternion_rotation(0.0, (gdouble)i*arc2*G_PI/180.0, 0.0, qrs);
        for(j=0;j<ring_points;j++)
          {
            (*p2).x=((*(p1+j)).x*qrs[0])+((*(p1+j)).y*qrs[1])+((*(p1+j)).z*qrs[2]);
            (*p2).y=((*(p1+j)).x*qrs[3])+((*(p1+j)).y*qrs[4])+((*(p1+j)).z*qrs[5]);
            (*p2).z=((*(p1+j)).x*qrs[6])+((*(p1+j)).y*qrs[7])+((*(p1+j)).z*qrs[8]);
            p2++;
          }
      }

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=TORUS;
    p->s=torus;
    p->sr=torus_rotated;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    g_ptr_array_add(shape_cache, p);
    
  }
static void initialize_twist()
  {
    gint i=0;
    gint j=0;
    gdouble qrs[9];
    gdouble arc=360.0/16.0;
    GArray *twist=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 289);
    g_array_set_size(twist, 289);
    GArray *twist_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point), 289);
    g_array_set_size(twist_rotated, 289);

    struct point3d *p1=&g_array_index(twist, struct point3d, 0);
    for(i=0;i<17;i++)
      {
        quaternion_rotation(0.0, (gdouble)i*arc*G_PI/180.0, 0.0, qrs);
        for(j=0;j<17;j++)
          {
            (*p1).x=-2.0+(gdouble)j*0.25;
            (*p1).y=-4.0+(gdouble)i*0.5;
            (*p1).z=0.0;
            (*p1).x=((*p1).x*qrs[0])+((*p1).y*qrs[1])+((*p1).z*qrs[2]);
            (*p1).y=((*p1).x*qrs[3])+((*p1).y*qrs[4])+((*p1).z*qrs[5]);
            (*p1).z=((*p1).x*qrs[6])+((*p1).y*qrs[7])+((*p1).z*qrs[8]);
            p1++;
          }
      }

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=TWIST;
    p->s=twist;
    p->sr=twist_rotated;
    p->rings=0.0;
    p->points_per_ring=0.0;
    g_ptr_array_add(shape_cache, p);
  }
static void initialize_triangle()
  {
    GArray *triangle=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 3);
    g_array_set_size(triangle, 3);
    //Only rotate triangle values. The 4th and 5th values are the light and dark circles.
    GArray *triangle_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 3);
    g_array_set_size(triangle_rotated, 3);

    //Triangle.
    struct point3d *p1=&g_array_index(triangle, struct point3d, 0);
    (*p1).x=-2.0;
    (*p1).y=1.0;
    (*p1).z=0.0;
    p1++;
    (*p1).x=0.0;
    (*p1).y=-1.0;
    (*p1).z=0.0;
    p1++;
    (*p1).x=2.0;
    (*p1).y=1.0;
    (*p1).z=0.0;

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=TRIANGLE;
    p->s=triangle;
    p->sr=triangle_rotated;
    p->rings=0.0;
    p->points_per_ring=0.0;
    g_ptr_array_add(shape_cache, p);
  }
static void initialize_pyramid()
  {
    GArray *pyramid=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 5);
    g_array_set_size(pyramid, 5);
    GArray *pyramid_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 5);
    g_array_set_size(pyramid_rotated, 5);

    struct point3d *p1=&g_array_index(pyramid, struct point3d, 0);
    //Top point.
    (*p1).x=0.0;
    (*p1).y=0.0;
    (*p1).z=2.0;
    p1++;
    //Bottom square.
    (*p1).x=-2.0;
    (*p1).y=-2.0;
    (*p1).z=-2.0;
    p1++;
    (*p1).x=2.0;
    (*p1).y=-2.0;
    (*p1).z=-2.0;
    p1++;
    (*p1).x=2.0;
    (*p1).y=2.0;
    (*p1).z=-2.0;
    p1++;
    (*p1).x=-2.0;
    (*p1).y=2.0;
    (*p1).z=-2.0;

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=PYRAMID;
    p->s=pyramid;
    p->sr=pyramid_rotated;
    p->rings=0.0;
    p->points_per_ring=0.0;
    g_ptr_array_add(shape_cache, p);
  }
static void initialize_cone(gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    if(rings<5||rings>50)
      {
        rings=5;
        g_print("Range for rings is 5<=x<=50. Set default rings=5.\n");
      }
    if(points_per_ring<10||points_per_ring>50)
      {
        points_per_ring=16;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=16.\n");
      }
   
    gint array_size=rings*(points_per_ring+1)+1;
    struct point3d *p1=NULL;

    GArray *cone=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(cone, array_size);
    p1=&g_array_index(cone, struct point3d, 0);
          
    GArray *cone_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(cone_rotated, array_size);
          
    gint ring_points=points_per_ring+1;
    gdouble height=4.0/(gdouble)rings;
    gdouble radius=2.0/rings;
    gdouble arc1=360.0/(gdouble)(points_per_ring);
    for(i=0;i<rings;i++)
      {
        for(j=0;j<ring_points;j++)
          {
            (*p1).x=(2.0-radius*(gdouble)i)*cos(arc1*(gdouble)j*G_PI/180.0);
            (*p1).y=(2.0-radius*(gdouble)i)*sin(arc1*(gdouble)j*G_PI/180.0);
            (*p1).z=-2.0+height*(gdouble)i;
            p1++;
          }
      }
    //Top.
    (*p1).x=0.0;
    (*p1).y=0.0;
    (*p1).z=2.0;

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=CONE;
    p->s=cone;
    p->sr=cone_rotated;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    g_ptr_array_add(shape_cache, p);    
  }
static void initialize_text_ring(gint points_per_ring)
  {
    //Only 2 rings in the text ring.
    gint i=0;
    gint j=0;

    if(points_per_ring<5||points_per_ring>30)
      {
        points_per_ring=16;
        g_print("Range for points_per_ring is 5<=x<=30. Set default points_per_ring=16.\n");
      }

    gint points=2*(points_per_ring+1);
    GArray *text_ring=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), points);
    g_array_set_size(text_ring, points);    
    GArray *text_ring_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), points);
    g_array_set_size(text_ring_rotated, points);
    
    struct point3d *p1=&g_array_index(text_ring, struct point3d, 0);
    gdouble arc1=360.0/(gdouble)(points_per_ring);
    gdouble y_value=1.0;
    for(i=0;i<2;i++)
      {
        for(j=0;j<points_per_ring+1;j++)
          {
            (*p1).x=3.0*cos(arc1*(gdouble)j*G_PI/180.0);
            (*p1).y=y_value;
            (*p1).z=3.0*sin(arc1*(gdouble)j*G_PI/180.0);
            p1++;
          }
        y_value=-1.0;
      }

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=TEXT_RING;
    p->s=text_ring;
    p->sr=text_ring_rotated;
    p->rings=0;
    p->points_per_ring=points_per_ring;
    g_ptr_array_add(shape_cache, p);
  }
static void clear_shape_cache()
  {
    gint i=0;
    gint length=shape_cache->len;
    struct shape *p=NULL;
    for(i=0;i<length;i++)
      {
        p=g_ptr_array_index(shape_cache, 0);
        g_array_free(p->s, TRUE);
        g_array_free(p->sr, TRUE);
        g_free(p);
        g_ptr_array_remove_index_fast(shape_cache, 0);
      }
  }
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data)
  {
    //Paint background of drawing area.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr);
    //Paint the background under the grid.
    cairo_set_source_rgba(cr, 0.0, 0.5, 1.0, 0.5);
    gint width=gtk_paned_get_position(GTK_PANED(data));
    gint height=gtk_widget_get_allocated_height(widget);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_rectangle(cr, width, 0.0, 10, height);
    cairo_fill(cr);
    return FALSE;
  }
static void show_frame_rate(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_function_time(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) function_time=TRUE;
    else function_time=FALSE;
  }
static void rotate_drawing(GtkComboBox *combo, gpointer *rotations)
  {
    rotate=gtk_combo_box_get_active(combo);
    //Reset rotations for animation.
    ((gdouble*)(rotations[0]))[0]=0.0;
    ((gdouble*)(rotations[0]))[1]=0.0;
    ((gdouble*)(rotations[0]))[2]=0.0;
    segment=0.0;
    compress=1.0;
    
    if(rotate==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(rotations[1]), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(rotations[1]));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(rotations[1]), (GtkTickCallback)animate_drawing, rotations, NULL);
          }
      }
    
  }
static void change_drawing(GtkComboBox *combo, gpointer data)
  {
    clear_shape_cache();
    initialize_id=atoi(gtk_combo_box_get_active_id(combo));

    //The basic 0-12 shapes.
    if(initialize_id==SPRING) initialize_spring();
    else if(initialize_id==CYLINDER) initialize_cylinder(16);
    else if(initialize_id==CUBE) initialize_cube();
    else if(initialize_id==CHECKERBOARD) initialize_checkerboard();
    else if(initialize_id==BALL) initialize_ball(30, 30);
    else if(initialize_id==FUNNEL) initialize_funnel(10, 12);
    else if(initialize_id==GEM) initialize_gem();
    else if(initialize_id==TORUS) initialize_torus(12, 16);
    else if(initialize_id==TWIST) initialize_twist();
    else if(initialize_id==PYRAMID) initialize_pyramid();
    else if(initialize_id==CONE) initialize_cone(8, 16);
    else if(initialize_id==TRIANGLE) initialize_triangle();
    else if(initialize_id==TEXT_RING) initialize_text_ring(16);
    //A gap in initialize_id and the combo drawings.
    else if(initialize_id==14)
      {
        initialize_ball(20, 20);
        initialize_gem();
        initialize_twist();
        initialize_cone(8, 16);
      }
    else if(initialize_id==15)
      {
        initialize_ball(20, 20);
      }
    else if(initialize_id==16)
      {
        initialize_ball(30, 30);
        initialize_ball(16, 16);
        initialize_ball(24, 24);
      }
    else if(initialize_id==17)
      {
        initialize_ball(20, 20);
        initialize_cone(8, 16);
      }
    else g_print("Error: Couldn't initialize shapes in drawing! %i\n", initialize_id);   

    drawing_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_rotation_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations)
  {
    if(value>360.0) ((gdouble*)(rotations[0]))[0]=360.0;
    else if(value<0.0) ((gdouble*)(rotations[0]))[0]=0.0;
    else ((gdouble*)(rotations[0]))[0]=value; 

    gtk_widget_queue_draw(GTK_WIDGET(rotations[1]));
  }
static void set_rotation_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations)
  {
    if(value>360.0) ((gdouble*)(rotations[0]))[1]=360.0;
    else if(value<0.0) ((gdouble*)(rotations[0]))[1]=0.0;
    else ((gdouble*)(rotations[0]))[1]=value; 

    gtk_widget_queue_draw(GTK_WIDGET(rotations[1]));
  }
static void set_rotation_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations)
  {
    if(value>360.0) ((gdouble*)(rotations[0]))[2]=360.0;
    else if(value<0.0) ((gdouble*)(rotations[0]))[2]=0.0;
    else ((gdouble*)(rotations[0]))[2]=value; 

    gtk_widget_queue_draw(GTK_WIDGET(rotations[1]));
  }
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer *rotations)
  {
    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    //Apply angles of rotation for animation.
    ((gdouble*)(rotations[0]))[0]+=0.5;
    ((gdouble*)(rotations[0]))[1]+=0.5;
    ((gdouble*)(rotations[0]))[2]+=0.5;

    //Move the colored segment.
    segment+=0.2;
    if((gint)segment==79) segment=0.0;
 
    //Compress the spring.
    static gboolean sw=TRUE;
    if(compress<0.3) sw=FALSE;
    else if(compress>1.6) sw=TRUE;
    if(sw) compress-=0.01;
    else compress+=0.01;

    //Scale the cube.
    static gboolean sw2=TRUE;
    if(scale_cube<0.2) sw2=FALSE;
    else if(scale_cube>1.0) sw2=TRUE;
    if(sw2) scale_cube-=0.001;
    else scale_cube+=0.001;

    gtk_widget_queue_draw(drawing);
    return G_SOURCE_CONTINUE;
  }
//The top drawing function.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer *rotations)
  {
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;
    gdouble yaw=((gdouble*)(rotations[0]))[0]*G_PI/180.0;
    gdouble roll=((gdouble*)(rotations[0]))[1]*G_PI/180.0;
    gdouble pitch=((gdouble*)(rotations[0]))[2]*G_PI/180.0;

    //Paint background.
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_paint(cr);

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 1.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Draw in the center
    cairo_translate(cr, width/2.0, height/2.0);
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);      
 
    GTimer *timer=g_timer_new();
    if(drawing_id==0)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_line_width(cr, 5.0); 
        draw_spring(cr, w1, h1);
      }
    else if(drawing_id==1)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_line_width(cr, 3.0); 
        draw_cylinder(cr, w1, h1, 16);
      } 
    else if(drawing_id==2)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_line_width(cr, 1.0); 
        cairo_scale(cr, scale_cube, scale_cube);
        draw_cube(cr, w1, h1, qrs);
      } 
    else if(drawing_id==3)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_line_width(cr, 1.0); 
        draw_transparent_cube(cr, w1, h1, rotations);
      } 
    else if(drawing_id==4)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_line_width(cr, 1.0); 
        draw_checkerboard(cr, w1, h1);
      }
    else if(drawing_id==5)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs); 
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        draw_ball(cr, w1, h1, 30, 30);
      }
    else if(drawing_id==6)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_line_width(cr, 2.0); 
        draw_wire_ball(cr, w1, h1);
      }
    else if(drawing_id==7)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_line_width(cr, 2.0); 
        draw_wire_ball2(cr, w1, h1);
      }
    else if(drawing_id==8)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        //See if it draws faster with anti aliasing turned off.
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        cairo_set_line_width(cr, 2.0); 
        draw_wire_ball2(cr, w1, h1);
      }
    else if(drawing_id==9)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        cairo_set_line_width(cr, 4.0); 
        draw_wire_funnel(cr, w1, h1, 10, 12);
      }
    else if(drawing_id==10)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        draw_funnel(cr, w1, h1, 10, 12);
      }
    else if(drawing_id==11)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_line_width(cr, 1.0); 
        draw_gem(cr, w1, h1);
      }
    else if(drawing_id==12)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        cairo_set_line_width(cr, 2.0); 
        draw_wire_torus(cr, w1, h1, 12, 16);
      }
    else if(drawing_id==13)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.3);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        cairo_set_line_width(cr, 2.0); 
        draw_torus(cr, w1, h1, 12, 16);
      }
    else if(drawing_id==14)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);

        //The 4-torus drawing.
        cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 0.3);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        cairo_set_line_width(cr, 2.0);
        cairo_scale(cr, 0.5, 0.5);
        cairo_translate(cr, -4.0*w1, -4.0*h1); 
        draw_torus(cr, w1, h1, 12, 16);

        cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.3);
        cairo_translate(cr, 8.0*w1, 0.0); 
        draw_torus(cr, w1, h1, 12, 16);

        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.3);
        cairo_translate(cr, -8.0*w1, 8.0*h1); 
        draw_torus(cr, w1, h1, 12, 16);

        cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 0.3);
        cairo_translate(cr, 8.0*w1, 0.0); 
        draw_torus(cr, w1, h1, 12, 16);
      }
    else if(drawing_id==15)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        cairo_set_line_width(cr, 2.0); 
        draw_wire_twist(cr, w1, h1);
      }
    else if(drawing_id==16)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs); 
        draw_twist(cr, w1, h1);
      }
    else if(drawing_id==17)
      { 
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        draw_triangle(cr, w1, h1);
      }
    else if(drawing_id==18)
      { 
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs); 
        draw_pyramid(cr, w1, h1);
      }
    else if(drawing_id==19)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        cairo_set_line_width(cr, 1.0); 
        draw_cone(cr, w1, h1, 8, 16);
      }
    else if(drawing_id==20)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_line_width(cr, 1.0); 
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        draw_cone_shade(cr, w1, h1, 8, 16);
      }
    else if(drawing_id==21)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);
        cairo_set_line_width(cr, 4.0); 
        draw_text_ring(cr, w1, h1, 16, rotations);
      }
    else if(drawing_id==22)
      {
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shapes(qrs);

        cairo_scale(cr, 0.5, 0.5);
        cairo_translate(cr, -4.0*w1, -4.0*h1); 
        draw_ball(cr, w1, h1, 20, 20);

        cairo_set_line_width(cr, 1.0); 
        cairo_translate(cr, 8.0*w1, 0.0); 
        draw_gem(cr, w1, h1);

        cairo_translate(cr, -8.0*w1, 8.0*h1); 
        draw_twist(cr, w1, h1);

        cairo_translate(cr, 8.0*w1, 0.0);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); 
        draw_cone(cr, w1, h1, 8, 16);
      }
    else if(drawing_id==23)
      {
        //Rotate each drawing individually.
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        rotate_cache_shape_index(qrs, 0);
        cairo_scale(cr, 0.5, 0.5);
        cairo_translate(cr, -4.0*w1, -4.0*h1);  
        draw_ball(cr, w1, h1, 20, 20);

        quaternion_rotation(yaw+G_PI/4.0, roll, pitch, qrs);
        rotate_cache_shape_index(qrs, 1);
        cairo_set_line_width(cr, 1.0); 
        cairo_translate(cr, 8.0*w1, 0.0); 
        draw_gem(cr, w1, h1);

        quaternion_rotation(yaw, roll+G_PI/4.0, pitch, qrs);
        rotate_cache_shape_index(qrs, 2);
        cairo_translate(cr, -8.0*w1, 8.0*h1); 
        draw_twist(cr, w1, h1);

        quaternion_rotation(yaw, roll, pitch*G_PI/4.0, qrs);
        rotate_cache_shape_index(qrs, 3);
        cairo_translate(cr, 8.0*w1, 0.0);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); 
        draw_cone(cr, w1, h1, 8, 16);
      }
    else if(drawing_id==24)
      {
        //Revolve and rotate a couple of balls.
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        //Set revolve initial location to x=1,y=0,z=0.
        revolve[0]=1.0;revolve[1]=0.0;revolve[2]=0.0;
        revolve_standard_point(qrs);
        rotate_cache_shape_index(qrs, 0);
        cairo_scale(cr, 0.5, 0.5);
        cairo_translate(cr, -6.0*w1+revolve_r[0]*12.0*w1, revolve_r[1]*12.0*h1);  
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); 
        draw_ball(cr, w1, h1, 20, 20);

        cairo_translate(cr, 6.0*w1-revolve_r[0]*12.0*w1, revolve_r[1]*12.0*h1);  
        draw_ball(cr, w1, h1, 20, 20);
      }
    else if(drawing_id==25)
      {
        //Planets. Draw several different sizes of the same shape.
        cairo_scale(cr, 0.5, 0.5);
        gdouble qrs1[9];
        gdouble qrs2[9];
        gdouble qrs3[9];
        //Set revolve initial location to x=1,y=0,z=0.
        revolve[0]=1.0;revolve[1]=0.0;revolve[2]=0.0;

        cairo_save(cr);
        cairo_scale(cr, 0.25, 0.25);
        quaternion_rotation(yaw+G_PI/2.0, roll-G_PI, pitch, qrs2);
        revolve_standard_point(qrs2); 
        cairo_translate(cr, 32.0*revolve_r[0]*w1, 32.0*revolve_r[1]*h1);       
        rotate_cache_shape_index(qrs2, 1);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);  
        draw_ball_mesh(cr, w1, h1, 16, 16);
        cairo_restore(cr);

        cairo_save(cr);
        cairo_scale(cr, 0.5, 0.5);
        quaternion_rotation(yaw, roll, pitch-G_PI, qrs3);
        revolve_standard_point(qrs3);
        cairo_translate(cr, 16.0*revolve_r[0]*w1, 16.0*revolve_r[1]*h1);  
        rotate_cache_shape_index(qrs3, 2);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);  
        draw_ball_mesh(cr, w1, h1, 24, 24);
        cairo_restore(cr);

        //Center ball.
        quaternion_rotation(yaw, roll, pitch, qrs1);
        rotate_cache_shape_index(qrs1, 0);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);  
        draw_ball_mesh(cr, w1, h1, 30, 30);

        //Possibly draw twice if revolving planet is in the front.
        revolve_standard_point(qrs2); 
        if(revolve_r[2]>=0.0)
          {    
            cairo_save(cr);
            cairo_scale(cr, 0.25, 0.25);
            cairo_translate(cr, 32.0*revolve_r[0]*w1, 32.0*revolve_r[1]*h1); 
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);  
            draw_ball_mesh(cr, w1, h1, 16, 16);
            cairo_restore(cr);
          }

        revolve_standard_point(qrs3); 
        if(revolve_r[2]>=0.0)
          {    
            cairo_save(cr);
            cairo_scale(cr, 0.5, 0.5);
            cairo_translate(cr, 16.0*revolve_r[0]*w1, 16.0*revolve_r[1]*h1);       
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);  
            draw_ball_mesh(cr, w1, h1, 24, 24);
            cairo_restore(cr);
          }
             
      }
    else
      {
        //Ball and cone sychronized.
        cairo_scale(cr, 0.5, 0.5);
        gdouble qrs1[9];
        //Set revolve initial location to x=0,y=0,z=1.
        revolve[0]=0.0;revolve[1]=0.0;revolve[2]=1.0;
        quaternion_rotation(yaw, roll, pitch, qrs1);
        revolve_standard_point(qrs1);

        //Draw cone twice for front and back of ball.
        rotate_cache_shape_index(qrs1, 1); 
        if(revolve_r[2]<=0.0)
          {       
            cairo_save(cr);
            cairo_translate(cr, revolve_r[0]*w1*4.0, revolve_r[1]*h1*4.0);
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); 
            draw_cone(cr, w1, h1, 8, 16);
            cairo_restore(cr);
          }

        rotate_cache_shape_index(qrs1, 0); 
        draw_ball(cr, w1, h1, 20, 20);

        //Draw cone second time.
        if(revolve_r[2]>=0.0)
          {
            cairo_translate(cr, revolve_r[0]*w1*4.0, revolve_r[1]*h1*4.0);
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); 
            draw_cone(cr, w1, h1, 8, 16);
          }
      }

    //print_shapes_in_cache();
    if(function_time) g_print("Draw time %f\n", g_timer_elapsed(timer, NULL));
    g_timer_destroy(timer);
     
    return FALSE;
  }
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    GTimer *timer1=g_timer_new();
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);

    if(function_time) g_print("Quaternion time %f\n", g_timer_elapsed(timer1, NULL));
    g_timer_destroy(timer1);
  }
static void matrix_multiply_rotations(gdouble qrs1[9], gdouble qrs2[9], gdouble qrs3[9])
  {
    //Multiply two 3x3 matrices.
    qrs3[0]=qrs1[0]*qrs2[0]+qrs1[3]*qrs2[1]+qrs1[6]*qrs2[2];
    qrs3[1]=qrs1[1]*qrs2[0]+qrs1[4]*qrs2[1]+qrs1[7]*qrs2[2];
    qrs3[2]=qrs1[2]*qrs2[0]+qrs1[5]*qrs2[1]+qrs1[8]*qrs2[2];

    qrs3[3]=qrs1[0]*qrs2[3]+qrs1[3]*qrs2[4]+qrs1[6]*qrs2[5];
    qrs3[4]=qrs1[1]*qrs2[3]+qrs1[4]*qrs2[4]+qrs1[7]*qrs2[5];
    qrs3[5]=qrs1[2]*qrs2[3]+qrs1[5]*qrs2[4]+qrs1[8]*qrs2[5];

    qrs3[6]=qrs1[0]*qrs2[6]+qrs1[3]*qrs2[7]+qrs1[6]*qrs2[8];
    qrs3[7]=qrs1[1]*qrs2[6]+qrs1[4]*qrs2[7]+qrs1[7]*qrs2[8];
    qrs3[8]=qrs1[2]*qrs2[6]+qrs1[5]*qrs2[7]+qrs1[8]*qrs2[8];
  }
static void revolve_standard_point(gdouble qrs[9])
  {
    revolve_r[0]=(revolve[0]*qrs[0])+(revolve[1]*qrs[1])+(revolve[2]*qrs[2]);
    revolve_r[1]=(revolve[0]*qrs[3])+(revolve[1]*qrs[4])+(revolve[2]*qrs[5]);
    revolve_r[2]=(revolve[0]*qrs[6])+(revolve[1]*qrs[7])+(revolve[2]*qrs[8]);
  }
static void rotate_cache_shapes(gdouble qrs[9])
  {
    GTimer *timer2=g_timer_new();
    gint i=0;

    const struct shape *s1=NULL;
    for(i=0;i<(shape_cache->len);i++)
      {
        s1=g_ptr_array_index(shape_cache, i); 
        rotate_shape(s1, qrs);  
      } 

    if(function_time) g_print("Rotation time %f\n", g_timer_elapsed(timer2, NULL));
    g_timer_destroy(timer2);

  }
static void rotate_cache_shape_index(gdouble qrs[9], gint index)
  {
    GTimer *timer2=g_timer_new();
    const struct shape *s1=NULL;

    if(index>=0&&index<(shape_cache->len))
      {
        s1=g_ptr_array_index(shape_cache, index); 
        rotate_shape(s1, qrs);  
      } 
    else g_print("Warning: The shape at the index wasn't found!\n");

    if(function_time) g_print("Rotation time %f\n", g_timer_elapsed(timer2, NULL));
    g_timer_destroy(timer2);

  }
static void rotate_shape(const struct shape *s1, gdouble qrs[9])
  {
    gint i=0;

    if((s1->cache_id)==SPRING)
      {
        struct point3d *p1=&g_array_index(s1->s, struct point3d, 0);
        struct point *p2=&g_array_index(s1->sr, struct point, 0);
        for(i=0;i<(s1->s->len);i++)
          {
            //Test changing the perspective of spiral at end of expression. *(0.4...
            (*p2).x=(((*p1).x*qrs[0])+((*p1).y*qrs[1])+((*p1).z*qrs[2]*compress))*(0.4+(*p1).z/3.0);
            (*p2).y=(((*p1).x*qrs[3])+((*p1).y*qrs[4])+((*p1).z*qrs[5]*compress))*(0.4+(*p1).z/3.0);
            p1++;p2++;
          }
      }
    else if(s1->cache_id==CYLINDER)
      {
        rotate_shape_xy(s1, qrs);
      }
    else if(s1->cache_id==CUBE)
      {
        rotate_shape_xyz(s1, qrs);
      }
    else if(s1->cache_id==CHECKERBOARD)
      {
        //Need z for perspective.
        gdouble z=0;
        struct point *p1=&g_array_index(s1->s, struct point, 0);
        struct point *p2=&g_array_index(s1->sr, struct point, 0);
        for(i=0;i<(s1->s->len);i++)
          {
            z=((*p1).x*qrs[6])+((*p1).y*qrs[7]);
            //Test changing the perspective of checkerboard at end of expression. *(1.0...
            (*p2).x=(((*p1).x*qrs[0])+((*p1).y*qrs[1]))*(1.0+(0.15*z));
            (*p2).y=(((*p1).x*qrs[3])+((*p1).y*qrs[4]))*(1.0+(0.15*z));
            p1++;p2++;
          }
      }
    else if(s1->cache_id==BALL)
      {
        rotate_shape_xyz(s1, qrs);
      }
    else if(s1->cache_id==FUNNEL)
      {
        rotate_shape_xyz(s1, qrs);
      }
    else if(s1->cache_id==GEM)
      {
        rotate_shape_xyz(s1, qrs);
      }
    else if(s1->cache_id==TORUS)
      {
        rotate_shape_xyz(s1, qrs);
      }
    else if(s1->cache_id==TWIST)
      {
        rotate_shape_xy(s1, qrs);
      }
    else if(s1->cache_id==TRIANGLE)
      {
        rotate_shape_xyz(s1, qrs);
      }
    else if(s1->cache_id==PYRAMID)
      {
        rotate_shape_xyz(s1, qrs);
      }
    else if(s1->cache_id==CONE)
      {
        rotate_shape_xyz(s1, qrs);
      }
    else if(s1->cache_id==TEXT_RING)
      {
        rotate_shape_xyz(s1, qrs);
      }
    else
      {
        g_print("Warning: Couldn't find shape to rotate!\n");
      }
  }
static void rotate_shape_xy(const struct shape *s1, gdouble qrs[9])
  {
    gint i=0;
    struct point3d *p1=&g_array_index(s1->s, struct point3d, 0);
    struct point *p2=&g_array_index(s1->sr, struct point, 0);
    for(i=0;i<(s1->s->len);i++)
      {
        (*p2).x=(((*p1).x*qrs[0])+((*p1).y*qrs[1])+((*p1).z*qrs[2]));
        (*p2).y=(((*p1).x*qrs[3])+((*p1).y*qrs[4])+((*p1).z*qrs[5]));
        p1++;p2++;
      }
  }
static void rotate_shape_xyz(const struct shape *s1, gdouble qrs[9])
  {
    gint i=0;
    struct point3d *p1=&g_array_index(s1->s, struct point3d, 0);
    struct point3d *p2=&g_array_index(s1->sr, struct point3d, 0);
    for(i=0;i<(s1->s->len);i++)
      {
        (*p2).x=((*p1).x*qrs[0])+((*p1).y*qrs[1])+((*p1).z*qrs[2]);
        (*p2).y=((*p1).x*qrs[3])+((*p1).y*qrs[4])+((*p1).z*qrs[5]);
        (*p2).z=((*p1).x*qrs[6])+((*p1).y*qrs[7])+((*p1).z*qrs[8]);
        p1++,p2++;
      }
  }
static const struct shape* get_drawing_from_shape_cache(gint cache_id, gint rings, gint points_per_ring)
  {
    gint i=0;
    gboolean shape_found=FALSE;

    //Get the drawing from the shape cache.
    struct shape *s1=NULL;

    if(cache_id>=0&&cache_id<=12)
      {
        for(i=0;i<shape_cache->len;i++)
          {
            s1=g_ptr_array_index(shape_cache, i); 
            if((s1->cache_id==cache_id)&&(s1->rings==rings)&&(s1->points_per_ring==points_per_ring))
              {
                shape_found=TRUE;
                break;
              }        
          }

        /*
            Couldn't find the drawing in the cache so initialize it and try again. Could just
        auto initialize all shapes.
        */
        if(shape_found==FALSE)
          {
            g_print("Warning: Auto initialize shape! %i %i %i\n", cache_id, rings, points_per_ring);
            initialize_shape(cache_id, rings, points_per_ring);
            for(i=0;i<shape_cache->len;i++)
              {
                s1=g_ptr_array_index(shape_cache, i);
                if(s1->cache_id==cache_id) break;
              }
          }
      }
    else g_print("Warning: Invalid shape cache_id requested!\n");

    return s1;
  }
static void print_shapes_in_cache()
  {
    gint i=0;
    
    if((shape_cache->len)>0)
      {
        struct shape *s1=g_ptr_array_index(shape_cache, 0);
        for(i=0;i<(shape_cache->len);i++)
          {
            s1=g_ptr_array_index(shape_cache, i);
            g_print("ShapeID %i, %s, Rings %i Points Per Ring %i\n", s1->cache_id, shape_names[s1->cache_id], s1->rings, s1->points_per_ring);      
          }
      }
    else
      {
        g_print("Empty Shape Cache\n");
      }
  }
static void draw_spring(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;
    struct point d1;
    struct point d2;
    struct controls c1; 
    gint length=0;

    const struct shape *s1=get_drawing_from_shape_cache(SPRING, 0, 0);

    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    GArray *control1=control_points_from_coords2(s1->sr);
    length=(s1->sr->len)-1;
    for(i=0;i<length;i++)
      {
        if(i==(gint)segment) cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0); 
        else cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0); 
        d1=g_array_index(s1->sr, struct point, i);
        d2=g_array_index(s1->sr, struct point, i+1);
        c1=g_array_index(control1, struct controls, i);
        cairo_move_to(cr, w1*d1.x, h1*d1.y);
        cairo_curve_to(cr, w1*c1.x1, h1*c1.y1, w1*c1.x2, h1*c1.y2, w1*d2.x, h1*d2.y);
        cairo_stroke(cr);
      }
    g_array_free(control1, TRUE);
  }
static void draw_cylinder(cairo_t *cr, gdouble w1, gdouble h1, gint points_per_ring)
  {
    gint i=0; 

    const struct shape *s1=get_drawing_from_shape_cache(CYLINDER, 0, points_per_ring);
    
    //Need the 2 rings of the cylinder to get the bezier points for them.
    gint ring_length=(s1->sr->len)/2;
    GArray *ring1=g_array_sized_new(FALSE, TRUE, sizeof(struct point), ring_length);
    g_array_set_size(ring1, ring_length);
    GArray *ring2=g_array_sized_new(FALSE, TRUE, sizeof(struct point), ring_length);
    g_array_set_size(ring2, ring_length);
    struct point *p1=&g_array_index(ring1, struct point, 0);
    struct point *p2=&g_array_index(s1->sr, struct point, 0);
    //Copy the rotated points.
    for(i=0;i<ring_length;i++)
      {
        (*p1).x=(*p2).x;
        (*p1).y=(*p2).y;
        p1++;p2++;
      }
    p1=&g_array_index(ring2, struct point, 0);
    for(i=0;i<ring_length;i++)
      {
        (*p1).x=(*p2).x;
        (*p1).y=(*p2).y;
        p1++;p2++;
      }
    
    //Draw the cylinder.
    GArray *control1=control_points_from_coords2(ring1);
    GArray *control2=control_points_from_coords2(ring2);
    struct point *d1=&g_array_index(ring1, struct point, 0);
    struct point *d2=&g_array_index(ring2, struct point, 0);
    struct point *d3=&g_array_index(ring1, struct point, 1);
    struct point *d4=&g_array_index(ring2, struct point, 1);
    struct controls *c1=&g_array_index(control1, struct controls, 0);
    struct controls *c2=&g_array_index(control2, struct controls, 0); 
    for(i=0;i<(s1->points_per_ring);i++)
      {
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        cairo_move_to(cr, w1*(d1->x), h1*(d1->y));
        cairo_curve_to(cr, w1*(c1->x1), h1*(c1->y1), w1*(c1->x2), h1*(c1->y2), w1*(d3->x), h1*(d3->y));
        cairo_line_to(cr, w1*(d4->x), h1*(d4->y));
        cairo_curve_to(cr, w1*(c2->x2), h1*(c2->y2), w1*(c2->x1), h1*(c2->y1), w1*(d2->x), h1*(d2->y));
        cairo_close_path(cr);
        cairo_stroke_preserve(cr);
        cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 0.5);
        cairo_fill(cr); 
        d1++;d2++;d3++;d4++;c1++;c2++;      
      }

    g_array_free(ring1, TRUE);
    g_array_free(ring2, TRUE);
    g_array_free(control1, TRUE);
    g_array_free(control2, TRUE);
  }
static void draw_cube(cairo_t *cr, gdouble w1, gdouble h1, gdouble qrs[9])
  {
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    cairo_set_font_size(cr, 50.0);
    const struct shape *s1=get_drawing_from_shape_cache(CUBE, 0, 0);

    struct point3d d1=g_array_index(s1->sr, struct point3d, 0);
    struct point3d d2=g_array_index(s1->sr, struct point3d, 1);
    struct point3d d3=g_array_index(s1->sr, struct point3d, 2);
    struct point3d d4=g_array_index(s1->sr, struct point3d, 3);
    struct point3d d5=g_array_index(s1->sr, struct point3d, 4);
    struct point3d d6=g_array_index(s1->sr, struct point3d, 5);
    struct point3d d7=g_array_index(s1->sr, struct point3d, 6);
    struct point3d d8=g_array_index(s1->sr, struct point3d, 7);
    //Just draw the 3 top planes of the cube.
    gboolean clip_cube=TRUE;

    if(clip_cube&&(d1.z+d2.z+d3.z+d4.z>0.0))
      {
        cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
        cairo_move_to(cr, w1*d1.x, h1*d1.y);
        cairo_line_to(cr, w1*d2.x, h1*d2.y);
        cairo_line_to(cr, w1*d3.x, h1*d3.y);
        cairo_line_to(cr, w1*d4.x, h1*d4.y);
        cairo_close_path(cr);
        cairo_fill(cr);

        //Add some text to the green square.
        cairo_save(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_matrix_init(&matrix1, qrs[0], qrs[3], qrs[1], qrs[4], w1*(d1.x+d2.x+d3.x+d4.x)/4.0, h1*(d1.y+d2.y+d3.y+d4.y)/4.0);
        cairo_transform(cr, &matrix1);
        cairo_text_extents(cr, "cairo", &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, "cairo");
        cairo_restore(cr);
      }
   
    if(clip_cube&&(d5.z+d6.z+d7.z+d8.z>0.0))
      {
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        cairo_move_to(cr, w1*d5.x, h1*d5.y);
        cairo_line_to(cr, w1*d6.x, h1*d6.y);
        cairo_line_to(cr, w1*d7.x, h1*d7.y);
        cairo_line_to(cr, w1*d8.x, h1*d8.y);
        cairo_close_path(cr);
        cairo_fill(cr);
      }

    if(clip_cube&&(d1.z+d5.z+d6.z+d2.z>0.0))
      {
        cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
        cairo_move_to(cr, w1*d1.x, h1*d1.y);
        cairo_line_to(cr, w1*d5.x, h1*d5.y);
        cairo_line_to(cr, w1*d6.x, h1*d6.y);
        cairo_line_to(cr, w1*d2.x, h1*d2.y);
        cairo_close_path(cr);
        cairo_fill(cr);
      }

    if(clip_cube&&(d2.z+d6.z+d7.z+d3.z>0.0))
      {
        cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
        cairo_move_to(cr, w1*d2.x, h1*d2.y);
        cairo_line_to(cr, w1*d6.x, h1*d6.y);
        cairo_line_to(cr, w1*d7.x, h1*d7.y);
        cairo_line_to(cr, w1*d3.x, h1*d3.y);
        cairo_close_path(cr);
        cairo_fill(cr);
      }

    if(clip_cube&&(d3.z+d7.z+d8.z+d4.z>0.0))
      {
        cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
        cairo_move_to(cr, w1*d3.x, h1*d3.y);
        cairo_line_to(cr, w1*d7.x, h1*d7.y);
        cairo_line_to(cr, w1*d8.x, h1*d8.y);
        cairo_line_to(cr, w1*d4.x, h1*d4.y);
        cairo_close_path(cr);
        cairo_fill(cr);
      }

    if(clip_cube&&(d4.z+d8.z+d5.z+d1.z>0.0))
      {
        cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
        cairo_move_to(cr, w1*d4.x, h1*d4.y);
        cairo_line_to(cr, w1*d8.x, h1*d8.y);
        cairo_line_to(cr, w1*d5.x, h1*d5.y);
        cairo_line_to(cr, w1*d1.x, h1*d1.y);
        cairo_close_path(cr);
        cairo_fill(cr);
      }
     
  }
static void draw_transparent_cube(cairo_t *cr, gdouble w1, gdouble h1, gpointer *rotations)
  {
    /*
        Draw the cube with painters algorithm or sorting the faces of the cube. Draw from back
    to front. This way you can draw with transparency and have the back facing fonts a different
    color than the front facing fonts. If you just want the front facing sides set the 
    draw_all_sides=FALSE and maybe change to solid colors.
    */
    gint i=0;
    gdouble yaw_r=((gdouble*)(rotations[0]))[0]*G_PI/180.0;
    gdouble roll_r=((gdouble*)(rotations[0]))[1]*G_PI/180.0;
    gdouble pitch_r=((gdouble*)(rotations[0]))[2]*G_PI/180.0;
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    cairo_set_font_size(cr, 50.0);
    //Draw all the sides or just the top three of the cube.
    gboolean draw_all_sides=TRUE;
    gdouble qrs1[9];
    gdouble qrs2[9];
    gdouble qrs3[9];

    //Rotations for the text on the cube.
    quaternion_rotation(yaw_r, roll_r, pitch_r, qrs1);

    const struct shape *s1=get_drawing_from_shape_cache(CUBE, 0, 0);

    struct point3d d1=g_array_index(s1->sr, struct point3d, 0);
    struct point3d d2=g_array_index(s1->sr, struct point3d, 1);
    struct point3d d3=g_array_index(s1->sr, struct point3d, 2);
    struct point3d d4=g_array_index(s1->sr, struct point3d, 3);
    struct point3d d5=g_array_index(s1->sr, struct point3d, 4);
    struct point3d d6=g_array_index(s1->sr, struct point3d, 5);
    struct point3d d7=g_array_index(s1->sr, struct point3d, 6);
    struct point3d d8=g_array_index(s1->sr, struct point3d, 7);

    //Sort the planes.
    struct plane_order po[6];
    po[0].id=0;
    po[0].value=d1.z+d2.z+d3.z+d4.z;
    po[1].id=1;
    po[1].value=d5.z+d6.z+d7.z+d8.z;
    po[2].id=2;
    po[2].value=d3.z+d7.z+d8.z+d4.z;
    po[3].id=3;
    po[3].value=d2.z+d6.z+d7.z+d3.z;
    po[4].id=4;
    po[4].value=d1.z+d5.z+d6.z+d2.z;
    po[5].id=5;
    po[5].value=d4.z+d8.z+d5.z+d1.z;
    qsort(po, 6, sizeof(struct plane_order), compare_planes);

    //Draw cube faces in z-order.
    for(i=0;i<6;i++)
      {
        if(po[i].id==0&&(draw_all_sides||po[i].value>0.0))
          {
            cairo_save(cr);        
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.3);
            cairo_move_to(cr, w1*d1.x, h1*d1.y);
            cairo_line_to(cr, w1*d2.x, h1*d2.y);
            cairo_line_to(cr, w1*d3.x, h1*d3.y);
            cairo_line_to(cr, w1*d4.x, h1*d4.y);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);

            quaternion_rotation(yaw_r, roll_r, pitch_r, qrs1);
            cairo_matrix_init(&matrix1, qrs1[0], qrs1[3], qrs1[1], qrs1[4], w1*(d1.x+d2.x+d3.x+d4.x)/4.0, h1*(d1.y+d2.y+d3.y+d4.y)/4.0);
            cairo_transform(cr, &matrix1);

            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_rectangle(cr, -60, -60, 120, 120);
            cairo_stroke(cr);  

            if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
            cairo_text_extents(cr, "4", &extents);
            cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
            cairo_show_text(cr, "4");
            cairo_restore(cr);
          }
  
        else if(po[i].id==1&&(draw_all_sides||po[i].value>0.0))
          {
            cairo_save(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.3);
            cairo_move_to(cr, w1*d5.x, h1*d5.y);
            cairo_line_to(cr, w1*d6.x, h1*d6.y);
            cairo_line_to(cr, w1*d7.x, h1*d7.y);
            cairo_line_to(cr, w1*d8.x, h1*d8.y);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);

            quaternion_rotation(yaw_r, roll_r, pitch_r, qrs1);
            cairo_matrix_init(&matrix1, qrs1[0], qrs1[3], qrs1[1], qrs1[4], w1*(d5.x+d6.x+d7.x+d8.x)/4.0, h1*(d5.y+d6.y+d7.y+d8.y)/4.0);
            cairo_transform(cr, &matrix1);

            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_rectangle(cr, -60, -60, 120, 120);
            cairo_stroke(cr);  

            if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
            cairo_text_extents(cr, "3", &extents);
            cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
            cairo_show_text(cr, "3");
            cairo_restore(cr);
          }

        else if(po[i].id==2&&(draw_all_sides||po[i].value>0.0))
          {
            cairo_save(cr);
            cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 0.3);
            cairo_move_to(cr, w1*d3.x, h1*d3.y);
            cairo_line_to(cr, w1*d7.x, h1*d7.y);
            cairo_line_to(cr, w1*d8.x, h1*d8.y);
            cairo_line_to(cr, w1*d4.x, h1*d4.y);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);

            quaternion_rotation(yaw_r, roll_r, pitch_r, qrs1);
            quaternion_rotation(0.0, -G_PI/2.0, 0.0, qrs2);
            matrix_multiply_rotations(qrs2, qrs1, qrs3);
            cairo_matrix_init(&matrix1, qrs3[0], qrs3[3], qrs3[1], qrs3[4], w1*(d3.x+d7.x+d8.x+d4.x)/4.0, h1*(d3.y+d7.y+d8.y+d4.y)/4.0);
            cairo_transform(cr, &matrix1);

            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_rectangle(cr, -60, -60, 120, 120);
            cairo_stroke(cr);  

            if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
            cairo_text_extents(cr, "1", &extents);
            cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
            cairo_show_text(cr, "1");
            cairo_restore(cr);
          }

        else if(po[i].id==3&&(draw_all_sides||po[i].value>0.0))
          {
            cairo_save(cr);
            cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 0.3);
            cairo_move_to(cr, w1*d2.x, h1*d2.y);
            cairo_line_to(cr, w1*d6.x, h1*d6.y);
            cairo_line_to(cr, w1*d7.x, h1*d7.y);
            cairo_line_to(cr, w1*d3.x, h1*d3.y);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);

            quaternion_rotation(yaw_r, roll_r, pitch_r-G_PI/2.0, qrs1);
            cairo_matrix_init(&matrix1, qrs1[0], qrs1[3], qrs1[1], qrs1[4], w1*(d2.x+d6.x+d7.x+d3.x)/4.0, h1*(d2.y+d6.y+d7.y+d3.y)/4.0);
            cairo_transform(cr, &matrix1);

            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_rectangle(cr, -60, -60, 120, 120);
            cairo_stroke(cr);  

            if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
            cairo_text_extents(cr, "5", &extents);
            cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
            cairo_show_text(cr, "5");
            cairo_restore(cr);
          }

        else if(po[i].id==4&&(draw_all_sides||po[i].value>0.0))
          {
            cairo_save(cr);
            cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 0.3);
            cairo_move_to(cr, w1*d1.x, h1*d1.y);
            cairo_line_to(cr, w1*d5.x, h1*d5.y);
            cairo_line_to(cr, w1*d6.x, h1*d6.y);
            cairo_line_to(cr, w1*d2.x, h1*d2.y);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);

            quaternion_rotation(yaw_r, roll_r, pitch_r, qrs1);
            quaternion_rotation(0.0, G_PI/2.0, 0.0, qrs2);
            matrix_multiply_rotations(qrs2, qrs1, qrs3);
            cairo_matrix_init(&matrix1, qrs3[0], qrs3[3], qrs3[1], qrs3[4], w1*(d1.x+d5.x+d6.x+d2.x)/4.0, h1*(d1.y+d5.y+d6.y+d2.y)/4.0);
            cairo_transform(cr, &matrix1);

            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_rectangle(cr, -60, -60, 120, 120);
            cairo_stroke(cr);  

            if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
            cairo_text_extents(cr, "6", &extents);
            cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
            cairo_show_text(cr, "6");
            cairo_restore(cr);
          }

        else if(po[i].id==5&&(draw_all_sides||po[i].value>0.0))
          {
            cairo_save(cr);
            cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 0.3);
            cairo_move_to(cr, w1*d4.x, h1*d4.y);
            cairo_line_to(cr, w1*d8.x, h1*d8.y);
            cairo_line_to(cr, w1*d5.x, h1*d5.y);
            cairo_line_to(cr, w1*d1.x, h1*d1.y);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);

            quaternion_rotation(yaw_r, roll_r, pitch_r-G_PI/2.0, qrs1);
            cairo_matrix_init(&matrix1, qrs1[0], qrs1[3], qrs1[1], qrs1[4], w1*(d4.x+d8.x+d5.x+d1.x)/4.0, h1*(d4.y+d8.y+d5.y+d1.y)/4.0);
            cairo_transform(cr, &matrix1);

            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_rectangle(cr, -60, -60, 120, 120);
            cairo_stroke(cr);  

            if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
            cairo_text_extents(cr, "2", &extents);
            cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
            cairo_show_text(cr, "2");
            cairo_restore(cr);
          }
      }    
  }
static void draw_checkerboard(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;
    gint j=0;
    gint counter=0;

    const struct shape *s1=get_drawing_from_shape_cache(CHECKERBOARD, 0, 0);

    struct point *p1=&g_array_index(s1->sr, struct point, 0);
    
    //Draw the 64 squares of the checkerboard.
    for(i=0;i<8;i++)
      {
        for(j=0;j<8;j++)
          {
            counter++;
            if(counter%2==0) cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            else cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
            cairo_move_to(cr, w1*((*p1).x), h1*((*p1).y));
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            cairo_line_to(cr, w1*((*(p1+10)).x), h1*((*(p1+10)).y));
            cairo_line_to(cr, w1*((*(p1+9)).x), h1*((*(p1+9)).y));
            cairo_close_path(cr);
            cairo_fill(cr);
            p1++;
          }
        p1++;
        counter++;
      }
  }
static void draw_ball(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    //Color the triangles and quads of the ball.
    gint i=0;
    gint j=0;
    gint counter=0;

    const struct shape *s1=get_drawing_from_shape_cache(BALL, rings, points_per_ring);

    gint array_len=s1->sr->len;
    gint rings_c=(s1->rings)-1;
    gint offset1=(s1->points_per_ring)+2;
    gint offset2=(s1->points_per_ring)+1;
    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    struct point3d top=g_array_index(s1->sr, struct point3d, array_len-2);
    struct point3d bottom=g_array_index(s1->sr, struct point3d, array_len-1);

    //Draw the top triangles.
    for(i=0;i<(s1->points_per_ring);i++)
      {
        if(counter%2==0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        else cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
        if(top.z+(*(p1)).z+(*(p1+1)).z>0.0)
          {
            cairo_move_to(cr, w1*top.x, h1*top.y);
            cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            cairo_close_path(cr);
            cairo_fill(cr);
          }
        counter++;
        p1++;
      }

    //Draw middle quads.
    counter++;
    p1=&g_array_index(s1->sr, struct point3d, 0);
    for(i=0;i<rings_c;i++)
      {
        for(j=0;j<(s1->points_per_ring);j++)
          {
            if(counter%2==0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
            if((*(p1)).z+(*(p1+1)).z+(*(p1+offset1)).z+(*(p1+offset2)).z>0.0)
              {
                cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
                cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
                cairo_line_to(cr, w1*((*(p1+offset1)).x), h1*((*(p1+offset1)).y));
                cairo_line_to(cr, w1*((*(p1+offset2)).x), h1*((*(p1+offset2)).y));
                cairo_close_path(cr);
                cairo_fill(cr);
              }
            counter++;
            p1++;
          }
        counter++;
        p1++;
      }

    //Draw the bottom triangles.
    //g_print("%i %i\n", counter, (rings_t-1)*(points_per_ring_t+1));
    p1=&g_array_index(s1->sr, struct point3d, ((s1->rings)-1)*((s1->points_per_ring)+1));
    for(i=0;i<(s1->points_per_ring);i++)
      {
        if(counter%2==0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        else cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
        if(bottom.z+(*(p1)).z+(*(p1+1)).z>0.0)
          {
            cairo_move_to(cr, w1*bottom.x, h1*bottom.y);
            cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            cairo_close_path(cr);
            cairo_fill(cr);
          }
        counter++;
        p1++;
      }

  }
static void draw_ball_mesh(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    /*
        Draw a ball with a mesh gradient. Use the symetry of the ball to draw the quads. That
      way there isn't a need to sort the quads before drawing like in the funnel.
    */
    gint i=0;
    gint j=0;
    gdouble c1[4]={0.0, 0.0, 1.0, 1.0};
    gdouble c2[4]={0.0, 0.0, 1.0, 1.0};

    const struct shape *s1=get_drawing_from_shape_cache(BALL, rings, points_per_ring);

    gint array_len=s1->sr->len;
    gint rings_c=(s1->rings)-1;
    gint offset1=(s1->points_per_ring)+2;
    gint offset2=(s1->points_per_ring)+1;
    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    struct point3d top=g_array_index(s1->sr, struct point3d, array_len-2);
    struct point3d bottom=g_array_index(s1->sr, struct point3d, array_len-1);

    //Draw the top triangles cyan.
    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
    for(i=0;i<(s1->points_per_ring);i++)
      {
        if(top.z+(*(p1)).z+(*(p1+1)).z>0.0)
          {
            cairo_move_to(cr, w1*top.x, h1*top.y);
            cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            cairo_close_path(cr);
            cairo_fill(cr);
          }
        p1++;
      }

    //Draw middle quads.
    p1=&g_array_index(s1->sr, struct point3d, 0);
    gdouble den=(gdouble)rings_c/2.0;
    c2[1]+=1.0/den;
    c2[2]-=1.0/den;
    for(i=0;i<rings_c;i++)
      {
        for(j=0;j<(s1->points_per_ring);j++)
          {
            if((*(p1)).z+(*(p1+1)).z+(*(p1+offset1)).z+(*(p1+offset2)).z>0.0)
              {
                cairo_pattern_t *pattern1=cairo_pattern_create_mesh();
                cairo_mesh_pattern_begin_patch(pattern1);
                cairo_mesh_pattern_move_to(pattern1, w1*((*(p1)).x), h1*((*(p1)).y));
                cairo_mesh_pattern_line_to(pattern1, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
                cairo_mesh_pattern_line_to(pattern1, w1*((*(p1+offset1)).x), h1*((*(p1+offset1)).y));
                cairo_mesh_pattern_line_to(pattern1, w1*((*(p1+offset2)).x), h1*((*(p1+offset2)).y));
                cairo_mesh_pattern_line_to(pattern1, w1*((*(p1)).x), h1*((*(p1)).y));        
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, c1[0], c1[1], c1[2], c1[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, c1[0], c1[1], c1[2], c1[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, c2[0], c2[1], c2[2], c2[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, c2[0], c2[1], c2[2], c2[3]);
                cairo_mesh_pattern_end_patch(pattern1);
                cairo_set_source(cr, pattern1);
                cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
                cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
                cairo_line_to(cr, w1*((*(p1+offset1)).x), h1*((*(p1+offset1)).y));
                cairo_line_to(cr, w1*((*(p1+offset2)).x), h1*((*(p1+offset2)).y));
                cairo_close_path(cr);
                cairo_fill(cr);
                cairo_pattern_destroy(pattern1);
              }
            p1++;
          }
        //g_print("%i %f   %f %f %f    %f %f %f\n", i, den, c1[0], c1[1], c1[2], c2[0], c2[1], c2[2]);
        if(i<(gint)(den-1.0))
          {
            c1[1]+=1.0/den;
            c1[2]-=1.0/den;
            c2[1]+=1.0/den;
            c2[2]-=1.0/den;
          }
        else if(i==(gint)(den-1))
          {
            c1[0]=0.0;c1[1]=1.0;c1[2]=0.0;
            c2[0]=1.0/den;c2[1]=1.0-1.0/den;c2[2]=0.0;
          }
        else
          {
            c1[0]+=1.0/den;
            c1[1]-=1.0/den;
            c2[0]+=1.0/den;
            c2[1]-=1.0/den;
          }
        if(c2[0]>1.0)
          {
            c2[0]=1.0;c2[1]=0.0;c2[2]=0.0;
          }
        p1++;
      }

    //Draw the bottom triangles yellow.
    p1=&g_array_index(s1->sr, struct point3d, ((s1->rings)-1)*((s1->points_per_ring)+1));
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    for(i=0;i<(s1->points_per_ring);i++)
      {
        if(bottom.z+(*(p1)).z+(*(p1+1)).z>0.0)
          {
            cairo_move_to(cr, w1*bottom.x, h1*bottom.y);
            cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            cairo_close_path(cr);
            cairo_fill(cr);
          }
        p1++;
      }

  }
static void draw_wire_ball(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;
    gint j=0;
    gint counter=1;

    const struct shape *s1=get_drawing_from_shape_cache(BALL, 30, 30);

    gint array_len=s1->sr->len;
    gint rings_c=(s1->rings)-1;
    gint offset1=(s1->points_per_ring)+2;
    gint offset2=(s1->points_per_ring)+1;
    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    struct point3d top=g_array_index(s1->sr, struct point3d, array_len-2);
    struct point3d bottom=g_array_index(s1->sr, struct point3d, array_len-1);
    
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    //Draw the top triangles.
    for(i=0;i<(s1->points_per_ring);i++)
      {
        if(counter%2==0) cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
        else cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        cairo_move_to(cr, w1*top.x, h1*top.y);
        cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
        cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
        cairo_stroke(cr);
        counter++;
        p1++;
      }

    //Draw middle quads.
    p1=&g_array_index(s1->sr, struct point3d, 0);
    for(i=0;i<rings_c;i++)
      {
        for(j=0;j<(s1->points_per_ring);j++)
          {
            if(counter%2==0) cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
            else cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            cairo_line_to(cr, w1*((*(p1+offset1)).x), h1*((*(p1+offset1)).y));
            cairo_line_to(cr, w1*((*(p1+offset2)).x), h1*((*(p1+offset2)).y));
            cairo_stroke(cr);
            counter++;
            p1++;
          }
        counter++;
        p1++;
      }

    //Draw the bottom triangles.
    p1=&g_array_index(s1->sr, struct point3d, ((s1->rings)-1)*((s1->points_per_ring)+1));
    for(i=0;i<(s1->points_per_ring);i++)
      {
        if(counter%2==0) cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
        else cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        cairo_move_to(cr, w1*bottom.x, h1*bottom.y);
        cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));  
        cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
        cairo_stroke(cr); 
        counter++;       
        p1++;
      }

  }
static void draw_wire_ball2(cairo_t *cr, gdouble w1, gdouble h1)
  {
    /*
       Reduce the number of lines drawn by drawing the rings of the circle instead of
       the patches. A little better performance.
    */
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(BALL, 30, 30);
   
    gint array_len=s1->sr->len;
    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    struct point3d top=g_array_index(s1->sr, struct point3d, array_len-2);
    struct point3d bottom=g_array_index(s1->sr, struct point3d, array_len-1);

    //Draw the latitude rings.
    cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
    for(i=0;i<(s1->rings);i++)
      {
        cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
        for(j=0;j<(s1->points_per_ring);j++)
          {
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            p1++;
          }
         cairo_stroke(cr);
         p1++;  
       }  

    //Draw the longitude arcs from pole to pole.
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    gint offset1=(s1->points_per_ring)+1;
    gint offset2=0;
    p1=&g_array_index(s1->sr, struct point3d, 0);
    for(i=0;i<(s1->points_per_ring);i++)
      {
        cairo_move_to(cr, w1*top.x, h1*top.y);
        for(j=0;j<(s1->rings);j++)
          {
            offset2=offset1*(j)+i;
            cairo_line_to(cr, w1*((*(p1+offset2)).x), h1*((*(p1+offset2)).y));
          }
        cairo_line_to(cr, w1*bottom.x, h1*bottom.y);
        cairo_stroke(cr);  
      }
      
  }
static void draw_wire_funnel(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    //Draw some blue rings and green rays for wire funnel.
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(FUNNEL, rings, points_per_ring);
  
    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    struct point3d *p2=&g_array_index(s1->sr, struct point3d, points_per_ring);

    //The blue rings on the funnel.
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0); 
    for(i=0;i<(s1->rings);i++)
      { 
        cairo_move_to(cr, w1*(p1->x), h1*(p1->y));
        p1++; 
        for(j=1;j<(s1->points_per_ring);j++)
          {   
            cairo_line_to(cr, w1*(p1->x), h1*(p1->y));
            p1++;        
          }
        cairo_close_path(cr);
        cairo_stroke(cr); 
      }

   
    //The green lines into the funnel.
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0); 
    p1=&g_array_index(s1->sr, struct point3d, 0);
    for(i=0;i<(s1->points_per_ring);i++)
      {  
        cairo_move_to(cr, w1*(p1->x), h1*(p1->y));     
        for(j=1;j<(s1->rings);j++)
          {   
            cairo_line_to(cr, w1*(p2->x), h1*(p2->y));
            p2+=(s1->points_per_ring);      
          } 
        cairo_stroke(cr);
        p1++;
        p2=p1+(s1->points_per_ring);
      }
    
  }
static void draw_funnel(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(FUNNEL, rings, points_per_ring);

    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    struct point3d *p2=&g_array_index(s1->sr, struct point3d, (s1->points_per_ring));
    struct point3d *p3=&g_array_index(s1->sr, struct point3d, 0);
    struct point3d *p4=&g_array_index(s1->sr, struct point3d, (s1->points_per_ring));

    //An array to sort quads.
    struct quad_plane_mesh *quads_start=g_malloc(((s1->rings)-1)*(s1->points_per_ring)*sizeof(struct quad_plane_mesh));
    struct quad_plane_mesh *quads=quads_start;

    //Fill the quad struct mesh array.
    gdouble c1[4]={0.0, 0.0, 1.0, 1.0};
    gdouble c2[4]={0.0, 0.0, 1.0, 1.0};
    gdouble den=(s1->rings);
    for(i=0;i<((s1->rings)-1);i++)
      {
        c1[1]=(gdouble)i/den;
        c2[1]=(gdouble)(i+1)/den;
        for(j=0;j<((s1->points_per_ring)-1);j++)
          {             
            quads->x1=((*(p1)).x);
            quads->y1=((*(p1)).y);
            quads->z1=((*(p1)).z);
            quads->x2=((*(p1+1)).x);
            quads->y2=((*(p1+1)).y);
            quads->z2=((*(p1+1)).z);            
            quads->x3=((*(p2+1)).x);
            quads->y3=((*(p2+1)).y);
            quads->z3=((*(p2+1)).z);
            quads->x4=((*(p2)).x);
            quads->y4=((*(p2)).y);
            quads->z4=((*(p2)).z);
            //Plane color.
            (quads->c1)[0]=c1[0];(quads->c1)[1]=c1[1];(quads->c1)[2]=c1[2];(quads->c1)[3]=c1[3];
            (quads->c2)[0]=c2[0];(quads->c2)[1]=c2[1];(quads->c2)[2]=c2[2];(quads->c2)[3]=c2[3];
            (quads->c3)[0]=1.0;(quads->c3)[1]=1.0;(quads->c3)[2]=0.0;(quads->c3)[3]=1.0;
            (quads->c4)[0]=1.0;(quads->c4)[1]=1.0;(quads->c4)[2]=0.0;(quads->c4)[3]=1.0;
            quads++;
            p1++;p2++;
          }
        //The last quad in each ring.
        quads->x1=((*(p1)).x);
        quads->y1=((*(p1)).y);
        quads->z1=((*(p1)).z);
        quads->x2=((*(p3)).x);
        quads->y2=((*(p3)).y);
        quads->z2=((*(p3)).z);            
        quads->x3=((*(p4)).x);
        quads->y3=((*(p4)).y);
        quads->z3=((*(p4)).z);
        quads->x4=((*(p2)).x);
        quads->y4=((*(p2)).y);
        quads->z4=((*(p2)).z);
        //Plane color.
        (quads->c1)[0]=c1[0];(quads->c1)[1]=c1[1];(quads->c1)[2]=c1[2];(quads->c1)[3]=c1[3];
        (quads->c2)[0]=c2[0];(quads->c2)[1]=c2[1];(quads->c2)[2]=c2[2];(quads->c2)[3]=c2[3];
        (quads->c3)[0]=1.0;(quads->c3)[1]=1.0;(quads->c3)[2]=0.0;(quads->c3)[3]=1.0;
        (quads->c4)[0]=1.0;(quads->c4)[1]=1.0;(quads->c4)[2]=0.0;(quads->c4)[3]=1.0;
        p1++;p2++;quads++;
        p3+=(s1->points_per_ring);
        p4+=(s1->points_per_ring);
      }
    
    //Sort the quad plane array based on z values.
    qsort(quads_start, ((s1->rings)-1)*(s1->points_per_ring), sizeof(struct quad_plane_mesh), compare_quads_mesh);

    //Draw the sorted quads.
    quads=quads_start;    
    for(i=0;i<((s1->rings)-1);i++)
      {
        for(j=0;j<(s1->points_per_ring);j++)
          {
            cairo_pattern_t *pattern1=cairo_pattern_create_mesh();
            cairo_mesh_pattern_begin_patch(pattern1);
            cairo_mesh_pattern_move_to(pattern1, w1*quads->x1, h1*quads->y1);
            cairo_mesh_pattern_line_to(pattern1, w1*quads->x2, h1*quads->y2);
            cairo_mesh_pattern_line_to(pattern1, w1*quads->x3, h1*quads->y3);
            cairo_mesh_pattern_line_to(pattern1, w1*quads->x4, h1*quads->y4);
            cairo_mesh_pattern_line_to(pattern1, w1*quads->x1, h1*quads->y1);        
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, (quads->c1)[0], (quads->c1)[1], (quads->c1)[2], (quads->c1)[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, (quads->c1)[0], (quads->c1)[1], (quads->c1)[2], (quads->c1)[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, (quads->c2)[0], (quads->c2)[1], (quads->c2)[2], (quads->c2)[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, (quads->c2)[0], (quads->c2)[1], (quads->c2)[2], (quads->c2)[3]);
            cairo_mesh_pattern_end_patch(pattern1);
            cairo_set_source(cr, pattern1);
            cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
            cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
            cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
            cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
            quads++;
          }
      } 
    g_free(quads_start);    
  }
static void draw_gem(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;

    const struct shape *s1=get_drawing_from_shape_cache(GEM, 0, 0);
   
    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    struct point3d *p2=&g_array_index(s1->sr, struct point3d, 0);

    //Draw the top polygon of the gem.
    cairo_pattern_t *radial1=cairo_pattern_create_radial(0.0, 0.0, 0.5*w1, 0.0, 0.0, 2.0*w1);  
    cairo_pattern_add_color_stop_rgba(radial1, 0.0, 1.0, 0.0, 1.0, 0.3);
    cairo_pattern_add_color_stop_rgba(radial1, 1.0, 0.0, 0.0, 1.0, 0.5);
    cairo_set_source(cr, radial1);
    cairo_move_to(cr, w1*(*p1).x, h1*(*p1).y);  
    for(i=0;i<12;i++)
      { 
        cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
        cairo_stroke_preserve(cr);
        p1++;        
      }
    cairo_close_path(cr);
    cairo_fill(cr);
    cairo_pattern_destroy(radial1); 

    //Draw quad ring.
    p1=&g_array_index(s1->sr, struct point3d, 0);
    p2=&g_array_index(s1->sr, struct point3d, 13); 
    for(i=0;i<12;i++)
      { 
        cairo_pattern_t *pattern1=cairo_pattern_create_mesh();
        cairo_mesh_pattern_begin_patch(pattern1);
        cairo_mesh_pattern_move_to(pattern1, w1*(*p1).x, h1*(*p1).y);
        cairo_mesh_pattern_line_to(pattern1, w1*((*(p2)).x), h1*((*(p2)).y));
        cairo_mesh_pattern_line_to(pattern1, w1*((*(p2+1)).x), h1*((*(p2+1)).y));
        cairo_mesh_pattern_line_to(pattern1, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
        cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, 0.0, 0.0, 1.0, 0.4);
        cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, 0.0, 0.0, 1.0, 0.4);
        cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, 1.0, 0.0, 1.0, 0.4);
        cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, 1.0, 0.0, 1.0, 0.4);
        cairo_mesh_pattern_end_patch(pattern1);
        cairo_set_source(cr, pattern1);
        cairo_paint(cr);   
        cairo_pattern_destroy(pattern1);

        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0); 
        cairo_move_to(cr, w1*(*p1).x, h1*(*p1).y); 
        cairo_line_to(cr, w1*((*(p2)).x), h1*((*(p2)).y));
        cairo_line_to(cr, w1*((*(p2+1)).x), h1*((*(p2+1)).y));
        cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
        cairo_close_path(cr);
        cairo_stroke(cr);
        p1++;p2++;        
      }  

    //Bottom triangles.
    p1=&g_array_index(s1->sr, struct point3d, 26);
    p2=&g_array_index(s1->sr, struct point3d, 13);  
    for(i=0;i<12;i++)
      { 
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.2);
        cairo_move_to(cr, w1*(*p2).x, h1*(*p2).y); 
        cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
        cairo_line_to(cr, w1*((*(p2+1)).x), h1*((*(p2+1)).y));
        cairo_close_path(cr);
        cairo_fill_preserve(cr);
        cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 0.5);
        cairo_stroke(cr);
        p2++;        
      }  
   
  }
static void draw_wire_torus(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(TORUS, rings, points_per_ring);

    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    
    //Draw the rings.
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    for(i=0;i<(s1->rings);i++)
      {
        cairo_move_to(cr, w1*((*p1).x), h1*((*p1).y));
        for(j=0;j<(s1->points_per_ring);j++)
          {
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            p1++;        
          }
         cairo_close_path(cr);
         cairo_stroke(cr);
         p1++;
       }

    //Draw lines between the rings.
    p1=&g_array_index(s1->sr, struct point3d, 0);
    gint offset=(s1->points_per_ring)+1;
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    for(i=0;i<(s1->points_per_ring);i++)
      {
        cairo_move_to(cr, w1*((*p1).x), h1*((*p1).y));
        for(j=0;j<(s1->rings)-1;j++)
          {
            cairo_line_to(cr, w1*((*(p1+offset*(j+1))).x), h1*((*(p1+offset*(j+1))).y));       
          }
         cairo_close_path(cr);
         cairo_stroke(cr);
         p1++;
       }
  }
static void draw_torus(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(TORUS, rings, points_per_ring);

    gint offset=(s1->points_per_ring)+1;
    struct point3d *p1=NULL;
    struct point3d *p2=NULL;
    
    //Draw the segments
    for(i=0;i<(s1->rings)-1;i++)
      {
        p1=&g_array_index(s1->sr, struct point3d, i*offset);
        p2=&g_array_index(s1->sr, struct point3d, (i+1)*offset);
        for(j=0;j<(s1->points_per_ring);j++)
          {
            cairo_move_to(cr, w1*((*p1).x), h1*((*p1).y));
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            cairo_line_to(cr, w1*((*(p2+1)).x), h1*((*(p2+1)).y));
            cairo_line_to(cr, w1*((*(p2)).x), h1*((*(p2)).y));
            cairo_close_path(cr);
            cairo_stroke_preserve(cr);
            cairo_fill(cr);
            p1++;p2++;        
          }
       }

     //The last segment
     p1=&g_array_index(s1->sr, struct point3d, i*offset);
     p2=&g_array_index(s1->sr, struct point3d, 0);
     for(j=0;j<(s1->points_per_ring);j++)
          {
            cairo_move_to(cr, w1*((*p1).x), h1*((*p1).y));
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            cairo_line_to(cr, w1*((*(p2+1)).x), h1*((*(p2+1)).y));
            cairo_line_to(cr, w1*((*(p2)).x), h1*((*(p2)).y));
            cairo_close_path(cr);
            cairo_stroke_preserve(cr);
            cairo_fill(cr);
            p1++;p2++;        
          }
  }
static void draw_wire_twist(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(TWIST, 0, 0);

    struct point *p1=&g_array_index(s1->sr, struct point, 0);
    struct point *p2=&g_array_index(s1->sr, struct point, 17);

    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    for(i=0;i<17;i++)
      {
        p2=&g_array_index(s1->sr, struct point, 17+i);
        cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
        for(j=0;j<16;j++)
          {            
            cairo_line_to(cr, w1*((*(p2)).x), h1*((*(p2)).y));
            p2+=17;
          }
        cairo_stroke(cr);
        p1++;
      }
  }
static void draw_twist(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(TWIST, 0, 0);
    
    struct point *p1=&g_array_index(s1->sr, struct point, 0);
    struct point *p2=&g_array_index(s1->sr, struct point, 17);

    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    for(i=0;i<16;i++)
      {
        if((i+1)%2==0) cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        else cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
        p1=&g_array_index(s1->sr, struct point, i);
        p2=&g_array_index(s1->sr, struct point, 17+i);
        for(j=0;j<16;j++)
          {   
            cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));         
            cairo_line_to(cr, w1*((*(p2)).x), h1*((*(p2)).y));
            cairo_line_to(cr, w1*((*(p2+1)).x), h1*((*(p2+1)).y));
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            cairo_close_path(cr);
            cairo_fill(cr);
            p1+=17;p2+=17;
          }
        
      }
  }
static void draw_triangle(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gdouble delta_x=0.0;
    gdouble delta_y=0.0;
    gdouble t_points[6]={0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    const struct shape *s1=get_drawing_from_shape_cache(TRIANGLE, 0, 0);

    //Try for some projections with a triangle.
    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    gdouble yellow_sun[3]={3.0, -3.0, 3.0};
    gdouble opposite_yellow[3]={-3.0, 3.0, -3.0};
    //A plane to project a rotated triangle onto.
    gdouble plane[12]; 
    plane[0]=-3.0*w1; plane[1]=-3.0*h1; plane[2]=0.0;
    plane[3]=3.0*w1; plane[4]=-3.0*h1; plane[5]=0.0;
    plane[6]=3.0*w1; plane[7]=3.0*h1; plane[8]=0.0;
    plane[9]=-3.0*w1; plane[10]=3.0*h1; plane[11]=0.0;
    gdouble plane_triangle[9];
    plane_triangle[0]=w1*(*(p1)).x;
    plane_triangle[1]=h1*(*(p1)).y;
    plane_triangle[2]=0.0;
    plane_triangle[3]=w1*(*(p1+1)).x;
    plane_triangle[4]=h1*(*(p1+1)).y;
    plane_triangle[5]=0.0;
    plane_triangle[6]=w1*(*(p1+2)).x;
    plane_triangle[7]=h1*(*(p1+2)).y;
    plane_triangle[8]=0.0;

    //Rotate plane and draw.
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.5);
    gdouble qrs2[9];
    quaternion_rotation(0.0, G_PI/4.0, 8.5*G_PI/6.0, qrs2);
    plane[0]=(plane[0]*qrs2[0])+(plane[1]*qrs2[1])+(plane[2]*qrs2[2]);
    plane[1]=(plane[0]*qrs2[3])+(plane[1]*qrs2[4])+(plane[2]*qrs2[5]);
    plane[3]=(plane[3]*qrs2[0])+(plane[4]*qrs2[1])+(plane[5]*qrs2[2]);
    plane[4]=(plane[3]*qrs2[3])+(plane[4]*qrs2[4])+(plane[5]*qrs2[5]);
    plane[6]=(plane[6]*qrs2[0])+(plane[7]*qrs2[1])+(plane[8]*qrs2[2]);
    plane[7]=(plane[6]*qrs2[3])+(plane[7]*qrs2[4])+(plane[8]*qrs2[5]);
    plane[9]=(plane[9]*qrs2[0])+(plane[10]*qrs2[1])+(plane[11]*qrs2[2]);
    plane[10]=(plane[9]*qrs2[3])+(plane[10]*qrs2[4])+(plane[11]*qrs2[5]);
    cairo_move_to(cr, plane[0]+w1*3.0, plane[1]+h1*3.0);
    cairo_line_to(cr, plane[3]+w1*3.0, plane[4]+h1*3.0);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane[6]+w1*3.0, plane[7]+h1*3.0);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane[9]+w1*3.0, plane[10]+h1*3.0);
    cairo_stroke_preserve(cr);
    cairo_close_path(cr);
    cairo_fill(cr);

    //Rotate plane triangle and draw.
    cairo_set_line_width(cr, 1.0);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    plane_triangle[0]=(plane_triangle[0]*qrs2[0])+(plane_triangle[1]*qrs2[1])+(plane_triangle[2]*qrs2[2]);
    plane_triangle[1]=(plane_triangle[0]*qrs2[3])+(plane_triangle[1]*qrs2[4])+(plane_triangle[2]*qrs2[5]);
    plane_triangle[3]=(plane_triangle[3]*qrs2[0])+(plane_triangle[4]*qrs2[1])+(plane_triangle[5]*qrs2[2]);
    plane_triangle[4]=(plane_triangle[3]*qrs2[3])+(plane_triangle[4]*qrs2[4])+(plane_triangle[5]*qrs2[5]);
    plane_triangle[6]=(plane_triangle[6]*qrs2[0])+(plane_triangle[7]*qrs2[1])+(plane_triangle[8]*qrs2[2]);
    plane_triangle[7]=(plane_triangle[6]*qrs2[3])+(plane_triangle[7]*qrs2[4])+(plane_triangle[8]*qrs2[5]);
    cairo_move_to(cr, plane_triangle[0]+w1*3.0, plane_triangle[1]+h1*3.0);
    cairo_line_to(cr, plane_triangle[3]+w1*3.0, plane_triangle[4]+h1*3.0);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_triangle[6]+w1*3.0, plane_triangle[7]+h1*3.0);
    cairo_stroke_preserve(cr);
    cairo_close_path(cr);
    cairo_stroke(cr);

    //Translated center triangle to plane.
    cairo_move_to(cr, w1*((*(p1)).x)+3.0*w1, h1*((*(p1)).y)+3.0*h1);         
    cairo_line_to(cr, w1*((*(p1+1)).x)+3.0*w1, h1*((*(p1+1)).y)+3.0*h1);                    
    cairo_line_to(cr, w1*((*(p1+2)).x)+3.0*w1, h1*((*(p1+2)).y)+3.0*h1);
    cairo_close_path(cr);
    cairo_stroke(cr); 

    //Yellow sun.
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_arc(cr,  w1*yellow_sun[0], h1*yellow_sun[1], 15.0, 0.0, 2*G_PI);
    cairo_fill(cr);

    //Lines from yellow sun to middle triangle and beyond.
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    delta_x=(w1*yellow_sun[0]-w1*((*(p1)).x));
    delta_y=(h1*yellow_sun[1]-h1*((*(p1)).y));
    t_points[0]=w1*((*(p1)).x)-delta_x;
    t_points[1]=h1*((*(p1)).y)-delta_y;
    cairo_move_to(cr,  w1*yellow_sun[0], h1*yellow_sun[1]);
    cairo_line_to(cr, t_points[0], t_points[1]);  
    cairo_stroke(cr);
    delta_x=(w1*yellow_sun[0]-w1*((*(p1+1)).x));
    delta_y=(h1*yellow_sun[1]-h1*((*(p1+1)).y));
    t_points[2]=w1*((*(p1+1)).x)-delta_x;
    t_points[3]=h1*((*(p1+1)).y)-delta_y;
    cairo_move_to(cr,  w1*yellow_sun[0], h1*yellow_sun[1]);
    cairo_line_to(cr, t_points[2], t_points[3]);  
    cairo_stroke(cr);
    delta_x=(w1*yellow_sun[0]-w1*((*(p1+2)).x));
    delta_y=(h1*yellow_sun[1]-h1*((*(p1+2)).y));
    t_points[4]=w1*((*(p1+2)).x)-delta_x;
    t_points[5]=h1*((*(p1+2)).y)-delta_y;
    cairo_move_to(cr,  w1*yellow_sun[0], h1*yellow_sun[1]);
    cairo_line_to(cr, t_points[4], t_points[5]);  
    cairo_stroke(cr);
    cairo_move_to(cr, t_points[0], t_points[1]);
    cairo_line_to(cr, t_points[2], t_points[3]);
    cairo_line_to(cr, t_points[4], t_points[5]);
    cairo_close_path(cr);
    cairo_stroke(cr);

    //Opposite of yellow sun.
    cairo_set_source_rgba(cr, 0.7, 0.7, 0.7, 1.0);
    cairo_arc(cr,  w1*opposite_yellow[0], h1*opposite_yellow[1], 15.0, 0.0, 2*G_PI);
    cairo_fill(cr);

    //Purple rotated triangle.
    cairo_set_line_width(cr, 10.0);  
    cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));         
    cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));                   
    cairo_line_to(cr, w1*((*(p1+2)).x), h1*((*(p1+2)).y));
    cairo_close_path(cr);
    cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 0.6);
    cairo_fill_preserve(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);  
    cairo_stroke(cr); 

    //Translate the rotated center triangle.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.2);  
    cairo_move_to(cr, w1*((*(p1)).x)-3.0*w1, h1*((*(p1)).y)+3.0*h1);         
    cairo_line_to(cr, w1*((*(p1+1)).x)-3.0*w1, h1*((*(p1+1)).y)+3.0*h1);                    
    cairo_line_to(cr, w1*((*(p1+2)).x)-3.0*w1, h1*((*(p1+2)).y)+3.0*h1);
    cairo_close_path(cr);
    cairo_stroke_preserve(cr);
    cairo_fill(cr); 

    //Connect the two triangles and fill.
    cairo_set_line_width(cr, 1.0);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.1);
    cairo_move_to(cr, w1*((*(p1)).x)-3.0*w1, h1*((*(p1)).y)+3.0*h1);         
    cairo_line_to(cr, w1*((*(p1+1)).x)-3.0*w1, h1*((*(p1+1)).y)+3.0*h1);                    
    cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
    cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
    cairo_close_path(cr);
    cairo_fill_preserve(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_stroke(cr); 

    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.1);
    cairo_move_to(cr, w1*((*(p1+1)).x)-3.0*w1, h1*((*(p1+1)).y)+3.0*h1);         
    cairo_line_to(cr, w1*((*(p1+2)).x)-3.0*w1, h1*((*(p1+2)).y)+3.0*h1);                   
    cairo_line_to(cr, w1*((*(p1+2)).x), h1*((*(p1+2)).y));
    cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
    cairo_close_path(cr);
    cairo_fill_preserve(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_stroke(cr); 

    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.1);
    cairo_move_to(cr, w1*((*(p1+2)).x)-3.0*w1, h1*((*(p1+2)).y)+3.0*h1);         
    cairo_line_to(cr, w1*((*(p1)).x)-3.0*w1, h1*((*(p1)).y)+3.0*h1);                   
    cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
    cairo_line_to(cr, w1*((*(p1+2)).x), h1*((*(p1+2)).y));
    cairo_close_path(cr);
    cairo_fill_preserve(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_stroke(cr);     
    
    //Center triangles to dot.
    cairo_set_source_rgba(cr, 1.0, 0.5, 1.0, 0.2);    
    cairo_move_to(cr, w1*opposite_yellow[0], h1*opposite_yellow[1]);
    cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));                              
    cairo_line_to(cr, w1*((*(p1+2)).x), h1*((*(p1+2)).y));
    cairo_close_path(cr);
    cairo_fill_preserve(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_stroke(cr);  
    
    cairo_set_source_rgba(cr, 1.0, 0.5, 1.0, 0.2); 
    cairo_move_to(cr, w1*opposite_yellow[0], h1*opposite_yellow[1]);
    cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));                              
    cairo_line_to(cr, w1*((*(p1+2)).x), h1*((*(p1+2)).y));
    cairo_close_path(cr);
    cairo_fill_preserve(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_stroke(cr);  

    cairo_set_source_rgba(cr, 1.0, 0.5, 1.0, 0.2); 
    cairo_move_to(cr, w1*opposite_yellow[0], h1*opposite_yellow[1]);
    cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));                              
    cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
    cairo_close_path(cr);
    cairo_fill_preserve(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_stroke(cr);   
  }
static void draw_pyramid(cairo_t *cr, gdouble w1, gdouble h1)
  {  
    /*
       Draw a pyramid. Order the planes with qsort before drawing so there isn't overlap.
       Draw the planes from -z to +z.
    */
    gint i=0;

    const struct shape *s1=get_drawing_from_shape_cache(PYRAMID, 0, 0);
    
    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    struct plane_order po[4];
    po[0].id=0;
    po[0].value=2.0*((*(p1)).z)+((*(p1+1)).z)+((*(p1+2)).z);
    po[1].id=1;
    po[1].value=2.0*((*(p1)).z)+((*(p1+2)).z)+((*(p1+3)).z);
    po[2].id=2;
    po[2].value=2.0*((*(p1)).z)+((*(p1+3)).z)+((*(p1+4)).z);
    po[3].id=3;
    po[3].value=2.0*((*(p1)).z)+((*(p1+4)).z)+((*(p1+1)).z);

    qsort(po, 4, sizeof(struct plane_order), compare_planes);

    //Draw four triangle faces in z-order.
    for(i=0;i<4;i++)
      {
        if(po[i].id==0)
          {
            cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
            cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));         
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            cairo_line_to(cr, w1*((*(p1+2)).x), h1*((*(p1+2)).y));
            cairo_close_path(cr);
            cairo_fill(cr);
          }
        else if(po[i].id==1)
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
            cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));         
            cairo_line_to(cr, w1*((*(p1+2)).x), h1*((*(p1+2)).y));
            cairo_line_to(cr, w1*((*(p1+3)).x), h1*((*(p1+3)).y));
            cairo_close_path(cr);
            cairo_fill(cr);
          }   
        else if(po[i].id==2)
          {
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));         
            cairo_line_to(cr, w1*((*(p1+3)).x), h1*((*(p1+3)).y));
            cairo_line_to(cr, w1*((*(p1+4)).x), h1*((*(p1+4)).y));
            cairo_close_path(cr);
            cairo_fill(cr);
          }    
        else
          {
            cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
            cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));         
            cairo_line_to(cr, w1*((*(p1+4)).x), h1*((*(p1+4)).y));
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            cairo_close_path(cr);
            cairo_fill(cr);
          }
      }

    //Base square.
    if(((*(p1+1)).z)+((*(p1+2)).z)+((*(p1+3)).z)+((*(p1+4)).z)>0.0)
      {
        cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
        cairo_move_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));         
        cairo_line_to(cr, w1*((*(p1+2)).x), h1*((*(p1+2)).y));
        cairo_line_to(cr, w1*((*(p1+3)).x), h1*((*(p1+3)).y));
        cairo_line_to(cr, w1*((*(p1+4)).x), h1*((*(p1+4)).y));
        cairo_close_path(cr);
        cairo_fill(cr);
      }
                        
  }
static void draw_cone(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    /*
       Try sorting more than a few planes and then drawing. Use quad planes for the triangles
    also. Still some flickering in the point.
    */
    gint i=0;
    gint j=0;
    gint counter=1;

    const struct shape *s1=get_drawing_from_shape_cache(CONE, rings, points_per_ring);

    gint array_len=s1->sr->len;
    gint offset1=(s1->points_per_ring)+2;
    gint offset2=(s1->points_per_ring)+1;

    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    struct point3d top=g_array_index(s1->sr, struct point3d, array_len-1);

    //An array to sort quads.
    struct quad_plane *quads_start=g_malloc((s1->rings)*(s1->points_per_ring)*sizeof(struct quad_plane));
    struct quad_plane *quads=quads_start;

    //Fill the quad struct array.
    for(i=0;i<(s1->rings);i++)
      {
        for(j=0;j<(s1->points_per_ring);j++)
          {
            if(i<(s1->rings)-1)
              {                
                quads->x1=((*(p1)).x);
                quads->y1=((*(p1)).y);
                quads->z1=((*(p1)).z);
                quads->x2=((*(p1+1)).x);
                quads->y2=((*(p1+1)).y);
                quads->z2=((*(p1+1)).z);            
                quads->x3=((*(p1+offset1)).x);
                quads->y3=((*(p1+offset1)).y);
                quads->z3=((*(p1+offset1)).z);
                quads->x4=((*(p1+offset2)).x);
                quads->y4=((*(p1+offset2)).y);
                quads->z4=((*(p1+offset2)).z);
                //Plane color.
                if(counter%2==0)
                  {
                    quads->r=0.0;
                    quads->g=0.0;
                    quads->b=1.0;
                    quads->a=1.0;
                  }
                else
                  {
                    quads->r=0.0;
                    quads->g=1.0;
                    quads->b=1.0;
                    quads->a=1.0;
                  }
              }
            else
              {
                quads->x1=((*(p1)).x);
                quads->y1=((*(p1)).y);
                quads->z1=((*(p1)).z);
                quads->x2=((*(p1+1)).x);
                quads->y2=((*(p1+1)).y);
                quads->z2=((*(p1+1)).z);            
                quads->x3=top.x;
                quads->y3=top.y;
                /*
                  Trouble sorting the tip of the cone. Reuse the last rings z values so that
                  the tip of the cone planes sort better in the compare function. ???
                */
                quads->z3=((*(p1)).z);
                quads->x4=top.x;
                quads->y4=top.y;
                quads->z4=((*(p1+1)).z); 
                //Plane color.
                if(counter%2==0)
                  {
                    quads->r=0.0;
                    quads->g=0.0;
                    quads->b=1.0;
                    quads->a=1.0;
                  }
                else
                  {
                    quads->r=0.0;
                    quads->g=1.0;
                    quads->b=1.0;
                    quads->a=1.0;
                  }
              }
            counter++;
            quads++;
            p1++;
          }
        p1++;
      }
    
    //Sort the quad plane array based on z values.
    qsort(quads_start, (s1->rings)*(s1->points_per_ring), sizeof(struct quad_plane), compare_quads);

    //Draw the sorted quads.
    quads=quads_start;
    for(i=0;i<(s1->rings);i++)
      {
        for(j=0;j<(s1->points_per_ring);j++)
          {
            cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
            cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
            cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
            cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
            cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
            cairo_close_path(cr);
            cairo_fill(cr);
            quads++;
          }
      }

    //Draw the base.
    if(top.z<0.0)
      {
        p1=&g_array_index(s1->sr, struct point3d, 0);
        cairo_set_source_rgba(cr, 0.8, 0.8, 0.8, 0.7);
        cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
        for(i=0;i<(s1->points_per_ring);i++)
          {           
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            p1++;
          }
        cairo_close_path(cr);
        cairo_fill(cr);
      }

    g_free(quads_start);    
  }
static void draw_cone_shade(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    /*
       Similar to draw_cone() but with shaded colors on the cone quads.
    */
    gint i=0;
    gint j=0;
    gint counter=0;

    const struct shape *s1=get_drawing_from_shape_cache(CONE, rings, points_per_ring);
    
    gint array_len=s1->sr->len;
    gint offset1=(s1->points_per_ring)+2;
    gint offset2=(s1->points_per_ring)+1;
    gint total_quads=(s1->rings)*(s1->points_per_ring);
    gdouble distance=0.0;
    //initial drawing min and max.
    gdouble max=0.0;
    gdouble min=0.0;

    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    struct point3d top=g_array_index(s1->sr, struct point3d, array_len-1);

    //An array to sort quads.
    struct quad_plane *quads_start=g_malloc(total_quads*sizeof(struct quad_plane));
    struct quad_plane *quads=quads_start;

    //Fill the quad struct array.
    for(i=0;i<(s1->rings);i++)
      {
        for(j=0;j<(s1->points_per_ring);j++)
          {
            if(i<(s1->rings)-1)
              {  
                //Get a min max estimate.
                if(((*(p1)).z)>max) max=((*(p1)).z);
                if(((*(p1)).z)<min) min=((*(p1)).z);              
                quads->x1=((*(p1)).x);
                quads->y1=((*(p1)).y);
                quads->z1=((*(p1)).z);
                quads->x2=((*(p1+1)).x);
                quads->y2=((*(p1+1)).y);
                quads->z2=((*(p1+1)).z);            
                quads->x3=((*(p1+offset1)).x);
                quads->y3=((*(p1+offset1)).y);
                quads->z3=((*(p1+offset1)).z);
                quads->x4=((*(p1+offset2)).x);
                quads->y4=((*(p1+offset2)).y);
                quads->z4=((*(p1+offset2)).z);
                //Plane color to blue.
                quads->r=0.0;
                quads->g=0.0;
                quads->b=1.0;
                quads->a=1.0;                
              }
            else
              {
                //Get a min max estimate.
                if(((*(p1)).z)>max) max=((*(p1)).z);
                if(((*(p1)).z)<min) min=((*(p1)).z);   
                quads->x1=((*(p1)).x);
                quads->y1=((*(p1)).y);
                quads->z1=((*(p1)).z);
                quads->x2=((*(p1+1)).x);
                quads->y2=((*(p1+1)).y);
                quads->z2=((*(p1+1)).z);            
                quads->x3=top.x;
                quads->y3=top.y;
                /*
                  Trouble sorting the tip of the cone. Reuse the last rings z values so that
                  the tip of the cone planes sort better in the compare function. ???
                */
                quads->z3=((*(p1)).z);
                quads->x4=top.x;
                quads->y4=top.y;
                quads->z4=((*(p1+1)).z); 
                //Initialize plane color to blue.
                quads->r=0.0;
                quads->g=0.0;
                quads->b=1.0;
                quads->a=1.0;
              }
            quads++;
            p1++;
          }
        p1++;
      }
    
    //Sort the quad plane array based on z values.
    qsort(quads_start, (s1->rings)*(s1->points_per_ring), sizeof(struct quad_plane), compare_quads);

    //Draw the sorted quads.
    quads=quads_start;
    for(i=0;i<(s1->rings);i++)
      {
        for(j=0;j<(s1->points_per_ring);j++)
          {
            /*
              Standardize numbers for shading and draw. Change around for 
              different effects. https://en.wikipedia.org/wiki/Shading
            */
            distance=((max-(quads->z1))/(max-min))*5.0;
            distance=1.0/(1.0+(distance*distance));
            cairo_set_source_rgba(cr, 0.0, distance, quads->b, quads->a);
            cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
            cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
            cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
            cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
            cairo_close_path(cr);
            cairo_fill(cr);
            quads++;
            counter++;
          }
      }

    g_free(quads_start);    
  }
static void draw_text_ring(cairo_t *cr, gdouble w1, gdouble h1, gint points_per_ring, gpointer *rotations)
  {
    gint i=0; 
    gdouble yaw_r=((gdouble*)(rotations[0]))[0]*G_PI/180.0;
    gdouble roll_r=((gdouble*)(rotations[0]))[1]*G_PI/180.0;
    gdouble pitch_r=((gdouble*)(rotations[0]))[2]*G_PI/180.0;
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    cairo_set_font_size(cr, 26.0);
    gdouble qrs1[9];
    gdouble qrs2[9];
    gdouble qrs3[9];
    gdouble translate_x=0.0;
    gdouble translate_y=0.0;
    gdouble advance=0.0;

    quaternion_rotation(yaw_r, roll_r, pitch_r, qrs1);

    const struct shape *s1=get_drawing_from_shape_cache(TEXT_RING, 0, points_per_ring);   
    struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
    struct point3d *p2=&g_array_index(s1->sr, struct point3d, points_per_ring+1);
        
    for(i=0;i<(s1->points_per_ring);i++)
      {
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0); 
        cairo_move_to(cr, w1*(p1->x), h1*(p1->y));        
        cairo_line_to(cr, w1*(p2->x), h1*(p2->y));
        cairo_line_to(cr, w1*((p2+1)->x), h1*((p2+1)->y));
        cairo_line_to(cr, w1*((p1+1)->x), h1*((p1+1)->y));
        cairo_close_path(cr);
        cairo_stroke_preserve(cr);
        cairo_set_source_rgba(cr, 0.7, 0.7, 0.7, 0.4);
        cairo_fill(cr); 

        cairo_save(cr);
        advance=(i+1)*(2.0*G_PI/points_per_ring)+G_PI/2.0;
        quaternion_rotation(0.0, -advance, 0.0, qrs2);
        matrix_multiply_rotations(qrs2, qrs1, qrs3);
        translate_x=w1*((p1->x)+(p2->x)+((p2+1)->x)+((p1+1)->x))/4.0;
        translate_y=h1*((p1->y)+(p2->y)+((p2+1)->y)+((p1+1)->y))/4.0;
        cairo_matrix_init(&matrix1, qrs3[0], qrs3[3], qrs3[1], qrs3[4], translate_x, translate_y);
        cairo_transform(cr, &matrix1); 

        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        gchar *number=g_strdup_printf("%i", i);
        cairo_text_extents(cr, number, &extents);
        cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
        cairo_show_text(cr, number);
        g_free(number);
        cairo_restore(cr);

        p1++;p2++;      
      }
  }
int compare_planes(const void *a, const void *b)
  {
    return(((struct plane_order*)a)->value-((struct plane_order*)b)->value);
  }
int compare_quads(const void *a, const void *b)
  {
    return(  (((struct quad_plane*)a)->z1+((struct quad_plane*)a)->z2+((struct quad_plane*)a)->z3+((struct quad_plane*)a)->z4) - (((struct quad_plane*)b)->z1+((struct quad_plane*)b)->z2+((struct quad_plane*)b)->z3+((struct quad_plane*)b)->z4));
  }
int compare_quads_mesh(const void *a, const void *b)
  {
    return(  (((struct quad_plane_mesh*)a)->z1+((struct quad_plane_mesh*)a)->z2+((struct quad_plane_mesh*)a)->z3+((struct quad_plane_mesh*)a)->z4) - (((struct quad_plane_mesh*)b)->z1+((struct quad_plane_mesh*)b)->z2+((struct quad_plane_mesh*)b)->z3+((struct quad_plane_mesh*)b)->z4));
  }
/*
    This is some exellent work done by Ramsundar Shandilya. Note the following for the original work
    and the rational behind it.
    
    https://medium.com/@ramshandilya/draw-smooth-curves-through-a-set-of-points-in-ios-34f6d73c8f9

    https://github.com/Ramshandilya/Bezier

    The MIT License (MIT)

    Copyright (c) 2015 Ramsundar Shandilya

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
  
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

    This is a translation of the original Swift code to C. It makes the function easy to use with GTK+,
    cairo and glib.
*/
static GArray* control_points_from_coords2(const GArray *dataPoints)
  {  
    gint i=0;
    GArray *controlPoints=NULL;      
    //Number of Segments
    gint count=0;
    if(dataPoints!=NULL) count=dataPoints->len-1;
    gdouble *fCP=NULL;
    gdouble *sCP=NULL;

    if(count>0)
      {
        fCP=(gdouble*)g_malloc(2*count*sizeof(gdouble));
        sCP=(gdouble*)g_malloc(2*count*sizeof(gdouble));
      }
        
    //P0, P1, P2, P3 are the points for each segment, where P0 & P3 are the knots and P1, P2 are the control points.
    if(count<1||dataPoints==NULL)
      {
        //Return NULL.
        controlPoints=NULL;
        g_print("Can't get control points from coordinates. NULL returned.\n");
      }
    else if(count==1)
      {
        struct point P0=g_array_index(dataPoints, struct point, 0);
        struct point P3=g_array_index(dataPoints, struct point, 1);

        //Calculate First Control Point
        //3P1 = 2P0 + P3
        struct point P1;
        P1.x=(2.0*P0.x+P3.x)/3.0;
        P1.y=(2.0*P0.y+P3.y)/3.0;

        *(fCP)=P1.x;
        *(fCP+1)=P1.y;

        //Calculate second Control Point
        //P2 = 2P1 - P0
        struct point P2;
        P2.x=(2.0*P1.x-P0.x);
        P2.y=(2.0*P1.y-P0.x);

        *(sCP)=P2.x;
        *(sCP+1)=P2.y;      
      }
    else
      {
        gdouble *rhs=(gdouble*)g_malloc(2*count*sizeof(gdouble));
        gdouble *a=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *b=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *c=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble rhsValueX=0;
        gdouble rhsValueY=0;
        struct point P0;
        struct point P3;        
        gdouble m=0;
        gdouble b1=0;
        gdouble r2x=0;
        gdouble r2y=0;
        gdouble P1_x=0;
        gdouble P1_y=0;
   
        for(i=0;i<count;i++)
          {
            P0=g_array_index(dataPoints, struct point, i);
            P3=g_array_index(dataPoints, struct point, i+1);

            if(i==0)
              {
                *(a)=0.0;
                *(b)=2.0;
                *(c)=1.0;

                //rhs for first segment
                rhsValueX=P0.x+2.0*P3.x;
                rhsValueY=P0.y+2.0*P3.y;
              }
            else if(i==count-1)
              {
                *(a+i)=2.0;
                *(b+i)=7.0;
                *(c+i)=0.0;

                //rhs for last segment
                rhsValueX=8.0*P0.x+P3.x;
                rhsValueY=8.0*P0.y+P3.y;
              }
            else
              {
                *(a+i)=1.0;
                *(b+i)=4.0;
                *(c+i)=1.0;

                rhsValueX=4.0*P0.x+2.0*P3.x;
                rhsValueY=4.0*P0.y+2.0*P3.y;
              }
            *(rhs+i*2)=rhsValueX;
            *(rhs+i*2+1)=rhsValueY;
          }

        //Solve Ax=B. Use Tridiagonal matrix algorithm a.k.a Thomas Algorithm
        for(i=1;i<count;i++)
          {
            m=(*(a+i))/(*(b+i-1));

            b1=(*(b+i))-m*(*(c+i-1));
            *(b+i)=b1;

            r2x=(*(rhs+i*2))-m*(*(rhs+(i-1)*2));
            r2y=(*(rhs+i*2+1))-m*(*(rhs+(i-1)*2+1));

            *(rhs+i*2)=r2x;
            *(rhs+i*2+1)=r2y;
          }

        //Get First Control Points
        
        //Last control Point
        gdouble lastControlPointX=(*(rhs+2*count-2))/(*(b+count-1));
        gdouble lastControlPointY=(*(rhs+2*count-1))/(*(b+count-1));

        *(fCP+2*count-2)=lastControlPointX;
        *(fCP+2*count-1)=lastControlPointY;

        gdouble controlPointX=0;
        gdouble controlPointY=0;

        for(i=count-2;i>=0;--i)
          {
            controlPointX=(*(rhs+i*2)-(*(c+i))*(*(fCP+(i+1)*2)))/(*(b+i));
            controlPointY=(*(rhs+i*2+1)-(*(c+i))*(*(fCP+(i+1)*2+1)))/(*(b+i));

             *(fCP+i*2)=controlPointX;
             *(fCP+i*2+1)=controlPointY; 
          }

        //Compute second Control Points from first.
        for(i=0;i<count;i++)
          {
            if(i==count-1)
              {
                P3=g_array_index(dataPoints, struct point, i+1);
                P1_x=(*(fCP+i*2));
                P1_y=(*(fCP+i*2+1));

                controlPointX=(P3.x+P1_x)/2.0;
                controlPointY=(P3.y+P1_y)/2.0;

                *(sCP+count*2-2)=controlPointX;
                *(sCP+count*2-1)=controlPointY;
              }
            else
              {
                P3=g_array_index(dataPoints, struct point, i+1);                
                P1_x=(*(fCP+(i+1)*2));
                P1_y=(*(fCP+(i+1)*2+1));

                controlPointX=2.0*P3.x-P1_x;
                controlPointY=2.0*P3.y-P1_y;

                *(sCP+i*2)=controlPointX;
                *(sCP+i*2+1)=controlPointY;
              }

          }

        controlPoints=g_array_new(FALSE, FALSE, sizeof(struct controls));
        struct controls cp;
        for(i=0;i<count;i++)
          {
            cp.x1=(*(fCP+i*2));
            cp.y1=(*(fCP+i*2+1));
            cp.x2=(*(sCP+i*2));
            cp.y2=(*(sCP+i*2+1));
            g_array_append_val(controlPoints, cp);
          }

        g_free(rhs);
        g_free(a);
        g_free(b);
        g_free(c);
     }

    if(fCP!=NULL) g_free(fCP);
    if(sCP!=NULL) g_free(sCP);

    return controlPoints;
  }


