
/* 
    Figure out the relationship between a velocity vector and a rotating cube. Useful with
the boid drawings.

    gcc -Wall velocity1.c -o velocity1 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<stdlib.h>

struct quad{
  gint id;
  gdouble z;
};

//Initial vector.
gdouble vector[3]={4.0, -4.0, -4.0};

//Reference cube.
gdouble cube[24]={0.0, 20.0, 20.0,   0.0, 20.0, -20.0,   0.0, -20.0, -20.0,   0.0, -20.0, 20.0,
                  -40.0, 20.0, 20.0,   -40.0, 20.0, -20.0,   -40.0, -20.0, -20.0,   -40.0, -20.0, 20.0};
gdouble cube_rot[24];
gdouble cube_rot2[24];

//Can get the pitch and yaw from the velocity vector but need the roll also. Use a unit roll vector.
gdouble roll_ref[3]={1.0, 0.0, 0.0};
gdouble roll_rot[3];

//Combo ids for drawing options.
static gint rotate=0;

//The combo rotation slider values.
static gdouble combo_pitch=0.0;
static gdouble combo_roll=0.0;
static gdouble combo_yaw=0.0;

//Tick id for animation frame clock.
static guint tick_id=0;

//For the performance check boxes.
static gboolean frame_rate=FALSE;
static gboolean function_time=FALSE;

//Draw the main window transparent.
static gboolean draw_main_window(GtkWidget *window, cairo_t *cr, gpointer data);
//UI functions for sliders and combo boxes.
static void show_frame_rate(GtkToggleButton *button, gpointer data);
static void show_function_time(GtkToggleButton *button, gpointer data);
static void rotate_drawing(GtkComboBox *combo, gpointer data);
static void set_vx(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_vy(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_vz(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data);
//The start of drawing.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer data);
//Rotation functions.
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9]);
static void rotate_cube(gdouble qrs[9]);
static void rotate_cube2(gdouble qrs[9]);
static void rotate_roll_reference(gdouble qrs[9]);
static void draw_cube2(cairo_t *cr, gdouble scale);
int compare_quads(const void *a, const void *b);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Yellow Velocity Vector");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    gtk_widget_set_app_paintable(window, TRUE);
    //Try to set transparency of main window.
    if(gtk_widget_is_composited(window))
      {
        GdkScreen *screen=gtk_widget_get_screen(window);  
        GdkVisual *visual=gdk_screen_get_rgba_visual(screen);
        gtk_widget_set_visual(window, visual);
      }
    else g_print("Can't set window transparency.\n");

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);

    GtkWidget *combo_rotate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_rotate, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 0, "1", "Draw Vector");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 1, "2", "Animate Vector");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_rotate), 0);

    GtkWidget *vx_label=gtk_label_new("Vx");
    gtk_widget_set_hexpand(vx_label, TRUE);

    GtkWidget *vy_label=gtk_label_new("Vy");
    gtk_widget_set_hexpand(vy_label, TRUE);

    GtkWidget *vz_label=gtk_label_new("Vz");
    gtk_widget_set_hexpand(vz_label, TRUE);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);
    
    GtkWidget *vx_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, -4.0, 4.0, 0.1);
    gtk_widget_set_vexpand(vx_slider, TRUE);
    gtk_range_set_value(GTK_RANGE(vx_slider), 4.0);

    GtkWidget *vy_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, -4.0, 4.0, 0.1);
    gtk_widget_set_vexpand(vy_slider, TRUE);

    GtkWidget *vz_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, -4.0, 4.0, 0.1);
    gtk_widget_set_vexpand(vz_slider, TRUE);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider, TRUE);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider, TRUE);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider, TRUE);

    g_signal_connect(da, "draw", G_CALLBACK(draw_main), NULL);
    g_signal_connect(combo_rotate, "changed", G_CALLBACK(rotate_drawing), da);
    g_signal_connect(vx_slider, "change-value", G_CALLBACK(set_vx), da);   
    g_signal_connect(vy_slider, "change-value", G_CALLBACK(set_vy), da);
    g_signal_connect(vz_slider, "change-value", G_CALLBACK(set_vz), da);
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_pitch), da);
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_roll), da);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_yaw), da);

    //Some checks for performance.
    GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
    gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
    g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), NULL);

    GtkWidget *check_functions=gtk_check_button_new_with_label("Show Function Time");
    gtk_widget_set_halign(check_functions, GTK_ALIGN_CENTER);
    g_signal_connect(check_functions, "toggled", G_CALLBACK(show_function_time), NULL);

    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_rotate, 0, 0, 6, 1);    
    gtk_grid_attach(GTK_GRID(grid1), vx_label, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), vy_label, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), vz_label, 2, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 3, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 4, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 5, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), vx_slider, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), vy_slider, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), vz_slider, 2, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 3, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 4, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 5, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_frame, 0, 3, 6, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_functions, 0, 4, 6, 1);

    GtkWidget *scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll, TRUE);
    gtk_widget_set_vexpand(scroll, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll), grid1);
   
    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), scroll, TRUE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 250);

    g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), paned1);
   
    gtk_container_add(GTK_CONTAINER(window), paned1);
    
    gtk_widget_show_all(window);

    gtk_main();

    return 0;  
  }
static gboolean draw_main_window(GtkWidget *window, cairo_t *cr, gpointer data)
  {
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.3);
    cairo_paint(cr);

    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
    gint width=gtk_paned_get_position(GTK_PANED(data));
    gint height=gtk_widget_get_allocated_height(window);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_rectangle(cr, width, 0.0, 10, height);
    cairo_fill(cr);

    return FALSE;
  }
static void show_frame_rate(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_function_time(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) function_time=TRUE;
    else function_time=FALSE;
  }
static void rotate_drawing(GtkComboBox *combo, gpointer data)
  {
    //Reset rotations for animation.
    vector[0]=4.0;
    vector[1]=-4.0;
    vector[2]=-4.0;
    
    rotate=gtk_combo_box_get_active(combo);
    if(rotate==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(data), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(data));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(data), (GtkTickCallback)animate_drawing, NULL, NULL);
          }
      }
    
  }
static void set_vx(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    vector[0]=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_vy(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    vector[1]=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_vz(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    vector[2]=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_pitch=value*G_PI/180.0;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_roll=value*G_PI/180.0;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_yaw=value*G_PI/180.0;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data)
  {
    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    //Apply angles of rotation for animation.
    if(vector[0]<4.0) vector[0]+=0.01;
    else vector[0]=-4.0;
    if(vector[1]<4.0) vector[1]+=0.03;
    else vector[1]=-4.0;
    if(vector[2]<4.0) vector[2]+=0.04;
    else vector[2]=-4.0;

    gtk_widget_queue_draw(GTK_WIDGET(drawing));
    return G_SOURCE_CONTINUE;
  }
//The top drawing function.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;

    //Paint background transparent.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.0);
    cairo_paint(cr);

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 3.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Draw in the center
    cairo_translate(cr, width/2.0, height/2.0);
    cairo_set_line_cap(cr, CAIRO_LINE_JOIN_ROUND);      
 
    GTimer *timer=g_timer_new();
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    gdouble yaw=atan(vector[1]/vector[0]);
    gdouble pitch=atan(vector[2]/sqrt(vector[0]*vector[0]+vector[1]*vector[1]));
    gdouble qrs[9];
    //Keep the reference line in the front
    if(vector[0]>0.0) quaternion_rotation(yaw, 0.0, pitch, qrs);
    else quaternion_rotation(yaw+G_PI, 0.0, pitch-G_PI/2.0, qrs);
    rotate_cube(qrs);

    //Rotate the reference vector.
    rotate_roll_reference(qrs);
    
    //Rotate the cube by itself.
    quaternion_rotation(combo_yaw, combo_roll, combo_pitch, qrs);                 
    rotate_cube2(qrs);

    cairo_set_line_width(cr, 2.0);
    draw_cube2(cr, 3.0);

    //Roll angle lines.
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    cairo_set_line_width(cr, 12.0);
    cairo_move_to(cr, 0.0, 0.0);
    cairo_line_to(cr, 200.0*roll_ref[0], 200.0*roll_ref[1]);
    cairo_stroke(cr);
    cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
    cairo_set_line_width(cr, 8.0);
    cairo_move_to(cr, 0.0, 0.0);
    cairo_line_to(cr, 200.0*roll_rot[0], 200.0*roll_rot[1]);
    cairo_stroke(cr);

    //Draw the direction vector.
    cairo_set_line_width(cr, 5.0);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_move_to(cr, 0.0, 0.0);
    cairo_line_to(cr, 30.0*vector[0], 30.0*vector[1]);
    cairo_stroke(cr);
    
    if(function_time) g_print("Draw time %f\n", g_timer_elapsed(timer, NULL));
    g_timer_destroy(timer);
     
    return FALSE;
  }
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    GTimer *timer1=g_timer_new();
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);

    if(function_time) g_print("Quaternion time %f\n", g_timer_elapsed(timer1, NULL));
    g_timer_destroy(timer1);
  }
static void rotate_cube(gdouble qrs[9])
  {
    gint i=0;
    for(i=0;i<8;i++)
      {        
        cube_rot[3*i]=(cube[3*i]*qrs[0])+(cube[3*i+1]*qrs[1])+(cube[3*i+2]*qrs[2]);
        cube_rot[3*i+1]=(cube[3*i]*qrs[3])+(cube[3*i+1]*qrs[4])+(cube[3*i+2]*qrs[5]);
        cube_rot[3*i+2]=(cube[3*i]*qrs[6])+(cube[3*i+1]*qrs[7])+(cube[3*i+2]*qrs[8]);
      }
  }
static void rotate_cube2(gdouble qrs[9])
  {
    gint i=0;
    for(i=0;i<8;i++)
      {        
        cube_rot2[3*i]=(cube_rot[3*i]*qrs[0])+(cube_rot[3*i+1]*qrs[1])+(cube_rot[3*i+2]*qrs[2]);
        cube_rot2[3*i+1]=(cube_rot[3*i]*qrs[3])+(cube_rot[3*i+1]*qrs[4])+(cube_rot[3*i+2]*qrs[5]);
        cube_rot2[3*i+2]=(cube_rot[3*i]*qrs[6])+(cube_rot[3*i+1]*qrs[7])+(cube_rot[3*i+2]*qrs[8]);
      }
  }
static void rotate_roll_reference(gdouble qrs[9])
  {
    roll_rot[0]=(roll_ref[0]*qrs[0])+(roll_ref[1]*qrs[1])+(roll_ref[2]*qrs[2]);
    roll_rot[1]=(roll_ref[0]*qrs[3])+(roll_ref[1]*qrs[4])+(roll_ref[2]*qrs[5]);
    roll_rot[2]=(roll_ref[0]*qrs[6])+(roll_ref[1]*qrs[7])+(roll_ref[2]*qrs[8]);
  }
static void draw_cube2(cairo_t *cr, gdouble scale)
  {
    gint i=0;
    //An array to sort quads.
    struct quad *quads_start=g_malloc(6*sizeof(struct quad));
    struct quad *quads=quads_start;
    
    (*quads).id=0;
    (*quads).z=cube_rot2[2]+cube_rot2[5]+cube_rot2[8]+cube_rot2[11];
    quads++;
    (*quads).id=1;
    (*quads).z=cube_rot2[14]+cube_rot2[17]+cube_rot2[20]+cube_rot2[23];
    quads++;
    (*quads).id=2;
    (*quads).z=cube_rot2[2]+cube_rot2[5]+cube_rot2[17]+cube_rot2[14];
    quads++;
    (*quads).id=3;
    (*quads).z=cube_rot2[5]+cube_rot2[8]+cube_rot2[20]+cube_rot2[17];
    quads++;
    (*quads).id=4;
    (*quads).z=cube_rot2[8]+cube_rot2[11]+cube_rot2[23]+cube_rot2[20];
    quads++;
    (*quads).id=5;
    (*quads).z=cube_rot2[11]+cube_rot2[2]+cube_rot2[14]+cube_rot2[23];
    quads++;

    //Sort array based on z values.
    qsort(quads_start, 6, sizeof(struct quad), compare_quads);

    //Just draw the top three cube planes.
    quads=quads_start;
    for(i=0;i<6;i++)
      {
        if((*quads).id==0)
          {
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.5);
            cairo_move_to(cr, cube_rot2[0]*scale, cube_rot2[1]*scale);
            cairo_line_to(cr, cube_rot2[3]*scale, cube_rot2[4]*scale);
            cairo_line_to(cr, cube_rot2[6]*scale, cube_rot2[7]*scale);
            cairo_line_to(cr, cube_rot2[9]*scale, cube_rot2[10]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else if((*quads).id==1)
          {
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.5);
            cairo_move_to(cr, cube_rot2[12]*scale, cube_rot2[13]*scale);
            cairo_line_to(cr, cube_rot2[15]*scale, cube_rot2[16]*scale);
            cairo_line_to(cr, cube_rot2[18]*scale, cube_rot2[19]*scale);
            cairo_line_to(cr, cube_rot2[21]*scale, cube_rot2[22]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else if((*quads).id==2)
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, cube_rot2[0]*scale, cube_rot2[1]*scale);
            cairo_line_to(cr, cube_rot2[3]*scale, cube_rot2[4]*scale);
            cairo_line_to(cr, cube_rot2[15]*scale, cube_rot2[16]*scale);
            cairo_line_to(cr, cube_rot2[12]*scale, cube_rot2[13]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else if((*quads).id==3)
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, cube_rot2[3]*scale, cube_rot2[4]*scale);
            cairo_line_to(cr, cube_rot2[6]*scale, cube_rot2[7]*scale);
            cairo_line_to(cr, cube_rot2[18]*scale, cube_rot2[19]*scale);
            cairo_line_to(cr, cube_rot2[15]*scale, cube_rot2[16]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        //Add a line for top of cube.
        else if((*quads).id==4)
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, cube_rot2[6]*scale, cube_rot2[7]*scale);
            cairo_line_to(cr, cube_rot2[9]*scale, cube_rot2[10]*scale);
            cairo_line_to(cr, cube_rot2[21]*scale, cube_rot2[22]*scale);
            cairo_line_to(cr, cube_rot2[18]*scale, cube_rot2[19]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
            cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
            cairo_move_to(cr, ((cube_rot2[6]*scale)+(cube_rot2[9]*scale))/2.0, ((cube_rot2[7]*scale)+(cube_rot2[10]*scale))/2.0);
            cairo_line_to(cr, ((cube_rot2[21]*scale)+(cube_rot2[18]*scale))/2.0, ((cube_rot2[22]*scale)+(cube_rot2[19]*scale))/2.0);
            cairo_stroke(cr);
          }
        else
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, cube_rot2[9]*scale, cube_rot2[10]*scale);
            cairo_line_to(cr, cube_rot2[0]*scale, cube_rot2[1]*scale);
            cairo_line_to(cr, cube_rot2[12]*scale, cube_rot2[13]*scale);
            cairo_line_to(cr, cube_rot2[21]*scale, cube_rot2[22]*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        quads++;
        }

    g_free(quads_start);  
  }
int compare_quads(const void *a, const void *b)
  {
    return(  (((struct quad*)a)->z) - (((struct quad*)b)->z) );
  }
