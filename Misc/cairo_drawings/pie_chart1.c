
/*   
    This is the continuation of bezier_ring1.c. This one puts the pie chart in a struct. The
pie chart can be drawn as a ring also.

    gcc -Wall pie_chart1.c -o pie_chart1 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<string.h>
#include<math.h>
#include<cairo-svg.h>
#include<cairo-pdf.h>
#include<cairo-ps.h>

struct point3d{
  gdouble x;
  gdouble y;
  gdouble z;
};

struct arc{
  struct point3d a1;
  struct point3d a2;
  struct point3d a3;
  struct point3d a4;
};

//A quad that can be sorted by point3d z-values.
struct quad{
  struct point3d a;
  struct point3d b;
  struct point3d c;
  struct point3d d;
  gdouble color[4];
  guint section_id;
  gdouble section_explode;
};

//A section of pie.
struct section{
  guint section_id;
  gint start;
  gint end;
  gdouble color[4];
  gdouble section_explode;
};

struct pie_chart{
  guint pieces;
  gdouble radius;
  //The rings of the pie chart
  GArray *pie_chart1;
  GArray *pie_chart1_t;
  GArray *pie_chart2;
  GArray *pie_chart2_t;
  //Array of vectors to move the pie sections along.
  GArray *v_pie;
  GArray *v_pie_t;
  //Array for the location of the pie chart sections.
  GArray *sections;
  //Reference vector for top of the ring or pie chart.
  gdouble top[3];
  gdouble top_t[3];
  //The depth of the pie chart from (0,0,0). The total z distance in 2*depth.
  gdouble depth;
  gboolean outline;
};

//Draw label lines with the pie chart.
enum{ 
  NO_LABEL_LINES,
  ADD_LABEL_LINES
};

//Clock number locations.
GArray *pie_clock=NULL;

static gint drawing_id=0;
static gdouble pitch=0.0;
static gdouble roll=0.0;
static gdouble yaw=0.0;
static gdouble scale=1.0;
static gint section_index=0;
static guint chart_pieces=60;
static guint chart_sections=1;
static gdouble chart_depth=20.0;
static gdouble explode_pieces=0.0;
static gint section_id=0;
static gdouble section_rgba[4]={0.0, 0.0, 0.0, 1.0};
static gint sweep=1;
static gdouble pie_explode=0.0;
static gdouble move_ring=0.25;
static guint tick_id=0;
static guint timer_id=0;
static guint draw_signal_id=0;
static gboolean frame_rate=FALSE;
static gboolean time_it=FALSE;
static guint label_lines_g=0;

//Set some variables from the UI.
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data);
static void set_animate_drawing(GtkComboBox *combo, gpointer *charts);
static void set_drawing_id(GtkComboBox *combo, gpointer *charts);
static void set_alpha(GtkComboBox *combo, gpointer *charts);
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void index_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void red_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void green_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void blue_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void alpha_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void depth_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void set_depth(GtkWidget *button, gpointer *charts);
static void set_section_color(GtkWidget *button, gpointer *charts);
static void pieces_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void create_new_chart(GtkWidget *button, gpointer *charts);
static void sections_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void create_new_section(GtkWidget *button, gpointer *charts);
static void explode_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void section_id_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void explode_all_sections(GtkWidget *button, gpointer *charts);
static void explode_single_section(GtkWidget *button, gpointer *charts);
static void show_frame_rate(GtkToggleButton *check_frame, gpointer data);
static void show_drawing_time(GtkToggleButton *check_time, gpointer data);
static void draw_label_lines(GtkToggleButton *check, gpointer data);
static void draw_svg(GtkWidget *widget, gpointer *charts);
static void draw_pdf(GtkWidget *widget, gpointer *charts);
static void draw_ps(GtkWidget *widget, gpointer *charts);
static void draw_surface(cairo_t *cr, gdouble width, gdouble height, gpointer *data);
//Initialize some points for the clock numbers.
static void initialize_pie_clock();
//Basic pie chart functions.
static struct pie_chart* pie_chart_new(guint pieces, gdouble radius, gdouble depth);
static void pie_chart_append_section(struct pie_chart *pc, guint end, gdouble rgba[4]);
static void pie_chart_clear_sections(struct pie_chart *pc);
static void pie_chart_explode_section(struct pie_chart *pc, gint section_num, gdouble radius);
static void pie_chart_explode_all(struct pie_chart *pc, gdouble radius);
static void pie_chart_set_z(struct pie_chart *pc, gdouble z);
static void pie_chart_free(struct pie_chart *pc);
static void initialize_bezier_ring(GArray **ring, GArray **ring_t, guint pieces, gdouble radius, gdouble z);
//Pie chart draw functions.
static gboolean redraw_clock(gpointer da);
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer *charts);
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer *charts);
static void draw_pie_chart(GtkWidget *da, cairo_t *cr, struct pie_chart *pc, guint label_lines);
static void draw_pie_clock(GtkWidget *da, cairo_t *cr, struct pie_chart *pc, gdouble qrs[9]);
static void draw_pie_chart_ring(GtkWidget *da, cairo_t *cr, struct pie_chart *pc, gdouble inside_ring, guint label_lines);
//3d transform functions.
static void reset_ring(GArray *ring, GArray *ring_t);
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9]);
static void rotate_reference_vector(struct pie_chart *pc, gdouble qrs[9]);
static void rotate_pie_reference_vectors(struct pie_chart *pc, gdouble qrs[9]);
static void rotate_ring(GArray *ring, gdouble qrs[9]);
//A sort function for the inside quads.
int compare_quads(const void *q1, const void *q2);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Pie Chart3d");
    gtk_window_set_default_size(GTK_WINDOW(window), 875, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_widget_set_app_paintable(window, TRUE);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    initialize_pie_clock();

    //Initialize a pie chart with 60 pieces.
    gdouble rgba[4]={1.0, 0.0, 0.0, 1.0};
    struct pie_chart *pc1=pie_chart_new(60, 150.0, 20.0);
    /*
      Need to fill sections of the chart in order. The append function looks at the previous 
      section for the start of the new section.
    */
    rgba[0]=1.0;rgba[1]=0.0;rgba[2]=0.0;rgba[3]=0.4;
    pie_chart_append_section(pc1, 9, rgba);
    rgba[0]=0.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.4;
    pie_chart_append_section(pc1, 29, rgba);
    rgba[0]=0.0;rgba[1]=0.0;rgba[2]=1.0;rgba[3]=0.4;
    pie_chart_append_section(pc1, 39, rgba);
    rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.4;
    pie_chart_append_section(pc1, 49, rgba);
    rgba[0]=1.0;rgba[1]=0.0;rgba[2]=1.0;rgba[3]=0.4;
    pie_chart_append_section(pc1, 59, rgba);  

    struct pie_chart *pc2=pie_chart_new(60, 150.0, 20.0);
    rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.8;
    pie_chart_append_section(pc2, 1, rgba);
    rgba[0]=0.0;rgba[1]=0.0;rgba[2]=1.0;rgba[3]=0.8;
    pie_chart_append_section(pc2, 59, rgba); 

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);
    gpointer charts[3]={pc1, pc2, da}; 
    draw_signal_id=g_signal_connect(da, "draw", G_CALLBACK(draw_main), charts); 

    GtkWidget *combo_animate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_animate, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_animate), 0, "1", "Draw Shape");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_animate), 1, "2", "Animate Shape");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_animate), 0);
    g_signal_connect(combo_animate, "changed", G_CALLBACK(set_animate_drawing), charts);

    GtkWidget *combo_drawing=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_drawing, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "1", "Pie Chart3d");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "2", "Pie Chart3dx4");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 2, "3", "Pie Chart Clock");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 3, "4", "Pie Chart Sweep");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 4, "5", "Pie Chart Ring");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 5, "6", "Pie Chart Ring Sweep");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
    g_signal_connect(combo_drawing, "changed", G_CALLBACK(set_drawing_id), charts);

    //Set alpha on pc1.
    GtkWidget *combo_alpha=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_alpha, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_alpha), 0, "1", "alpha 0.4");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_alpha), 1, "2", "alpha 0.6");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_alpha), 2, "3", "alpha 0.8");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_alpha), 3, "4", "alpha 1.0");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_alpha), 0);
    g_signal_connect(combo_alpha, "changed", G_CALLBACK(set_alpha), charts);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);

    GtkWidget *scale_label=gtk_label_new("Scale");
    gtk_widget_set_hexpand(scale_label, TRUE);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider, TRUE);
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_pitch), da);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider, TRUE);
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_roll), da);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider, TRUE);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_yaw), da); 

    GtkWidget *scale_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 2.0, 0.01);
    gtk_widget_set_vexpand(scale_slider, TRUE);
    gtk_range_set_value(GTK_RANGE(scale_slider), 1.0);
    g_signal_connect(scale_slider, "change-value", G_CALLBACK(set_scale), da); 

    GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
    gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
    g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), NULL);

    GtkWidget *check_time=gtk_check_button_new_with_label("Time Drawing");
    gtk_widget_set_halign(check_time, GTK_ALIGN_CENTER);
    g_signal_connect(check_time, "toggled", G_CALLBACK(show_drawing_time), NULL);

    GtkWidget *menu1=gtk_menu_new();
    GtkWidget *menu1item1=gtk_menu_item_new_with_label("pie1.svg");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item1);
    GtkWidget *menu1item2=gtk_menu_item_new_with_label("pie1.pdf");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item2);
    GtkWidget *menu1item3=gtk_menu_item_new_with_label("pie1.ps");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item3);
    GtkWidget *title1=gtk_menu_item_new_with_label("Test Output");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(title1), menu1);

    GtkWidget *menu_bar=gtk_menu_bar_new();
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), title1);
 
    g_signal_connect(menu1item1, "activate", G_CALLBACK(draw_svg), charts);
    g_signal_connect(menu1item2, "activate", G_CALLBACK(draw_pdf), charts);
    g_signal_connect(menu1item3, "activate", G_CALLBACK(draw_ps), charts);
   
    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 10);
    gtk_grid_attach(GTK_GRID(grid1), combo_animate, 0, 0, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_drawing, 0, 1, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_alpha, 0, 2, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale_label, 3, 3, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale_slider, 3, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_frame, 0, 5, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_time, 0, 6, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), menu_bar, 0, 7, 4, 1);

    //The second tab in the notebook.
    GtkAdjustment *index_adj=gtk_adjustment_new(0.0, 0.0, 60.0, 1.0, 0.0, 0.0);
    GtkAdjustment *red_adj=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *green_adj=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *blue_adj=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *alpha_adj=gtk_adjustment_new(1.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *depth_adj=gtk_adjustment_new(20.0, 0.0, 100.0, 1.0, 0.0, 0.0);
    GtkAdjustment *pieces_adj=gtk_adjustment_new(60.0, 40.0, 120.0, 1.0, 0.0, 0.0);
    GtkAdjustment *sections_adj=gtk_adjustment_new(1.0, 1.0, 120.0, 1.0, 0.0, 0.0);

    GtkWidget *index_label=gtk_label_new("Section Index");
    GtkWidget *index_spin=gtk_spin_button_new(index_adj, 1, 0);
    g_signal_connect(index_spin, "value-changed", G_CALLBACK(index_spin_changed), NULL);

    GtkWidget *rgba_label=gtk_label_new("RGBA");
    GtkWidget *red_spin=gtk_spin_button_new(red_adj, 0.01, 2);
    g_signal_connect(red_spin, "value-changed", G_CALLBACK(red_spin_changed), NULL);

    GtkWidget *green_spin=gtk_spin_button_new(green_adj, 0.01, 2);
    g_signal_connect(green_spin, "value-changed", G_CALLBACK(green_spin_changed), NULL);

    GtkWidget *blue_spin=gtk_spin_button_new(blue_adj, 0.01, 2);
    g_signal_connect(blue_spin, "value-changed", G_CALLBACK(blue_spin_changed), NULL);

    GtkWidget *alpha_spin=gtk_spin_button_new(alpha_adj, 0.01, 2);
    g_signal_connect(alpha_spin, "value-changed", G_CALLBACK(alpha_spin_changed), NULL);

    GtkWidget *depth_label=gtk_label_new("Depth");
    GtkWidget *depth_spin=gtk_spin_button_new(depth_adj, 20.0, 1);
    g_signal_connect(depth_spin, "value-changed", G_CALLBACK(depth_spin_changed), NULL);

    GtkWidget *depth_button=gtk_button_new_with_label("Set Depth");
    g_signal_connect(depth_button, "clicked", G_CALLBACK(set_depth), charts);

    GtkWidget *section_color_button=gtk_button_new_with_label("Update Section Color");
    g_signal_connect(section_color_button, "clicked", G_CALLBACK(set_section_color), charts);

    GtkWidget *pieces_label=gtk_label_new("Pie Chart Pieces");
    GtkWidget *pieces_spin=gtk_spin_button_new(pieces_adj, 60.0, 0.0);
    g_signal_connect(pieces_spin, "value-changed", G_CALLBACK(pieces_spin_changed), NULL);

    GtkWidget *new_pie_button=gtk_button_new_with_label("New Pie Chart");
    g_signal_connect(new_pie_button, "clicked", G_CALLBACK(create_new_chart), charts);

    GtkWidget *sections_label=gtk_label_new("Section Size");
    GtkWidget *sections_spin=gtk_spin_button_new(sections_adj, 1.0, 0.0);
    g_signal_connect(sections_spin, "value-changed", G_CALLBACK(sections_spin_changed), NULL);

    GtkWidget *new_section_button=gtk_button_new_with_label("New Section");
    g_signal_connect(new_section_button, "clicked", G_CALLBACK(create_new_section), charts);

    GtkWidget *label_lines=gtk_check_button_new_with_label("Label Lines");
    gtk_widget_set_halign(label_lines, GTK_ALIGN_CENTER);
    g_signal_connect(label_lines, "toggled", G_CALLBACK(draw_label_lines), da);

    GtkWidget *grid2=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid2), 8);
    gtk_grid_set_row_spacing(GTK_GRID(grid2), 8);
    gtk_grid_attach(GTK_GRID(grid2), index_label, 0, 0, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid2), index_spin, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), rgba_label, 0, 2, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid2), red_spin, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), green_spin, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), blue_spin, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), alpha_spin, 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), section_color_button, 1, 5, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid2), depth_label, 0, 6, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), depth_spin, 0, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), depth_button, 1, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), pieces_label, 0, 8, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), pieces_spin, 0, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), new_pie_button, 1, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), sections_label, 0, 10, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), sections_spin, 0, 11, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), new_section_button, 1, 11, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), label_lines, 0, 12, 2, 1);

    GtkAdjustment *explode_adj=gtk_adjustment_new(0.0, 0.0, 100.0, 1.0, 0.0, 0.0);
    GtkAdjustment *section_id_adj=gtk_adjustment_new(0.0, 0.0, 120.0, 1.0, 0.0, 0.0);

    GtkWidget *explode_label=gtk_label_new("Explode");
    GtkWidget *explode_spin=gtk_spin_button_new(explode_adj, 0.0, 0.0);
    g_signal_connect(explode_spin, "value-changed", G_CALLBACK(explode_spin_changed), NULL);

    GtkWidget *section_id_label=gtk_label_new("Section ID");
    GtkWidget *section_id_spin=gtk_spin_button_new(section_id_adj, 1.0, 0.0);
    g_signal_connect(section_id_spin, "value-changed", G_CALLBACK(section_id_spin_changed), NULL);

    GtkWidget *explode_button=gtk_button_new_with_label("Explode ALL");
    g_signal_connect(explode_button, "clicked", G_CALLBACK(explode_all_sections), charts);

    GtkWidget *single_section_button=gtk_button_new_with_label("Explode Section");
    g_signal_connect(single_section_button, "clicked", G_CALLBACK(explode_single_section), charts);

    GtkWidget *grid3=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid3), 8);
    gtk_grid_set_row_spacing(GTK_GRID(grid3), 8);
    gtk_grid_attach(GTK_GRID(grid3), explode_label, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), section_id_label, 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), explode_spin, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), section_id_spin, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), explode_button, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), single_section_button, 1, 2, 1, 1);

    GtkWidget *nb_label1=gtk_label_new("Drawing");
    GtkWidget *nb_label2=gtk_label_new("Options");
    GtkWidget *nb_label3=gtk_label_new("Options2");
    GtkWidget *notebook=gtk_notebook_new();
    gtk_container_set_border_width(GTK_CONTAINER(notebook), 15);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), grid1, nb_label1);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), grid2, nb_label2);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), grid3, nb_label3);

    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), notebook, FALSE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 350);

    //Draw background window based on the paned window splitter.
    g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), paned1);
   
    gtk_container_add(GTK_CONTAINER(window), paned1);

    gtk_widget_show_all(window);

    gtk_main();

    //Clean up.
    g_array_free(pie_clock, TRUE);
    //This chart may have been changed.
    pie_chart_free((struct pie_chart*)charts[0]);
    pie_chart_free(pc2);

    return 0;  
  }
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data)
  {
    //Paint background of drawing area.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr);
    //Paint the background under the grid.
    cairo_set_source_rgba(cr, 0.0, 0.5, 1.0, 0.5);
    gint width=gtk_paned_get_position(GTK_PANED(data));
    gint height=gtk_widget_get_allocated_height(widget);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_rectangle(cr, width, 0.0, 10, height);
    cairo_fill(cr);
    return FALSE;
  }
static void set_animate_drawing(GtkComboBox *combo, gpointer *charts)
  {   
    pitch=0.0;
    roll=0.0;
    yaw=0.0;
    struct pie_chart *pc=charts[0];
    GtkWidget *da=charts[2];

    //Reset pie chart.
    pie_chart_explode_all(pc, 0.0);
    pie_explode=0.0;
 
    if(gtk_combo_box_get_active(combo)==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(da), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(da));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(da), (GtkTickCallback)animate_drawing, charts, NULL);
          }
      }
    
  }
static void set_drawing_id(GtkComboBox *combo, gpointer *charts)
  {  
    drawing_id=gtk_combo_box_get_active(combo);
    struct pie_chart *pc=charts[0];
    GtkWidget *da=charts[2];

    //Reset pie chart.
    pie_chart_explode_all(pc, 0.0);
    pie_explode=0.0;

    if(drawing_id==2&&timer_id==0)
      {
        timer_id=g_timeout_add_seconds(3, (GSourceFunc)redraw_clock, da);
      }
    else if(timer_id!=0)
      {
        g_source_remove(timer_id);
        timer_id=0;
      }

    gtk_widget_queue_draw(GTK_WIDGET(da));
  }
static void set_alpha(GtkComboBox *combo, gpointer *charts)
  {
    gint i=0;
    gdouble alpha=0.0;
    gint combo_id=gtk_combo_box_get_active(combo);
    struct section *s1=&g_array_index(((struct pie_chart*)(charts[0]))->sections, struct section, 0);

    if(combo_id==0) alpha=0.4;
    else if(combo_id==1) alpha=0.6;
    else if(combo_id==2) alpha=0.8;
    else alpha=1.0;

    for(i=0;i<(((struct pie_chart*)(charts[0]))->sections->len);i++)
      {
        s1->color[3]=alpha;
        s1++;
      }

    gtk_widget_queue_draw(GTK_WIDGET(charts[2]));
  }
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    pitch=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    roll=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    yaw=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>2.0) scale=2.0;
    else if(value<0.0) scale=0.0;
    else scale=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void index_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    section_index=gtk_spin_button_get_value_as_int(spin_button);
  }
static void red_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    section_rgba[0]=gtk_spin_button_get_value(spin_button);
  }
static void green_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    section_rgba[1]=gtk_spin_button_get_value(spin_button);
  }
static void blue_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    section_rgba[2]=gtk_spin_button_get_value(spin_button);
  }
static void alpha_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    section_rgba[3]=gtk_spin_button_get_value(spin_button);
  }
static void depth_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_depth=gtk_spin_button_get_value(spin_button);
  }
static void set_depth(GtkWidget *button, gpointer *charts)
  {
    struct pie_chart *pc=charts[0];
    GtkWidget *da=charts[2];

    if(chart_depth<0.0)
      {
        g_warning("Chart depth is greater than 0.0.. The depth wasn't set.\n");
      }
    else
      {
        //Block the draw and tick when updating the array.
        gboolean was_ticking=FALSE;
        g_signal_handler_block(da, draw_signal_id);
        if(tick_id!=0)
          {
            gtk_widget_remove_tick_callback(GTK_WIDGET(da), tick_id);
            tick_id=0;
            was_ticking=TRUE;
          }

        pie_chart_set_z(pc, chart_depth);

        g_signal_handler_unblock(da, draw_signal_id);
        if(was_ticking)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(da), (GtkTickCallback)animate_drawing, charts, NULL);
          }

        gtk_widget_queue_draw(GTK_WIDGET(da));
      }
  }
static void set_section_color(GtkWidget *button, gpointer *charts)
  {
    struct pie_chart *pc=charts[0];
    GtkWidget *da=charts[2];

    if(section_index<0||section_index>(pc->sections->len)-1)
      {
        g_warning("Section index out of range. The color wasn't set.\n");
      }
    else
      {
        //Block the draw and tick when updating the array.
        gboolean was_ticking=FALSE;
        g_signal_handler_block(da, draw_signal_id);
        if(tick_id!=0)
          {
            gtk_widget_remove_tick_callback(GTK_WIDGET(da), tick_id);
            tick_id=0;
            was_ticking=TRUE;
          }

        struct section *s1=&g_array_index(pc->sections, struct section, section_index);
        s1->color[0]=section_rgba[0];s1->color[1]=section_rgba[1];
        s1->color[2]=section_rgba[2];s1->color[3]=section_rgba[3];

        g_signal_handler_unblock(da, draw_signal_id);
        if(was_ticking)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(da), (GtkTickCallback)animate_drawing, charts, NULL);
          }

        gtk_widget_queue_draw(GTK_WIDGET(da));
      }
  }
static void pieces_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_pieces=gtk_spin_button_get_value(spin_button);
  }
static void create_new_chart(GtkWidget *button, gpointer *charts)
  {
    struct pie_chart *pc1=charts[0];
    GtkWidget *da=charts[2];
    gboolean was_ticking=FALSE;

    //Block the draw and tick when updating the array.
    g_signal_handler_block(da, draw_signal_id);
    if(tick_id!=0)
      {
        gtk_widget_remove_tick_callback(GTK_WIDGET(da), tick_id);
        tick_id=0;
        was_ticking=TRUE;
      }

    //Free the previous chart and cylinders.
    if(pc1!=NULL)
      {
        pie_chart_free(pc1);
      }

    //Get a new pie chart.    
    charts[0]=pie_chart_new(chart_pieces, 150.0, 20.0);

    g_signal_handler_unblock(da, draw_signal_id);
    if(was_ticking)
      {
        tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(da), (GtkTickCallback)animate_drawing, charts, NULL);
      }

    gtk_widget_queue_draw(GTK_WIDGET(da));
  }
static void sections_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_sections=gtk_spin_button_get_value(spin_button);
  }
static void create_new_section(GtkWidget *button, gpointer *charts)
  {
    struct pie_chart *pc1=charts[0];
    GtkWidget *da=charts[2];
    gboolean was_ticking=FALSE;

    //Block the draw and tick when updating the array.
    g_signal_handler_block(da, draw_signal_id);
    if(tick_id!=0)
      {
        gtk_widget_remove_tick_callback(GTK_WIDGET(da), tick_id);
        tick_id=0;
        was_ticking=TRUE;
      }

    guint end=0;
    struct section *s1=NULL;
    if((pc1->sections->len)==0)
      {
        end=chart_sections-1;
      }
    else
      {
        s1=&g_array_index(pc1->sections, struct section, (pc1->sections->len)-1);
        end=(s1->end)+chart_sections;
      }
    
    gdouble rgba[4]={0.0, 0.0, 1.0, 0.5};
    pie_chart_append_section(pc1, end, rgba);

    g_signal_handler_unblock(da, draw_signal_id);
    if(was_ticking)
      {
        tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(da), (GtkTickCallback)animate_drawing, charts, NULL);
      }

    gtk_widget_queue_draw(GTK_WIDGET(da));
  }
static void explode_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    explode_pieces=gtk_spin_button_get_value(spin_button);
  }
static void section_id_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    section_id=gtk_spin_button_get_value_as_int(spin_button);
  }
static void explode_all_sections(GtkWidget *button, gpointer *charts)
  {
    struct pie_chart *pc=charts[0];
    GtkWidget *da=charts[2];
    pie_chart_explode_all(pc, explode_pieces);
    gtk_widget_queue_draw(GTK_WIDGET(da));
  }
static void explode_single_section(GtkWidget *button, gpointer *charts)
  {
    struct pie_chart *pc=charts[0];
    GtkWidget *da=charts[2];
    pie_chart_explode_section(pc, section_id, explode_pieces);
    gtk_widget_queue_draw(GTK_WIDGET(da));
  }
static void show_frame_rate(GtkToggleButton *check_frame, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_frame)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_drawing_time(GtkToggleButton *check_time, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_time)) time_it=TRUE;
    else time_it=FALSE;
  }
static void draw_label_lines(GtkToggleButton *check, gpointer data)
  {
    if(gtk_toggle_button_get_active(check)) label_lines_g=ADD_LABEL_LINES;
    else label_lines_g=NO_LABEL_LINES;

    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void draw_svg(GtkWidget *widget, gpointer *charts)
  {
    GtkWidget *da=charts[2];
    gdouble width=(gdouble)gtk_widget_get_allocated_width(GTK_WIDGET(da));
    gdouble height=(gdouble)gtk_widget_get_allocated_height(GTK_WIDGET(da));
    cairo_surface_t *surface=cairo_svg_surface_create("pie1.svg", width, height);
    cairo_t *cr=cairo_create(surface);

    draw_surface(cr, width, height, charts);

    g_print("pie1.svg saved\n");
    cairo_destroy(cr);
    cairo_surface_destroy(surface);
  }
static void draw_pdf(GtkWidget *widget, gpointer *charts)
  {
    GtkWidget *da=charts[2];
    gdouble width=(gdouble)gtk_widget_get_allocated_width(GTK_WIDGET(da));
    gdouble height=(gdouble)gtk_widget_get_allocated_height(GTK_WIDGET(da));
    cairo_surface_t *surface=cairo_pdf_surface_create("pie1.pdf", width, height);
    cairo_t *cr=cairo_create(surface);

    draw_surface(cr, width, height, charts);

    g_print("pie1.pdf saved\n");
    cairo_destroy(cr);
    cairo_surface_destroy(surface);
  }
static void draw_ps(GtkWidget *widget, gpointer *charts)
  {
    GtkWidget *da=charts[2];
    gdouble width=(gdouble)gtk_widget_get_allocated_width(GTK_WIDGET(da));
    gdouble height=(gdouble)gtk_widget_get_allocated_height(GTK_WIDGET(da));
    cairo_surface_t *surface=cairo_ps_surface_create("pie1.ps", width, height);
    cairo_t *cr=cairo_create(surface);

    draw_surface(cr, width, height, charts);

    g_print("pie1.ps saved\n");
    cairo_destroy(cr);
    cairo_surface_destroy(surface);
  }
static void draw_surface(cairo_t *cr, gdouble width, gdouble height, gpointer *charts)
  {
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;
    struct pie_chart *pc1=charts[0];
    struct pie_chart *pc2=charts[1];
    GtkWidget *da=charts[2];

    //Background.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr); 

    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);        
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_translate(cr, width/2.0, height/2.0); 
    cairo_scale(cr, scale+0.01, scale+0.01);

    if(drawing_id==0)
      {
        pc1->outline=TRUE;        
        draw_pie_chart(da, cr, pc1, label_lines_g);
      }
    else if(drawing_id==1)
      {
        pc1->outline=TRUE;
        cairo_save(cr);
        cairo_translate(cr, -2.0*w1, -2.0*h1);
        cairo_scale(cr, 0.5, 0.5);
        draw_pie_chart(da, cr, pc1, label_lines_g);
        cairo_restore(cr);
        cairo_save(cr);
        cairo_translate(cr, 2.0*w1, -2.0*h1);
        cairo_scale(cr, 0.5, 0.5);
        draw_pie_chart(da, cr, pc1, label_lines_g);
        cairo_restore(cr);
        cairo_save(cr);
        cairo_translate(cr, -2.0*w1, 2.0*h1);
        cairo_scale(cr, 0.5, 0.5);
        draw_pie_chart(da, cr, pc1, label_lines_g);
        cairo_restore(cr);
        cairo_save(cr);
        cairo_translate(cr, 2.0*w1, 2.0*h1);
        cairo_scale(cr, 0.5, 0.5);
        draw_pie_chart(da, cr, pc1, label_lines_g);
        cairo_restore(cr);
      }
     //Draw with the clock.
     else if(drawing_id==2)
      {
        pc1->outline=FALSE;        
        draw_pie_chart(da, cr, pc1, label_lines_g);
      }
     else if(drawing_id==3)
      {
        gdouble rgba[4]={0.0, 0.0, 1.0, 0.8};
        pie_chart_clear_sections(pc2);
        if(sweep==0)
          {
            pie_chart_append_section(pc2, 59, rgba);
          }
        else if(sweep==59)
          {
            rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.8;
            pie_chart_append_section(pc2, sweep, rgba);
          } 
        else
          {
            rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.8;
            pie_chart_append_section(pc2, sweep, rgba);
            rgba[0]=0.0;rgba[1]=0.0;rgba[2]=1.0;rgba[3]=0.8;
            pie_chart_append_section(pc2, 59, rgba);
          }
        pc2->outline=FALSE;        
        draw_pie_chart(da, cr, pc2, label_lines_g);
      }
    else if(drawing_id==4)
      {
        pc1->outline=TRUE;       
        draw_pie_chart_ring(da, cr, pc1, 0.5, label_lines_g);
      }
    else
      {
        gdouble rgba[4]={1.0, 0.0, 1.0, 0.4};
        pie_chart_clear_sections(pc2);
        if(sweep==0)
          {
            pie_chart_append_section(pc2, 59, rgba);
          }
        else if(sweep==59)
          {
            rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.4;
            pie_chart_append_section(pc2, sweep, rgba);
          } 
        else
          {
            rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.4;
            pie_chart_append_section(pc2, sweep, rgba);
            rgba[0]=1.0;rgba[1]=0.0;rgba[2]=1.0;rgba[3]=0.4;
            pie_chart_append_section(pc2, 59, rgba);
          }
        pc2->outline=TRUE;        
        draw_pie_chart_ring(da, cr, pc2, move_ring, label_lines_g);
      }
  }
static void initialize_pie_clock()
  {
    gint i=0;
    pie_clock=g_array_sized_new(FALSE, FALSE, sizeof(struct point3d), 12);
    g_array_set_size(pie_clock, 12);

    struct point3d *p1=&g_array_index(pie_clock, struct point3d, 0);
    gdouble hour=-G_PI/2.0;
    for(i=0;i<(pie_clock->len);i++)
      {
        p1->x=cos(hour);
        p1->y=sin(hour);
        p1->z=0.0;
        hour+=G_PI/6.0;
        p1++;
      }
  }
static struct pie_chart* pie_chart_new(guint pieces, gdouble radius, gdouble depth)
  {
    //Enforce some semi-arbitrary bounds. 
    if(pieces>120||pieces<30)
      {
        pieces=60;
        g_warning("Pieces bounds, 30<=pieces<=120.0. Default pieces set to 60.0.\n");
      }      
    if(radius>200.0||radius<1.0)
      {
        radius=100.0;
        g_warning("Radius bounds, 1.0<=radius<=200.0. Default radius set to 100.0.\n");
      }
    if(depth>100.0||depth<0.0)
      {
        depth=20.0;
        g_warning("Depth bounds, 0.0<=depth<=100.0. Default depth set to 20.0.\n");
      }

    struct pie_chart *pc=g_new(struct pie_chart, 1);
    pc->pieces=pieces;
    pc->radius=radius;
    pc->pie_chart1=NULL;
    pc->pie_chart1_t=NULL;
    pc->pie_chart2=NULL;
    pc->pie_chart2_t=NULL;
    pc->v_pie=NULL;
    pc->v_pie_t=NULL;
    pc->sections=NULL;
    pc->top[0]=0.0;
    pc->top[1]=0.0;
    pc->top[2]=1.0;
    pc->top_t[0]=0.0;
    pc->top_t[1]=0.0;
    pc->top_t[2]=1.0;
    pc->depth=20.0;
    pc->outline=TRUE;

    initialize_bezier_ring(&(pc->pie_chart1), &(pc->pie_chart1_t), pieces, radius, depth);
    initialize_bezier_ring(&(pc->pie_chart2), &(pc->pie_chart2_t), pieces, radius, -depth);

    pc->v_pie=g_array_new(FALSE, FALSE, sizeof(struct point3d));
    pc->v_pie_t=g_array_new(FALSE, FALSE, sizeof(struct point3d));
    pc->sections=g_array_new(FALSE, FALSE, sizeof(struct section));

    return pc;
  }
static void pie_chart_append_section(struct pie_chart *pc, guint end, gdouble rgba[4])
  {
    gboolean error=FALSE;
    struct section *p1=NULL;

    //Check some error conditions.
    if(pc==NULL)
      {
        error=TRUE;
        g_warning("There isn't a valid pie chart. No section appended.\n");
      }
    else if(end<0||end>(pc->pie_chart1->len)-1)
      {
        error=TRUE;
        g_warning("Section end is outside of the pie chart boundry. No section appended.\n");
      }
    else if((pc->sections->len)>0)
      {
        p1=&g_array_index(pc->sections, struct section, (pc->sections->len)-1);
        if(end<=(p1->end))
          {
            error=TRUE;
            g_warning("The end is contained in an existing section. No section appended.\n");
          }
      }

    //Add the new section and vectors.
    if(!error)
      {
        gdouble pieces=(gdouble)(pc->pie_chart1->len)/2.0;
        struct section s1;
        s1.end=end;
        s1.color[0]=rgba[0];s1.color[1]=rgba[1];s1.color[2]=rgba[2];s1.color[3]=rgba[3];
        s1.section_explode=0.0;
        //Get the start from the last section.
        if((pc->sections->len)==0)
          {
            s1.start=0;
          }
        else
          {
            p1=&g_array_index(pc->sections, struct section, (pc->sections->len)-1);
            s1.start=(p1->end)+1;
          }
        gdouble vc=s1.start+((end+1)-s1.start)/2.0;
        struct point3d v1;
        v1.x=cos(vc*G_PI/pieces);v1.y=-sin(vc*G_PI/pieces);v1.z=0.0;
        s1.section_id=(pc->sections->len);
        g_array_append_val(pc->sections, s1);
        g_array_append_val(pc->v_pie, v1);
        g_array_append_val(pc->v_pie_t, v1);
      }

  }
static void pie_chart_clear_sections(struct pie_chart *pc)
  {
    gint i=0;
    gint len=(pc->sections->len);

    if(len>0)
      {
        for(i=len;i>0;i--) g_array_remove_index_fast(pc->sections, i-1);
        for(i=len;i>0;i--) g_array_remove_index_fast(pc->v_pie, i-1);
        for(i=len;i>0;i--) g_array_remove_index_fast(pc->v_pie_t, i-1);
      }
    else
      {
        g_warning("The sections are already cleared.\n");
      }

  }
static void pie_chart_explode_section(struct pie_chart *pc, gint section_num, gdouble radius)
  {
    guint len=(pc->sections->len);
    if(len>0&&section_num>=0&&section_num<len&&radius>=0.0)
      {
        struct section *s1=&g_array_index(pc->sections, struct section, section_num);
        s1->section_explode=radius;
      }
    else
      {
        g_warning("The radius range is >=0.0 and the section needs to be present.\n");
      }
  }
static void pie_chart_explode_all(struct pie_chart *pc, gdouble radius)
  {
    gint i=0;
    struct section *s1=&g_array_index(pc->sections, struct section, 0);

    if(radius>=0.0)
      {
        for(i=0;i<(pc->sections->len);i++)
          {
            s1->section_explode=radius;
            s1++;
          }
      }
    else
      {
        g_warning("The radius range is >=0.0.\n");
      }
  }
static void pie_chart_set_z(struct pie_chart *pc, gdouble z)
  {
    gint i=0;
    struct arc *p1=&g_array_index(pc->pie_chart1, struct arc, 0);

    pc->depth=z;
    for(i=0;i<(pc->pie_chart1->len);i++)
      {
        (p1->a1).z=z;
        (p1->a2).z=z;
        (p1->a3).z=z;
        (p1->a4).z=z;
        p1++;
      }

     p1=&g_array_index(pc->pie_chart2, struct arc, 0);
     for(i=0;i<(pc->pie_chart2->len);i++)
      {
        (p1->a1).z=-z;
        (p1->a2).z=-z;
        (p1->a3).z=-z;
        (p1->a4).z=-z;
        p1++;
      }
  }
static void pie_chart_free(struct pie_chart *pc)
  {
    if((pc->pie_chart1)!=NULL) g_array_free((pc->pie_chart1), TRUE);
    if((pc->pie_chart1_t)!=NULL) g_array_free((pc->pie_chart1_t), TRUE);
    if((pc->pie_chart2)!=NULL) g_array_free((pc->pie_chart2), TRUE);
    if((pc->pie_chart2_t)!=NULL) g_array_free((pc->pie_chart2_t), TRUE);
    if((pc->v_pie)!=NULL) g_array_free((pc->v_pie), TRUE);
    if((pc->v_pie_t)!=NULL) g_array_free((pc->v_pie_t), TRUE);
    if((pc->sections)!=NULL) g_array_free((pc->sections), TRUE);
    g_free(pc);
    pc=NULL;
  }
static void initialize_bezier_ring(GArray **ring, GArray **ring_t, guint pieces, gdouble radius, gdouble z)
  {
    /*
         Bezier control points. 
         https://en.wikipedia.org/wiki/Composite_B%C3%A9zier_curve#Approximating_circular_arcs 

         Get the points in one arc of the ring and rotate them for the other points in the ring.
    */  
    gint i=0;
    //Get one section arc.
    gdouble arc_top=-(2.0*G_PI/pieces)/2.0;
    gdouble arc_offset=-2.0*G_PI/pieces;
    gdouble start_x=radius*cos(arc_top);
    gdouble start_y=radius*sin(arc_top);
    gdouble end_x=start_x;
    gdouble end_y=-start_y;
    gdouble c1_x=(4.0*radius-start_x)/3.0;
    gdouble c1_y=((radius-start_x)*(3.0*radius-start_x))/(3.0*start_y);
    gdouble c2_x=c1_x;
    gdouble c2_y=-c1_y;
    gdouble offset1=0.0;
    gdouble offset2=0.0;

    *ring=g_array_sized_new(FALSE, FALSE, sizeof(struct arc), pieces);
    g_array_set_size(*ring, pieces);
    *ring_t=g_array_sized_new(FALSE, FALSE, sizeof(struct arc), pieces);
    g_array_set_size(*ring_t, pieces);

    struct arc *p1=&g_array_index(*ring, struct arc, 0);
    struct arc *p2=&g_array_index(*ring, struct arc, 1);

    //Rotate the points in the section to align with the zero point on the unit circle.
    offset1=cos(arc_top);
    offset2=sin(arc_top);
    (p1->a4).x=start_x*offset1-start_y*offset2;
    (p1->a4).y=start_x*offset2+start_y*offset1;
    (p1->a4).z=z;
    (p1->a3).x=c1_x*offset1-c1_y*offset2;
    (p1->a3).y=c1_x*offset2+c1_y*offset1;
    (p1->a3).z=z;
    (p1->a2).x=c2_x*offset1-c2_y*offset2;
    (p1->a2).y=c2_x*offset2+c2_y*offset1;
    (p1->a2).z=z;
    (p1->a1).x=end_x*offset1-end_y*offset2;
    (p1->a1).y=end_x*offset2+end_y*offset1;
    (p1->a1).z=z;

    offset1=cos(arc_offset);
    offset2=sin(arc_offset);
    for(i=1;i<pieces;i++)
      {
        (p2->a1).x=(p1->a1).x*offset1-(p1->a1).y*offset2;
        (p2->a1).y=(p1->a1).x*offset2+(p1->a1).y*offset1;
        (p2->a1).z=z;
        (p2->a2).x=(p1->a2).x*offset1-(p1->a2).y*offset2;
        (p2->a2).y=(p1->a2).x*offset2+(p1->a2).y*offset1;
        (p2->a2).z=z;
        (p2->a3).x=(p1->a3).x*offset1-(p1->a3).y*offset2;
        (p2->a3).y=(p1->a3).x*offset2+(p1->a3).y*offset1;
        (p2->a3).z=z;
        (p2->a4).x=(p1->a4).x*offset1-(p1->a4).y*offset2;
        (p2->a4).y=(p1->a4).x*offset2+(p1->a4).y*offset1;
        (p2->a4).z=z;
        p1++;p2++;
      }
  }
static gboolean redraw_clock(gpointer da)
  {
    gtk_widget_queue_draw(GTK_WIDGET(da));
    return TRUE;
  }
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer *charts)
  {
    struct pie_chart *pc=charts[0];

    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    if(pitch>360.0) pitch=0.0;
    else pitch+=0.5;
    if(roll>360.0) roll=0.0;
    else roll+=0.5;
    if(yaw>360.0) yaw=0.0;
    else yaw+=0.5;
    if(pie_explode>100.0)
      {
        pie_explode=0.0;
        if(drawing_id==1) pie_chart_explode_section(pc, 2, pie_explode);
        else pie_chart_explode_all(pc, pie_explode);
      }
    else
      {
        pie_explode+=0.5;
        if(drawing_id==1) pie_chart_explode_section(pc, 2, pie_explode);
        else pie_chart_explode_all(pc, pie_explode);
      }
    if(sweep>58) sweep=0;
    else sweep++;
    if(move_ring>0.75) move_ring=0.25;
    else move_ring+=0.005;

    gtk_widget_queue_draw(GTK_WIDGET(drawing));
    return G_SOURCE_CONTINUE;
  }
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer *charts)
  {  
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;
    struct pie_chart *pc1=charts[0];
    struct pie_chart *pc2=charts[1];

    //Background.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr); 

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 2.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);
  
    if((pc1->sections->len)==0||(pc2->sections->len)==0)
      {
        g_print("There are no pie sections to draw!\n");
        goto exit;
      }

    GTimer *timer=NULL; 
    if(time_it==TRUE) timer=g_timer_new();

    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);        
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_translate(cr, width/2.0, height/2.0); 
    cairo_scale(cr, scale+0.01, scale+0.01);

    if(drawing_id==0)
      {
        pc1->outline=TRUE;        
        draw_pie_chart(da, cr, pc1, label_lines_g);
      }
    else if(drawing_id==1)
      {
        pc1->outline=TRUE;
        cairo_save(cr);
        cairo_translate(cr, -2.0*w1, -2.0*h1);
        cairo_scale(cr, 0.5, 0.5);
        draw_pie_chart(da, cr, pc1, label_lines_g);
        cairo_restore(cr);
        cairo_save(cr);
        cairo_translate(cr, 2.0*w1, -2.0*h1);
        cairo_scale(cr, 0.5, 0.5);
        draw_pie_chart(da, cr, pc1, label_lines_g);
        cairo_restore(cr);
        cairo_save(cr);
        cairo_translate(cr, -2.0*w1, 2.0*h1);
        cairo_scale(cr, 0.5, 0.5);
        draw_pie_chart(da, cr, pc1, label_lines_g);
        cairo_restore(cr);
        cairo_save(cr);
        cairo_translate(cr, 2.0*w1, 2.0*h1);
        cairo_scale(cr, 0.5, 0.5);
        draw_pie_chart(da, cr, pc1, label_lines_g);
        cairo_restore(cr);
      }
     //Draw with the clock.
     else if(drawing_id==2)
      {
        pc1->outline=FALSE;        
        draw_pie_chart(da, cr, pc1, label_lines_g);
      }
     else if(drawing_id==3)
      {
        gdouble rgba[4]={0.0, 0.0, 1.0, 0.8};
        pie_chart_clear_sections(pc2);
        if(sweep==0)
          {
            pie_chart_append_section(pc2, 59, rgba);
          }
        else if(sweep==59)
          {
            rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.8;
            pie_chart_append_section(pc2, sweep, rgba);
          } 
        else
          {
            rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.8;
            pie_chart_append_section(pc2, sweep, rgba);
            rgba[0]=0.0;rgba[1]=0.0;rgba[2]=1.0;rgba[3]=0.8;
            pie_chart_append_section(pc2, 59, rgba);
          }
        pc2->outline=FALSE;        
        draw_pie_chart(da, cr, pc2, label_lines_g);
      }
    else if(drawing_id==4)
      {
        pc1->outline=TRUE;       
        draw_pie_chart_ring(da, cr, pc1, 0.5, label_lines_g);
      }
    else
      {
        gdouble rgba[4]={1.0, 0.0, 1.0, 0.4};
        pie_chart_clear_sections(pc2);
        if(sweep==0)
          {
            pie_chart_append_section(pc2, 59, rgba);
          }
        else if(sweep==59)
          {
            rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.4;
            pie_chart_append_section(pc2, sweep, rgba);
          } 
        else
          {
            rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.4;
            pie_chart_append_section(pc2, sweep, rgba);
            rgba[0]=1.0;rgba[1]=0.0;rgba[2]=1.0;rgba[3]=0.4;
            pie_chart_append_section(pc2, 59, rgba);
          }
        pc2->outline=TRUE;        
        draw_pie_chart_ring(da, cr, pc2, move_ring, label_lines_g);
      }
     
    if(time_it==TRUE) 
     {
       g_print("Draw Time %f\n", g_timer_elapsed(timer, NULL));
       g_timer_destroy(timer);
     }

    exit:
    return FALSE;
  }
static void draw_pie_chart(GtkWidget *da, cairo_t *cr, struct pie_chart *pc, guint label_lines)
  {
    //An eploding 3d pie chart.
    gint i=0;
    gint j=0;
    gint offset=0;
    gdouble line_start=0.0;
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;

    cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, 24);

    cairo_set_line_width(cr, 2.0); 
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    gdouble qrs[9]; 
    quaternion_rotation(yaw1, roll1, pitch1, qrs);
    reset_ring(pc->pie_chart1, pc->pie_chart1_t); 
    reset_ring(pc->pie_chart2, pc->pie_chart2_t);     
    rotate_ring(pc->pie_chart1_t, qrs);
    rotate_ring(pc->pie_chart2_t, qrs); 
    rotate_reference_vector(pc, qrs); 
    rotate_pie_reference_vectors(pc, qrs); 
      
    //Draw the pie chart. 60 arcs or pieces of pie.
    struct arc *p1=NULL;
    struct arc *p2=NULL;
    struct point3d *pt=NULL;
    struct section *s1=NULL;

    /*
      Draw the bottom of pie chart. This doesn't need to be drawn for solid colors.
      Needed for transparent colors.
    */
    if((pc->top_t)[2]>0.0) p2=&g_array_index(pc->pie_chart2_t, struct arc, 0);
    else p2=&g_array_index(pc->pie_chart1_t, struct arc, 0);
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct point3d, 0);

    for(i=0;i<(pc->sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;    
        cairo_save(cr);
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y));
        cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]);
        if((pc->top_t)[2]>0.0) cairo_move_to(cr, -(pc->depth)*(pc->top_t)[0], -(pc->depth)*(pc->top_t)[1]);
        else cairo_move_to(cr, (pc->depth)*(pc->top_t)[0], (pc->depth)*(pc->top_t)[1]);
        cairo_line_to(cr, (p2->a1).x, (p2->a1).y); 
        for(j=0;j<offset;j++)
          {
            cairo_curve_to(cr, (p2->a2).x, (p2->a2).y, (p2->a3).x, (p2->a3).y, (p2->a4).x, (p2->a4).y);
            p2++;
          }
        cairo_close_path(cr);
        if((pc->outline))
          {
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else
          {
            cairo_fill(cr);
          }        

        cairo_restore(cr);
        s1++;pt++;
      }

    //An array to sort inside quads.
    struct quad q;
    GArray *quads=g_array_new(FALSE, FALSE, sizeof(struct quad));
     
    //Get the inside quad points.
    if((pc->top_t)[2]>0.0)
      {
        p1=&g_array_index(pc->pie_chart1_t, struct arc, 0);
        p2=&g_array_index(pc->pie_chart2_t, struct arc, 0);
      }
    else
      {
        p1=&g_array_index(pc->pie_chart2_t, struct arc, 0);
        p2=&g_array_index(pc->pie_chart1_t, struct arc, 0);
      } 

    s1=&g_array_index(pc->sections, struct section, 0);
    for(i=0;i<(pc->sections->len);i++)
      {
        q.color[0]=(s1->color)[0];q.color[1]=(s1->color)[1];q.color[2]=(s1->color)[2];q.color[3]=(s1->color)[3];  
        if((pc->top_t)[2]>0.0)
          {
            q.a.x=(pc->depth)*(pc->top_t)[0];q.a.y=(pc->depth)*(pc->top_t)[1];q.a.z=(pc->depth)*(pc->top_t)[2];
          }
        else
          {
            q.a.x=-(pc->depth)*(pc->top_t)[0];q.a.y=-(pc->depth)*(pc->top_t)[1];q.a.z=-(pc->depth)*(pc->top_t)[2];
          }
        q.b.x=(p1->a1).x;q.b.y=(p1->a1).y;q.b.z=(p1->a1).z;
        q.c.x=(p2->a1).x;q.c.y=(p2->a1).y;q.c.z=(p2->a1).z;
        if((pc->top_t)[2]>0.0)
          {
            q.d.x=-(pc->depth)*(pc->top_t)[0];q.d.y=-(pc->depth)*(pc->top_t)[1];q.d.z=-(pc->depth)*(pc->top_t)[2];
          }
        else
          {
            q.d.x=(pc->depth)*(pc->top_t)[0];q.d.y=(pc->depth)*(pc->top_t)[1];q.d.z=(pc->depth)*(pc->top_t)[2];
          }
        q.section_id=(s1->section_id);
        q.section_explode=(s1->section_explode);
        g_array_append_val(quads, q);

        //Second side in pie section.
        offset=(s1->end)-(s1->start);
        p1+=offset;p2+=offset; 
        q.color[0]=(s1->color)[0];q.color[1]=(s1->color)[1];q.color[2]=(s1->color)[2];q.color[3]=(s1->color)[3];  
        if((pc->top_t)[2]>0.0)
          {
            q.a.x=(pc->depth)*(pc->top_t)[0];q.a.y=(pc->depth)*(pc->top_t)[1];q.a.z=(pc->depth)*(pc->top_t)[2];
          }
        else
          {
            q.a.x=-(pc->depth)*(pc->top_t)[0];q.a.y=-(pc->depth)*(pc->top_t)[1];q.a.z=-(pc->depth)*(pc->top_t)[2];
          }
        q.b.x=(p1->a4).x;q.b.y=(p1->a4).y;q.b.z=(p1->a4).z;
        q.c.x=(p2->a4).x;q.c.y=(p2->a4).y;q.c.z=(p2->a4).z;
        if((pc->top_t)[2]>0.0)
          {
            q.d.x=-(pc->depth)*(pc->top_t)[0];q.d.y=-(pc->depth)*(pc->top_t)[1];q.d.z=-(pc->depth)*(pc->top_t)[2];
          }
        else
          {
            q.d.x=(pc->depth)*(pc->top_t)[0];q.d.y=(pc->depth)*(pc->top_t)[1];q.d.z=(pc->depth)*(pc->top_t)[2];
          }
        q.section_id=(s1->section_id);
        q.section_explode=(s1->section_explode);
        g_array_append_val(quads, q);
        p1++;p2++;s1++;
      }

    //Sort and draw the inside quads.
    g_array_sort(quads, compare_quads);
    struct quad *q1=&g_array_index(quads, struct quad, 0);
    pt=&g_array_index(pc->v_pie_t, struct point3d, 0);
    for(i=0;i<(quads->len);i++)
      {
        cairo_save(cr);
        cairo_translate(cr, (q1->section_explode)*((pt+(q1->section_id))->x), (q1->section_explode)*((pt+(q1->section_id))->y));        
        cairo_set_source_rgba(cr, (q1->color)[0], (q1->color)[1], (q1->color)[2], (q1->color)[3]); 
        cairo_move_to(cr, (q1->a).x, (q1->a).y);
        cairo_line_to(cr, (q1->b).x, (q1->b).y);
        cairo_line_to(cr, (q1->c).x, (q1->c).y);
        cairo_line_to(cr, (q1->d).x, (q1->d).y);
        cairo_close_path(cr);
        cairo_fill(cr);
        cairo_restore(cr);
        q1++;
      }
    g_array_free(quads, TRUE);

    //Draw circular outside sides of the pieces of the pie chart.
    if((pc->top_t)[2]>0.0)
      {
        p1=&g_array_index(pc->pie_chart1_t, struct arc, 0);
        p2=&g_array_index(pc->pie_chart2_t, struct arc, 0);
      }
    else
      {
        p1=&g_array_index(pc->pie_chart2_t, struct arc, 0);
        p2=&g_array_index(pc->pie_chart1_t, struct arc, 0);
      } 
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct point3d, 0);

    for(i=0;i<(pc->sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;
        cairo_save(cr);
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y));
        for(j=0;j<offset;j++)
          {
            if((p1->a1).z>0.0)
              {
                cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]); 
                cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
                cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
                cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
                cairo_curve_to(cr, (p2->a3).x, (p2->a3).y, (p2->a2).x, (p2->a2).y, (p2->a1).x, (p2->a1).y);
                cairo_close_path(cr);
                cairo_fill(cr);
              }
            p1++;p2++;
          }
        cairo_restore(cr);
        s1++;pt++;
      }

    //For the pie clock drawing.
    if(drawing_id==2)
      {
        cairo_set_line_width(cr, 10.0);
        draw_pie_clock(da, cr, pc, qrs);
        cairo_set_line_width(cr, 2.0);
      } 

    //Draw the top of pie chart.
    if((pc->top_t)[2]>0.0) p1=&g_array_index(pc->pie_chart1_t, struct arc, 0);
    else p1=&g_array_index(pc->pie_chart2_t, struct arc, 0); 
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct point3d, 0);

    for(i=0;i<(pc->sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;    
        cairo_save(cr);
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y)); 
        cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]);
        if((pc->top_t)[2]>0.0) cairo_move_to(cr, (pc->depth)*(pc->top_t)[0], (pc->depth)*(pc->top_t)[1]);
        else cairo_move_to(cr, -(pc->depth)*(pc->top_t)[0], -(pc->depth)*(pc->top_t)[1]);
        cairo_line_to(cr, (p1->a1).x, (p1->a1).y); 
        for(j=0;j<offset;j++)
          {
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            p1++;
          }
        cairo_close_path(cr);
        if((pc->outline))
          {
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else
          {
            cairo_fill(cr);
          }

        //Add label lines.
        if(label_lines)
          {
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            line_start=(pc->radius)/1.5;
            cairo_move_to(cr, (pt->x)*line_start, (pt->y)*line_start);
            cairo_line_to(cr, (pt->x)*line_start*2.0, (pt->y)*line_start*2.0);
            if((pt->x)>0) cairo_rel_line_to(cr, line_start/2.0, 0.0);
            else cairo_rel_line_to(cr, -line_start/2.0, 0.0);
            cairo_stroke(cr);

            cairo_save(cr);
            gchar *string=g_strdup_printf("%i", i);
            cairo_text_extents(cr, string, &extents);
            if((pt->x)>0) cairo_move_to(cr, (pt->x)*line_start*2.0+line_start/2.0+5.0, (pt->y)*line_start*2.0+extents.height/2.0);
            else cairo_move_to(cr, (pt->x)*line_start*2.0-line_start/2.0-extents.width-5.0, (pt->y)*line_start*2.0+extents.height/2.0);
            cairo_matrix_init(&matrix1, qrs[0], qrs[3], qrs[1], qrs[4], 0.0, 0.0);
            cairo_transform(cr, &matrix1); 
            cairo_show_text(cr, string);
            g_free(string);
            cairo_restore(cr);
          }

        cairo_restore(cr);
        s1++;pt++;
      }

  }
static void draw_pie_clock(GtkWidget *da, cairo_t *cr, struct pie_chart *pc, gdouble qrs[9])
  {
    gint i=0;
    gchar *hours[]={"12", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"};
    cairo_select_font_face(cr, "Arial", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, 30);
    cairo_text_extents_t tick_extents;
    cairo_matrix_t matrix1;

    //Get the time.
    GTimeZone *time_zone=g_time_zone_new_local();
    GDateTime *date_time=g_date_time_new_now(time_zone);
    gint minute=g_date_time_get_minute(date_time);
    gint hour=g_date_time_get_hour(date_time);
    gdouble minute_x=cos(-G_PI/2.0+(gdouble)minute*G_PI/30.0);
    gdouble minute_y=sin(-G_PI/2.0+(gdouble)minute*G_PI/30.0);
    gdouble hour_x=cos(-G_PI/2.0+(gdouble)hour*G_PI/6.0);
    gdouble hour_y=sin(-G_PI/2.0+(gdouble)hour*G_PI/6.0);
    g_time_zone_unref(time_zone);
    g_date_time_unref(date_time);

    //Use cairo to rotate clock numbers.
    cairo_save(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_matrix_init(&matrix1, qrs[0], qrs[3], qrs[1], qrs[4], 0.0, 0.0);
    cairo_transform(cr, &matrix1);   
    struct point3d *p1=&g_array_index(pie_clock, struct point3d, 0);
    for(i=0;i<(pie_clock->len);i++)
      {
        cairo_text_extents(cr, hours[i], &tick_extents);
        cairo_move_to(cr, 125.0*(p1->x)-tick_extents.width/2.0, 125.0*(p1->y)+tick_extents.height/2.0);
        cairo_show_text(cr, hours[i]);
        p1++;
      }

    //Draw minute and hour hands of the clock.    
    cairo_move_to(cr, 0.0, 0.0);
    cairo_line_to(cr, 100.0*minute_x, 100.0*minute_y);
    cairo_stroke(cr);

    cairo_move_to(cr, 0.0, 0.0);
    cairo_line_to(cr, 60.0*hour_x, 60.0*hour_y);
    cairo_stroke(cr);

    cairo_restore(cr);
  }
static void draw_pie_chart_ring(GtkWidget *da, cairo_t *cr, struct pie_chart *pc, gdouble inside_ring, guint label_lines)
  {
    /*
      Draw a pie chart ring. It isn't z-sorted so the quads can be drawn out of order. Use cairo
      to transform and draw the inside ring from the rotated outside rings.
    */
    gint i=0;
    gint j=0;
    gint offset=0;
    gdouble line_start=0.0;
    gdouble line_length=0.0;
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;

    cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, 24);

    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    gdouble qrs[9]; 
    quaternion_rotation(yaw1, roll1, pitch1, qrs);
    reset_ring(pc->pie_chart1, pc->pie_chart1_t); 
    reset_ring(pc->pie_chart2, pc->pie_chart2_t);     
    rotate_ring(pc->pie_chart1_t, qrs);
    rotate_ring(pc->pie_chart2_t, qrs); 
    rotate_reference_vector(pc, qrs); 
    rotate_pie_reference_vectors(pc, qrs);
    gdouble scale=inside_ring;
    gdouble scale_inv=(1.0/scale)*(1.0-scale);      
 
    struct arc *p1=NULL;
    struct arc *p2=NULL;
    struct point3d *pt=NULL;
    struct section *s1=NULL;

    //Draw the bottom of pie chart.
    if((pc->top_t)[2]>0.0) p2=&g_array_index(pc->pie_chart2_t, struct arc, 0);
    else p2=&g_array_index(pc->pie_chart1_t, struct arc, 0);
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct point3d, 0);
 
    for(i=0;i<(pc->sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;    
        cairo_save(cr); 
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y));        
        cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]);       
        cairo_move_to(cr, (p2->a1).x, (p2->a1).y); 
        for(j=0;j<offset;j++)
          {
            cairo_curve_to(cr, (p2->a2).x, (p2->a2).y, (p2->a3).x, (p2->a3).y, (p2->a4).x, (p2->a4).y);
            p2++;
          }
        cairo_scale(cr, scale, scale);
        if((pc->top_t)[2]<0.0) cairo_translate(cr, scale_inv*pc->top_t[0]*pc->depth, scale_inv*pc->top_t[1]*pc->depth);
        else cairo_translate(cr, scale_inv*pc->top_t[0]*(-pc->depth), scale_inv*pc->top_t[1]*(-pc->depth));
        p2--;
        cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
        for(j=0;j<offset;j++)
          {
            cairo_curve_to(cr, ((p2-j)->a3).x, ((p2-j)->a3).y, ((p2-j)->a2).x, ((p2-j)->a2).y, ((p2-j)->a1).x, ((p2-j)->a1).y);
          }
        p2++;
        cairo_close_path(cr);
        if((pc->outline))
          {
            cairo_fill_preserve(cr); 
            gdouble dx=1.0;
            gdouble dy=0.0;
            cairo_device_to_user_distance(cr, &dx, &dy);
            cairo_set_line_width(cr, 2.0*dx); 
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else
          {
            cairo_fill(cr);
          }
        cairo_restore(cr);
        s1++;pt++;
      }

    //Draw one end cap of the sections of the pie chart.
    if((pc->top_t)[2]>0.0)
      {
        p1=&g_array_index(pc->pie_chart1_t, struct arc, 0);
        p2=&g_array_index(pc->pie_chart2_t, struct arc, 0);
      }
    else
      {
        p1=&g_array_index(pc->pie_chart2_t, struct arc, 0);
        p2=&g_array_index(pc->pie_chart1_t, struct arc, 0);
      } 
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct point3d, 0);

    for(i=0;i<(pc->sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;
        cairo_save(cr);    
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y));        
        cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]);       
        cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
        cairo_save(cr);
        cairo_scale(cr, scale, scale);
        if((pc->top_t)[2]>0.0) cairo_translate(cr, scale_inv*pc->top_t[0]*pc->depth, scale_inv*pc->top_t[1]*pc->depth);
        else cairo_translate(cr, scale_inv*pc->top_t[0]*(-pc->depth), scale_inv*pc->top_t[1]*(-pc->depth));        
        cairo_line_to(cr, (p1->a1).x, (p1->a1).y); 
        cairo_restore(cr);
        cairo_save(cr);
        cairo_scale(cr, scale, scale);
        if((pc->top_t)[2]<0.0) cairo_translate(cr, scale_inv*pc->top_t[0]*pc->depth, scale_inv*pc->top_t[1]*pc->depth);
        else cairo_translate(cr, scale_inv*pc->top_t[0]*(-pc->depth), scale_inv*pc->top_t[1]*(-pc->depth));
        cairo_line_to(cr, (p2->a1).x, (p2->a1).y);
        cairo_restore(cr);
        cairo_line_to(cr, (p2->a1).x, (p2->a1).y);
        cairo_close_path(cr);
        cairo_fill(cr);
        cairo_restore(cr);
        p1+=offset;p2+=offset;
        s1++;pt++;
      }

    //Draw the second end cap of the sections of the pie chart.
    if((pc->top_t)[2]>0.0)
      {
        p1=&g_array_index(pc->pie_chart1_t, struct arc, 0);
        p2=&g_array_index(pc->pie_chart2_t, struct arc, 0);
      }
    else
      {
        p1=&g_array_index(pc->pie_chart2_t, struct arc, 0);
        p2=&g_array_index(pc->pie_chart1_t, struct arc, 0);
      } 
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct point3d, 0);

    offset=(s1->end)-(s1->start);
    p1+=offset;p2+=offset;
    for(i=0;i<(pc->sections->len);i++)
      {
        cairo_save(cr);    
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y));        
        cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]);       
        cairo_move_to(cr, (p1->a4).x, (p1->a4).y);
        cairo_save(cr);
        cairo_scale(cr, scale, scale);
        if((pc->top_t)[2]>0.0) cairo_translate(cr, scale_inv*pc->top_t[0]*pc->depth, scale_inv*pc->top_t[1]*pc->depth);
        else cairo_translate(cr, scale_inv*pc->top_t[0]*(-pc->depth), scale_inv*pc->top_t[1]*(-pc->depth));        
        cairo_line_to(cr, (p1->a4).x, (p1->a4).y); 
        cairo_restore(cr);
        cairo_save(cr);
        cairo_scale(cr, scale, scale);
        if((pc->top_t)[2]<0.0) cairo_translate(cr, scale_inv*pc->top_t[0]*pc->depth, scale_inv*pc->top_t[1]*pc->depth);
        else cairo_translate(cr, scale_inv*pc->top_t[0]*(-pc->depth), scale_inv*pc->top_t[1]*(-pc->depth));
        cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
        cairo_restore(cr);
        cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
        cairo_close_path(cr);
        cairo_fill(cr);
        cairo_restore(cr);
        s1++;pt++;
        offset=(s1->end)-(s1->start)+1;
        p1+=offset;p2+=offset;
      }

    //Draw circular inside sides of the pieces of the pie chart.
    if((pc->top_t)[2]>0.0)
      {
        p1=&g_array_index(pc->pie_chart1_t, struct arc, 0);
        p2=&g_array_index(pc->pie_chart2_t, struct arc, 0);
      }
    else
      {
        p1=&g_array_index(pc->pie_chart2_t, struct arc, 0);
        p2=&g_array_index(pc->pie_chart1_t, struct arc, 0);
      } 
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct point3d, 0);

    for(i=0;i<(pc->sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;
        cairo_save(cr);
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y));
        cairo_scale(cr, scale, scale);
        for(j=0;j<offset;j++)
          {
            cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]);
            //If drawing solid color only draw the back part of ring.
            if((s1->color)[3]<0.9||(p1->a1).z<0.0)
              {
                cairo_save(cr);
                if((pc->top_t)[2]>0.0) cairo_translate(cr, scale_inv*pc->top_t[0]*pc->depth, scale_inv*pc->top_t[1]*pc->depth);
                else cairo_translate(cr, scale_inv*pc->top_t[0]*(-pc->depth), scale_inv*pc->top_t[1]*(-pc->depth));
                cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
                cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
                cairo_restore(cr);
                cairo_save(cr);
                if((pc->top_t)[2]>0.0) cairo_translate(cr, scale_inv*pc->top_t[0]*(-pc->depth), scale_inv*pc->top_t[1]*(-pc->depth));
                else cairo_translate(cr, scale_inv*pc->top_t[0]*pc->depth, scale_inv*pc->top_t[1]*pc->depth);
                cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
                cairo_curve_to(cr, (p2->a3).x, (p2->a3).y, (p2->a2).x, (p2->a2).y, (p2->a1).x, (p2->a1).y);
                cairo_close_path(cr);
                cairo_fill(cr);
                cairo_restore(cr);
              }
            p1++;p2++;
          }
        cairo_restore(cr);
        s1++;pt++;
      }

    //Draw circular outside sides of the pieces of the pie chart.
    if((pc->top_t)[2]>0.0)
      {
        p1=&g_array_index(pc->pie_chart1_t, struct arc, 0);
        p2=&g_array_index(pc->pie_chart2_t, struct arc, 0);
      }
    else
      {
        p1=&g_array_index(pc->pie_chart2_t, struct arc, 0);
        p2=&g_array_index(pc->pie_chart1_t, struct arc, 0);
      } 
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct point3d, 0);

    for(i=0;i<(pc->sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;
        cairo_save(cr);
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y));
        for(j=0;j<offset;j++)
          {
            cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]);
            //If drawing solid color only draw the front part of ring. 
            if((s1->color)[3]<0.9||(p1->a1).z>0.0)
              {
                cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
                cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
                cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
                cairo_curve_to(cr, (p2->a3).x, (p2->a3).y, (p2->a2).x, (p2->a2).y, (p2->a1).x, (p2->a1).y);
                cairo_close_path(cr);
                cairo_fill(cr);
              }
            p1++;p2++;
          }
        cairo_restore(cr);
        s1++;pt++;
      }    

    //Draw the top of pie chart.
    if((pc->top_t)[2]>0.0) p1=&g_array_index(pc->pie_chart1_t, struct arc, 0);
    else p1=&g_array_index(pc->pie_chart2_t, struct arc, 0); 
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct point3d, 0);

    for(i=0;i<(pc->sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;    
        cairo_save(cr); 
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y));        
        cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]);       
        cairo_move_to(cr, (p1->a1).x, (p1->a1).y); 
        for(j=0;j<offset;j++)
          {
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            p1++;
          }
        cairo_scale(cr, scale, scale);
        if((pc->top_t)[2]>0.0) cairo_translate(cr, scale_inv*pc->top_t[0]*pc->depth, scale_inv*pc->top_t[1]*pc->depth);
        else cairo_translate(cr, scale_inv*pc->top_t[0]*(-pc->depth), scale_inv*pc->top_t[1]*(-pc->depth));
        p1--;
        cairo_line_to(cr, (p1->a4).x, (p1->a4).y);
        for(j=0;j<offset;j++)
          {
            cairo_curve_to(cr, ((p1-j)->a3).x, ((p1-j)->a3).y, ((p1-j)->a2).x, ((p1-j)->a2).y, ((p1-j)->a1).x, ((p1-j)->a1).y);
          }
        p1++;
        cairo_close_path(cr);
        if((pc->outline))
          {
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            gdouble dx=1.0;
            gdouble dy=0.0;
            cairo_device_to_user_distance(cr, &dx, &dy);
            cairo_set_line_width(cr, 2.0*dx);  
            cairo_stroke(cr); 
          }
        else
          {
            cairo_fill(cr);
          }
        cairo_restore(cr); 

        //Add label lines.
        if(label_lines)
          {
            cairo_save(cr);
            cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y)); 
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            line_start=(pc->radius)*(1.0+inside_ring)/2.0;
            line_length=(pc->radius)+40.0;
            cairo_move_to(cr, (pt->x)*line_start, (pt->y)*line_start);
            cairo_line_to(cr, (pt->x)*line_length, (pt->y)*line_length);
            if((pt->x)>0) cairo_rel_line_to(cr, line_start/2.0, 0.0);
            else cairo_rel_line_to(cr, -line_start/2.0, 0.0);
            cairo_stroke(cr);

            gchar *string=g_strdup_printf("%i", i);
            cairo_text_extents(cr, string, &extents);
            if((pt->x)>0) cairo_move_to(cr, (pt->x)*line_length+line_start/2.0+5.0, (pt->y)*line_length+extents.height/2.0);
            else cairo_move_to(cr, (pt->x)*line_length-line_start/2.0-extents.width-5.0, (pt->y)*line_length+extents.height/2.0);
            cairo_matrix_init(&matrix1, qrs[0], qrs[3], qrs[1], qrs[4], 0.0, 0.0);
            cairo_transform(cr, &matrix1); 
            cairo_show_text(cr, string);
            g_free(string);
            cairo_restore(cr);
          }

        s1++;pt++;
      }
  }
static void reset_ring(GArray *ring, GArray *ring_t)
  {
    struct arc *p1=&g_array_index(ring, struct arc, 0);
    struct arc *p2=&g_array_index(ring_t, struct arc, 0);
    memcpy(p2, p1, sizeof(struct arc)*(ring->len));
  }
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
static void rotate_reference_vector(struct pie_chart *pc, gdouble qrs[9])
  {
    //Rotate the top[] reference vector.
    gdouble cr1=(pc->top)[0];
    gdouble cr2=(pc->top)[1];
    gdouble cr3=(pc->top)[2];         
    (pc->top_t)[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
    (pc->top_t)[1]=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
    (pc->top_t)[2]=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);  
  }
static void rotate_pie_reference_vectors(struct pie_chart *pc, gdouble qrs[9])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0; 
    struct point3d *p1=&g_array_index(pc->v_pie, struct point3d, 0);
    struct point3d *p2=&g_array_index(pc->v_pie_t, struct point3d, 0);

    for(i=0;i<(pc->v_pie->len);i++)
      {
        cr1=(p1->x);
        cr2=(p1->y);
        cr3=(p1->z);          
        (p2->x)=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p2->y)=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p2->z)=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]); 
        p1++;p2++;
      } 
  }
static void rotate_ring(GArray *ring, gdouble qrs[9])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0;   
    struct arc *p1=&g_array_index(ring, struct arc, 0);

    for(i=0;i<(ring->len);i++)
      {
        cr1=(p1->a1).x;
        cr2=(p1->a1).y;
        cr3=(p1->a1).z;         
        (p1->a1).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p1->a1).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p1->a1).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p1->a2).x;
        cr2=(p1->a2).y;
        cr3=(p1->a2).z;         
        (p1->a2).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p1->a2).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p1->a2).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p1->a3).x;
        cr2=(p1->a3).y;
        cr3=(p1->a3).z;         
        (p1->a3).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p1->a3).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p1->a3).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p1->a4).x;
        cr2=(p1->a4).y;
        cr3=(p1->a4).z;         
        (p1->a4).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p1->a4).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p1->a4).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        p1++;
      }  
  }
int compare_quads(const void *q1, const void *q2)
  {
    return((((struct quad*)q1)->a.z+((struct quad*)q1)->b.z+((struct quad*)q1)->c.z+((struct quad*)q1)->d.z) - (((struct quad*)q2)->a.z+((struct quad*)q2)->b.z+((struct quad*)q2)->c.z+((struct quad*)q2)->d.z));
  }

