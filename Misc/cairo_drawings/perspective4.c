
/* 
    Test drawing a cube, ball, cylinder, checkerboard, spiral and simple graph combining 3d
rotations, perspective and a stress matrix with cairo.
    Just use GArrays and compare to graphene. Not a fair comparison since it uses 3x3 matrices.
Either way works. The bottleneck is in the drawing code and not in the transforms.

    gcc -Wall perspective4.c -o perspective4 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<stdlib.h>
#include<string.h>

struct point3d{
  gdouble x;
  gdouble y;
  gdouble z;
};

//For getting bezier control points for an array of point3d's.
struct controls3d{
  gdouble x1;
  gdouble y1;
  gdouble z1;
  gdouble x2;
  gdouble y2;
  gdouble z2;
};

//Sort cube faces from back to front.
struct quad{
  gint id;
  gdouble z;
};

//For sorting planes in the ball drawing.
struct quad_plane{
  gdouble x1;
  gdouble y1;
  gdouble z1;
  gdouble x2;
  gdouble y2;
  gdouble z2;
  gdouble x3;
  gdouble y3;
  gdouble z3;
  gdouble x4;
  gdouble y4;
  gdouble z4;
  gdouble r;
  gdouble g;
  gdouble b;
  gdouble a;
};

//Reference cube and a transformed cube.
static GArray *cube=NULL;
static GArray *cube_t=NULL;
//Some lines across one face of the cube.
static GArray *lines=NULL;
static GArray *lines_t=NULL;
//Reference ball and a transformed ball.
static GArray *ball=NULL;
static GArray *ball_t=NULL;
static gint rings=0;
static gint points_per_ring=0;
//Reference cylinder and a transformed cylinder.
static GArray *cylinder=NULL;
static GArray *cylinder_t=NULL;
static gint points_per_ring_c=0;
//Reference checkerboard and a transformed checkerboard.
static GArray *checkerboard=NULL;
static GArray *checkerboard_t=NULL;
//Reference spiral and a transformed spiral.
static GArray *spiral=NULL;
static GArray *spiral_t=NULL;
//Reference random points and transformed points for a graph.
static GArray *graph_points=NULL;
static GArray *graph_points_t=NULL;
//A reference vector. Used for centering the object under perspective.
static struct point3d v1={.x=0.0, .y=0.0, .z=-1.0};;
static struct point3d v1_t;

//Combo ids for the perspective and stress.
static gint shape_id=0;
static gint perspective_id=0;
static gint stress_id=0;

//The combo rotation slider values.
static gdouble combo_pitch=0.0;
static gdouble combo_roll=0.0;
static gdouble combo_yaw=0.0;
static gdouble combo_perspective=0.0;

//Tick id for animation frame clock.
static guint tick_id=0;

//For the performance check boxes.
static gboolean frame_rate=FALSE;
static gboolean function_time=FALSE;

//Initialize some shapes to test.
static void initialize_cube();
static void initialize_ball(gint rings_i, gint points_per_ring_i);
static void initialize_cylinder(gint points_per_ring_i);
static void initialize_checkerboard();
static void initialize_bezier_spiral();
static void initialize_graph_points(gint num_points);
//Draw the main window transparent.
static gboolean draw_main_window(GtkWidget *window, cairo_t *cr, gpointer data);
//UI functions for sliders and combo boxes.
static void show_frame_rate(GtkToggleButton *button, gpointer data);
static void show_function_time(GtkToggleButton *button, gpointer data);
static void rotate_drawing(GtkComboBox *combo, gpointer data);
static void set_shape_id(GtkComboBox *combo, gpointer data);
static void set_perspective_id(GtkComboBox *combo, gpointer data);
static void set_stress_id(GtkComboBox *combo, gpointer data);
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data);
//The start of drawing. Get the combo_id's to draw.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer data);
/*
  Some matrix functions for drawing. Reset points, stress matrix, perspective vector, rotation matrix is
  the order of the operations.
*/
static void reset_draw_cube();
static void reset_draw_ball();
static void reset_draw_cylinder();
static void reset_draw_checkerboard();
static void reset_draw_bezier_spiral();
static void reset_draw_graph_points();
static void change_stress(const gdouble sm[9]);
static void change_perspective(gdouble scale);
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9]);
static void rotate_shape(gdouble qrs[9]);
//After the shape points are transformed, draw it.
static void draw_cube(cairo_t *cr, gdouble scale);
static void draw_cylinder(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_ball(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_checkerboard(cairo_t *cr, gdouble w1, gdouble h1);
int compare_quads(const void *a, const void *b);
int compare_quads2(const void *a, const void *b);
static void draw_bezier_spiral(cairo_t *cr, gdouble w1, gdouble h1);
static GArray* control_points_from_coords_3d(const GArray *data_points3d);
static void draw_graph_points(cairo_t *cr, gdouble w1, gdouble h1);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Perspective4");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    gtk_widget_set_app_paintable(window, TRUE);
    //Try to set transparency of main window.
    if(gtk_widget_is_composited(window))
      {
        GdkScreen *screen=gtk_widget_get_screen(window);  
        GdkVisual *visual=gdk_screen_get_rgba_visual(screen);
        gtk_widget_set_visual(window, visual);
      }
    else g_print("Can't set window transparency.\n");

    initialize_cube();
    initialize_ball(20, 20);
    initialize_cylinder(16);
    initialize_checkerboard();
    initialize_bezier_spiral();
    initialize_graph_points(50);

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);

    GtkWidget *combo_rotate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_rotate, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 0, "1", "Draw Shape");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 1, "2", "Animate Shape");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_rotate), 0);

    GtkWidget *combo_shape=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_shape, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_shape), 0, "1", "Cube");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_shape), 1, "2", "Ball");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_shape), 2, "3", "Cylinder");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_shape), 3, "4", "Checkerboard");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_shape), 4, "5", "Bezier Spiral");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_shape), 5, "6", "Graph Points");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_shape), 0);

    GtkWidget *combo_perspective=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_perspective, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_perspective), 0, "1", "Perspective1");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_perspective), 1, "2", "Perspective2");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_perspective), 0);

    GtkWidget *combo_stress=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_stress, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 0, "1", "No Stress");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 1, "2", "Stress 2x");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 2, "3", "Stress 2y");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 3, "4", "Stress 2z");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_stress), 0);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);

    GtkWidget *perspective_label=gtk_label_new("Perspective");
    gtk_widget_set_hexpand(perspective_label, TRUE);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider, TRUE);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider, TRUE);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider, TRUE);

    GtkWidget *perspective_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 100.0, 1.0);
    gtk_widget_set_vexpand(perspective_slider, TRUE);

    g_signal_connect(da, "draw", G_CALLBACK(draw_main), NULL);
    g_signal_connect(combo_rotate, "changed", G_CALLBACK(rotate_drawing), da);
    g_signal_connect(combo_shape, "changed", G_CALLBACK(set_shape_id), da);
    g_signal_connect(combo_perspective, "changed", G_CALLBACK(set_perspective_id), da);
    g_signal_connect(combo_stress, "changed", G_CALLBACK(set_stress_id), da);
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_pitch), da);
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_roll), da);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_yaw), da);
    g_signal_connect(perspective_slider, "change-value", G_CALLBACK(set_perspective), da);

    //Some checks for performance.
    GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
    gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
    g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), NULL);

    GtkWidget *check_functions=gtk_check_button_new_with_label("Show Function Time");
    gtk_widget_set_halign(check_functions, GTK_ALIGN_CENTER);
    g_signal_connect(check_functions, "toggled", G_CALLBACK(show_function_time), NULL);

    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_rotate, 0, 0, 6, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_shape, 0, 1, 6, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_perspective, 0, 2, 6, 1); 
    gtk_grid_attach(GTK_GRID(grid1), combo_stress, 0, 3, 6, 1);         
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 4, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid1), perspective_label, 3, 4, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), perspective_slider, 3, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_frame, 0, 6, 6, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_functions, 0, 7, 6, 1);

    GtkWidget *scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll, TRUE);
    gtk_widget_set_vexpand(scroll, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll), grid1);
   
    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), scroll, TRUE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 250);

    g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), paned1);
   
    gtk_container_add(GTK_CONTAINER(window), paned1);
    
    gtk_widget_show_all(window);

    gtk_main();

    g_array_free(cube, TRUE);
    g_array_free(cube_t, TRUE);
    g_array_free(lines, TRUE);
    g_array_free(lines_t, TRUE);
    g_array_free(ball, TRUE);
    g_array_free(ball_t, TRUE);
    g_array_free(cylinder, TRUE);
    g_array_free(cylinder_t, TRUE);
    g_array_free(checkerboard, TRUE);
    g_array_free(checkerboard_t, TRUE);
    g_array_free(spiral, TRUE);
    g_array_free(spiral_t, TRUE);
    g_array_free(graph_points, TRUE);
    g_array_free(graph_points_t, TRUE);

    return 0;  
  }
static void initialize_cube()
  {
    cube=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 8);
    g_array_set_size(cube, 8);
    cube_t=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 8);
    g_array_set_size(cube_t, 8);
    lines=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 18);
    g_array_set_size(lines, 18);
    lines_t=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 18);
    g_array_set_size(lines_t, 18);

    struct point3d *p1=&g_array_index(cube, struct point3d, 0);
    p1->x=1.0;p1->y=1.0;p1->z=1.0;
    p1++;
    p1->x=1.0;p1->y=1.0;p1->z=-1.0;
    p1++;
    p1->x=1.0;p1->y=-1.0;p1->z=-1.0;
    p1++;
    p1->x=1.0;p1->y=-1.0;p1->z=1.0;
    p1++;
    p1->x=-1.0;p1->y=1.0;p1->z=1.0;
    p1++;
    p1->x=-1.0;p1->y=1.0;p1->z=-1.0;
    p1++;
    p1->x=-1.0;p1->y=-1.0;p1->z=-1.0;
    p1++;
    p1->x=-1.0;p1->y=-1.0;p1->z=1.0;

    p1=&g_array_index(lines, struct point3d, 0);
    p1->x=-1.0;p1->y=-1.0;p1->z=0.8;
    p1++;
    p1->x=-1.0;p1->y=-1.0;p1->z=0.6;
    p1++;
    p1->x=-1.0;p1->y=-1.0;p1->z=0.4;
    p1++;
    p1->x=-1.0;p1->y=-1.0;p1->z=0.2;
    p1++;
    p1->x=-1.0;p1->y=-1.0;p1->z=0.0;
    p1++;
    p1->x=-1.0;p1->y=-1.0;p1->z=-0.2;
    p1++;
    p1->x=-1.0;p1->y=-1.0;p1->z=-0.4;
    p1++;
    p1->x=-1.0;p1->y=-1.0;p1->z=-0.6;
    p1++;
    p1->x=-1.0;p1->y=-1.0;p1->z=-0.8;
    p1++;

    p1->x=1.0;p1->y=-1.0;p1->z=0.8;
    p1++;
    p1->x=1.0;p1->y=-1.0;p1->z=0.6;
    p1++;
    p1->x=1.0;p1->y=-1.0;p1->z=0.4;
    p1++;
    p1->x=1.0;p1->y=-1.0;p1->z=0.2;
    p1++;
    p1->x=1.0;p1->y=-1.0;p1->z=0.0;
    p1++;
    p1->x=1.0;p1->y=-1.0;p1->z=-0.2;
    p1++;
    p1->x=1.0;p1->y=-1.0;p1->z=-0.4;
    p1++;
    p1->x=1.0;p1->y=-1.0;p1->z=-0.6;
    p1++;
    p1->x=1.0;p1->y=-1.0;p1->z=-0.8;
  }
static void initialize_ball(gint rings_i, gint points_per_ring_i)
  {
    gint i=0;
    gint j=0;
   
    rings=rings_i;
    points_per_ring=points_per_ring_i;

    if(rings<4||rings>50)
      {
        rings=30;
        g_print("Range for rings is 5<=x<=50. Set default rings=30.\n");
      }
    if(points_per_ring<4||points_per_ring>50)
      {
        points_per_ring=30;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=30.\n");
      }

    gint ball_array_size=rings*(points_per_ring+1)+2;

    ball=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), ball_array_size);
    g_array_set_size(ball, ball_array_size);
    ball_t=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), ball_array_size);
    g_array_set_size(ball_t, ball_array_size);

    struct point3d *p1=&g_array_index(ball, struct point3d, 0);
     
    //Multiplying by 3.0 seems to help with drawing the quads smoothly???    
    gint ring_points=points_per_ring+1;
    gdouble arc1=180.0/(gdouble)(rings+1);
    gdouble arc2=360.0/(gdouble)(points_per_ring);
    for(i=0;i<rings;i++)
      {
        for(j=0;j<ring_points;j++)
          {
            (*p1).x=(3.0*sin(arc1*(gdouble)(i+1)*G_PI/180.0)*cos(arc2*(gdouble)j*G_PI/180.0));
            (*p1).y=(3.0*sin(arc1*(gdouble)(i+1)*G_PI/180.0)*sin(arc2*(gdouble)j*G_PI/180.0));
            (*p1).z=(3.0*cos(arc1*(gdouble)(i+1)*G_PI/180.0));
            p1++;
          }
      }
    //Top.
    (*p1).x=0.0;
    (*p1).y=0.0;
    (*p1).z=3.0;
    p1++;
    //Bottom.
    (*p1).x=0.0;
    (*p1).y=0.0;
    (*p1).z=-3.0;
  }
static void initialize_cylinder(gint points_per_ring_i)
  {
    //Only 2 rings in the cylinder.
    gint i=0;
    gint j=0;

    points_per_ring_c=points_per_ring_i;

    if(points_per_ring_c<10||points_per_ring_c>50)
      {
        points_per_ring=16;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=16.\n");
      }

    gint cylinder_array_size=2*(points_per_ring_c+1);

    cylinder=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), cylinder_array_size);
    g_array_set_size(cylinder, cylinder_array_size);
    cylinder_t=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), cylinder_array_size);
    g_array_set_size(cylinder_t, cylinder_array_size);

    struct point3d *p1=&g_array_index(cylinder, struct point3d, 0);

    gdouble arc1=360.0/(gdouble)(points_per_ring_c);
    gdouble z_value=1.0;
    for(i=0;i<2;i++)
      {
        for(j=0;j<points_per_ring_c+1;j++)
          {
            (*p1).x=(cos(arc1*(gdouble)j*G_PI/180.0));
            (*p1).y=(sin(arc1*(gdouble)j*G_PI/180.0));
            (*p1).z=z_value;
            p1++;
          }
        z_value=-1.0;
      }
  }
static void initialize_checkerboard()
  {
    gint i=0;
    gint j=0;

    //A 64 square 81 point checkerboard.
    checkerboard=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 81);
    g_array_set_size(checkerboard, 81);
    checkerboard_t=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 81);
    g_array_set_size(checkerboard_t, 81);

    struct point3d *p1=&g_array_index(checkerboard, struct point3d, 0);

    for(i=0;i<9;i++)
      {
        for(j=0;j<9;j++)
          {
            (*p1).x=(gdouble)j-4.0;
            (*p1).y=(gdouble)i-4.0;
            (*p1).z=0.0;
            p1++;
          }
      }
  }
static void initialize_bezier_spiral()
  {
    gint i=0;
    gint j=0;
    gdouble radius=1.0;
    gdouble z_value=1.0;
    gdouble radius_inc=0.8/160.0;
    gdouble z_inc=2.0/160.0;

    spiral=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 160);
    g_array_set_size(spiral, 160);
    spiral_t=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 160);
    g_array_set_size(spiral_t, 160);

    struct point3d *p1=&g_array_index(spiral, struct point3d, 0);

    for(i=0;i<10;i++)
      {
        for(j=0;j<16;j++)
          {
            (*p1).x=radius*cos((gdouble)j/16.0*2.0*G_PI);
            (*p1).y=radius*sin((gdouble)j/16.0*2.0*G_PI);
            (*p1).z=z_value;
            z_value-=z_inc;
            radius-=radius_inc;
            p1++;
          }
      }
  }
static void initialize_graph_points(gint num_points)
  {
    gint i=0;

    graph_points=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), num_points+6);
    g_array_set_size(graph_points, num_points+6);
    graph_points_t=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), num_points+6);
    g_array_set_size(graph_points_t, num_points+6);

    struct point3d *p1=&g_array_index(graph_points, struct point3d, 0);
     
    //The first six points are for the x,y,z axis.
    (*p1).x=-1.0;(*p1).y=0.0;(*p1).z=0.0;
    p1++;
    (*p1).x=1.0;(*p1).y=0.0;(*p1).z=0.0;
    p1++;
    (*p1).x=0.0;(*p1).y=-1.0;(*p1).z=0.0;
    p1++;
    (*p1).x=0.0;(*p1).y=1.0;(*p1).z=0.0;
    p1++;
    (*p1).x=0.0;(*p1).y=0.0;(*p1).z=-1.0;
    p1++;
    (*p1).x=0.0;(*p1).y=0.0;(*p1).z=1.0;
    p1++;


    //Some random spherical coordinates.
    for(i=0;i<num_points;i++)
      {       
        (*p1).x=sin(g_random_double_range(-1.0, 1.0)*2.0*G_PI)*cos(g_random_double_range(-1.0, 1.0)*2.0*G_PI);
        (*p1).y=sin(g_random_double_range(-1.0, 1.0)*2.0*G_PI)*sin(g_random_double_range(-1.0, 1.0)*2.0*G_PI);
        (*p1).z=cos(g_random_double_range(-1.0, 1.0)*2.0*G_PI);
        p1++;
      }
  }
static gboolean draw_main_window(GtkWidget *window, cairo_t *cr, gpointer data)
  {
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.3);
    cairo_paint(cr);

    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
    gint width=gtk_paned_get_position(GTK_PANED(data));
    gint height=gtk_widget_get_allocated_height(window);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_rectangle(cr, width, 0.0, 10, height);
    cairo_fill(cr);

    return FALSE;
  }
static void show_frame_rate(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_function_time(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) function_time=TRUE;
    else function_time=FALSE;
  }
static void rotate_drawing(GtkComboBox *combo, gpointer data)
  {   
    combo_pitch=0.0;
    combo_roll=0.0;
    combo_yaw=0.0;
    combo_perspective=0.0;
 
    if(gtk_combo_box_get_active(combo)==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(data), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(data));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(data), (GtkTickCallback)animate_drawing, NULL, NULL);
          }
      }
    
  }
static void set_shape_id(GtkComboBox *combo, gpointer data)
  {  
    shape_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_perspective_id(GtkComboBox *combo, gpointer data)
  {  
    perspective_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_stress_id(GtkComboBox *combo, gpointer data)
  {  
    stress_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_pitch=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_roll=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_yaw=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>100.0) combo_perspective=100.0;
    else if(value<0.0) combo_perspective=0.0;
    else combo_perspective=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data)
  {
    static gboolean up=TRUE;

    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    if(combo_pitch>360.0) combo_pitch=0.0;
    else combo_pitch+=0.5;
    if(combo_roll>360.0) combo_roll=0.0;
    else combo_roll+=0.5;
    if(combo_yaw>360.0) combo_yaw=0.0;
    else combo_yaw+=0.5;

    if(combo_perspective>100.0) up=FALSE;
    else if(combo_perspective<0.0) up=TRUE; 
    if(up) combo_perspective+=0.5;
    else combo_perspective-=0.5;

    gtk_widget_queue_draw(GTK_WIDGET(drawing));
    return G_SOURCE_CONTINUE;
  }
//The top drawing function.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;

    //Paint background transparent.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.0);
    cairo_paint(cr);

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 3.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Draw in the center
    cairo_translate(cr, width/2.0, height/2.0);
    //Sometimes there are small extra line fragments if CAIRO_LINE_JOIN_MITER is used.
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND); 
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    gdouble pitch=combo_pitch*G_PI/180.0;
    gdouble roll=combo_roll*G_PI/180.0;
    gdouble yaw=combo_yaw*G_PI/180.0;
    gdouble qrs[9];     
 
    //Time the transforms.
    GTimer *timer1=g_timer_new();
    //Reset to initial values.
    if(shape_id==0) reset_draw_cube();
    else if(shape_id==1) reset_draw_ball();
    else if(shape_id==2) reset_draw_cylinder();
    else if(shape_id==3) reset_draw_checkerboard();
    else if(shape_id==4) reset_draw_bezier_spiral();
    else reset_draw_graph_points();
    //Get a rotation matrix.
    quaternion_rotation(yaw, roll, pitch, qrs);                                           
    //Remember matrix multiplication isn't commutative or the order of matrix multiplication matters.   
    if(stress_id==1)
      {
        //A simple stress matrix.
        const gdouble sm[9]={2.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0};
        change_stress(sm);
      }
    if(stress_id==2)
      {
        const gdouble sm[9]={1.0, 0.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0, 1.0};
        change_stress(sm);
      }
    if(stress_id==3)
      {
        const gdouble sm[9]={1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 2.0};
        change_stress(sm);
      }
    if(perspective_id==0) change_perspective(1.0);
    if(perspective_id==1) change_perspective(3.0);
    rotate_shape(qrs);
    if(function_time) g_print("Transform time %f\n", g_timer_elapsed(timer1, NULL));

    //Time the drawings.
    g_timer_start(timer1);
    if(shape_id==0)
      {
        if(perspective_id==0) draw_cube(cr, 80.0);
        else draw_cube(cr, 60.0);
      }
    else if(shape_id==1)
      {
        draw_ball(cr, 0.75*w1, 0.75*h1);
      }
    else if(shape_id==2)
      {
        draw_cylinder(cr, 2.0*w1, 2.0*h1);
      }
    else if(shape_id==3)
      {
        draw_checkerboard(cr, 0.5*w1, 0.5*h1);
      }
    else if(shape_id==4)
      {
        draw_bezier_spiral(cr, 2.0*w1, 2.0*h1);
      }
    else
      {
        draw_graph_points(cr, 2.0*w1, 2.0*h1);
      }
    if(function_time) g_print("     Draw time %f\n", g_timer_elapsed(timer1, NULL));    
    g_timer_destroy(timer1);
     
    return FALSE;
  }
static void reset_draw_cube()
  {
    //Start with a new copy of the original cube, lines and reference vector.
    struct point3d *p1=&g_array_index(cube, struct point3d, 0);
    struct point3d *p2=&g_array_index(cube_t, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(cube->len));
    p1=&g_array_index(lines, struct point3d, 0);
    p2=&g_array_index(lines_t, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(lines->len));

    v1_t=v1;
  }
static void reset_draw_ball()
  {
    struct point3d *p1=&g_array_index(ball, struct point3d, 0);
    struct point3d *p2=&g_array_index(ball_t, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(ball->len));

    v1_t=v1;
    v1_t.z=-3.0;
  }
static void reset_draw_cylinder()
  {
    struct point3d *p1=&g_array_index(cylinder, struct point3d, 0);
    struct point3d *p2=&g_array_index(cylinder_t, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(cylinder->len));

    v1_t=v1;
  }
static void reset_draw_checkerboard()
  {
    struct point3d *p1=&g_array_index(checkerboard, struct point3d, 0);
    struct point3d *p2=&g_array_index(checkerboard_t, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(checkerboard->len));

    v1_t=v1;
  }
static void reset_draw_bezier_spiral()
  {
    struct point3d *p1=&g_array_index(spiral, struct point3d, 0);
    struct point3d *p2=&g_array_index(spiral_t, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(spiral->len));

    v1_t=v1;
  }
static void reset_draw_graph_points()
  {
    struct point3d *p1=&g_array_index(graph_points, struct point3d, 0);
    struct point3d *p2=&g_array_index(graph_points_t, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(graph_points->len));

    v1_t=v1;
  }
static void change_stress(const gdouble sm[9])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0;   
    struct point3d *p1=NULL;

    if(shape_id==0)
      {
        p1=&g_array_index(cube_t, struct point3d, 0);
        for(i=0;i<(cube_t->len);i++)
          {
            cr1=(p1->x);
            cr2=(p1->y);
            cr3=(p1->z);      
            p1->x=(cr1*sm[0])+(cr2*sm[1])+(cr3*sm[2]);
            p1->y=(cr1*sm[3])+(cr2*sm[4])+(cr3*sm[5]);
            p1->z=(cr1*sm[6])+(cr2*sm[7])+(cr3*sm[8]);           
            p1++;
          }    
        p1=&g_array_index(lines_t, struct point3d, 0);
        for(i=0;i<(lines_t->len);i++)
          {   
            cr1=(p1->x);
            cr2=(p1->y);
            cr3=(p1->z);      
            p1->x=(cr1*sm[0])+(cr2*sm[1])+(cr3*sm[2]);
            p1->y=(cr1*sm[3])+(cr2*sm[4])+(cr3*sm[5]);
            p1->z=(cr1*sm[6])+(cr2*sm[7])+(cr3*sm[8]); 
            p1++;
          }
      }
    else if(shape_id==1)
      {
        p1=&g_array_index(ball_t, struct point3d, 0);
        for(i=0;i<(ball_t->len);i++)
          {
            cr1=(p1->x);
            cr2=(p1->y);
            cr3=(p1->z);      
            p1->x=(cr1*sm[0])+(cr2*sm[1])+(cr3*sm[2]);
            p1->y=(cr1*sm[3])+(cr2*sm[4])+(cr3*sm[5]);
            p1->z=(cr1*sm[6])+(cr2*sm[7])+(cr3*sm[8]); 
            p1++;
          }   
      }
    else if(shape_id==2)
      {
        p1=&g_array_index(cylinder_t, struct point3d, 0);
        for(i=0;i<(cylinder_t->len);i++)
          {
            cr1=(p1->x);
            cr2=(p1->y);
            cr3=(p1->z);      
            p1->x=(cr1*sm[0])+(cr2*sm[1])+(cr3*sm[2]);
            p1->y=(cr1*sm[3])+(cr2*sm[4])+(cr3*sm[5]);
            p1->z=(cr1*sm[6])+(cr2*sm[7])+(cr3*sm[8]); 
            p1++;
          }   
      }
    else if(shape_id==3)
      {
        p1=&g_array_index(checkerboard_t, struct point3d, 0);
        for(i=0;i<(checkerboard_t->len);i++)
          {
            cr1=(p1->x);
            cr2=(p1->y);
            cr3=(p1->z);      
            p1->x=(cr1*sm[0])+(cr2*sm[1])+(cr3*sm[2]);
            p1->y=(cr1*sm[3])+(cr2*sm[4])+(cr3*sm[5]);
            p1->z=(cr1*sm[6])+(cr2*sm[7])+(cr3*sm[8]); 
            p1++;
          }   
      }
    else if(shape_id==4)
      {
        p1=&g_array_index(spiral_t, struct point3d, 0);
        for(i=0;i<(spiral_t->len);i++)
          {
            cr1=(p1->x);
            cr2=(p1->y);
            cr3=(p1->z);      
            p1->x=(cr1*sm[0])+(cr2*sm[1])+(cr3*sm[2]);
            p1->y=(cr1*sm[3])+(cr2*sm[4])+(cr3*sm[5]);
            p1->z=(cr1*sm[6])+(cr2*sm[7])+(cr3*sm[8]); 
            p1++;
          }   
      } 
    else
      {
        p1=&g_array_index(graph_points_t, struct point3d, 0);
        for(i=0;i<(graph_points_t->len);i++)
          {
            cr1=(p1->x);
            cr2=(p1->y);
            cr3=(p1->z);      
            p1->x=(cr1*sm[0])+(cr2*sm[1])+(cr3*sm[2]);
            p1->y=(cr1*sm[3])+(cr2*sm[4])+(cr3*sm[5]);
            p1->z=(cr1*sm[6])+(cr2*sm[7])+(cr3*sm[8]); 
            p1++;
          }   
      } 
 

    cr1=v1_t.x;
    cr2=v1_t.y;
    cr3=v1_t.z;      
    v1_t.x=(cr1*sm[0])+(cr2*sm[1])+(cr3*sm[2]);
    v1_t.y=(cr1*sm[3])+(cr2*sm[4])+(cr3*sm[5]);
    v1_t.z=(cr1*sm[6])+(cr2*sm[7])+(cr3*sm[8]);    
  }
static void change_perspective(gdouble scale)
  {
    /*
      This perspective function doesn't use a standard 3x3 matrix. This doesn't make it easy
      to multiply matrices and then apply the resulting matrix to the points.???
    */
    gint i=0;
    struct point3d *p1=NULL;
    struct point3d *p2=NULL;
    gdouble perspective1=0.0;
    gdouble perspective2=0.0;
    gdouble translate_z=0.0;

    if(shape_id==0)
      {
        p1=&g_array_index(cube, struct point3d, 0);
        p2=&g_array_index(cube_t, struct point3d, 0);
        perspective1=combo_perspective/100.0;
        translate_z=perspective1*v1_t.z*scale;
        for(i=0;i<(cube_t->len);i++)
          {   
            perspective2=(1.0+(perspective1*(p1->z)*scale));
            p2->x=(p2->x)*perspective2;
            p2->y=(p2->y)*perspective2;
            p2->z=(p2->z)*perspective2+translate_z;
            p1++;p2++;
          }    
        p1=&g_array_index(lines, struct point3d, 0);
        p2=&g_array_index(lines_t, struct point3d, 0);
        for(i=0;i<(lines_t->len);i++)
          {   
            perspective2=(1.0+(perspective1*(p1->z)*scale));
            p2->x=(p2->x)*perspective2;
            p2->y=(p2->y)*perspective2;
            p2->z=(p2->z)*perspective2+translate_z;
            p1++;p2++;
          }
      }
    else if(shape_id==1)
      {
        p1=&g_array_index(ball, struct point3d, 0);
        p2=&g_array_index(ball_t, struct point3d, 0);
        perspective1=combo_perspective/300.0;
        translate_z=perspective1*v1_t.z*scale; 
        for(i=0;i<(ball_t->len);i++)
          {  
            perspective2=(1.0+(perspective1*(p1->z)*scale));
            p2->x=(p2->x)*perspective2;
            p2->y=(p2->y)*perspective2;
            p2->z=(p2->z)*perspective2+translate_z;
            p1++;p2++;
          }
      }
    else if(shape_id==2)
      {
        p1=&g_array_index(cylinder, struct point3d, 0);
        p2=&g_array_index(cylinder_t, struct point3d, 0);
        perspective1=combo_perspective/100.0;
        translate_z=perspective1*v1_t.z*scale; 
        for(i=0;i<(cylinder_t->len);i++)
          {  
            perspective2=(1.0+(perspective1*(p1->z)*scale));
            p2->x=(p2->x)*perspective2;
            p2->y=(p2->y)*perspective2;
            p2->z=(p2->z)*perspective2+translate_z;
            p1++;p2++;
          }
      }
    else if(shape_id==3)
      {
        p1=&g_array_index(checkerboard, struct point3d, 0);
        p2=&g_array_index(checkerboard_t, struct point3d, 0);
        perspective1=combo_perspective/400.0;
        translate_z=perspective1*v1_t.x*scale;
        for(i=0;i<(checkerboard_t->len);i++)
          {  
            perspective2=(1.0+(perspective1*(p1->x)*scale));
            p2->x=(p2->x)*perspective2;
            p2->y=(p2->y)*perspective2;
            p2->z=(p2->z)*perspective2+translate_z;
            p1++;p2++;
          }
      }
    else if(shape_id==4)
      {
        p1=&g_array_index(spiral, struct point3d, 0);
        p2=&g_array_index(spiral_t, struct point3d, 0);
        perspective1=combo_perspective/100.0;
        translate_z=perspective1*v1_t.z*scale;
        for(i=0;i<(spiral_t->len);i++)
          {  
            perspective2=(1.0+(perspective1*(p1->z)*scale));
            p2->x=(p2->x)*perspective2;
            p2->y=(p2->y)*perspective2;
            p2->z=(p2->z)*perspective2+translate_z;
            p1++;p2++;
          }
      }
    else
      {
        p1=&g_array_index(graph_points, struct point3d, 0);
        p2=&g_array_index(graph_points_t, struct point3d, 0);
        perspective1=combo_perspective/100.0;
        translate_z=perspective1*v1_t.z*scale;
        for(i=0;i<(graph_points_t->len);i++)
          {  
            perspective2=(1.0+(perspective1*(p1->z)*scale));
            p2->x=(p2->x)*perspective2;
            p2->y=(p2->y)*perspective2;
            p2->z=(p2->z)*perspective2+translate_z;
            p1++;p2++;
          }
      }
  }
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
static void rotate_shape(gdouble qrs[9])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0;   
    struct point3d *p1=NULL;

    if(shape_id==0)
      {
        p1=&g_array_index(cube_t, struct point3d, 0);
        for(i=0;i<(cube_t->len);i++)
          {
            cr1=(p1->x);
            cr2=(p1->y);
            cr3=(p1->z);         
            p1->x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
            p1->y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
            p1->z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
            p1++;
          }    
        p1=&g_array_index(lines_t, struct point3d, 0);
        for(i=0;i<(lines_t->len);i++)
          {   
            cr1=(p1->x);
            cr2=(p1->y);
            cr3=(p1->z);         
            p1->x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
            p1->y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
            p1->z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
            p1++;
          }
      }
    else if(shape_id==1)
      {
        p1=&g_array_index(ball_t, struct point3d, 0);
        for(i=0;i<(ball_t->len);i++)
          {
            cr1=(p1->x);
            cr2=(p1->y);
            cr3=(p1->z);         
            p1->x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
            p1->y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
            p1->z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
            p1++;
          }   
      }
    else if(shape_id==2)
      {
        p1=&g_array_index(cylinder_t, struct point3d, 0);
        for(i=0;i<(cylinder_t->len);i++)
          {
            cr1=(p1->x);
            cr2=(p1->y);
            cr3=(p1->z);         
            p1->x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
            p1->y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
            p1->z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
            p1++;
          }   
      }
    else if(shape_id==3)
      {
        p1=&g_array_index(checkerboard_t, struct point3d, 0);
        for(i=0;i<(checkerboard_t->len);i++)
          {
            cr1=(p1->x);
            cr2=(p1->y);
            cr3=(p1->z);         
            p1->x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
            p1->y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
            p1->z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
            p1++;
          }   
      }
    else if(shape_id==4)
      {
        p1=&g_array_index(spiral_t, struct point3d, 0);
        for(i=0;i<(spiral_t->len);i++)
          {
            cr1=(p1->x);
            cr2=(p1->y);
            cr3=(p1->z);         
            p1->x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
            p1->y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
            p1->z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
            p1++;
          }   
      }
    else
      {
        p1=&g_array_index(graph_points_t, struct point3d, 0);
        for(i=0;i<(graph_points_t->len);i++)
          {
            cr1=(p1->x);
            cr2=(p1->y);
            cr3=(p1->z);         
            p1->x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
            p1->y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
            p1->z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
            p1++;
          }   
      }
  }
static void draw_cube(cairo_t *cr, gdouble scale)
  {
    gint i=0;
    gint j=0;
    gdouble slope_x1=0.0;
    gdouble slope_y1=0.0;
    gdouble slope_x2=0.0;
    gdouble slope_y2=0.0;
    gdouble x1=0.0;
    gdouble y1=0.0;
    gdouble x2=0.0;
    gdouble y2=0.0;
    struct point3d *p1=&g_array_index(cube_t, struct point3d, 0);
    struct point3d *p2=&g_array_index(lines_t, struct point3d, 0);
    //An array to sort quads.
    struct quad *quads_start=g_malloc(6*sizeof(struct quad));
    struct quad *quads=quads_start;
    
    (*quads).id=0;
    (*quads).z=((*(p1)).z)+((*(p1+1)).z)+((*(p1+2)).z)+((*(p1+3)).z);
    quads++;
    (*quads).id=1;
    (*quads).z=((*(p1+4)).z)+((*(p1+5)).z)+((*(p1+6)).z)+((*(p1+7)).z);
    quads++;
    (*quads).id=2;
    (*quads).z=((*(p1)).z)+((*(p1+1)).z)+((*(p1+5)).z)+((*(p1+4)).z);
    quads++;
    (*quads).id=3;
    (*quads).z=((*(p1+1)).z)+((*(p1+2)).z)+((*(p1+6)).z)+((*(p1+5)).z);
    quads++;
    (*quads).id=4;
    (*quads).z=((*(p1+2)).z)+((*(p1+3)).z)+((*(p1+7)).z)+((*(p1+6)).z);
    quads++;
    (*quads).id=5;
    (*quads).z=((*(p1+3)).z)+((*(p1)).z)+((*(p1+4)).z)+((*(p1+7)).z);
    quads++;

    //Sort array based on z values.
    qsort(quads_start, 6, sizeof(struct quad), compare_quads);

    //Just draw the top three cube planes.
    quads=quads_start;
    for(i=0;i<6;i++)
      {
        if((*quads).id==0)
          {
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.5);
            cairo_move_to(cr, ((*(p1)).x)*scale, ((*(p1)).y)*scale);
            cairo_line_to(cr, ((*(p1+1)).x)*scale, ((*(p1+1)).y)*scale);
            cairo_line_to(cr, ((*(p1+2)).x)*scale, ((*(p1+2)).y)*scale);
            cairo_line_to(cr, ((*(p1+3)).x)*scale, ((*(p1+3)).y)*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else if((*quads).id==1)
          {
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.5);
            cairo_move_to(cr, ((*(p1+4)).x)*scale, ((*(p1+4)).y)*scale);
            cairo_line_to(cr, ((*(p1+5)).x)*scale, ((*(p1+5)).y)*scale);
            cairo_line_to(cr, ((*(p1+6)).x)*scale, ((*(p1+6)).y)*scale);
            cairo_line_to(cr, ((*(p1+7)).x)*scale, ((*(p1+7)).y)*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else if((*quads).id==2)
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, ((*(p1)).x)*scale, ((*(p1)).y)*scale);
            cairo_line_to(cr, ((*(p1+1)).x)*scale, ((*(p1+1)).y)*scale);
            cairo_line_to(cr, ((*(p1+5)).x)*scale, ((*(p1+5)).y)*scale);
            cairo_line_to(cr, ((*(p1+4)).x)*scale, ((*(p1+4)).y)*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);

            //Draw perspective lines.
            cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
            slope_x1=(((*(p1+1)).x)-((*(p1)).x))/10.0; 
            slope_y1=(((*(p1+1)).y)-((*(p1)).y))/10.0;
            slope_x2=(((*(p1+5)).x)-((*(p1+4)).x))/10.0; 
            slope_y2=(((*(p1+5)).y)-((*(p1+4)).y))/10.0;             
            for(j=1;j<10;j++)
              {
                x1=slope_x1*(gdouble)(j);
                y1=slope_y1*(gdouble)(j);
                x2=slope_x2*(gdouble)(j);
                y2=slope_y2*(gdouble)(j);
                cairo_move_to(cr, ((*(p1)).x+x1)*scale, ((*(p1)).y+y1)*scale);
                cairo_line_to(cr, ((*(p1+4)).x+x2)*scale, ((*(p1+4)).y+y2)*scale);
                cairo_stroke(cr);
              }

          }
        else if((*quads).id==3)
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, ((*(p1+1)).x)*scale, ((*(p1+1)).y)*scale);
            cairo_line_to(cr, ((*(p1+2)).x)*scale, ((*(p1+2)).y)*scale);
            cairo_line_to(cr, ((*(p1+6)).x)*scale, ((*(p1+6)).y)*scale);
            cairo_line_to(cr, ((*(p1+5)).x)*scale, ((*(p1+5)).y)*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else if((*quads).id==4)
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, ((*(p1+2)).x)*scale, ((*(p1+2)).y)*scale);
            cairo_line_to(cr, ((*(p1+3)).x)*scale, ((*(p1+3)).y)*scale);
            cairo_line_to(cr, ((*(p1+7)).x)*scale, ((*(p1+7)).y)*scale);
            cairo_line_to(cr, ((*(p1+6)).x)*scale, ((*(p1+6)).y)*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
            //Draw a few more lines for perspective.
            cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
            for(j=0;j<9;j++)
              {
                cairo_move_to(cr, ((*(p2+j)).x)*scale, ((*(p2+j)).y)*scale);
                cairo_line_to(cr, ((*(p2+j+9)).x)*scale, ((*(p2+j+9)).y)*scale);
                cairo_stroke(cr);
              }

            cairo_move_to(cr, ((*(p2)).x)*scale, ((*(p2)).y)*scale);
            for(j=1;j<9;j++)
              {
                cairo_line_to(cr, ((*(p2+j)).x)*scale, ((*(p2+j)).y)*scale);
              }
            cairo_stroke(cr);

            cairo_move_to(cr, ((*(p2+9)).x)*scale, ((*(p2+9)).y)*scale);
            for(j=1;j<9;j++)
              {
                cairo_line_to(cr, ((*(p2+j+9)).x)*scale, ((*(p2+j+9)).y)*scale);
              }
            cairo_stroke(cr);
          }
        else
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, ((*(p1+3)).x)*scale, ((*(p1+3)).y)*scale);
            cairo_line_to(cr, ((*(p1)).x)*scale, ((*(p1)).y)*scale);
            cairo_line_to(cr, ((*(p1+4)).x)*scale, ((*(p1+4)).y)*scale);
            cairo_line_to(cr, ((*(p1+7)).x)*scale, ((*(p1+7)).y)*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        quads++;
        }

    g_free(quads_start);  
  }
static void draw_ball(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;
    gint j=0;
    gint counter=0;

    struct point3d *p1=&g_array_index(ball_t, struct point3d, 0);
    struct point3d *top=&g_array_index(ball_t, struct point3d, (ball_t->len)-2);
    struct point3d *bottom=&g_array_index(ball_t, struct point3d, (ball_t->len)-1);

    gint offset1=(points_per_ring)+2;
    gint offset2=(points_per_ring)+1;

    //An array to sort quads.
    struct quad_plane *quads_start=g_malloc((rings+1)*points_per_ring*sizeof(struct quad_plane));
    struct quad_plane *quads=quads_start;

    //Fill the middle quads.
    for(i=0;i<rings-1;i++)
      {
        for(j=0;j<points_per_ring;j++)
          {              
            quads->x1=((*(p1)).x);
            quads->y1=((*(p1)).y);
            quads->z1=((*(p1)).z);
            quads->x2=((*(p1+1)).x);
            quads->y2=((*(p1+1)).y);
            quads->z2=((*(p1+1)).z);            
            quads->x3=((*(p1+offset1)).x);
            quads->y3=((*(p1+offset1)).y);
            quads->z3=((*(p1+offset1)).z);
            quads->x4=((*(p1+offset2)).x);
            quads->y4=((*(p1+offset2)).y);
            quads->z4=((*(p1+offset2)).z);
            //Plane color.
            if(counter%2==0)
              {
                quads->r=1.0;
                quads->g=0.0;
                quads->b=1.0;
                quads->a=1.0;
              }
            else
              {
                quads->r=1.0;
                quads->g=1.0;
                quads->b=0.0;
                quads->a=1.0;
              } 
            p1++;           
            counter++;
            quads++;
          }
        p1++;
      }

    p1=&g_array_index(ball_t, struct point3d, 0);
    for(i=0;i<points_per_ring;i++)
      {
        quads->x1=((*(p1)).x);
        quads->y1=((*(p1)).y);
        quads->z1=((*(p1)).z);
        quads->x2=((*(p1+1)).x);
        quads->y2=((*(p1+1)).y);
        quads->z2=((*(p1+1)).z);            
        quads->x3=top->x;
        quads->y3=top->y;
        quads->z3=((*(p1)).z);
        quads->x4=top->x;
        quads->y4=top->y;
        quads->z4=((*(p1+1)).z); 
        if(counter%2==0)
          {
            quads->r=1.0;
            quads->g=0.0;
            quads->b=1.0;
            quads->a=1.0;
          }
        else
          {
            quads->r=1.0;
            quads->g=1.0;
            quads->b=0.0;
            quads->a=1.0;
          }
        p1++;
        counter++;
        quads++;
      }

    p1=&g_array_index(ball_t, struct point3d, (ball_t->len)-points_per_ring-3);
    for(i=0;i<points_per_ring;i++)
      {
        quads->x1=((*(p1)).x);
        quads->y1=((*(p1)).y);
        quads->z1=((*(p1)).z);
        quads->x2=((*(p1+1)).x);
        quads->y2=((*(p1+1)).y);
        quads->z2=((*(p1+1)).z);            
        quads->x3=bottom->x;
        quads->y3=bottom->y;
        quads->z3=((*(p1)).z);
        quads->x4=bottom->x;
        quads->y4=bottom->y;
        quads->z4=((*(p1+1)).z); 
        if(counter%2==0)
          {
            quads->r=1.0;
            quads->g=0.0;
            quads->b=1.0;
            quads->a=1.0;
          }
        else
          {
            quads->r=1.0;
            quads->g=1.0;
            quads->b=0.0;
            quads->a=1.0;
          }
        p1++;
        counter++;
        quads++;
      }
   
    //Sort the quad plane array based on z values.
    qsort(quads_start, (rings+1)*points_per_ring, sizeof(struct quad_plane), compare_quads2);

    //Draw the sorted quads.
    quads=quads_start;
    //Setting antialias to none seems to fill the quads with color better.
    cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
    for(i=0;i<rings+1;i++)
      {
        for(j=0;j<points_per_ring;j++)
          {
            cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
            cairo_move_to(cr, w1*quads->x1, w1*quads->y1);
            cairo_line_to(cr, w1*quads->x2, w1*quads->y2);
            cairo_line_to(cr, w1*quads->x3, w1*quads->y3);
            cairo_line_to(cr, w1*quads->x4, w1*quads->y4);
            cairo_close_path(cr);
            cairo_fill(cr);
            quads++;
          }
      }

    g_free(quads_start); 
  }
static void draw_cylinder(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0; 
    struct point3d *p1=&g_array_index(cylinder_t, struct point3d, 0);
    struct point3d *p2=&g_array_index(cylinder_t, struct point3d, 1);
    gint offset=points_per_ring_c+1;
    
    for(i=0;i<points_per_ring_c;i++)
      {
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        cairo_move_to(cr, w1*(p1->x), w1*(p1->y));
        cairo_line_to(cr, w1*((p1+offset)->x), w1*((p1+offset)->y));
        cairo_line_to(cr, w1*((p2+offset)->x), w1*((p2+offset)->y));
        cairo_line_to(cr, w1*(p2->x), w1*(p2->y));
        cairo_close_path(cr);
        cairo_stroke_preserve(cr);
        cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 0.5);
        cairo_fill(cr);
        p1++;p2++;       
      }

  }
static void draw_checkerboard(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;
    gint j=0;
    gint counter=0;
    struct point3d *p1=&g_array_index(checkerboard_t, struct point3d, 0);
    struct point3d *p2=&g_array_index(checkerboard_t, struct point3d, 9);
    
    //Draw the 64 squares of the checkerboard.
    for(i=0;i<8;i++)
      {
        for(j=0;j<8;j++)
          {
            counter++;
            if(counter%2==0) cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            else cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
            cairo_move_to(cr, w1*(p1->x), w1*(p1->y));
            cairo_line_to(cr, w1*((p1+1)->x), w1*((p1+1)->y));
            cairo_line_to(cr, w1*((p2+1)->x), w1*((p2+1)->y));
            cairo_line_to(cr, w1*(p2->x), w1*(p2->y));
            cairo_close_path(cr);
            cairo_fill(cr);
            p1++;p2++;
          }
        p1++;p2++;
        counter++;
      }
  }
int compare_quads(const void *a, const void *b)
  {
    return(  (((struct quad*)a)->z) - (((struct quad*)b)->z) );
  }
int compare_quads2(const void *a, const void *b)
  {
    return(  (((struct quad_plane*)a)->z1+((struct quad_plane*)a)->z2+((struct quad_plane*)a)->z3+((struct quad_plane*)a)->z4) - (((struct quad_plane*)b)->z1+((struct quad_plane*)b)->z2+((struct quad_plane*)b)->z3+((struct quad_plane*)b)->z4));
  }
static void draw_bezier_spiral(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;
    GArray *control1=control_points_from_coords_3d(spiral_t);
    struct point3d *p1=NULL;
    struct controls3d *c1=NULL;

    //Draw spiral twice with two different colors.
    cairo_set_line_width(cr, 8.0);
    cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
    p1=&g_array_index(spiral_t, struct point3d, 0);
    c1=&g_array_index(control1, struct controls3d, 0);
    cairo_move_to(cr, w1*(p1->x), w1*(p1->y));
    for(i=1;i<(spiral_t->len);i++)
      {
        p1++;
        cairo_curve_to(cr, w1*(c1->x1), w1*(c1->y1), w1*(c1->x2), w1*(c1->y2), w1*(p1->x), w1*(p1->y));
        c1++; 
      }
    cairo_stroke(cr);

    cairo_set_line_width(cr, 2.0);
    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
    p1=&g_array_index(spiral_t, struct point3d, 0);
    c1=&g_array_index(control1, struct controls3d, 0);
    cairo_move_to(cr, w1*(p1->x), w1*(p1->y));
    for(i=1;i<(spiral_t->len);i++)
      {
        p1++;
        cairo_curve_to(cr, w1*(c1->x1), w1*(c1->y1), w1*(c1->x2), w1*(c1->y2), w1*(p1->x), w1*(p1->y));
        c1++; 
      }
    cairo_stroke(cr);

    if(control1!=NULL) g_array_free(control1, TRUE);
  }
/*
    This is some exellent work done by Ramsundar Shandilya. Note the following for the original work
    and the rational behind it.
    
    https://medium.com/@ramshandilya/draw-smooth-curves-through-a-set-of-points-in-ios-34f6d73c8f9

    https://github.com/Ramshandilya/Bezier

    The MIT License (MIT)

    Copyright (c) 2015 Ramsundar Shandilya

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
  
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

    This is a translation of the original Swift code to C. It makes the function easy to use with GTK+,
    cairo and glib.

    For arrays with 3d points. Just calculate x and y. z=0 for returned array which probably isn't
    needed. Could just use a 2d point array for the control points.
*/
static GArray* control_points_from_coords_3d(const GArray *data_points3d)
  {  
    gint i=0;
    GArray *control_points=NULL;      
    //Number of Segments
    gint count=0;
    if(data_points3d!=NULL) count=data_points3d->len-1;
    gdouble *fCP=NULL;
    gdouble *sCP=NULL;

    if(count>0)
      {
        fCP=(gdouble*)g_malloc(2*count*sizeof(gdouble));
        sCP=(gdouble*)g_malloc(2*count*sizeof(gdouble));
      }
        
    //P0, P1, P2, P3 are the points for each segment, where P0 & P3 are the knots and P1, P2 are the control points.
    if(count<1||data_points3d==NULL)
      {
        //Return NULL.
        control_points=NULL;
        g_warning("Can't get control points from coordinates. NULL returned.\n");
      }
    else if(count==1)
      {
        struct point3d P0=g_array_index(data_points3d, struct point3d, 0);
        struct point3d P3=g_array_index(data_points3d, struct point3d, 1);

        //Calculate First Control Point
        //3P1 = 2P0 + P3
        struct point3d P1;
        P1.x=(2.0*P0.x+P3.x)/3.0;
        P1.y=(2.0*P0.y+P3.y)/3.0;

        *(fCP)=P1.x;
        *(fCP+1)=P1.y;

        //Calculate second Control Point
        //P2 = 2P1 - P0
        struct point3d P2;
        P2.x=(2.0*P1.x-P0.x);
        P2.y=(2.0*P1.y-P0.x);

        *(sCP)=P2.x;
        *(sCP+1)=P2.y;     
      }
    else
      {
        gdouble *rhs=(gdouble*)g_malloc(2*count*sizeof(gdouble));
        gdouble *a=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *b=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *c=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble rhsValueX=0;
        gdouble rhsValueY=0;
        struct point3d P0;
        struct point3d P3;        
        gdouble m=0;
        gdouble b1=0;
        gdouble r2x=0;
        gdouble r2y=0;
        gdouble P1_x=0;
        gdouble P1_y=0;
   
        for(i=0;i<count;i++)
          {
            P0=g_array_index(data_points3d, struct point3d, i);
            P3=g_array_index(data_points3d, struct point3d, i+1);

            if(i==0)
              {
                *(a)=0.0;
                *(b)=2.0;
                *(c)=1.0;

                //rhs for first segment
                rhsValueX=P0.x+2.0*P3.x;
                rhsValueY=P0.y+2.0*P3.y;
              }
            else if(i==count-1)
              {
                *(a+i)=2.0;
                *(b+i)=7.0;
                *(c+i)=0.0;

                //rhs for last segment
                rhsValueX=8.0*P0.x+P3.x;
                rhsValueY=8.0*P0.y+P3.y;
              }
            else
              {
                *(a+i)=1.0;
                *(b+i)=4.0;
                *(c+i)=1.0;

                rhsValueX=4.0*P0.x+2.0*P3.x;
                rhsValueY=4.0*P0.y+2.0*P3.y;
              }
            *(rhs+i*2)=rhsValueX;
            *(rhs+i*2+1)=rhsValueY;
          }

        //Solve Ax=B. Use Tridiagonal matrix algorithm a.k.a Thomas Algorithm
        for(i=1;i<count;i++)
          {
            m=(*(a+i))/(*(b+i-1));

            b1=(*(b+i))-m*(*(c+i-1));
            *(b+i)=b1;

            r2x=(*(rhs+i*2))-m*(*(rhs+(i-1)*2));
            r2y=(*(rhs+i*2+1))-m*(*(rhs+(i-1)*2+1));

            *(rhs+i*2)=r2x;
            *(rhs+i*2+1)=r2y;
          }
        
        //Last control Point
        gdouble lastControlPointX=(*(rhs+2*count-2))/(*(b+count-1));
        gdouble lastControlPointY=(*(rhs+2*count-1))/(*(b+count-1));

        *(fCP+2*count-2)=lastControlPointX;
        *(fCP+2*count-1)=lastControlPointY;

        gdouble controlPointX=0;
        gdouble controlPointY=0;

        for(i=count-2;i>=0;--i)
          {
            controlPointX=(*(rhs+i*2)-(*(c+i))*(*(fCP+(i+1)*2)))/(*(b+i));
            controlPointY=(*(rhs+i*2+1)-(*(c+i))*(*(fCP+(i+1)*2+1)))/(*(b+i));

             *(fCP+i*2)=controlPointX;
             *(fCP+i*2+1)=controlPointY; 
          }

        //Compute second Control Points from first.
        for(i=0;i<count;i++)
          {
            if(i==count-1)
              {
                P3=g_array_index(data_points3d, struct point3d, i+1);
                P1_x=(*(fCP+i*2));
                P1_y=(*(fCP+i*2+1));

                controlPointX=(P3.x+P1_x)/2.0;
                controlPointY=(P3.y+P1_y)/2.0;

                *(sCP+count*2-2)=controlPointX;
                *(sCP+count*2-1)=controlPointY;
              }
            else
              {
                P3=g_array_index(data_points3d, struct point3d, i+1);                
                P1_x=(*(fCP+(i+1)*2));
                P1_y=(*(fCP+(i+1)*2+1));

                controlPointX=2.0*P3.x-P1_x;
                controlPointY=2.0*P3.y-P1_y;

                *(sCP+i*2)=controlPointX;
                *(sCP+i*2+1)=controlPointY;
              }

          }

        control_points=g_array_new(FALSE, FALSE, sizeof(struct controls3d));
        struct controls3d cp;
        for(i=0;i<count;i++)
          {
            cp.x1=(*(fCP+i*2));
            cp.y1=(*(fCP+i*2+1));
            cp.z1=0.0;
            cp.x2=(*(sCP+i*2));
            cp.y2=(*(sCP+i*2+1));
            cp.z2=0.0;
            g_array_append_val(control_points, cp);
          }

        g_free(rhs);
        g_free(a);
        g_free(b);
        g_free(c);
     }

    if(fCP!=NULL) g_free(fCP);
    if(sCP!=NULL) g_free(sCP);

    return control_points;
  }
static void draw_graph_points(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;
    struct point3d *p1=&g_array_index(graph_points_t, struct point3d, 0);

    //Draw spiral twice with two different colors.
    cairo_set_line_width(cr, 8.0);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_BUTT);

    //The x,y and z axis.
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    cairo_move_to(cr, 2.0*w1*(p1->x), 2.0*h1*(p1->y));
    p1++;
    cairo_line_to(cr, 2.0*w1*(p1->x), 2.0*h1*(p1->y));
    cairo_stroke(cr);
    p1++; 
    cairo_move_to(cr, 2.0*w1*(p1->x), 2.0*h1*(p1->y));
    p1++;
    cairo_line_to(cr, 2.0*w1*(p1->x), 2.0*h1*(p1->y));
    cairo_stroke(cr);
    p1++; 
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    cairo_move_to(cr, 2.0*w1*(p1->x), 2.0*h1*(p1->y));
    p1++;
    cairo_line_to(cr, 2.0*w1*(p1->x), 2.0*h1*(p1->y));
    cairo_stroke(cr);
    p1++; 
      
    //Draw the points.
    cairo_set_line_width(cr, 8.0);
    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
    for(i=0;i<((graph_points_t->len)-6);i++)
      {
        cairo_move_to(cr, w1*(p1->x), h1*(p1->y));
        cairo_line_to(cr, w1*(p1->x), h1*(p1->y));
        cairo_stroke(cr);
        p1++; 
      }

  }
