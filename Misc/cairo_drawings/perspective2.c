
/* 
    Test drawing a cube combining 3d rotations, perspective and a stress matrix with cairo.

    This one uses the Graphene library.
        https://developer.gnome.org/graphene/stable/ch01.html
        https://github.com/ebassi/graphene
        

    gcc -Wall -msse2 -std=c11 perspective2.c -o perspective2 `pkg-config --cflags --libs gtk+-3.0` -lm -Wl,-rpath=/home/eric/Graphene/graphene-master/build/src libgraphene-1.0.so

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<stdlib.h>
#include"graphene.h"

//Sort cube faces from back to front.
struct quad{
  gint id;
  gfloat z;
};

//Reference cube and a transformed cube.
graphene_point3d_t *cube=NULL;
graphene_point3d_t *cube_t=NULL;
//Some lines across one face of the cube.
graphene_point3d_t *lines=NULL;
graphene_point3d_t *lines_t=NULL;
//A reference vector. Used for centering the object under perspective.
graphene_point3d_t v1;
graphene_point3d_t v1_t;

//Combo ids for the perspective and stress.
static gint perspective_id=0;
static gint stress_id=0;

//The combo rotation slider values.
static gdouble combo_pitch=0.0;
static gdouble combo_roll=0.0;
static gdouble combo_yaw=0.0;
static gdouble combo_perspective=0.0;

//Tick id for animation frame clock.
static guint tick_id=0;

//For the performance check boxes.
static gboolean frame_rate=FALSE;
static gboolean function_time=FALSE;

static void initialize_points();
//Draw the main window transparent.
static gboolean draw_main_window(GtkWidget *window, cairo_t *cr, gpointer data);
//UI functions for sliders and combo boxes.
static void show_frame_rate(GtkToggleButton *button, gpointer data);
static void show_function_time(GtkToggleButton *button, gpointer data);
static void rotate_drawing(GtkComboBox *combo, gpointer data);
static void set_perspective_id(GtkComboBox *combo, gpointer data);
static void set_stress_id(GtkComboBox *combo, gpointer data);
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data);
//The start of drawing. Get the combo_id's to draw.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer data);
/*
  Some matrix functions for drawing. Reset points, stress matrix, perspective vector, rotation matrix is
  the order of the operations.
*/
static void reset_draw_cube();
static void change_stress(const gfloat sm[16]);
static void change_perspective1();
static void change_perspective2();
static void rotate_cube(graphene_matrix_t *m);
//After the cube points are transformed, draw it.
static void draw_cube(cairo_t *cr, gdouble scale);
int compare_quads(const void *a, const void *b);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Perspective2");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    gtk_widget_set_app_paintable(window, TRUE);
    //Try to set transparency of main window.
    if(gtk_widget_is_composited(window))
      {
        GdkScreen *screen=gtk_widget_get_screen(window);  
        GdkVisual *visual=gdk_screen_get_rgba_visual(screen);
        gtk_widget_set_visual(window, visual);
      }
    else g_print("Can't set window transparency.\n");

    initialize_points();

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);

    GtkWidget *combo_rotate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_rotate, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 0, "1", "Draw Cube");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 1, "2", "Animate Cube");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_rotate), 0);

    GtkWidget *combo_perspective=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_perspective, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_perspective), 0, "1", "Perspective1");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_perspective), 1, "2", "Perspective2");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_perspective), 0);

    GtkWidget *combo_stress=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_stress, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 0, "1", "No Stress");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 1, "2", "Stress Matrix1");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stress), 2, "3", "Stress Matrix2");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_stress), 0);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);

    GtkWidget *perspective_label=gtk_label_new("Perspective");
    gtk_widget_set_hexpand(perspective_label, TRUE);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider, TRUE);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider, TRUE);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider, TRUE);

    GtkWidget *perspective_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 100.0, 1.0);
    gtk_widget_set_vexpand(perspective_slider, TRUE);

    g_signal_connect(da, "draw", G_CALLBACK(draw_main), NULL);
    g_signal_connect(combo_rotate, "changed", G_CALLBACK(rotate_drawing), da);
    g_signal_connect(combo_perspective, "changed", G_CALLBACK(set_perspective_id), da);
    g_signal_connect(combo_stress, "changed", G_CALLBACK(set_stress_id), da);
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_pitch), da);
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_roll), da);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_yaw), da);
    g_signal_connect(perspective_slider, "change-value", G_CALLBACK(set_perspective), da);

    //Some checks for performance.
    GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
    gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
    g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), NULL);

    GtkWidget *check_functions=gtk_check_button_new_with_label("Show Function Time");
    gtk_widget_set_halign(check_functions, GTK_ALIGN_CENTER);
    g_signal_connect(check_functions, "toggled", G_CALLBACK(show_function_time), NULL);

    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_rotate, 0, 0, 6, 1); 
    gtk_grid_attach(GTK_GRID(grid1), combo_perspective, 0, 1, 6, 1); 
    gtk_grid_attach(GTK_GRID(grid1), combo_stress, 0, 2, 6, 1);         
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 3, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid1), perspective_label, 3, 3, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), perspective_slider, 3, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_frame, 0, 5, 6, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_functions, 0, 6, 6, 1);

    GtkWidget *scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll, TRUE);
    gtk_widget_set_vexpand(scroll, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll), grid1);
   
    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), scroll, TRUE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 250);

    g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), paned1);
   
    gtk_container_add(GTK_CONTAINER(window), paned1);
    
    gtk_widget_show_all(window);

    gtk_main();

    free(cube);
    free(cube_t);
    free(lines);
    free(lines_t);

    return 0;  
  }
static void initialize_points()
  {
    cube=(graphene_point3d_t*)aligned_alloc(sizeof(graphene_point3d_t), sizeof(graphene_point3d_t)*8);
    cube_t=(graphene_point3d_t*)aligned_alloc(sizeof(graphene_point3d_t), sizeof(graphene_point3d_t)*8);
    lines=(graphene_point3d_t*)aligned_alloc(sizeof(graphene_point3d_t), sizeof(graphene_point3d_t)*18);
    lines_t=(graphene_point3d_t*)aligned_alloc(sizeof(graphene_point3d_t), sizeof(graphene_point3d_t)*18);

    graphene_point3d_t *p1=cube;
    p1->x=20.0f;p1->y=20.0f;p1->z=20.0f;
    p1++;
    p1->x=20.0f;p1->y=20.0f;p1->z=-20.0f;
    p1++;
    p1->x=20.0f;p1->y=-20.0f;p1->z=-20.0f;
    p1++;
    p1->x=20.0f;p1->y=-20.0f;p1->z=20.0f;
    p1++;
    p1->x=-20.0f;p1->y=20.0f;p1->z=20.0f;
    p1++;
    p1->x=-20.0f;p1->y=20.0f;p1->z=-20.0f;
    p1++;
    p1->x=-20.0f;p1->y=-20.0f;p1->z=-20.0f;
    p1++;
    p1->x=-20.0f;p1->y=-20.0f;p1->z=20.0f;

    p1=lines;
    p1->x=-20.0f;p1->y=-20.0f;p1->z=16.0f;
    p1++;
    p1->x=-20.0f;p1->y=-20.0f;p1->z=12.0f;
    p1++;
    p1->x=-20.0f;p1->y=-20.0f;p1->z=8.0f;
    p1++;
    p1->x=-20.0f;p1->y=-20.0f;p1->z=4.0f;
    p1++;
    p1->x=-20.0f;p1->y=-20.0f;p1->z=0.0f;
    p1++;
    p1->x=-20.0f;p1->y=-20.0f;p1->z=-4.0f;
    p1++;
    p1->x=-20.0f;p1->y=-20.0f;p1->z=-8.0f;
    p1++;
    p1->x=-20.0f;p1->y=-20.0f;p1->z=-12.0f;
    p1++;
    p1->x=-20.0f;p1->y=-20.0f;p1->z=-16.0f;
    p1++;

    p1->x=20.0f;p1->y=-20.0f;p1->z=16.0f;
    p1++;
    p1->x=20.0f;p1->y=-20.0f;p1->z=12.0f;
    p1++;
    p1->x=20.0f;p1->y=-20.0f;p1->z=8.0f;
    p1++;
    p1->x=20.0f;p1->y=-20.0f;p1->z=4.0f;
    p1++;
    p1->x=20.0f;p1->y=-20.0f;p1->z=0.0f;
    p1++;
    p1->x=20.0f;p1->y=-20.0f;p1->z=-4.0f;
    p1++;
    p1->x=20.0f;p1->y=-20.0f;p1->z=-8.0f;
    p1++;
    p1->x=20.0f;p1->y=-20.0f;p1->z=-12.0f;
    p1++;
    p1->x=20.0f;p1->y=-20.0f;p1->z=-16.0f;
  
    v1.x=0.0f;
    v1.y=0.0f;
    v1.z=-20.0f;
  }
static gboolean draw_main_window(GtkWidget *window, cairo_t *cr, gpointer data)
  {
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.3);
    cairo_paint(cr);

    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
    gint width=gtk_paned_get_position(GTK_PANED(data));
    gint height=gtk_widget_get_allocated_height(window);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_rectangle(cr, width, 0.0, 10, height);
    cairo_fill(cr);

    return FALSE;
  }
static void show_frame_rate(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_function_time(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) function_time=TRUE;
    else function_time=FALSE;
  }
static void rotate_drawing(GtkComboBox *combo, gpointer data)
  {   
    combo_pitch=0.0;
    combo_roll=0.0;
    combo_yaw=0.0;
    combo_perspective=0.0;
 
    if(gtk_combo_box_get_active(combo)==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(data), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(data));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(data), (GtkTickCallback)animate_drawing, NULL, NULL);
          }
      }
    
  }
static void set_perspective_id(GtkComboBox *combo, gpointer data)
  {  
    perspective_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_stress_id(GtkComboBox *combo, gpointer data)
  {  
    stress_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_pitch=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_roll=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_yaw=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>100.0) combo_perspective=100.0;
    else if(value<0.0) combo_perspective=0.0;
    else combo_perspective=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data)
  {
    static gboolean up=TRUE;

    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    if(combo_pitch>360.0) combo_pitch=0.0;
    else combo_pitch+=0.5;
    if(combo_roll>360.0) combo_roll=0.0;
    else combo_roll+=0.5;
    if(combo_yaw>360.0) combo_yaw=0.0;
    else combo_yaw+=0.5;

    if(combo_perspective>100.0) up=FALSE;
    else if(combo_perspective<0.0) up=TRUE; 
    if(up) combo_perspective+=0.5;
    else combo_perspective-=0.5;

    gtk_widget_queue_draw(GTK_WIDGET(drawing));
    return G_SOURCE_CONTINUE;
  }
//The top drawing function.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;

    //Paint background transparent.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.0);
    cairo_paint(cr);

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 3.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Draw in the center
    cairo_translate(cr, width/2.0, height/2.0);
    //Sometimes there are small extra line fragments if CAIRO_LINE_JOIN_MITER is used.
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND); 
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    gdouble pitch=combo_pitch*G_PI/180.0;
    gdouble roll=combo_roll*G_PI/180.0;
    gdouble yaw=combo_yaw*G_PI/180.0;     
 
    //Time the transforms.
    GTimer *timer1=g_timer_new();
    //Reset to initial values.
    reset_draw_cube();
    //Get a rotation matrix.
    graphene_quaternion_t q;
    graphene_matrix_t m;
    graphene_quaternion_init_from_radians (&q, (float)pitch, (float)roll, (float)yaw);
    graphene_quaternion_to_matrix(&q, &m);                                              
    //Remember matrix multiplication isn't commutative or the order of matrix multiplication matters.   
    if(stress_id==1)
      {
        //A simple stress matrix.
        const float sm[16]={2.0, 0.0, 0.0, 0.0,
                            0.0, 1.0, 0.0, 0.0,
                            0.0, 0.0, 1.0, 0.0,
                            0.0, 0.0, 0.0, 1.0};
        change_stress(sm);
      }
    if(stress_id==2)
      {
        const float sm[16]={1.0, 0.0, 0.0, 0.0,
                            0.0, 2.0, 0.0, 0.0,
                            0.0, 0.0, 1.0, 0.0,
                            0.0, 0.0, 0.0, 1.0};
        change_stress(sm);
      }

    if(perspective_id==0) change_perspective1();
    if(perspective_id==1) change_perspective2();
    rotate_cube(&m);
    if(function_time) g_print("Transform time %f\n", g_timer_elapsed(timer1, NULL));

    //Time the drawings.
    g_timer_start(timer1);
    if(perspective_id==0) draw_cube(cr, 4.0);
    else draw_cube(cr, 3.0);
    if(function_time) g_print("     Draw time %f\n", g_timer_elapsed(timer1, NULL));    
    g_timer_destroy(timer1);
     
    return FALSE;
  }
static void reset_draw_cube()
  {
    //Start with a new copy of the original cube, lines and reference vector.
    gint i=0;
    graphene_point3d_t *p1=cube;
    graphene_point3d_t *p2=cube_t;

    for(i=0;i<8;i++)
      {        
        *p2=*p1;
        p1++;p2++;
      }

    p1=lines;
    p2=lines_t;
    for(i=0;i<18;i++)
      {        
        *p2=*p1;
        p1++;p2++;
      }

    v1_t=v1;
  }
static void change_stress(const gfloat sm[16])
  {
    gint i=0;
    graphene_matrix_t m;
    graphene_matrix_init_from_float(&m, sm);     
    graphene_point3d_t *p1=cube_t;

    for(i=0;i<8;i++)
      {
        graphene_matrix_transform_point3d(&m, p1, p1);
        p1++;
      }
    
    p1=lines_t;
    for(i=0;i<18;i++)
      {   
        graphene_matrix_transform_point3d(&m, p1, p1);
        p1++;
      }
  }
static void change_perspective1()
  {
    gint i=0;
    graphene_point3d_t *p1=cube;
    graphene_point3d_t *p2=cube_t;
    gfloat perspective1=(gfloat)(combo_perspective/2000.0f);
    gfloat perspective2=0.0f;

    //Center the perspective object.
    perspective2=(1.0f+(perspective1*v1.z)); 
    gfloat translate_z=v1_t.z-v1_t.z*perspective2;

    for(i=0;i<8;i++)
      {   
        perspective2=(1.0f+(perspective1*(p1->z))); 
        graphene_point3d_scale(p2, perspective2, p2);
        p2->z=(p2->z)+translate_z;
        p1++;p2++;
      }
    
    p1=lines;
    p2=lines_t;
    for(i=0;i<18;i++)
      {   
        perspective2=(1.0f+(perspective1*(p1->z)));
        graphene_point3d_scale(p2, perspective2, p2); 
        p2->z=(p2->z)+translate_z;
        p1++;p2++;
      }

    perspective2=(1.0f+(perspective1*v1_t.z));
    graphene_point3d_scale(&v1_t, perspective2, &v1_t);
    v1_t.z=v1_t.z+translate_z;
  }
static void change_perspective2()
  {
    gint i=0;
    graphene_point3d_t *p1=cube;
    graphene_point3d_t *p2=cube_t;
    gfloat perspective1=(gfloat)(combo_perspective/2000.0f);
    gfloat perspective2=0.0f;

    //Center the perspective object.
    perspective2=(1.0f+(perspective1*v1.z)*3.0f); 
    gfloat translate_z=v1_t.z-v1_t.z*perspective2;

    for(i=0;i<8;i++)
      {   
        perspective2=(1.0+(perspective1*(p1->z)*3.0f));
        graphene_point3d_scale(p2, perspective2, p2);  
        p2->z=(p2->z)+translate_z;
        p1++;p2++;
      }
    
    p1=lines;
    p2=lines_t;
    for(i=0;i<18;i++)
      {   
        perspective2=(1.0+(perspective1*(p1->z)*3.0f));
        graphene_point3d_scale(p2, perspective2, p2);  
        p2->z=(p2->z)+translate_z;
        p1++;p2++;
      }

    perspective2=(1.0f+(perspective1*v1_t.z));
    graphene_point3d_scale(&v1_t, perspective2, &v1_t); 
    v1_t.z=v1_t.z+translate_z;
  }
static void rotate_cube(graphene_matrix_t *m)
  {
    gint i=0;   
    graphene_point3d_t *p1=cube_t;

    for(i=0;i<8;i++)
      {
        graphene_matrix_transform_point3d(m, p1, p1);
        p1++;
      }
    
    p1=lines_t;
    for(i=0;i<18;i++)
      {   
        graphene_matrix_transform_point3d(m, p1, p1);
        p1++;
      }

    graphene_matrix_transform_point3d(m, &v1_t, &v1_t);
  }
static void draw_cube(cairo_t *cr, gdouble scale)
  {
    gint i=0;
    gint j=0;
    gdouble slope_x1=0.0;
    gdouble slope_y1=0.0;
    gdouble slope_x2=0.0;
    gdouble slope_y2=0.0;
    gdouble x1=0.0;
    gdouble y1=0.0;
    gdouble x2=0.0;
    gdouble y2=0.0;
    //An array to sort quads.
    struct quad *quads_start=g_malloc(6*sizeof(struct quad));
    struct quad *quads=quads_start;
    
    (*quads).id=0;
    (*quads).z=((*(cube_t)).z)+((*(cube_t+1)).z)+((*(cube_t+2)).z)+((*(cube_t+3)).z);
    quads++;
    (*quads).id=1;
    (*quads).z=((*(cube_t+4)).z)+((*(cube_t+5)).z)+((*(cube_t+6)).z)+((*(cube_t+7)).z);
    quads++;
    (*quads).id=2;
    (*quads).z=((*(cube_t)).z)+((*(cube_t+1)).z)+((*(cube_t+5)).z)+((*(cube_t+4)).z);
    quads++;
    (*quads).id=3;
    (*quads).z=((*(cube_t+1)).z)+((*(cube_t+2)).z)+((*(cube_t+6)).z)+((*(cube_t+5)).z);
    quads++;
    (*quads).id=4;
    (*quads).z=((*(cube_t+2)).z)+((*(cube_t+3)).z)+((*(cube_t+7)).z)+((*(cube_t+6)).z);
    quads++;
    (*quads).id=5;
    (*quads).z=((*(cube_t+3)).z)+((*(cube_t)).z)+((*(cube_t+4)).z)+((*(cube_t+7)).z);
    quads++;

    //Sort array based on z values.
    qsort(quads_start, 6, sizeof(struct quad), compare_quads);

    //Just draw the top three cube planes.
    quads=quads_start;
    for(i=0;i<6;i++)
      {
        if((*quads).id==0)
          {
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.5);
            cairo_move_to(cr, ((*(cube_t)).x)*scale, ((*(cube_t)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+1)).x)*scale, ((*(cube_t+1)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+2)).x)*scale, ((*(cube_t+2)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+3)).x)*scale, ((*(cube_t+3)).y)*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else if((*quads).id==1)
          {
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.5);
            cairo_move_to(cr, ((*(cube_t+4)).x)*scale, ((*(cube_t+4)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+5)).x)*scale, ((*(cube_t+5)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+6)).x)*scale, ((*(cube_t+6)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+7)).x)*scale, ((*(cube_t+7)).y)*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else if((*quads).id==2)
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, ((*(cube_t)).x)*scale, ((*(cube_t)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+1)).x)*scale, ((*(cube_t+1)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+5)).x)*scale, ((*(cube_t+5)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+4)).x)*scale, ((*(cube_t+4)).y)*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);

            //Draw perspective lines.
            cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
            slope_x1=(((*(cube_t+1)).x)-((*(cube_t)).x))/10.0; 
            slope_y1=(((*(cube_t+1)).y)-((*(cube_t)).y))/10.0;
            slope_x2=(((*(cube_t+5)).x)-((*(cube_t+4)).x))/10.0; 
            slope_y2=(((*(cube_t+5)).y)-((*(cube_t+4)).y))/10.0;             
            for(j=1;j<10;j++)
              {
                x1=slope_x1*(gdouble)(j);
                y1=slope_y1*(gdouble)(j);
                x2=slope_x2*(gdouble)(j);
                y2=slope_y2*(gdouble)(j);
                cairo_move_to(cr, ((*(cube_t)).x+x1)*scale, ((*(cube_t)).y+y1)*scale);
                cairo_line_to(cr, ((*(cube_t+4)).x+x2)*scale, ((*(cube_t+4)).y+y2)*scale);
                cairo_stroke(cr);
              }

          }
        else if((*quads).id==3)
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, ((*(cube_t+1)).x)*scale, ((*(cube_t+1)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+2)).x)*scale, ((*(cube_t+2)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+6)).x)*scale, ((*(cube_t+6)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+5)).x)*scale, ((*(cube_t+5)).y)*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else if((*quads).id==4)
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, ((*(cube_t+2)).x)*scale, ((*(cube_t+2)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+3)).x)*scale, ((*(cube_t+3)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+7)).x)*scale, ((*(cube_t+7)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+6)).x)*scale, ((*(cube_t+6)).y)*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
            //Draw a few more lines for perspective.
            cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
            for(j=0;j<9;j++)
              {
                cairo_move_to(cr, ((*(lines_t+j)).x)*scale, ((*(lines_t+j)).y)*scale);
                cairo_line_to(cr, ((*(lines_t+j+9)).x)*scale, ((*(lines_t+j+9)).y)*scale);
                cairo_stroke(cr);
              }

            cairo_move_to(cr, ((*(lines_t)).x)*scale, ((*(lines_t)).y)*scale);
            for(j=1;j<9;j++)
              {
                cairo_line_to(cr, ((*(lines_t+j)).x)*scale, ((*(lines_t+j)).y)*scale);
              }
            cairo_stroke(cr);

            cairo_move_to(cr, ((*(lines_t+9)).x)*scale, ((*(lines_t+9)).y)*scale);
            for(j=1;j<9;j++)
              {
                cairo_line_to(cr, ((*(lines_t+j+9)).x)*scale, ((*(lines_t+j+9)).y)*scale);
              }
            cairo_stroke(cr);
          }
        else
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 0.5);
            cairo_move_to(cr, ((*(cube_t+3)).x)*scale, ((*(cube_t+3)).y)*scale);
            cairo_line_to(cr, ((*(cube_t)).x)*scale, ((*(cube_t)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+4)).x)*scale, ((*(cube_t+4)).y)*scale);
            cairo_line_to(cr, ((*(cube_t+7)).x)*scale, ((*(cube_t+7)).y)*scale);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        quads++;
        }

    g_free(quads_start);  
  }
int compare_quads(const void *a, const void *b)
  {
    return(  (((struct quad*)a)->z) - (((struct quad*)b)->z) );
  }
