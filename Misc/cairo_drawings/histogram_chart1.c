
/*   
    Start testing some ideas for a 3d histogram.

    gcc -Wall histogram_chart1.c -o histogram_chart1 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<string.h>
#include<math.h>
#include"cairo-svg.h"
#include"cairo-pdf.h"

struct point3d{
  gdouble x;
  gdouble y;
  gdouble z;
};

struct color{
  gdouble r;
  gdouble g;
  gdouble b;
  gdouble a;
};

struct rectangle3d{
  struct point3d b1;
  struct point3d b2;
  struct point3d b3;
  struct point3d b4;
  struct color cl;
};

struct cube3d{
  struct point3d c1;
  struct point3d c2;
  struct point3d c3;
  struct point3d c4;
  struct point3d c5;
  struct point3d c6;
  struct point3d c7;
  struct point3d c8;
  struct color cl;
};

struct histogram_chart{ 
   guint rows;
   guint columns; 
   gdouble outside_square;
   GArray *base;
   GArray *base_t;
   GArray *cubes;
   GArray *cubes_t;
   //Reference vectors for each axis.
   gdouble top[3];
   gdouble top_t[3];
   gdouble rx[3];
   gdouble rx_t[3];
   gdouble ry[3];
   gdouble ry_t[3];
};

//Position the histogram cube.
enum{ 
  CUBE_UPPER_LEFT,
  CUBE_CENTER
};

static gint drawing_id=0;
static gdouble pitch=0.0;
static gdouble roll=0.0;
static gdouble yaw=0.0;
static gdouble scale=1.0;
static guint tick_id=0;
static gboolean frame_rate=FALSE;
static gboolean time_it=FALSE;

//Histogram initialization and free.
static struct histogram_chart* histogram_chart_new(guint rows, guint columns, gdouble outside_square, gdouble inside_square, guint inside_square_position);
static void initialize_base(struct histogram_chart *hc, guint rows, guint columns, gdouble outside_square);
static void initialize_cubes(struct histogram_chart *hc, guint rows, guint columns, gdouble inside_square, gdouble gap, guint inside_square_position);
static void histogram_chart_set_bar_color(struct histogram_chart *hc, guint index, gdouble rgba[4]);
static void histogram_chart_set_bar_height(struct histogram_chart *hc, guint index, gdouble height);
static double histogram_chart_get_bar_height(struct histogram_chart *hc, guint index);
static void histogram_chart_set_base_color(struct histogram_chart *hc, guint index, gdouble rgba[4]);
static void histogram_chart_free(struct histogram_chart *hc);
//Set some variables from the UI.
static void set_animate_drawing(GtkComboBox *combo, gpointer *data);
static void set_drawing_id(GtkComboBox *combo, gpointer da);
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void show_frame_rate(GtkToggleButton *check_frame, gpointer data);
static void show_drawing_time(GtkToggleButton *check_time, gpointer data);
static gboolean animate_drawing(GtkWidget *da, GdkFrameClock *frame_clock, struct histogram_chart *hc);
//Drawing functions.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, struct histogram_chart *hc);
static void histogram_chart_draw(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[9]);
static void histogram_chart_draw_base(struct histogram_chart *hc, cairo_t *cr);
static void histogram_chart_draw_cubes(struct histogram_chart *hc, cairo_t *cr);
static void histogram_chart_draw_numbers_left(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[9]);
static void histogram_chart_draw_numbers_right(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[9]);
static void histogram_chart_draw_numbers_z(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[9]);
static void histogram_chart_draw_back(struct histogram_chart *hc, cairo_t *cr);
static void histogram_chart_draw_side(struct histogram_chart *hc, cairo_t *cr);
static void histogram_chart_reset_base();
static void histogram_chart_reset_cubes();
//3d transform functions.
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9]);
static void histogram_chart_rotate(struct histogram_chart *hc, gdouble qrs[9]);
int compare_cubes(const void *a, const void *b);
static void matrix_multiply(const gdouble in1[9], const gdouble in2[9], gdouble out[9]);
//Test output.
static void draw_svg(GtkWidget *widget, gpointer *data);
static void draw_pdf(GtkWidget *widget, gpointer *data);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Histogram Chart3d");
    gtk_window_set_default_size(GTK_WINDOW(window), 725, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    /*
      Initialize a histogram chart. The outside_square is the base square size and the inside_square
      is the bar square size. The bar square is smaller than or equal to the base square.
    */
    guint rows=9;
    guint columns=11;
    gdouble outside_square=40.0;
    gdouble inside_square=20.0;
    struct histogram_chart *hc=histogram_chart_new(rows, columns, outside_square, inside_square, CUBE_CENTER);

    //Set histogram bar colors.
    gint i=0;
    gint total=rows*columns;
    gdouble inv=1.0/(gdouble)total;
    gdouble rgba[4]={1.0, 0.0, 1.0, 1.0};
    for(i=0;i<total;i++)
      {
        histogram_chart_set_bar_color(hc, i, rgba);
        rgba[1]+=inv;
        rgba[2]-=inv;
      }

    //Set some random histogram bar heights.
    gdouble rand=0.0;
    for(i=0;i<total;i++)
      {
        rand=5.0*inside_square*g_random_double();
        histogram_chart_set_bar_height(hc, i, rand);
      }

    //Set histogram base square colors.
    rgba[0]=0.0;rgba[3]=1.0;
    for(i=0;i<total;i++)
      {
        if(i%2==0)
          {
            rgba[1]=0.0;rgba[2]=1.0;
          }
        else
          {
            rgba[1]=1.0;rgba[2]=1.0;
          }
        histogram_chart_set_base_color(hc, i, rgba);
      }

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);
    g_signal_connect(da, "draw", G_CALLBACK(draw_main), hc); 

    GtkWidget *combo_animate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_animate, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_animate), 0, "1", "Draw Shape");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_animate), 1, "2", "Animate Shape");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_animate), 0);
    gpointer data[]={da, hc};
    g_signal_connect(combo_animate, "changed", G_CALLBACK(set_animate_drawing), data);

    GtkWidget *combo_drawing=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_drawing, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "1", "Histogram Chart3d");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
    g_signal_connect(combo_drawing, "changed", G_CALLBACK(set_drawing_id), da);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);

    GtkWidget *scale_label=gtk_label_new("Scale");
    gtk_widget_set_hexpand(scale_label, TRUE);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider, TRUE);
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_pitch), da);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider, TRUE);
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_roll), da);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider, TRUE);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_yaw), da);  

    GtkWidget *scale_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 1.0, 0.01);
    gtk_widget_set_vexpand(scale_slider, TRUE);
    gtk_range_set_value(GTK_RANGE(scale_slider), 1.0);
    g_signal_connect(scale_slider, "change-value", G_CALLBACK(set_scale), da);

    GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
    gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
    g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), da);

    GtkWidget *check_time=gtk_check_button_new_with_label("Time Drawing");
    gtk_widget_set_halign(check_time, GTK_ALIGN_CENTER);
    g_signal_connect(check_time, "toggled", G_CALLBACK(show_drawing_time), da);

    GtkWidget *menu1=gtk_menu_new();
    GtkWidget *menu1item1=gtk_menu_item_new_with_label("histo1.svg");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item1);
    GtkWidget *menu1item2=gtk_menu_item_new_with_label("histo1.pdf");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item2);
    GtkWidget *title1=gtk_menu_item_new_with_label("Test Output");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(title1), menu1);

    GtkWidget *menu_bar=gtk_menu_bar_new();
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), title1);

    g_signal_connect(menu1item1, "activate", G_CALLBACK(draw_svg), data);
    g_signal_connect(menu1item2, "activate", G_CALLBACK(draw_pdf), data);
   
    GtkWidget *grid=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid), 10);
    gtk_grid_attach(GTK_GRID(grid), combo_animate, 0, 0, 4, 1);
    gtk_grid_attach(GTK_GRID(grid), combo_drawing, 0, 1, 4, 1);
    gtk_grid_attach(GTK_GRID(grid), pitch_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), roll_label, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), yaw_label, 2, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), scale_label, 3, 2, 1, 1);     
    gtk_grid_attach(GTK_GRID(grid), pitch_slider, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), roll_slider, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), yaw_slider, 2, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), scale_slider, 3, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), check_frame, 0, 4, 4, 1);
    gtk_grid_attach(GTK_GRID(grid), check_time, 0, 5, 4, 1);
    gtk_grid_attach(GTK_GRID(grid), menu_bar, 0, 6, 4, 1);

    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), grid, FALSE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 200);
   
    gtk_container_add(GTK_CONTAINER(window), paned1);

    gtk_widget_show_all(window);

    gtk_main();

    //Clean up.
    histogram_chart_free(hc);

    return 0;  
  }
static struct histogram_chart* histogram_chart_new(guint rows, guint columns, gdouble outside_square, gdouble inside_square, guint inside_square_position)
  {
    gdouble gap=outside_square-inside_square;

     if(gap<0.0)
      {
        gap=0.0;
        g_warning("The outside_square can't be greater than the inside_square.\n");
      }

    struct histogram_chart *hc=g_new(struct histogram_chart, 1);

    hc->rows=rows;
    hc->columns=columns;
    hc->outside_square=outside_square;
    initialize_base(hc, rows, columns, outside_square);
    initialize_cubes(hc, rows, columns, inside_square, gap, inside_square_position);
    hc->top[0]=0.0;
    hc->top[1]=0.0;
    hc->top[2]=1.0;
    hc->top_t[0]=0.0;
    hc->top_t[1]=0.0;
    hc->top_t[2]=1.0;
    hc->rx[0]=1.0;
    hc->rx[1]=0.0;
    hc->rx[2]=0.0;
    hc->rx_t[0]=1.0;
    hc->rx_t[1]=0.0;
    hc->rx_t[2]=0.0;
    hc->ry[0]=0.0;
    hc->ry[1]=1.0;
    hc->ry[2]=0.0;
    hc->ry_t[0]=0.0;
    hc->ry_t[1]=1.0;
    hc->ry_t[2]=0.0;

    return hc;
  }
static void initialize_base(struct histogram_chart *hc, guint rows, guint columns, gdouble outside_square)
  {
    gint i=0;
    gint j=0;
    gint total=rows*columns;
    //Center points.
    gdouble move_x=-(columns*outside_square)/2.0;
    gdouble move_y=-(rows*outside_square)/2.0;

    hc->base=g_array_sized_new(FALSE, TRUE, sizeof(struct rectangle3d), total);
    g_array_set_size(hc->base, total);
    hc->base_t=g_array_sized_new(FALSE, TRUE, sizeof(struct rectangle3d), total);
    g_array_set_size(hc->base_t, total);

    struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, 0);

    for(i=0;i<rows;i++)
      {
        for(j=0;j<columns;j++)
          {
            (p1->b1).x=move_x;
            (p1->b1).y=move_y;
            (p1->b1).z=0.0;
            (p1->b2).x=move_x+outside_square;
            (p1->b2).y=move_y;
            (p1->b2).z=0.0;
            (p1->b3).x=move_x+outside_square;
            (p1->b3).y=move_y+outside_square;
            (p1->b3).z=0.0;
            (p1->b4).x=move_x;
            (p1->b4).y=move_y+outside_square;
            (p1->b4).z=0.0;
            //Initialize color to green.
            (p1->cl).r=0.0;(p1->cl).g=0.0;(p1->cl).b=1.0;(p1->cl).a=1.0;
            move_x+=outside_square;
            p1++;
          }
        move_x=-(columns*outside_square)/2.0;
        move_y+=outside_square;
      }

  }
static void initialize_cubes(struct histogram_chart *hc, guint rows, guint columns, gdouble inside_square, gdouble gap, guint inside_square_position)
  {
    gint i=0;
    gint j=0;
    gint total=rows*columns;
    gdouble position=0.0;
    if(inside_square_position==CUBE_CENTER) position=gap/2.0;
    //Center points.
    gdouble move_x=-(columns*(inside_square+gap))/2.0+position;
    gdouble move_y=-(rows*(inside_square+gap))/2.0+position;

    hc->cubes=g_array_sized_new(FALSE, TRUE, sizeof(struct cube3d), total);
    g_array_set_size(hc->cubes, total);
    hc->cubes_t=g_array_sized_new(FALSE, TRUE, sizeof(struct cube3d), total);
    g_array_set_size(hc->cubes_t, total);

    struct cube3d *p1=&g_array_index(hc->cubes, struct cube3d, 0);

    for(i=0;i<rows;i++)
      {
        for(j=0;j<columns;j++)
          {
            (p1->c1).x=move_x;
            (p1->c1).y=move_y;
            (p1->c1).z=0.0;
            (p1->c2).x=move_x+inside_square;
            (p1->c2).y=move_y;
            (p1->c2).z=0.0;
            (p1->c3).x=move_x+inside_square;
            (p1->c3).y=move_y+inside_square;
            (p1->c3).z=0.0;
            (p1->c4).x=move_x;
            (p1->c4).y=move_y+inside_square;
            (p1->c4).z=0.0;
            (p1->c5).x=move_x;
            (p1->c5).y=move_y;
            (p1->c5).z=0.0;
            (p1->c6).x=move_x+inside_square;
            (p1->c6).y=move_y;
            (p1->c6).z=0.0;
            (p1->c7).x=move_x+inside_square;
            (p1->c7).y=move_y+inside_square;
            (p1->c7).z=0.0;
            (p1->c8).x=move_x;
            (p1->c8).y=move_y+inside_square;
            (p1->c8).z=0.0;
            //Initialize color to green.
            (p1->cl).r=0.0;(p1->cl).g=1.0;(p1->cl).b=0.0;(p1->cl).a=1.0;
            move_x+=inside_square+gap;
            p1++;
          }
        move_x=-(columns*(inside_square+gap))/2.0+position;
        move_y+=inside_square+gap;
      }

  }
static void histogram_chart_set_bar_color(struct histogram_chart *hc, guint index, gdouble rgba[4])
  {
    if(index>(hc->cubes->len)-1)
      {
        g_warning("The cube index %i isn't valid. No color was set.\n", index);
      }
    else
      {
        struct cube3d *p1=&g_array_index(hc->cubes, struct cube3d, index);
        (p1->cl).r=rgba[0];(p1->cl).g=rgba[1];(p1->cl).b=rgba[2];(p1->cl).a=rgba[3];
      }
  }
static void histogram_chart_set_bar_height(struct histogram_chart *hc, guint index, gdouble height)
  {
    if(index>(hc->cubes->len)-1)
      {
        g_warning("The cube index %i isn't valid. No height was set.\n", index);
      }
    else
      {
        struct cube3d *p1=&g_array_index(hc->cubes, struct cube3d, index);
        (p1->c5).z=height;
        (p1->c6).z=height;
        (p1->c7).z=height;
        (p1->c8).z=height;
      }
  }
static double histogram_chart_get_bar_height(struct histogram_chart *hc, guint index)
  {
    gdouble ret=0.0;

    if(index>(hc->cubes->len)-1)
      {
        g_warning("The cube index %i isn't valid. The returned height is 0.\n", index);
      }
    else
      {
        struct cube3d *p1=&g_array_index(hc->cubes, struct cube3d, index);
        ret=(p1->c5).z;
      }

    return ret;
  }
static void histogram_chart_set_base_color(struct histogram_chart *hc, guint index, gdouble rgba[4])
  {
    if(index>(hc->base->len)-1)
      {
        g_warning("The base index %i isn't valid. No color was set.\n", index);
      }
    else
      {
        struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, index);
        (p1->cl).r=rgba[0];(p1->cl).g=rgba[1];(p1->cl).b=rgba[2];(p1->cl).a=rgba[3];
      }
  }
static void histogram_chart_free(struct histogram_chart *hc)
  {
    if((hc->base)!=NULL) g_array_free((hc->base), TRUE);
    if((hc->base_t)!=NULL) g_array_free((hc->base_t), TRUE);
    if((hc->cubes)!=NULL) g_array_free((hc->cubes), TRUE);
    if((hc->cubes_t)!=NULL) g_array_free((hc->cubes_t), TRUE);
    g_free(hc);
    hc=NULL;
  }
static void set_animate_drawing(GtkComboBox *combo, gpointer *data)
  {   
    pitch=0.0;
    roll=0.0;
    yaw=0.0;
 
    if(gtk_combo_box_get_active(combo)==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(data[0]), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(data[0]));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(data[0]), (GtkTickCallback)animate_drawing, (struct histogram_chart*)data[1], NULL);
          }
      }
    
  }
static void set_drawing_id(GtkComboBox *combo, gpointer da)
  {  
    drawing_id=gtk_combo_box_get_active(combo);

    gtk_widget_queue_draw(GTK_WIDGET(da));
  }
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    pitch=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    roll=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    yaw=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>1.0) scale=1.0;
    else if(value<0.0) scale=0.0;
    else scale=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void show_frame_rate(GtkToggleButton *check_frame, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_frame)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_drawing_time(GtkToggleButton *check_time, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_time)) time_it=TRUE;
    else time_it=FALSE;
  }
static gboolean animate_drawing(GtkWidget *da, GdkFrameClock *frame_clock, struct histogram_chart *hc)
  {
    gint i=0;
    gdouble z=0.0;

    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    if(pitch>360.0) pitch=0.0;
    else pitch+=0.5;
    if(roll>360.0) roll=0.0;
    else roll+=0.5;
    if(yaw>360.0) yaw=0.0;
    else yaw+=0.5;

    //Change bar heights.
    for(i=0;i<(hc->cubes->len);i++)
      {
        z=histogram_chart_get_bar_height(hc, i);
        if(z>200.0) histogram_chart_set_bar_height(hc, i, 0.0);
        else histogram_chart_set_bar_height(hc, i, z+2.0);
      }
    
    gtk_widget_queue_draw(GTK_WIDGET(da));
    return G_SOURCE_CONTINUE;
  }
static gboolean draw_main(GtkWidget *da, cairo_t *cr, struct histogram_chart *hc)
  {  
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;

    //Background.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr); 

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 2.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);
  
    GTimer *timer=NULL; 
    if(time_it==TRUE) timer=g_timer_new();
 
    cairo_translate(cr, width/2.0, height/2.0);
    cairo_scale(cr, scale+0.01, scale+0.01);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_BUTT);
    cairo_set_line_width(cr, 1.0);

    //Rotation.
    gdouble qrs[9];
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    quaternion_rotation(yaw1, roll1, pitch1, qrs);
   
    //Draw the chart.
    histogram_chart_draw(hc, cr, qrs);
     
    if(time_it==TRUE) 
     {
       g_print("Draw Time %f\n", g_timer_elapsed(timer, NULL));
       g_timer_destroy(timer);
     }

    return FALSE;
  }
static void histogram_chart_draw(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[9])
  {
    histogram_chart_reset_base(hc);
    histogram_chart_reset_cubes(hc);
    histogram_chart_rotate(hc, qrs);

    if(hc->top_t[2]>0.0)
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_numbers_z(hc, cr, qrs);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_numbers_left(hc, cr, qrs);
        histogram_chart_draw_numbers_right(hc, cr, qrs);
        histogram_chart_draw_base(hc, cr);    
        g_array_sort(hc->cubes_t, compare_cubes);
        histogram_chart_draw_cubes(hc, cr);
        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_numbers_z(hc, cr, qrs);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
      }
    else
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_numbers_z(hc, cr, qrs);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr); 
        g_array_sort(hc->cubes_t, compare_cubes);
        histogram_chart_draw_cubes(hc, cr);
        histogram_chart_draw_back(hc, cr);
        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_numbers_z(hc, cr, qrs);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_base(hc, cr);        
        histogram_chart_draw_numbers_left(hc, cr, qrs);
        histogram_chart_draw_numbers_right(hc, cr, qrs);  
      }
  }
static void histogram_chart_draw_base(struct histogram_chart *hc, cairo_t *cr)
  {
    gint i=0;
    struct rectangle3d *p1=&g_array_index(hc->base_t, struct rectangle3d, 0);

    for(i=0;i<(hc->base_t->len);i++)
      {
        cairo_set_source_rgba(cr, (p1->cl).r, (p1->cl).g, (p1->cl).b, (p1->cl).a);
        cairo_move_to(cr, (p1->b1).x, (p1->b1).y);
        cairo_line_to(cr, (p1->b2).x, (p1->b2).y);
        cairo_line_to(cr, (p1->b3).x, (p1->b3).y);
        cairo_line_to(cr, (p1->b4).x, (p1->b4).y);
        cairo_close_path(cr);
        cairo_fill(cr);
        p1++;
      }
  }
static void histogram_chart_draw_cubes(struct histogram_chart *hc, cairo_t *cr)
  {
    gint i=0;
    struct cube3d *p1=&g_array_index(hc->cubes_t, struct cube3d, 0);

    for(i=0;i<(hc->cubes_t->len);i++)
      {
        cairo_set_source_rgba(cr, (p1->cl).r, (p1->cl).g, (p1->cl).b, (p1->cl).a);
        //Bottom of cube.
        cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
        cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
        cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
        cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        //Sides of cube
        cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
        cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
        cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
        cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        cairo_move_to(cr, (p1->c2).x, (p1->c2).y);
        cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
        cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
        cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        cairo_move_to(cr, (p1->c3).x, (p1->c3).y);
        cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
        cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
        cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        cairo_move_to(cr, (p1->c4).x, (p1->c4).y);
        cairo_line_to(cr, (p1->c1).x, (p1->c1).y);
        cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
        cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        //Top of cube
        cairo_move_to(cr, (p1->c5).x, (p1->c5).y);
        cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
        cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
        cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
        cairo_close_path(cr);
        if(hc->top_t[2]>0.0)
          {
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
          }
        else
          {
            cairo_fill(cr);
          }
        p1++;
      }
  }
static void histogram_chart_draw_numbers_left(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[9])
  {
    gint i=0;
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    gdouble font_size=(hc->outside_square)/2.0;

    struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, 0);

    cairo_save(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_matrix_init(&matrix1, qrs[0], qrs[3], qrs[1], qrs[4], 0.0, 0.0);
    cairo_transform(cr, &matrix1); 
    cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, font_size);
    for(i=0;i<(hc->columns);i++)
      {
        gchar *string=g_strdup_printf("%i", i);
        /*
          Try to align numbers with base grid. Probably not the best way to do this. Just get
          extents once for one or two numbers so that they line up better.
        */
        if(i==0) cairo_text_extents(cr, string, &extents);
        if(i==10) cairo_text_extents(cr, string, &extents);
        if(i<10) cairo_move_to(cr, (p1->b1).x+1.3*extents.width, (p1->b1).y-extents.height/2.0);
        else cairo_move_to(cr, (p1->b1).x+extents.width/3.0, (p1->b1).y-extents.height/2.0);
        cairo_show_text(cr, string);
        g_free(string);
        p1++;
      }

    p1=&g_array_index(hc->base, struct rectangle3d, 0);
    for(i=0;i<(hc->rows);i++)
      {
        gchar *string=g_strdup_printf("%i", i);
        if(i==0) cairo_text_extents(cr, string, &extents);
        if(i==10) cairo_text_extents(cr, string, &extents);
        if(i<10)cairo_move_to(cr, (p1->b1).x-2.0*extents.width, (p1->b1).y+2.0*extents.height);
        else cairo_move_to(cr, (p1->b1).x-1.3*extents.width, (p1->b1).y+2.0*extents.height);
        cairo_show_text(cr, string);
        g_free(string);
        p1+=(hc->columns);
      }
    cairo_restore(cr);
  }
static void histogram_chart_draw_numbers_right(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[9])
  {
    gint i=0;
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    gdouble font_size=(hc->outside_square)/2.0;

    struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, (hc->rows)*(hc->columns)-(hc->columns));

    cairo_save(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_matrix_init(&matrix1, qrs[0], qrs[3], qrs[1], qrs[4], 0.0, 0.0);
    cairo_transform(cr, &matrix1); 
    cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, font_size);
    for(i=0;i<(hc->columns);i++)
      {
        gchar *string=g_strdup_printf("%i", i);
        /*
          Try to align numbers with base grid. Probably not the best way to do this. Just get
          extents once for one or two numbers so that they line up better.
        */
        if(i==0) cairo_text_extents(cr, string, &extents);
        if(i==10) cairo_text_extents(cr, string, &extents);
        if(i<10) cairo_move_to(cr, (p1->b4).x+1.3*extents.width, (p1->b4).y+1.3*extents.height);
        else cairo_move_to(cr, (p1->b4).x+extents.width/3.0, (p1->b4).y+1.3*extents.height);
        cairo_show_text(cr, string);
        g_free(string);
        p1++;
      }

    p1=&g_array_index(hc->base, struct rectangle3d, hc->columns-1);
    for(i=0;i<(hc->rows);i++)
      {
        gchar *string=g_strdup_printf("%i", i);
        if(i==0) cairo_text_extents(cr, string, &extents);
        cairo_move_to(cr, (p1->b2).x+extents.width/4.0, (p1->b2).y+2.0*extents.height);
        cairo_show_text(cr, string);
        g_free(string);
        p1+=(hc->columns);
      }
    cairo_restore(cr);
  }
static void histogram_chart_draw_numbers_z(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[9])
  {
    gint i=0;
    gdouble length=0.0;
    gdouble a[3]={0.0, 0.0, 0.0};
    gdouble b[3]={0.0, 0.0, 0.0};
    gdouble c[3]={0.0, 0.0, 0.0};
    cairo_matrix_t matrix1;
    gdouble font_size=(hc->outside_square);
    gdouble r[9];
    gdouble out[9];

    cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, font_size);

    //Need to rotate the text 90 degrees.
    quaternion_rotation(0.0, 0.0, -G_PI/2.0, r);
    matrix_multiply(r, qrs, out);

    struct rectangle3d *p1=&g_array_index(hc->base_t, struct rectangle3d, 0);
    struct rectangle3d *p2=&g_array_index(hc->base_t, struct rectangle3d, (hc->columns)-1);

    //Cross product.
    a[0]=(p1->b4).x-(p1->b1).x;
    a[1]=(p1->b4).y-(p1->b1).y;
    a[2]=(p1->b4).z-(p1->b1).z;
    b[0]=(p1->b2).x-(p1->b1).x;
    b[1]=(p1->b2).y-(p1->b1).y;
    b[2]=(p1->b2).z-(p1->b1).z;
    c[0]=a[1]*b[2]-a[2]*b[1];
    c[1]=a[2]*b[0]-a[0]*b[2];
    c[2]=a[0]*b[1]-a[1]*b[0];
    length=sqrt(c[0]*c[0]+c[1]*c[1]+c[2]*c[2]);    
    c[0]/=length;
    c[1]/=length;
    c[2]/=length;

    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    for(i=0;i<4;i++)
      {
        //Move along the cross product normal.
        cairo_move_to(cr, (p2->b2).x+((gdouble)i*50.0*-c[0]), (p2->b2).y+((gdouble)i*50.0*-c[1]));
        cairo_save(cr);
        gchar *string=g_strdup_printf("%i", i);
        //Rotate the text in the center but have it drawn at the move to position.
        cairo_matrix_init(&matrix1, out[0], out[3], out[1], out[4], 0.0, 0.0);
        cairo_transform(cr, &matrix1); 
        cairo_show_text(cr, string);
        g_free(string);
        cairo_restore(cr);
      }
  }
static void histogram_chart_draw_back(struct histogram_chart *hc, cairo_t *cr)
  {
    gdouble length=0.0;
    gdouble a[3]={0.0, 0.0, 0.0};
    gdouble b[3]={0.0, 0.0, 0.0};
    gdouble c[3]={0.0, 0.0, 0.0};
    struct rectangle3d *p1=&g_array_index(hc->base_t, struct rectangle3d, 0);
    struct rectangle3d *p2=&g_array_index(hc->base_t, struct rectangle3d, hc->columns-1);

    //Cross product.
    a[0]=(p1->b4).x-(p1->b1).x;
    a[1]=(p1->b4).y-(p1->b1).y;
    a[2]=(p1->b4).z-(p1->b1).z;
    b[0]=(p1->b2).x-(p1->b1).x;
    b[1]=(p1->b2).y-(p1->b1).y;
    b[2]=(p1->b2).z-(p1->b1).z;
    c[0]=a[1]*b[2]-a[2]*b[1];
    c[1]=a[2]*b[0]-a[0]*b[2];
    c[2]=a[0]*b[1]-a[1]*b[0];
    length=sqrt(c[0]*c[0]+c[1]*c[1]+c[2]*c[2]);    
    c[0]/=length;
    c[1]/=length;
    c[2]/=length;

    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    cairo_set_line_width(cr, 3.0);

    cairo_move_to(cr, (p1->b1).x, (p1->b1).y);
    cairo_line_to(cr, (p1->b1).x+200.0*-c[0], (p1->b1).y+200.0*-c[1]);
    cairo_line_to(cr, (p2->b2).x+200.0*-c[0], (p2->b2).y+200.0*-c[1]); 
    cairo_line_to(cr, (p2->b2).x, (p2->b2).y);
    cairo_close_path(cr);
    cairo_stroke(cr);

    cairo_move_to(cr, (p1->b1).x+50.0*-c[0], (p1->b1).y+50.0*-c[1]);
    cairo_line_to(cr, (p2->b2).x+50.0*-c[0], (p2->b2).y+50.0*-c[1]); 
    cairo_stroke(cr);

    cairo_move_to(cr, (p1->b1).x+100.0*-c[0], (p1->b1).y+100.0*-c[1]);
    cairo_line_to(cr, (p2->b2).x+100.0*-c[0], (p2->b2).y+100.0*-c[1]); 
    cairo_stroke(cr);

    cairo_move_to(cr, (p1->b1).x+150.0*-c[0], (p1->b1).y+150.0*-c[1]);
    cairo_line_to(cr, (p2->b2).x+150.0*-c[0], (p2->b2).y+150.0*-c[1]); 
    cairo_stroke(cr);

    cairo_set_line_width(cr, 1.0);
  }
static void histogram_chart_draw_side(struct histogram_chart *hc, cairo_t *cr)
  {
    gdouble length=0.0;
    gdouble a[3]={0.0, 0.0, 0.0};
    gdouble b[3]={0.0, 0.0, 0.0};
    gdouble c[3]={0.0, 0.0, 0.0};
    struct rectangle3d *p1=&g_array_index(hc->base_t, struct rectangle3d, 0);
    struct rectangle3d *p2=&g_array_index(hc->base_t, struct rectangle3d, (hc->rows)*(hc->columns)-(hc->columns));  

    //Cross product.
    a[0]=(p1->b4).x-(p1->b1).x;
    a[1]=(p1->b4).y-(p1->b1).y;
    a[2]=(p1->b4).z-(p1->b1).z;
    b[0]=(p1->b2).x-(p1->b1).x;
    b[1]=(p1->b2).y-(p1->b1).y;
    b[2]=(p1->b2).z-(p1->b1).z;
    c[0]=a[1]*b[2]-a[2]*b[1];
    c[1]=a[2]*b[0]-a[0]*b[2];
    c[2]=a[0]*b[1]-a[1]*b[0];
    length=sqrt(c[0]*c[0]+c[1]*c[1]+c[2]*c[2]);    
    c[0]/=length;
    c[1]/=length;
    c[2]/=length;

    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    cairo_set_line_width(cr, 3.0);
    cairo_move_to(cr, (p2->b4).x, (p2->b4).y);
    cairo_line_to(cr, (p2->b4).x+200.0*-c[0], (p2->b4).y+200.0*-c[1]);
    cairo_line_to(cr, (p1->b1).x+200.0*-c[0], (p1->b1).y+200.0*-c[1]);
    cairo_line_to(cr, (p1->b1).x, (p1->b1).y);
    cairo_stroke(cr);

    cairo_move_to(cr, (p2->b4).x+50.0*-c[0], (p2->b4).y+50.0*-c[1]);
    cairo_line_to(cr, (p1->b1).x+50.0*-c[0], (p1->b1).y+50.0*-c[1]);
    cairo_stroke(cr);

    cairo_move_to(cr, (p2->b4).x+100.0*-c[0], (p2->b4).y+100.0*-c[1]);
    cairo_line_to(cr, (p1->b1).x+100.0*-c[0], (p1->b1).y+100.0*-c[1]);
    cairo_stroke(cr);

    cairo_move_to(cr, (p2->b4).x+150.0*-c[0], (p2->b4).y+150.0*-c[1]);
    cairo_line_to(cr, (p1->b1).x+150.0*-c[0], (p1->b1).y+150.0*-c[1]);
    cairo_stroke(cr);

    cairo_set_line_width(cr, 1.0);
  }
static void histogram_chart_reset_base(struct histogram_chart *hc)
  {
    struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, 0);
    struct rectangle3d *p2=&g_array_index(hc->base_t, struct rectangle3d, 0);
    memcpy(p2, p1, sizeof(struct rectangle3d)*(hc->base->len));
  }
static void histogram_chart_reset_cubes(struct histogram_chart *hc)
  {
    struct cube3d *p1=&g_array_index(hc->cubes, struct cube3d, 0);
    struct cube3d *p2=&g_array_index(hc->cubes_t, struct cube3d, 0);
    memcpy(p2, p1, sizeof(struct cube3d)*(hc->cubes->len));
  }
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
static void histogram_chart_rotate(struct histogram_chart *hc, gdouble qrs[9])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0;   
    struct rectangle3d *p1=&g_array_index(hc->base_t, struct rectangle3d, 0);
    struct cube3d *p2=&g_array_index(hc->cubes_t, struct cube3d, 0);

    for(i=0;i<(hc->base_t->len);i++)
      {
        cr1=(p1->b1).x;
        cr2=(p1->b1).y;
        cr3=(p1->b1).z;         
        (p1->b1).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p1->b1).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p1->b1).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p1->b2).x;
        cr2=(p1->b2).y;
        cr3=(p1->b2).z;         
        (p1->b2).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p1->b2).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p1->b2).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p1->b3).x;
        cr2=(p1->b3).y;
        cr3=(p1->b3).z;         
        (p1->b3).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p1->b3).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p1->b3).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p1->b4).x;
        cr2=(p1->b4).y;
        cr3=(p1->b4).z;         
        (p1->b4).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p1->b4).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p1->b4).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        p1++;
      }  

    for(i=0;i<(hc->cubes_t->len);i++)
      {
        cr1=(p2->c1).x;
        cr2=(p2->c1).y;
        cr3=(p2->c1).z;         
        (p2->c1).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p2->c1).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p2->c1).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p2->c2).x;
        cr2=(p2->c2).y;
        cr3=(p2->c2).z;         
        (p2->c2).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p2->c2).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p2->c2).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p2->c3).x;
        cr2=(p2->c3).y;
        cr3=(p2->c3).z;         
        (p2->c3).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p2->c3).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p2->c3).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p2->c4).x;
        cr2=(p2->c4).y;
        cr3=(p2->c4).z;         
        (p2->c4).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p2->c4).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p2->c4).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p2->c5).x;
        cr2=(p2->c5).y;
        cr3=(p2->c5).z;         
        (p2->c5).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p2->c5).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p2->c5).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p2->c6).x;
        cr2=(p2->c6).y;
        cr3=(p2->c6).z;         
        (p2->c6).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p2->c6).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p2->c6).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p2->c7).x;
        cr2=(p2->c7).y;
        cr3=(p2->c7).z;         
        (p2->c7).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p2->c7).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p2->c7).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p2->c8).x;
        cr2=(p2->c8).y;
        cr3=(p2->c8).z;         
        (p2->c8).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p2->c8).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p2->c8).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        p2++;
      } 

    //Rotate reference vectors.
    cr1=hc->top[0];
    cr2=hc->top[1];
    cr3=hc->top[2];          
    hc->top_t[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
    hc->top_t[1]=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
    hc->top_t[2]=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);

    cr1=hc->rx[0];
    cr2=hc->rx[1];
    cr3=hc->rx[2];          
    hc->rx_t[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
    hc->rx_t[1]=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
    hc->rx_t[2]=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);

    cr1=hc->ry[0];
    cr2=hc->ry[1];
    cr3=hc->ry[2];          
    hc->ry_t[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
    hc->ry_t[1]=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
    hc->ry_t[2]=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
  
  }
int compare_cubes(const void *a, const void *b)
  {
    return((((struct cube3d*)a)->c1.z)-(((struct cube3d*)b)->c1.z));
  }
static void matrix_multiply(const gdouble in1[9], const gdouble in2[9], gdouble out[9])
  {
    //Multiply two 3x3 matrices.
    out[0]=in1[0]*in2[0]+in1[3]*in2[1]+in1[6]*in2[2];
    out[1]=in1[1]*in2[0]+in1[4]*in2[1]+in1[7]*in2[2];
    out[2]=in1[2]*in2[0]+in1[5]*in2[1]+in1[8]*in2[2];

    out[3]=in1[0]*in2[3]+in1[3]*in2[4]+in1[6]*in2[5];
    out[4]=in1[1]*in2[3]+in1[4]*in2[4]+in1[7]*in2[5];
    out[5]=in1[2]*in2[3]+in1[5]*in2[4]+in1[8]*in2[5];

    out[6]=in1[0]*in2[6]+in1[3]*in2[7]+in1[6]*in2[8];
    out[7]=in1[1]*in2[6]+in1[4]*in2[7]+in1[7]*in2[8];
    out[8]=in1[2]*in2[6]+in1[5]*in2[7]+in1[8]*in2[8];
  }
static void draw_svg(GtkWidget *widget, gpointer *data)
  {
    gdouble width=(gdouble)gtk_widget_get_allocated_width(GTK_WIDGET(data[0]));
    gdouble height=(gdouble)gtk_widget_get_allocated_height(GTK_WIDGET(data[0]));
    cairo_surface_t *surface=cairo_svg_surface_create("histo1.svg", width, height);
    cairo_t *cr=cairo_create(surface);

    //Background.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr); 

    cairo_translate(cr, width/2.0, height/2.0);
    cairo_scale(cr, scale+0.01, scale+0.01);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_BUTT);
    cairo_set_line_width(cr, 1.0);

    //Rotation.
    gdouble qrs[9];
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    quaternion_rotation(yaw1, roll1, pitch1, qrs);
   
    //Draw the chart.
    histogram_chart_draw((struct histogram_chart*)data[1], cr, qrs);

    g_print("histo1.svg saved\n");

    cairo_destroy(cr);
    cairo_surface_destroy(surface);
  }
static void draw_pdf(GtkWidget *widget, gpointer *data)
  {
    gdouble width=(gdouble)gtk_widget_get_allocated_width(GTK_WIDGET(data[0]));
    gdouble height=(gdouble)gtk_widget_get_allocated_height(GTK_WIDGET(data[0]));
    cairo_surface_t *surface=cairo_pdf_surface_create("histo1.pdf", width, height);
    cairo_t *cr=cairo_create(surface);

    //Background.
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_paint(cr); 

    cairo_translate(cr, width/2.0, height/2.0);
    cairo_scale(cr, scale+0.01, scale+0.01);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_BUTT);
    cairo_set_line_width(cr, 1.0);

    //Rotation.
    gdouble qrs[9];
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    quaternion_rotation(yaw1, roll1, pitch1, qrs);
   
    //Draw the chart.
    histogram_chart_draw((struct histogram_chart*)data[1], cr, qrs);

    g_print("histo1.pdf saved\n");

    cairo_destroy(cr);
    cairo_surface_destroy(surface);
  }









