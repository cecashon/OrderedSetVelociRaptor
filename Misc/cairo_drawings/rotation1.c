
/* 
    Test rotating a 2d drawing in 3d with quaternions. Test angles in the domain of real numbers.
    Try cairo transforms for compatibility.
     
    gcc -Wall rotation1.c -o rotation1 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<stdlib.h>
#include<math.h>

//The starting reference plane drawing.
gdouble plane_coords[11][2]={  
      {0.0, -3.0},  
      {-3.0, 0.0},  
      {-1.0, 0.0}, 
      {-1.5, 2.0},
      {-0.5, 2.0},
      {0.0, 1.0},
      {0.5, 2.0},
      {1.5, 2.0},
      {1.0, 0.0},
      {3.0, 0.0},
      {0.0, -3.0},   
      };

//Tick id for animation frame clock.
static guint tick_id=0;

//Animate the plane or keep it still.
static gint rotate=0;

static void rotate_drawing(GtkComboBox *combo, gpointer *rotations);
static void set_rotation_pitch(GtkWidget *button, gpointer *rotations);
static void set_rotation_roll(GtkWidget *button, gpointer *rotations);
static void set_rotation_yaw(GtkWidget *button, gpointer *rotations);
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer *rotations);
static gboolean start_drawing(GtkWidget *da, cairo_t *cr, gpointer *rotations);
static void quaternion_rotation(cairo_t *cr, gdouble yaw, gdouble roll, gdouble pitch);
static void draw_plane(cairo_t *cr, gdouble w1, gdouble h1);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Rotations");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);

    GtkWidget *combo_rotate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_rotate, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 0, "1", "No Rotate");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 1, "2", "Animate Pitch, Roll and Yaw");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_rotate), 0);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *pitch_entry=gtk_entry_new();
    gtk_widget_set_hexpand(pitch_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(pitch_entry), "-10.0");

    GtkWidget *pitch_button=gtk_button_new_with_label("Update Pitch");
    gtk_widget_set_hexpand(pitch_button, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *roll_entry=gtk_entry_new();
    gtk_widget_set_hexpand(roll_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(roll_entry), "-10.0");

    GtkWidget *roll_button=gtk_button_new_with_label("Update Roll");
    gtk_widget_set_hexpand(roll_button, TRUE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);

    GtkWidget *yaw_entry=gtk_entry_new();
    gtk_widget_set_hexpand(yaw_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(yaw_entry), "-10.0");

    GtkWidget *yaw_button=gtk_button_new_with_label("Update Yaw");
    gtk_widget_set_hexpand(yaw_button, TRUE);

    //Set initial rotations for the plane. Yaw, roll and pitch.
    gdouble plane_r[3]={-10.0, -10.0, -10.0};

    //Setup callbacks.
    gpointer rotations[]={pitch_entry, roll_entry, yaw_entry, plane_r, da};
    g_signal_connect(da, "draw", G_CALLBACK(start_drawing), rotations);
    g_signal_connect(combo_rotate, "changed", G_CALLBACK(rotate_drawing), rotations);
    g_signal_connect(pitch_button, "clicked", G_CALLBACK(set_rotation_pitch), rotations);
    g_signal_connect(roll_button, "clicked", G_CALLBACK(set_rotation_roll), rotations);
    g_signal_connect(yaw_button, "clicked", G_CALLBACK(set_rotation_yaw), rotations);

    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_rotate, 0, 0, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_entry, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_button, 0, 2, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_entry, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_button, 0, 4, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 0, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_entry, 1, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_button, 0, 6, 2, 1);

    GtkWidget *scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll, TRUE);
    gtk_widget_set_vexpand(scroll, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll), grid1);
   
    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), scroll, TRUE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 300);
   
    gtk_container_add(GTK_CONTAINER(window), paned1);

    gtk_widget_show_all(window);

    gtk_main();

    return 0;  
  }
static void rotate_drawing(GtkComboBox *combo, gpointer *rotations)
  {
    rotate=gtk_combo_box_get_active(combo);
    //Reset rotations for animation. Check some negative numbers.
    ((gdouble*)(rotations[3]))[0]=-10.0;
    ((gdouble*)(rotations[3]))[1]=-10.0;
    ((gdouble*)(rotations[3]))[2]=-10.0;
    
    if(rotate==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(rotations[4]), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(rotations[4]));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(rotations[4]), (GtkTickCallback)animate_drawing, rotations, NULL);
          }
      }
    
  }
static void set_rotation_pitch(GtkWidget *button, gpointer *rotations)
  {
    g_print("Set Pitch\n");
    gdouble x=strtod(gtk_entry_get_text(GTK_ENTRY(rotations[0])), NULL);

    ((gdouble*)(rotations[3]))[2]=x;

    gtk_widget_queue_draw(GTK_WIDGET(rotations[4]));
  }
static void set_rotation_roll(GtkWidget *button, gpointer *rotations)
  {
    g_print("Set Roll\n");
    gdouble y=strtod(gtk_entry_get_text(GTK_ENTRY(rotations[1])), NULL);
   
    ((gdouble*)(rotations[3]))[1]=y;

    gtk_widget_queue_draw(GTK_WIDGET(rotations[4]));
  }
static void set_rotation_yaw(GtkWidget *button, gpointer *rotations)
  {
    g_print("Set Yaw\n");
    gdouble z=strtod(gtk_entry_get_text(GTK_ENTRY(rotations[2])), NULL);
   
    ((gdouble*)(rotations[3]))[0]=z;

    gtk_widget_queue_draw(GTK_WIDGET(rotations[4]));
  }
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer *rotations)
  {
    //Apply angles of rotation for animation.
    ((gdouble*)(rotations[3]))[0]+=1.0;
    ((gdouble*)(rotations[3]))[1]+=1.0;
    ((gdouble*)(rotations[3]))[2]+=1.0; 

    //Check frame rate.
    gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
    gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
    gint64 start = gdk_frame_clock_get_history_start(frame_clock);
    gint64 history_len=frame-start;
    GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
    gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
    g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));   

    gtk_widget_queue_draw(drawing);
    return G_SOURCE_CONTINUE;
  }
static gboolean start_drawing(GtkWidget *da, cairo_t *cr, gpointer *rotations)
  {
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;
    gdouble yaw=((gdouble*)(rotations[3]))[0]*G_PI/180.0;
    gdouble roll=((gdouble*)(rotations[3]))[1]*G_PI/180.0;
    gdouble pitch=((gdouble*)(rotations[3]))[2]*G_PI/180.0;
    gdouble c1=0;

    //Size circles.
    if(w1>h1) c1=h1;
    else c1=w1;

    //Paint background.
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_paint(cr);

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 1.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Draw in the center
    cairo_translate(cr, width/2.0, height/2.0);
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);

    //Test a cairo rotate.
    //cairo_rotate(cr, G_PI/2.0);

    cairo_set_line_width(cr, 15.0);
    cairo_set_source_rgba(cr, 0.5, 0.5, 1.0, 1.0);
    cairo_arc(cr, 0.0, 0.0, 4.0*c1, 0.0, 2.0*G_PI);
    cairo_stroke(cr);

    cairo_save(cr);
    cairo_scale(cr, 8.0*c1/400.0, 8.0*c1/400.0);
    //Calculate the current rotations for the plane using radians.
    quaternion_rotation(cr, yaw, roll, pitch);
    cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 0.7);
    cairo_set_line_width(cr, 4.0);
    draw_plane(cr, w1, h1);
    cairo_restore(cr);
 
    //The yaw circle. 
    cairo_save(cr);
    quaternion_rotation(cr, yaw, roll, pitch);
    cairo_arc(cr, 0.0, 0.0, 4.0*c1, 0.0, 2.0*G_PI);
    cairo_restore(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    cairo_set_line_width(cr, 8.0);
    cairo_stroke(cr);
     
    //Roll circle.
    cairo_save(cr);     
    quaternion_rotation(cr, yaw, roll, pitch+G_PI/2.0);
    cairo_arc(cr, 0.0, 0.0, 4.0*c1, 0.0, 2.0*G_PI);
    cairo_restore(cr);
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    cairo_set_line_width(cr, 8.0);
    cairo_stroke(cr);

    //Pitch circle.
    cairo_save(cr);     
    quaternion_rotation(cr, yaw, roll+G_PI/2.0, 0.0);
    cairo_arc(cr, 0.0, 0.0, 4.0*c1, 0.0, 2.0*G_PI);
    cairo_restore(cr);
    cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
    cairo_set_line_width(cr, 8.0);
    cairo_stroke(cr);

    return FALSE;
  }
static void quaternion_rotation(cairo_t *cr, gdouble yaw, gdouble roll, gdouble pitch)
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion

       Rotate a plane with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians.
    */
    gdouble qi=0.0;
    gdouble qj=0.0;
    gdouble qk=0.0;
    gdouble qr=0.0;
    gdouble r1=0.0;
    gdouble r2=0.0;
    gdouble r4=0.0;
    gdouble r5=0.0;

    qi=sin(pitch/2.0)*cos(roll/2.0)*cos(yaw/2.0)-cos(pitch/2.0)*sin(roll/2.0)*sin(yaw/2.0);
    qj=cos(pitch/2.0)*sin(roll/2.0)*cos(yaw/2.0)+sin(pitch/2.0)*cos(roll/2.0)*sin(yaw/2.0);
    qk=cos(pitch/2.0)*cos(roll/2.0)*sin(yaw/2.0)-sin(pitch/2.0)*sin(roll/2.0)*cos(yaw/2.0);
    qr=cos(pitch/2.0)*cos(roll/2.0)*cos(yaw/2.0)+sin(pitch/2.0)*sin(roll/2.0)*sin(yaw/2.0);

    r1=1-2.0*qj*qj-2.0*qk*qk;
    r2=2.0*(qi*qj-qk*qr);
    r4=2.0*(qi*qj+qk*qr);
    r5=1.0-2.0*qi*qi-2.0*qk*qk;
    
    cairo_matrix_t matrix;
    cairo_matrix_init(&matrix, r1, r4, r2, r5, 0.0, 0.0);
    cairo_transform(cr, &matrix);
  }
static void draw_plane(cairo_t *cr, gdouble w1, gdouble h1)
  {
    cairo_move_to(cr, plane_coords[0][0]*w1, plane_coords[0][1]*h1);
    cairo_line_to(cr, plane_coords[1][0]*w1, plane_coords[1][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_coords[2][0]*w1, plane_coords[2][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_coords[3][0]*w1, plane_coords[3][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_coords[4][0]*w1, plane_coords[4][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_coords[5][0]*w1, plane_coords[5][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_coords[6][0]*w1, plane_coords[6][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_coords[7][0]*w1, plane_coords[7][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_coords[8][0]*w1, plane_coords[8][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_coords[9][0]*w1, plane_coords[9][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_coords[10][0]*w1, plane_coords[10][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_fill_preserve(cr);
    cairo_stroke(cr);
  }

