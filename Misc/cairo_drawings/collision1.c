/*
    If a rectangle is over a triangle in a box then color the rectangle yellow. Try to refine
the collision and draw the rectangle yellow just over the triangle. Still some problems since
it is possible for the triangle and rectangle to intersect but no points are contained in the
other shape. Test a circle over the triangle also.

    gcc -Wall collision1.c -o collision1 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<string.h>
#include<math.h>

struct triangle{
  gdouble x1;
  gdouble y1;
  gdouble x2;
  gdouble y2;
  gdouble x3;
  gdouble y3;
  gdouble vx;
  gdouble vy;
  gboolean overlap;
};

struct rectangle{
  gdouble x1;
  gdouble y1;
  gdouble x2;
  gdouble y2;
  gdouble x3;
  gdouble y3;
  gdouble x4;
  gdouble y4;
  gdouble vx;
  gdouble vy;
  gboolean overlap;
};

struct circle{
  gdouble x1;
  gdouble y1;
  gdouble vx;
  gdouble vy;
  gdouble radius;
  gboolean overlap;
};

GArray *triangles=NULL;
//A copy of triangles to rotate.
GArray *triangles_t=NULL;
GArray *rectangles=NULL;
GArray *circles=NULL;

static gdouble velocity=0.2;
static gdouble window_width=0.0;
static gdouble window_height=0.0;
static gdouble rec_width=40.0;
static gdouble rec_height=20.0;
static gdouble tri_width=40.0;
static gdouble tri_height=40.0;
static gdouble circle_radius=10.0;

//Show the frame rate.
static gboolean frame_rate=FALSE;
static gboolean rotate_tri=FALSE;
static guint collision_id=0;
static gdouble pitch=0.0;
static gdouble roll=0.0;
static gdouble yaw=0.0;

static guint tick_id=0;

static void set_collision_id(GtkComboBox *combo, gpointer data);
static void set_velocity(GtkComboBox *combo, gpointer data);
static void set_triangle_scale(GtkSpinButton *spin_button, gpointer data);
static void set_rectangle_scale(GtkSpinButton *spin_button, gpointer data);
static void set_circle_scale(GtkSpinButton *spin_button, gpointer data);
static void show_frame_rate(GtkToggleButton *button, gpointer data);
static void spin_triangles(GtkToggleButton *button, gpointer data);
static void initialize_triangles(gint num);
static void initialize_rectangles(gint num);
static void initialize_circles(gint num);
static gboolean start_draw_shapes(GtkWidget *da, GdkFrameClock *frame_clock, gpointer data);
static gboolean draw_shapes(GtkWidget *da, cairo_t *cr, gpointer data);
static void scale_triangles(gdouble scale);
static void scale_rectangles(gdouble scale);
static void scale_circles(gdouble scale);
static void update_shape_positions();
//Detect intersecting shapes.
static void check_overlap_rectangle();
static void check_overlap_circle();
static gboolean point_in_triangle(gdouble rx, gdouble ry, struct triangle *t1);
static gboolean same_side(gdouble pt1_x, gdouble pt1_y, gdouble pt2_x, gdouble pt2_y, gdouble ax, gdouble ay, gdouble bx, gdouble by);
static gboolean triangle_point_in_rectangle(struct triangle *p1, struct rectangle *p2);
static gboolean triangle_point_in_circle(struct triangle *p1, struct circle *p2);
//Barycentric approach.
static gboolean point_in_triangle2(gdouble rx, gdouble ry, struct triangle *t1);
//Rotate the triangle.
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9]);
static void rotate_triangles(gdouble qrs[9]);
static void reset_triangles_t();

int main(int argc, char **argv)
 {
   gtk_init(&argc, &argv);

   GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
   gtk_window_set_title(GTK_WINDOW(window), "Collisions");
   gtk_window_set_default_size(GTK_WINDOW(window), 750, 500);
   gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
   g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

   initialize_triangles(40);
   initialize_rectangles(20);
   initialize_circles(20);

   GtkWidget *da=gtk_drawing_area_new();
   gtk_widget_set_hexpand(da, TRUE);
   gtk_widget_set_vexpand(da, TRUE);
   g_signal_connect(da, "draw", G_CALLBACK(draw_shapes), NULL);

   GtkWidget *combo_collision=gtk_combo_box_text_new();
   gtk_widget_set_hexpand(combo_collision, TRUE);
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_collision), 0, "1", "Collision Triangle Box");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_collision), 1, "2", "Collision Triangle1");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_collision), 2, "3", "Collision Triangle2");
   gtk_combo_box_set_active(GTK_COMBO_BOX(combo_collision), 0);
   g_signal_connect(combo_collision, "changed", G_CALLBACK(set_collision_id), da);

   GtkWidget *combo_velocity=gtk_combo_box_text_new();
   gtk_widget_set_hexpand(combo_velocity, TRUE);
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_velocity), 0, "1", "Velocity 0.1");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_velocity), 1, "2", "Velocity 0.2");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_velocity), 2, "3", "Velocity 0.5");
   gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_velocity), 3, "4", "Velocity 0.8");
   gtk_combo_box_set_active(GTK_COMBO_BOX(combo_velocity), 1);
   g_signal_connect(combo_velocity, "changed", G_CALLBACK(set_velocity), da);

   GtkWidget *triangle_label=gtk_label_new("Scale Triangle");

   GtkWidget *rectangle_label=gtk_label_new("Scale Rectangle");

   GtkWidget *circle_label=gtk_label_new("Scale Circle");
   
   GtkAdjustment *adjustment1=gtk_adjustment_new(1.0, 0.5, 1.5, 0.1, 0, 0);
   GtkAdjustment *adjustment2=gtk_adjustment_new(1.0, 0.5, 1.5, 0.1, 0, 0);
   GtkAdjustment *adjustment3=gtk_adjustment_new(1.0, 0.5, 1.5, 0.1, 0, 0);

   GtkWidget *spin_triangle_scale=gtk_spin_button_new(adjustment1, 1, 1);
   g_signal_connect(spin_triangle_scale, "value-changed", G_CALLBACK(set_triangle_scale), da);

   GtkWidget *spin_rectangle_scale=gtk_spin_button_new(adjustment2, 1, 1);
   g_signal_connect(spin_rectangle_scale, "value-changed", G_CALLBACK(set_rectangle_scale), da);

   GtkWidget *spin_circle_scale=gtk_spin_button_new(adjustment3, 1, 1);
   g_signal_connect(spin_circle_scale, "value-changed", G_CALLBACK(set_circle_scale), da);

   GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
   gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
   g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), NULL);

   GtkWidget *check_rotate=gtk_check_button_new_with_label("Rotate Triangles");
   gtk_widget_set_halign(check_rotate, GTK_ALIGN_CENTER);
   g_signal_connect(check_rotate, "toggled", G_CALLBACK(spin_triangles), NULL);

   GtkWidget *grid=gtk_grid_new();
   gtk_grid_set_row_spacing(GTK_GRID(grid), 5);
   gtk_container_set_border_width(GTK_CONTAINER(grid), 8);
   gtk_grid_attach(GTK_GRID(grid), combo_collision, 0, 0, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), combo_velocity, 0, 1, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), triangle_label, 0, 2, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), spin_triangle_scale, 0, 3, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), rectangle_label, 0, 4, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), spin_rectangle_scale, 0, 5, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), circle_label, 0, 6, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), spin_circle_scale, 0, 7, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), check_frame, 0, 8, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), check_rotate, 0, 9, 1, 1);

   GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
   gtk_paned_pack1(GTK_PANED(paned1), grid, TRUE, TRUE);
   gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
   gtk_paned_set_position(GTK_PANED(paned1), 250);

   gtk_container_add(GTK_CONTAINER(window), paned1);

   tick_id=gtk_widget_add_tick_callback(da, (GtkTickCallback)start_draw_shapes, NULL, NULL);

   gtk_widget_show_all(window);

   gtk_main();

   g_array_free(triangles, TRUE);
   g_array_free(triangles_t, TRUE);
   g_array_free(rectangles, TRUE);
   g_array_free(circles, TRUE);

   return 0;  
 }
static void set_collision_id(GtkComboBox *combo, gpointer data)
  {  
    collision_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_velocity(GtkComboBox *combo, gpointer data)
  {  
    gint row=gtk_combo_box_get_active(combo);

    if(row==0) velocity=0.1;
    else if(row==1) velocity=0.2;
    else if(row==2) velocity=0.5;
    else velocity=0.8;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_triangle_scale(GtkSpinButton *spin_button, gpointer data)
  {
    static gdouble last=1.0;
    gdouble scale=0.0;
    gdouble spin=gtk_spin_button_get_value(spin_button);

    if(spin>last) scale=0.1;
    else if(spin<last) scale=-0.1;
    else scale=0.0;
    last=spin;

    scale_triangles(scale);

    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_rectangle_scale(GtkSpinButton *spin_button, gpointer data)
  {
    static gdouble last=1.0;
    gdouble scale=0.0;
    gdouble spin=gtk_spin_button_get_value(spin_button);

    if(spin>last) scale=0.1;
    else if(spin<last) scale=-0.1;
    else scale=0.0;
    last=spin;

    scale_rectangles(scale);

    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_circle_scale(GtkSpinButton *spin_button, gpointer data)
  {
    static gdouble last=1.0;
    gdouble scale=0.0;
    gdouble spin=gtk_spin_button_get_value(spin_button);

    if(spin>last) scale=0.1;
    else if(spin<last) scale=-0.1;
    else scale=0.0;
    last=spin;

    scale_circles(scale);

    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void show_frame_rate(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void spin_triangles(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) rotate_tri=TRUE;
    else rotate_tri=FALSE;
  }
static void initialize_triangles(gint num)
  {
    gint i=0;
    gdouble angle=0;
    gdouble rand_x=0.0;
    gdouble rand_y=0.0;
    triangles=g_array_sized_new(FALSE, FALSE, sizeof(struct triangle), num);
    g_array_set_size(triangles, num);
    triangles_t=g_array_sized_new(FALSE, FALSE, sizeof(struct triangle), num);
    g_array_set_size(triangles_t, num);
    struct triangle *p1=&g_array_index(triangles, struct triangle, 0);

    for(i=0;i<num;i++)
      {
        rand_x=500.0*g_random_double();
        rand_y=500.0*g_random_double();
        (*p1).x1=rand_x;
        (*p1).y1=rand_y;
        (*p1).x2=rand_x+tri_width/2.0;
        (*p1).y2=rand_y+tri_height;
        (*p1).x3=rand_x-tri_width/2.0;
        (*p1).y3=rand_y+tri_height;
        angle=2.0*G_PI*g_random_double();
        (*p1).vx=cos(angle); 
        (*p1).vy=sin(angle);
        (*p1).overlap=FALSE;   
        p1++;
      }
    
    //initialize the triangles_t array.
    reset_triangles_t();
  }
static void initialize_rectangles(gint num)
  {
    gint i=0;
    gdouble angle=0;
    gdouble rand_x=0.0;
    gdouble rand_y=0.0;
    rectangles=g_array_sized_new(FALSE, FALSE, sizeof(struct rectangle), num);
    g_array_set_size(rectangles, num);
    struct rectangle *p1=&g_array_index(rectangles, struct rectangle, 0);

    for(i=0;i<num;i++)
      {
        rand_x=500.0*g_random_double();
        rand_y=500.0*g_random_double();
        (*p1).x1=rand_x;
        (*p1).y1=rand_y;
        (*p1).x2=rand_x+rec_width;
        (*p1).y2=rand_y;
        (*p1).x3=rand_x+rec_width;
        (*p1).y3=rand_y+rec_height;
        (*p1).x4=rand_x;
        (*p1).y4=rand_y+rec_height;
        angle=2.0*G_PI*g_random_double();
        (*p1).vx=cos(angle); 
        (*p1).vy=sin(angle); 
        (*p1).overlap=FALSE;     
        p1++;
      }
  }
static void initialize_circles(gint num)
  {
    gint i=0;
    gdouble angle=0;
    circles=g_array_sized_new(FALSE, FALSE, sizeof(struct circle), num);
    g_array_set_size(circles, num);
    struct circle *p1=&g_array_index(circles, struct circle, 0);

    for(i=0;i<num;i++)
      {
        (*p1).x1=500.0*g_random_double();
        (*p1).y1=500.0*g_random_double();        
        angle=2.0*G_PI*g_random_double();
        (*p1).vx=cos(angle); 
        (*p1).vy=sin(angle);
        (*p1).radius=circle_radius;
        (*p1).overlap=FALSE;   
        p1++;
      }
  }
static gboolean start_draw_shapes(GtkWidget *da, GdkFrameClock *frame_clock, gpointer data)
  {
    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    update_shape_positions();

    //Rotate the triangles.
    reset_triangles_t();
    if(rotate_tri)
      {
        gdouble qrs[9];
        pitch+=0.5;
        roll+=0.5;
        yaw+=0.5;
        quaternion_rotation(yaw*G_PI/180.0, roll*G_PI/180.0, pitch*G_PI/180.0, qrs);
        rotate_triangles(qrs);
      }
    else
      {
        pitch=0.0;
        roll=0.0;
        yaw=0.0;
      }
    
    check_overlap_rectangle();
    check_overlap_circle();

    gtk_widget_queue_draw(da);
    return G_SOURCE_CONTINUE;
  }
static gboolean draw_shapes(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    gint i=0;
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    window_width=width;
    window_height=height;
  
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_paint(cr);

    struct triangle *p1=&g_array_index(triangles_t, struct triangle, 0);
    struct rectangle *p2=&g_array_index(rectangles, struct rectangle, 0);
    struct circle *p3=&g_array_index(circles, struct circle, 0);
    struct triangle *p4=&g_array_index(triangles, struct triangle, 0);

    cairo_set_line_width(cr, 1.0);
    cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
    for(i=0;i<triangles_t->len;i++)
      {
        cairo_move_to(cr, p1->x1, p1->y1);
        cairo_line_to(cr, p1->x2, p1->y2);
        cairo_line_to(cr, p1->x3, p1->y3);
        cairo_close_path(cr);
        cairo_fill(cr);     
        p1++;
      }

    //Unrotated box around the triangle.
    if(rotate_tri)
      {
        cairo_set_line_width(cr, 3.0);
        cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
        for(i=0;i<triangles->len;i++)
          {
            cairo_move_to(cr, p4->x1, p4->y1);
            cairo_line_to(cr, (p4->x1)+((p4->x2)-(p4->x3))/2.0, (p4->y1)+((p4->y2)-(p4->y3))/2.0);
            cairo_line_to(cr, p4->x2, p4->y2);
            cairo_line_to(cr, p4->x3, p4->y3);
            cairo_line_to(cr, (p4->x1)-((p4->x2)-(p4->x3))/2.0, (p4->y1)-((p4->y2)-(p4->y3))/2.0);
            cairo_close_path(cr);
            cairo_stroke(cr);     
            p4++;
          }
      }

    //Rotated box around the triangle.
    p1=&g_array_index(triangles_t, struct triangle, 0);
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    cairo_set_line_width(cr, 3.0);
    for(i=0;i<triangles_t->len;i++)
      {
        cairo_move_to(cr, p1->x1, p1->y1);
        cairo_line_to(cr, (p1->x1)+((p1->x2)-(p1->x3))/2.0, (p1->y1)+((p1->y2)-(p1->y3))/2.0);
        cairo_line_to(cr, p1->x2, p1->y2);
        cairo_line_to(cr, p1->x3, p1->y3);
        cairo_line_to(cr, (p1->x1)-((p1->x2)-(p1->x3))/2.0, (p1->y1)-((p1->y2)-(p1->y3))/2.0);
        cairo_close_path(cr);
        cairo_stroke(cr);     
        p1++;
      }

    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
    for(i=0;i<rectangles->len;i++)
      {
        if((p2->overlap)==TRUE)
          {
            cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
            (p2->overlap)=FALSE;
          } 
        else cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
        cairo_move_to(cr, p2->x1, p2->y1);
        cairo_line_to(cr, p2->x2, p2->y2);
        cairo_line_to(cr, p2->x3, p2->y3);
        cairo_line_to(cr, p2->x4, p2->y4);
        cairo_close_path(cr);
        cairo_fill(cr);     
        p2++;
      }

    for(i=0;i<circles->len;i++)
      {
        if((p3->overlap)==TRUE)
          {
            cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
            (p3->overlap)=FALSE;
          } 
        else cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
        cairo_arc(cr, p3->x1, p3->y1, p3->radius, 0.0, 2.0*G_PI);        
        cairo_fill(cr);     
        p3++;
      }

    return FALSE;
  }
static void scale_triangles(gdouble scale)
  {
    gint i=0;
    struct triangle *p1=&g_array_index(triangles, struct triangle, 0);

    for(i=0;i<triangles->len;i++)
      {
        (p1->y1)-=(scale*tri_height/2.0);
        (p1->x2)+=(scale*tri_width/2.0);
        (p1->y2)+=(scale*tri_height/2.0);
        (p1->x3)-=(scale*tri_width/2.0);
        (p1->y3)+=(scale*tri_height/2.0);
        p1++;
      }

    tri_width+=(scale*tri_width);
    tri_height+=(scale*tri_height);
  }
static void scale_rectangles(gdouble scale)
  {
    gint i=0;
    struct rectangle *p1=&g_array_index(rectangles, struct rectangle, 0);

    for(i=0;i<rectangles->len;i++)
      {
        (p1->x1)-=(scale*rec_width);
        (p1->y1)-=(scale*rec_height);
        (p1->x2)+=(scale*rec_width);
        (p1->y2)-=(scale*rec_height);
        (p1->x3)+=(scale*rec_width);
        (p1->y3)+=(scale*rec_height);
        (p1->x4)-=(scale*rec_width);
        (p1->y4)+=(scale*rec_height);
        p1++;
      }

    rec_width+=(scale*rec_width);
    rec_height+=(scale*rec_height);
  }
static void scale_circles(gdouble scale)
  {
    gint i=0;
    struct circle *p1=&g_array_index(circles, struct circle, 0);

    for(i=0;i<circles->len;i++)
      {
        (p1->radius)+=(scale*circle_radius);
        p1++;
      }

    circle_radius+=(scale*circle_radius);
  }
static void update_shape_positions()
  {
    //Check window boundries and update positions.
    gint i=0;
    struct triangle *p1=&g_array_index(triangles, struct triangle, 0);
    struct rectangle *p2=&g_array_index(rectangles, struct rectangle, 0);
    struct circle *p3=&g_array_index(circles, struct circle, 0);

    for(i=0;i<triangles->len;i++)
      {
        if((p1->x1)>window_width)
          {
            (p1->x1)-=window_width; 
            (p1->y1)+=velocity*(p1->vy);
            (p1->x2)-=window_width; 
            (p1->y2)+=velocity*(p1->vy);
            (p1->x3)-=window_width; 
            (p1->y3)+=velocity*(p1->vy);
          }
        else if((p1->y1)>window_height)
          {
            (p1->x1)+=velocity*(p1->vx);
            (p1->y1)-=window_height;
            (p1->x2)+=velocity*(p1->vx);
            (p1->y2)-=window_height;
            (p1->x3)+=velocity*(p1->vx);
            (p1->y3)-=window_height;
          }
        if((p1->x1)<0.0)
          {
            (p1->x1)+=window_width; 
            (p1->y1)+=velocity*(p1->vy);
            (p1->x2)+=window_width; 
            (p1->y2)+=velocity*(p1->vy);
            (p1->x3)+=window_width; 
            (p1->y3)+=velocity*(p1->vy);
          }
        else if((p1->y1)<0.0)
          {
            (p1->x1)+=velocity*(p1->vx);
            (p1->y1)+=window_height;
            (p1->x2)+=velocity*(p1->vx);
            (p1->y2)+=window_height;
            (p1->x3)+=velocity*(p1->vx);
            (p1->y3)+=window_height;
          }
        else
          {
            (p1->x1)+=velocity*(p1->vx);
            (p1->y1)+=velocity*(p1->vy);
            (p1->x2)+=velocity*(p1->vx);
            (p1->y2)+=velocity*(p1->vy);
            (p1->x3)+=velocity*(p1->vx);
            (p1->y3)+=velocity*(p1->vy);
          }
        p1++;
      }

    for(i=0;i<rectangles->len;i++)
      {
        if((p2->x1)>window_width)
          {
            (p2->x1)-=window_width; 
            (p2->y1)+=velocity*(p2->vy);
            (p2->x2)-=window_width; 
            (p2->y2)+=velocity*(p2->vy);
            (p2->x3)-=window_width; 
            (p2->y3)+=velocity*(p2->vy);
            (p2->x4)-=window_width; 
            (p2->y4)+=velocity*(p2->vy);
          }
        else if((p2->y1)>window_height)
          {
            (p2->x1)+=velocity*(p2->vx);
            (p2->y1)-=window_height;
            (p2->x2)+=velocity*(p2->vx);
            (p2->y2)-=window_height;
            (p2->x3)+=velocity*(p2->vx);
            (p2->y3)-=window_height;
            (p2->x4)+=velocity*(p2->vx);
            (p2->y4)-=window_height;
          }
        if((p2->x1)<0.0)
          {
            (p2->x1)+=window_width; 
            (p2->y1)+=velocity*(p2->vy);
            (p2->x2)+=window_width; 
            (p2->y2)+=velocity*(p2->vy);
            (p2->x3)+=window_width; 
            (p2->y3)+=velocity*(p2->vy);
            (p2->x4)+=window_width; 
            (p2->y4)+=velocity*(p2->vy);
          }
        else if((p2->y1)<0.0)
          {
            (p2->x1)+=velocity*(p2->vx);
            (p2->y1)+=window_height;
            (p2->x2)+=velocity*(p2->vx);
            (p2->y2)+=window_height;
            (p2->x3)+=velocity*(p2->vx);
            (p2->y3)+=window_height;
            (p2->x4)+=velocity*(p2->vx);
            (p2->y4)+=window_height;
          }
        else
          {
            (p2->x1)+=velocity*(p2->vx);
            (p2->y1)+=velocity*(p2->vy);
            (p2->x2)+=velocity*(p2->vx);
            (p2->y2)+=velocity*(p2->vy);
            (p2->x3)+=velocity*(p2->vx);
            (p2->y3)+=velocity*(p2->vy);
            (p2->x4)+=velocity*(p2->vx);
            (p2->y4)+=velocity*(p2->vy);
          }  
        p2++;
      }

    for(i=0;i<circles->len;i++)
      {
        if((p3->x1)>window_width)
          {
            (p3->x1)-=window_width; 
            (p3->y1)+=velocity*(p3->vy);
          }
        else if((p3->y1)>window_height)
          {
            (p3->x1)+=velocity*(p3->vx);
            (p3->y1)-=window_height;
          }
        if((p3->x1)<0.0)
          {
            (p3->x1)+=window_width; 
            (p3->y1)+=velocity*(p3->vy);
          }
        else if((p3->y1)<0.0)
          {
            (p3->x1)+=velocity*(p3->vx);
            (p3->y1)+=window_height;
          }
        else
          {
            (p3->x1)+=velocity*(p3->vx);
            (p3->y1)+=velocity*(p3->vy);
          }
        p3++;
      }
  }
static void check_overlap_rectangle()
  {
    gint i=0;
    gint j=0;
    gdouble t_x1=0.0;
    gdouble t_x2=0.0;
    gdouble t_y1=0.0;
    gdouble t_y2=0.0;
    gdouble cx=0.0;
    gdouble cy=0.0;

    struct triangle *p1=&g_array_index(triangles_t, struct triangle, 0);
    struct rectangle *p2=&g_array_index(rectangles, struct rectangle, 0);
    //Use the box from the unrotated triangles.
    struct triangle *p3=&g_array_index(triangles, struct triangle, 0);

    for(i=0;i<triangles_t->len;i++)
      {
        p2=&g_array_index(rectangles, struct rectangle, 0);
        t_x1=(p3->x1)-tri_width/2.0;
        t_y1=(p3->y1);
        t_x2=(p3->x2);
        t_y2=(p3->y2);
        p3++;
        for(j=0;j<rectangles->len;j++)
          {
            cx=(p2->x1);
            cy=(p2->y1);
            if((cx>t_x1)&&((cx)<t_x2)) 
              {
                if(((cy)>t_y1)&&((cy)<t_y2)) 
                  {
                    if(collision_id==1)
                      {
                        if(point_in_triangle(cx, cy, p1)||triangle_point_in_rectangle(p1, p2)) (p2->overlap)=TRUE;
                      }
                    else if(collision_id==2)
                      {
                        if(point_in_triangle2(cx, cy, p1)||triangle_point_in_rectangle(p1, p2)) (p2->overlap)=TRUE;
                      }
                    else (p2->overlap)=TRUE;
                  }
              }
            cx=(p2->x2);
            cy=(p2->y2);
            if((cx>t_x1)&&((cx)<t_x2)) 
              {
                if(((cy)>t_y1)&&((cy)<t_y2)) 
                  {
                    if(collision_id==1)
                      {
                        if(point_in_triangle(cx, cy, p1)||triangle_point_in_rectangle(p1, p2)) (p2->overlap)=TRUE;
                      }
                    else if(collision_id==2)
                      {
                        if(point_in_triangle2(cx, cy, p1)||triangle_point_in_rectangle(p1, p2)) (p2->overlap)=TRUE;
                      }
                    else (p2->overlap)=TRUE;
                  }
              }
            cx=(p2->x3);
            cy=(p2->y3);
            if((cx>t_x1)&&((cx)<t_x2)) 
              {
                if(((cy)>t_y1)&&((cy)<t_y2)) 
                  {
                    if(collision_id==1)
                      {
                        if(point_in_triangle(cx, cy, p1)||triangle_point_in_rectangle(p1, p2)) (p2->overlap)=TRUE;
                      }
                    else if(collision_id==2)
                      {
                        if(point_in_triangle2(cx, cy, p1)||triangle_point_in_rectangle(p1, p2)) (p2->overlap)=TRUE;
                      }
                    else (p2->overlap)=TRUE;
                  }
              }
            cx=(p2->x4);
            cy=(p2->y4);
            if((cx>t_x1)&&((cx)<t_x2)) 
              {
                if(((cy)>t_y1)&&((cy)<t_y2)) 
                  {
                    if(collision_id==1)
                      {
                        if(point_in_triangle(cx, cy, p1)||triangle_point_in_rectangle(p1, p2)) (p2->overlap)=TRUE;
                      }
                    else if(collision_id==2)
                      {
                        if(point_in_triangle2(cx, cy, p1)||triangle_point_in_rectangle(p1, p2)) (p2->overlap)=TRUE;
                      }
                    else (p2->overlap)=TRUE;
                  }
              }
            p2++;
          }
        p1++;
      }
  }
static void check_overlap_circle()
  {
    gint i=0;
    gint j=0;
    gdouble t_x1=0.0;
    gdouble t_x2=0.0;
    gdouble t_y1=0.0;
    gdouble t_y2=0.0;
    gdouble cx=0.0;
    gdouble cy=0.0;

    struct triangle *p1=&g_array_index(triangles_t, struct triangle, 0);
    struct circle *p2=&g_array_index(circles, struct circle, 0);
    struct triangle *p3=&g_array_index(triangles, struct triangle, 0);

    for(i=0;i<triangles_t->len;i++)
      {
        p2=&g_array_index(circles, struct circle, 0);
        t_x1=(p3->x1)-tri_width/2.0;
        t_y1=(p3->y1);
        t_x2=(p3->x2);
        t_y2=(p3->y2);
        p3++;
        for(j=0;j<circles->len;j++)
          {
            //Check center point of circle.
            cx=(p2->x1);
            cy=(p2->y1);
            if((cx>t_x1)&&((cx)<t_x2)) 
              {
                if(((cy)>t_y1)&&((cy)<t_y2)) 
                  {
                    if(collision_id==1)
                      {
                        if(point_in_triangle(cx, cy, p1)||triangle_point_in_circle(p1, p2)) (p2->overlap)=TRUE;
                      }
                    else if(collision_id==2)
                      {
                        if(point_in_triangle2(cx, cy, p1)||triangle_point_in_circle(p1, p2)) (p2->overlap)=TRUE;
                      }
                    else (p2->overlap)=TRUE;
                  }
              }
            //Check leading edge of circle along the velocity vector.
            cx=(p2->x1)+circle_radius*(p2->vx);
            cy=(p2->y1)+circle_radius*(p2->vy);
            if((cx>t_x1)&&((cx)<t_x2)) 
              {
                if(((cy)>t_y1)&&((cy)<t_y2)) 
                  {
                    if(collision_id==1)
                      {
                        if(point_in_triangle(cx, cy, p1)||triangle_point_in_circle(p1, p2)) (p2->overlap)=TRUE;
                      }
                    else if(collision_id==2)
                      {
                        if(point_in_triangle2(cx, cy, p1)||triangle_point_in_circle(p1, p2)) (p2->overlap)=TRUE;
                      }
                    else (p2->overlap)=TRUE;
                  }
              }
            //Check trailing edge of circle along the velocity vector.
            cx=(p2->x1)-circle_radius*(p2->vx);
            cy=(p2->y1)-circle_radius*(p2->vy);
            if((cx>t_x1)&&((cx)<t_x2)) 
              {
                if(((cy)>t_y1)&&((cy)<t_y2)) 
                  {
                    if(collision_id==1)
                      {
                        if(point_in_triangle(cx, cy, p1)||triangle_point_in_circle(p1, p2)) (p2->overlap)=TRUE;
                      }
                    else if(collision_id==2)
                      {
                        if(point_in_triangle2(cx, cy, p1)||triangle_point_in_circle(p1, p2)) (p2->overlap)=TRUE;
                      }
                    else (p2->overlap)=TRUE;
                  }
              }                        
            p2++;
          }
        p1++;
      }
  }
/*
    Some extra checks
    http://blackpawn.com/texts/pointinpoly/default.html
*/
static gboolean point_in_triangle(gdouble rx, gdouble ry, struct triangle *t1)
  {
    if(same_side(rx, ry, t1->x1, t1->y1, t1->x2, t1->y2, t1->x3, t1->y3)&&same_side(rx, ry, t1->x2, t1->y2, t1->x1, t1->y1, t1->x3, t1->y3)&&same_side(rx, ry, t1->x3, t1->y3, t1->x1, t1->y1, t1->x2, t1->y2))
      {
        return TRUE;
      }
    else
      {
        return FALSE;
      }
  }
static gboolean same_side(gdouble pt1_x, gdouble pt1_y, gdouble pt2_x, gdouble pt2_y, gdouble ax, gdouble ay, gdouble bx, gdouble by)
  {
    bx-=ax;
    by-=ay;
    pt1_x-=ax;
    pt1_y-=ay;
    pt2_x-=ax;
    pt2_y-=ay;

    gdouble cp1=(bx*pt1_y)-(by*pt1_x);
    gdouble cp2=(bx*pt2_y)-(by*pt2_x);

    if(cp1*cp2>=0.0) return TRUE;
    else return FALSE;
  }
static gboolean triangle_point_in_rectangle(struct triangle *p1, struct rectangle *p2)
  {
    if((p1->x1)>(p2->x1)&&(p1->x1)<(p2->x3)&&(p1->y1)>(p2->y1)&&(p1->y1)<(p2->y3)) return TRUE;
    else if((p1->x2)>(p2->x1)&&(p1->x2)<(p2->x3)&&(p1->y2)>(p2->y1)&&(p1->y2)<(p2->y3)) return TRUE;
    else if((p1->x3)>(p2->x1)&&(p1->x3)<(p2->x3)&&(p1->y3)>(p2->y1)&&(p1->y3)<(p2->y3)) return TRUE;
    //check center point of triangle.
    else if((p1->x1)+tri_width/2.0>(p2->x1)&&(p1->x1)+tri_width/2.0<(p2->x3)&&(p1->y1)+tri_height/2.0>(p2->y1)&&(p1->y1)+tri_height/2.0<(p2->y3)) return TRUE;
    else return FALSE;
  }
static gboolean triangle_point_in_circle(struct triangle *p1, struct circle *p2)
  {
    if(((p1->x1)-(p2->x1))*((p1->x1)-(p2->x1))+((p1->y1)-(p2->y1))*((p1->y1)-(p2->y1))<circle_radius*circle_radius) return TRUE;
    else if(((p1->x2)-(p2->x1))*((p1->x2)-(p2->x1))+((p1->y2)-(p2->y1))*((p1->y2)-(p2->y1))<circle_radius*circle_radius) return TRUE;
    else if(((p1->x3)-(p2->x1))*((p1->x3)-(p2->x1))+((p1->y3)-(p2->y1))*((p1->y3)-(p2->y1))<circle_radius*circle_radius) return TRUE;
    else return FALSE;
  }
static gboolean point_in_triangle2(gdouble rx, gdouble ry, struct triangle *t1)
  {
    gdouble v0[2];
    gdouble v1[2];
    gdouble v2[2];
    v0[0]=(t1->x3)-(t1->x1);
    v0[1]=(t1->y3)-(t1->y1);
    v1[0]=(t1->x2)-(t1->x1);
    v1[1]=(t1->y2)-(t1->y1);
    v2[0]=rx-(t1->x1);
    v2[1]=ry-(t1->y1);

    gdouble dot00=v0[0]*v0[0]+v0[1]*v0[1];
    gdouble dot01=v0[0]*v1[0]+v0[1]*v1[1];
    gdouble dot02=v0[0]*v2[0]+v0[1]*v2[1];
    gdouble dot11=v1[0]*v1[0]+v1[1]*v1[1];
    gdouble dot12=v1[0]*v2[0]+v1[1]*v2[1];

    gdouble invDenom=1.0/(dot00*dot11-dot01*dot01);
    gdouble u=(dot11*dot02-dot01*dot12)*invDenom;
    gdouble v=(dot00*dot12-dot01*dot02)*invDenom;

    return(u>=0.0)&&(v>=0.0)&&(u+v<1.0);
  }
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
static void rotate_triangles(gdouble qrs[9])
  {
    gint i=0;
    //rotate the standard triangle.
    gdouble r1=(0.0*qrs[0])+(-tri_height/2.0*qrs[1]);
    gdouble r2=(0.0*qrs[3])+(-tri_height/2.0*qrs[4]);
    gdouble r3=(tri_width/2.0*qrs[0])+(tri_height/2.0*qrs[1]);
    gdouble r4=(tri_width/2.0*qrs[3])+(tri_height/2.0*qrs[4]);
    gdouble r5=(-tri_width/2.0*qrs[0])+(tri_height/2.0*qrs[1]);
    gdouble r6=(-tri_width/2.0*qrs[3])+(tri_height/2.0*qrs[4]);

    //Get difference of orginal values and rotated value.
    r1-=0.0;
    r2-=(-tri_height/2.0);
    r3-=(tri_width/2.0);
    r4-=(tri_height/2.0);
    r5-=(-tri_width/2.0);
    r6-=(tri_height/2.0);      

    struct triangle *p1=&g_array_index(triangles_t, struct triangle, 0);
    for(i=0;i<(triangles_t->len);i++)
      {       
        p1->x1+=r1;
        p1->y1+=r2;
        p1->x2+=r3;
        p1->y2+=r4;
        p1->x3+=r5;
        p1->y3+=r6;
        p1++;
      } 
  }
static void reset_triangles_t()
  {
    struct triangle *p1=&g_array_index(triangles, struct triangle, 0);
    struct triangle *p2=&g_array_index(triangles_t, struct triangle, 0);
    memcpy(p2, p1, sizeof(struct triangle)*(triangles->len));
  }  













