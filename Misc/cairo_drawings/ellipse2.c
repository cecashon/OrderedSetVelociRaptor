
/* 
    Test drawing an ellipse with transforming a circle, getting equal angle points and getting points
with the Steiner method. Draw more than one ellipse for comparison. Try some animation to get an idea 
of drawing times.
    Set up a gyro with pitch, roll and yaw along with a high tech stealth aircraft. Needs some rotational work.
     
    gcc -Wall ellipse2.c -o ellipse2 `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<stdlib.h>
#include<math.h>

struct point{
  gdouble x;
  gdouble y;
}points;

struct controls{
  gdouble x1;
  gdouble y1;
  gdouble x2;
  gdouble y2;
}controls;

//The start plane drawing.
const gdouble plane_coords[11][2]={  
      {0.0, -3.0},  
      {-3.0, 0.0},  
      {-1.0, 0.0}, 
      {-1.5, 2.0},
      {-0.5, 2.0},
      {0.0, 1.0},
      {0.5, 2.0},
      {1.5, 2.0},
      {1.0, 0.0},
      {3.0, 0.0},
      {0.0, -3.0},   
      };
//The rotated plane drawing.
static gdouble plane_rotated[11][2];

//Combo box values.
static gint method_id=0;
static gint center_id=0;
static gint line_id=0;

//Drawing values.
static gdouble line_width=10.0;
static gint points_per_quadrant=6;
//Mark a segment in the ellipse a different color.
static gint ellipse_mark1=5;

//If the drawing is animated.
static gint rotate=0;
//Tick id for animation frame clock.
static guint tick_id=0;

//Keep a rotation counter for each ellipse for animation.
static gdouble pitch=10.0;
static gdouble roll=10.0;
static gdouble yaw=10.0;

static void change_method(GtkComboBox *combo, gpointer data);
static void change_center(GtkComboBox *combo, gpointer data);
static void change_line(GtkComboBox *combo, gpointer data);
static void change_points(GtkComboBox *combo, gpointer data);
static void rotate_drawing(GtkComboBox *combo, gpointer data);
static void set_rotation_pitch(GtkWidget *button, gpointer *rotations);
static void set_rotation_roll(GtkWidget *button, gpointer *rotations);
static void set_rotation_yaw(GtkWidget *button, gpointer *rotations);
static void euler_rotation();
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data);
static gboolean draw_ellipses(GtkWidget *da, cairo_t *cr, gpointer data);
static void move_ellipse_pitch(cairo_t *cr, gdouble width, gdouble height, gdouble pos_x, gdouble pos_y, gdouble rotation_count);
static void move_ellipse_roll(cairo_t *cr, gdouble width, gdouble height, gdouble pos_x, gdouble pos_y, gdouble rotation_count);
static void move_ellipse_yaw(cairo_t *cr, gdouble width, gdouble height, gdouble pos_x, gdouble pos_y, gdouble rotation_count);
static void draw_ellipse_with_transform(cairo_t *cr, gdouble width, gdouble height, gdouble scale_x, gdouble scale_y, gdouble ac[], gdouble mc[]);
static void draw_ellipse(cairo_t *cr, gdouble width, gdouble height, gdouble scale_x, gdouble scale_y, gdouble ac[], gdouble mc[]);
static void draw_plane(cairo_t *cr, gdouble w1, gdouble h1);
static void get_elliptical_points(GArray *coords1, gdouble w1, gdouble h1, gdouble scale_x, gdouble scale_y);
static void get_steiner_points(GArray *coords1, gdouble w1, gdouble h1, gdouble scale_x, gdouble scale_y);
static GArray* control_points_from_coords2(const GArray *dataPoints);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Ellipses");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);
    g_signal_connect(da, "draw", G_CALLBACK(draw_ellipses), NULL);

    GtkWidget *combo_method=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_method, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_method), 0, "1", "Transform Circle");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_method), 1, "2", "Equal Angle Points");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_method), 2, "3", "Steiner Points");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_method), 0);
    g_signal_connect(combo_method, "changed", G_CALLBACK(change_method), da);

    GtkWidget *combo_center=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_center, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_center), 0, "1", "Center Ellipses");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_center), 1, "2", "Separate Ellipses");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_center), 0);
    g_signal_connect(combo_center, "changed", G_CALLBACK(change_center), da);

    GtkWidget *combo_line=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_line, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_line), 0, "1", "Draw Curves");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_line), 1, "2", "Draw Lines");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_line), 0);
    g_signal_connect(combo_line, "changed", G_CALLBACK(change_line), da);

    GtkWidget *combo_points=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_points, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_points), 0, "1", "24 Points");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_points), 1, "2", "32 Points");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_points), 2, "3", "40 Points");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_points), 0);
    g_signal_connect(combo_points, "changed", G_CALLBACK(change_points), da);

    GtkWidget *combo_rotate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_rotate, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 0, "1", "No Rotate");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 1, "2", "Animate Pitch, Roll and Yaw");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_rotate), 0);
    g_signal_connect(combo_rotate, "changed", G_CALLBACK(rotate_drawing), da);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *pitch_entry=gtk_entry_new();
    gtk_widget_set_hexpand(pitch_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(pitch_entry), "10");

    GtkWidget *pitch_button=gtk_button_new_with_label("Update Pitch");
    gtk_widget_set_hexpand(pitch_button, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *roll_entry=gtk_entry_new();
    gtk_widget_set_hexpand(roll_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(roll_entry), "10");

    GtkWidget *roll_button=gtk_button_new_with_label("Update Roll");
    gtk_widget_set_hexpand(roll_button, TRUE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);

    GtkWidget *yaw_entry=gtk_entry_new();
    gtk_widget_set_hexpand(yaw_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(yaw_entry), "10");

    GtkWidget *yaw_button=gtk_button_new_with_label("Update Yaw");
    gtk_widget_set_hexpand(yaw_button, TRUE);

    gpointer rotations[]={pitch_entry, roll_entry, yaw_entry, da};
    g_signal_connect(pitch_button, "clicked", G_CALLBACK(set_rotation_pitch), rotations);
    g_signal_connect(roll_button, "clicked", G_CALLBACK(set_rotation_roll), rotations);
    g_signal_connect(yaw_button, "clicked", G_CALLBACK(set_rotation_yaw), rotations);

    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_method, 0, 0, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_center, 0, 1, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_line, 0, 2, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_points, 0, 3, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_rotate, 0, 4, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_entry, 1, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_button, 0, 6, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 0, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_entry, 1, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_button, 0, 8, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 0, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_entry, 1, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_button, 0, 10, 2, 1);

    GtkWidget *scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll, TRUE);
    gtk_widget_set_vexpand(scroll, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll), grid1);
   
    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), scroll, TRUE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 300);
   
    gtk_container_add(GTK_CONTAINER(window), paned1);

    gtk_widget_show_all(window);

    gtk_main();

    return 0;  
  }
static void change_method(GtkComboBox *combo, gpointer data)
  {
    method_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void change_line(GtkComboBox *combo, gpointer data)
  {
    line_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void change_center(GtkComboBox *combo, gpointer data)
  {
    center_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void change_points(GtkComboBox *combo, gpointer data)
  {
    gint points_id=gtk_combo_box_get_active(combo);

    if(points_id==0)
      {
        points_per_quadrant=6;
        ellipse_mark1=5;
      }
    else if(points_id==1)
      {
        points_per_quadrant=8;
        ellipse_mark1=7;
      }
    else
      {
        points_per_quadrant=10;
        ellipse_mark1=9;
      }

    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_rotation_pitch(GtkWidget *button, gpointer *rotations)
  {
    g_print("Set Pitch\n");
    gdouble x=strtod(gtk_entry_get_text(GTK_ENTRY(rotations[0])), NULL);

    if(x>0)
      {
        pitch=x;
      }

    gtk_widget_queue_draw(GTK_WIDGET(rotations[3]));
  }
static void set_rotation_roll(GtkWidget *button, gpointer *rotations)
  {
    g_print("Set Roll\n");
    gdouble y=strtod(gtk_entry_get_text(GTK_ENTRY(rotations[1])), NULL);
   
    if(y>0)
      {
        roll=y;
      }

    gtk_widget_queue_draw(GTK_WIDGET(rotations[3]));
  }
static void set_rotation_yaw(GtkWidget *button, gpointer *rotations)
  {
    g_print("Set Yaw\n");
    gdouble z=strtod(gtk_entry_get_text(GTK_ENTRY(rotations[2])), NULL);
   
    if(z>0)
      {
        yaw=z;
      }

    gtk_widget_queue_draw(GTK_WIDGET(rotations[3]));
  }
static void euler_rotation()
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → rotation matrix

       This probably should be changed out for a standardized pitch, roll and yaw system. Good
       for seeing if I can get something to work.
    */
    gint i=0;
    gdouble x=0;
    gdouble y=0;
    gdouble pitch_r=(pitch*G_PI/180.0);
    gdouble roll_r=(roll*G_PI/180.0);
    gdouble yaw_r=(yaw*G_PI/180.0);

    //Just need x and y from 3d rotation. It is a plane plane so z=0.
    for(i=0;i<11;i++)
      {
        x=(plane_coords[i][0]*cos(roll_r)*cos(yaw_r)) - (plane_coords[i][1]*cos(pitch_r)*sin(yaw_r)+plane_coords[i][1]*sin(pitch_r)*sin(roll_r)*cos(yaw_r));
        y=(plane_coords[i][0]*cos(roll_r)*sin(yaw_r)) + (plane_coords[i][1]*cos(pitch_r)*cos(yaw_r)+plane_coords[i][1]*sin(pitch_r)*sin(roll_r)*sin(yaw_r));
        plane_rotated[i][0]=x;
        plane_rotated[i][1]=y;
        //g_print("x %f y %f rx %f ry %f\n", plane_coords[i][0], plane_coords[i][1], plane_rotated[i][0], plane_rotated[i][1]); 
      }

  }
static void rotate_drawing(GtkComboBox *combo, gpointer data)
  {
    rotate=gtk_combo_box_get_active(combo);
    //Reset rotations for animation.
    pitch=10.0;
    roll=10.0;
    yaw=10.0;

    if(rotate==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(data), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(data));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(data), (GtkTickCallback)animate_drawing, NULL, NULL);
          }
      }
    
  }
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data)
  {
    //Apply different rates of rotation for animation.    
    pitch+=0.5;
    roll+=0.5;
    //yaw+=0.5;

    gtk_widget_queue_draw(drawing);
    return G_SOURCE_CONTINUE;
  }
//The top drawing function.
static gboolean draw_ellipses(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    GTimer *timer=g_timer_new();
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;
    //Ellipse color different for each ellipse.
    gdouble ac[4]={0.0, 0.0, 0.0, 1.0};
    //The mark color for the ellipse if relevant.
    gdouble mc[4]={0.0, 1.0, 1.0, 1.0};

    //Paint background.
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_paint(cr);

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 1.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr); 

    //Get the current rotations for the plane.
    euler_rotation();

    cairo_set_line_width(cr, line_width);
    if(method_id>0)
      {
        cairo_save(cr);
        ac[0]=1.0; ac[1]=0.0; ac[2]=0.0; ac[3]=1.0;
        if(center_id==0) move_ellipse_pitch(cr, width, height, 2.0, 2.0, pitch);
        else
          {
            cairo_scale(cr, 0.5, 0.5);
            move_ellipse_pitch(cr, width, height, 5.0/3.0, 5.0/3.0, pitch);
          }
        draw_ellipse(cr, width, height, 1.0, 1.0, ac, mc);
        cairo_restore(cr);
        cairo_save(cr);
        ac[0]=0.0; ac[1]=1.0; ac[2]=0.0; ac[3]=1.0;
        if(center_id==0) move_ellipse_roll(cr, width, height, 2.0, 2.0, roll);
        else
          {
            cairo_scale(cr, 0.5, 0.5);
            move_ellipse_roll(cr, width, height, 5.0/7.0, 5.0/3.0, roll);
          }
        draw_ellipse(cr, width, height, 0.95, 0.95, ac, mc);
        cairo_restore(cr);
        cairo_save(cr);
        ac[0]=0.0; ac[1]=0.0; ac[2]=1.0; ac[3]=1.0;
        if(center_id==0) move_ellipse_yaw(cr, width, height, 2.0, 2.0, yaw);
        else
          {
            cairo_scale(cr, 0.5, 0.5);
            move_ellipse_yaw(cr, width, height, 5.0/3.0, 5.0/7.0, yaw);
          }
        draw_ellipse(cr, width, height, 0.90, 0.90, ac, mc);
        cairo_restore(cr);
        cairo_save(cr);
        ac[0]=1.0; ac[1]=0.0; ac[2]=1.0; ac[3]=1.0;
        if(center_id==0) cairo_translate(cr, width/2.0, height/2.0);
        else
          {
            cairo_scale(cr, 0.5, 0.5);
            cairo_translate(cr, 2.0*5.0*width/7.0, 2.0*5.0*height/7.0);
          }
        cairo_set_source_rgba(cr, 0.8, 0.0, 0.8, 0.8);
        cairo_set_line_width(cr, 4.0);
        draw_plane(cr, w1, h1);
        cairo_restore(cr);
      }
    else
      {
        cairo_save(cr);
        ac[0]=1.0; ac[1]=0.0; ac[2]=0.0; ac[3]=1.0;
        if(center_id==0) move_ellipse_pitch(cr, width, height, 2.0, 2.0, pitch);
        else
          {
            cairo_scale(cr, 0.5, 0.5);
            move_ellipse_pitch(cr, width, height, 5.0/3.0, 5.0/3.0, pitch);
          }
        draw_ellipse_with_transform(cr, width, height, 1.0, 1.0, ac, mc);
        cairo_restore(cr);
        cairo_save(cr);
        ac[0]=0.0; ac[1]=1.0; ac[2]=0.0; ac[3]=1.0;
        if(center_id==0) move_ellipse_roll(cr, width, height, 2.0, 2.0, roll);
        else
          {
            cairo_scale(cr, 0.5, 0.5);
            move_ellipse_roll(cr, width, height, 5.0/7.0, 5.0/3.0, roll);
          }
        draw_ellipse_with_transform(cr, width, height, 0.95, 0.95, ac, mc);
        cairo_restore(cr);
        cairo_save(cr);
        ac[0]=0.0; ac[1]=0.0; ac[2]=1.0; ac[3]=1.0;
        if(center_id==0) move_ellipse_yaw(cr, width, height, 2.0, 2.0, yaw);
        else
          {
            cairo_scale(cr, 0.5, 0.5);
            move_ellipse_yaw(cr, width, height, 5.0/3.0, 5.0/7.0, yaw);
          }
        draw_ellipse_with_transform(cr, width, height, 0.90, 0.90, ac, mc);
        cairo_restore(cr);
        cairo_save(cr);
        if(center_id==0) cairo_translate(cr, width/2.0, height/2.0);
        else
          {
            cairo_scale(cr, 0.5, 0.5);
            cairo_translate(cr, 2.0*5.0*width/7.0, 2.0*5.0*height/7.0);
          }
        cairo_set_source_rgba(cr, 0.8, 0.0, 0.8, 0.8);
        cairo_set_line_width(cr, 4.0);
        draw_plane(cr, w1, h1);
        cairo_restore(cr);
      }
    
    //g_print("Time %f Pitch %i Roll %i Yaw %i\n", g_timer_elapsed(timer, NULL), (gint)pitch%360, (gint)roll%360, (gint)yaw%360);
    g_timer_destroy(timer);
    return FALSE;
  }
//Change the drawing translation for a still or animated drawing.
static void move_ellipse_pitch(cairo_t *cr, gdouble width, gdouble height, gdouble pos_x, gdouble pos_y, gdouble rotation_count)
  {
    gdouble angle=0;
    gdouble scale=0;
    gdouble scale_inv=0;

    angle=-rotation_count*G_PI/180.0+G_PI/2.0;
    scale=cos(angle);
    scale_inv=1.0/scale;
    cairo_scale(cr, scale, 1.0);
    cairo_translate(cr, scale_inv*width/pos_x, height/pos_y);    
  } 
static void move_ellipse_roll(cairo_t *cr, gdouble width, gdouble height, gdouble pos_x, gdouble pos_y, gdouble rotation_count)
  {
    gdouble angle=0;
    gdouble scale=0;
    gdouble scale_inv=0;
      
    angle=-rotation_count*G_PI/180.0;
    scale=cos(angle);
    scale_inv=1.0/scale;
    cairo_scale(cr, scale, 1.0);
    cairo_translate(cr, scale_inv*width/pos_x, height/pos_y);
    cairo_rotate(cr, -angle);
  }
static void move_ellipse_yaw(cairo_t *cr, gdouble width, gdouble height, gdouble pos_x, gdouble pos_y, gdouble rotation_count)
  {
    gdouble angle=0;
    gdouble scale=0;
    gdouble scale_inv=0;
      
    angle=-rotation_count*G_PI/180.0;
    scale=sin(angle);
    scale_inv=1.0/scale;
    cairo_scale(cr, 1.0, scale);
    cairo_translate(cr, width/pos_x, scale_inv*height/pos_y);
    cairo_rotate(cr, -angle);
  }
static void draw_ellipse_with_transform(cairo_t *cr, gdouble width, gdouble height, gdouble scale_x, gdouble scale_y, gdouble ac[], gdouble mc[])
  {
    cairo_set_source_rgba(cr, ac[0], ac[1], ac[2], ac[3]);

    if(line_id==0)
      {
        cairo_save(cr);
        cairo_scale(cr, width/400.0*scale_x, height/400.0*scale_y);
        cairo_arc(cr, 0.0, 0.0, 160.0, 0.0, 2.0*G_PI);
        cairo_restore(cr);
        cairo_stroke(cr);
      }

    gint i=0;
    gdouble hour_radius=160.0;
    gdouble hour_start=0.0;
    gdouble next_hour=G_PI/((gdouble)points_per_quadrant*2.0);
    gint points=4*points_per_quadrant+1;
    gdouble temp_cos=0.0;
    gdouble temp_sin=0.0;
    if(line_id==1)
      {
        temp_cos=cos(hour_start);
        temp_sin=sin(hour_start);
        for(i=1;i<points;i++)
          {
            if((i==ellipse_mark1+1)) cairo_set_source_rgba(cr, mc[0], mc[1], mc[2], mc[3]);
            else cairo_set_source_rgba(cr, ac[0], ac[1], ac[2], ac[3]);
            cairo_save(cr);
            cairo_scale(cr, width/400.0*scale_x, height/400.0*scale_y);
            cairo_move_to(cr, temp_cos*hour_radius, temp_sin*hour_radius);
            temp_cos=cos(hour_start-(next_hour*i));
            temp_sin=sin(hour_start-(next_hour*i));
            cairo_line_to(cr, temp_cos*hour_radius, temp_sin*hour_radius);
            cairo_restore(cr);
            cairo_stroke(cr);
          }
      }

    points--;
    cairo_scale(cr, width/400.0*scale_x, height/400.0*scale_y);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_set_line_width(cr, 5.0);
    for(i=0;i<48;i++)
      {
        temp_cos=cos(hour_start-(next_hour*i));
        temp_sin=sin(hour_start-(next_hour*i));
        cairo_move_to(cr, temp_cos*hour_radius, temp_sin*hour_radius);
        cairo_line_to(cr, temp_cos*hour_radius, temp_sin*hour_radius);
        cairo_stroke(cr);
      }
  }
static void draw_ellipse(cairo_t *cr, gdouble width, gdouble height, gdouble scale_x, gdouble scale_y, gdouble ac[], gdouble mc[])
  {
    gint i=0;
    gdouble w1=8.0*width/10.0;
    gdouble h1=8.0*height/10.0;

    GArray *coords1=g_array_new(FALSE, FALSE, sizeof(struct point));
    if(method_id==1) get_elliptical_points(coords1, w1, h1, scale_x, scale_y); 
    else get_steiner_points(coords1, w1, h1, scale_x, scale_y); 
  
    //Get the control points.
    GArray *control1=control_points_from_coords2(coords1);

    //Draw the curves.
    cairo_set_source_rgba(cr, ac[0], ac[1], ac[2], ac[3]);
    struct point d1;
    struct point d2;
    struct controls c1; 
    gint length=coords1->len-1;
    for(i=0;i<length;i++)
      {
        d1=g_array_index(coords1, struct point, i);
        d2=g_array_index(coords1, struct point, i+1);
        c1=g_array_index(control1, struct controls, i);
        if(i==ellipse_mark1) cairo_set_source_rgba(cr, mc[0], mc[1], mc[2], mc[3]);
        else cairo_set_source_rgba(cr, ac[0], ac[1], ac[2], ac[3]);
        cairo_move_to(cr, d1.x, d1.y);
        if(line_id==0) cairo_curve_to(cr, c1.x1, c1.y1, c1.x2, c1.y2, d2.x, d2.y);
        else cairo_line_to(cr, d2.x, d2.y);
        cairo_stroke(cr);
      }

    //Draw the points.
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_set_line_width(cr, 3.0);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
    length=coords1->len;
    for(i=0;i<length;i++)
      {
        d1=g_array_index(coords1, struct point, i);
        cairo_move_to(cr, d1.x, d1.y);
        cairo_line_to(cr, d1.x, d1.y);
        cairo_stroke(cr);
      }   
   
    g_array_free(coords1, TRUE);   
    g_array_free(control1, TRUE);  
  }
static void draw_plane(cairo_t *cr, gdouble w1, gdouble h1)
  {
    cairo_move_to(cr, plane_rotated[0][0]*w1, plane_rotated[0][1]*h1);
    cairo_line_to(cr, plane_rotated[1][0]*w1, plane_rotated[1][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_rotated[2][0]*w1, plane_rotated[2][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_rotated[3][0]*w1, plane_rotated[3][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_rotated[4][0]*w1, plane_rotated[4][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_rotated[5][0]*w1, plane_rotated[5][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_rotated[6][0]*w1, plane_rotated[6][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_rotated[7][0]*w1, plane_rotated[7][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_rotated[8][0]*w1, plane_rotated[8][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_rotated[9][0]*w1, plane_rotated[9][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_line_to(cr, plane_rotated[10][0]*w1, plane_rotated[10][1]*h1);
    cairo_stroke_preserve(cr);
    cairo_fill_preserve(cr);
    cairo_stroke(cr);
  }
/*
  Points are evenly dispersed by angle but not along the arc.
*/
static void get_elliptical_points(GArray *coords1, gdouble w1, gdouble h1, gdouble scale_x, gdouble scale_y)
  {
    gint i=0;
    struct point p1;
    //Scale the points.
    w1=w1*scale_x/2.0;
    h1=h1*scale_y/2.0;
    gdouble hour_radius=w1;
    gdouble hour_start=0.0;
    gdouble next_hour=G_PI/((gdouble)points_per_quadrant*2.0);
    gint points=4*points_per_quadrant;
    gdouble temp_cos=0.0;
    gdouble temp_sin=0.0;
    for(i=0;i<points;i++)
      {        
        temp_cos=cos(hour_start-(next_hour*i));
        temp_sin=sin(hour_start-(next_hour*i));
        hour_radius=(w1*h1)/sqrt((w1*w1*temp_sin*temp_sin) + (h1*h1*temp_cos*temp_cos));
        p1.x=temp_cos*hour_radius;
        p1.y=temp_sin*hour_radius;
        g_array_append_val(coords1, p1);          
      }
    //Add the last point.
    p1.x=w1;
    p1.y=0.0;
    g_array_append_val(coords1, p1);
  }
/*
  This evens out the points along the arc and draws a better ellipse.
*/
static void get_steiner_points(GArray *coords1, gdouble w1, gdouble h1, gdouble scale_x, gdouble scale_y)
  {
    gint i=0;
    gint j=0;
    struct point p1;
    //Scale the points.
    w1=w1*scale_x;
    h1=h1*scale_y*2.0;
    gint points1=points_per_quadrant;
    gint points2=2*points1;
    gdouble x1=w1;
    gdouble y1=0.5*h1;
    gdouble x2=w1;
    gdouble y2=0.0;
    gdouble x3=0.0;
    gdouble y3=0.5*h1;
    gdouble x4=w1;
    gdouble y4=0.5*h1;
    gdouble m1=0.0;
    gdouble m2=0.0;
    gdouble b1=0.0;
    gdouble b2=0.0;

    //Add the first point.
    p1.x=w1*0.5;
    p1.y=0.0;
    g_array_append_val(coords1, p1);
    for(i=0;i<4;i++)
      {
        if(i==2) y4=h1;
        for(j=0;j<points1;j++)
          {
            if(i==0)
              {
                x2=x2-w1/points1;
                y4=y4-h1/points2;
              }
            else if(i==1)
              {
                x4=x4-w1/points1;
                y2=y2+h1/points2;
              }
            else if(i==2)
              {
                x4=x4+w1/points1;
                y2=y2+h1/points2;
              }
            else
              {
                x2=x2+w1/points1;
                y4=y4-h1/points2;
              }
            m1=(y2-y1)/(x2-x1);
            m2=(y4-y3)/(x4-x3);
            b1=y2-m1*x2;
            b2=y4-m2*x4;
            //Divide by 0 problems at ends of ellipse.
            if(i==3&&j==points1-1)
              {
                p1.x=w1*0.5;
                p1.y=0.0;
              }
            else if(i==1&&j==points1-1)
              {
                p1.x=-w1*0.5;
                p1.y=0.0;
              }
            else
              {
                p1.x=(b2-b1)/(m1-m2)-w1*0.5;
                p1.y=(m1*b2-m2*b1)/(m1-m2)-h1*0.5;
              }
            //g_print("x %f y %f\n", p1.x, p1.y);
            g_array_append_val(coords1, p1);
          } 
      }
  }
/*
    This is some exellent work done by Ramsundar Shandilya. Note the following for the original work
    and the rational behind it.
    
    https://medium.com/@ramshandilya/draw-smooth-curves-through-a-set-of-points-in-ios-34f6d73c8f9

    https://github.com/Ramshandilya/Bezier

    The MIT License (MIT)

    Copyright (c) 2015 Ramsundar Shandilya

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
  
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

    This is a translation of the original Swift code to C. It makes the function easy to use with GTK+,
    cairo and glib.
*/
static GArray* control_points_from_coords2(const GArray *dataPoints)
  {  
    gint i=0;
    GArray *controlPoints=NULL;      
    //Number of Segments
    gint count=0;
    if(dataPoints!=NULL) count=dataPoints->len-1;
    gdouble *fCP=NULL;
    gdouble *sCP=NULL;

    if(count>0)
      {
        fCP=(gdouble*)g_malloc(2*count*sizeof(gdouble));
        sCP=(gdouble*)g_malloc(2*count*sizeof(gdouble));
      }
        
    //P0, P1, P2, P3 are the points for each segment, where P0 & P3 are the knots and P1, P2 are the control points.
    if(count<1||dataPoints==NULL)
      {
        //Return NULL.
        controlPoints=NULL;
        g_print("Can't get control points from coordinates. NULL returned.\n");
      }
    else if(count==1)
      {
        struct point P0=g_array_index(dataPoints, struct point, 0);
        struct point P3=g_array_index(dataPoints, struct point, 1);

        //Calculate First Control Point
        //3P1 = 2P0 + P3
        struct point P1;
        P1.x=(2.0*P0.x+P3.x)/3.0;
        P1.y=(2.0*P0.y+P3.y)/3.0;

        *(fCP)=P1.x;
        *(fCP+1)=P1.y;

        //Calculate second Control Point
        //P2 = 2P1 - P0
        struct point P2;
        P2.x=(2.0*P1.x-P0.x);
        P2.y=(2.0*P1.y-P0.x);

        *(sCP)=P2.x;
        *(sCP+1)=P2.y;      
      }
    else
      {
        gdouble *rhs=(gdouble*)g_malloc(2*count*sizeof(gdouble));
        gdouble *a=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *b=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *c=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble rhsValueX=0;
        gdouble rhsValueY=0;
        struct point P0;
        struct point P3;        
        gdouble m=0;
        gdouble b1=0;
        gdouble r2x=0;
        gdouble r2y=0;
        gdouble P1_x=0;
        gdouble P1_y=0;
   
        for(i=0;i<count;i++)
          {
            P0=g_array_index(dataPoints, struct point, i);
            P3=g_array_index(dataPoints, struct point, i+1);

            if(i==0)
              {
                *(a)=0.0;
                *(b)=2.0;
                *(c)=1.0;

                //rhs for first segment
                rhsValueX=P0.x+2.0*P3.x;
                rhsValueY=P0.y+2.0*P3.y;
              }
            else if(i==count-1)
              {
                *(a+i)=2.0;
                *(b+i)=7.0;
                *(c+i)=0.0;

                //rhs for last segment
                rhsValueX=8.0*P0.x+P3.x;
                rhsValueY=8.0*P0.y+P3.y;
              }
            else
              {
                *(a+i)=1.0;
                *(b+i)=4.0;
                *(c+i)=1.0;

                rhsValueX=4.0*P0.x+2.0*P3.x;
                rhsValueY=4.0*P0.y+2.0*P3.y;
              }
            *(rhs+i*2)=rhsValueX;
            *(rhs+i*2+1)=rhsValueY;
          }

        //Solve Ax=B. Use Tridiagonal matrix algorithm a.k.a Thomas Algorithm
        for(i=1;i<count;i++)
          {
            m=(*(a+i))/(*(b+i-1));

            b1=(*(b+i))-m*(*(c+i-1));
            *(b+i)=b1;

            r2x=(*(rhs+i*2))-m*(*(rhs+(i-1)*2));
            r2y=(*(rhs+i*2+1))-m*(*(rhs+(i-1)*2+1));

            *(rhs+i*2)=r2x;
            *(rhs+i*2+1)=r2y;
          }

        //Get First Control Points
        
        //Last control Point
        gdouble lastControlPointX=(*(rhs+2*count-2))/(*(b+count-1));
        gdouble lastControlPointY=(*(rhs+2*count-1))/(*(b+count-1));

        *(fCP+2*count-2)=lastControlPointX;
        *(fCP+2*count-1)=lastControlPointY;

        gdouble controlPointX=0;
        gdouble controlPointY=0;

        for(i=count-2;i>=0;--i)
          {
            controlPointX=(*(rhs+i*2)-(*(c+i))*(*(fCP+(i+1)*2)))/(*(b+i));
            controlPointY=(*(rhs+i*2+1)-(*(c+i))*(*(fCP+(i+1)*2+1)))/(*(b+i));

             *(fCP+i*2)=controlPointX;
             *(fCP+i*2+1)=controlPointY; 
          }

        //Compute second Control Points from first.
        for(i=0;i<count;i++)
          {
            if(i==count-1)
              {
                P3=g_array_index(dataPoints, struct point, i+1);
                P1_x=(*(fCP+i*2));
                P1_y=(*(fCP+i*2+1));

                controlPointX=(P3.x+P1_x)/2.0;
                controlPointY=(P3.y+P1_y)/2.0;

                *(sCP+count*2-2)=controlPointX;
                *(sCP+count*2-1)=controlPointY;
              }
            else
              {
                P3=g_array_index(dataPoints, struct point, i+1);                
                P1_x=(*(fCP+(i+1)*2));
                P1_y=(*(fCP+(i+1)*2+1));

                controlPointX=2.0*P3.x-P1_x;
                controlPointY=2.0*P3.y-P1_y;

                *(sCP+i*2)=controlPointX;
                *(sCP+i*2+1)=controlPointY;
              }

          }

        controlPoints=g_array_new(FALSE, FALSE, sizeof(struct controls));
        struct controls cp;
        for(i=0;i<count;i++)
          {
            cp.x1=(*(fCP+i*2));
            cp.y1=(*(fCP+i*2+1));
            cp.x2=(*(sCP+i*2));
            cp.y2=(*(sCP+i*2+1));
            g_array_append_val(controlPoints, cp);
          }

        g_free(rhs);
        g_free(a);
        g_free(b);
        g_free(c);
     }

    if(fCP!=NULL) g_free(fCP);
    if(sCP!=NULL) g_free(sCP);

    return controlPoints;
  }



