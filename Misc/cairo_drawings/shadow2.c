
/* 
    Draw a shadow of a star. Cast the shadow onto a plane. Rotate the star and the plane.

    This one uses the Graphene library.
        https://developer.gnome.org/graphene/stable/ch01.html
        https://github.com/ebassi/graphene

    gcc -Wall shadow2.c -o shadow2 `pkg-config --cflags --libs gtk+-3.0` -lm -Wl,-rpath=/home/eric/Graphene/graphene-master/build/src libgraphene-1.0.so

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<stdlib.h>
#include<string.h>
#include"graphene.h"

//A star and transformed star.
static GArray *star=NULL;
static GArray *star_t=NULL;
//A bezier star and transformed bezier star.
static GArray *b_star=NULL;
static GArray *b_star_t=NULL;

//A plane to project the star onto and a transformed plane for revolving the plane.
gfloat plane_pts[9]={0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.5f, 1.0f, -1.0f};
gfloat plane_pts_t[9];

static gint shape_id=0;

//The combo rotation slider values.
static gdouble combo_pitch=0.0;
static gdouble combo_roll=0.0;
static gdouble combo_yaw=0.0;
static gdouble combo_shadow=0.0;
static gdouble combo_pitch2=0.0;
static gdouble combo_roll2=0.0;
static gdouble combo_yaw2=0.0;
static gdouble combo_shadow2=0.0;

//Tick id for animation frame clock.
static guint tick_id=0;

//For the performance check boxes.
static gboolean frame_rate=FALSE;
static gboolean function_time=FALSE;

//Initialize a star to test.
static void initialize_star();
static void initialize_b_star();
//Draw the main window transparent.
static gboolean draw_main_window(GtkWidget *window, cairo_t *cr, gpointer data);
//UI functions for sliders and combo boxes.
static void show_frame_rate(GtkToggleButton *button, gpointer data);
static void show_function_time(GtkToggleButton *button, gpointer data);
static void rotate_drawing(GtkComboBox *combo, gpointer data);
static void set_shape_id(GtkComboBox *combo, gpointer data);
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_shadow(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_pitch2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_shadow2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data);
//The start of drawing. Get the combo_id's to draw.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer data);
static void reset_draw_star();
static void reset_draw_b_star();
static void reset_plane_pts();
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9]);
static void rotate_shape(gdouble qrs[9]);
static void rotate_plane(gdouble qrs[9]);
//After the shape points are transformed, draw it.
static void draw_star(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_b_star(cairo_t *cr, gdouble w1, gdouble h1);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Shadow2");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 500);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    gtk_widget_set_app_paintable(window, TRUE);
    //Try to set transparency of main window.
    if(gtk_widget_is_composited(window))
      {
        GdkScreen *screen=gtk_widget_get_screen(window);  
        GdkVisual *visual=gdk_screen_get_rgba_visual(screen);
        gtk_widget_set_visual(window, visual);
      }
    else g_print("Can't set window transparency.\n");

    initialize_star();
    initialize_b_star();

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);

    GtkWidget *combo_rotate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_rotate, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 0, "1", "Draw Shape");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 1, "2", "Animate Shape");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_rotate), 0);

    GtkWidget *combo_shape=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_shape, TRUE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_shape), 0, "1", "Star");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_shape), 1, "2", "Bezier Star");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_shape), 0);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);

    GtkWidget *shadow_label=gtk_label_new("Revolve");
    gtk_widget_set_hexpand(shadow_label, TRUE);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider, TRUE);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider, TRUE);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider, TRUE);

    GtkWidget *shadow_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 100.0, 1.0);
    gtk_widget_set_vexpand(shadow_slider, TRUE);

    g_signal_connect(da, "draw", G_CALLBACK(draw_main), NULL);
    g_signal_connect(combo_rotate, "changed", G_CALLBACK(rotate_drawing), da);
    g_signal_connect(combo_shape, "changed", G_CALLBACK(set_shape_id), da);
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_pitch), da);
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_roll), da);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_yaw), da);
    g_signal_connect(shadow_slider, "change-value", G_CALLBACK(set_shadow), da);

    //Some checks for performance.
    GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
    gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
    g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), NULL);

    GtkWidget *check_functions=gtk_check_button_new_with_label("Show Function Time");
    gtk_widget_set_halign(check_functions, GTK_ALIGN_CENTER);
    g_signal_connect(check_functions, "toggled", G_CALLBACK(show_function_time), NULL);

    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_rotate, 0, 0, 6, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_shape, 0, 1, 6, 1);    
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 2, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid1), shadow_label, 3, 2, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), shadow_slider, 3, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_frame, 0, 6, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_functions, 0, 5, 6, 1);

    GtkWidget *pitch_label2=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label2, TRUE);

    GtkWidget *roll_label2=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label2, TRUE);

    GtkWidget *yaw_label2=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label2, TRUE);

    GtkWidget *shadow_label2=gtk_label_new("Distance");
    gtk_widget_set_hexpand(shadow_label2, TRUE);

    GtkWidget *pitch_slider2=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider2, TRUE);

    GtkWidget *roll_slider2=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider2, TRUE);

    GtkWidget *yaw_slider2=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider2, TRUE);

    GtkWidget *shadow_slider2=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 100.0, 1.0);
    gtk_widget_set_vexpand(shadow_slider2, TRUE);

    g_signal_connect(pitch_slider2, "change-value", G_CALLBACK(set_pitch2), da);
    g_signal_connect(roll_slider2, "change-value", G_CALLBACK(set_roll2), da);
    g_signal_connect(yaw_slider2, "change-value", G_CALLBACK(set_yaw2), da);
    g_signal_connect(shadow_slider2, "change-value", G_CALLBACK(set_shadow2), da);

    GtkWidget *grid2=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid2), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid2), 8); 
    gtk_grid_attach(GTK_GRID(grid2), pitch_label2, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), roll_label2, 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), yaw_label2, 2, 0, 1, 1); 
    //gtk_grid_attach(GTK_GRID(grid2), shadow_label2, 3, 0, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid2), pitch_slider2, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), roll_slider2, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), yaw_slider2, 2, 1, 1, 1);
    //gtk_grid_attach(GTK_GRID(grid2), shadow_slider2, 3, 1, 1, 1);

    GtkWidget *scroll1=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll1, TRUE);
    gtk_widget_set_vexpand(scroll1, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll1), grid1);

    GtkWidget *scroll2=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll2, TRUE);
    gtk_widget_set_vexpand(scroll2, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll2), grid2);

    GtkWidget *nb_label1=gtk_label_new("Star");
    GtkWidget *nb_label2=gtk_label_new("Shadow Plane");   
    GtkWidget *notebook=gtk_notebook_new();
    //gtk_container_set_border_width(GTK_CONTAINER(notebook), 15);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll1, nb_label1);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll2, nb_label2);
   
    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), notebook, TRUE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 250);

    g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), paned1);
   
    gtk_container_add(GTK_CONTAINER(window), paned1);
    
    gtk_widget_show_all(window);

    gtk_main();

    g_array_free(star, TRUE);
    g_array_free(star_t, TRUE);
    g_array_free(b_star, TRUE);
    g_array_free(b_star_t, TRUE);

    return 0;  
  }
static void initialize_star()
  {
    gint i=0;

    star=g_array_sized_new(FALSE, TRUE, sizeof(graphene_point3d_t), 10);
    g_array_set_size(star, 10);
    star_t=g_array_sized_new(FALSE, TRUE, sizeof(graphene_point3d_t), 10);
    g_array_set_size(star_t, 10);

    graphene_point3d_t *p1=&g_array_index(star, graphene_point3d_t, 0);
    
    gfloat radius=1.0f; 
    for(i=0;i<10;i++)
      {       
        (*p1).x=radius*(gfloat)(cos((gdouble)i*2.0*G_PI/10.0));
        (*p1).y=radius*(gfloat)(sin((gdouble)i*2.0*G_PI/10.0));
        (*p1).z=0.0f;
        if(radius>0.8f) radius=0.5f;
        else radius=1.0f; 
        p1++;         
      }
  }
static void initialize_b_star()
  {
    gint i=0;

    b_star=g_array_sized_new(FALSE, TRUE, sizeof(graphene_point3d_t), 15);
    g_array_set_size(b_star, 15);
    b_star_t=g_array_sized_new(FALSE, TRUE, sizeof(graphene_point3d_t), 15);
    g_array_set_size(b_star_t, 15);

    graphene_point3d_t *p1=&g_array_index(b_star, graphene_point3d_t, 0);
    
    gdouble radius=1.0; 
    for(i=0;i<15;i++)
      {  
        if(i%3==0||i==0) radius=1.0;
        else radius=0.5;      
        (*p1).x=radius*cos((gdouble)i*2.0*G_PI/15.0);
        (*p1).y=radius*sin((gdouble)i*2.0*G_PI/15.0);
        (*p1).z=0.0;
        p1++;         
      }
  }
static gboolean draw_main_window(GtkWidget *window, cairo_t *cr, gpointer data)
  {
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.3);
    cairo_paint(cr);

    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
    gint width=gtk_paned_get_position(GTK_PANED(data));
    gint height=gtk_widget_get_allocated_height(window);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_rectangle(cr, width, 0.0, 10, height);
    cairo_fill(cr);

    return FALSE;
  }
static void show_frame_rate(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_function_time(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) function_time=TRUE;
    else function_time=FALSE;
  }
static void rotate_drawing(GtkComboBox *combo, gpointer data)
  {   
    combo_pitch=0.0;
    combo_roll=0.0;
    combo_yaw=0.0;
    combo_shadow=0.0;
 
    if(gtk_combo_box_get_active(combo)==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(data), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(data));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(data), (GtkTickCallback)animate_drawing, NULL, NULL);
          }
      }
    
  }
static void set_shape_id(GtkComboBox *combo, gpointer data)
  {  
    shape_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_pitch=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_roll=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_yaw=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }

static void set_shadow(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>100.0) combo_shadow=100.0;
    else if(value<0.0) combo_shadow=0.0;
    else combo_shadow=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_pitch2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_pitch2=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_roll2=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_yaw2=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_shadow2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>100.0) combo_shadow2=100.0;
    else if(value<0.0) combo_shadow2=0.0;
    else combo_shadow2=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer data)
  {
    static gboolean up=TRUE;

    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    if(combo_pitch>360.0) combo_pitch=0.0;
    else combo_pitch+=0.5;
    if(combo_roll>360.0) combo_roll=0.0;
    else combo_roll+=0.5;
    if(combo_yaw>360.0) combo_yaw=0.0;
    else combo_yaw+=0.5;

    if(combo_shadow>100.0) up=FALSE;
    else if(combo_shadow<0.0) up=TRUE; 
    if(up) combo_shadow+=0.5;
    else combo_shadow-=0.5;

    gtk_widget_queue_draw(GTK_WIDGET(drawing));
    return G_SOURCE_CONTINUE;
  }
//The top drawing function.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer data)
  {
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;

    //Paint background transparent.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.0);
    cairo_paint(cr);

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 3.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Draw in the center
    cairo_translate(cr, width/2.0, height/2.0);
    //Sometimes there are small extra line fragments if CAIRO_LINE_JOIN_MITER is used.
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND); 
    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    gdouble pitch=combo_pitch*G_PI/180.0;
    gdouble roll=combo_roll*G_PI/180.0;
    gdouble yaw=combo_yaw*G_PI/180.0;
    gdouble qrs[9];     
 
    //Time the transforms.
    GTimer *timer1=g_timer_new();
    //Reset to initial values.
    if(shape_id==0) reset_draw_star();
    else reset_draw_b_star();
    reset_plane_pts();
    //Get a rotation matrix.
    quaternion_rotation(yaw, roll, pitch, qrs);                                           
    rotate_shape(qrs);
    yaw=combo_shadow*2.0*G_PI/100.0;
    quaternion_rotation(yaw, 0.0, 0.0, qrs);
    rotate_plane(qrs);
    if(function_time) g_print("Transform time %f\n", g_timer_elapsed(timer1, NULL));

    //Time the drawings.
    g_timer_start(timer1);
    if(shape_id==0) draw_star(cr, w1, h1);
    else draw_b_star(cr, w1, h1);
    if(function_time) g_print("     Draw time %f\n", g_timer_elapsed(timer1, NULL));    
    g_timer_destroy(timer1);
     
    return FALSE;
  }
static void reset_draw_star()
  {
    //Start with a new copy of the original cube, lines and reference vector.
    graphene_point3d_t *p1=&g_array_index(star, graphene_point3d_t, 0);
    graphene_point3d_t *p2=&g_array_index(star_t, graphene_point3d_t, 0);
    memcpy(p2, p1, sizeof(graphene_point3d_t)*(star->len));
  }
static void reset_draw_b_star()
  {
    //Start with a new copy of the original cube, lines and reference vector.
    graphene_point3d_t *p1=&g_array_index(b_star, graphene_point3d_t, 0);
    graphene_point3d_t *p2=&g_array_index(b_star_t, graphene_point3d_t, 0);
    memcpy(p2, p1, sizeof(graphene_point3d_t)*(b_star->len));
  }
static void reset_plane_pts()
  {
    gint i=0;
    gdouble qrs[9];
    quaternion_rotation(combo_yaw2*G_PI/180.0, combo_roll2*G_PI/180.0, combo_pitch2*G_PI/180.0, qrs); 

    for(i=0;i<3;i++)
      {
        plane_pts_t[3*i]=plane_pts[3*i];
        plane_pts_t[3*i+1]=plane_pts[3*i+1];
        plane_pts_t[3*i+2]=plane_pts[3*i+2];
      }
    rotate_plane(qrs);
  }
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
static void rotate_shape(gdouble qrs[9])
  {
    gint i=0;
    gfloat cr1=0.0;
    gfloat cr2=0.0;
    gfloat cr3=0.0;   
    graphene_point3d_t *p1=NULL;

    if(shape_id==0)
      {
        p1=&g_array_index(star_t, graphene_point3d_t, 0);
        for(i=0;i<(star_t->len);i++)
          {
            cr1=(p1->x);
            cr2=(p1->y);
            cr3=(p1->z);         
            p1->x=(cr1*(gfloat)qrs[0])+(cr2*(gfloat)qrs[1])+(cr3*(gfloat)qrs[2]);
            p1->y=(cr1*(gfloat)qrs[3])+(cr2*(gfloat)qrs[4])+(cr3*(gfloat)qrs[5]);
            p1->z=(cr1*(gfloat)qrs[6])+(cr2*(gfloat)qrs[7])+(cr3*(gfloat)qrs[8]);
            p1++;
          }    
      }
    else if(shape_id==1)
      {
        p1=&g_array_index(b_star_t, graphene_point3d_t, 0);
        for(i=0;i<(b_star_t->len);i++)
          {
            cr1=(p1->x);
            cr2=(p1->y);
            cr3=(p1->z);         
            p1->x=(cr1*(gfloat)qrs[0])+(cr2*(gfloat)qrs[1])+(cr3*(gfloat)qrs[2]);
            p1->y=(cr1*(gfloat)qrs[3])+(cr2*(gfloat)qrs[4])+(cr3*(gfloat)qrs[5]);
            p1->z=(cr1*(gfloat)qrs[6])+(cr2*(gfloat)qrs[7])+(cr3*(gfloat)qrs[8]);
            p1++;
          }    
      }
    else g_print("Nothing Rotated\n");  
  }
static void rotate_plane(gdouble qrs[9])
  {
    gint i=0;
    gfloat cr1=0.0;
    gfloat cr2=0.0;
    gfloat cr3=0.0;   

    for(i=0;i<3;i++)
      {
        cr1=plane_pts_t[3*i];
        cr2=plane_pts_t[3*i+1];
        cr3=plane_pts_t[3*i+2];;         
        plane_pts_t[3*i]=(cr1*(gfloat)qrs[0])+(cr2*(gfloat)qrs[1])+(cr3*(gfloat)qrs[2]);
        plane_pts_t[3*i+1]=(cr1*(gfloat)qrs[3])+(cr2*(gfloat)qrs[4])+(cr3*(gfloat)qrs[5]);
        plane_pts_t[3*i+2]=(cr1*(gfloat)qrs[6])+(cr2*(gfloat)qrs[7])+(cr3*(gfloat)qrs[8]);
      }    
  }
static void draw_star(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;
    gdouble sun_x=0.0;
    gdouble sun_y=0.0; 
    gdouble shadow[30];
    graphene_point3d_t *p1=&g_array_index(star_t, graphene_point3d_t, 0);

    //Draw the sun.
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    sun_x=3.5*w1*cos(combo_shadow*2.0*G_PI/100.0);
    sun_y=3.5*w1*sin(combo_shadow*2.0*G_PI/100.0);
    cairo_arc(cr,  sun_x, sun_y, 15.0, 0.0, 2*G_PI);
    cairo_fill(cr);

    //A reference plane to stretch the shadow on.
    plane_pts_t[0]=(gfloat)w1*plane_pts_t[0]-(gfloat)sun_x;
    plane_pts_t[1]=(gfloat)w1*plane_pts_t[1]-(gfloat)sun_y;
    plane_pts_t[2]=(gfloat)w1*plane_pts_t[2];
    plane_pts_t[3]=(gfloat)w1*plane_pts_t[3]-(gfloat)sun_x;
    plane_pts_t[4]=(gfloat)w1*plane_pts_t[4]-(gfloat)sun_y;
    plane_pts_t[5]=(gfloat)w1*plane_pts_t[5];
    plane_pts_t[6]=(gfloat)w1*plane_pts_t[6]-(gfloat)sun_x;
    plane_pts_t[7]=(gfloat)w1*plane_pts_t[7]-(gfloat)sun_y;
    plane_pts_t[8]=(gfloat)w1*plane_pts_t[8];

    //Check for line plane intersection with the shadow star and rays.
    graphene_plane_t g_plane;
    graphene_point3d_t point1={.x=plane_pts_t[0], .y=plane_pts_t[1], .z=plane_pts_t[2]};
    graphene_point3d_t point2={.x=plane_pts_t[3], .y=plane_pts_t[4], .z=plane_pts_t[5]};
    graphene_point3d_t point3={.x=plane_pts_t[6], .y=plane_pts_t[7], .z=plane_pts_t[8]};
    graphene_plane_init_from_points(&g_plane, &point1, &point2, &point3);
    gfloat origin_a[3]={(gfloat)sun_x, (gfloat)sun_y, 0.0f};
    graphene_vec3_t origin;
    graphene_vec3_init_from_float(&origin, origin_a);
    gfloat direction_a[3];
    graphene_vec3_t direction;    
    graphene_ray_t ray;    
    gfloat distance1=0.0f;
    gfloat distance2=0.0f;
    gdouble u=0.0;
    for(i=0;i<10;i++)
      {
        direction_a[0]=(gfloat)w1*(p1->x)-(gfloat)sun_x;
        direction_a[1]=(gfloat)w1*(p1->y)-(gfloat)sun_y;
        direction_a[2]=(gfloat)w1*(p1->z);
        graphene_vec3_init_from_float(&direction, direction_a);

        graphene_ray_init_from_vec3(&ray, &origin, &direction);
        distance1=graphene_ray_get_distance_to_plane(&ray, &g_plane);
        distance2=graphene_vec3_length(&direction); 
     
        //Constraints for when the plane is parallel with the rays. Still some problems.
        if(distance2<1.0&&distance2>0.0) distance2=1.0;
        if(distance2>-1.0&&distance2<=0.0) distance2=-1.0;
        if(distance1==INFINITY) u=1.0;
        else u=distance1/distance2; 
        if(u<1.0) u=1.0; 

        //u=(gdouble)(distance1/distance2);     
        shadow[3*i]=(sun_x+u*(w1*(gdouble)(p1->x)-sun_x));
        shadow[3*i+1]=(sun_y+u*(w1*(gdouble)(p1->y)-sun_y));
        shadow[3*i+2]=u*w1*(gdouble)(p1->z);
        p1++;
      }
 
    //Draw the shadow.
    p1=&g_array_index(star_t, graphene_point3d_t, 0);
    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
    for(i=0;i<9;i++)
      {
        cairo_pattern_t *pattern1=cairo_pattern_create_linear( w1*(gdouble)(p1->x), w1*(gdouble)(p1->y), shadow[3*i], shadow[3*i+1]); 
        cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 0.0, 1.0, 1.0, 0.7); 
        cairo_pattern_add_color_stop_rgba(pattern1, 1.0, 0.0, 1.0, 1.0, 0.05);  
        cairo_set_source(cr, pattern1); 
        cairo_move_to(cr, w1*(gdouble)(p1->x), w1*(gdouble)(p1->y));
        cairo_line_to(cr, shadow[3*i], shadow[3*i+1]);
        cairo_line_to(cr, shadow[3*i+3], shadow[3*i+4]);
        cairo_line_to(cr, w1*(gdouble)((p1+1)->x), w1*(gdouble)((p1+1)->y));
        cairo_close_path(cr);     
        cairo_fill(cr);
        cairo_pattern_destroy(pattern1);
        p1++;  
      }
    cairo_set_operator(cr, CAIRO_OPERATOR_OVER);

    //Draw the shadow star.
    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 0.5);
    cairo_move_to(cr, shadow[0], shadow[1]);
    for(i=1;i<10;i++)
      {
        cairo_line_to(cr, shadow[3*i], shadow[3*i+1]);  
      }
    cairo_close_path(cr);
    cairo_fill(cr);

    //Draw the lines from the sun to the star to the shadow star.
    p1=&g_array_index(star_t, graphene_point3d_t, 0);
    for(i=0;i<(star_t->len);i++)
      {
        cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
        cairo_move_to(cr, sun_x, sun_y);
        cairo_line_to(cr, w1*(gdouble)(p1->x), w1*(gdouble)(p1->y));
        cairo_stroke(cr);
        cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
        cairo_move_to(cr, w1*(gdouble)(p1->x), w1*(gdouble)(p1->y));
        cairo_line_to(cr, shadow[3*i], shadow[3*i+1]);  
        cairo_stroke(cr);
        p1++;
      }
 
    //Draw the star.
    p1=&g_array_index(star_t, graphene_point3d_t, 0);
    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 0.8);
    cairo_move_to(cr, w1*(gdouble)(p1->x), w1*(gdouble)(p1->y));
    p1++;
    for(i=1;i<(star_t->len);i++)
      {
        cairo_line_to(cr, w1*(gdouble)(p1->x), w1*(gdouble)(p1->y));
        p1++;       
      }
    cairo_close_path(cr);
    cairo_fill_preserve(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_stroke(cr);

    //Draw the plane
    //cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    //cairo_move_to(cr, (gdouble)plane_pts_t[0], (gdouble)plane_pts_t[1]);
    //cairo_line_to(cr, (gdouble)plane_pts_t[3], (gdouble)plane_pts_t[4]);
    //cairo_line_to(cr, (gdouble)plane_pts_t[6], (gdouble)plane_pts_t[7]);
    //cairo_close_path(cr);
    //cairo_fill(cr);
  }
static void draw_b_star(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;
    gdouble sun_x=0.0;
    gdouble sun_y=0.0; 
    gdouble shadow[45];
    graphene_point3d_t *p1=&g_array_index(b_star_t, graphene_point3d_t, 0);
    graphene_point3d_t *p2=&g_array_index(b_star_t, graphene_point3d_t, 0);

    //Draw the sun.
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    sun_x=3.5*w1*cos(combo_shadow*2.0*G_PI/100.0);
    sun_y=3.5*w1*sin(combo_shadow*2.0*G_PI/100.0);
    cairo_arc(cr,  sun_x, sun_y, 15.0, 0.0, 2*G_PI);
    cairo_fill(cr);

    //A reference plane to stretch the shadow on.
    plane_pts_t[0]=(gfloat)w1*plane_pts_t[0]-(gfloat)sun_x;
    plane_pts_t[1]=(gfloat)w1*plane_pts_t[1]-(gfloat)sun_y;
    plane_pts_t[2]=(gfloat)w1*plane_pts_t[2];
    plane_pts_t[3]=(gfloat)w1*plane_pts_t[3]-(gfloat)sun_x;
    plane_pts_t[4]=(gfloat)w1*plane_pts_t[4]-(gfloat)sun_y;
    plane_pts_t[5]=(gfloat)w1*plane_pts_t[5];
    plane_pts_t[6]=(gfloat)w1*plane_pts_t[6]-(gfloat)sun_x;
    plane_pts_t[7]=(gfloat)w1*plane_pts_t[7]-(gfloat)sun_y;
    plane_pts_t[8]=(gfloat)w1*plane_pts_t[8];

    //Check for line plane intersection with the shadow star and rays.
    graphene_plane_t g_plane;
    graphene_point3d_t point1={.x=plane_pts_t[0], .y=plane_pts_t[1], .z=plane_pts_t[2]};
    graphene_point3d_t point2={.x=plane_pts_t[3], .y=plane_pts_t[4], .z=plane_pts_t[5]};
    graphene_point3d_t point3={.x=plane_pts_t[6], .y=plane_pts_t[7], .z=plane_pts_t[8]};
    graphene_plane_init_from_points(&g_plane, &point1, &point2, &point3);
    gfloat origin_a[3]={(gfloat)sun_x, (gfloat)sun_y, 0.0f};
    graphene_vec3_t origin;
    graphene_vec3_init_from_float(&origin, origin_a);
    gfloat direction_a[3];
    graphene_vec3_t direction;    
    graphene_ray_t ray;    
    gfloat distance1=0.0f;
    gfloat distance2=0.0f;
    gdouble u=0.0;
    for(i=0;i<15;i++)
      {
        direction_a[0]=(gfloat)w1*(p1->x)-(gfloat)sun_x;
        direction_a[1]=(gfloat)w1*(p1->y)-(gfloat)sun_y;
        direction_a[2]=(gfloat)w1*(p1->z);
        graphene_vec3_init_from_float(&direction, direction_a);

        graphene_ray_init_from_vec3(&ray, &origin, &direction);
        distance1=graphene_ray_get_distance_to_plane(&ray, &g_plane);
        distance2=graphene_vec3_length(&direction); 
     
        //Constraints for when the plane is parallel with the rays. Still some problems.
        if(distance2<1.0&&distance2>0.0) distance2=1.0;
        if(distance2>-1.0&&distance2<=0.0) distance2=-1.0;
        if(distance1==INFINITY) u=1.0;
        else u=distance1/distance2; 
        if(u<1.0) u=1.0; 

        //u=(gdouble)(distance1/distance2);     
        shadow[3*i]=(sun_x+u*(w1*(gdouble)(p1->x)-sun_x));
        shadow[3*i+1]=(sun_y+u*(w1*(gdouble)(p1->y)-sun_y));
        shadow[3*i+2]=u*w1*(gdouble)(p1->z);
        p1++;
      }
    
    //Draw the shadow.
    p1=&g_array_index(b_star_t, graphene_point3d_t, 0);
    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
    for(i=0;i<5;i++)
      {
        cairo_pattern_t *pattern1=cairo_pattern_create_linear( w1*(p1->x), w1*(p1->y), shadow[9*i], shadow[9*i+1]); 
        cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 0.0, 1.0, 1.0, 0.7); 
        cairo_pattern_add_color_stop_rgba(pattern1, 1.0, 0.0, 1.0, 1.0, 0.05);  
        cairo_set_source(cr, pattern1); 
        cairo_move_to(cr, w1*(p1->x), w1*(p1->y));
        cairo_line_to(cr, shadow[9*i], shadow[9*i+1]);
        if(i<4)cairo_curve_to(cr, shadow[9*i+3], shadow[9*i+4], shadow[9*i+6], shadow[9*i+7], shadow[9*i+9], shadow[9*i+10]);
        else cairo_curve_to(cr, shadow[9*i+3], shadow[9*i+4], shadow[9*i+6], shadow[9*i+7], shadow[0], shadow[1]);
        if(i<4) cairo_line_to(cr, w1*((p1+3)->x), w1*((p1+3)->y));
        else cairo_line_to(cr, w1*(p2->x), w1*(p2->y));
        cairo_curve_to(cr, w1*((p1+2)->x), w1*((p1+2)->y), w1*((p1+1)->x), w1*((p1+1)->y), w1*(p1->x), w1*(p1->y));
        cairo_close_path(cr);     
        cairo_fill(cr);
        cairo_pattern_destroy(pattern1);
        p1+=3;  
      }
    cairo_set_operator(cr, CAIRO_OPERATOR_OVER);

    //Draw the shadow star.
    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 0.5);
    cairo_move_to(cr, shadow[0], shadow[1]);
    for(i=0;i<4;i++)
      {
        cairo_curve_to(cr, shadow[9*i+3], shadow[9*i+4], shadow[9*i+6], shadow[9*i+7], shadow[9*i+9], shadow[9*i+10]);  
      }
    cairo_curve_to(cr, shadow[36], shadow[37], shadow[39], shadow[40], shadow[0], shadow[1]);
    cairo_close_path(cr);
    cairo_fill(cr);

    //Draw the lines from the sun to the star to the shadow star.
    p1=&g_array_index(b_star_t, graphene_point3d_t, 0);
    for(i=0;i<(b_star_t->len);i++)
      {
        cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
        cairo_move_to(cr, sun_x, sun_y);
        cairo_line_to(cr, w1*(p1->x), w1*(p1->y));
        cairo_stroke(cr);
        cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
        cairo_move_to(cr, w1*(p1->x), w1*(p1->y));
        cairo_line_to(cr, shadow[3*i], shadow[3*i+1]);  
        cairo_stroke(cr);
        p1++;
      }

    //Draw the star.
    p1=&g_array_index(b_star_t, graphene_point3d_t, 0);
    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 0.8);
    cairo_move_to(cr, w1*(p1->x), w1*(p1->y));  
    for(i=0;i<4;i++)
      {
        cairo_curve_to(cr, w1*((p1+1)->x), w1*((p1+1)->y), w1*((p1+2)->x), w1*((p1+2)->y), w1*((p1+3)->x), w1*((p1+3)->y));
        p1+=3;       
      }
    cairo_curve_to(cr, w1*((p1+1)->x), w1*((p1+1)->y), w1*((p1+2)->x), w1*((p1+2)->y), w1*(p2->x), w1*(p2->y));
    cairo_close_path(cr);
    cairo_fill_preserve(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_stroke(cr);
  }



