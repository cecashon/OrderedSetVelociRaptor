
#For use with introspection and the velo_gyro widget. See velo_gyro.c for setting it up.

use strict;
use diagnostics;
use warnings;
use Gtk3 '-init';

Glib::Object::Introspection->setup(basename => 'Velo', version => '0.1', package => 'Velo');

package TestGyro;
sub new
{
    my $class = shift;
    my $self = {};
    bless $self, $class;

    $self->{'window'} = Gtk3::Window->new();
    $self->{'window'}->signal_connect('delete_event' => sub { Gtk3->main_quit; });
    $self->{'window'}->set_default_size(400, 400);
    $self->{'window'}->set_title("VeloGyro");

    $self->{'gyro'} = Velo::Gyro->new();

    $self->{'window'}->add($self->{'gyro'});

    return $self;
}
my $win = new TestGyro();
$win->{'window'}->show_all();
Gtk3->main;




