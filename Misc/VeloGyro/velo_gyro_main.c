
/*
    Put rotation2.c into a gyro widget and test.

    gcc -Wall velo_gyro.c velo_gyro_main.c -o velo_gyro `pkg-config gtk+-3.0 --cflags --libs` -lm

    Tested on Ubuntu16.04, GTK3.18.

    C. Eric Cashon

*/

#include<gtk/gtk.h>
#include "velo_gyro.h"
#include<stdlib.h>

//Rotations for gyro1.
static gdouble yaw=0;
static gdouble roll=0;
static gdouble pitch=0;

static guint tick_id=0;

static gboolean update_rotation(GtkWidget *da, GdkFrameClock *frame_clock, gpointer data);
static void set_rotation_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations);
static void set_rotation_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations);
static void set_rotation_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations);

int main(int argc, char *argv[])
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Velo Gyro");
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), 1000, 400);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    GtkWidget *gyro1=velo_gyro_new();
    gtk_widget_set_hexpand(gyro1, TRUE);
    gtk_widget_set_vexpand(gyro1, TRUE); 

    GtkWidget *gyro2=velo_gyro_new();
    velo_gyro_show_text(VELO_GYRO(gyro2), TRUE);
    velo_gyro_set_drawing(VELO_GYRO(gyro2), FLYING_SQUIRREL);
    velo_gyro_set_draw_square(VELO_GYRO(gyro2), TRUE);
    gtk_widget_set_hexpand(gyro2, TRUE);
    gtk_widget_set_vexpand(gyro2, TRUE); 

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider, TRUE);
    gtk_range_set_value(GTK_RANGE(yaw_slider), 10.0);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider, TRUE);
    gtk_range_set_value(GTK_RANGE(roll_slider), 10.0);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider, TRUE);
    gtk_range_set_value(GTK_RANGE(pitch_slider), 10.0);

    //Rotations for gyro2.
    gdouble plane_r[3]={10.0, 10.0, 10.0};

    //Setup callbacks.
    gpointer rotations[]={plane_r, gyro2};
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_rotation_yaw), rotations);
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_rotation_roll), rotations);
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_rotation_pitch), rotations);

    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 5);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 2, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 2, 1, 1, 1);

    GtkWidget *grid2=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid2), gyro1, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), gyro2, 1, 0, 1, 1);

    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), grid2, TRUE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), grid1, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 775);
  
    gtk_container_add(GTK_CONTAINER(window), paned1);
    gtk_widget_show_all(window);

    tick_id=gtk_widget_add_tick_callback(gyro1, (GtkTickCallback)update_rotation, NULL, NULL);

    gtk_main();
    return 0;
  }
static gboolean update_rotation(GtkWidget *da, GdkFrameClock *frame_clock, gpointer data)
 {
   //Check frame rate.
   gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
   gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
   gint64 start = gdk_frame_clock_get_history_start(frame_clock);
   gint64 history_len=frame-start;
   GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
   gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
   g_print("Frame %lld, %f fps\n", frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));

   //Update angles.
   if(yaw>359.0) yaw=0.0;
   else yaw+=1.0;    
   if(pitch>359.0) pitch=0.0;
   else pitch+=1.0;
   if(roll>359.0) roll=0.0;
   else roll+=0.1;

   velo_gyro_set_rotations(VELO_GYRO(da), yaw, roll, pitch);

   gtk_widget_queue_draw(da);
   return G_SOURCE_CONTINUE;
 }
static void set_rotation_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations)
  {
    if(value>360.0) ((gdouble*)(rotations[0]))[0]=360.0;
    else if(value<0.0) ((gdouble*)(rotations[0]))[0]=0.0;
    else ((gdouble*)(rotations[0]))[0]=value; 

    velo_gyro_set_rotations(VELO_GYRO(rotations[1]), ((gdouble*)(rotations[0]))[0], ((gdouble*)(rotations[0]))[1], ((gdouble*)(rotations[0]))[2]);

    gtk_widget_queue_draw(GTK_WIDGET(rotations[1]));
  }
static void set_rotation_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations)
  {
    if(value>360.0) ((gdouble*)(rotations[0]))[1]=360.0;
    else if(value<0.0) ((gdouble*)(rotations[0]))[1]=0.0;
    else ((gdouble*)(rotations[0]))[1]=value; 

    velo_gyro_set_rotations(VELO_GYRO(rotations[1]), ((gdouble*)(rotations[0]))[0], ((gdouble*)(rotations[0]))[1], ((gdouble*)(rotations[0]))[2]);

    gtk_widget_queue_draw(GTK_WIDGET(rotations[1]));
  }
static void set_rotation_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations)
  {
    if(value>360.0) ((gdouble*)(rotations[0]))[2]=360.0;
    else if(value<0.0) ((gdouble*)(rotations[0]))[2]=0.0;
    else ((gdouble*)(rotations[0]))[2]=value;

    velo_gyro_set_rotations(VELO_GYRO(rotations[1]), ((gdouble*)(rotations[0]))[0], ((gdouble*)(rotations[0]))[1], ((gdouble*)(rotations[0]))[2]); 

    gtk_widget_queue_draw(GTK_WIDGET(rotations[1]));
  }
