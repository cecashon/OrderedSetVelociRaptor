
#For use with introspection and the velo_gyro widget. See velo_gyro.c for setting it up.

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Velo', '0.1')
from gi.repository import Gtk, Velo

class TestGyro(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="VeloGyro")
        self.set_default_size(400, 400)

        self.gyro=Velo.Gyro()

        self.add(self.gyro)

win = TestGyro()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()
