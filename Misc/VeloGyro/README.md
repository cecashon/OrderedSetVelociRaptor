

--A velo gyro widget. Set the pitch, roll and yaw for the stealth plane.

--There are some instructions in the velo_gyro.c file for setting up introspection. Then
the widget can be used from higher level languages like Python, Perl or Java Script.

Velo Gyro

![ScreenShot](/Misc/VeloGyro/velo_gyro.png)

