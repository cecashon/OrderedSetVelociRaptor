
//For use with introspection and the velo_gyro widget. See velo_gyro.c for setting it up.

const Lang = imports.lang;
const Gtk = imports.gi.Gtk;
const Velo = imports.gi.Velo; 

Gtk.init(null);

const TestGyro = new Lang.Class({
    Name: 'Gyro',
    Extends: Gtk.Window,

    _init: function() {
        this.parent({title: "VeloGyro"});
        this.set_default_size(400, 400);

        this.gyro = new Velo.Gyro();
        
        this.add(this.gyro);
    },
});

let win = new TestGyro();
win.connect("delete-event", Gtk.main_quit);
win.show_all();
Gtk.main();
