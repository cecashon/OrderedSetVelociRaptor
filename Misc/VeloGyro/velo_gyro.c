
/*

   For use with velo_gyro_main.c. Look in velo_gyro_main.c for more information.

   For building a library for introspection. The starting point for this is the following reference

    http://helgo.net/simon/introspection-tutorial/index.xhtml

    For introspection notice the test paths that are being used. They need to be changed
to the test folder the velo_gyro files are in. Also, this is assuming that is your current
directory that you are working in. 

      libtool compile gcc `pkg-config gtk+-3.0 gobject-introspection-1.0 --cflags --libs` -g -c velo_gyro.c -o Velo.lo

      libtool link gcc `pkg-config gtk+-3.0 gobject-introspection-1.0 --libs` -rpath /home/eric/Velo/Misc/VeloGyro Velo.lo -o libVelo.la 

      g-ir-scanner velo_gyro.[ch] --warn-all --library-path=/home/eric/Velo/Misc/VeloGyro/ --library=libVelo.la `pkg-config gtk+-3.0 --cflags` --include=Gtk-3.0 --namespace=Velo --nsversion=0.1 --output=Velo-0.1.gir

      g-ir-compiler Velo-0.1.gir --output=Velo-0.1.typelib

      export LD_LIBRARY_PATH=`pwd`/.libs:$LD_LIBRARY_PATH
      export GI_TYPELIB_PATH=`pwd`

   Now try the test files.

      python velo_gyro.py
      perl velo_gyro.pl
      gjs velo_gyro.js
   
   For building the documentation. (sudo apt install gtk-doc-tool)

      mkdir doc
      gtkdoc-scan --module=Velo --source-dir=/home/eric/Velo/Misc/VeloGyro --output-dir=/home/eric/Velo/Misc/VeloGyro/doc 
      cd ./doc
      mkdir xml
      gtkdoc-mkdb --default-stability=Unstable --module=Velo --source-dir=/home/eric/Velo/Misc/VeloGyro --source-dir=/home/eric/Velo/Misc/VeloGyro/doc --output-dir=/home/eric/Velo/Misc/VeloGyro/doc/xml --output-format=xml --main-sgml-file=Velo-docs.sgml
      gtkdoc-mkhtml Velo Velo-docs.sgml

   Should have a Velo-VeloGyro.html file that can be opened in the browser.

   
   Tested with: GTK3.18 and Ubuntu16.04


   C. Eric Cashon

*/

#include<math.h>
#include "velo_gyro.h"

/**
 * SECTION:velo_gyro
 * @Short_description: A gyro.
 * @Title: VeloGyro
 * @See_also: #GtkDrawingArea, #GtkWidget
 *
 * VeloGyro is a simple gyro widget.
 *
 */
#define VELO_GYRO_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), VELO_GYRO_TYPE, VeloGyroPrivate))

typedef struct _VeloGyroPrivate VeloGyroPrivate;

//A 3d point for rotations.
struct point3d{
  gdouble x;
  gdouble y;
  gdouble z;
};

//For generating bezier arcs for a ring.
struct arc{
  struct point3d a1;
  struct point3d a2;
  struct point3d a3;
  struct point3d a4;
};

/*
  The starting reference plane drawing. These are constant drawings in that the numbers
  are always the same but the data is written to. Can be shared across drawings.
*/
static gdouble plane_coords[19][3]={  
      {-3.0, 0.0, 0.0},  
      {0.0, 3.0, 0.0},  
      {0.0, 1.0, 0.0}, 
      {2.0, 1.5, 0.0},
      {2.0, 0.5, 0.0},
      {1.0, 0.0, 0.0},
      {2.0, -0.5, 0.0},
      {2.0, -1.5, 0.0},
      {0.0, -1.0, 0.0},
      {0.0, -3.0, 0.0},
      {-3.0, 0.0, 0.0},
      //cockpit.
      {-2.5, 0.0, 0.0},
      {-1.7, 0.0, 0.3},
      {-1.2, 0.0, 0.3},
      {-0.5, 0.0, 0.0},
      {-1.7, -0.5, 0.0}, 
      {-1.7, 0.5, 0.0}, 
      {-1.2, -0.5, 0.0},
      {-1.2, 0.5, 0.0}          
      };

//From rooster1.c. Shift back x-5.0 and y-5.0. The rooster1.c drawing didn't draw from the center.
static gdouble raptor_coords[26][6]={  
      {-5.00, -5.00, -5.00, -5.00, 0.00, -2.00},
      {-0.30, -1.70, -0.30, -1.50, -0.50, -1.00},
      {-1.50, -0.70, -2.00, -1.40, -3.60, -1.25},
      {-3.50, -1.10, -3.20, -1.10, -3.20, -1.00},
      {-3.20, -1.00, -3.50, -1.00, -3.90, -1.10},
      {-3.50, -0.80, -3.20, -0.80, -3.30, -0.80},
      {-3.20, -0.70, -3.50, -0.80, -4.00, -0.85},
      {-3.50, -0.50, -3.20, -0.50, -3.30, -0.50},
      {-3.20, -0.50, -3.50, -0.50, -4.00, -0.55},
      {-3.50, -0.20, -3.20, -0.20, -3.50, -0.20},
      {-3.20, -0.20, -3.50, -0.20, -3.80, -0.15},
      {-2.00, 0.70, -1.00, 0.70, -0.50, 0.50},
      {-5.00, -5.00, -5.00, -5.00, -0.80, 2.00},
      {-0.40, 2.50, 0.40, 2.50, 0.80, 2.00},
      {-5.00, -5.00, -5.00, -5.00, 0.50, 0.50},
      {1.00, 0.70, 2.00, 0.70, 3.80, -0.15},
      {3.50, -0.20, 3.20, -0.20, 3.50, -0.20},
      {3.20, -0.20, 3.50, -0.20, 4.00, -0.55},
      {3.50, -0.50, 3.20, -0.50, 3.30, -0.50},
      {3.20, -0.50, 3.50, -0.50, 4.00, -0.85},
      {3.50, -0.80, 3.20, -0.70, 3.30, -0.80},
      {3.20, -0.80, 3.50, -0.80, 3.90, -1.10},
      {3.50, -1.00, 3.20, -1.00, 3.20, -1.00},
      {3.20, -1.10, 3.50, -1.10, 3.60, -1.25},
      {2.00, -1.40, 1.50, -0.70, 0.50, -1.00},
      {0.30, -1.50, 0.30, -1.70, 0.00, -2.00},
      };

static gdouble flying_squirrel_coords[16][6]={  
      {-5.0, -5.0, -5.0, -5.0, 0.0, -3.5},
      {-0.5, -3.0, -0.5, -3.0, -0.6, -2.5},
      {-1.0, -2.0, -2.5, -2.0, -3.0, -2.5},
      {-3.2, -2.6, -3.2, -2.6, -3.3, -2.5},
      {-2.0, -1.0, -2.0, 0.0, -3.0, 1.0},
      {-3.5, 1.7, -3.5, 1.7, -2.8, 1.5},
      {-2.2, 0.9, -1.6, 0.9, -0.5, 1.2},
      {-0.7, 4.8, 0.7, 4.8, 0.5, 1.2},
      {1.6, 0.9, 2.2, 0.9, 2.8, 1.5},
      {3.5, 1.7, 3.5, 1.7, 3.0, 1.0},
      {2.0, 0.0, 2.0, -1.0, 3.3, -2.5},
      {3.2, -2.6, 3.2, -2.6, 3.0, -2.5},
      {2.5, -2.0, 1.0, -2.0, 0.6, -2.5},
      {0.5, -3.0, 0.5, -3.0, 0.0, -3.5},
      //eyes
      {-5.0, -5.0, -5.0, -5.0, -0.2, -3.0},
      {-5.0, -5.0, -5.0, -5.0, 0.2, -3.0},
      };

//The rotated plane drawing.
static gdouble plane_rotated[19][2];
//The rotated raptor.
static gdouble raptor_rotated[26][6];
//The rotated flying squirrel.
static gdouble flying_squirrel_rotated[16][6];
//A reference point to rotate for determining the top and bottom of the drawing.
static gdouble ref[3]={0.0, 0.0, 1.0};
static gdouble ref_r[3]={0.0, 0.0, 1.0};
//Rotations for aligning squirrel gradient.
static gdouble rotations[4]={0.0, 0.0, 0.0, 0.0};

//Private variables for the VeloGyro instance.
struct _VeloGyroPrivate
{  
  gint drawing_id;
  gboolean draw_square;
  //Background color.
  gchar *bc_string;
  gdouble bc[4];
  gdouble pitch;
  gdouble roll;
  gdouble yaw;
  //Reference circles.
  GArray *circle1;
  GArray *circle2;
  GArray *circle3;  
  //Rotated circles.
  GArray *circle1_r;
  GArray *circle2_r;
  GArray *circle3_r;
  //Show the yaw, roll and pitch numbers in the drawing.
  gboolean show_text;
};

enum
{
  PROP_0,
  DRAWING_ID,
  DRAW_SQUARE,
  BACKGROUND_COLOR,
  PITCH,
  ROLL,
  YAW,
  SHOW_TEXT
};

//Private functions.
static void velo_gyro_class_init(VeloGyroClass *klass);
static void velo_gyro_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec);
static void velo_gyro_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec);
static void velo_gyro_init(VeloGyro *da);
static void velo_gyro_finalize(GObject *gobject);
//Draw the gyro.
static gboolean velo_gyro_draw(GtkWidget *widget, cairo_t *cr);
static void draw_ellipses(GtkWidget *da, cairo_t *cr);
static void draw_smooth_with_rotation(GtkWidget *da, cairo_t *cr, gdouble w1, gdouble h1);
static void draw_plane(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_raptor(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_flying_squirrel(cairo_t *cr, gdouble w1, gdouble h1);
static void quaternion_rotation(GtkWidget *da);
//Generate a bezier ring.
static GArray* get_bezier_ring(gdouble radius, gdouble points_per_ring);

G_DEFINE_TYPE(VeloGyro, velo_gyro, GTK_TYPE_DRAWING_AREA)

static void velo_gyro_class_init(VeloGyroClass *klass)
  { 
    GObjectClass *gobject_class;
    GtkWidgetClass *widget_class;

    gobject_class=(GObjectClass*)klass;
    widget_class=(GtkWidgetClass*)klass;

    //Set the property funtions.
    gobject_class->set_property=velo_gyro_set_property;
    gobject_class->get_property=velo_gyro_get_property;

    //Draw when first shown.
    widget_class->draw=velo_gyro_draw;
    gobject_class->finalize=velo_gyro_finalize;

    g_type_class_add_private(klass, sizeof(VeloGyroPrivate));

    g_object_class_install_property(gobject_class, DRAWING_ID, g_param_spec_int("drawing_id", "drawing_id", "drawing_id", 0, 1, 0, G_PARAM_READWRITE));

    g_object_class_install_property(gobject_class, DRAW_SQUARE, g_param_spec_boolean("draw_square", "draw_square", "draw_square", FALSE, G_PARAM_READWRITE));

    g_object_class_install_property(gobject_class, BACKGROUND_COLOR, g_param_spec_string("background_color", "background_color", "background_color", NULL, G_PARAM_READWRITE));

    g_object_class_install_property(gobject_class, PITCH, g_param_spec_int("pitch", "pitch", "pitch", 0, 1, 0, G_PARAM_READWRITE));

    g_object_class_install_property(gobject_class, ROLL, g_param_spec_int("roll", "roll", "roll", 0, 1, 0, G_PARAM_READWRITE));

    g_object_class_install_property(gobject_class, YAW, g_param_spec_int("yaw", "yaw", "yaw", 0, 1, 0, G_PARAM_READWRITE));

    g_object_class_install_property(gobject_class, SHOW_TEXT, g_param_spec_boolean("show_text", "show_text", "show_text", FALSE, G_PARAM_READWRITE));
  }
//Needed for g_object_set().
static void velo_gyro_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
  {
    VeloGyro *da=VELO_GYRO(object);

    switch(prop_id)
      {
        case DRAWING_ID:
          velo_gyro_set_drawing(da, g_value_get_int(value));
          break;
        case DRAW_SQUARE:
          velo_gyro_set_draw_square(da, g_value_get_boolean(value));
          break;
        case BACKGROUND_COLOR:
          velo_gyro_set_background_color(da, g_value_get_string(value));
          break;
        case PITCH:
          velo_gyro_set_pitch(da, g_value_get_double(value));
          break;
        case ROLL:
          velo_gyro_set_roll(da, g_value_get_double(value));
          break;
        case YAW:
          velo_gyro_set_yaw(da, g_value_get_double(value));
          break;
        case SHOW_TEXT:
          velo_gyro_show_text(da, g_value_get_boolean(value));
          break;
        default:
          G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
          break;
      }
  }
/**
 * velo_gyro_set_drawing:
 * @da: a #VeloGyro
 * @drawing_id: The gyro drawing to set.
 *
 * Set the drawing of the gyro to be displayed. (STEALTH_PLANE1=0)
 * 
 */
void velo_gyro_set_drawing(VeloGyro *da, gint drawing_id)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);
 
    if(drawing_id==0||drawing_id==1||drawing_id==2)
      {
        priv->drawing_id=drawing_id;         
      }
    else
      {
        g_warning("Only three drawings. STEALTH_PLANE1=0, RAPTOR1=1 and FLYING_SQUIRREL=2.");
      }
  }
/**
 * velo_gyro_set_draw_square:
 * @da: a #VeloGyro
 * @draw_square: Keep the drawing square.
 *
 * Set if the drawing should be kept square.
 * 
 */
void velo_gyro_set_draw_square(VeloGyro *da, gboolean draw_square)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da); 
    
    priv->draw_square=draw_square;         
  }
/**
 * velo_gyro_set_background_color:
 * @da: a #VeloGyro
 * @bc_string: An rgba string
 *
 * Set a background color rgba string of the form "rgba(255, 255, 255, 1.0)". 
 * 
 */  
void velo_gyro_set_background_color(VeloGyro *da, const gchar *bc_string)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);

    GdkRGBA rgba;
    if(gdk_rgba_parse(&rgba, bc_string))
      {
        priv->bc[0]=rgba.red;
        priv->bc[1]=rgba.green;
        priv->bc[2]=rgba.blue;
        priv->bc[3]=rgba.alpha;
        if(priv->bc_string!=NULL) g_free(priv->bc_string);
        priv->bc_string=g_strdup(bc_string); 
      }
    else
      {
        g_warning("background color string not set.\n");
      } 
  }
/**
 * velo_gyro_set_pitch:
 * @da: a #VeloGyro
 * @pitch: The gyro rotation around the x-axis to set.
 *
 * Set the pitch of the drawing.
 * 
 */
void velo_gyro_set_pitch(VeloGyro *da, gdouble pitch)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);
 
    priv->pitch=pitch;         
  }
/**
 * velo_gyro_set_roll:
 * @da: a #VeloGyro
 * @roll: The gyro rotation around the y-axis to set.
 *
 * Set the roll of the drawing.
 * 
 */
void velo_gyro_set_roll(VeloGyro *da, gdouble roll)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);
 
    priv->roll=roll;         
  }
/**
 * velo_gyro_set_yaw:
 * @da: a #VeloGyro
 * @yaw: The gyro rotation around the z-axis to set.
 *
 * Set the yaw of the drawing.
 * 
 */
void velo_gyro_set_yaw(VeloGyro *da, gdouble yaw)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);
 
    priv->yaw=yaw;         
  }
void velo_gyro_show_text(VeloGyro *da, gboolean show_text)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);
 
    priv->show_text=show_text;
  }
void velo_gyro_set_rotations(VeloGyro *da, gdouble yaw, gdouble roll, gdouble pitch)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);

    priv->yaw=yaw; 
    priv->roll=roll;
    priv->pitch=pitch;       
  }
static void velo_gyro_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
  {
    VeloGyro *da=VELO_GYRO(object);
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);
  
    switch(prop_id)
      {
        case DRAWING_ID:
          g_value_set_int(value, priv->drawing_id);
          break;
        case DRAW_SQUARE:
          g_value_set_boolean(value, priv->draw_square);
          break;
        case BACKGROUND_COLOR:
          //free??
          g_value_set_string(value, priv->bc_string);
          break;
        case PITCH:
          g_value_set_double(value, priv->pitch);
          break;
        case ROLL:
          g_value_set_double(value, priv->roll);
          break;
        case YAW:
          g_value_set_double(value, priv->yaw);
          break;
        case SHOW_TEXT:
          g_value_set_boolean(value, priv->show_text);
          break;
        default:
          G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
      }
  }
/**
 * velo_gyro_get_drawing:
 * @da: a #VeloGyro
 *
 * Get the gyro drawing id.
 *
 * Returns: The int type value of the gyro center drawing.
 */
gint velo_gyro_get_drawing(VeloGyro *da)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);
    return priv->drawing_id;
  }
/**
 * velo_gyro_get_draw_square:
 * @da: a #VeloGyro
 *
 * Get if the drawing is square or not.
 *
 * Returns: The gboolean value for drawing square.
 */
gboolean velo_gyro_get_draw_square(VeloGyro *da)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);
    return priv->draw_square;
  }
/**
 * velo_gyro_get_background_color:
 * @da: a #VeloGyro
 *
 * Get the background color as a rgba string.
 *
 * Returns: (transfer none): another constant
 */
const gchar* velo_gyro_get_background_color(VeloGyro *da)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);
    return priv->bc_string;
  }
/**
 * velo_gyro_get_pitch:
 * @da: a #VeloGyro
 *
 * Get the pitch of gyro.
 *
 * Returns: The double type value of the gyro's pitch.
 */
gdouble velo_gyro_get_pitch(VeloGyro *da)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);
    return priv->pitch;
  }
/**
 * velo_gyro_get_roll:
 * @da: a #VeloGyro
 *
 * Get the roll of gyro.
 *
 * Returns: The double type value of the gyro's roll.
 */
gdouble velo_gyro_get_roll(VeloGyro *da)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);
    return priv->roll;
  }
/**
 * velo_gyro_get_yaw:
 * @da: a #VeloGyro
 *
 * Get the yaw of gyro.
 *
 * Returns: The double type value of the gyro's yaw.
 */
gdouble velo_gyro_get_yaw(VeloGyro *da)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);
    return priv->yaw;
  }
static void velo_gyro_init(VeloGyro *da)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);

    //Initialize default drawing.
    priv->drawing_id=STEALTH_PLANE1;
 
    //Initialize background color to white.
    priv->bc[0]=1.0;
    priv->bc[1]=1.0;
    priv->bc[2]=1.0;
    priv->bc[3]=1.0;
    priv->bc_string=g_strdup("rgba(255, 255, 255, 1.0)");

    priv->pitch=10.0;
    priv->roll=10.0;
    priv->yaw=10.0;

    priv->show_text=FALSE;

    //Get a minimum bezier ring of 4 arcs.
    priv->circle1=get_bezier_ring(4.2, 4);
    struct arc *p1=&g_array_index(priv->circle1, struct arc, 0); 

    priv->circle2=g_array_sized_new(FALSE, TRUE, sizeof(struct arc), (priv->circle1->len));
    g_array_set_size(priv->circle2, (priv->circle1->len));
    struct arc *p2=&g_array_index(priv->circle2, struct arc, 0); 

    priv->circle3=g_array_sized_new(FALSE, TRUE, sizeof(struct arc), (priv->circle1->len));
    g_array_set_size(priv->circle3, (priv->circle1->len));
    struct arc *p3=&g_array_index(priv->circle3, struct arc, 0); 

    //Rotate circle2 and circle3 90 degrees.
    gint i=0;
    for(i=0;i<(priv->circle1->len);i++)
      {
        (p2->a1).x=0.0;
        (p2->a1).y=(p1->a1).x;
        (p2->a1).z=(p1->a1).y;
        (p2->a2).x=0.0;
        (p2->a2).y=(p1->a2).x;
        (p2->a2).z=(p1->a2).y;
        (p2->a3).x=0.0;
        (p2->a3).y=(p1->a3).x;
        (p2->a3).z=(p1->a3).y;
        (p2->a4).x=0.0;
        (p2->a4).y=(p1->a4).x;
        (p2->a4).z=(p1->a4).y;
        
        (p3->a1).x=(p1->a1).x;
        (p3->a1).y=0.0;
        (p3->a1).z=(p1->a1).y;
        (p3->a2).x=(p1->a2).x;
        (p3->a2).y=0.0;
        (p3->a2).z=(p1->a2).y;
        (p3->a3).x=(p1->a3).x;
        (p3->a3).y=0.0;
        (p3->a3).z=(p1->a3).y;
        (p3->a4).x=(p1->a4).x;
        (p3->a4).y=0.0;
        (p3->a4).z=(p1->a4).y;
        p1++;p2++;p3++;
      }

    //Arrays for rotated circles.
    priv->circle1_r=g_array_sized_new(FALSE, TRUE, sizeof(struct arc), (priv->circle1->len));
    g_array_set_size(priv->circle1_r, (priv->circle1->len));

    priv->circle2_r=g_array_sized_new(FALSE, TRUE, sizeof(struct arc), (priv->circle1->len));
    g_array_set_size(priv->circle2_r, (priv->circle1->len));

    priv->circle3_r=g_array_sized_new(FALSE, TRUE, sizeof(struct arc), (priv->circle1->len));
    g_array_set_size(priv->circle3_r, (priv->circle1->len));
  }
static void velo_gyro_finalize(GObject *object)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(object);
    g_free(priv->bc_string);

    g_array_free(priv->circle1, TRUE);
    g_array_free(priv->circle2, TRUE);
    g_array_free(priv->circle3, TRUE);
    g_array_free(priv->circle1_r, TRUE);
    g_array_free(priv->circle2_r, TRUE);
    g_array_free(priv->circle3_r, TRUE);
    
    G_OBJECT_CLASS(velo_gyro_parent_class)->finalize(object);
  }
/**
 * velo_gyro_new: (constructor)
 *
 * Creates a new velo_gyro widget.
 *
 * Returns: (transfer full): a new #VeloGyro
 */
GtkWidget* velo_gyro_new()
  {
    return GTK_WIDGET(g_object_new(velo_gyro_get_type(), NULL));
  }

//Start drawing the gyro.
static gboolean velo_gyro_draw(GtkWidget *da, cairo_t *cr)
  {    
    draw_ellipses(da, cr);
    
    return FALSE;
  }
static void draw_ellipses(GtkWidget *da, cairo_t *cr)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);
    gint i=0;
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    gdouble w1=1.0*width/10.0;
    gdouble h1=1.0*height/10.0;

    //Paint background.
    cairo_set_source_rgba(cr, priv->bc[0], priv->bc[1], priv->bc[2], priv->bc[3]);
    cairo_paint(cr);

    if(priv->show_text)
      {
        cairo_text_extents_t extents;
        gchar *string1=g_strdup_printf("yaw   %.1f", priv->yaw);
        gchar *string2=g_strdup_printf("roll  %.1f", priv->roll);
        gchar *string3=g_strdup_printf("pitch %.1f", priv->pitch);
        cairo_set_source_rgb(cr, 0.0, 0.0, 1.0);
        cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
        cairo_set_font_size(cr, 18.0*width/400.0);
        cairo_text_extents(cr, string1, &extents); 
        cairo_move_to(cr, 7.0*w1, 9.0*h1);  
        cairo_show_text(cr, string1);
        cairo_move_to(cr, 7.0*w1, 9.0*h1+extents.height);  
        cairo_show_text(cr, string2);
        cairo_move_to(cr, 7.0*w1, 9.0*h1+2.0*extents.height);  
        cairo_show_text(cr, string3);
        g_free(string1);
        g_free(string2);
        g_free(string3);
      }

    //Draw in the center
    cairo_translate(cr, width/2.0, height/2.0);
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);

    //If the drawing needs to be kept square.
    if(priv->draw_square)
      {
        if(w1>h1) w1=h1;
        else h1=w1;
      } 

    //Draw reference circle.
    cairo_set_line_width(cr, 15.0);
    cairo_set_source_rgba(cr, 0.5, 0.5, 1.0, 1.0);    
    struct arc *p1=&g_array_index(priv->circle1, struct arc, 0);
    cairo_move_to(cr, w1*((p1->a1).x), h1*((p1->a1).y));
    for(i=0;i<(priv->circle1->len);i++)
      {
        cairo_curve_to(cr, w1*((p1->a2).x), h1*((p1->a2).y), w1*((p1->a3).x), h1*((p1->a3).y), w1*((p1->a4).x), h1*((p1->a4).y));
        p1++;
      }
    cairo_stroke(cr);

    cairo_set_line_width(cr, 5.0);

    //Calculate the current rotations for the plane and circles.
    quaternion_rotation(da); 
    draw_smooth_with_rotation(da, cr, w1, h1);  
  }
static void draw_smooth_with_rotation(GtkWidget *da, cairo_t *cr, gdouble w1, gdouble h1)
  {
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);
    gint i=0;
    struct arc *p1=NULL;

    cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
    p1=&g_array_index(priv->circle1_r, struct arc, 0);
    cairo_move_to(cr, w1*((p1->a1).x), h1*((p1->a1).y));
    for(i=0;i<(priv->circle1_r->len);i++)
      {
        cairo_curve_to(cr, w1*((p1->a2).x), h1*((p1->a2).y), w1*((p1->a3).x), h1*((p1->a3).y), w1*((p1->a4).x), h1*((p1->a4).y));
        p1++;
      }
    cairo_stroke(cr);

    cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
    p1=&g_array_index(priv->circle2_r, struct arc, 0);
    cairo_move_to(cr, w1*((p1->a1).x), h1*((p1->a1).y));
    for(i=0;i<(priv->circle2_r->len);i++)
      {
        cairo_curve_to(cr, w1*((p1->a2).x), h1*((p1->a2).y), w1*((p1->a3).x), h1*((p1->a3).y), w1*((p1->a4).x), h1*((p1->a4).y));
        p1++;
      }
    cairo_stroke(cr);
    
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    p1=&g_array_index(priv->circle3_r, struct arc, 0);
    cairo_move_to(cr, w1*((p1->a1).x), h1*((p1->a1).y));
    for(i=0;i<(priv->circle3_r->len);i++)
      {
        cairo_curve_to(cr, w1*((p1->a2).x), h1*((p1->a2).y), w1*((p1->a3).x), h1*((p1->a3).y), w1*((p1->a4).x), h1*((p1->a4).y));
        p1++;
      }
    cairo_stroke(cr);
    
    if(priv->drawing_id==0)
      {        
        cairo_set_source_rgba(cr, 0.8, 0.0, 0.8, 0.8);
        cairo_set_line_width(cr, 4.0);
        draw_plane(cr, w1, h1);
      }
    else if(priv->drawing_id==1)
      {
        cairo_set_line_width(cr, 4.0);
        draw_raptor(cr, w1, h1);
      }
    else
      {
        cairo_set_line_width(cr, 2.0);
        draw_flying_squirrel(cr, w1, h1);
      }
  }
static void draw_plane(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;
    gdouble (*plane)[19][2];

    plane=&plane_rotated;

    //The top view of the plane.
    if(ref_r[2]>=0.0)
      {
        cairo_move_to(cr, (*plane)[0][0]*w1, (*plane)[0][1]*h1);
        for(i=1;i<11;i++)
          {
            cairo_line_to(cr, (*plane)[i][0]*w1, (*plane)[i][1]*h1);
          }
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke_preserve(cr);
        cairo_set_source_rgba(cr, 0.8, 0.4, 0.8, 0.8); 
        cairo_fill(cr);
      }

    //Cockpit
    cairo_set_line_width(cr, 1.5);
    cairo_move_to(cr, (*plane)[11][0]*w1, (*plane)[11][1]*h1);
    cairo_line_to(cr, (*plane)[12][0]*w1, (*plane)[12][1]*h1);
    cairo_line_to(cr, (*plane)[15][0]*w1, (*plane)[15][1]*h1);
    cairo_close_path(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_stroke_preserve(cr);
    cairo_set_source_rgba(cr, 0.8, 0.4, 0.8, 0.8);
    cairo_fill(cr);

    cairo_move_to(cr, (*plane)[11][0]*w1, (*plane)[11][1]*h1);
    cairo_line_to(cr, (*plane)[12][0]*w1, (*plane)[12][1]*h1);
    cairo_line_to(cr, (*plane)[16][0]*w1, (*plane)[16][1]*h1);
    cairo_close_path(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_stroke_preserve(cr);
    cairo_set_source_rgba(cr, 0.8, 0.4, 0.8, 0.8);
    cairo_fill(cr);

    cairo_move_to(cr, (*plane)[12][0]*w1, (*plane)[12][1]*h1);
    cairo_line_to(cr, (*plane)[15][0]*w1, (*plane)[15][1]*h1);
    cairo_line_to(cr, (*plane)[17][0]*w1, (*plane)[17][1]*h1);
    cairo_line_to(cr, (*plane)[13][0]*w1, (*plane)[13][1]*h1);
    cairo_close_path(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_stroke_preserve(cr);
    cairo_set_source_rgba(cr, 0.8, 0.4, 0.8, 0.8);
    cairo_fill(cr);

    cairo_move_to(cr, (*plane)[12][0]*w1, (*plane)[12][1]*h1);
    cairo_line_to(cr, (*plane)[16][0]*w1, (*plane)[16][1]*h1);
    cairo_line_to(cr, (*plane)[18][0]*w1, (*plane)[18][1]*h1);
    cairo_line_to(cr, (*plane)[13][0]*w1, (*plane)[13][1]*h1);
    cairo_close_path(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_stroke_preserve(cr);
    cairo_set_source_rgba(cr, 0.8, 0.4, 0.8, 0.8);
    cairo_fill(cr);

    cairo_move_to(cr, (*plane)[13][0]*w1, (*plane)[13][1]*h1);
    cairo_line_to(cr, (*plane)[17][0]*w1, (*plane)[17][1]*h1);
    cairo_line_to(cr, (*plane)[14][0]*w1, (*plane)[14][1]*h1);
    cairo_close_path(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_stroke_preserve(cr);
    cairo_set_source_rgba(cr, 0.8, 0.4, 0.8, 0.8);
    cairo_fill(cr);

    cairo_move_to(cr, (*plane)[13][0]*w1, (*plane)[13][1]*h1);
    cairo_line_to(cr, (*plane)[18][0]*w1, (*plane)[18][1]*h1);
    cairo_line_to(cr, (*plane)[14][0]*w1, (*plane)[14][1]*h1);
    cairo_close_path(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_stroke_preserve(cr);
    cairo_set_source_rgba(cr, 0.8, 0.4, 0.8, 0.8);
    cairo_fill(cr);

    //The bottom view of the plane.
    if(ref_r[2]<0.0)
      {
        cairo_move_to(cr, (*plane)[0][0]*w1, (*plane)[0][1]*h1);
        for(i=1;i<11;i++)
          {
            cairo_line_to(cr, (*plane)[i][0]*w1, (*plane)[i][1]*h1);
          }
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_stroke_preserve(cr);
        cairo_set_source_rgba(cr, 0.8, 0.7, 0.8, 1.0); 
        cairo_fill(cr);
      }
  }
static void draw_raptor(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;

    //The raptor.
    cairo_move_to(cr, raptor_rotated[0][4]*w1, raptor_rotated[0][5]*h1);
    //Left wing.
    for(i=1;i<12;i++)
      {
        cairo_curve_to(cr, raptor_rotated[i][0]*w1, raptor_rotated[i][1]*h1, raptor_rotated[i][2]*w1, raptor_rotated[i][3]*h1, raptor_rotated[i][4]*w1, raptor_rotated[i][5]*h1);
      }

    //Tail
    cairo_line_to(cr, raptor_rotated[12][4]*w1, raptor_rotated[12][5]*h1);
    cairo_curve_to(cr, raptor_rotated[13][0]*w1, raptor_rotated[13][1]*h1, raptor_rotated[13][2]*w1, raptor_rotated[13][3]*h1, raptor_rotated[13][4]*w1, raptor_rotated[13][5]*h1);
    cairo_line_to(cr, raptor_rotated[14][4]*w1, raptor_rotated[14][5]*h1);

    //Right wing
    for(i=15;i<26;i++)
      {
        cairo_curve_to(cr, raptor_rotated[i][0]*w1, raptor_rotated[i][1]*h1, raptor_rotated[i][2]*w1, raptor_rotated[i][3]*h1, raptor_rotated[i][4]*w1, raptor_rotated[i][5]*h1);
      }

    cairo_close_path(cr);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_stroke_preserve(cr);
    if(ref_r[2]>=0.0) cairo_set_source_rgba(cr, 0.80, 0.45, 0.2, 0.5);
    else cairo_set_source_rgba(cr, 0.8, 0.8, 0.8, 0.5);
    cairo_fill(cr);
  }
static void draw_flying_squirrel(cairo_t *cr, gdouble w1, gdouble h1)
  {
    //No squirrels where hurt while testing.
    gint i=0;

    //The flying_squirrel.
    cairo_move_to(cr, flying_squirrel_rotated[0][4]*w1, flying_squirrel_rotated[0][5]*h1);
    for(i=1;i<14;i++)
      {
        cairo_curve_to(cr, flying_squirrel_rotated[i][0]*w1, flying_squirrel_rotated[i][1]*h1, flying_squirrel_rotated[i][2]*w1, flying_squirrel_rotated[i][3]*h1, flying_squirrel_rotated[i][4]*w1, flying_squirrel_rotated[i][5]*h1);
      }
    cairo_close_path(cr);
    cairo_stroke_preserve(cr);
    
    if(ref_r[2]>=0.0)
      {
        //Fill top of squirrel with brown gradient.
        cairo_save(cr);
        cairo_matrix_t matrix1;
        cairo_matrix_init(&matrix1, rotations[0], rotations[1], rotations[2], rotations[3], 0.0, 0.0);
        cairo_transform(cr, &matrix1);
        cairo_pattern_t *pattern1=cairo_pattern_create_linear(-4.0*w1, 0.0, 4.0*w1, 0.0); 
        cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 1.0, 0.9, 0.8, 1.0); 
        cairo_pattern_add_color_stop_rgba(pattern1, 0.5, 0.7, 0.4, 0.2, 1.0);
        cairo_pattern_add_color_stop_rgba(pattern1, 1.0, 1.0, 0.9, 0.8, 1.0);  
        cairo_set_source(cr, pattern1); 
        cairo_fill(cr);
        cairo_pattern_destroy(pattern1);
        cairo_restore(cr);
    
        //The eyes.
        cairo_set_line_width(cr, 5);
        cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
        cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
        cairo_move_to(cr, flying_squirrel_rotated[14][4]*w1, flying_squirrel_rotated[14][5]*h1);
        cairo_line_to(cr, flying_squirrel_rotated[14][4]*w1, flying_squirrel_rotated[14][5]*h1);
        cairo_stroke(cr);
        cairo_move_to(cr, flying_squirrel_rotated[15][4]*w1, flying_squirrel_rotated[15][5]*h1);
        cairo_line_to(cr, flying_squirrel_rotated[15][4]*w1, flying_squirrel_rotated[15][5]*h1);
        cairo_stroke(cr);
      }
    else
      { 
        //Fill bottom of squirrel with grey gradient.
        cairo_matrix_t matrix1;
        cairo_matrix_init(&matrix1, rotations[0], rotations[1], rotations[2], rotations[3], 0.0, 0.0);
        cairo_transform(cr, &matrix1);
        cairo_pattern_t *pattern1=cairo_pattern_create_linear(-4.0*w1, 0.0, 4.0*w1, 0.0);  
        cairo_pattern_add_color_stop_rgba(pattern1, 0.0, 0.9, 0.9, 0.9, 1.0); 
        cairo_pattern_add_color_stop_rgba(pattern1, 0.5, 0.6, 0.6, 0.6, 1.0);
        cairo_pattern_add_color_stop_rgba(pattern1, 1.0, 0.9, 0.9, 0.9, 1.0);  
        cairo_set_source(cr, pattern1); 
        cairo_fill(cr);
        cairo_pattern_destroy(pattern1);
      } 
  }
static void quaternion_rotation(GtkWidget *da)
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate a plane with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    VeloGyroPrivate *priv=VELO_GYRO_GET_PRIVATE(da);
    gint i=0;
    gdouble yaw_r=(priv->yaw*G_PI/180.0);
    gdouble roll_r=(priv->roll*G_PI/180.0);
    gdouble pitch_r=(priv->pitch*G_PI/180.0);    
    gdouble qi=0;
    gdouble qj=0;
    gdouble qk=0;
    gdouble qr=0;
    gdouble r1=0;
    gdouble r2=0;
    gdouble r3=0;
    gdouble r4=0;
    gdouble r5=0;
    gdouble r6=0;
    gdouble r7=0;
    gdouble r8=0;
    gdouble r9=0;

    qi=sin(pitch_r/2.0)*cos(roll_r/2.0)*cos(yaw_r/2.0)-cos(pitch_r/2.0)*sin(roll_r/2.0)*sin(yaw_r/2.0);
    qj=cos(pitch_r/2.0)*sin(roll_r/2.0)*cos(yaw_r/2.0)+sin(pitch_r/2.0)*cos(roll_r/2.0)*sin(yaw_r/2.0);
    qk=cos(pitch_r/2.0)*cos(roll_r/2.0)*sin(yaw_r/2.0)-sin(pitch_r/2.0)*sin(roll_r/2.0)*cos(yaw_r/2.0);
    qr=cos(pitch_r/2.0)*cos(roll_r/2.0)*cos(yaw_r/2.0)+sin(pitch_r/2.0)*sin(roll_r/2.0)*sin(yaw_r/2.0);

    r1=1-2.0*qj*qj-2.0*qk*qk;
    r2=2.0*(qi*qj-qk*qr);
    r3=2.0*(qi*qk+qj*qr);
    r4=2.0*(qi*qj+qk*qr);
    r5=1.0-2.0*qi*qi-2.0*qk*qk;
    r6=2.0*(qj*qk-qi*qr);
    r7=2.0*(qi*qk-qj*qr);
    r8=2.0*(qj*qk+qi*qr);
    r9=1.0-2.0*(qi*qi+qj*qj);

    //Rotate reference point for the top and bottom of the drawings. Just need z.
    ref_r[2]=(ref[0]*r7)+(ref[1]*r8)+(ref[2]*r9);
    
    //Rotate the drawing.
    if(priv->drawing_id==0)
      {
        for(i=0;i<11;i++)
          {
            plane_rotated[i][0]=(plane_coords[i][0]*r1)+(plane_coords[i][1]*r2);
            plane_rotated[i][1]=(plane_coords[i][0]*r4)+(plane_coords[i][1]*r5);
          }

        for(i=11;i<19;i++)
          {
            plane_rotated[i][0]=(plane_coords[i][0]*r1)+(plane_coords[i][1]*r2)+(plane_coords[i][2]*r3);
            plane_rotated[i][1]=(plane_coords[i][0]*r4)+(plane_coords[i][1]*r5)+(plane_coords[i][2]*r6);
          }
      }
    else if(priv->drawing_id==1)
      {
        for(i=0;i<26;i++)
          {
            raptor_rotated[i][0]=(raptor_coords[i][0]*r1)+(raptor_coords[i][1]*r2);
            raptor_rotated[i][1]=(raptor_coords[i][0]*r4)+(raptor_coords[i][1]*r5);
            raptor_rotated[i][2]=(raptor_coords[i][2]*r1)+(raptor_coords[i][3]*r2);
            raptor_rotated[i][3]=(raptor_coords[i][2]*r4)+(raptor_coords[i][3]*r5);
            raptor_rotated[i][4]=(raptor_coords[i][4]*r1)+(raptor_coords[i][5]*r2);
            raptor_rotated[i][5]=(raptor_coords[i][4]*r4)+(raptor_coords[i][5]*r5);
          }
      }
    else
      {
        rotations[0]=r1;
        rotations[1]=r4;
        rotations[2]=r2;
        rotations[3]=r5;

        for(i=0;i<16;i++)
          {
            flying_squirrel_rotated[i][0]=(flying_squirrel_coords[i][0]*r1)+(flying_squirrel_coords[i][1]*r2);
            flying_squirrel_rotated[i][1]=(flying_squirrel_coords[i][0]*r4)+(flying_squirrel_coords[i][1]*r5);
            flying_squirrel_rotated[i][2]=(flying_squirrel_coords[i][2]*r1)+(flying_squirrel_coords[i][3]*r2);
            flying_squirrel_rotated[i][3]=(flying_squirrel_coords[i][2]*r4)+(flying_squirrel_coords[i][3]*r5);
            flying_squirrel_rotated[i][4]=(flying_squirrel_coords[i][4]*r1)+(flying_squirrel_coords[i][5]*r2);
            flying_squirrel_rotated[i][5]=(flying_squirrel_coords[i][4]*r4)+(flying_squirrel_coords[i][5]*r5);
          }
      }

    //Rotate the circles.
    struct arc *c1=&g_array_index(priv->circle1, struct arc, 0);
    struct arc *c2=&g_array_index(priv->circle2, struct arc, 0);
    struct arc *c3=&g_array_index(priv->circle3, struct arc, 0);
    struct arc *p1=&g_array_index(priv->circle1_r, struct arc, 0);
    struct arc *p2=&g_array_index(priv->circle2_r, struct arc, 0);
    struct arc *p3=&g_array_index(priv->circle3_r, struct arc, 0);
    for(i=0;i<(priv->circle1->len);i++)
      {
        (p1->a1).x=((c1->a1).x*r1)+((c1->a1).y*r2);
        (p1->a1).y=((c1->a1).x*r4)+((c1->a1).y*r5);
        (p1->a2).x=((c1->a2).x*r1)+((c1->a2).y*r2);
        (p1->a2).y=((c1->a2).x*r4)+((c1->a2).y*r5);
        (p1->a3).x=((c1->a3).x*r1)+((c1->a3).y*r2);
        (p1->a3).y=((c1->a3).x*r4)+((c1->a3).y*r5);
        (p1->a4).x=((c1->a4).x*r1)+((c1->a4).y*r2);
        (p1->a4).y=((c1->a4).x*r4)+((c1->a4).y*r5);
        c1++;p1++;
      }

    for(i=0;i<(priv->circle2->len);i++)
      {
        (p2->a1).x=((c2->a1).y*r2)+((c2->a1).z*r3);
        (p2->a1).y=((c2->a1).y*r5)+((c2->a1).z*r6);
        (p2->a2).x=((c2->a2).y*r2)+((c2->a2).z*r3);
        (p2->a2).y=((c2->a2).y*r5)+((c2->a2).z*r6);
        (p2->a3).x=((c2->a3).y*r2)+((c2->a3).z*r3);
        (p2->a3).y=((c2->a3).y*r5)+((c2->a3).z*r6);
        (p2->a4).x=((c2->a4).y*r2)+((c2->a4).z*r3);
        (p2->a4).y=((c2->a4).y*r5)+((c2->a4).z*r6);     
        c2++;p2++;
      }

    for(i=0;i<(priv->circle3->len);i++)
      {
        (p3->a1).x=((c3->a1).x*r1)+((c3->a1).z*r3);
        (p3->a1).y=((c3->a1).x*r4)+((c3->a1).z*r6);
        (p3->a2).x=((c3->a2).x*r1)+((c3->a2).z*r3);
        (p3->a2).y=((c3->a2).x*r4)+((c3->a2).z*r6);
        (p3->a3).x=((c3->a3).x*r1)+((c3->a3).z*r3);
        (p3->a3).y=((c3->a3).x*r4)+((c3->a3).z*r6);
        (p3->a4).x=((c3->a4).x*r1)+((c3->a4).z*r3);
        (p3->a4).y=((c3->a4).x*r4)+((c3->a4).z*r6);
        c3++;p3++;
      }

  }
static GArray* get_bezier_ring(gdouble radius, gdouble points_per_ring)
  {
    gint i=0;
    gdouble arc_top=-G_PI/points_per_ring;
    gdouble arc_offset=-2.0*G_PI/points_per_ring;
    gdouble start_x=radius*cos(arc_top);
    gdouble start_y=radius*sin(arc_top);
    gdouble end_x=start_x;
    gdouble end_y=-start_y;
    gdouble c1_x=(4.0*radius-start_x)/3.0;
    gdouble c1_y=((radius-start_x)*(3.0*radius-start_x))/(3.0*start_y);
    gdouble c2_x=c1_x;
    gdouble c2_y=-c1_y;
    gdouble offset1=0.0;
    gdouble offset2=0.0;

    GArray *one_ring=g_array_sized_new(FALSE, FALSE, sizeof(struct arc), points_per_ring);
    g_array_set_size(one_ring, points_per_ring);
    struct arc *p1=&g_array_index(one_ring, struct arc, 0);
    struct arc *p2=&g_array_index(one_ring, struct arc, 1);

    //Rotate the points in the section to align with the zero point on the unit circle.
    offset1=cos(arc_top);
    offset2=sin(arc_top);
    (p1->a4).x=start_x*offset1-start_y*offset2;
    (p1->a4).y=start_x*offset2+start_y*offset1;
    (p1->a4).z=0.0;
    (p1->a3).x=c1_x*offset1-c1_y*offset2;
    (p1->a3).y=c1_x*offset2+c1_y*offset1;
    (p1->a3).z=0.0;
    (p1->a2).x=c2_x*offset1-c2_y*offset2;
    (p1->a2).y=c2_x*offset2+c2_y*offset1;
    (p1->a2).z=0.0;
    (p1->a1).x=end_x*offset1-end_y*offset2;
    (p1->a1).y=end_x*offset2+end_y*offset1;
    (p1->a1).z=0.0;

    //Add the rest of the sections.
    offset1=cos(arc_offset);
    offset2=sin(arc_offset);
    for(i=1;i<points_per_ring;i++)
      {
        (p2->a1).x=(p1->a1).x*offset1-(p1->a1).y*offset2;
        (p2->a1).y=(p1->a1).x*offset2+(p1->a1).y*offset1;
        (p2->a1).z=0.0;
        (p2->a2).x=(p1->a2).x*offset1-(p1->a2).y*offset2;
        (p2->a2).y=(p1->a2).x*offset2+(p1->a2).y*offset1;
        (p2->a2).z=0.0;
        (p2->a3).x=(p1->a3).x*offset1-(p1->a3).y*offset2;
        (p2->a3).y=(p1->a3).x*offset2+(p1->a3).y*offset1;
        (p2->a3).z=0.0;
        (p2->a4).x=(p1->a4).x*offset1-(p1->a4).y*offset2;
        (p2->a4).y=(p1->a4).x*offset2+(p1->a4).y*offset1;
        (p2->a4).z=0.0;
        p1++;p2++;
      }

    return one_ring;
  }



















