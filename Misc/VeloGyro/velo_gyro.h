
/*

    For use with velo_gyro.c. Look in velo_gyro_main.c for more information.

    C. Eric Cashon

*/

#ifndef __VELO_GYRO_H__
#define __VELO_GYRO_H__

#include<gtk/gtk.h>

G_BEGIN_DECLS

/**
 * VeloGyro:
 *
 * A Gyro structure.
 *
 */
struct _VeloGyro
{
  /*< private >*/
  GtkDrawingArea da;
};

#define VELO_GYRO_TYPE velo_gyro_get_type()

G_DECLARE_FINAL_TYPE(VeloGyro, velo_gyro, VELO, GYRO, GtkDrawingArea)

//Different center drawings of the gyro.
enum
{
  STEALTH_PLANE1,
  RAPTOR1,
  FLYING_SQUIRREL
};

//Public functions.
GtkWidget* velo_gyro_new();
void velo_gyro_set_drawing(VeloGyro *da, gint drawing_id);
gint velo_gyro_get_drawing(VeloGyro *da);
void velo_gyro_set_draw_square(VeloGyro *da, gboolean draw_square);
gboolean velo_gyro_get_draw_square(VeloGyro *da);
void velo_gyro_set_background_color(VeloGyro *da, const gchar *bc_string);
const gchar* velo_gyro_get_background_color(VeloGyro *da);
void velo_gyro_set_rotations(VeloGyro *da, gdouble yaw, gdouble roll, gdouble pitch);
void velo_gyro_set_pitch(VeloGyro *da, gdouble pitch);
void velo_gyro_set_roll(VeloGyro *da, gdouble roll);
void velo_gyro_set_yaw(VeloGyro *da, gdouble yaw);
void velo_gyro_show_text(VeloGyro *da, gboolean show_text);
gdouble velo_gyro_get_pitch(VeloGyro *da);
gdouble velo_gyro_get_roll(VeloGyro *da);
gdouble velo_gyro_get_yaw(VeloGyro *da);

G_END_DECLS

#endif 
