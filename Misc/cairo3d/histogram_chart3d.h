/*
    See histogram_chart3d_main.c for the test program.
*/

#include<glib.h>
#include<cairo.h>
#include<math.h>
#include<string.h>
#include<stdlib.h>


struct histo_point3d{
  gdouble x;
  gdouble y;
  gdouble z;
};

//For drawing chart points with cairo.
enum{ 
  C_MOVE_TO_R,
  C_MOVE_TO_L,
  C_LINE_TO,
  C_CURVE_TO,
  C_LINE_TO_STROKE,
  C_CURVE_TO_STROKE
};

struct chart_point3d{
  gdouble x;
  gdouble y;
  gdouble z;
  gint move_id;
};

struct color{
  gdouble r;
  gdouble g;
  gdouble b;
  gdouble a;
};

/*
  For drawing a bezier ring. A little wasteful to have a color for each arc to color a cylinder
  but it works.
*/
struct histo_arc{
  gint id;
  struct histo_point3d a1;
  struct histo_point3d a2;
  struct histo_point3d a3;
  struct histo_point3d a4;
  struct color cl;
};

/*
  A definition for sorting cylinders with two rings of 8 before drawing on the chart. If
8 point rings are not good enough then a different struct along with a qsort compare function
needs to be used. Use aligned memory for this.
*/
struct cylinder16{
  struct histo_arc ar1;
  struct histo_arc ar2;
  struct histo_arc ar3;
  struct histo_arc ar4;
  struct histo_arc ar5;
  struct histo_arc ar6;
  struct histo_arc ar7;
  struct histo_arc ar8;
  struct histo_arc ar9;
  struct histo_arc ar10;
  struct histo_arc ar11;
  struct histo_arc ar12;
  struct histo_arc ar13;
  struct histo_arc ar14;
  struct histo_arc ar15;
  struct histo_arc ar16;
};

//For sorting 24 arcs or 2 rings of 12 for the cylinder.
struct cylinder24{
  struct histo_arc ar1;
  struct histo_arc ar2;
  struct histo_arc ar3;
  struct histo_arc ar4;
  struct histo_arc ar5;
  struct histo_arc ar6;
  struct histo_arc ar7;
  struct histo_arc ar8;
  struct histo_arc ar9;
  struct histo_arc ar10;
  struct histo_arc ar11;
  struct histo_arc ar12;
  struct histo_arc ar13;
  struct histo_arc ar14;
  struct histo_arc ar15;
  struct histo_arc ar16;
  struct histo_arc ar17;
  struct histo_arc ar18;
  struct histo_arc ar19;
  struct histo_arc ar20;
  struct histo_arc ar21;
  struct histo_arc ar22;
  struct histo_arc ar23;
  struct histo_arc ar24;
};

//For getting bezier control points for an array of point3d's. Just need the 2d points.
struct histo_controls2d{
  gdouble x1;
  gdouble y1;
  gdouble x2;
  gdouble y2;
};

struct rectangle3d{
  struct histo_point3d b1;
  struct histo_point3d b2;
  struct histo_point3d b3;
  struct histo_point3d b4;
  struct color cl;
};

struct cube3d{
  gint id;
  struct histo_point3d c1;
  struct histo_point3d c2;
  struct histo_point3d c3;
  struct histo_point3d c4;
  struct histo_point3d c5;
  struct histo_point3d c6;
  struct histo_point3d c7;
  struct histo_point3d c8;
  struct color cl;
};

//Basic layout for 2d and 3d charting.
struct histogram_chart{ 
   guint rows;
   guint columns; 
   gdouble outside_square;
   GArray *base;
   GArray *base_t;
   //The z height of the side and back rows.
   gdouble back_height;
   guint back_rows;
   gdouble back_color[4];
   //Reference vectors for x, y and z axis.
   gdouble rx[3];
   gdouble rx_t[3];
   gdouble ry[3];
   gdouble ry_t[3];
   gdouble rz[3];
   gdouble rz_t[3];
   //The three z corners of the chart.
   gdouble c1[3];
   gdouble c1_t[3];
   gdouble c2[3];
   gdouble c2_t[3];
   gdouble c3[3];
   gdouble c3_t[3];
   //Labels for the chart.
   GPtrArray *labels_xr;
   GPtrArray *labels_yr;
   GPtrArray *labels_xl;
   GPtrArray *labels_yl;
   GPtrArray *labels_zr;
};

//Data point arrays for the chart. Separate the data from the chart. Test various shapes.
struct chart_data{
   gint data_id;
   gdouble max_z;
   GArray *d;
   GArray *d_t;
};

//For the cylinders. The cylinders need aligned memory for the sorts of different block sizes.
struct chart_data_aligned{
   gint data_id;
   gint ring_size;
   gint len;
   gdouble max_z;
   struct histo_arc *d;
   struct histo_arc *d_t;
};

//Groups of chart data to draw multiple lines on a chart.
struct chart_data_group{
   gdouble line_color[4];
   gdouble point_color[4];
   gdouble line_width;
   struct chart_data *cd;
};


//Position the histogram cube.
enum{ 
  CUBE_UPPER_LEFT,
  CUBE_CENTER
};

//Draw options for histogram cubes.
enum{ 
  SOLID_CUBES,
  GRADIENT_CUBES,
  GRADIENT_CUBES_MESH,
  GRADIENT_CUBES2,
  GRADIENT_CUBES2_MESH
};

//Draw options for histogram cylinders.
enum{ 
  SOLID_CYLINDERS,
  GRADIENT_CYLINDERS,
  GRADIENT_CYLINDERS_MESH
};

//Draw options for data points.
enum{ 
  DRAW_POINTS,
  DRAW_LINES,
  DRAW_SMOOTH
};

//Draw options for font position.
enum{ 
  FONT_START,
  FONT_MIDDLE,
};

//A new basic chart. This is the bottom checkerboard and sides of the chart.
struct histogram_chart* histogram_chart_new(guint rows, guint columns, gdouble outside_square);
void histogram_chart_set_base_color(struct histogram_chart *hc, guint index, gdouble rgba[4]);
void histogram_chart_set_base_color_all(struct histogram_chart *hc, gdouble rgba[4]);
void histogram_chart_set_back_height(struct histogram_chart *hc, gdouble height);
void histogram_chart_append_label_xr(struct histogram_chart *hc, const gchar *label);
void histogram_chart_append_label_yr(struct histogram_chart *hc, const gchar *label);
void histogram_chart_append_label_xl(struct histogram_chart *hc, const gchar *label);
void histogram_chart_append_label_yl(struct histogram_chart *hc, const gchar *label);
void histogram_chart_append_label_zr(struct histogram_chart *hc, const gchar *label);
void histogram_chart_clear_labels(struct histogram_chart *hc);
void histogram_chart_free(struct histogram_chart *hc);
//Draw the basic chart.
void histogram_chart_draw_base(struct histogram_chart *hc, cairo_t *cr);
void histogram_chart_draw_base_lines(struct histogram_chart *hc, cairo_t *cr);
void histogram_chart_draw_labels_xr(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[16], guint font_size, guint font_position);
void histogram_chart_draw_labels_yr(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[16], guint font_size, guint font_position);
void histogram_chart_draw_labels_xl(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[16], guint font_size, guint font_position);
void histogram_chart_draw_labels_yl(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[16], guint font_size, guint font_position);
void histogram_chart_draw_labels_zr(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[16], guint font_size, guint font_position);
void histogram_chart_draw_back(struct histogram_chart *hc, cairo_t *cr);
void histogram_chart_draw_side(struct histogram_chart *hc, cairo_t *cr);
void histogram_chart_reset_base();
void histogram_chart_rotate_base(struct histogram_chart *hc, gdouble qrs[16]);
//A new cube chart. The cube data to draw.
struct chart_data* chart_cubes_new(struct histogram_chart *hc, gdouble inside_square, guint inside_square_position);
void cube_chart_set_bar_color(struct chart_data *cd, guint index, gdouble rgba[4]);
void cube_chart_set_bar_height(struct chart_data *cd, guint index, gdouble height);
double cube_chart_get_bar_height(struct chart_data *cd, guint index);
double cube_chart_get_max_z(struct chart_data *cd);
void cube_chart_free(struct chart_data *cd);
//A new cylinder chart. The cylinder data to draw.
struct chart_data_aligned* chart_cylinders_new(struct histogram_chart *hc, gint sections, gdouble radius);
void cylinder_chart_set_bar_color(struct chart_data_aligned *cd, guint index, gdouble rgba[4]);
void cylinder_chart_set_bar_height(struct chart_data_aligned *cd, guint index, gdouble height);
double cylinder_chart_get_bar_height(struct chart_data_aligned *cd, guint index);
double cylinder_chart_get_max_z(struct chart_data_aligned *cd);
void cylinder_chart_free(struct chart_data_aligned *cd);
//Set gradient colors.
void set_gradient_color_bottom(gdouble r, gdouble g, gdouble b);
void set_gradient_color_middle(gdouble r, gdouble g, gdouble b);
void set_gradient_color_top(gdouble r, gdouble g, gdouble b);
//Draw the bars or cubes.
void cube_chart_draw(struct histogram_chart *hc, struct chart_data *cd, cairo_t *cr, gdouble qrs[16], guint draw_type, guint font_size, guint font_position);
//Line, smooth line and points charts.
void chart_data_draw(struct histogram_chart *hc, struct chart_data *cd, cairo_t *cr, gdouble qrs[16], gint draw_option, gdouble line_color[4], gdouble point_color[4], gdouble line_width, guint font_size, guint font_position);
void chart_data_draw_groups(struct histogram_chart *hc, GPtrArray *chart_groups, cairo_t *cr, gdouble qrs[16], gint draw_option, guint font_size, guint font_position);
//Draw the cylinders.
void chart_data_draw_aligned(struct histogram_chart *hc, struct chart_data_aligned *cd, cairo_t *cr, gdouble qrs[16], gint draw_option, guint font_size, guint font_position);
//For setting the drawing projection.
void set_projection_matrix(gdouble qrs[16], gdouble angle, gdouble near, gdouble far);
void histo_quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[16]);
void histo_matrix_multiply(const gdouble in1[16], const gdouble in2[16], gdouble out[16]);
//Set up chart groups for drawing data from a key file.
void set_chart_data_groups(GPtrArray *chart_groups, const gchar *key_file_string);
void free_chart_data_groups(GPtrArray *chart_groups);
void clear_chart_data_groups(GPtrArray *chart_groups);



