
/*
    Draw a mesh that fits together like tiles. Be able to dynamically change the curves, colors and
control points. Similar to tensor_product1.c but you can draw the mesh dynamically. 
Draw a t-shirt, sock, fish, butterfly, rooster, penguin and chameleon with the tiled mesh pattern.
Rotate the drawing in 3d. Some tensor product patch mesh clothing and critter design.

    gcc -Wall mesh_maker.c -o mesh_maker `pkg-config --cflags --libs gtk+-3.0` -lm

    Test GTK_THEME=Adwaita:dark ./mesh_maker or GTK_THEME=Adwaita:light ./mesh_maker

    Tested on Ubuntu20.04 and GTK3.24

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<cairo-svg.h>
#include<cairo-pdf.h>
#include<cairo-ps.h>

static gboolean start_drawing(GtkWidget *widget, cairo_t *cr, gpointer data);
static void draw_grids(GtkWidget *widget, cairo_t *cr);
static void draw_grid1(cairo_t *cr, gdouble width, gdouble height);
static void draw_grid2(cairo_t *cr, gdouble width, gdouble height);
static void draw_grid3(cairo_t *cr, gdouble width, gdouble height);
static void draw_shapes(GtkWidget *widget, cairo_t *cr);
static void draw_mesh(cairo_t *cr, gdouble width, gdouble height);
static void draw_background_mesh(cairo_t *cr, gdouble drawing_width, gdouble drawing_height);
static void draw_t_shirt(cairo_t *cr, gdouble width, gdouble height);
static void draw_sock(cairo_t *cr, gdouble width, gdouble height);
static void draw_fish(cairo_t *cr, gdouble width, gdouble height);
static void draw_butterfly(cairo_t *cr, gdouble width, gdouble height);
static void draw_rooster(cairo_t *cr, gdouble width, gdouble height);
static void draw_penguin(cairo_t *cr, gdouble width, gdouble height);
static void draw_penguin_head(cairo_t *cr, gdouble w1, gdouble h1);
static void draw_chameleon(cairo_t *cr, gdouble width, gdouble height);
static gboolean start_press(GtkWidget *widget, GdkEvent *event, gpointer data);
static gboolean stop_press(GtkWidget *widget, GdkEvent *event, gpointer data);
static gboolean cursor_motion(GtkWidget *widget, GdkEvent *event, gpointer data);
static void combo1_changed(GtkComboBox *combo1, gpointer data);
static void set_standard_mesh(GtkToggleButton *check_standard, gpointer data);
static void combo2_changed(GtkComboBox *combo2, gpointer *combos);
static void combo3_changed(GtkComboBox *combo3, gpointer data);
static void check_colors(GtkWidget *widget, GtkWidget **colors);
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data);
static gchar* save_file(gpointer *widgets, gint surface_type);
static void draw_svg(GtkWidget *widget, gpointer *widgets);
static void draw_pdf(GtkWidget *widget, gpointer *widgets);
static void draw_ps(GtkWidget *widget, gpointer *widgets);
static void draw_png(GtkWidget *widget, gpointer *widgets);
static void draw_surface(cairo_t *cr, gpointer *widgets,guint surface_type);
//About dialog and icon.
static void about_dialog(GtkWidget *widget, gpointer data);
static GdkPixbuf* draw_icon();
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void scale_mesh_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9]);
static void translate_x_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void translate_y_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void layout_width_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void layout_height_spin_changed(GtkSpinButton *spin_button, gpointer data);

/*
  Used for calculating points in .svg, .pdf and .ps output.
  Why 90. This works on the test computer. When a .svg is saved and then opened, the pixels
match the original drawing. Rsvg rsvg_handle_set_dpi() notes "Common values are 75, 90,
and 300 DPI." which it looks like 90 is being used as default to open the picture. 
  The ppi might need to be changed for matching ouput on different computers.
*/
static gdouble ppi=90.0;
//For blocking motion signal. Block when not drawing top rectangle
static gint motion_id=0;
//Coordinates for the drawing rectangle.
static gdouble rect[]={0.0, 0.0, 0.0, 0.0};
//Control points for the "box" lines.
static gdouble mesh[]={1.0, 3.0, 3.0, 4.0, 3.0, 3.0, 4.0, 1.0};
//Inside control points P0, P1, P2, P3.
static gdouble mesh_p[]={6.0, 3.0, 7.0, 3.0, 6.0, 4.0, 7.0, 4.0};
//Combo row.
static gint mesh_combo=0;
static gint tile_combo=0;
static gint drawing_combo=0;
static gboolean grid_combo=TRUE;
static gdouble scale=1.0;
static gdouble scale_mesh=1.0;
static gboolean default_drawing=TRUE;
//Drawing background color.
static gdouble b1[]={1.0, 1.0, 1.0, 1.0};
//Bezier curve control point colors.
static gdouble c0[]={1.0, 0.0, 0.0, 1.0};
static gdouble c1[]={1.0, 0.0, 1.0, 1.0};
static gdouble c2[]={0.0, 1.0, 0.0, 1.0};
static gdouble c3[]={0.0, 0.0, 1.0, 1.0};
//The combo rotation slider values.
static gdouble combo_pitch=0.0;
static gdouble combo_roll=0.0;
static gdouble combo_yaw=0.0;
static gdouble layout_width=500.0;
static gdouble layout_height=500.0;
static gdouble translate_x=0.0;
static gdouble translate_y=0.0;
static gdouble drawing_width=500.0;
static gdouble drawing_height=500.0;

int main(int argc, char *argv[])
  {
    gtk_init (&argc, &argv);

    GtkWidget *window=gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Mesh Maker");
    gtk_window_set_default_size(GTK_WINDOW(window), 1000, 650);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    gtk_widget_set_app_paintable(window, TRUE);
    //Try to set transparency of main window.
    GdkScreen *screen=gtk_widget_get_screen(window);  
    if(gdk_screen_is_composited(screen))
      {
        GdkVisual *visual=gdk_screen_get_rgba_visual(screen);
        gtk_widget_set_visual(window, visual);
      }
    else g_print("Can't set window transparency.\n");

    //Draw a 256x256 program icon. A butterfly with 2x2 mesh tiles.
    default_drawing=FALSE;
    rect[2]=0.1*256.0;
    rect[3]=0.3*256.0;
    GdkPixbuf *icon=draw_icon();
    gtk_window_set_default_icon(icon);
    default_drawing=TRUE;
    
    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);
    //Add some extra events to the top drawing area.
    gtk_widget_add_events(da, GDK_BUTTON_PRESS_MASK|GDK_BUTTON_RELEASE_MASK|GDK_POINTER_MOTION_MASK);
    g_signal_connect(da, "draw", G_CALLBACK(start_drawing), NULL);
    g_signal_connect(da, "button-press-event", G_CALLBACK(start_press), NULL);
    g_signal_connect(da, "button-release-event", G_CALLBACK(stop_press), NULL);
    motion_id=g_signal_connect(da, "motion-notify-event", G_CALLBACK(cursor_motion), NULL);
    g_signal_handler_block(da, motion_id);

    GtkWidget *combo1=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo1, TRUE);
    gtk_widget_set_vexpand(combo1, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo1), 0, "1", "Drag Point 1");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo1), 1, "2", "Drag Point 2");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo1), 2, "3", "Drag Point 3");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo1), 3, "4", "Drag Point 4");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo1), 4, "5", "Drag Point A");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo1), 5, "6", "Drag Point B");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo1), 6, "7", "Drag Point C");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo1), 7, "8", "Drag Point D");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo1), 0);
    g_signal_connect(combo1, "changed", G_CALLBACK(combo1_changed), NULL);

    GtkWidget *check_standard=gtk_check_button_new_with_label("Lock Standard Mesh");
    gtk_widget_set_halign(check_standard, GTK_ALIGN_CENTER);
    gtk_widget_set_hexpand(check_standard, FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_standard), TRUE);
    g_signal_connect(check_standard, "toggled", G_CALLBACK(set_standard_mesh), da);

    GtkWidget *combo2=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo2, TRUE);
    gtk_widget_set_vexpand(combo2, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo2), 0, "1", "Tiled Mesh 2x2");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo2), 1, "2", "Tiled Mesh 4x4");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo2), 2, "3", "Tiled Mesh 8x8");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo2), 3, "4", "Tiled Mesh 16x16");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo2), 0);

    GtkWidget *combo3=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo3, TRUE);
    gtk_widget_set_vexpand(combo3, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo3), 0, "1", "No Clip");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo3), 1, "2", "Draw t-shirt");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo3), 2, "3", "Draw Sock");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo3), 3, "4", "Draw Fish");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo3), 4, "5", "Draw Butterfly");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo3), 5, "6", "Draw Rooster");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo3), 6, "7", "Draw Penguin");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo3), 7, "8", "Draw Chameleon");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo3), 0);
    
    g_signal_connect(combo3, "changed", G_CALLBACK(combo3_changed), da);
    gpointer combos[2]={da, combo3};
    g_signal_connect(combo2, "changed", G_CALLBACK(combo2_changed), combos);

    GtkWidget *label1=gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label1), "<span font_weight='heavy'> C0 </span>");
    GtkWidget *label2=gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label2), "<span font_weight='heavy'> C1 </span>");
    GtkWidget *label3=gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label3), "<span font_weight='heavy'> C2 </span>");
    GtkWidget *label4=gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label4), "<span font_weight='heavy'> C3 </span>");
    GtkWidget *label5=gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label5), "<span font_weight='heavy'>Background </span>");

    GtkWidget *entry1=gtk_entry_new();
    gtk_widget_set_hexpand(entry1, TRUE);
    gtk_entry_set_text(GTK_ENTRY(entry1), "rgba(255, 0, 0, 1.0)");

    GtkWidget *entry2=gtk_entry_new();
    gtk_widget_set_hexpand(entry2, TRUE);
    gtk_entry_set_text(GTK_ENTRY(entry2), "rgba(255, 0, 255, 1.0)");

    GtkWidget *entry3=gtk_entry_new();
    gtk_widget_set_hexpand(entry3, TRUE);
    gtk_entry_set_text(GTK_ENTRY(entry3), "rgba(0, 255, 0, 1.0)");

    GtkWidget *entry4=gtk_entry_new();
    gtk_widget_set_hexpand(entry4, TRUE);
    gtk_entry_set_text(GTK_ENTRY(entry4), "rgba(0, 0, 255, 1.0)");

    GtkWidget *entry5=gtk_entry_new();
    gtk_widget_set_hexpand(entry5, TRUE);
    gtk_entry_set_text(GTK_ENTRY(entry5), "rgba(255, 255, 255, 1.0)");

    GtkWidget *button1=gtk_button_new_with_label("Update Colors");
    gtk_widget_set_hexpand(button1, FALSE);
    GtkWidget *colors[]={entry1, entry2, entry3, entry4, entry5, window, da};
    g_signal_connect(button1, "clicked", G_CALLBACK(check_colors), colors);

    GtkWidget *menu1=gtk_menu_new();
    GtkWidget *menu1item1=gtk_menu_item_new_with_label("Save as .svg");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item1);
    GtkWidget *menu1item2=gtk_menu_item_new_with_label("Save as .pdf");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item2);
    GtkWidget *menu1item3=gtk_menu_item_new_with_label("Save as .ps");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item3);
    GtkWidget *menu1item4=gtk_menu_item_new_with_label("Save as .png");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item4);
    GtkWidget *title1=gtk_menu_item_new_with_label("Save Drawing");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(title1), menu1);

    GtkWidget *menu2=gtk_menu_new();
    GtkWidget *menu2item1=gtk_menu_item_new_with_label("Mesh Maker");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu2), menu2item1);
    GtkWidget *title2=gtk_menu_item_new_with_label("About");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(title2), menu2);

    GtkWidget *menu_bar=gtk_menu_bar_new();
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), title1);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), title2);

    gpointer widgets[2]={da, window};
    g_signal_connect(menu1item1, "activate", G_CALLBACK(draw_svg), widgets);
    g_signal_connect(menu1item2, "activate", G_CALLBACK(draw_pdf), widgets);
    g_signal_connect(menu1item3, "activate", G_CALLBACK(draw_ps), widgets);
    g_signal_connect(menu1item4, "activate", G_CALLBACK(draw_png), widgets);
    g_signal_connect(menu2item1, "activate", G_CALLBACK(about_dialog), window);
    
    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo1, 0, 0, 2, 1);    
    gtk_grid_attach(GTK_GRID(grid1), label5, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), entry5, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), label1, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), entry1, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), label2, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), entry2, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), label3, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), entry3, 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), label4, 0, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), entry4, 1, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), button1, 0, 6, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_standard, 0, 7, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo2, 0, 8, 2, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo3, 0, 9, 2, 1);

    //Rotation tab widgets.
    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, TRUE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, TRUE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, TRUE);

    GtkWidget *scale_label=gtk_label_new("Scale");
    gtk_widget_set_hexpand(scale_label, TRUE);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider, TRUE);
    gtk_widget_set_hexpand(pitch_slider, FALSE);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider, TRUE);
    gtk_widget_set_hexpand(roll_slider, FALSE);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider, TRUE);
    gtk_widget_set_hexpand(yaw_slider, FALSE);

    GtkWidget *scale_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 2.0, 0.01);
    gtk_widget_set_vexpand(scale_slider, TRUE);
    gtk_widget_set_hexpand(scale_slider, FALSE);
    gtk_range_set_value(GTK_RANGE(scale_slider), 1.0);

    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_pitch), da);
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_roll), da);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_yaw), da);
    g_signal_connect(scale_slider, "change-value", G_CALLBACK(set_scale), da);

    GtkAdjustment *scale_mesh_adj=gtk_adjustment_new(1.0, 0.0, 2.0, 0.1, 0.0, 0.0);
    GtkWidget *scale_mesh_label=gtk_label_new("Scale Mesh Only");
    gtk_widget_set_hexpand(scale_mesh_label, FALSE);
    GtkWidget *scale_mesh_spin=gtk_spin_button_new(scale_mesh_adj, 1.0, 1);
    gtk_widget_set_hexpand(scale_mesh_spin, FALSE);
    g_signal_connect(scale_mesh_spin, "value-changed", G_CALLBACK(scale_mesh_spin_changed), da); 

    GtkWidget *grid2=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid2), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid2), 8);
    gtk_grid_attach(GTK_GRID(grid2), pitch_label, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), roll_label, 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), yaw_label, 2, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), scale_label, 3, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), pitch_slider, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), roll_slider, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), yaw_slider, 2, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), scale_slider, 3, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), scale_mesh_label, 0, 2, 3, 1);
    gtk_grid_attach(GTK_GRID(grid2), scale_mesh_spin, 0, 3, 3, 1);

    GtkAdjustment *translate_x_adj=gtk_adjustment_new(0.0, -500.0, 500.0, 10.0, 0.0, 0.0);
    GtkAdjustment *translate_y_adj=gtk_adjustment_new(0.0, -500.0, 500.0, 10.0, 0.0, 0.0);
    GtkAdjustment *layout_width_adj=gtk_adjustment_new(500.0, 1.0, 2000.0, 50.0, 0.0, 0.0);
    GtkAdjustment *layout_height_adj=gtk_adjustment_new(500.0, 1.0, 2000.0, 50.0, 0.0, 0.0); 

    GtkWidget *translate_x_label=gtk_label_new("Translate x");
    gtk_widget_set_hexpand(translate_x_label, FALSE);
    GtkWidget *translate_x_spin=gtk_spin_button_new(translate_x_adj, 0.0, 1);
    gtk_widget_set_hexpand(translate_x_spin, FALSE);
    g_signal_connect(translate_x_spin, "value-changed", G_CALLBACK(translate_x_spin_changed), da); 

    GtkWidget *translate_y_label=gtk_label_new("Translate y");
    gtk_widget_set_hexpand(translate_y_label, FALSE);
    GtkWidget *translate_y_spin=gtk_spin_button_new(translate_y_adj, 0.0, 1);
    gtk_widget_set_hexpand(translate_y_spin, FALSE);
    g_signal_connect(translate_y_spin, "value-changed", G_CALLBACK(translate_y_spin_changed), da);

    GtkWidget *layout_width_label=gtk_label_new("Layout Width");
    gtk_widget_set_hexpand(layout_width_label, FALSE);
    GtkWidget *layout_width_spin=gtk_spin_button_new(layout_width_adj, 500.0, 1);
    gtk_widget_set_hexpand(layout_width_spin, FALSE);
    g_signal_connect(layout_width_spin, "value-changed", G_CALLBACK(layout_width_spin_changed), da);

    GtkWidget *layout_height_label=gtk_label_new("Layout Height");
    gtk_widget_set_hexpand(layout_height_label, FALSE);
    GtkWidget *layout_height_spin=gtk_spin_button_new(layout_height_adj, 500.0, 1);
    gtk_widget_set_hexpand(layout_height_spin, FALSE);
    g_signal_connect(layout_height_spin, "value-changed", G_CALLBACK(layout_height_spin_changed), da);

    GtkWidget *grid3=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid3), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid3), 8);
    gtk_grid_attach(GTK_GRID(grid3), translate_x_label, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), translate_y_label, 1, 0, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid3), translate_x_spin, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), translate_y_spin, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), layout_width_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), layout_height_label, 1, 2, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid3), layout_width_spin, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), layout_height_spin, 1, 3, 1, 1);

    GtkWidget *nb_label1=gtk_label_new("Drawing");
    GtkWidget *nb_label2=gtk_label_new("Rotation");
    GtkWidget *nb_label3=gtk_label_new("Options");
    GtkWidget *notebook=gtk_notebook_new();
    gtk_container_set_border_width(GTK_CONTAINER(notebook), 15);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), grid1, nb_label1);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), grid2, nb_label2);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), grid3, nb_label3);

    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), notebook, FALSE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 375);

    GtkWidget *grid4=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid4), menu_bar, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), paned1, 0, 1, 1, 1);

    //Draw background window based on the paned window splitter.
    g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), paned1);

    gtk_container_add(GTK_CONTAINER(window), grid4);

    gchar *css_string=g_strdup("#about_dialog{background: rgba(127,127,255,1.0); font-size: 16pt; font-weight: bold;} #about_dialog label selection{background: rgba(0,50,220,0.5);}");
    GError *css_error=NULL;
    GtkCssProvider *provider=gtk_css_provider_new();
    gtk_css_provider_load_from_data(provider, css_string, -1, &css_error);
    gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    if(css_error!=NULL)
      {
        g_print("CSS loader error %s\n", css_error->message);
        g_error_free(css_error);
      }
    g_object_unref(provider);
    g_free(css_string);

    gtk_widget_show_all(window);

    //Set some initial values for the drawing area.
    gdouble width=(gdouble)gtk_widget_get_allocated_width(da);
    gdouble height=(gdouble)gtk_widget_get_allocated_height(da);
    rect[2]=0.1*width;
    rect[3]=0.3*height;
    gtk_widget_queue_draw(da);

    gtk_main();

    g_object_unref(icon);

    return 0;
  }
static gboolean start_drawing(GtkWidget *widget, cairo_t *cr, gpointer data)
  {
    cairo_set_source_rgba(cr, b1[0], b1[1], b1[2], b1[3]);
    cairo_paint(cr);

    //Layout bounding box.
    cairo_set_line_width(cr, 4.0);
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_rectangle(cr, 0.0, 0.0, layout_width, layout_height);
    cairo_stroke(cr);

    if(!default_drawing)
      {
        gdouble qrs[9];
        cairo_matrix_t matrix1;
        quaternion_rotation(-combo_yaw, combo_roll, combo_pitch, qrs);
        cairo_translate(cr, translate_x, translate_y);
        cairo_matrix_init(&matrix1, qrs[0], qrs[3], qrs[1], qrs[4], layout_width/2.0, layout_height/2.0);
        cairo_transform(cr, &matrix1); 
        cairo_scale(cr, scale+0.01, scale+0.01);
        if(grid_combo) draw_grids(widget, cr);
        else draw_shapes(widget, cr);
      }
    else
      {
        draw_mesh(cr, drawing_width, drawing_height);
      }
    
    return TRUE;
  }
static void draw_grids(GtkWidget *widget, cairo_t *cr)
  {
    if(tile_combo==0)
      {
        cairo_scale(cr, scale_mesh, scale_mesh);
        cairo_translate(cr, -drawing_width/2.0, -drawing_height/2.0);
        draw_mesh(cr, drawing_width, drawing_height);
      }
    else if(tile_combo==1)
      {
        draw_grid1(cr, drawing_width, drawing_height); 
      }
    else if(tile_combo==2)
      {
        draw_grid2(cr, drawing_width, drawing_height);
      }
    else
      {
        draw_grid3(cr, drawing_width, drawing_height);
      }
  }
static void draw_grid1(cairo_t *cr, gdouble width, gdouble height)
  {
    gint i=0;
    gint j=0;
    gdouble w1=width/10.0;
    gdouble h1=height/10.0;

    cairo_scale(cr, 0.5, 0.5);
    cairo_scale(cr, scale_mesh, scale_mesh);
    cairo_translate(cr, -8.0*w1, -8.0*h1);
    for(i=0;i<2;i++)
      {
        for(j=0;j<2;j++)
          {
            draw_mesh(cr, width, height);
            cairo_translate(cr, 6.0*w1, 0.0);
          }
        cairo_translate(cr, -12.0*w1, 6.0*h1);
      }
  }
static void draw_grid2(cairo_t *cr, gdouble width, gdouble height)
  {
    gint i=0;
    gint j=0;
    gdouble w1=width/10.0;
    gdouble h1=height/10.0;

    cairo_scale(cr, 0.25, 0.25);
    cairo_scale(cr, scale_mesh, scale_mesh);
    cairo_translate(cr, -14.0*w1, -14.0*h1);
    for(i=0;i<4;i++)
      {
        for(j=0;j<4;j++)
          {
            draw_mesh(cr, width, height);
            cairo_translate(cr, 6.0*w1, 0.0);
          }
        cairo_translate(cr, -24.0*w1, 6.0*h1);
      }
  }
static void draw_grid3(cairo_t *cr, gdouble width, gdouble height)
  {
    gint i=0;
    gint j=0;
    gdouble w1=width/10.0;
    gdouble h1=height/10.0;

    cairo_scale(cr, 0.125, 0.125);
    cairo_scale(cr, scale_mesh, scale_mesh);
    cairo_translate(cr, -26.0*w1, -26.0*h1);
    for(i=0;i<8;i++)
      {
        for(j=0;j<8;j++)
          {
            draw_mesh(cr, width, height);
            cairo_translate(cr, 6.0*w1, 0.0);
          }
        cairo_translate(cr, -48.0*w1, 6.0*h1);
      }
  }
static void draw_shapes(GtkWidget *widget, cairo_t *cr)
  {
    gdouble w1=drawing_width/10.0;
    gdouble h1=drawing_height/10.0;

    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.0);
    cairo_save(cr);
    cairo_translate(cr, -5.0*w1, -5.0*h1);
    if(drawing_combo==1) draw_t_shirt(cr, drawing_width, drawing_height);
    else if(drawing_combo==2) draw_sock(cr, drawing_width, drawing_height);
    else if(drawing_combo==3) draw_fish(cr, drawing_width, drawing_height);
    else if(drawing_combo==4) draw_butterfly(cr, drawing_width, drawing_height);
    else if(drawing_combo==5)
      {
        cairo_translate(cr, w1, h1);
        cairo_scale(cr, 0.8, 0.8);
        draw_rooster(cr, drawing_width, drawing_height);
      }
    else if(drawing_combo==6)
      {
        cairo_translate(cr, w1, h1);
        cairo_scale(cr, 0.8, 0.8);
        draw_penguin(cr, drawing_width, drawing_height);
      }
    else if(drawing_combo==7)
      {
        cairo_translate(cr, w1, h1);
        cairo_scale(cr, 0.8, 0.8);
        draw_chameleon(cr, drawing_width, drawing_height);
      }
    cairo_restore(cr);

    cairo_save(cr);
    cairo_clip(cr);
    draw_background_mesh(cr, drawing_width, drawing_height);
    cairo_restore(cr);

    if(drawing_combo==3)
      {
        //Draw fish eye.
        cairo_translate(cr, -5.0*w1, -5.0*h1);
        cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
        cairo_arc(cr, 7.15*w1, 4.2*h1, 0.15*h1, 0.0, 2.0*G_PI);
        cairo_fill(cr); 
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_arc(cr, 7.15*w1, 4.2*h1, 0.1*h1, 0.0, 2.0*G_PI);
        cairo_fill(cr); 
      }
    if(drawing_combo==5)
      {
        //Draw rooster eye.
        cairo_translate(cr, -5.0*w1, -5.0*h1);
        cairo_translate(cr, w1, h1);
        cairo_scale(cr, 0.8, 0.8);
        cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
        cairo_arc(cr, 7.25*w1, 1.85*h1, 0.1*h1, 0.0, 2.0*G_PI);
        cairo_fill(cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_arc(cr, 7.25*w1, 1.85*h1, 0.07*h1, 0.0, 2.0*G_PI);
        cairo_fill(cr); 
      }
    if(drawing_combo==6)
      {
        cairo_translate(cr, -5.0*w1, -5.0*h1);
        cairo_translate(cr, w1, h1);
        cairo_scale(cr, 0.8, 0.8);
        draw_penguin_head(cr, w1, h1); 
      }
    if(drawing_combo==7)
      {
        cairo_translate(cr, -5.0*w1, -5.0*h1);
        cairo_translate(cr, w1, h1);
        cairo_scale(cr, 0.8, 0.8);
        //Chameleon eye.
        cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
        cairo_arc(cr, 8.2*w1, 5.05*h1, 0.24*h1, 0.0, 2.0*G_PI);
        cairo_stroke(cr);  
        cairo_arc(cr, 8.25*w1, 5.00*h1, 0.08*h1, 0.7, 2.0*G_PI);
        cairo_fill(cr); 
        //Chameleon mouth.
        cairo_move_to(cr, 9.0*w1, 5.5*h1);
        cairo_line_to(cr, 8.0*w1, 5.5*h1);
        cairo_stroke(cr); 
      }   
  }
static void draw_mesh(cairo_t *cr, gdouble width, gdouble height)
  {
    gint i=0;
    gint j=0;
    gdouble w1=width/10.0;
    gdouble h1=height/10.0;
    gdouble p1=(rect[2]/width)*10.0;
    gdouble p2=(rect[3]/height)*10.0;

    //Save the mesh points.
    //g_print("x %f y %f x %f y %f\n", (rect[2]/width)*10.0, (rect[3]/height)*10.0, rect[2], rect[3]);
    if(mesh_combo==0)
      {
        mesh[0]=p1;
        mesh[1]=p2;
      }
    else if(mesh_combo==1)
      {
        mesh[2]=p1;
        mesh[3]=p2;
      }
    else if(mesh_combo==2)
      {
        mesh[4]=p1;
        mesh[5]=p2;
      }
    else if(mesh_combo==3)
      {
        mesh[6]=p1;
        mesh[7]=p2;
      }
    //The inside control points.
    else if(mesh_combo==4)
      {
        mesh_p[0]=p1;
        mesh_p[1]=p2;
      }
    else if(mesh_combo==5)
      {
        mesh_p[2]=p1;
        mesh_p[3]=p2;
      }
    else if(mesh_combo==6)
      {
        mesh_p[4]=p1;
        mesh_p[5]=p2;
      }
    else
      {
        mesh_p[6]=p1;
        mesh_p[7]=p2;
      } 
    cairo_set_line_width(cr, 4);
 
    //Four gradient patches.
    for(i=0;i<4;i++)
      {
        cairo_save(cr);
        if(i==1) cairo_translate(cr, 3.0*w1, 0.0);
        if(i==2) cairo_translate(cr, 0.0*w1, 3.0*h1);
        if(i==3) cairo_translate(cr, 3.0*w1, 3.0*h1);
        cairo_pattern_t *pattern1=cairo_pattern_create_mesh();
        cairo_mesh_pattern_begin_patch(pattern1);
        cairo_mesh_pattern_move_to(pattern1, 2.0*w1, 2.0*h1);
        cairo_mesh_pattern_curve_to(pattern1, mesh[4]*w1, mesh[5]*h1, mesh[6]*w1, mesh[7]*h1, 5.0*w1, 2.0*h1);
        cairo_mesh_pattern_curve_to(pattern1, mesh[0]*w1+3.0*w1, mesh[1]*h1, mesh[2]*w1+3.0*w1, mesh[3]*h1, 5.0*w1, 5.0*h1);
        cairo_mesh_pattern_curve_to(pattern1, mesh[6]*w1, mesh[7]*h1+3.0*h1, mesh[4]*w1, mesh[5]*h1+3.0*h1, 2.0*w1, 5.0*h1);
       cairo_mesh_pattern_curve_to(pattern1, mesh[2]*w1, mesh[3]*h1, mesh[0]*w1, mesh[1]*h1, 2.0*w1, 2.0*h1);   
       cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, c0[0], c0[1], c0[2], c0[3]);
       cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, c1[0], c1[1], c1[2], c1[3]);
       cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, c2[0], c2[1], c2[2], c2[3]);
       cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, c3[0], c3[1], c3[2], c3[3]);
       /*
           From https://www.adobe.com/products/postscript/pdfs/PLRM.pdf P.286
           Default positions of inside control points.
           P11=S(1/3, 2/3)
           P12=S(2/3, 2/3)
           P21=S(1/3, 1/3)
           P22=S(2/3, 1/3)
           The labels A, B, C and D are a little different but in the same 1/3 and 2/3 positions.
       */
       cairo_mesh_pattern_set_control_point(pattern1, 0, mesh_p[0]*w1-3.0*w1, mesh_p[1]*h1);
       cairo_mesh_pattern_set_control_point(pattern1, 1, mesh_p[2]*w1-3.0*w1, mesh_p[3]*h1);
       cairo_mesh_pattern_set_control_point(pattern1, 2, mesh_p[4]*w1-3.0*w1, mesh_p[5]*h1);
       cairo_mesh_pattern_set_control_point(pattern1, 3, mesh_p[6]*w1-3.0*w1, mesh_p[7]*h1);
       cairo_mesh_pattern_end_patch(pattern1);

       cairo_set_source(cr, pattern1);
       cairo_paint(cr);
       cairo_pattern_destroy(pattern1);
       cairo_restore(cr);
     }

    cairo_set_line_width(cr, 6);    
    //Layout axis for drawing mesh.
    if(default_drawing)
      {
        cairo_set_source_rgb(cr, 0.0, 0.0, 1.0);
        cairo_rectangle(cr, 2.0*w1, 2.0*h1, 6.0*w1, 6.0*h1);
        cairo_stroke(cr);
        cairo_move_to(cr, 2.0*w1, 5.0*h1);
        cairo_line_to(cr, 8.0*w1, 5.0*h1);
        cairo_stroke(cr);
        cairo_move_to(cr, 5.0*w1, 2.0*h1);
        cairo_line_to(cr, 5.0*w1, 8.0*h1);
        cairo_stroke(cr);
      }

    //Add layout lines for initial drawing.
     if(default_drawing)
      {
        //6 vertical Bezier curves over layout axis.
        cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
        cairo_save(cr);
        for(i=0;i<2;i++)
          {
            for(j=0;j<3;j++)
              {
                cairo_move_to(cr, 2.0*w1, 2.0*h1);
                //cairo_curve_to(cr, 1.0*w1, 3.0*h1, 3.0*w1, 4.0*h1, 2.0*w1, 5.0*h1);
                cairo_curve_to(cr, mesh[0]*w1, mesh[1]*h1, mesh[2]*w1, mesh[3]*h1, 2.0*w1, 5.0*h1);
                cairo_stroke(cr);
                cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
                cairo_translate(cr, 3.0*w1, 0.0);
              }
            cairo_translate(cr, -9.0*w1, 3.0*h1);
          }
        cairo_restore(cr);

        //6 horizontal Bezier curves over layout axis.
        cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
        cairo_save(cr);
        for(i=0;i<2;i++)
          {
            for(j=0;j<3;j++)
              {
                cairo_move_to(cr, 2.0*w1, 2.0*h1);
                //cairo_curve_to(cr, 3.0*w1, 3.0*h1, 4.0*w1, 1.0*h1, 5.0*w1, 2.0*h1);
                cairo_curve_to(cr, mesh[4]*w1, mesh[5]*h1, mesh[6]*w1, mesh[7]*h1, 5.0*w1, 2.0*h1);
                cairo_stroke(cr);
                cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
                cairo_translate(cr, 0.0, 3.0*h1);
              }
           cairo_translate(cr, 3.0*w1, -9.0*h1);
          }
        cairo_restore(cr);
      }

    if(default_drawing)
      {
        cairo_select_font_face(cr, "Serif", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
        cairo_set_font_size(cr, 12);    
        cairo_set_source_rgb(cr, 0.0 , 0.0 , 0.0);
        cairo_move_to(cr, mesh[0]*width/10.0, mesh[1]*height/10.0);
        cairo_show_text(cr, "1");
        cairo_stroke(cr);
        cairo_move_to(cr, mesh[2]*width/10.0, mesh[3]*height/10.0);
        cairo_show_text(cr, "2");
        cairo_stroke(cr);
        cairo_move_to(cr, mesh[4]*width/10.0, mesh[5]*height/10.0);
        cairo_show_text(cr, "3");
        cairo_stroke(cr);
        cairo_move_to(cr, mesh[6]*width/10.0, mesh[7]*height/10.0);
        cairo_show_text(cr, "4");
        cairo_stroke(cr);
        cairo_move_to(cr, mesh_p[0]*width/10.0, mesh_p[1]*height/10.0);
        cairo_show_text(cr, "A");
        cairo_stroke(cr);
        cairo_move_to(cr, mesh_p[2]*width/10.0, mesh_p[3]*height/10.0);
        cairo_show_text(cr, "B");
        cairo_stroke(cr);
        cairo_move_to(cr, mesh_p[4]*width/10.0, mesh_p[5]*height/10.0);
        cairo_show_text(cr, "C");
        cairo_stroke(cr);
        cairo_move_to(cr, mesh_p[6]*width/10.0, mesh_p[7]*height/10.0);
        cairo_show_text(cr, "D");
        cairo_stroke(cr);
      }
  }
static void draw_background_mesh(cairo_t *cr, gdouble drawing_width, gdouble drawing_height)
  {
    if(tile_combo==0)
      {
        cairo_scale(cr, scale_mesh, scale_mesh);
        cairo_translate(cr, -drawing_width/2.0, -drawing_height/2.0);
        draw_mesh(cr, drawing_width, drawing_height);
      }
    else if(tile_combo==1)
      {
        draw_grid1(cr, drawing_width, drawing_height); 
      }
    else if(tile_combo==2)
      {
        draw_grid2(cr, drawing_width, drawing_height);
      }
    else
      {
        draw_grid3(cr, drawing_width, drawing_height);
      }      
  }
static void draw_t_shirt(cairo_t *cr, gdouble width, gdouble height)
  {
    gdouble w1=width/10.0;
    gdouble h1=height/10.0;

    //Draw the shirt.
    cairo_move_to(cr, 4.25*w1, 2.0*h1);
    cairo_curve_to(cr, 4.5*w1, 2.5*h1, 5.5*w1, 2.5*h1, 5.75*w1, 2.0*h1);
    cairo_curve_to(cr, 6.0*w1, 2.0*h1, 7.0*w1, 2.0*h1, 8.0*w1, 3.5*h1);
    cairo_line_to(cr, 7.25*w1, 4.5*h1);
    cairo_line_to(cr, 6.75*w1, 4.0*h1);
    cairo_line_to(cr, 6.75*w1, 8.0*h1);
    cairo_line_to(cr, 3.25*w1, 8.0*h1);
    cairo_line_to(cr, 3.25*w1, 4.0*h1);
    cairo_line_to(cr, 2.75*w1, 4.5*h1);
    cairo_line_to(cr, 2.0*w1, 3.5*h1);
    cairo_curve_to(cr, 3.0*w1, 2.0*h1, 4.0*w1, 2.0*h1, 4.0*w1, 2.0*h1);
    cairo_close_path(cr);
    cairo_stroke_preserve(cr);        
  }
static void draw_sock(cairo_t *cr, gdouble width, gdouble height)
  {
    gdouble w1=width/10.0;
    gdouble h1=height/10.0;

    //The sock.
    cairo_move_to(cr, 4.0*w1, 2.25*h1);
    cairo_curve_to(cr, 3.5*w1, 2.3*h1, 2.5*w1, 2.3*h1, 2.1*w1, 2.25*h1);
    cairo_line_to(cr, 3.0*w1, 5.0*h1);
    cairo_curve_to(cr, 3.0*w1, 6.0*h1, 3.0*w1, 6.5*h1, 5.0*w1, 7.0*h1);
    cairo_line_to(cr, 7.0*w1, 7.5*h1);
    cairo_curve_to(cr, 8.4*w1, 8.0*h1, 8.4*w1, 6.25*h1, 7.15*w1, 5.75*h1);
    cairo_line_to(cr, 6.0*w1, 5.25*h1);
    cairo_curve_to(cr, 5.5*w1, 5.0*h1, 5.5*w1, 5.0*h1, 5.0*w1, 4.75*h1);
    cairo_close_path(cr);
    cairo_stroke_preserve(cr);   
  }
static void draw_fish(cairo_t *cr, gdouble width, gdouble height)
  {
    gdouble w1=width/10.0;
    gdouble h1=height/10.0;

    //The fish.
    cairo_set_source_rgb(cr, 0.0, 0.0, 1.0);
    cairo_set_line_width(cr, 2);  
    cairo_move_to(cr, 8.0*w1, 5.0*h1);
    cairo_curve_to(cr, 7.5*w1, 5.65*h1, 7.5*w1, 5.65*h1, 6.5*w1, 6.0*h1);
    cairo_curve_to(cr, 6.5*w1, 6.25*h1, 6.5*w1, 6.25*h1, 6.25*w1, 6.75*h1);
    cairo_curve_to(cr, 6.25*w1, 6.25*h1, 6.25*w1, 6.25*h1, 6.1*w1, 6.1*h1);
    cairo_curve_to(cr, 5.0*w1, 6.25*h1, 5.0*w1, 6.25*h1, 4.0*w1, 6.1*h1);    
    cairo_curve_to(cr, 3.65*w1, 6.75*h1, 3.65*w1, 6.75*h1, 3.25*w1, 7.0*h1);    
    cairo_curve_to(cr, 3.5*w1, 6.65*h1, 3.5*w1, 6.65*h1, 3.6*w1, 6.0*h1);
    cairo_curve_to(cr, 3.25*w1, 5.75*h1, 3.25*w1, 5.75*h1, 2.5*w1, 5.25*h1);    
    cairo_curve_to(cr, 2.25*w1, 6.0*h1, 2.25*w1, 6.0*h1, 2.0*w1, 6.5*h1);
    cairo_curve_to(cr, 2.2*w1, 5.0*h1, 2.2*w1, 5.0*h1, 2.0*w1, 3.5*h1);
    cairo_curve_to(cr, 2.25*w1, 4.0*h1, 2.25*w1, 4.0*h1, 2.5*w1, 4.75*h1);
    cairo_curve_to(cr, 3.25*w1, 4.25*h1, 3.25*w1, 4.25*h1, 3.6*w1, 4.0*h1);
    cairo_curve_to(cr, 3.5*w1, 3.25*h1, 3.5*w1, 3.25*h1, 3.25*w1, 3.0*h1);
    cairo_curve_to(cr, 3.65*w1, 3.25*h1, 3.65*w1, 3.25*h1, 4.0*w1, 3.9*h1);
    cairo_curve_to(cr, 4.5*w1, 3.75*h1, 4.5*w1, 3.75*h1, 5.0*w1, 3.75*h1);
    cairo_curve_to(cr, 5.25*w1, 3.5*h1, 5.75*w1, 3.5*h1, 6.0*w1, 2.75*h1);
    cairo_curve_to(cr, 6.25*w1, 3.0*h1, 6.25*w1, 3.0*h1, 6.5*w1, 3.65*h1);
    cairo_curve_to(cr, 7.5*w1, 4.0*h1, 7.5*w1, 4.0*h1, 8.0*w1, 4.7*h1);
    cairo_line_to(cr, 7.0*w1, 4.9*h1);
    cairo_line_to(cr, 8.0*w1, 5.0*h1);
    cairo_stroke_preserve(cr);
  }
static void draw_butterfly(cairo_t *cr, gdouble width, gdouble height)
  {
    gdouble w1=width/10.0;
    gdouble h1=height/10.0;

    //The antenna.
    cairo_set_source_rgb(cr, 0.0, 0.0, 1.0);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
    cairo_set_line_width(cr, 4);  
    cairo_move_to(cr, 5.0*w1, 3.75*h1);
    cairo_curve_to(cr, 5.5*w1, 2.5*h1, 5.5*w1, 2.5*h1, 6.0*w1, 2.5*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 3.75*h1);
    cairo_curve_to(cr, 4.5*w1, 2.5*h1, 4.5*w1, 2.5*h1, 4.0*w1, 2.5*h1);
    cairo_stroke(cr);

    //The butterfly.
    cairo_set_source_rgb(cr, 0.0, 0.0, 1.0);
    cairo_set_line_width(cr, 2);  
    cairo_move_to(cr, 5.25*w1, 6.0*h1);
    cairo_curve_to(cr, 6.25*w1, 6.0*h1, 6.0*w1, 7.5*h1, 6.25*w1, 7.25*h1);
    cairo_curve_to(cr, 6.25*w1, 5.5*h1, 6.25*w1, 5.5*h1, 6.75*w1, 5.0*h1);
    cairo_curve_to(cr, 7.75*w1, 4.5*h1, 7.75*w1, 4.5*h1, 8.0*w1, 4.0*h1);
    cairo_curve_to(cr, 7.0*w1, 3.25*h1, 6.0*w1, 3.75*h1, 5.25*w1, 4.0*h1);
    cairo_curve_to(cr, 5.2*w1, 3.25*h1, 4.8*w1, 3.25*h1, 4.75*w1, 4.0*h1);
    cairo_curve_to(cr, 4.0*w1, 3.75*h1, 3.0*w1, 3.25*h1, 2.0*w1, 4.0*h1);
    cairo_curve_to(cr, 2.25*w1, 4.5*h1, 2.25*w1, 4.5*h1, 3.25*w1, 5.0*h1);
    cairo_curve_to(cr, 3.75*w1, 5.5*h1, 3.75*w1, 5.5*h1, 3.75*w1, 7.25*h1);
    cairo_curve_to(cr, 4.0*w1, 7.5*h1, 3.75*w1, 6.0*h1, 4.75*w1, 6.0*h1);
    cairo_curve_to(cr, 4.8*w1, 5.5*h1, 5.2*w1, 5.5*h1, 5.25*w1, 6.0*h1);
    cairo_stroke_preserve(cr);
  }
static void draw_rooster(cairo_t *cr, gdouble width, gdouble height)
  {
    gdouble w1=width/10.0;
    gdouble h1=height/10.0;
  
    //First leg and foot.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_move_to(cr, 5.0*w1, 7.0*h1);
    cairo_line_to(cr, 5.5*w1, 8.5*h1);
    cairo_line_to(cr, 5.0*w1, 8.5*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.5*w1, 8.5*h1);
    cairo_line_to(cr, 6.0*w1, 8.3*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.5*w1, 8.5*h1);
    cairo_line_to(cr, 6.2*w1, 8.6*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.5*w1, 8.5*h1);
    cairo_line_to(cr, 6.0*w1, 8.9*h1);
    cairo_stroke(cr);
    //Second leg and foot.
    cairo_move_to(cr, 5.5*w1, 7.0*h1);
    cairo_line_to(cr, 6.75*w1, 8.25*h1);
    cairo_line_to(cr, 6.25*w1, 8.25*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 6.75*w1, 8.25*h1);
    cairo_line_to(cr, 7.25*w1, 8.05*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 6.75*w1, 8.25*h1);
    cairo_line_to(cr, 7.45*w1, 8.35*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 6.75*w1, 8.25*h1);
    cairo_line_to(cr, 7.25*w1, 8.65*h1);
    cairo_stroke(cr);

    //Draw the rooster body.
    cairo_move_to(cr, 8.0*w1, 3.0*h1);
    cairo_curve_to(cr, 9.0*w1, 4.0*h1, 9.0*w1, 7.5*h1, 5.0*w1, 7.0*h1);
    cairo_curve_to(cr, 4.0*w1, 7.0*h1, 3.0*w1, 6.0*h1, 3.0*w1, 4.0*h1);
    cairo_curve_to(cr, 3.0*w1, 4.0*h1, 2.0*w1, 4.0*h1, 2.0*w1, 4.5*h1);
    cairo_curve_to(cr, 2.0*w1, 3.5*h1, 2.5*w1, 3.25*h1, 2.75*w1, 3.5*h1);
    cairo_curve_to(cr, 2.0*w1, 3.0*h1, 1.5*w1, 3.25*h1, 1.5*w1, 4.0*h1);
    cairo_curve_to(cr, 1.5*w1, 2.5*h1, 2.0*w1, 2.5*h1, 2.5*w1, 3.0*h1);
    cairo_curve_to(cr, 2.0*w1, 2.0*h1, 1.5*w1, 2.0*h1, 1.0*w1, 3.5*h1);
    cairo_curve_to(cr, 1.5*w1, 1.0*h1, 2.5*w1, 1.0*h1, 4.0*w1, 4.0*h1);
    cairo_curve_to(cr, 5.0*w1, 5.0*h1, 6.0*w1, 4.0*h1, 6.5*w1, 2.5*h1);
    cairo_curve_to(cr, 5.5*w1, 2.5*h1, 5.5*w1, 2.25*h1, 6.5*w1, 2.25*h1);
    cairo_curve_to(cr, 5.75*w1, 2.0*h1, 5.75*w1, 1.75*h1, 6.75*w1, 2.0*h1);
    cairo_curve_to(cr, 6.25*w1, 1.5*h1, 6.25*w1, 1.25*h1, 7.0*w1, 1.75*h1);
    cairo_curve_to(cr, 7.0*w1, 1.0*h1, 7.25*w1, 1.0*h1, 7.5*w1, 1.8*h1);    
    cairo_curve_to(cr, 7.7*w1, 1.6*h1, 8.0*w1, 1.4*h1, 8.5*w1, 1.5*h1);
    cairo_curve_to(cr, 8.0*w1, 1.6*h1, 7.7*w1, 1.8*h1, 7.75*w1, 2.0*h1);
    cairo_curve_to(cr, 7.7*w1, 2.0*h1, 8.0*w1, 1.8*h1, 8.5*w1, 2.15*h1);
    cairo_curve_to(cr, 8.25*w1, 2.15*h1, 8.15*w1, 2.05*h1, 7.85*w1, 2.15*h1);
    cairo_curve_to(cr, 8.4*w1, 2.5*h1, 8.5*w1, 2.7*h1, 8.0*w1, 3.0*h1);
    cairo_close_path(cr);
    cairo_stroke_preserve(cr);
  }
static void draw_penguin(cairo_t *cr, gdouble width, gdouble height)
  {
    gdouble w1=width/10.0;
    gdouble h1=height/10.0;

    //Draw a penguin outline.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_move_to(cr, 6.5*w1, 8.0*h1);
    cairo_curve_to(cr, 7.0*w1, 6.5*h1, 7.0*w1, 5.5*h1, 6.5*w1, 4.0*h1);
    cairo_curve_to(cr, 8.0*w1, 4.4*h1, 8.2*w1, 4.6*h1, 8.9*w1, 5.0*h1);
    cairo_curve_to(cr, 9.0*w1, 4.9*h1, 9.0*w1, 4.7*h1, 8.8*w1, 4.5*h1);
    cairo_curve_to(cr, 8.0*w1, 3.8*h1, 7.0*w1, 3.2*h1, 6.5*w1, 3.2*h1);
    cairo_curve_to(cr, 6.4*w1, 3.0*h1, 6.3*w1, 2.7*h1, 6.0*w1, 2.5*h1);
    cairo_curve_to(cr, 6.4*w1, 2.0*h1, 6.6*w1, 2.0*h1, 7.2*w1, 1.7*h1);
    cairo_curve_to(cr, 7.0*w1, 1.4*h1, 6.6*w1, 1.4*h1, 6.4*w1, 1.4*h1);
    cairo_curve_to(cr, 5.7*w1, 1.0*h1, 5.0*w1, 1.0*h1, 4.3*w1, 2.5*h1);
    cairo_curve_to(cr, 4.0*w1, 3.4*h1, 3.8*w1, 3.2*h1, 3.4*w1, 3.3*h1);
    cairo_curve_to(cr, 2.5*w1, 3.2*h1, 2.0*w1, 3.4*h1, 1.5*w1, 3.6*h1);
    cairo_curve_to(cr, 1.0*w1, 3.8*h1, 1.0*w1, 4.0*h1, 1.5*w1, 4.1*h1);
    cairo_curve_to(cr, 2.0*w1, 3.9*h1, 3.0*w1, 4.0*h1, 3.6*w1, 4.1*h1);
    cairo_curve_to(cr, 3.2*w1, 6.0*h1, 3.2*w1, 7.0*h1, 3.5*w1, 8.0*h1);
    cairo_line_to(cr, 3.5*w1, 8.5*h1);
    cairo_line_to(cr, 3.0*w1, 9.0*h1);
    cairo_curve_to(cr, 3.4*w1, 8.8*h1, 3.4*w1, 8.8*h1, 3.6*w1, 9.0*h1);
    cairo_curve_to(cr, 4.0*w1, 8.8*h1, 4.0*w1, 8.8*h1, 4.3*w1, 9.0*h1);
    cairo_line_to(cr, 4.0*w1, 8.5*h1);
    cairo_line_to(cr, 4.0*w1, 8.0*h1);
    cairo_curve_to(cr, 4.5*w1, 8.4*h1, 5.5*w1, 8.4*h1, 6.0*w1, 8.0*h1);
    cairo_line_to(cr, 6.0*w1, 8.5*h1);
    cairo_line_to(cr, 5.5*w1, 9.0*h1);
    cairo_curve_to(cr, 5.9*w1, 8.8*h1, 5.9*w1, 8.8*h1, 6.1*w1, 9.0*h1);
    cairo_curve_to(cr, 6.5*w1, 8.8*h1, 6.5*w1, 8.8*h1, 6.8*w1, 9.0*h1);
    cairo_line_to(cr, 6.5*w1, 8.5*h1);
    cairo_close_path(cr);
    cairo_stroke_preserve(cr);
  }
static void draw_penguin_head(cairo_t *cr, gdouble w1, gdouble h1)
  {    
    //Redraw the head black.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_move_to(cr, 6.0*w1, 2.5*h1);
    cairo_curve_to(cr, 6.4*w1, 2.0*h1, 6.6*w1, 2.0*h1, 7.2*w1, 1.7*h1);
    cairo_curve_to(cr, 7.0*w1, 1.4*h1, 6.6*w1, 1.4*h1, 6.4*w1, 1.4*h1);
    cairo_curve_to(cr, 5.7*w1, 1.0*h1, 5.0*w1, 1.0*h1, 4.3*w1, 2.5*h1);
    cairo_curve_to(cr, 4.0*w1, 3.4*h1, 3.8*w1, 3.2*h1, 3.7*w1, 4.0*h1);
    cairo_curve_to(cr, 5.3*w1, 0.5*h1, 5.8*w1, 3.4*h1, 6.0*w1, 2.5*h1);
    cairo_close_path(cr);
    cairo_fill(cr);

    //The eye.
    gdouble radius=0;
    if(w1>h1) radius=h1;
    else radius=w1;
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_arc(cr, 5.8*w1, 1.5*h1, 0.2*radius, 0.0, 2*G_PI);
    cairo_fill(cr);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_arc(cr, 5.8*w1, 1.5*h1, 0.1*radius, 0.0, 2*G_PI);
    cairo_fill(cr);  
  }
static void draw_chameleon(cairo_t *cr, gdouble width, gdouble height)
  {
    gdouble w1=width/10.0;
    gdouble h1=height/10.0;

    //The chameleon.
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_set_line_width(cr, 2);

    cairo_move_to(cr, 9.0*w1, 5.5*h1);
    cairo_curve_to(cr, 8.75*w1, 5.0*h1, 8.75*w1, 4.8*h1, 8.0*w1, 4.7*h1);
    cairo_line_to(cr, 7.75*w1, 4.5*h1);
    cairo_line_to(cr, 7.5*w1, 4.8*h1);
    cairo_curve_to(cr, 6.0*w1, 2.8*h1, 3.2*w1, 2.5*h1, 2.5*w1, 5.0*h1);
    cairo_line_to(cr, 1.0*w1, 1.0*h1);
    cairo_line_to(cr, 2.25*w1, 5.25*h1);
    cairo_line_to(cr, 2.5*w1, 5.75*h1);
    cairo_line_to(cr, 2.75*w1, 6.5*h1);
    cairo_line_to(cr, 2.5*w1, 7.5*h1);
    cairo_line_to(cr, 2.6*w1, 7.6*h1);
    cairo_line_to(cr, 2.8*w1, 7.4*h1);
    cairo_line_to(cr, 3.2*w1, 7.4*h1);
    cairo_line_to(cr, 3.2*w1, 7.2*h1);
    cairo_line_to(cr, 2.8*w1, 7.2*h1);
    cairo_line_to(cr, 3.0*w1, 6.5*h1);
    cairo_line_to(cr, 2.7*w1, 5.75*h1);
    cairo_line_to(cr, 3.5*w1, 6.25*h1);
    cairo_line_to(cr, 3.6*w1, 6.75*h1);
    cairo_line_to(cr, 3.4*w1, 7.0*h1);
    cairo_line_to(cr, 3.6*w1, 7.1*h1);
    cairo_line_to(cr, 3.7*w1, 6.9*h1);
    cairo_line_to(cr, 4.0*w1, 6.9*h1);
    cairo_line_to(cr, 4.0*w1, 6.7*h1);
    cairo_line_to(cr, 3.8*w1, 6.7*h1);
    cairo_line_to(cr, 3.7*w1, 6.25*h1);
    cairo_line_to(cr, 3.1*w1, 5.75*h1);
    cairo_curve_to(cr, 5.0*w1, 5.0*h1, 5.0*w1, 5.0*h1, 6.0*w1, 5.75*h1);
    cairo_line_to(cr, 5.75*w1, 7.5*h1);
    cairo_line_to(cr, 5.6*w1, 7.8*h1);
    cairo_line_to(cr, 5.8*w1, 8.0*h1);
    cairo_line_to(cr, 6.0*w1, 7.6*h1);
    cairo_line_to(cr, 6.3*w1, 7.6*h1);
    cairo_line_to(cr, 6.3*w1, 7.4*h1);
    cairo_line_to(cr, 6.1*w1, 7.4*h1);
    cairo_line_to(cr, 6.2*w1, 5.75*h1);
    cairo_line_to(cr, 6.8*w1, 7.0*h1);
    cairo_line_to(cr, 6.6*w1, 7.3*h1);
    cairo_line_to(cr, 6.8*w1, 7.5*h1);
    cairo_line_to(cr, 7.0*w1, 7.2*h1);
    cairo_line_to(cr, 7.2*w1, 7.2*h1);
    cairo_line_to(cr, 7.2*w1, 7.0*h1);
    cairo_line_to(cr, 7.0*w1, 7.0*h1);
    cairo_line_to(cr, 6.4*w1, 5.75*h1);
    cairo_line_to(cr, 7.0*w1, 6.0*h1);
    cairo_line_to(cr, 7.5*w1, 6.0*h1);
    cairo_curve_to(cr, 8.0*w1, 6.1*h1, 8.5*w1, 6.2*h1, 9.0*w1, 5.5*h1);
    cairo_stroke_preserve(cr);
  }
static gboolean start_press(GtkWidget *widget, GdkEvent *event, gpointer data)
  {
    //Just allow dragging points when the rotations are 0.
    if(default_drawing)
      {
        g_signal_handler_unblock(widget, motion_id);
        rect[0]=event->button.x;
        rect[1]=event->button.y;
      }

    return TRUE;
  }
static gboolean stop_press(GtkWidget *widget, GdkEvent *event, gpointer data)
  {
    if(default_drawing)
      {
        g_signal_handler_block(widget, motion_id);
      }

    return TRUE;
  }
static gboolean cursor_motion(GtkWidget *widget, GdkEvent *event, gpointer data)
  {
    rect[2]=event->button.x;
    rect[3]=event->button.y;
    gtk_widget_queue_draw(widget);
    return TRUE;
  }
static void combo1_changed(GtkComboBox *combo1, gpointer data)
  {
    mesh_combo=gtk_combo_box_get_active(combo1);
  }
static void set_standard_mesh(GtkToggleButton *check_standard, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_standard)) default_drawing=TRUE;
    else default_drawing=FALSE;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void combo2_changed(GtkComboBox *combo2, gpointer *combos)
  {
    tile_combo=gtk_combo_box_get_active(combo2);
    grid_combo=TRUE;
    gtk_combo_box_set_active(GTK_COMBO_BOX(combos[1]), 0);
    gtk_widget_queue_draw(GTK_WIDGET(combos[0]));
  }
static void combo3_changed(GtkComboBox *combo3, gpointer da)
  {
    drawing_combo=gtk_combo_box_get_active(combo3);
    if(drawing_combo!=0)
      {
        grid_combo=FALSE;
      }
    else grid_combo=TRUE;
    gtk_widget_queue_draw(GTK_WIDGET(da));
  }
static void check_colors(GtkWidget *widget, GtkWidget **colors)
  {
    gint i=0;
    GdkRGBA rgba;

    for(i=0;i<5;i++)
      {
        if(gdk_rgba_parse(&rgba, gtk_entry_get_text(GTK_ENTRY(colors[i]))))
          {
            switch(i)
              {
                case 0:
                  c0[0]=rgba.red;
                  c0[1]=rgba.green;
                  c0[2]=rgba.blue;
                  c0[3]=rgba.alpha;
                  break;
                case 1:
                  c1[0]=rgba.red;
                  c1[1]=rgba.green;
                  c1[2]=rgba.blue;
                  c1[3]=rgba.alpha;
                  break;
                case 2:
                  c2[0]=rgba.red;
                  c2[1]=rgba.green;
                  c2[2]=rgba.blue;
                  c2[3]=rgba.alpha;
                  break;
                case 3:
                  c3[0]=rgba.red;
                  c3[1]=rgba.green;
                  c3[2]=rgba.blue;
                  c3[3]=rgba.alpha;
                  break;
                case 4:
                  b1[0]=rgba.red;
                  b1[1]=rgba.green;
                  b1[2]=rgba.blue;
                  b1[3]=rgba.alpha;
             }
          }
        else
          {
            g_print("Color string format error in c%i\n", i);
          } 
      }
    //Update main window.
    gtk_widget_queue_draw(colors[5]);
    //Update the drawing area.
    gtk_widget_queue_draw(colors[6]);
  }
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data)
  {
    //Paint background of drawing area.
    cairo_set_source_rgba(cr, b1[0], b1[1], b1[2], b1[3]);
    cairo_paint(cr);
    //Paint the background under the grid.
    cairo_set_source_rgba(cr, 0.5, 0.5, 1.0, 1.0);
    gint width=gtk_paned_get_position(GTK_PANED(data));
    gint height=gtk_widget_get_allocated_height(widget);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_rectangle(cr, width, 0.0, 10, height);
    cairo_fill(cr);
    return FALSE;
  }
static gchar* save_file(gpointer *widgets, gint surface_type)
  {
    int result=0;

    GtkWidget *dialog=gtk_file_chooser_dialog_new("Save File", GTK_WINDOW(widgets[1]), GTK_FILE_CHOOSER_ACTION_SAVE, "Cancel", GTK_RESPONSE_CANCEL, "Save", GTK_RESPONSE_ACCEPT, NULL);

    if(surface_type==0) gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".svg");
    else if(surface_type==1) gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".pdf");
    else if(surface_type==2) gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".ps");
    else gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".png"); 

    result=gtk_dialog_run(GTK_DIALOG(dialog));
    gchar *file_name=NULL;
    if(result==GTK_RESPONSE_ACCEPT)
      {

        file_name=gtk_file_chooser_get_current_name(GTK_FILE_CHOOSER(dialog));
      }

    gtk_widget_destroy(dialog);

    return file_name;
  }
static void draw_svg(GtkWidget *widget, gpointer *widgets)
  {
    gchar *file_name=save_file(widgets, 0);
    
    //Scale factor to convert to points.
    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_svg_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, widgets, 0);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name);
  }
static void draw_pdf(GtkWidget *widget, gpointer *widgets)
  {
    gchar *file_name=save_file(widgets, 1);

    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_pdf_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, widgets, 1);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }
    
    g_free(file_name);
  }
static void draw_ps(GtkWidget *widget, gpointer *widgets)
  {
    gchar *file_name=save_file(widgets, 2);

    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_ps_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, widgets, 2);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name); 
  }
static void draw_png(GtkWidget *widget, gpointer *widgets)
  {
    gchar *file_name=save_file(widgets, 3);

    if(file_name!=NULL)
      {
        cairo_surface_t *surface=cairo_image_surface_create(CAIRO_FORMAT_ARGB32, layout_width, layout_height);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, widgets, 3);
        cairo_surface_write_to_png(surface, file_name);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name);    
  }
static void draw_surface(cairo_t *cr, gpointer *widgets, guint surface_type)
  {
    gdouble points_scale=0.0;

    if(surface_type<3) points_scale=72.0/ppi;
    else points_scale=1.0;

    cairo_set_source_rgba(cr, b1[0], b1[1], b1[2], b1[3]);
    cairo_paint(cr);

    if(!default_drawing)
      {
        gdouble qrs[9];
        cairo_matrix_t matrix1;
        quaternion_rotation(-combo_yaw, combo_roll, combo_pitch, qrs);
        cairo_translate(cr, translate_x*points_scale, translate_y*points_scale);
        cairo_matrix_init(&matrix1, qrs[0], qrs[3], qrs[1], qrs[4], layout_width*points_scale/2.0, layout_height*points_scale/2.0);
        cairo_transform(cr, &matrix1); 
        cairo_scale(cr, scale*points_scale+0.01, scale*points_scale+0.01);
        if(grid_combo) draw_grids(widgets[0], cr);
        else draw_shapes(widgets[0], cr);
      }
    else
      {
        draw_mesh(cr, drawing_width, drawing_height);
      }
  }
static void about_dialog(GtkWidget *widget, gpointer data)
  {
    GtkWidget *dialog=gtk_about_dialog_new();
    gtk_widget_set_name(dialog, "about_dialog");
    gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(data));
   
    gtk_about_dialog_set_logo(GTK_ABOUT_DIALOG(dialog), NULL);
    gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(dialog), "Mesh Maker");
    gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog), "Test Version 1.0");
    gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(dialog), "Tile a mesh and clip the pattern.");
    gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(dialog), "(C) 2017 C. Eric Cashon");
   
    gtk_widget_show_all(dialog);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
  }
//The butterfly program icon.
static GdkPixbuf* draw_icon()
  {
    //Create a surface to draw a 256x256 icon. 
    cairo_surface_t *surface=cairo_image_surface_create(CAIRO_FORMAT_ARGB32, 256, 256);
    cairo_t *cr=cairo_create(surface);
    
    //Paint the background.
    cairo_set_source_rgb(cr, 0.5, 0.8, 1.0);
    cairo_paint(cr);

    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.0);
    /*
        Scale drawing 256*1.5=384. 128 extra pixels, half on each side of drawing.
        Move the drawing back by 64 and scale.
    */
    cairo_translate(cr, -64.0, -64.0);
    cairo_scale(cr, 1.5, 1.5);
    cairo_save(cr);
    draw_butterfly(cr, 256.0, 256.0);
    cairo_restore(cr);
    cairo_clip(cr);

    cairo_translate(cr, 128.0, 128.0);
    draw_grid2(cr, 256.0, 256.0);

    GdkPixbuf *icon=gdk_pixbuf_get_from_surface(surface, 0, 0, 256, 256);

    cairo_destroy(cr);
    cairo_surface_destroy(surface); 
    return icon;
  }
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_pitch=value*G_PI/180.0;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_roll=value*G_PI/180.0;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    combo_yaw=value*G_PI/180.0;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>2.0) scale=2.0;
    else if(value<0.0) scale=0.0;
    else scale=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void scale_mesh_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    scale_mesh=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
static void translate_x_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    translate_x=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void translate_y_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    translate_y=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void layout_width_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    layout_width=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void layout_height_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    layout_height=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }








