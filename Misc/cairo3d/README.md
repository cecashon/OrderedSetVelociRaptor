

--A start of a cairo 3d library. It needs work but there are some good things working. It has some basic functions for drawing, rotating and revolving shapes and text in 3d. Cairo has great support for drawing on a wide range of surfaces along with drawing text. Add a little 3d to the mix and it gives you an extended range for drawing.

--Along with the shapes, there is a 3d pie chart and 3d histogram program for drawing graphs. A couple other drawing test programs as well.

--For building individual programs look in the *_UI.c files. The programs can output the 3d drawings to a .pdf, .svg or .ps. An advantage if you want to add the drawings to an Inkscape layout and send it to a printer. Or maybe open a svg drawing with a web browser.

--There is also a simple make file that will build all the programs in the cairo3d folder. It has dependencies for GTK3 and Freetype2.

--Test out and have fun drawing.

--The cairo 3d art gallery of shapes, critters, text and charts.

![ScreenShot](/Misc/cairo3d/cairo_drawings1.png)

![ScreenShot](/Misc/cairo3d/cairo_drawings2.png)

![ScreenShot](/Misc/cairo3d/cairo_drawings3.png)

![ScreenShot](/Misc/cairo3d/cairo_drawings4.png)

![ScreenShot](/Misc/cairo3d/cairo_drawings5.png)

![ScreenShot](/Misc/cairo3d/cairo_drawings6.png)

![ScreenShot](/Misc/cairo3d/cairo_drawings7.png)

![ScreenShot](/Misc/cairo3d/cairo_drawings8.png)

![ScreenShot](/Misc/cairo3d/cairo_text_drawing1.png)

![ScreenShot](/Misc/cairo3d/pie_chart3d_drawing1.png)

![ScreenShot](/Misc/cairo3d/histogram_chart3d_drawing1.png)

