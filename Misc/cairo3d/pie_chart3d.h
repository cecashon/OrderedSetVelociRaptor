/*
    See pie_chart3d_main.c for the test program.
*/

#include<glib.h>
#include<cairo.h>
#include<math.h>
#include<string.h>

struct pie_point3d{
  gdouble x;
  gdouble y;
  gdouble z;
};

struct pie_arc{
  struct pie_point3d a1;
  struct pie_point3d a2;
  struct pie_point3d a3;
  struct pie_point3d a4;
};

//A quad that can be sorted by point3d z-values.
struct pie_quad{
  struct pie_point3d a;
  struct pie_point3d b;
  struct pie_point3d c;
  struct pie_point3d d;
  gdouble color[4];
  guint section_id;
  gdouble section_explode;
};

//A section of pie.
struct section{
  guint section_id;
  gint start;
  gint end;
  gdouble color[4];
  //Multiplier for pie section vectors.
  gdouble section_explode;
};

struct pie_chart{
  guint pieces;
  gdouble radius;
  GArray *pie_chart1;
  GArray *pie_chart1_t;
  GArray *pie_chart2;
  GArray *pie_chart2_t;
  //Array of vectors to move the pie sections along.
  GArray *v_pie;
  GArray *v_pie_t;
  //Array for the location of the pie chart sections.
  GArray *sections;
  //An array for the section labels.
  GPtrArray *labels;
  //Reference vector for top of the ring or pie chart.
  gdouble top[3];
  gdouble top_t[3];
  //The depth of the pie chart from (0,0,0). The total z distance in 2*depth.
  gdouble depth;
  gboolean outline;
};

//Draw label lines with the pie chart.
enum{ 
  NO_LABEL_LINES,
  ADD_LABEL_LINES
};

/*
   A pie chart is made of pieces. For example 60 pieces. A section contains 1 or more pieces.
*/
struct pie_chart* pie_chart_new(guint pieces, gdouble radius, gdouble depth);
gboolean pie_chart_append_section(struct pie_chart *pc, guint end, gdouble rgba[4]);
void pie_chart_clear_sections(struct pie_chart *pc);
void pie_chart_append_section_label(struct pie_chart *pc, guint section, const gchar *label);
void pie_chart_clear_section_labels(struct pie_chart *pc);
void pie_chart_explode_section(struct pie_chart *pc, gint section_num, gdouble radius);
void pie_chart_explode_all(struct pie_chart *pc, gdouble radius);
void pie_chart_set_z(struct pie_chart *pc, gdouble z);
void pie_chart_free(struct pie_chart *pc);
void draw_pie_chart(cairo_t *cr, struct pie_chart *pc, gdouble qrs[9], guint label_lines);
/*
  The inside_ring value is clamped between 0.25<=inside_ring<=0.75 percent of the
  outside ring which can be easily changed in the function if needed. 
*/
void draw_pie_chart_ring(cairo_t *cr, struct pie_chart *pc, gdouble qrs[9], gdouble inside_ring, guint label_lines);
void pie_quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9]);



