/*   
    This is pie_chart1.c but using the pie chart static library.

    gcc -Wall pie_chart3d.c pie_chart3d_UI.c -o pie_chart3d `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu 20.04 with GTK3.24

    C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<string.h>
#include<math.h>
#include<cairo-svg.h>
#include<cairo-pdf.h>
#include<cairo-ps.h>
#include"pie_chart3d.h"

/*
  Used for calculating points in .svg, .pdf and .ps output.
  Why 90. This works on the test computer. When a .svg is saved and then opened, the pixels
match the original drawing. Rsvg rsvg_handle_set_dpi() notes "Common values are 75, 90,
and 300 DPI." which it looks like 90 is being used as default to open the picture. 
  The ppi might need to be changed for matching ouput on different computers.
*/
static gdouble ppi=90.0;
//Setting for UI.
static gint drawing_id=0;
static gdouble pitch=0.0;
static gdouble roll=0.0;
static gdouble yaw=0.0;
static gdouble scale=1.0;
static gint section_index=0;
static guint chart_pieces=60;
static guint chart_sections=1;
static gdouble chart_depth=20.0;
static gdouble explode_pieces=0.0;
static gint section_id=0;
static gdouble section_rgba[4]={0.0, 0.0, 0.0, 1.0};
static gint sweep=1;
static gdouble pie_explode=0.0;
static gdouble move_ring=0.25;
static guint tick_id=0;
static guint timer_id=0;
static guint draw_signal_id=0;
static gboolean frame_rate=FALSE;
static gboolean time_it=FALSE;
static guint label_lines_g=0;
static gdouble layout_width=450.0;
static gdouble layout_height=450.0;
static gdouble translate_x=0.0;
static gdouble translate_y=0.0;
static gdouble background_color[4]={1.0, 1.0, 1.0, 1.0};

//Set some variables from the UI.
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data);
static void set_animate_drawing(GtkComboBox *combo, gpointer *charts);
static void set_drawing_id(GtkComboBox *combo, gpointer *charts);
static void set_alpha(GtkComboBox *combo, gpointer *charts);
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void index_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void red_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void green_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void blue_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void alpha_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void depth_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void set_depth(GtkWidget *button, gpointer *charts);
static void set_section_color(GtkWidget *button, gpointer *charts);
static void pieces_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void create_new_chart(GtkWidget *button, gpointer *charts);
static void sections_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void create_new_section(GtkWidget *button, gpointer *charts);
static void explode_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void section_id_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void explode_all_sections(GtkWidget *button, gpointer *charts);
static void explode_single_section(GtkWidget *button, gpointer *charts);
static void show_frame_rate(GtkToggleButton *check_frame, gpointer data);
static void show_drawing_time(GtkToggleButton *check_time, gpointer data);
static void draw_label_lines(GtkToggleButton *check, gpointer data);
static gchar* save_file(gpointer *charts, gint surface_type);
static void draw_svg(GtkWidget *widget, gpointer *charts);
static void draw_pdf(GtkWidget *widget, gpointer *charts);
static void draw_ps(GtkWidget *widget, gpointer *charts);
static void draw_png(GtkWidget *widget, gpointer *charts);
static void draw_surface(cairo_t *cr, gpointer *charts, gint surface_type);
static gboolean redraw_clock(gpointer da);
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer *charts);
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer *data);
static GtkListStore* get_list_store(guint labels);
static void updata_chart_labels(GtkWidget *button, gpointer *charts2);
static void update_row_labels(GtkCellRendererText *renderer, gchar *path, gchar *new_text, gpointer data);
static void about_dialog(GtkWidget *widget, gpointer data);
static void translate_x_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void translate_y_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void layout_width_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void layout_height_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void set_background_color(GtkWidget *button, gpointer *data);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Pie Chart3d");
    gtk_window_set_default_size(GTK_WINDOW(window), 1000, 650);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_widget_set_app_paintable(window, TRUE);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    //Try to set transparency of main window.
    GdkScreen *screen=gtk_widget_get_screen(window); 
    if(gdk_screen_is_composited(screen))
      {
        GdkVisual *visual=gdk_screen_get_rgba_visual(screen);
        gtk_widget_set_visual(window, visual);
      }
    else
      {
        g_print("Can't set window transparency.\n");
      } 

    //Initialize a pie chart with 60 pieces.
    gint i=0;
    gdouble rgba[4]={1.0, 0.0, 0.0, 1.0};
    struct pie_chart *pc1=pie_chart_new(60, 150.0, 20.0);
    /*
      Need to fill sections of the chart in order. The append function looks at the previous 
      section for the start of the new section.
    */
    rgba[0]=1.0;rgba[1]=0.0;rgba[2]=0.0;rgba[3]=0.4;
    pie_chart_append_section(pc1, 9, rgba);
    rgba[0]=0.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.4;
    pie_chart_append_section(pc1, 29, rgba);
    rgba[0]=0.0;rgba[1]=0.0;rgba[2]=1.0;rgba[3]=0.4;
    pie_chart_append_section(pc1, 39, rgba);
    rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.4;
    pie_chart_append_section(pc1, 49, rgba);
    rgba[0]=1.0;rgba[1]=0.0;rgba[2]=1.0;rgba[3]=0.4;
    pie_chart_append_section(pc1, 59, rgba); 

    for(i=0;i<5;i++)
      {
        gchar *label=g_strdup_printf("Label %i", i);
        pie_chart_append_section_label(pc1, i, label);
        g_free(label);
      } 

    struct pie_chart *pc2=pie_chart_new(60, 150.0, 20.0);
    rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.8;
    pie_chart_append_section(pc2, 1, rgba);
    rgba[0]=0.0;rgba[1]=0.0;rgba[2]=1.0;rgba[3]=0.8;
    pie_chart_append_section(pc2, 59, rgba); 

    //A list store for saving and changing labels in the UI.
    GtkListStore *store=get_list_store(pc1->sections->len);

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);
    gpointer charts[5]={pc1, pc2, da, store, window}; 
    draw_signal_id=g_signal_connect(da, "draw", G_CALLBACK(draw_main), charts); 

    GtkWidget *da_scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(da_scroll, TRUE);
    gtk_widget_set_vexpand(da_scroll, TRUE);
    gtk_widget_set_size_request(da, 1000, 1000);
    gtk_container_add(GTK_CONTAINER(da_scroll), da);

    GtkWidget *combo_animate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_animate, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_animate), 0, "1", "Draw Pie Chart");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_animate), 1, "2", "Animate Pie Chart");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_animate), 0);
    g_signal_connect(combo_animate, "changed", G_CALLBACK(set_animate_drawing), charts);

    GtkWidget *combo_drawing=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_drawing, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "1", "Pie Chart3d");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "2", "Pie Chart3dx4");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 2, "3", "Pie Chart Sweep");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 3, "4", "Pie Chart Ring");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 4, "5", "Pie Chart Ring Sweep");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
    g_signal_connect(combo_drawing, "changed", G_CALLBACK(set_drawing_id), charts);

    //Set alpha on pc1.
    GtkWidget *combo_alpha=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_alpha, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_alpha), 0, "1", "alpha 0.4");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_alpha), 1, "2", "alpha 0.6");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_alpha), 2, "3", "alpha 0.8");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_alpha), 3, "4", "alpha 1.0");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_alpha), 0);
    g_signal_connect(combo_alpha, "changed", G_CALLBACK(set_alpha), charts);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, FALSE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, FALSE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, FALSE);

    GtkWidget *scale_label=gtk_label_new("Scale");
    gtk_widget_set_hexpand(scale_label, FALSE);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_hexpand(pitch_slider, FALSE);
    gtk_widget_set_vexpand(pitch_slider, TRUE);
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_pitch), da);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_hexpand(roll_slider, FALSE);
    gtk_widget_set_vexpand(roll_slider, TRUE);
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_roll), da);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_hexpand(yaw_slider, FALSE);
    gtk_widget_set_vexpand(yaw_slider, TRUE);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_yaw), da); 

    GtkWidget *scale_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 2.0, 0.01);
    gtk_widget_set_hexpand(scale_slider, FALSE);
    gtk_widget_set_vexpand(scale_slider, TRUE);
    gtk_range_set_value(GTK_RANGE(scale_slider), 1.0);
    g_signal_connect(scale_slider, "change-value", G_CALLBACK(set_scale), da); 

    GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
    gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
    g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), NULL);

    GtkWidget *check_time=gtk_check_button_new_with_label("Time Drawing");
    gtk_widget_set_halign(check_time, GTK_ALIGN_CENTER);
    g_signal_connect(check_time, "toggled", G_CALLBACK(show_drawing_time), NULL);
   
    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 10);
    gtk_grid_attach(GTK_GRID(grid1), combo_animate, 0, 0, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_drawing, 0, 1, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_alpha, 0, 2, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale_label, 3, 3, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale_slider, 3, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_frame, 0, 5, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_time, 0, 6, 4, 1);

    GtkWidget *scroll1=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll1, TRUE);
    gtk_widget_set_vexpand(scroll1, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll1), grid1);

    //The second tab in the notebook.
    GtkAdjustment *index_adj=gtk_adjustment_new(0.0, 0.0, 60.0, 1.0, 0.0, 0.0);
    GtkAdjustment *red_adj=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *green_adj=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *blue_adj=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *alpha_adj=gtk_adjustment_new(1.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *depth_adj=gtk_adjustment_new(20.0, 0.0, 100.0, 1.0, 0.0, 0.0);
    GtkAdjustment *pieces_adj=gtk_adjustment_new(60.0, 40.0, 120.0, 1.0, 0.0, 0.0);
    GtkAdjustment *sections_adj=gtk_adjustment_new(1.0, 1.0, 120.0, 1.0, 0.0, 0.0);

    GtkWidget *index_label=gtk_label_new("Section Index");
    gtk_widget_set_hexpand(index_label, FALSE);
    GtkWidget *index_spin=gtk_spin_button_new(index_adj, 1, 0);
    gtk_widget_set_hexpand(index_spin, FALSE);
    g_signal_connect(index_spin, "value-changed", G_CALLBACK(index_spin_changed), NULL);

    GtkWidget *rgba_label=gtk_label_new("RGBA");
    gtk_widget_set_hexpand(rgba_label, FALSE);
    GtkWidget *red_spin=gtk_spin_button_new(red_adj, 0.01, 2);
    gtk_widget_set_hexpand(red_spin, FALSE);
    g_signal_connect(red_spin, "value-changed", G_CALLBACK(red_spin_changed), NULL);

    GtkWidget *green_spin=gtk_spin_button_new(green_adj, 0.01, 2);
    gtk_widget_set_hexpand(green_spin, FALSE);
    g_signal_connect(green_spin, "value-changed", G_CALLBACK(green_spin_changed), NULL);

    GtkWidget *blue_spin=gtk_spin_button_new(blue_adj, 0.01, 2);
    gtk_widget_set_hexpand(blue_spin, FALSE);
    g_signal_connect(blue_spin, "value-changed", G_CALLBACK(blue_spin_changed), NULL);

    GtkWidget *alpha_spin=gtk_spin_button_new(alpha_adj, 0.01, 2);
    gtk_widget_set_hexpand(alpha_spin, FALSE);
    g_signal_connect(alpha_spin, "value-changed", G_CALLBACK(alpha_spin_changed), NULL);

    GtkWidget *depth_label=gtk_label_new("Depth");
    gtk_widget_set_hexpand(depth_label, FALSE);
    GtkWidget *depth_spin=gtk_spin_button_new(depth_adj, 20.0, 1);
    gtk_widget_set_hexpand(depth_spin, FALSE);
    g_signal_connect(depth_spin, "value-changed", G_CALLBACK(depth_spin_changed), NULL);

    GtkWidget *depth_button=gtk_button_new_with_label("Set Depth");
    gtk_widget_set_hexpand(depth_button, FALSE);
    g_signal_connect(depth_button, "clicked", G_CALLBACK(set_depth), charts);

    GtkWidget *section_color_button=gtk_button_new_with_label("Update Section Color");
    gtk_widget_set_hexpand(section_color_button, FALSE);
    g_signal_connect(section_color_button, "clicked", G_CALLBACK(set_section_color), charts);

    GtkWidget *pieces_label=gtk_label_new("Pie Chart Pieces");
    gtk_widget_set_hexpand(pieces_label, FALSE);
    GtkWidget *pieces_spin=gtk_spin_button_new(pieces_adj, 60.0, 0.0);
    gtk_widget_set_hexpand(pieces_spin, FALSE);
    g_signal_connect(pieces_spin, "value-changed", G_CALLBACK(pieces_spin_changed), NULL);

    GtkWidget *new_pie_button=gtk_button_new_with_label("New Pie Chart");
    gtk_widget_set_hexpand(new_pie_button, FALSE);
    g_signal_connect(new_pie_button, "clicked", G_CALLBACK(create_new_chart), charts);

    GtkWidget *sections_label=gtk_label_new("Section Size");
    gtk_widget_set_hexpand(sections_label, FALSE);
    GtkWidget *sections_spin=gtk_spin_button_new(sections_adj, 1.0, 0.0);
    gtk_widget_set_hexpand(sections_spin, FALSE);
    g_signal_connect(sections_spin, "value-changed", G_CALLBACK(sections_spin_changed), NULL);

    GtkWidget *new_section_button=gtk_button_new_with_label("New Section");
    gtk_widget_set_hexpand(new_section_button, FALSE);
    g_signal_connect(new_section_button, "clicked", G_CALLBACK(create_new_section), charts);

    GtkWidget *grid2=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid2), 8);
    gtk_grid_set_row_spacing(GTK_GRID(grid2), 8);
    gtk_grid_attach(GTK_GRID(grid2), index_label, 0, 0, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid2), index_spin, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), rgba_label, 0, 2, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid2), red_spin, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), green_spin, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), blue_spin, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), alpha_spin, 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), section_color_button, 1, 5, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid2), depth_label, 0, 6, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), depth_spin, 0, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), depth_button, 1, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), pieces_label, 0, 8, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), pieces_spin, 0, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), new_pie_button, 1, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), sections_label, 0, 10, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), sections_spin, 0, 11, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), new_section_button, 1, 11, 1, 1);

    GtkWidget *scroll2=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll2, TRUE);
    gtk_widget_set_vexpand(scroll2, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll2), grid2);

    GtkAdjustment *explode_adj=gtk_adjustment_new(0.0, 0.0, 100.0, 1.0, 0.0, 0.0);
    GtkAdjustment *section_id_adj=gtk_adjustment_new(0.0, 0.0, 120.0, 1.0, 0.0, 0.0);

    GtkWidget *explode_label=gtk_label_new("Explode");
    gtk_widget_set_hexpand(explode_label, FALSE);
    GtkWidget *explode_spin=gtk_spin_button_new(explode_adj, 0.0, 0.0);
    gtk_widget_set_hexpand(explode_spin, FALSE);
    g_signal_connect(explode_spin, "value-changed", G_CALLBACK(explode_spin_changed), NULL);

    GtkWidget *section_id_label=gtk_label_new("Section ID");
    gtk_widget_set_hexpand(section_id_label, FALSE);
    GtkWidget *section_id_spin=gtk_spin_button_new(section_id_adj, 1.0, 0.0);
    gtk_widget_set_hexpand(section_id_spin, FALSE);
    g_signal_connect(section_id_spin, "value-changed", G_CALLBACK(section_id_spin_changed), NULL);

    GtkWidget *explode_button=gtk_button_new_with_label("Explode ALL");
    gtk_widget_set_hexpand(explode_button, FALSE);
    g_signal_connect(explode_button, "clicked", G_CALLBACK(explode_all_sections), charts);

    GtkWidget *single_section_button=gtk_button_new_with_label("Explode Section");
    gtk_widget_set_hexpand(single_section_button, FALSE);
    g_signal_connect(single_section_button, "clicked", G_CALLBACK(explode_single_section), charts);

    GtkWidget *label_lines=gtk_check_button_new_with_label("Add Label Lines");
    gtk_widget_set_halign(label_lines, GTK_ALIGN_CENTER);
    g_signal_connect(label_lines, "toggled", G_CALLBACK(draw_label_lines), da);

    GtkWidget *tree=gtk_tree_view_new_with_model(GTK_TREE_MODEL(store));
    gtk_widget_set_hexpand(tree, FALSE);
    gtk_widget_set_vexpand(tree, TRUE);
    gtk_tree_view_set_activate_on_single_click(GTK_TREE_VIEW(tree), TRUE);
    g_object_unref(G_OBJECT(store));
     
    GtkCellRenderer *renderer1=gtk_cell_renderer_text_new();
    g_object_set(renderer1, "editable", FALSE, NULL);
    GtkCellRenderer *renderer2=gtk_cell_renderer_text_new();
    g_object_set(renderer2, "editable", TRUE, NULL);
    g_signal_connect(renderer2, "edited", G_CALLBACK(update_row_labels), store);
   
    GtkTreeViewColumn *column1=gtk_tree_view_column_new_with_attributes("Index", renderer1, "text", 0, NULL);
    GtkTreeViewColumn *column2=gtk_tree_view_column_new_with_attributes("Label", renderer2, "text", 1, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW(tree), column1);
    gtk_tree_view_append_column(GTK_TREE_VIEW(tree), column2);  

    GtkWidget *tree_scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_container_set_border_width(GTK_CONTAINER(tree_scroll), 5);
    gtk_widget_set_vexpand(tree_scroll, TRUE);
    gtk_widget_set_hexpand(tree_scroll, FALSE);
    gtk_container_add(GTK_CONTAINER(tree_scroll), tree); 

    //Just update labels in the first chart.
    GtkWidget *chart_labels=gtk_button_new_with_label("Update Chart Labels");
    gtk_widget_set_hexpand(chart_labels, FALSE);
    gpointer charts2[3]={da, pc1, tree};
    g_signal_connect(chart_labels, "clicked", G_CALLBACK(updata_chart_labels), charts2); 

    GtkWidget *grid3=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid3), 8);
    gtk_grid_set_row_spacing(GTK_GRID(grid3), 8);
    gtk_grid_attach(GTK_GRID(grid3), explode_label, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), section_id_label, 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), explode_spin, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), section_id_spin, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), explode_button, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), single_section_button, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), label_lines, 0, 3, 2, 1);
    gtk_grid_attach(GTK_GRID(grid3), tree_scroll, 0, 4, 2, 1);
    gtk_grid_attach(GTK_GRID(grid3), chart_labels, 0, 5, 2, 1);

    GtkWidget *scroll3=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll3, TRUE);
    gtk_widget_set_vexpand(scroll3, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll3), grid3);

    GtkAdjustment *translate_x_adj=gtk_adjustment_new(0.0, -500.0, 500.0, 10.0, 0.0, 0.0);
    GtkAdjustment *translate_y_adj=gtk_adjustment_new(0.0, -500.0, 500.0, 10.0, 0.0, 0.0);
    GtkAdjustment *layout_width_adj=gtk_adjustment_new(450.0, 1.0, 2000.0, 50.0, 0.0, 0.0);
    GtkAdjustment *layout_height_adj=gtk_adjustment_new(450.0, 1.0, 2000.0, 50.0, 0.0, 0.0);

    GtkWidget *translate_x_label=gtk_label_new("Translate x");
    gtk_widget_set_hexpand(translate_x_label, FALSE);
    GtkWidget *translate_x_spin=gtk_spin_button_new(translate_x_adj, 0.0, 1);
    gtk_widget_set_hexpand(translate_x_spin, FALSE);
    g_signal_connect(translate_x_spin, "value-changed", G_CALLBACK(translate_x_spin_changed), da); 

    GtkWidget *translate_y_label=gtk_label_new("Translate y");
    gtk_widget_set_hexpand(translate_y_label, FALSE);
    GtkWidget *translate_y_spin=gtk_spin_button_new(translate_y_adj, 0.0, 1);
    gtk_widget_set_hexpand(translate_y_spin, FALSE);
    g_signal_connect(translate_y_spin, "value-changed", G_CALLBACK(translate_y_spin_changed), da); 

    GtkWidget *layout_width_label=gtk_label_new("Layout Width");
    gtk_widget_set_hexpand(layout_width_label, FALSE);
    GtkWidget *layout_width_spin=gtk_spin_button_new(layout_width_adj, 450.0, 1);
    gtk_widget_set_hexpand(layout_width_spin, FALSE);
    g_signal_connect(layout_width_spin, "value-changed", G_CALLBACK(layout_width_spin_changed), da);

    GtkWidget *layout_height_label=gtk_label_new("Layout Height");
    gtk_widget_set_hexpand(layout_height_label, FALSE);
    GtkWidget *layout_height_spin=gtk_spin_button_new(layout_height_adj, 450.0, 1);
    gtk_widget_set_hexpand(layout_height_spin, FALSE);
    g_signal_connect(layout_height_spin, "value-changed", G_CALLBACK(layout_height_spin_changed), da);

    GtkWidget *background_label=gtk_label_new("Background Color");
    gtk_widget_set_hexpand(background_label, FALSE);
    GtkWidget *background_entry=gtk_entry_new();
    gtk_widget_set_hexpand(background_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(background_entry), "rgba(255, 255, 255, 1.0)");

    GtkWidget *background_button=gtk_button_new_with_label("Update Background Color");
    gtk_widget_set_hexpand(background_button, FALSE);
    gpointer w1[]={da, background_entry};
    g_signal_connect(background_button, "clicked", G_CALLBACK(set_background_color), w1);

    GtkWidget *grid4=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid4), 8);
    gtk_grid_set_row_spacing(GTK_GRID(grid4), 8);
    gtk_grid_attach(GTK_GRID(grid4), translate_x_label, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), translate_y_label, 1, 0, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid4), translate_x_spin, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), translate_y_spin, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), layout_width_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), layout_height_label, 1, 2, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid4), layout_width_spin, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), layout_height_spin, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), background_label, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), background_entry, 0, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), background_button, 0, 6, 1, 1);

    GtkWidget *scroll4=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll4, TRUE);
    gtk_widget_set_vexpand(scroll4, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll4), grid4);

    GtkWidget *nb_label1=gtk_label_new("Pie3d");
    GtkWidget *nb_label2=gtk_label_new("Options1");
    GtkWidget *nb_label3=gtk_label_new("Options2");
    GtkWidget *nb_label4=gtk_label_new("Options3");
    GtkWidget *notebook=gtk_notebook_new();
    gtk_container_set_border_width(GTK_CONTAINER(notebook), 15);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll1, nb_label1);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll2, nb_label2);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll3, nb_label3);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll4, nb_label4);

    GtkWidget *scroll5=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll5, TRUE);
    gtk_widget_set_vexpand(scroll5, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll5), notebook);

    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), scroll5, FALSE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da_scroll, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 375);

    GtkWidget *menu1=gtk_menu_new();
    GtkWidget *menu1item1=gtk_menu_item_new_with_label("Save as .svg");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item1);
    GtkWidget *menu1item2=gtk_menu_item_new_with_label("Save as .pdf");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item2);
    GtkWidget *menu1item3=gtk_menu_item_new_with_label("Save as .ps");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item3);
    GtkWidget *menu1item4=gtk_menu_item_new_with_label("Save as .png");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item4);
    GtkWidget *title1=gtk_menu_item_new_with_label("Save Drawing");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(title1), menu1);

    GtkWidget *menu2=gtk_menu_new();
    GtkWidget *menu2item1=gtk_menu_item_new_with_label("Pie Chart3d");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu2), menu2item1);
    GtkWidget *title2=gtk_menu_item_new_with_label("About");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(title2), menu2);

    GtkWidget *menu_bar=gtk_menu_bar_new();
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), title1);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), title2);
 
    g_signal_connect(menu1item1, "activate", G_CALLBACK(draw_svg), charts);
    g_signal_connect(menu1item2, "activate", G_CALLBACK(draw_pdf), charts);
    g_signal_connect(menu1item3, "activate", G_CALLBACK(draw_ps), charts);
    g_signal_connect(menu1item4, "activate", G_CALLBACK(draw_png), charts);
    g_signal_connect(menu2item1, "activate", G_CALLBACK(about_dialog), window);

    GtkWidget *grid5=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid5), menu_bar, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid5), paned1, 0, 1, 1, 1);

    //Draw background window based on the paned window splitter.
    g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), paned1);
   
    gtk_container_add(GTK_CONTAINER(window), grid5);

    gchar *css_string=g_strdup("#about_dialog{background: rgba(127,127,255,1.0); font-size: 20px; font-weight: bold;} #about_dialog label selection{background: rgba(0,50,220,0.5);}");
    GError *css_error=NULL;
    GtkCssProvider *provider=gtk_css_provider_new();
    gtk_css_provider_load_from_data(provider, css_string, -1, &css_error);
    gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    if(css_error!=NULL)
      {
        g_print("CSS loader error %s\n", css_error->message);
        g_error_free(css_error);
      }
    g_object_unref(provider);
    g_free(css_string);


    gtk_widget_show_all(window);

    gtk_main();

    //This chart may have been changed.
    pie_chart_free((struct pie_chart*)charts[0]);
    pie_chart_free(pc2);

    return 0;  
  }
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data)
  {
    //Paint background of drawing area transparent.
    cairo_set_source_rgba(cr, background_color[0], background_color[1], background_color[2], background_color[3]);
    cairo_paint(cr);
    //Paint the background under the grid.
    cairo_set_source_rgba(cr, 0.5, 0.5, 1.0, 1.0);
    gint width=gtk_paned_get_position(GTK_PANED(data));
    gint height=gtk_widget_get_allocated_height(widget);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_rectangle(cr, width-4, 0.0, 4, height);
    cairo_fill(cr);
    return FALSE;
  }
static void set_animate_drawing(GtkComboBox *combo, gpointer *charts)
  {   
    pitch=0.0;
    roll=0.0;
    yaw=0.0;
    struct pie_chart *pc=charts[0];
    GtkWidget *da=charts[2];

    //Reset pie chart.
    pie_chart_explode_all(pc, 0.0);
    pie_explode=0.0;
 
    if(gtk_combo_box_get_active(combo)==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(da), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(da));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(da), (GtkTickCallback)animate_drawing, charts, NULL);
          }
      }
    
  }
static void set_drawing_id(GtkComboBox *combo, gpointer *charts)
  {  
    drawing_id=gtk_combo_box_get_active(combo);
    struct pie_chart *pc=charts[0];
    GtkWidget *da=charts[2];

    //Reset pie chart.
    pie_chart_explode_all(pc, 0.0);
    pie_explode=0.0;

    if(drawing_id==2&&timer_id==0)
      {
        timer_id=g_timeout_add_seconds(3, (GSourceFunc)redraw_clock, da);
      }
    else if(timer_id!=0)
      {
        g_source_remove(timer_id);
        timer_id=0;
      }

    gtk_widget_queue_draw(GTK_WIDGET(da));
  }
static void set_alpha(GtkComboBox *combo, gpointer *charts)
  {
    gint i=0;
    gdouble alpha=0.0;
    gint combo_id=gtk_combo_box_get_active(combo);
    struct section *s1=&g_array_index(((struct pie_chart*)(charts[0]))->sections, struct section, 0);

    if(combo_id==0) alpha=0.4;
    else if(combo_id==1) alpha=0.6;
    else if(combo_id==2) alpha=0.8;
    else alpha=1.0;

    for(i=0;i<(((struct pie_chart*)(charts[0]))->sections->len);i++)
      {
        s1->color[3]=alpha;
        s1++;
      }

    gtk_widget_queue_draw(GTK_WIDGET(charts[2]));
  }
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    pitch=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    roll=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    yaw=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>2.0) scale=2.0;
    else if(value<0.0) scale=0.0;
    else scale=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void index_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    section_index=gtk_spin_button_get_value_as_int(spin_button);
  }
static void red_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    section_rgba[0]=gtk_spin_button_get_value(spin_button);
  }
static void green_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    section_rgba[1]=gtk_spin_button_get_value(spin_button);
  }
static void blue_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    section_rgba[2]=gtk_spin_button_get_value(spin_button);
  }
static void alpha_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    section_rgba[3]=gtk_spin_button_get_value(spin_button);
  }
static void depth_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_depth=gtk_spin_button_get_value(spin_button);
  }
static void set_depth(GtkWidget *button, gpointer *charts)
  {
    struct pie_chart *pc=charts[0];
    GtkWidget *da=charts[2];

    if(chart_depth<0.0)
      {
        g_warning("Chart depth is greater than 0.0.. The depth wasn't set.\n");
      }
    else
      {
        //Block the draw and tick when updating the array.
        gboolean was_ticking=FALSE;
        g_signal_handler_block(da, draw_signal_id);
        if(tick_id!=0)
          {
            gtk_widget_remove_tick_callback(GTK_WIDGET(da), tick_id);
            tick_id=0;
            was_ticking=TRUE;
          }

        pie_chart_set_z(pc, chart_depth);

        g_signal_handler_unblock(da, draw_signal_id);
        if(was_ticking)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(da), (GtkTickCallback)animate_drawing, charts, NULL);
          }

        gtk_widget_queue_draw(GTK_WIDGET(da));
      }
  }
static void set_section_color(GtkWidget *button, gpointer *charts)
  {
    struct pie_chart *pc=charts[0];
    GtkWidget *da=charts[2];

    if(section_index<0||section_index>(pc->sections->len)-1)
      {
        g_warning("Section index out of range. The color wasn't set.\n");
      }
    else
      {
        //Block the draw and tick when updating the array.
        gboolean was_ticking=FALSE;
        g_signal_handler_block(da, draw_signal_id);
        if(tick_id!=0)
          {
            gtk_widget_remove_tick_callback(GTK_WIDGET(da), tick_id);
            tick_id=0;
            was_ticking=TRUE;
          }

        struct section *s1=&g_array_index(pc->sections, struct section, section_index);
        s1->color[0]=section_rgba[0];s1->color[1]=section_rgba[1];
        s1->color[2]=section_rgba[2];s1->color[3]=section_rgba[3];

        g_signal_handler_unblock(da, draw_signal_id);
        if(was_ticking)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(da), (GtkTickCallback)animate_drawing, charts, NULL);
          }

        gtk_widget_queue_draw(GTK_WIDGET(da));
      }
  }
static void pieces_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_pieces=gtk_spin_button_get_value(spin_button);
  }
static void create_new_chart(GtkWidget *button, gpointer *charts)
  {
    struct pie_chart *pc1=charts[0];
    GtkWidget *da=charts[2];
    gboolean was_ticking=FALSE;

    //Block the draw and tick when updating the array.
    g_signal_handler_block(da, draw_signal_id);
    if(tick_id!=0)
      {
        gtk_widget_remove_tick_callback(GTK_WIDGET(da), tick_id);
        tick_id=0;
        was_ticking=TRUE;
      }

    //Free the previous chart.
    if(pc1!=NULL)
      {
        pie_chart_free(pc1);
        gtk_list_store_clear(GTK_LIST_STORE(charts[3]));
      }

    //Get a new pie chart.    
    charts[0]=pie_chart_new(chart_pieces, 150.0, 20.0);

    g_signal_handler_unblock(da, draw_signal_id);
    if(was_ticking)
      {
        tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(da), (GtkTickCallback)animate_drawing, charts, NULL);
      }

    gtk_widget_queue_draw(GTK_WIDGET(da));
  }
static void sections_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_sections=gtk_spin_button_get_value(spin_button);
  }
static void create_new_section(GtkWidget *button, gpointer *charts)
  {
    struct pie_chart *pc1=charts[0];
    GtkWidget *da=charts[2];
    gboolean was_ticking=FALSE;

    //Block the draw and tick when updating the array.
    g_signal_handler_block(da, draw_signal_id);
    if(tick_id!=0)
      {
        gtk_widget_remove_tick_callback(GTK_WIDGET(da), tick_id);
        tick_id=0;
        was_ticking=TRUE;
      }

    guint end=0;
    struct section *s1=NULL;
    if((pc1->sections->len)==0)
      {
        end=chart_sections-1;
      }
    else
      {
        s1=&g_array_index(pc1->sections, struct section, (pc1->sections->len)-1);
        end=(s1->end)+chart_sections;
      }
    
    gdouble rgba[4]={0.0, 0.0, 1.0, 0.5};
    gboolean error=FALSE;
    error=pie_chart_append_section(pc1, end, rgba);

    if(!error)
      {
        //Add a label to the chart and the list store.
        GtkTreeIter iter1;
        gchar *label=g_strdup_printf("Label %i", (pc1->sections->len)-1);
        pie_chart_append_section_label(pc1, (pc1->sections->len)-1, label);
        gtk_list_store_append(GTK_LIST_STORE(charts[3]), &iter1);
        gtk_list_store_set(GTK_LIST_STORE(charts[3]), &iter1, 0, (pc1->sections->len)-1, 1, label, -1);
        g_free(label);
      }

    g_signal_handler_unblock(da, draw_signal_id);
    if(was_ticking)
      {
        tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(da), (GtkTickCallback)animate_drawing, charts, NULL);
      }

    gtk_widget_queue_draw(GTK_WIDGET(da));
  }
static void explode_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    explode_pieces=gtk_spin_button_get_value(spin_button);
  }
static void section_id_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    section_id=gtk_spin_button_get_value_as_int(spin_button);
  }
static void explode_all_sections(GtkWidget *button, gpointer *charts)
  {
    struct pie_chart *pc=charts[0];
    GtkWidget *da=charts[2];
    pie_chart_explode_all(pc, explode_pieces);
    gtk_widget_queue_draw(GTK_WIDGET(da));
  }
static void explode_single_section(GtkWidget *button, gpointer *charts)
  {
    struct pie_chart *pc=charts[0];
    GtkWidget *da=charts[2];
    pie_chart_explode_section(pc, section_id, explode_pieces);
    gtk_widget_queue_draw(GTK_WIDGET(da));
  }
static void show_frame_rate(GtkToggleButton *check_frame, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_frame)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_drawing_time(GtkToggleButton *check_time, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_time)) time_it=TRUE;
    else time_it=FALSE;
  }
static void draw_label_lines(GtkToggleButton *check, gpointer data)
  {
    if(gtk_toggle_button_get_active(check)) label_lines_g=ADD_LABEL_LINES;
    else label_lines_g=NO_LABEL_LINES;

    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static gchar* save_file(gpointer *charts, gint surface_type)
  {
    int result=0;

    GtkWidget *dialog=gtk_file_chooser_dialog_new("Save File", GTK_WINDOW(charts[4]), GTK_FILE_CHOOSER_ACTION_SAVE, "Cancel", GTK_RESPONSE_CANCEL, "Save", GTK_RESPONSE_ACCEPT, NULL);

    if(surface_type==0) gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".svg");
    else if(surface_type==1) gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".pdf");
    else if(surface_type==2) gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".ps");
    else gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".png"); 

    result=gtk_dialog_run(GTK_DIALOG(dialog));
    gchar *file_name=NULL;
    if(result==GTK_RESPONSE_ACCEPT)
      {

        file_name=gtk_file_chooser_get_current_name(GTK_FILE_CHOOSER(dialog));
      }

    gtk_widget_destroy(dialog);

    return file_name;
  }
static void draw_svg(GtkWidget *widget, gpointer *charts)
  {
    gchar *file_name=save_file(charts, 0);
    
    //Scale factor to convert to points.
    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_svg_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, charts, 0);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name);
  }
static void draw_pdf(GtkWidget *widget, gpointer *charts)
  {
    gchar *file_name=save_file(charts, 1);

    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_pdf_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, charts, 1);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }
    
    g_free(file_name);
  }
static void draw_ps(GtkWidget *widget, gpointer *charts)
  {
    gchar *file_name=save_file(charts, 2);

    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_ps_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, charts, 2);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name);    
  }
static void draw_png(GtkWidget *widget, gpointer *charts)
  {
    gchar *file_name=save_file(charts, 3);

    if(file_name!=NULL)
      {
        cairo_surface_t *surface=cairo_image_surface_create(CAIRO_FORMAT_ARGB32, layout_width, layout_height);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, charts, 3);
        cairo_surface_write_to_png(surface, file_name);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name);    
  }
static void draw_surface(cairo_t *cr, gpointer *charts, gint surface_type)
  {
    struct pie_chart *pc1=charts[0];
    struct pie_chart *pc2=charts[1];
    gdouble points_scale=0.0;

    if(surface_type<3) points_scale=72.0/ppi;
    else points_scale=1.0;

    //Background.
    cairo_set_source_rgba(cr, background_color[0], background_color[1], background_color[2], background_color[3]);
    cairo_paint(cr); 

    //Rotate the pie chart.
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    gdouble qrs[9]; 
    pie_quaternion_rotation(yaw1, roll1, pitch1, qrs);

    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);        
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_translate(cr, layout_width*points_scale/2.0, layout_height*points_scale/2.0); 
    cairo_translate(cr, translate_x, translate_y);
    cairo_scale(cr, scale*points_scale+0.01, scale*points_scale+0.01);

    if(drawing_id==0)
      {
        pc1->outline=TRUE;        
        draw_pie_chart(cr, pc1, qrs, label_lines_g);
      }
    else if(drawing_id==1)
      {
        pc1->outline=TRUE;
        cairo_save(cr);
        cairo_translate(cr, -layout_width/5.0, -layout_height/5.0);
        cairo_scale(cr, 0.5, 0.5);
        draw_pie_chart(cr, pc1, qrs, label_lines_g);
        cairo_restore(cr);
        cairo_save(cr);
        cairo_translate(cr, layout_width/5.0, -layout_height/5.0);
        cairo_scale(cr, 0.5, 0.5);
        draw_pie_chart(cr, pc1, qrs, label_lines_g);
        cairo_restore(cr);
        cairo_save(cr);
        cairo_translate(cr, -layout_width/5.0, layout_height/5.0);
        cairo_scale(cr, 0.5, 0.5);
        draw_pie_chart(cr, pc1, qrs, label_lines_g);
        cairo_restore(cr);
        cairo_save(cr);
        cairo_translate(cr, layout_width/5.0, layout_height/5.0);
        cairo_scale(cr, 0.5, 0.5);
        draw_pie_chart(cr, pc1, qrs, label_lines_g);
        cairo_restore(cr);
      }
     else if(drawing_id==2)
      {
        gdouble rgba[4]={0.0, 0.0, 1.0, 0.8};
        pie_chart_clear_sections(pc2);
        if(sweep==0)
          {
            pie_chart_append_section(pc2, 59, rgba);
          }
        else if(sweep==59)
          {
            rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.8;
            pie_chart_append_section(pc2, sweep, rgba);
          } 
        else
          {
            rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.8;
            pie_chart_append_section(pc2, sweep, rgba);
            rgba[0]=0.0;rgba[1]=0.0;rgba[2]=1.0;rgba[3]=0.8;
            pie_chart_append_section(pc2, 59, rgba);
          }
        pc2->outline=FALSE;        
        draw_pie_chart(cr, pc2, qrs, label_lines_g);
      }
    else if(drawing_id==3)
      {
        pc1->outline=TRUE;       
        draw_pie_chart_ring(cr, pc1, qrs, 0.5, label_lines_g);
      }
    else
      {
        gdouble rgba[4]={1.0, 0.0, 1.0, 0.4};
        pie_chart_clear_sections(pc2);
        if(sweep==0)
          {
            pie_chart_append_section(pc2, 59, rgba);
          }
        else if(sweep==59)
          {
            rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.4;
            pie_chart_append_section(pc2, sweep, rgba);
          } 
        else
          {
            rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.4;
            pie_chart_append_section(pc2, sweep, rgba);
            rgba[0]=1.0;rgba[1]=0.0;rgba[2]=1.0;rgba[3]=0.4;
            pie_chart_append_section(pc2, 59, rgba);
          }
        pc2->outline=TRUE;        
        draw_pie_chart_ring(cr, pc2, qrs, move_ring, label_lines_g);
      }
  }
static gboolean redraw_clock(gpointer da)
  {
    gtk_widget_queue_draw(GTK_WIDGET(da));
    return TRUE;
  }
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer *charts)
  {
    struct pie_chart *pc=charts[0];

    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    if(pitch>360.0) pitch=0.0;
    else pitch+=0.5;
    if(roll>360.0) roll=0.0;
    else roll+=0.5;
    if(yaw>360.0) yaw=0.0;
    else yaw+=0.5;
    if(pie_explode>100.0)
      {
        pie_explode=0.0;
        if(drawing_id==1) pie_chart_explode_section(pc, 2, pie_explode);
        else pie_chart_explode_all(pc, pie_explode);
      }
    else
      {
        pie_explode+=0.5;
        if(drawing_id==1) pie_chart_explode_section(pc, 2, pie_explode);
        else pie_chart_explode_all(pc, pie_explode);
      }
    if(sweep>58) sweep=0;
    else sweep++;
    if(move_ring>0.75) move_ring=0.25;
    else move_ring+=0.005;

    gtk_widget_queue_draw(GTK_WIDGET(drawing));
    return G_SOURCE_CONTINUE;
  }
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer *charts)
  {  
    gdouble w1=layout_width/10.0;
    gdouble h1=layout_height/10.0;
    struct pie_chart *pc1=charts[0];
    struct pie_chart *pc2=charts[1];

    //Background.
    cairo_set_source_rgba(cr, background_color[0], background_color[1], background_color[2], background_color[3]);
    cairo_paint(cr); 

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 2.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Layout bounding box.
    cairo_rectangle(cr, 0.0, 0.0, layout_width, layout_height);
    cairo_stroke(cr);
  
    if((pc1->sections->len)==0||(pc2->sections->len)==0)
      {
        g_print("There are no pie sections to draw!\n");
        goto exit;
      }

    GTimer *timer=NULL; 
    if(time_it==TRUE) timer=g_timer_new();

    //Rotate the pie chart.
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    gdouble qrs[9]; 
    pie_quaternion_rotation(yaw1, roll1, pitch1, qrs);

    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);        
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_translate(cr, layout_width/2.0, layout_height/2.0); 
    cairo_translate(cr, translate_x, translate_y);
    cairo_scale(cr, scale+0.01, scale+0.01);

    if(drawing_id==0)
      {
        pc1->outline=TRUE;        
        draw_pie_chart(cr, pc1, qrs, label_lines_g);
      }
    else if(drawing_id==1)
      {
        pc1->outline=TRUE;
        cairo_save(cr);
        cairo_translate(cr, -2.0*w1, -2.0*h1);
        cairo_scale(cr, 0.5, 0.5);
        draw_pie_chart(cr, pc1, qrs, label_lines_g);
        cairo_restore(cr);
        cairo_save(cr);
        cairo_translate(cr, 2.0*w1, -2.0*h1);
        cairo_scale(cr, 0.5, 0.5);
        draw_pie_chart(cr, pc1, qrs, label_lines_g);
        cairo_restore(cr);
        cairo_save(cr);
        cairo_translate(cr, -2.0*w1, 2.0*h1);
        cairo_scale(cr, 0.5, 0.5);
        draw_pie_chart(cr, pc1, qrs, label_lines_g);
        cairo_restore(cr);
        cairo_save(cr);
        cairo_translate(cr, 2.0*w1, 2.0*h1);
        cairo_scale(cr, 0.5, 0.5);
        draw_pie_chart(cr, pc1, qrs, label_lines_g);
        cairo_restore(cr);
      }
     else if(drawing_id==2)
      {
        gdouble rgba[4]={0.0, 0.0, 1.0, 0.8};
        pie_chart_clear_sections(pc2);
        if(sweep==0)
          {
            pie_chart_append_section(pc2, 59, rgba);
          }
        else if(sweep==59)
          {
            rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.8;
            pie_chart_append_section(pc2, sweep, rgba);
          } 
        else
          {
            rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.8;
            pie_chart_append_section(pc2, sweep, rgba);
            rgba[0]=0.0;rgba[1]=0.0;rgba[2]=1.0;rgba[3]=0.8;
            pie_chart_append_section(pc2, 59, rgba);
          }
        pc2->outline=FALSE;        
        draw_pie_chart(cr, pc2, qrs, label_lines_g);
      }
    else if(drawing_id==3)
      {
        pc1->outline=TRUE;       
        draw_pie_chart_ring(cr, pc1, qrs, 0.5, label_lines_g);
      }
    else
      {
        gdouble rgba[4]={1.0, 0.0, 1.0, 0.4};
        pie_chart_clear_sections(pc2);
        if(sweep==0)
          {
            pie_chart_append_section(pc2, 59, rgba);
          }
        else if(sweep==59)
          {
            rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.4;
            pie_chart_append_section(pc2, sweep, rgba);
          } 
        else
          {
            rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.4;
            pie_chart_append_section(pc2, sweep, rgba);
            rgba[0]=1.0;rgba[1]=0.0;rgba[2]=1.0;rgba[3]=0.4;
            pie_chart_append_section(pc2, 59, rgba);
          }
        pc2->outline=TRUE;        
        draw_pie_chart_ring(cr, pc2, qrs, move_ring, label_lines_g);
      }
     
    if(time_it==TRUE) 
     {
       g_print("Draw Time %f\n", g_timer_elapsed(timer, NULL));
       g_timer_destroy(timer);
     }

    exit:
    return FALSE;
  }
static GtkListStore* get_list_store(guint labels)
  {
    gint i=0;
    GtkListStore *store=gtk_list_store_new(2, G_TYPE_INT, G_TYPE_STRING);
        
    GtkTreeIter iter1;
    for(i=0;i<labels;i++)
      {
        gchar *string1=g_strdup_printf("Label %i", i);
        gtk_list_store_append(store, &iter1);
        gtk_list_store_set(store, &iter1, 0, i, 1, string1, -1);
        g_free(string1);
      }
  
    return store;
  }
static void updata_chart_labels(GtkWidget *button, gpointer *charts2)
  {
    gint i=0;
    GtkTreeIter iter;
    gint index=0;
    struct pie_chart *pc1=charts2[1];
    GtkTreeModel *model=gtk_tree_view_get_model(GTK_TREE_VIEW(charts2[2]));

    pie_chart_clear_section_labels(pc1);
    gtk_tree_model_get_iter_first(model, &iter);
    for(i=0;i<(pc1->sections->len);i++)
      {
        gchar *label=NULL;
        gtk_tree_model_get(model, &iter, 0, &index, 1, &label, -1);
        pie_chart_append_section_label(pc1, i, label);
        gtk_tree_model_iter_next(model, &iter);
        g_free(label);
      }

    gtk_widget_queue_draw(GTK_WIDGET(charts2[0]));
  }
static void update_row_labels(GtkCellRendererText *renderer, gchar *path, gchar *new_text, gpointer data)
  {
    GtkTreeIter iter;
    GValue value=G_VALUE_INIT;
    g_value_init(&value, G_TYPE_STRING);
    g_value_set_static_string(&value, new_text);
    GtkTreePath *tree_path=gtk_tree_path_new_from_string(path);
    gtk_tree_model_get_iter(GTK_TREE_MODEL(data), &iter, tree_path);
    gtk_list_store_set_value(GTK_LIST_STORE(data), &iter, 1, &value);
    gtk_tree_path_free(tree_path);
    g_value_unset(&value);
  }
static void about_dialog(GtkWidget *widget, gpointer data)
  {
    GtkWidget *dialog=gtk_about_dialog_new();
    gtk_widget_set_name(dialog, "about_dialog");
    gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(data));
    //gtk_about_dialog_set_logo(GTK_ABOUT_DIALOG(dialog), NULL);
    gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(dialog), "Pie Chart3d");
    gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog), "Test Version 1.0");
    gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(dialog), "3d pie charts.");
    gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(dialog), "(C) 2019 C. Eric Cashon");

    gtk_widget_show_all(dialog);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
  }
static void translate_x_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    translate_x=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void translate_y_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    translate_y=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void layout_width_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    layout_width=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void layout_height_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    layout_height=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_background_color(GtkWidget *button, gpointer *data)
  {
    GdkRGBA rgba;
    
    if(gdk_rgba_parse(&rgba, gtk_entry_get_text(GTK_ENTRY(data[1]))))
      {
        background_color[0]=rgba.red;
        background_color[1]=rgba.green;
        background_color[2]=rgba.blue;
        background_color[3]=rgba.alpha;
      }
    else
      {
        g_print("Background color string format error.\n");
      } 

    gtk_widget_queue_draw(GTK_WIDGET(data[0]));
  }


