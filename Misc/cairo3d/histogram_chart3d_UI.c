
/*
  This is histogram_chart2.c but using the histogram static library.

  The gradient drawings use both a linear gradient and a mesh gradient. The linear drawings
wiil save as a "text" svg and the mesh gradient as an image svg. The mesh gradient isn't part
of the svg 1.0 specification so it gets rasterized. 

  gcc -Wall histogram_chart3d.c histogram_chart3d_UI.c -o histogram_chart3d `pkg-config --cflags --libs gtk+-3.0` -lm

  Tested on Ubuntu20.04 and GTK3.24

  C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<cairo-svg.h>
#include<cairo-pdf.h>
#include<cairo-ps.h>
#include"histogram_chart3d.h"

/*
  Used for calculating points in .svg, .pdf and .ps output.
  Why 90. This works on the test computer. When a .svg is saved and then opened, the pixels
match the original drawing. Rsvg rsvg_handle_set_dpi() notes "Common values are 75, 90,
and 300 DPI." which it looks like 90 is being used as default to open the picture. 
  The ppi might need to be changed for matching ouput on different computers.
*/
static gdouble ppi=90.0;
//Some settings for the UI.
static gint drawing_id=0;
static gint chart_rows=7;
static gint chart_columns=9;
static gint chart_outside_square=40;
static gint chart_inside_square=20;
static guint chart_font_size=20;
static guint chart_font_position=FONT_MIDDLE;
static gint chart_index=0;
static gdouble zvalue=0.0;
static gdouble chart_rgba[4]={0.0, 0.0, 0.0, 1.0};
static gdouble pitch=0.0;
static gdouble roll=0.0;
static gdouble yaw=0.0;
static gdouble scale=1.0;
static gdouble perspective=0.0;
static guint tick_id=0;
static gboolean frame_rate=FALSE;
static gboolean time_it=FALSE;
static guint draw_signal_id=0;
static gdouble layout_width=500.0;
static gdouble layout_height=500.0;
static gdouble translate_x=0.0;
static gdouble translate_y=0.0;
static gdouble background_color[4]={1.0, 1.0, 1.0, 1.0};
//Chart drawing variables.
static guint line_id=0;
static gdouble point_xyz[3]={0.0,0.0,0.0};
static guint line_id2=0;
static gdouble current_rgba[4]={0.0, 0.0, 0.0, 1.0};
static gdouble grad_color[3]={0.0, 1.0, 0.0};
static gdouble line_color[4]={1.0, 0.0, 1.0, 1.0};
static gdouble point_color[4]={1.0, 1.0, 0.0, 1.0};
static gdouble line_width=10.0;
//Chart data from key value string.
static GPtrArray *chart_groups=NULL;
//Starting test string for key value text view.
static const gchar *key_value_string=\
                   "[Set1]\n"
                   "line_color=1.0;0.0;0.0;1.0\n"
                   "point_color=1.0;1.0;0.0;1.0\n"
                   "line_width=8.0\n"
                   "data="
                   "0.0;0.0;0.0;"
                   "20.0;20.0;2.0;"
                   "40.0;40.0;4.0;"
                   "60.0;60.0;16.0;"
                   "80.0;80.0;32.0;"
                   "100.0;100.0;64.0;"
                   "120.0;120.0;128.0;"
                   "140.0;140.0;256.0\n"
                   "[Set2]\n"
                   "line_color=0.0;1.0;0.0;1.0\n"
                   "point_color=1.0;1.0;0.0;1.0\n"
                   "line_width=8.0\n"
                   "data="
                   "0.0;0.0;0.0;"
                   "-20.0;20.0;2.0;"
                   "-40.0;40.0;4.0;"
                   "-60.0;60.0;16.0;"
                   "-80.0;80.0;32.0;"
                   "-100.0;100.0;64.0;"
                   "-120.0;120.0;128.0;"
                   "-140.0;140.0;256.0\n"
                   "[Set3]\n"
                   "line_color=0.0;0.0;1.0;1.0\n"
                   "point_color=1.0;1.0;0.0;1.0\n"
                   "line_width=8.0\n"
                   "data="
                   "0.0;0.0;0.0;"
                   "20.0;-20.0;2.0;"
                   "40.0;-40.0;4.0;"
                   "60.0;-60.0;16.0;"
                   "80.0;-80.0;32.0;"
                   "100.0;-100.0;64.0;"
                   "120.0;-120.0;128.0;"
                   "140.0;-140.0;256.0\n"
                   "[Set4]\n"
                   "line_color=1.0;0.0;1.0;1.0\n"
                   "point_color=1.0;1.0;0.0;1.0\n"
                   "line_width=8.0\n"
                   "data="
                   "0.0;0.0;0.0;"
                   "-20.0;-20.0;2.0;"
                   "-40.0;-40.0;4.0;"
                   "-60.0;-60.0;16.0;"
                   "-80.0;-80.0;32.0;"
                   "-100.0;-100.0;64.0;"
                   "-120.0;-120.0;128.0;"
                   "-140.0;-140.0;256.0";

//The UI part.
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data);
static GtkListStore* get_list_store(guint labels, const gchar *label);
static void update_list_store(GtkListStore *store, guint labels, const gchar *label);
static void set_animate_drawing(GtkComboBox *combo, gpointer *parts);
static void rows_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void columns_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void outside_square_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void inside_square_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void set_chart(GtkWidget *button, gpointer *parts);
static void index_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void zvalue_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void set_zvalue(GtkWidget *button, gpointer *parts);
static void red_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void green_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void blue_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void alpha_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void set_bar_color(GtkWidget *button, gpointer *parts);
static void set_base_color(GtkWidget *button, gpointer *parts);
static void set_base_color_all(GtkWidget *button, gpointer *parts);
static void set_back_color(GtkWidget *button, gpointer *parts);
static void back_height_spin_changed(GtkSpinButton *spin_button, gpointer *parts);
static void back_rows_spin_changed(GtkSpinButton *spin_button, gpointer *parts);
static void set_drawing_id(GtkComboBox *combo, gpointer da);
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void show_frame_rate(GtkToggleButton *check_frame, gpointer data);
static void show_drawing_time(GtkToggleButton *check_time, gpointer data);
static gboolean animate_drawing(GtkWidget *da, GdkFrameClock *frame_clock, gpointer *parts);
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer *parts);
static gchar* save_file(gpointer *parts, gint surface_type);
static void draw_svg(GtkWidget *widget, gpointer *parts);
static void draw_pdf(GtkWidget *widget, gpointer *parts);
static void draw_ps(GtkWidget *widget, gpointer *parts);
static void draw_png(GtkWidget *widget, gpointer *parts);
static void draw_surface(cairo_t *cr, gpointer *parts, guint surface_type);
static void about_dialog(GtkWidget *widget, gpointer data);
static void translate_x_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void translate_y_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void layout_width_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void layout_height_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void set_background_color(GtkWidget *button, gpointer *data);
static void red_grad_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void green_grad_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void blue_grad_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void set_bottom_grad(GtkWidget *button, gpointer data);
static void set_middle_grad(GtkWidget *button, gpointer data);
static void set_top_grad(GtkWidget *button, gpointer data);
static void change_label_store(GtkComboBox *combo, gpointer *parts);
static void update_row_labels(GtkCellRendererText *renderer, gchar *path, gchar *new_text, gpointer *parts);
static void update_labels_empty(GtkWidget *button, gpointer *parts);
static void updata_chart_labels(GtkWidget *button, gpointer *parts);
static void font_size_changed(GtkSpinButton *spin_button, gpointer data);
static void font_position_changed(GtkComboBox *combo, gpointer data);
static void red_spin_changed2(GtkSpinButton *spin_button, gpointer data);
static void green_spin_changed2(GtkSpinButton *spin_button, gpointer data);
static void blue_spin_changed2(GtkSpinButton *spin_button, gpointer data);
static void alpha_spin_changed2(GtkSpinButton *spin_button, gpointer data);
static void set_line_color(GtkWidget *button, gpointer *parts);
static void set_point_color(GtkWidget *button, gpointer *parts);
static void line_width_spin_changed(GtkSpinButton *spin_button, gpointer *parts);
static void set_line_id(GtkComboBox *combo, gpointer *parts);
static void point_x_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void point_y_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void point_z_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void set_line_id2(GtkComboBox *combo, gpointer *parts);
static void delete_points(GtkWidget *button, gpointer *parts);
static void add_point(GtkWidget *button, gpointer *parts);
//Initialize a histogram chart with some cubes, cylinders and data to plot.
static struct histogram_chart* initialize_histogram_chart(guint rows, guint columns, gdouble outside_square);
static struct chart_data* initialize_cubes(struct histogram_chart *hc, gdouble inside_square, guint inside_square_position);
static struct chart_data_aligned* initialize_cylinders(struct histogram_chart *hc, gint sections, gdouble radius);
static struct chart_data* initialize_test_points();
//Key value parse function.
static void parse_key_value_text(GtkWidget *button, gpointer *parse_data);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Histogram Chart3d");
    gtk_window_set_default_size(GTK_WINDOW(window), 1000, 650);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_widget_set_app_paintable(window, TRUE);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    //Try to set transparency of main window.
    GdkScreen *screen=gtk_widget_get_screen(window); 
    if(gdk_screen_is_composited(screen))
      {
        GdkVisual *visual=gdk_screen_get_rgba_visual(screen);
        gtk_widget_set_visual(window, visual);
      }
    else
      {
        g_print("Can't set window transparency.\n");
      } 

    /*
      Initialize a histogram chart. The outside_square is the base square size and the inside_square
      is the bar square size. The bar square is smaller than or equal to the base square.
    */
    struct histogram_chart *hc=initialize_histogram_chart(chart_rows, chart_columns, chart_outside_square);

    //List stores for saving and changing labels in the UI.
    gchar *label1=g_strdup("Lbx");
    GtkListStore *store_xr=get_list_store((hc->rows), label1);
    GtkListStore *store_xl=get_list_store((hc->rows), label1);
    g_free(label1);
    gchar *label2=g_strdup("Lby");
    GtkListStore *store_yr=get_list_store((hc->columns), label2);    
    GtkListStore *store_yl=get_list_store((hc->columns), label2);
    g_free(label2);
    gchar *label3=g_strdup("Lbz");
    GtkListStore *store_zr=get_list_store((hc->back_rows), label3);
    g_free(label3);

    //Add labels to the histogram chart.
    gint i=0;
    for(i=0;i<(hc->rows);i++)
      {
        gchar *label=g_strdup_printf("Lbx%i", i);
        histogram_chart_append_label_xr(hc, label);
        histogram_chart_append_label_xl(hc, label);
        g_free(label);
      } 

    for(i=0;i<(hc->columns);i++)
      {
        gchar *label=g_strdup_printf("Lby%i", i);
        histogram_chart_append_label_yr(hc, label);
        histogram_chart_append_label_yl(hc, label);
        g_free(label);
      } 

    for(i=0;i<(hc->back_rows);i++)
      {
        gchar *label=g_strdup_printf("Lbz%i", i);
        histogram_chart_append_label_zr(hc, label);
        g_free(label);
      } 

    struct chart_data *cubes=initialize_cubes(hc, chart_inside_square, CUBE_CENTER);

    /*
       Initialize arrays for cylinders. This is hard coded for 8 or 12 sides. To change from 8 or 12 the
       qsort and the compare functions need to be changed! Use aligned memory for the compare
       function.
    */
    struct chart_data_aligned *cylinders=initialize_cylinders(hc, 8, 12.0);

    struct chart_data *test_points=initialize_test_points();

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);
    //The pointers for the nulls are added after the treeview and stores are created.
    gpointer parts[13]={da, hc, cylinders, cubes, window, NULL, NULL, NULL, NULL, NULL, NULL, NULL, test_points};
    draw_signal_id=g_signal_connect(da, "draw", G_CALLBACK(draw_main), parts); 

    GtkWidget *da_scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(da_scroll, TRUE);
    gtk_widget_set_vexpand(da_scroll, TRUE);
    gtk_widget_set_size_request(da, 1000, 1000);
    gtk_container_add(GTK_CONTAINER(da_scroll), da);

    GtkWidget *combo_animate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_animate, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_animate), 0, "1", "Draw Chart");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_animate), 1, "2", "Animate Chart");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_animate), 0);
    g_signal_connect(combo_animate, "changed", G_CALLBACK(set_animate_drawing), parts);

    GtkWidget *combo_drawing=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_drawing, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "1", "Histogram Chart3d");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "2", "Histogram Chart3d Grad Linear");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 2, "3", "Histogram Chart3d Grad Mesh");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 3, "4", "Histogram Chart3d Grad2 Linear");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 4, "5", "Histogram Chart3d Grad2 Mesh");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 5, "6", "Histogram Cylinders");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 6, "7", "Hiistogram Cylinders Grad Linear");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 7, "8", "Hiistogram Cylinders Grad Mesh");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 8, "9", "Test Points");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 9, "10", "Test Key Points");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
    g_signal_connect(combo_drawing, "changed", G_CALLBACK(set_drawing_id), da);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, FALSE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, FALSE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, FALSE);

    GtkWidget *scale_label=gtk_label_new("Scale");
    gtk_widget_set_hexpand(scale_label, FALSE);

    GtkWidget *perspective_label=gtk_label_new("Perspective");
    gtk_widget_set_hexpand(perspective_label, FALSE);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider, TRUE);
    gtk_widget_set_hexpand(pitch_slider, FALSE);
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_pitch), da);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider, TRUE);
    gtk_widget_set_hexpand(roll_slider, FALSE);
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_roll), da);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider, TRUE);
    gtk_widget_set_hexpand(yaw_slider, FALSE);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_yaw), da);  

    GtkWidget *scale_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 2.0, 0.01);
    gtk_widget_set_vexpand(scale_slider, TRUE);
    gtk_widget_set_hexpand(scale_slider, FALSE);
    gtk_range_set_value(GTK_RANGE(scale_slider), 1.0);
    g_signal_connect(scale_slider, "change-value", G_CALLBACK(set_scale), da);

    GtkWidget *perspective_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 0.5, 0.01);
    gtk_widget_set_vexpand(perspective_slider, TRUE);
    gtk_widget_set_hexpand(perspective_slider, FALSE);
    g_signal_connect(perspective_slider, "change-value", G_CALLBACK(set_perspective), da);

    GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
    gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
    gtk_widget_set_hexpand(check_frame, FALSE);
    g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), da);

    GtkWidget *check_time=gtk_check_button_new_with_label("Time Drawing");
    gtk_widget_set_halign(check_time, GTK_ALIGN_CENTER);
    gtk_widget_set_hexpand(check_time, FALSE);
    g_signal_connect(check_time, "toggled", G_CALLBACK(show_drawing_time), da);
   
    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 8);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_animate, 0, 0, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_drawing, 0, 1, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale_label, 3, 2, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid1), perspective_label, 4, 2, 1, 1);     
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale_slider, 3, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), perspective_slider, 4, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_frame, 0, 4, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_time, 0, 5, 4, 1);

    GtkWidget *scroll1=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll1, TRUE);
    gtk_widget_set_vexpand(scroll1, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll1), grid1);

    //Second page of notebook. 
    GtkAdjustment *row_adj=gtk_adjustment_new(7.0, 1.0, 40.0, 1.0, 0.0, 0.0);
    GtkAdjustment *column_adj=gtk_adjustment_new(9.0, 1.0, 40.0, 1.0, 0.0, 0.0);
    GtkAdjustment *outside_square_adj=gtk_adjustment_new(40.0, 10.0, 100.0, 1.0, 0.0, 0.0);
    GtkAdjustment *inside_square_adj=gtk_adjustment_new(20.0, 10.0, 100.0, 1.0, 0.0, 0.0);
    GtkAdjustment *index_adj=gtk_adjustment_new(0.0, 0.0, 400.0, 1.0, 0.0, 0.0);
    GtkAdjustment *zvalue_adj=gtk_adjustment_new(0.0, 0.0, 500.0, 1.0, 0.0, 0.0);
    GtkAdjustment *red_adj=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *green_adj=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *blue_adj=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *alpha_adj=gtk_adjustment_new(1.0, 0.0, 1.0, 0.01, 0.0, 0.0);

    GtkWidget *rows_label=gtk_label_new("Rows");
    GtkWidget *rows_spin=gtk_spin_button_new(row_adj, 1, 0);
    g_signal_connect(rows_spin, "value-changed", G_CALLBACK(rows_spin_changed), NULL);

    GtkWidget *columns_label=gtk_label_new("Columns");
    GtkWidget *columns_spin=gtk_spin_button_new(column_adj, 1, 0);
    g_signal_connect(columns_spin, "value-changed", G_CALLBACK(columns_spin_changed), NULL);

    GtkWidget *outside_square_label=gtk_label_new("Outside Square");
    GtkWidget *outside_square_spin=gtk_spin_button_new(outside_square_adj, 1, 0);
    g_signal_connect(outside_square_spin, "value-changed", G_CALLBACK(outside_square_spin_changed), NULL);

    GtkWidget *inside_square_label=gtk_label_new("Inside Square");
    GtkWidget *inside_square_spin=gtk_spin_button_new(inside_square_adj, 1, 0);
    g_signal_connect(inside_square_spin, "value-changed", G_CALLBACK(inside_square_spin_changed), NULL);

    GtkWidget *rc_button=gtk_button_new_with_label("New Chart");
    g_signal_connect(rc_button, "clicked", G_CALLBACK(set_chart), parts);

    GtkWidget *index_label=gtk_label_new("Index");
    GtkWidget *index_spin=gtk_spin_button_new(index_adj, 1, 0);
    g_signal_connect(index_spin, "value-changed", G_CALLBACK(index_spin_changed), NULL);

    GtkWidget *zvalue_label=gtk_label_new("Bar Height");
    GtkWidget *zvalue_spin=gtk_spin_button_new(zvalue_adj, 1, 0);
    g_signal_connect(zvalue_spin, "value-changed", G_CALLBACK(zvalue_spin_changed), NULL);

    GtkWidget *zvalue_button=gtk_button_new_with_label("Update Bar Height");
    g_signal_connect(zvalue_button, "clicked", G_CALLBACK(set_zvalue), parts);

    GtkWidget *rgb_label=gtk_label_new("RGBA");
    GtkWidget *red_spin=gtk_spin_button_new(red_adj, 0.01, 2);
    g_signal_connect(red_spin, "value-changed", G_CALLBACK(red_spin_changed), NULL);

    GtkWidget *green_spin=gtk_spin_button_new(green_adj, 0.01, 2);
    g_signal_connect(green_spin, "value-changed", G_CALLBACK(green_spin_changed), NULL);

    GtkWidget *blue_spin=gtk_spin_button_new(blue_adj, 0.01, 2);
    g_signal_connect(blue_spin, "value-changed", G_CALLBACK(blue_spin_changed), NULL);

    GtkWidget *alpha_spin=gtk_spin_button_new(alpha_adj, 0.01, 2);
    g_signal_connect(alpha_spin, "value-changed", G_CALLBACK(alpha_spin_changed), NULL);

    GtkWidget *bar_color_button=gtk_button_new_with_label("Update Bar Color Index");
    g_signal_connect(bar_color_button, "clicked", G_CALLBACK(set_bar_color), parts);

    GtkWidget *base_color_button=gtk_button_new_with_label("Update Base Color Index");
    g_signal_connect(base_color_button, "clicked", G_CALLBACK(set_base_color), parts);

    GtkWidget *base_color_all_button=gtk_button_new_with_label("Update Base Color All");
    g_signal_connect(base_color_all_button, "clicked", G_CALLBACK(set_base_color_all), parts);

    GtkWidget *back_color_button=gtk_button_new_with_label("Update Back Color");
    g_signal_connect(back_color_button, "clicked", G_CALLBACK(set_back_color), parts);

    GtkWidget *grid2=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid2), 8);
    gtk_grid_set_row_spacing(GTK_GRID(grid2), 8);
    gtk_grid_attach(GTK_GRID(grid2), rows_label, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), columns_label, 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), rows_spin, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), columns_spin, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), outside_square_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), inside_square_label, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), outside_square_spin, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), inside_square_spin, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), rc_button, 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), index_label, 0, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), zvalue_label, 1, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), index_spin, 0, 6, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), zvalue_spin, 1, 6, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), zvalue_button, 1, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), rgb_label, 0, 8, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid2), red_spin, 0, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), green_spin, 1, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), blue_spin, 0, 10, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), alpha_spin, 1, 10, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), base_color_all_button, 0, 11, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), base_color_button, 1, 11, 2, 1);
    gtk_grid_attach(GTK_GRID(grid2), back_color_button, 0, 12, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), bar_color_button, 1, 12, 2, 1);

    GtkWidget *scroll2=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll2, TRUE);
    gtk_widget_set_vexpand(scroll2, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll2), grid2);

    GtkAdjustment *back_height_adj=gtk_adjustment_new(200.0, 0.0, 400.0, 10.0, 0.0, 0.0);
    GtkAdjustment *back_rows_adj=gtk_adjustment_new(10.0, 0.0, 20.0, 1.0, 0.0, 0.0);
   
    GtkWidget *back_height_label=gtk_label_new("Back Height");
    gtk_widget_set_hexpand(back_height_label, FALSE);
    GtkWidget *back_height_spin=gtk_spin_button_new(back_height_adj, 100.0, 1);
    gtk_widget_set_hexpand(back_height_spin, FALSE);
    g_signal_connect(back_height_spin, "value-changed", G_CALLBACK(back_height_spin_changed), parts);

    GtkWidget *back_rows_label=gtk_label_new("Back Rows");
    gtk_widget_set_hexpand(back_rows_label, FALSE);
    GtkWidget *back_rows_spin=gtk_spin_button_new(back_rows_adj, 10.0, 0);
    gtk_widget_set_hexpand(back_rows_spin, FALSE);
    g_signal_connect(back_rows_spin, "value-changed", G_CALLBACK(back_rows_spin_changed), parts);

    GtkAdjustment *translate_x_adj=gtk_adjustment_new(0.0, -500.0, 500.0, 10.0, 0.0, 0.0);
    GtkAdjustment *translate_y_adj=gtk_adjustment_new(0.0, -500.0, 500.0, 10.0, 0.0, 0.0);
    GtkAdjustment *layout_width_adj=gtk_adjustment_new(500.0, 1.0, 2000.0, 50.0, 0.0, 0.0);
    GtkAdjustment *layout_height_adj=gtk_adjustment_new(500.0, 1.0, 2000.0, 50.0, 0.0, 0.0); 

    GtkWidget *translate_x_label=gtk_label_new("Translate x");
    gtk_widget_set_hexpand(translate_x_label, FALSE);
    GtkWidget *translate_x_spin=gtk_spin_button_new(translate_x_adj, 0.0, 1);
    gtk_widget_set_hexpand(translate_x_spin, FALSE);
    g_signal_connect(translate_x_spin, "value-changed", G_CALLBACK(translate_x_spin_changed), da); 

    GtkWidget *translate_y_label=gtk_label_new("Translate y");
    gtk_widget_set_hexpand(translate_y_label, FALSE);
    GtkWidget *translate_y_spin=gtk_spin_button_new(translate_y_adj, 0.0, 1);
    gtk_widget_set_hexpand(translate_y_spin, FALSE);
    g_signal_connect(translate_y_spin, "value-changed", G_CALLBACK(translate_y_spin_changed), da);

    GtkWidget *layout_width_label=gtk_label_new("Layout Width");
    gtk_widget_set_hexpand(layout_width_label, FALSE);
    GtkWidget *layout_width_spin=gtk_spin_button_new(layout_width_adj, 500.0, 1);
    gtk_widget_set_hexpand(layout_width_spin, FALSE);
    g_signal_connect(layout_width_spin, "value-changed", G_CALLBACK(layout_width_spin_changed), da);

    GtkWidget *layout_height_label=gtk_label_new("Layout Height");
    gtk_widget_set_hexpand(layout_height_label, FALSE);
    GtkWidget *layout_height_spin=gtk_spin_button_new(layout_height_adj, 500.0, 1);
    gtk_widget_set_hexpand(layout_height_spin, FALSE);
    g_signal_connect(layout_height_spin, "value-changed", G_CALLBACK(layout_height_spin_changed), da);

    GtkWidget *background_label=gtk_label_new("Background Color");
    GtkWidget *background_entry=gtk_entry_new();
    gtk_widget_set_hexpand(background_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(background_entry), "rgba(255, 255, 255, 1.0)");

    GtkWidget *background_button=gtk_button_new_with_label("Update Background Color");
    gtk_widget_set_hexpand(background_button, FALSE);
    gpointer w1[]={da, background_entry};
    g_signal_connect(background_button, "clicked", G_CALLBACK(set_background_color), w1);

    GtkAdjustment *red_grad_adj=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *green_grad_adj=gtk_adjustment_new(1.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *blue_grad_adj=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);

    GtkWidget *rgb_grad_label=gtk_label_new("RGB");
    GtkWidget *red_grad_spin=gtk_spin_button_new(red_grad_adj, 0.01, 2);
    g_signal_connect(red_grad_spin, "value-changed", G_CALLBACK(red_grad_spin_changed), NULL);

    GtkWidget *green_grad_spin=gtk_spin_button_new(green_grad_adj, 0.01, 2);
    g_signal_connect(green_grad_spin, "value-changed", G_CALLBACK(green_grad_spin_changed), NULL);

    GtkWidget *blue_grad_spin=gtk_spin_button_new(blue_grad_adj, 0.01, 2);
    g_signal_connect(blue_grad_spin, "value-changed", G_CALLBACK(blue_grad_spin_changed), NULL);

    GtkWidget *bottom_grad_button=gtk_button_new_with_label("Update Bottom Grad");
    g_signal_connect(bottom_grad_button, "clicked", G_CALLBACK(set_bottom_grad), da);

    GtkWidget *middle_grad_button=gtk_button_new_with_label("Update Middle Grad");
    g_signal_connect(middle_grad_button, "clicked", G_CALLBACK(set_middle_grad), da);

    GtkWidget *top_grad_button=gtk_button_new_with_label("Update Top Grad");
    g_signal_connect(top_grad_button, "clicked", G_CALLBACK(set_top_grad), da);

    GtkWidget *grid3=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid3), 8);
    gtk_grid_set_row_spacing(GTK_GRID(grid3), 8);
    gtk_grid_attach(GTK_GRID(grid3), back_height_label, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), back_rows_label, 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), back_height_spin, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), back_rows_spin, 1, 1, 1, 1);       
    gtk_grid_attach(GTK_GRID(grid3), translate_x_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), translate_y_label, 1, 2, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid3), translate_x_spin, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), translate_y_spin, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), layout_width_label, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), layout_height_label, 1, 4, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid3), layout_width_spin, 0, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), layout_height_spin, 1, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), background_label, 0, 6, 2, 1);
    gtk_grid_attach(GTK_GRID(grid3), background_entry, 0, 7, 2, 1);
    gtk_grid_attach(GTK_GRID(grid3), background_button, 0, 8, 2, 1);
    gtk_grid_attach(GTK_GRID(grid3), rgb_grad_label, 0, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), red_grad_spin, 0, 10, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), green_grad_spin, 1, 10, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), blue_grad_spin, 0, 11, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), bottom_grad_button, 0, 12, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), middle_grad_button, 1, 12, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), top_grad_button, 0, 13, 1, 1);

    GtkWidget *scroll3=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll3, TRUE);
    gtk_widget_set_vexpand(scroll3, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll3), grid3);

    GtkWidget *combo_stores=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_stores, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stores), 0, "1", "Labels Right Rows");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stores), 1, "2", "Labels Right Columns");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stores), 2, "3", "Labels Left Rows");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stores), 3, "4", "Labels Left Columns");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_stores), 4, "5", "Labels Z Rows");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_stores), 0);

    GtkWidget *empty_labels=gtk_button_new_with_label("Empty Tree Labels");
    gtk_widget_set_hexpand(empty_labels, FALSE);

    GtkWidget *tree=gtk_tree_view_new_with_model(GTK_TREE_MODEL(store_xr));
    gtk_widget_set_hexpand(tree, FALSE);
    gtk_tree_view_set_activate_on_single_click(GTK_TREE_VIEW(tree), TRUE);
     
    GtkCellRenderer *renderer1=gtk_cell_renderer_text_new();
    g_object_set(renderer1, "editable", FALSE, NULL);
    GtkCellRenderer *renderer2=gtk_cell_renderer_text_new();
    g_object_set(renderer2, "editable", TRUE, NULL);
   
    GtkTreeViewColumn *column1=gtk_tree_view_column_new_with_attributes("Index", renderer1, "text", 0, NULL);
    GtkTreeViewColumn *column2=gtk_tree_view_column_new_with_attributes("Label", renderer2, "text", 1, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW(tree), column1);
    gtk_tree_view_append_column(GTK_TREE_VIEW(tree), column2);  

    GtkWidget *tree_scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_vexpand(tree_scroll, TRUE);
    gtk_widget_set_hexpand(tree_scroll, FALSE);
    gtk_container_add(GTK_CONTAINER(tree_scroll), tree);

    GtkWidget *chart_labels=gtk_button_new_with_label("Update Chart Labels");
    gtk_widget_set_hexpand(chart_labels, FALSE);

    //Add the extra parts.
    parts[5]=tree; parts[6]=store_xr; parts[7]=store_yr; parts[8]=store_xl; parts[9]=store_yl; parts[10]= store_zr; parts[11]=combo_stores;
    g_signal_connect(combo_stores, "changed", G_CALLBACK(change_label_store), parts);
    g_signal_connect(empty_labels, "clicked", G_CALLBACK(update_labels_empty), parts);
    g_signal_connect(renderer2, "edited", G_CALLBACK(update_row_labels), parts);
    g_signal_connect(chart_labels, "clicked", G_CALLBACK(updata_chart_labels), parts); 

    GtkAdjustment *font_size_adj=gtk_adjustment_new(20.0, 6.0, 40.0, 1.0, 0.0, 0.0); 

    GtkWidget *font_size_label=gtk_label_new("Font Size");
    gtk_widget_set_hexpand(font_size_label, FALSE);
    GtkWidget *font_size_spin=gtk_spin_button_new(font_size_adj, 1.0, 0);
    gtk_widget_set_hexpand(font_size_spin, FALSE);
    g_signal_connect(font_size_spin, "value-changed", G_CALLBACK(font_size_changed), da);

    GtkWidget *font_position=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(font_position, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(font_position), 0, "1", "Label Start");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(font_position), 1, "2", "Label Middle");
    gtk_combo_box_set_active(GTK_COMBO_BOX(font_position), 1);
    g_signal_connect(font_position, "changed", G_CALLBACK(font_position_changed), da);

    GtkWidget *grid4=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid4), 8);
    gtk_grid_set_row_spacing(GTK_GRID(grid4), 8);
    gtk_grid_attach(GTK_GRID(grid4), combo_stores, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), empty_labels, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), tree_scroll, 0, 2, 2, 1);
    gtk_grid_attach(GTK_GRID(grid4), chart_labels, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), font_position, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), font_size_label, 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), font_size_spin, 1, 5, 1, 1);

    GtkWidget *scroll4=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll4, TRUE);
    gtk_widget_set_vexpand(scroll4, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll4), grid4);

    GtkAdjustment *red_adj2=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *green_adj2=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *blue_adj2=gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *alpha_adj2=gtk_adjustment_new(1.0, 0.0, 1.0, 0.01, 0.0, 0.0);
    GtkAdjustment *line_width_adj=gtk_adjustment_new(10.0, 0.0, 20.0, 1.0, 0.0, 0.0);

    GtkWidget *rgba_label2=gtk_label_new("RGBA");
    gtk_widget_set_hexpand(rgba_label2, FALSE);
    GtkWidget *red_spin2=gtk_spin_button_new(red_adj2, 0.01, 2);
    gtk_widget_set_hexpand(red_spin2, FALSE);
    g_signal_connect(red_spin2, "value-changed", G_CALLBACK(red_spin_changed2), NULL);

    GtkWidget *green_spin2=gtk_spin_button_new(green_adj2, 0.01, 2);
    gtk_widget_set_hexpand(green_spin2, FALSE);
    g_signal_connect(green_spin2, "value-changed", G_CALLBACK(green_spin_changed2), NULL);

    GtkWidget *blue_spin2=gtk_spin_button_new(blue_adj2, 0.01, 2);
    gtk_widget_set_hexpand(blue_spin2, FALSE);
    g_signal_connect(blue_spin2, "value-changed", G_CALLBACK(blue_spin_changed2), NULL);

    GtkWidget *alpha_spin2=gtk_spin_button_new(alpha_adj2, 0.01, 2);
    gtk_widget_set_hexpand(alpha_spin2, FALSE);
    g_signal_connect(alpha_spin2, "value-changed", G_CALLBACK(alpha_spin_changed2), NULL);

    GtkWidget *line_color_button=gtk_button_new_with_label("Set Line Color");
    gtk_widget_set_hexpand(line_color_button, FALSE);
    g_signal_connect(line_color_button, "clicked", G_CALLBACK(set_line_color), parts);

    GtkWidget *point_color_button=gtk_button_new_with_label("Set Point Color");
    gtk_widget_set_hexpand(point_color_button, FALSE);
    g_signal_connect(point_color_button, "clicked", G_CALLBACK(set_point_color), parts); 

    GtkWidget *line_width_label=gtk_label_new("Line Width");
    gtk_widget_set_hexpand(line_width_label, FALSE);
    GtkWidget *line_width_spin=gtk_spin_button_new(line_width_adj, 1.0, 1);
    gtk_widget_set_hexpand(line_width_spin, FALSE);
    g_signal_connect(line_width_spin, "value-changed", G_CALLBACK(line_width_spin_changed), parts);

    GtkWidget *combo_line=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_line, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_line), 0, "1", "Draw Points");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_line), 1, "2", "Draw Lines");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_line), 2, "3", "Draw Smooth");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_line), 0);
    g_signal_connect(combo_line, "changed", G_CALLBACK(set_line_id), parts);

    GtkAdjustment *point_x_adj=gtk_adjustment_new(0.0, -1000.0, 1000.0, 10.0, 0.0, 0.0);
    GtkAdjustment *point_y_adj=gtk_adjustment_new(0.0, -1000.0, 1000.0, 10.0, 0.0, 0.0);
    GtkAdjustment *point_z_adj=gtk_adjustment_new(0.0, -1000.0, 1000.0, 10.0, 0.0, 0.0);

    GtkWidget *point_label=gtk_label_new("Point XYZ");
    gtk_widget_set_hexpand(point_label, FALSE);
    GtkWidget *point_x_spin=gtk_spin_button_new(point_x_adj, 0.0, 3);
    gtk_widget_set_hexpand(point_x_spin, FALSE);
    g_signal_connect(point_x_spin, "value-changed", G_CALLBACK(point_x_spin_changed), NULL);

    GtkWidget *point_y_spin=gtk_spin_button_new(point_y_adj, 0.0, 3);
    gtk_widget_set_hexpand(point_y_spin, FALSE);
    g_signal_connect(point_y_spin, "value-changed", G_CALLBACK(point_y_spin_changed), NULL);

    GtkWidget *point_z_spin=gtk_spin_button_new(point_z_adj, 0.0, 3);
    gtk_widget_set_hexpand(point_z_spin, FALSE);
    g_signal_connect(point_z_spin, "value-changed", G_CALLBACK(point_z_spin_changed), NULL);

    GtkWidget *delete_button=gtk_button_new_with_label("Delete Test Points");
    gtk_widget_set_hexpand(delete_button, FALSE);
    g_signal_connect(delete_button, "clicked", G_CALLBACK(delete_points), parts);

    GtkWidget *point_button=gtk_button_new_with_label("Add Test Point");
    gtk_widget_set_hexpand(point_button, FALSE);
    g_signal_connect(point_button, "clicked", G_CALLBACK(add_point), parts); 

    GtkWidget *grid5=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid5), 8);
    //gtk_grid_set_column_homogeneous(GTK_GRID(grid5), TRUE);
    gtk_grid_set_row_spacing(GTK_GRID(grid5), 8);
    gtk_grid_attach(GTK_GRID(grid5), rgba_label2, 0, 0, 2, 1);    
    gtk_grid_attach(GTK_GRID(grid5), red_spin2, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid5), green_spin2, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid5), blue_spin2, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid5), alpha_spin2, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid5), line_color_button, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid5), point_color_button, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid5), line_width_label, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid5), line_width_spin, 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid5), combo_line, 0, 5, 1, 1);
    gtk_grid_attach(GTK_GRID(grid5), point_label, 0, 6, 2, 1);
    gtk_grid_attach(GTK_GRID(grid5), point_x_spin, 0, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid5), point_y_spin, 1, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid5), point_z_spin, 0, 8, 1, 1);
    gtk_grid_attach(GTK_GRID(grid5), point_button, 0, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid5), delete_button, 1, 9, 1, 1);

    GtkWidget *scroll5=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll5, TRUE);
    gtk_widget_set_vexpand(scroll5, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll5), grid5);

    GtkWidget *text_view=gtk_text_view_new();
    gtk_widget_set_name(text_view, "key_text_view");
    gtk_widget_set_hexpand(text_view, TRUE);
    gtk_widget_set_vexpand(text_view, TRUE);
    gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(text_view), GTK_WRAP_CHAR);

    GtkWidget *text_scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(text_scroll, TRUE);
    gtk_widget_set_vexpand(text_scroll, TRUE);
    gtk_container_add(GTK_CONTAINER(text_scroll), text_view);

    GtkTextBuffer *buffer=gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view));
    gtk_text_buffer_set_text(buffer, key_value_string, -1);

    GtkWidget *combo_line2=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_line2, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_line2), 0, "1", "Draw Points");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_line2), 1, "2", "Draw Lines");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_line2), 2, "3", "Draw Smooth");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_line2), 0);
    g_signal_connect(combo_line2, "changed", G_CALLBACK(set_line_id2), parts);

    GtkWidget *parse_button=gtk_button_new_with_label("Parse Key File");
    gtk_widget_set_hexpand(parse_button, FALSE);
    gpointer parse_data[2]={buffer, da};
    g_signal_connect(parse_button, "clicked", G_CALLBACK(parse_key_value_text), parse_data); 

    //Pointer array for charts from the key value text.
    chart_groups=g_ptr_array_new();

    GtkWidget *grid6=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid6), 8);
    gtk_grid_set_row_spacing(GTK_GRID(grid6), 8);
    gtk_grid_attach(GTK_GRID(grid6), text_scroll, 0, 0, 6, 1);
    gtk_grid_attach(GTK_GRID(grid6), combo_line2, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid6), parse_button, 0, 2, 1, 1);  

    GtkWidget *scroll6=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll6, TRUE);
    gtk_widget_set_vexpand(scroll6, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll6), grid6); 
   
    GtkWidget *nb_label1=gtk_label_new("Histogram3d");
    GtkWidget *nb_label2=gtk_label_new("Options1");
    GtkWidget *nb_label3=gtk_label_new("Options2");
    GtkWidget *nb_label4=gtk_label_new("Labels");
    GtkWidget *nb_label5=gtk_label_new("Test Points");
    GtkWidget *nb_label6=gtk_label_new("Test Key Points");
    GtkWidget *notebook=gtk_notebook_new();
    gtk_container_set_border_width(GTK_CONTAINER(notebook), 15);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll1, nb_label1);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll2, nb_label2);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll3, nb_label3);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll4, nb_label4);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll5, nb_label5);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll6, nb_label6);

    GtkWidget *scroll7=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll7, TRUE);
    gtk_widget_set_vexpand(scroll7, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll7), notebook);

    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), scroll7, FALSE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da_scroll, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 375);

    GtkWidget *menu1=gtk_menu_new();
    GtkWidget *menu1item1=gtk_menu_item_new_with_label("Save as .svg");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item1);
    GtkWidget *menu1item2=gtk_menu_item_new_with_label("Save as .pdf");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item2);
    GtkWidget *menu1item3=gtk_menu_item_new_with_label("Save as .ps");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item3);
    GtkWidget *menu1item4=gtk_menu_item_new_with_label("Save as .png");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item4);
    GtkWidget *title1=gtk_menu_item_new_with_label("Save Drawing");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(title1), menu1);

    GtkWidget *menu2=gtk_menu_new();
    GtkWidget *menu2item1=gtk_menu_item_new_with_label("Histogram Chart3d");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu2), menu2item1);
    GtkWidget *title2=gtk_menu_item_new_with_label("About");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(title2), menu2);

    GtkWidget *menu_bar=gtk_menu_bar_new();
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), title1);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), title2);

    g_signal_connect(menu1item1, "activate", G_CALLBACK(draw_svg), parts);
    g_signal_connect(menu1item2, "activate", G_CALLBACK(draw_pdf), parts);
    g_signal_connect(menu1item3, "activate", G_CALLBACK(draw_ps), parts);
    g_signal_connect(menu1item4, "activate", G_CALLBACK(draw_png), parts);
    g_signal_connect(menu2item1, "activate", G_CALLBACK(about_dialog), window);

    GtkWidget *grid7=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid7), menu_bar, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid7), paned1, 0, 1, 1, 1);

    //Draw background window based on the paned window splitter.
    g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), paned1);

    gtk_container_add(GTK_CONTAINER(window), grid7);

    gchar *css_string=g_strdup("#about_dialog{background: rgba(127,127,255,1.0); font-size: 16pt; font-weight: bold;} #about_dialog label selection{background: rgba(0,50,220,0.5);} #key_text_view{font-size: 14pt;}");
    GError *css_error=NULL;
    GtkCssProvider *provider=gtk_css_provider_new();
    gtk_css_provider_load_from_data(provider, css_string, -1, &css_error);
    gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    if(css_error!=NULL)
      {
        g_print("CSS loader error %s\n", css_error->message);
        g_error_free(css_error);
      }
    g_object_unref(provider);
    g_free(css_string);

    gtk_widget_show_all(window);

    gtk_main();

    //Clean up. Free the copied hc pointer in the array since it may have changed.
    histogram_chart_free((struct histogram_chart*)parts[1]);

    //Free copy of cylinder since it might be changed.
    cylinder_chart_free((struct chart_data_aligned*)parts[2]);

    cube_chart_free((struct chart_data*)parts[3]);

    free_chart_data_groups(chart_groups);

    g_array_free(test_points->d, TRUE);
    g_array_free(test_points->d_t, TRUE);
    g_free(test_points);

    g_object_unref(G_OBJECT(store_xr));
    g_object_unref(G_OBJECT(store_yr));
    g_object_unref(G_OBJECT(store_xl));
    g_object_unref(G_OBJECT(store_yl));
    g_object_unref(G_OBJECT(store_zr));

    return 0;  
  }
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data)
  {
    //Paint background of drawing area.
    cairo_set_source_rgba(cr, background_color[0], background_color[1], background_color[2], background_color[3]);
    cairo_paint(cr);
    //Paint the background under the grid.
    cairo_set_source_rgba(cr, 0.5, 0.5, 1.0, 1.0);
    gint width=gtk_paned_get_position(GTK_PANED(data));
    gint height=gtk_widget_get_allocated_height(widget);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_rectangle(cr, width-4, 0.0, 4, height);
    cairo_fill(cr);
    return FALSE;
  }
static GtkListStore* get_list_store(guint labels, const gchar *label)
  {
    gint i=0;
    GtkListStore *store=gtk_list_store_new(2, G_TYPE_INT, G_TYPE_STRING);
        
    GtkTreeIter iter1;
    for(i=0;i<labels;i++)
      {
        gchar *string1=NULL;
        if(label!=NULL) string1=g_strdup_printf("%s%i", label, i);
        else string1=g_strdup_printf("%i", i);
        gtk_list_store_append(store, &iter1);
        gtk_list_store_set(store, &iter1, 0, i, 1, string1, -1);
        g_free(string1);
      }
  
    return store;
  }
static void update_list_store(GtkListStore *store, guint labels, const gchar *label)
  {
    gint i=0;
        
    GtkTreeIter iter1;
    for(i=0;i<labels;i++)
      {
        gchar *string1=NULL;
        if(label!=NULL) string1=g_strdup_printf("%s%i", label, i);
        else string1=g_strdup_printf("%i", i);
        gtk_list_store_append(store, &iter1);
        gtk_list_store_set(store, &iter1, 0, i, 1, string1, -1);
        g_free(string1);
      }
  }
static void set_animate_drawing(GtkComboBox *combo, gpointer *parts)
  {   
    pitch=0.0;
    roll=0.0;
    yaw=0.0;
 
    if(gtk_combo_box_get_active(combo)==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
          }
      }
    
  }
static void rows_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_rows=gtk_spin_button_get_value_as_int(spin_button);
  }
static void columns_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_columns=gtk_spin_button_get_value_as_int(spin_button);
  }
static void outside_square_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_outside_square=gtk_spin_button_get_value_as_int(spin_button);
  }
static void inside_square_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_inside_square=gtk_spin_button_get_value_as_int(spin_button);
  }
static void set_chart(GtkWidget *button, gpointer *parts)
  {
    gint i=0;
    gboolean was_ticking=FALSE;

    //Block the draw and tick when updating the array.
    g_signal_handler_block(parts[0], draw_signal_id);
    if(tick_id!=0)
      {
        gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
        tick_id=0;
        was_ticking=TRUE;
      }

    //Free the previous chart and cylinders.
    if(((struct histogram_chart*)parts[1])!=NULL)
      {
        histogram_chart_free((struct histogram_chart*)parts[1]);
      }

    if(((struct chart_data_aligned*)parts[2])!=NULL)
      {
        cylinder_chart_free((struct chart_data_aligned*)parts[2]);
      }

    if(((struct chart_data*)parts[3])!=NULL)
      {
        cube_chart_free((struct chart_data*)parts[3]);
      }

    //Check inside square bounds.
    if(chart_inside_square>chart_outside_square)
      {
        chart_inside_square=chart_outside_square;
        g_print("The chart inside square is larger than the outside square.\n");
      }

    //Get a new chart and reinitialize cylinders.    
    parts[1]=initialize_histogram_chart(chart_rows, chart_columns, chart_outside_square);
    parts[2]=initialize_cylinders((struct histogram_chart*)parts[1], 8, chart_inside_square*1.0/2.0);
    parts[3]=initialize_cubes((struct histogram_chart*)parts[1], chart_inside_square, CUBE_CENTER);

    //Clear list stores.
    gtk_list_store_clear(GTK_LIST_STORE(parts[6]));
    gtk_list_store_clear(GTK_LIST_STORE(parts[7]));
    gtk_list_store_clear(GTK_LIST_STORE(parts[8]));
    gtk_list_store_clear(GTK_LIST_STORE(parts[9]));
    gtk_list_store_clear(GTK_LIST_STORE(parts[10]));

    //Add new labels to the liststores.
    struct histogram_chart *hc=(struct histogram_chart*)parts[1];
    gchar *label1=g_strdup("Lbx");
    update_list_store(GTK_LIST_STORE(parts[6]), (hc->rows), label1);
    update_list_store(GTK_LIST_STORE(parts[8]), (hc->rows), label1);
    g_free(label1);
    gchar *label2=g_strdup("Lby");
    update_list_store(GTK_LIST_STORE(parts[7]), (hc->columns), label2);   
    update_list_store(GTK_LIST_STORE(parts[9]), (hc->columns), label2); 
    g_free(label2);
    gchar *label3=g_strdup("Lbz");
    update_list_store(GTK_LIST_STORE(parts[10]), (hc->back_rows), label3);
    g_free(label3);
   
    //Add labels to the new histogram chart.
    for(i=0;i<(hc->rows);i++)
      {
        gchar *label=g_strdup_printf("Lbx%i", i);
        histogram_chart_append_label_xr(hc, label);
        histogram_chart_append_label_xl(hc, label);
        g_free(label);
      } 

    for(i=0;i<(hc->columns);i++)
      {
        gchar *label=g_strdup_printf("Lby%i", i);
        histogram_chart_append_label_yr(hc, label);
        histogram_chart_append_label_yl(hc, label);
        g_free(label);
      } 

    for(i=0;i<(hc->back_rows);i++)
      {
        gchar *label=g_strdup_printf("Lbz%i", i);
        histogram_chart_append_label_zr(hc, label);
        g_free(label);
      } 

    g_signal_handler_unblock(parts[0], draw_signal_id);
    if(was_ticking)
      {
        tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
      }

    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static void index_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_index=gtk_spin_button_get_value_as_int(spin_button);
  }
static void zvalue_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    zvalue=gtk_spin_button_get_value(spin_button);
  }
static void set_zvalue(GtkWidget *button, gpointer *parts)
  {
    struct histogram_chart *hc=parts[1];
    gint total=(hc->rows)*(hc->columns)-1;

    if(chart_index<0||chart_index>total)
      {
        g_warning("Index out of range. The bar height wasn't set.\n");
      }
    else
      {
        //Block the draw and tick when updating the array.
        gboolean was_ticking=FALSE;
        g_signal_handler_block(parts[0], draw_signal_id);
        if(tick_id!=0)
          {
            gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
            tick_id=0;
            was_ticking=TRUE;
          }

        cube_chart_set_bar_height((struct chart_data*)parts[3], chart_index, zvalue);
        cylinder_chart_set_bar_height((struct chart_data_aligned*)parts[2], chart_index, zvalue);

        g_signal_handler_unblock(parts[0], draw_signal_id);
        if(was_ticking)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
          }

        gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
      }
  }
static void red_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_rgba[0]=gtk_spin_button_get_value(spin_button);
  }
static void green_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_rgba[1]=gtk_spin_button_get_value(spin_button);
  }
static void blue_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_rgba[2]=gtk_spin_button_get_value(spin_button);
  }
static void alpha_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_rgba[3]=gtk_spin_button_get_value(spin_button);
  }
static void set_bar_color(GtkWidget *button, gpointer *parts)
  {
    struct histogram_chart *hc=parts[1];
    gint total=(hc->rows)*(hc->columns)-1;

    if(chart_index<0||chart_index>total)
      {
        g_warning("Index out of range. The bar height wasn't set.\n");
      }
    else
      {
        //Block the draw and tick when updating the array.
        gboolean was_ticking=FALSE;
        g_signal_handler_block(parts[0], draw_signal_id);
        if(tick_id!=0)
          {
            gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
            tick_id=0;
            was_ticking=TRUE;
          }

        cube_chart_set_bar_color((struct chart_data*)parts[3], chart_index, chart_rgba);
        cylinder_chart_set_bar_color((struct chart_data_aligned*)parts[2], chart_index, chart_rgba);

        g_signal_handler_unblock(parts[0], draw_signal_id);
        if(was_ticking)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
          }

        gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
      }
  }
static void set_base_color(GtkWidget *button, gpointer *parts)
  {
    struct histogram_chart *hc=parts[1];
    gint total=(hc->rows)*(hc->columns)-1;

    if(chart_index<0||chart_index>total)
      {
        g_warning("Index out of range. The base height wasn't set.\n");
      }
    else
      {
        //Block the draw and tick when updating the array.
        gboolean was_ticking=FALSE;
        g_signal_handler_block(parts[0], draw_signal_id);
        if(tick_id!=0)
          {
            gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
            tick_id=0;
            was_ticking=TRUE;
          }

        histogram_chart_set_base_color(hc, chart_index, chart_rgba);

        g_signal_handler_unblock(parts[0], draw_signal_id);
        if(was_ticking)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
          }

        gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
      }
  }
static void set_base_color_all(GtkWidget *button, gpointer *parts)
  {
    struct histogram_chart *hc=parts[1];
   
    //Block the draw and tick when updating the array.
    gboolean was_ticking=FALSE;
    g_signal_handler_block(parts[0], draw_signal_id);
    if(tick_id!=0)
      {
        gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
        tick_id=0;
        was_ticking=TRUE;
      }

    histogram_chart_set_base_color_all(hc, chart_rgba);

    g_signal_handler_unblock(parts[0], draw_signal_id);
    if(was_ticking)
      {
        tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
      }

    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static void set_back_color(GtkWidget *button, gpointer *parts)
  {
    struct histogram_chart *hc=parts[1];
    gboolean was_ticking=FALSE;

    g_signal_handler_block(parts[0], draw_signal_id);
    if(tick_id!=0)
      {
        gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
        tick_id=0;
        was_ticking=TRUE;
      }

    //Update back_height and reference points.
    hc->back_color[0]=chart_rgba[0];
    hc->back_color[1]=chart_rgba[1];
    hc->back_color[2]=chart_rgba[2];
    hc->back_color[3]=chart_rgba[3];   

    g_signal_handler_unblock(parts[0], draw_signal_id);
    if(was_ticking)
      {
        tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
      }

    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static void back_height_spin_changed(GtkSpinButton *spin_button, gpointer *parts)
  {
    struct histogram_chart *hc=parts[1];
    gboolean was_ticking=FALSE;

    g_signal_handler_block(parts[0], draw_signal_id);
    if(tick_id!=0)
      {
        gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
        tick_id=0;
        was_ticking=TRUE;
      }

    //Update back_height and reference points.
    hc->back_height=gtk_spin_button_get_value(spin_button);
    hc->c1[2]=(hc->back_height);
    hc->c1_t[2]=(hc->back_height);
    hc->c2[2]=(hc->back_height);
    hc->c2_t[2]=(hc->back_height);
    hc->c3[2]=(hc->back_height);
    hc->c3_t[2]=(hc->back_height);

    g_signal_handler_unblock(parts[0], draw_signal_id);
    if(was_ticking)
      {
        tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
      }

    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static void back_rows_spin_changed(GtkSpinButton *spin_button, gpointer *parts)
  {
    gint i=0;
    struct histogram_chart *hc=parts[1];
    gboolean was_ticking=FALSE;
    guint previous_rows=0;

    g_signal_handler_block(parts[0], draw_signal_id);
    if(tick_id!=0)
      {
        gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
        tick_id=0;
        was_ticking=TRUE;
      }

    previous_rows=(hc->back_rows);
    (hc->back_rows)=gtk_spin_button_get_value_as_int(spin_button);

    //Append or remove label.
    GtkTreeIter iter;
    if(previous_rows<(hc->back_rows))
      {
        gchar *label=g_strdup_printf("Lbz%i", (hc->back_rows)-1);
        gtk_list_store_append(GTK_LIST_STORE(parts[10]), &iter);
        gtk_list_store_set(GTK_LIST_STORE(parts[10]), &iter, 0, (hc->back_rows)-1, 1, label, -1);
        histogram_chart_append_label_zr(hc, label);
        g_free(label);
      }
    else
      {
        GtkTreeModel *model=GTK_TREE_MODEL(parts[10]);
        gtk_tree_model_get_iter_first(model, &iter);
        for(i=0;i<previous_rows-1;i++) gtk_tree_model_iter_next(model, &iter);
        gtk_list_store_remove(GTK_LIST_STORE(parts[10]), &iter);
        g_ptr_array_remove_index_fast((GPtrArray*)(hc->labels_zr), (hc->labels_zr->len)-1);
      }

    g_signal_handler_unblock(parts[0], draw_signal_id);
    if(was_ticking)
      {
        tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
      }

    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static void set_drawing_id(GtkComboBox *combo, gpointer da)
  {  
    drawing_id=gtk_combo_box_get_active(combo);

    gtk_widget_queue_draw(GTK_WIDGET(da));
  }
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    pitch=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    roll=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    yaw=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>2.0) scale=2.0;
    else if(value<0.0) scale=0.0;
    else scale=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value<0.0) perspective=0.0;
    else if(value>0.5) perspective=0.5;
    else perspective=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void show_frame_rate(GtkToggleButton *check_frame, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_frame)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_drawing_time(GtkToggleButton *check_time, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_time)) time_it=TRUE;
    else time_it=FALSE;
  }
static gboolean animate_drawing(GtkWidget *da, GdkFrameClock *frame_clock, gpointer *parts)
  {
    gint i=0;
    gdouble z=0.0;

    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    if(pitch>360.0) pitch=0.0;
    else pitch+=0.5;
    if(roll>360.0) roll=0.0;
    else roll+=0.5;
    if(yaw>360.0) yaw=0.0;
    else yaw+=0.5;

    //Change bar heights and reset max.
    if(drawing_id==0||drawing_id==1||drawing_id==2||drawing_id==3||drawing_id==4)
      {
        struct chart_data *cd=parts[3];
        for(i=0;i<(cd->d->len);i++)
          {
            z=cube_chart_get_bar_height(cd, i)+2.0;
            if(z>200.0) cube_chart_set_bar_height(cd, i, 0.0);
            else cube_chart_set_bar_height(cd, i, z);
            if((cd->max_z)<z) (cd->max_z)=z;
          }
         (cd->max_z)=cube_chart_get_max_z(cd);
       }
    else if(drawing_id==5||drawing_id==6||drawing_id==7)
      {
        struct chart_data_aligned *cd=parts[2];
        gint cylinders=(cd->len)/((cd->ring_size)*2);
        for(i=0;i<cylinders;i++)
          {
            z=cylinder_chart_get_bar_height(cd, i)+2.0;
            if(z>200.0) cylinder_chart_set_bar_height(cd, i, 0.0);
            else cylinder_chart_set_bar_height(cd, i, z);
          }
        (cd->max_z)=cylinder_chart_get_max_z(cd);
      }
    
    gtk_widget_queue_draw(GTK_WIDGET(da));
    return G_SOURCE_CONTINUE;
  }
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer *parts)
  {  
    gdouble w1=layout_width/10.0;
    gdouble h1=layout_height/10.0;
    struct histogram_chart *hc=parts[1];
    struct chart_data_aligned *cylinders=parts[2];
    struct chart_data *cubes=parts[3];
    struct chart_data *test_points=parts[12];

    //Background.
    cairo_set_source_rgba(cr, background_color[0], background_color[1], background_color[2], background_color[3]);
    cairo_paint(cr); 

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 2.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Layout bounding box.
    cairo_rectangle(cr, 0.0, 0.0, layout_width, layout_height);
    cairo_stroke(cr);
  
    GTimer *timer=NULL; 
    if(time_it==TRUE) timer=g_timer_new();
 
    cairo_translate(cr, layout_width/2.0, layout_height/2.0);  
    cairo_translate(cr, translate_x, translate_y);
    cairo_scale(cr, scale+0.01, scale+0.01);
    //Keep the lines smooth.
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);        
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_set_line_width(cr, 1.0);

    //Rotation.
    gdouble qrs[16];
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    histo_quaternion_rotation(yaw1, roll1, pitch1, qrs);

    //Perspective.
    gdouble pm[16];
    gdouble out[16];
    gdouble near=1.0;
    gdouble far=1.0+perspective*0.01;
    set_projection_matrix(pm, 90.0, near, far); 
    histo_matrix_multiply(qrs, pm, out);  
   
    //Draw the chart.
    if(drawing_id==0) cube_chart_draw(hc, cubes, cr, out, SOLID_CUBES, chart_font_size, chart_font_position);
    else if(drawing_id==1) cube_chart_draw(hc, cubes, cr, out, GRADIENT_CUBES, chart_font_size, chart_font_position);
     else if(drawing_id==2) cube_chart_draw(hc, cubes, cr, out, GRADIENT_CUBES_MESH, chart_font_size, chart_font_position);
    else if(drawing_id==3) cube_chart_draw(hc, cubes, cr, out, GRADIENT_CUBES2, chart_font_size, chart_font_position);
    else if(drawing_id==4) cube_chart_draw(hc, cubes, cr, out, GRADIENT_CUBES2_MESH, chart_font_size, chart_font_position);
    else if(drawing_id==5) chart_data_draw_aligned(hc, cylinders, cr, out, SOLID_CYLINDERS, chart_font_size, chart_font_position);
    else if(drawing_id==6) chart_data_draw_aligned(hc, cylinders, cr, out, GRADIENT_CYLINDERS, chart_font_size, chart_font_position);
    else if(drawing_id==7) chart_data_draw_aligned(hc, cylinders, cr, out, GRADIENT_CYLINDERS_MESH, chart_font_size, chart_font_position);
    else if(drawing_id==8)
      {
        if(line_id==0)
          {
            chart_data_draw(hc, test_points, cr, out, DRAW_POINTS, line_color, point_color, line_width, chart_font_size, chart_font_position);
          }
        else if(line_id==1)
          {
            chart_data_draw(hc, test_points, cr, out, DRAW_LINES, line_color, point_color, line_width, chart_font_size, chart_font_position);
          }
        else
          {
            //Need at least two points for the smoothing function.
            if((test_points->d->len)<3)
              {
                chart_data_draw(hc, test_points, cr, out, DRAW_LINES, line_color, point_color, line_width, chart_font_size, chart_font_position);
              }
            else
              {
                chart_data_draw(hc, test_points, cr, out, DRAW_SMOOTH, line_color, point_color, line_width, chart_font_size, chart_font_position);
              }
          }
      }
    else
      {
        if(line_id2==0)
          {
            chart_data_draw_groups(hc, chart_groups, cr, out, DRAW_POINTS, chart_font_size, chart_font_position);
          }
        else if(line_id2==1)
          {
            chart_data_draw_groups(hc, chart_groups, cr, out, DRAW_LINES, chart_font_size, chart_font_position);
          }
        else
          {
            chart_data_draw_groups(hc, chart_groups, cr, out, DRAW_SMOOTH, chart_font_size, chart_font_position);
          }
          
      }
     
    if(time_it==TRUE) 
     {
       g_print("Draw Time %f\n", g_timer_elapsed(timer, NULL));
       g_timer_destroy(timer);
     }

    return FALSE;
  }
static gchar* save_file(gpointer *parts, gint surface_type)
  {
    int result=0;

    GtkWidget *dialog=gtk_file_chooser_dialog_new("Save File", GTK_WINDOW(parts[4]), GTK_FILE_CHOOSER_ACTION_SAVE, "Cancel", GTK_RESPONSE_CANCEL, "Save", GTK_RESPONSE_ACCEPT, NULL);

    if(surface_type==0) gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".svg");
    else if(surface_type==1) gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".pdf");
    else if(surface_type==2) gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".ps");
    else gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".png"); 

    result=gtk_dialog_run(GTK_DIALOG(dialog));
    gchar *file_name=NULL;
    if(result==GTK_RESPONSE_ACCEPT)
      {

        file_name=gtk_file_chooser_get_current_name(GTK_FILE_CHOOSER(dialog));
      }

    gtk_widget_destroy(dialog);

    return file_name;
  }
static void draw_svg(GtkWidget *widget, gpointer *parts)
  {
    gchar *file_name=save_file(parts, 0);
    
    //Scale factor to convert to points.
    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_svg_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, parts, 0);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name);
  }
static void draw_pdf(GtkWidget *widget, gpointer *parts)
  {
    gchar *file_name=save_file(parts, 1);

    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_pdf_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, parts, 1);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }
    
    g_free(file_name);
  }
static void draw_ps(GtkWidget *widget, gpointer *parts)
  {
    gchar *file_name=save_file(parts, 2);

    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_ps_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, parts, 2);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name); 
  }
static void draw_png(GtkWidget *widget, gpointer *parts)
  {
    gchar *file_name=save_file(parts, 3);

    if(file_name!=NULL)
      {
        cairo_surface_t *surface=cairo_image_surface_create(CAIRO_FORMAT_ARGB32, layout_width, layout_height);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, parts, 3);
        cairo_surface_write_to_png(surface, file_name);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name);    
  }
static void draw_surface(cairo_t *cr, gpointer *parts, guint surface_type)
  {
    struct histogram_chart *hc=parts[1];
    struct chart_data_aligned *cylinders=parts[2];
    struct chart_data *cubes=parts[3];
    struct chart_data *test_points=parts[12];
    gdouble points_scale=0.0;

    if(surface_type<3) points_scale=72.0/ppi;
    else points_scale=1.0;

    //Background.
    cairo_set_source_rgba(cr, background_color[0], background_color[1], background_color[2], background_color[3]);
    cairo_paint(cr); 

    cairo_translate(cr, layout_width*points_scale/2.0, layout_height*points_scale/2.0);
    cairo_translate(cr, translate_x, translate_y);
    cairo_scale(cr, scale*points_scale+0.01, scale*points_scale+0.01);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);        
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_set_line_width(cr, 1.0);

    //Rotation.
    gdouble qrs[16];
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    histo_quaternion_rotation(yaw1, roll1, pitch1, qrs);

    //Perspective.
    gdouble pm[16];
    gdouble out[16];
    gdouble near=1.0;
    gdouble far=1.0+perspective*0.01;
    set_projection_matrix(pm, 90.0, near, far);  
    histo_matrix_multiply(qrs, pm, out);

    //Draw the chart.
    if(drawing_id==0) cube_chart_draw(hc, cubes, cr, out, SOLID_CUBES, chart_font_size, chart_font_position);
    else if(drawing_id==1) cube_chart_draw(hc, cubes, cr, out, GRADIENT_CUBES, chart_font_size, chart_font_position);
    else if(drawing_id==2) cube_chart_draw(hc, cubes, cr, out, GRADIENT_CUBES_MESH, chart_font_size, chart_font_position);
    else if(drawing_id==3) cube_chart_draw(hc, cubes, cr, out, GRADIENT_CUBES2, chart_font_size, chart_font_position);
    else if(drawing_id==4) cube_chart_draw(hc, cubes, cr, out, GRADIENT_CUBES2_MESH, chart_font_size, chart_font_position);
    else if(drawing_id==5) chart_data_draw_aligned(hc, cylinders, cr, out, SOLID_CYLINDERS, chart_font_size, chart_font_position);
    else if(drawing_id==6) chart_data_draw_aligned(hc, cylinders, cr, out, GRADIENT_CYLINDERS, chart_font_size, chart_font_position);
    else if(drawing_id==7) chart_data_draw_aligned(hc, cylinders, cr, out, GRADIENT_CYLINDERS, chart_font_size, chart_font_position);
    else if(drawing_id==8)
      {
        if(line_id==0)
          {
            chart_data_draw(hc, test_points, cr, out, DRAW_POINTS, line_color, point_color, line_width, chart_font_size, chart_font_position);
          }
        else if(line_id==1)
          {
            chart_data_draw(hc, test_points, cr, out, DRAW_LINES, line_color, point_color, line_width, chart_font_size, chart_font_position);
          }
        else
          {
            //Need at least two points for the smoothing function.
            if((test_points->d->len)<3)
              {
                chart_data_draw(hc, test_points, cr, out, DRAW_LINES, line_color, point_color, line_width, chart_font_size, chart_font_position);
              }
            else
              {
                chart_data_draw(hc, test_points, cr, out, DRAW_SMOOTH, line_color, point_color, line_width, chart_font_size, chart_font_position);
              }
          }
      }
    else
      {
        if(line_id2==0)
          {
            chart_data_draw_groups(hc, chart_groups, cr, out, DRAW_POINTS, chart_font_size, chart_font_position);
          }
        else if(line_id2==1)
          {
            chart_data_draw_groups(hc, chart_groups, cr, out, DRAW_LINES, chart_font_size, chart_font_position);
          }
        else
          {
            chart_data_draw_groups(hc, chart_groups, cr, out, DRAW_SMOOTH, chart_font_size, chart_font_position);
          }
          
      }
  }
static void about_dialog(GtkWidget *widget, gpointer data)
  {
    GtkWidget *dialog=gtk_about_dialog_new();
    gtk_widget_set_name(dialog, "about_dialog");
    gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(data));
    //gtk_about_dialog_set_logo(GTK_ABOUT_DIALOG(dialog), NULL);
    gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(dialog), "Histogram Chart3d");
    gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog), "Test Version 1.0");
    gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(dialog), "3d histogram charts.");
    gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(dialog), "(C) 2019 C. Eric Cashon");

    gtk_widget_show_all(dialog);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
  }
static void translate_x_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    translate_x=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void translate_y_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    translate_y=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void layout_width_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    layout_width=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void layout_height_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    layout_height=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_background_color(GtkWidget *button, gpointer *data)
  {
    GdkRGBA rgba;
    
    if(gdk_rgba_parse(&rgba, gtk_entry_get_text(GTK_ENTRY(data[1]))))
      {
        background_color[0]=rgba.red;
        background_color[1]=rgba.green;
        background_color[2]=rgba.blue;
        background_color[3]=rgba.alpha;
      }
    else
      {
        g_print("Background color string format error.\n");
      } 

    gtk_widget_queue_draw(GTK_WIDGET(data[0]));
  }
static void red_grad_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    grad_color[0]=gtk_spin_button_get_value(spin_button);
  }
static void green_grad_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    grad_color[1]=gtk_spin_button_get_value(spin_button);
  }
static void blue_grad_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    grad_color[2]=gtk_spin_button_get_value(spin_button);
  }
static void set_bottom_grad(GtkWidget *button, gpointer data)
  {
    set_gradient_color_bottom(grad_color[0], grad_color[1], grad_color[2]);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_middle_grad(GtkWidget *button, gpointer data)
  {
    set_gradient_color_middle(grad_color[0], grad_color[1], grad_color[2]);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_top_grad(GtkWidget *button, gpointer data)
  {
    set_gradient_color_top(grad_color[0], grad_color[1], grad_color[2]);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void change_label_store(GtkComboBox *combo, gpointer *parts)
  {
    gint i=gtk_combo_box_get_active(combo);
    if(i==0) gtk_tree_view_set_model(GTK_TREE_VIEW(parts[5]), GTK_TREE_MODEL(parts[6]));
    else if(i==1) gtk_tree_view_set_model(GTK_TREE_VIEW(parts[5]), GTK_TREE_MODEL(parts[7]));
    else if(i==2) gtk_tree_view_set_model(GTK_TREE_VIEW(parts[5]), GTK_TREE_MODEL(parts[8]));
    else if(i==3) gtk_tree_view_set_model(GTK_TREE_VIEW(parts[5]), GTK_TREE_MODEL(parts[9]));
    else gtk_tree_view_set_model(GTK_TREE_VIEW(parts[5]), GTK_TREE_MODEL(parts[10]));
 
    gtk_widget_queue_draw(GTK_WIDGET(parts[5]));
  }
static void update_row_labels(GtkCellRendererText *renderer, gchar *path, gchar *new_text, gpointer *parts)
  {
    gint i=gtk_combo_box_get_active(GTK_COMBO_BOX(parts[11]));
    GtkListStore *store=parts[i+6];
    GtkTreeIter iter;
    GValue value=G_VALUE_INIT;
    g_value_init(&value, G_TYPE_STRING);
    g_value_set_static_string(&value, new_text);
    GtkTreePath *tree_path=gtk_tree_path_new_from_string(path);
    gtk_tree_model_get_iter(GTK_TREE_MODEL(store), &iter, tree_path);
    gtk_list_store_set_value(GTK_LIST_STORE(store), &iter, 1, &value);
    gtk_tree_path_free(tree_path);
    g_value_unset(&value);
  }
static void update_labels_empty(GtkWidget *button, gpointer *parts)
  {
    gint i=gtk_combo_box_get_active(GTK_COMBO_BOX(parts[11]));
    gboolean check=FALSE;
    gchar string[]={'\0'};
    GtkListStore *store=parts[i+6];
    GtkTreeIter iter;
    GValue value=G_VALUE_INIT;
    g_value_init(&value, G_TYPE_STRING);
    g_value_set_static_string(&value, string);

    check=gtk_tree_model_get_iter_first(GTK_TREE_MODEL(store), &iter);    
    while(check) 
      {
        gtk_list_store_set_value(GTK_LIST_STORE(store), &iter, 1, &value);
        check=gtk_tree_model_iter_next(GTK_TREE_MODEL(store), &iter);
      }

    g_value_unset(&value);
  }
static void updata_chart_labels(GtkWidget *button, gpointer *parts)
  {
    gint i=0;
    GtkTreeIter iter;
    gint index=0;
    struct histogram_chart *hc=parts[1];
    GtkTreeModel *model=GTK_TREE_MODEL(parts[6]);

    histogram_chart_clear_labels(hc);
    gtk_tree_model_get_iter_first(model, &iter);
    for(i=0;i<(hc->rows);i++)
      {
        gchar *label=NULL;
        gtk_tree_model_get(model, &iter, 0, &index, 1, &label, -1);
        histogram_chart_append_label_xr(hc, label);
        gtk_tree_model_iter_next(model, &iter);
        g_free(label);
      }

    model=GTK_TREE_MODEL(parts[7]);
    gtk_tree_model_get_iter_first(model, &iter);
    for(i=0;i<(hc->columns);i++)
      {
        gchar *label=NULL;
        gtk_tree_model_get(model, &iter, 0, &index, 1, &label, -1);
        histogram_chart_append_label_yr(hc, label);
        gtk_tree_model_iter_next(model, &iter);
        g_free(label);
      }

    model=GTK_TREE_MODEL(parts[8]);
    gtk_tree_model_get_iter_first(model, &iter);
    for(i=0;i<(hc->rows);i++)
      {
        gchar *label=NULL;
        gtk_tree_model_get(model, &iter, 0, &index, 1, &label, -1);
        histogram_chart_append_label_xl(hc, label);
        gtk_tree_model_iter_next(model, &iter);
        g_free(label);
      }

    model=GTK_TREE_MODEL(parts[9]);
    gtk_tree_model_get_iter_first(model, &iter);
    for(i=0;i<(hc->columns);i++)
      {
        gchar *label=NULL;
        gtk_tree_model_get(model, &iter, 0, &index, 1, &label, -1);
        histogram_chart_append_label_yl(hc, label);
        gtk_tree_model_iter_next(model, &iter);
        g_free(label);
      }

    model=GTK_TREE_MODEL(parts[10]);
    gtk_tree_model_get_iter_first(model, &iter);
    for(i=0;i<(hc->back_rows);i++)
      {
        gchar *label=NULL;
        gtk_tree_model_get(model, &iter, 0, &index, 1, &label, -1);
        histogram_chart_append_label_zr(hc, label);
        gtk_tree_model_iter_next(model, &iter);
        g_free(label);
      }

    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static void font_size_changed(GtkSpinButton *spin_button, gpointer data)
  {
    chart_font_size=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void font_position_changed(GtkComboBox *combo, gpointer data)
  {
    chart_font_position=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void red_spin_changed2(GtkSpinButton *spin_button, gpointer data)
  {
    current_rgba[0]=gtk_spin_button_get_value(spin_button);
  }
static void green_spin_changed2(GtkSpinButton *spin_button, gpointer data)
  {
    current_rgba[1]=gtk_spin_button_get_value(spin_button);
  }
static void blue_spin_changed2(GtkSpinButton *spin_button, gpointer data)
  {
    current_rgba[2]=gtk_spin_button_get_value(spin_button);
  }
static void alpha_spin_changed2(GtkSpinButton *spin_button, gpointer data)
  {
    current_rgba[3]=gtk_spin_button_get_value(spin_button);
  }
static void set_line_color(GtkWidget *button, gpointer *parts)
  {
    line_color[0]=current_rgba[0];
    line_color[1]=current_rgba[1];
    line_color[2]=current_rgba[2];
    line_color[3]=current_rgba[3];
    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static void set_point_color(GtkWidget *button, gpointer *parts)
  {
    point_color[0]=current_rgba[0];
    point_color[1]=current_rgba[1];
    point_color[2]=current_rgba[2];
    point_color[3]=current_rgba[3];
    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static void line_width_spin_changed(GtkSpinButton *spin_button, gpointer *parts)
  {
    line_width=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static void set_line_id(GtkComboBox *combo, gpointer *parts)
  {
    line_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static void point_x_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    point_xyz[0]=gtk_spin_button_get_value(spin_button);
  }
static void point_y_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    //Reverse y. Cairo has positive y going down but better to have standard cartesian coordinates.
    point_xyz[1]=-gtk_spin_button_get_value(spin_button);
  }
static void point_z_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    point_xyz[2]=gtk_spin_button_get_value(spin_button);
  }
static void set_line_id2(GtkComboBox *combo, gpointer *parts)
  {
    line_id2=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static void delete_points(GtkWidget *button, gpointer *parts)
  {
    gint i=0;
    struct chart_data *test_points=parts[12];
    gint len=test_points->d->len;

    //Make sure to delete from both arrays.
    for(i=len;i>0;i--) g_array_remove_index_fast(test_points->d, i-1);
    for(i=len;i>0;i--) g_array_remove_index_fast(test_points->d_t, i-1);

    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static void add_point(GtkWidget *button, gpointer *parts)
  {
    struct chart_data *test_points=parts[12];

    struct chart_point3d point;
    point.x=point_xyz[0];
    point.y=point_xyz[1];
    point.z=point_xyz[2];
    point.move_id=C_CURVE_TO_STROKE;

    //Keep the points grouped.
    if(test_points->d->len>0)
      {
        struct chart_point3d *p1=&g_array_index(test_points->d, struct chart_point3d, (test_points->d->len)-1);
        p1->move_id=C_MOVE_TO_R;
      }

    g_array_append_val(test_points->d, point);
    g_array_append_val(test_points->d_t, point);

    gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
  }
static struct histogram_chart* initialize_histogram_chart(guint rows, guint columns, gdouble outside_square)
  {
    struct histogram_chart *hc=histogram_chart_new(rows, columns, outside_square);

    //Reduce the back height. Default back height is the width of rows.
    histogram_chart_set_back_height(hc, hc->back_height/2.0);

    //Set histogram base square colors.
    gint i=0;
    gdouble total=rows*columns;
    gdouble rgba[4]={0.0, 0.0, 0.0, 1.0};
    for(i=0;i<total;i++)
      {
        if(i%2==0)
          {
            rgba[1]=0.0;rgba[2]=1.0;
          }
        else
          {
            rgba[1]=1.0;rgba[2]=1.0;
          }
        histogram_chart_set_base_color(hc, i, rgba);
      }
    
    return hc;
  }
static struct chart_data* initialize_cubes(struct histogram_chart *hc, gdouble inside_square, guint inside_square_position)
  {
    gint i=0;
    gint total=(hc->rows)*(hc->columns);
    gdouble inv=1.0/(gdouble)total;
    gdouble rgba[4]={1.0, 0.0, 1.0, 1.0};
    gdouble rand=0.0;

    struct chart_data *cd=chart_cubes_new(hc, inside_square, inside_square_position);

    //Set histogram bar colors.
    for(i=0;i<total;i++)
      {
        cube_chart_set_bar_color(cd, i, rgba);
        rgba[1]+=inv;
        rgba[2]-=inv;
      }

    //Set some random histogram bar heights.
    for(i=0;i<total;i++)
      {
        rand=100.0*g_random_double();
        cube_chart_set_bar_height(cd, i, rand);
      }

    //Set the max z height.
    cd->max_z=cube_chart_get_max_z(cd);

    return cd;
  }
static struct chart_data_aligned* initialize_cylinders(struct histogram_chart *hc, gint sections, gdouble radius)
  {
    gint i=0;
    gint total=(hc->rows)*(hc->columns);
    gdouble inv=1.0/(gdouble)total;
    gdouble rgba[4]={1.0, 0.0, 1.0, 1.0};
    gdouble rand=0.0;

    struct chart_data_aligned *cd=chart_cylinders_new(hc, sections, radius);

    //Set cylinder colors.
    struct histo_arc *p1=(cd->d);
    for(i=0;i<total;i++)
      {
        cylinder_chart_set_bar_color(cd, i, rgba);       
        rgba[1]+=inv;
        rgba[2]-=inv;
        p1++;
      }

    //Set some random histogram bar heights.
    for(i=0;i<total;i++)
      {
        rand=100.0*g_random_double();
        cylinder_chart_set_bar_height(cd, i, rand);
      }

    //Set the max z height.
    cd->max_z=cylinder_chart_get_max_z(cd);

    return cd;
  }
static struct chart_data* initialize_test_points()
  {
    struct chart_data *test_points=g_new(struct chart_data, 1);

    //Initialize empty arrays.
    test_points->data_id=0;
    test_points->max_z=0.0;
    test_points->d=g_array_new(FALSE, TRUE, sizeof(struct chart_point3d));
    test_points->d_t=g_array_new(FALSE, TRUE, sizeof(struct chart_point3d));

    return test_points;
  }
static void parse_key_value_text(GtkWidget *button, gpointer *parse_data)
  {
    GtkTextIter start;
    GtkTextIter end;

    clear_chart_data_groups(chart_groups);

    gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(parse_data[0]), &start, &end);
    gchar *string=gtk_text_buffer_get_text(GTK_TEXT_BUFFER(parse_data[0]), &start, &end, FALSE);
    set_chart_data_groups(chart_groups, string);
    g_free(string);
    gtk_widget_queue_draw(GTK_WIDGET(parse_data[1]));
  }







