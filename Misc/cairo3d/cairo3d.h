
/*
    Compile with cairo3d_UI.c.

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include<glib.h>
#include<cairo.h>
#include<pixman.h>
#include<math.h>
#include<stdlib.h>
#include<stdio.h>

//A 3d point for rotations.
struct point3d{
  gdouble x;
  gdouble y;
  gdouble z;
};

//Used to add to the points on the morph sphere.
struct morph_sphere_point{
  gint index;
  gint x;
  gint y;
  gint z;
  gint ray;
};

//For generating bezier arcs.
struct arc{
  struct point3d a1;
  struct point3d a2;
  struct point3d a3;
  struct point3d a4;
};

//Bezier points for smoothing.
struct controls2d{
  gdouble x1;
  gdouble y1;
  gdouble x2;
  gdouble y2;
};

//Bezier points for smoothing.
struct controls3d{
  gdouble x1;
  gdouble y1;
  gdouble z1;
  gdouble x2;
  gdouble y2;
  gdouble z2;
};

//For sorting drawing planes to draw in order from -z to +z. Used in the pyramid.
struct plane_order{
  gint id;
  gdouble value;
};

//For sorting quad planes and a single color.
struct quad_plane{
  gint index;
  gdouble x1;
  gdouble y1;
  gdouble z1;
  gdouble x2;
  gdouble y2;
  gdouble z2;
  gdouble x3;
  gdouble y3;
  gdouble z3;
  gdouble x4;
  gdouble y4;
  gdouble z4;
  gdouble r;
  gdouble g;
  gdouble b;
  gdouble a;
};

//This was going to be the quad_plane replacement but still might want a single color in a quad.
struct quad_plane2{
  gdouble x1;
  gdouble y1;
  gdouble z1;
  gdouble x2;
  gdouble y2;
  gdouble z2;
  gdouble x3;
  gdouble y3;
  gdouble z3;
  gdouble x4;
  gdouble y4;
  gdouble z4;
  gint index;
};

/*
  For sorting a quad plane with 4 bezier sides. Colors can set with the index value.
*/
struct bezier_quad{
  gdouble x1;
  gdouble y1;
  gdouble z1;
  gdouble bx1;
  gdouble by1;
  gdouble bz1;
  gdouble bx2;
  gdouble by2;
  gdouble bz2;
  gdouble x2;
  gdouble y2;
  gdouble z2;
  gdouble bx3;
  gdouble by3;
  gdouble bz3;
  gdouble bx4;
  gdouble by4;
  gdouble bz4;
  gdouble x3;
  gdouble y3;
  gdouble z3;
  gdouble bx5;
  gdouble by5;
  gdouble bz5;
  gdouble bx6;
  gdouble by6;
  gdouble bz6;
  gdouble x4;
  gdouble y4;
  gdouble z4;
  gdouble bx7;
  gdouble by7;
  gdouble bz7;
  gdouble bx8;
  gdouble by8;
  gdouble bz8;
  gint index;
};

/*
    The data arrays for the initial shape(*s) and shape rotated(*sr). The rotated shape may also
    be sorted before drawn. The rings and points_per_ring can also be used as rows and columns
    such as in a bezier sheet. There is a z_max and z_min for standardizing gradient draws
    on the z-axis for a mesh. These may often be set to 0.0 if they aren't needed.
*/
struct shape{
  gint cache_id;
  GArray *s;
  GArray *sr;
  gint rings;
  gint points_per_ring;
  gboolean default_drawing;
  gdouble z_max;
  gdouble z_min;
};

//These are the shape cache_id values or basic shapes.
enum{ 
  SPRING, 
  SOLID_CYLINDER, 
  CUBE,
  CHECKERBOARD,
  WIRE_BEZIER_SHEET,
  SOLID_BEZIER_SHEET,
  BALL,
  WIRE_SPHERE,
  SOLID_SPHERE,
  WIRE_FUNNEL,
  SOLID_FUNNEL,
  GEM,
  WIRE_TORUS,
  SOLID_TORUS,
  WIRE_TWIST,
  SOLID_TWIST,
  PYRAMID,
  CONE,
  TEXT_RING,
  WIRE_BEZIER_DISC,
  SOLID_BEZIER_DISC,
  WIRE_BEZIER_SPHERE,
  SOLID_BEZIER_SPHERE,
  FISH,
  TURTLE,
  SHAPE_LIST_END
};

//Initialize and free a shape cache.
void start_shape_cache();
guint64 bytes_in_shape_cache();
gint shapes_in_shape_cache();
void print_shapes_in_cache();
void clear_shape_cache();
void free_shape_cache(); 
/*
  Initialize adjustable bezier sheet, disc and sphere. The points on these shapes can be changed. They are not automatically initialized.
  The other shapes are automatically initialized but the shapes are not intended to be changes after initialization.
*/
void initialize_wire_bezier_sheet(gint rows, gint columns, gint sheet_width, gint sheet_height);
void initialize_solid_bezier_sheet(gint rows, gint columns, gint sheet_width, gint sheet_height);
void initialize_wire_bezier_disc(gint rings, gint points_per_ring, gint radius);
void initialize_solid_bezier_disc(gint rings, gint points_per_ring, gint radius);
void initialize_wire_bezier_sphere(gint rings, gint points_per_ring, gint radius);
void initialize_solid_bezier_sphere(gint rings, gint points_per_ring, gint radius);
//Rotation functions.
void set_rotation3d(gdouble yaw, gdouble roll, gdouble pitch);
void set_rotation3d_identity();
void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9]);
void set_revolve_standard_point(gdouble x, gdouble y, gdouble z);
void get_revolved_point(gdouble *x, gdouble *y, gdouble *z);
void revolve_standard_point(gdouble qrs[9]);
void rotate_cache_shapes(gdouble qrs[9]);
void rotate_cache_shape_index(gdouble qrs[9], guint index);
//Save a shape to a key-value pair text file.
void save_wire_morph_sphere_to_file(const gchar *file_name, GArray *morph_points, gint radius, gint rings, gint points_per_ring);
GArray* get_wire_morph_sphere_from_file(const gchar *file_name, gint *cache_id, gint *radius, gint *rings, gint *points_per_ring);
void save_solid_morph_sphere_to_file(const gchar *file_name, GArray *morph_points, gint radius, gint rings, gint points_per_ring);
GArray* get_solid_morph_sphere_from_file(const gchar *file_name, gint *cache_id, gint *radius, gint *rings, gint *points_per_ring);
//Draw the shapes with transparency. This affects the sorting of quads also.
void set_transparency(gboolean value);
//Draw basic shapes that are automatically initialized.
void draw_spring(cairo_t *cr, gdouble w1, gdouble h1);
void draw_solid_cylinder(cairo_t *cr, gdouble w1, gdouble h1, gint points_per_ring);
void draw_cube(cairo_t *cr, gdouble w1, gdouble h1);
void draw_transparent_cube(cairo_t *cr, gdouble w1, gdouble h1);
void draw_checkerboard(cairo_t *cr, gdouble w1, gdouble h1, gint rows, gint columns);
void draw_ball(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
void draw_ball_mesh(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
void draw_wire_ball(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
void draw_wire_ball2(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
void draw_wire_sphere(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
void draw_solid_sphere(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
void draw_wire_funnel(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
void draw_solid_funnel(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
void draw_gem(cairo_t *cr, gdouble w1, gdouble h1);
void draw_wire_torus(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
void draw_solid_torus(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
void draw_wire_twist(cairo_t *cr, gdouble w1, gdouble h1, gint strands);
void draw_solid_twist(cairo_t *cr, gdouble w1, gdouble h1, gint strands);
void draw_pyramid(cairo_t *cr, gdouble w1, gdouble h1);
void draw_cone(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
void draw_cone_shade(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring);
void draw_text_ring(cairo_t *cr, gdouble w1, gdouble h1, gint points_per_ring);
//Some shapes that can be changed but are not automatically initialized. 
void draw_wire_bezier_sheet(cairo_t *cr, gint rows, gint columns);
void draw_solid_bezier_sheet(cairo_t *cr, gint rows, gint columns);
void draw_solid_bezier_sheet_mesh(cairo_t *cr, gint rows, gint columns);
void draw_wire_bezier_disc(cairo_t *cr, gint rings, gint points_per_ring);
void draw_solid_bezier_disc(cairo_t *cr, gint rings, gint points_per_ring);
void draw_solid_bezier_disc_mesh(cairo_t *cr, gint rings, gint points_per_ring);
void draw_wire_bezier_sphere(cairo_t *cr, gint rings, gint points_per_ring);
void draw_solid_bezier_sphere(cairo_t *cr, gint rings, gint points_per_ring, gint compare_quads, gdouble alpha);
void draw_solid_bezier_sphere_mesh(cairo_t *cr, gint rings, gint points_per_ring, gint compare_quads, gdouble alpha);
void draw_solid_bezier_sphere_explode(cairo_t *cr, gint rings, gint points_per_ring);
void draw_wrap_sphere(cairo_t *cr, gint rings, gint points_per_ring);
void draw_wrap_sphere_image(cairo_t *cr, gint rings, gint points_per_ring, GPtrArray *pictures);
//Draw 4 spheres using the shape cache index.
void draw_solid_bezier_sphere_index(cairo_t *cr, gint w1, gint h1, gint shape_cache_index, gint compare_quads, gdouble alpha);
/*
  Change shapes for the bezier sheet, disc and sphere. set_cosine_wave is a poor approximation but
  good for testing. The smoothing functions used by the bezier functions should give a better
  approximation. Maybe more art than math.
*/
void set_cosine_wave(gint cache_id, gint rings, gint points_per_ring, gdouble radius, gdouble z_height);
/*
  Set a z_min and z_max on a shape. Used for drawing gradients such as with the solid bezier sheet mesh example but can be set on any shape.
  Notice the shape_cache_index parameter in some of the following functions. A value of -1 will get the first occurrence of the shape
  in the cache and a value of 0 or greater will get the shape at that index.
*/
void set_shape_z_min_max(gint cache_id, gint rings, gint points_per_ring, gdouble min, gdouble max, gint shape_cache_index); 
//These add, as in addition, to the existing points. No new points are added.
void set_bezier_sheet_points(gint cache_id, gint rings, gint points_per_ring, GArray *add_points, gdouble z_min, gdouble z_max);
void set_bezier_disc_points(gint cache_id, gint rings, gint points_per_ring, GArray *add_points, gdouble z_min, gdouble z_max);
void set_bezier_sphere_single_point(gint cache_id, gint rings, gint points_per_ring, struct morph_sphere_point *point, gint shape_cache_index);
//Smoothing functions for the bezier spheres.
void smooth_wire_bezier_sphere_longitude(gint cache_id, gint rings, gint points_per_ring, gint shape_cache_index);
void smooth_wire_bezier_sphere_latitude(gint cache_id, gint rings, gint points_per_ring, gint shape_cache_index);
void smooth_solid_bezier_sphere_longitude(gint cache_id, gint rings, gint points_per_ring, gint shape_cache_index);
void smooth_solid_bezier_sphere_latitude(gint cache_id, gint rings, gint points_per_ring, gint shape_cache_index);
//Some art.
void draw_fish(cairo_t *cr, gdouble w1, gdouble h1);
void draw_turtle(cairo_t *cr, gdouble w1, gdouble h1);


