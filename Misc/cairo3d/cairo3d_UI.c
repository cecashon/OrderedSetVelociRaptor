/*
    Draw some basic 3d shapes and art with cairo. 

    There are some bezier surfaces that can be helpful for graphing and a bezier sphere that
 can be deformed into different shapes. An exploding sphere also.
    
    gcc -Wall cairo3d.c cairo3d_UI.c -o cairo3d_shapes `pkg-config --cflags --libs gtk+-3.0` -lm

    Tested on Ubuntu20.04 and GTK3.24
*/

#include<gtk/gtk.h>
#include<math.h>
#include<stdlib.h>
#include<cairo-svg.h>
#include<cairo-pdf.h>
#include<cairo-ps.h>
#include"cairo3d.h"

//For holding points for the morphing sphere.
GArray *morph_points=NULL;

/*
  Used for calculating points in .svg, .pdf and .ps output.
  Why 90. This works on the test computer. When a .svg is saved and then opened, the pixels
match the original drawing. Rsvg rsvg_handle_set_dpi() notes "Common values are 75, 90,
and 300 DPI." which it looks like 90 is being used as default to open the picture. 
  The ppi might need to be changed for matching ouput on different computers.
*/
static gdouble ppi=90.0;
//Combo ids for drawing options.
static gint rotate=0;
static gint drawing_id=0;
static gint initialize_id=0;
//Tick id for animation frame clock.
static guint tick_id=0;
//Scale the cube.
static gdouble slider_scale=1.0;
//For the performance check boxes.
static gboolean frame_rate=FALSE;
static gboolean function_time=FALSE;
//Sizing for output.
static gdouble layout_width=500.0;
static gdouble layout_height=500.0;
static gdouble translate_x=0.0;
static gdouble translate_y=0.0;
static gdouble background_color[4]={1.0, 1.0, 1.0, 1.0};
//For bezier random drawings. Rows/rings or columns/points_per_ring.
static gint bezier_rows=10;
static gint bezier_columns=10;
static gint sphere_rows=10;
static gint sphere_columns=10;
static gboolean smooth_longitude=FALSE;
static gboolean smooth_latitude=FALSE;
//The sorting function to use with the solid bezier sphere.
static gint compare_quads=1;
//Test transparency with the solid bezier sphere.
static gdouble alpha=1.0;
//Block signals to index spinners.
static gulong index_id=0;
static gulong index_ray_id=0;

//Test array of image surfaces.
GPtrArray *pictures=NULL;

static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data);
static void show_frame_rate(GtkToggleButton *button, gpointer data);
static void show_function_time(GtkToggleButton *button, gpointer data);
static void check_longitude(GtkToggleButton *button, gpointer *data);
static void check_latitude(GtkToggleButton *button, gpointer *data);
static void combo_compare_quads(GtkComboBox *combo, gpointer *data);
static void combo_alpha_quads(GtkComboBox *combo, gpointer *data);
static void rotate_drawing(GtkComboBox *combo, gpointer *rotations);
static void change_drawing(GtkComboBox *combo, gpointer data);
static gboolean initialize_bezier_surfaces(gpointer data);
static void set_rotation_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations);
static void set_rotation_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations);
static void set_rotation_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations);
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void layout_width_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void layout_height_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void translate_x_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void translate_y_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void bezier_row_spin_changed(GtkSpinButton *spin_button, gpointer *data);
static void bezier_column_spin_changed(GtkSpinButton *spin_button, gpointer *data);
static void sphere_row_spin_changed(GtkSpinButton *spin_button, gpointer *data);
static void sphere_column_spin_changed(GtkSpinButton *spin_button, gpointer *data);
static void sphere_point_spin_changed(GtkSpinButton *spin_button, gpointer *data);
static void sphere_point_spin_ray_changed(GtkSpinButton *spin_button, gpointer *data);
static void clear_morph_points(GtkWidget *button, gpointer data);
static void set_background_color(GtkWidget *button, gpointer *data);
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer *rotations);
//The start of drawing.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer *rotations);
static void draw_svg(GtkWidget *widget, gpointer *rotations);
static void draw_pdf(GtkWidget *widget, gpointer *rotations);
static void draw_ps(GtkWidget *widget, gpointer *rotations);
static void draw_png(GtkWidget *widget, gpointer *rotations);
static void save_wire_sphere_kvp(GtkWidget *widget, gpointer *data);
static void save_solid_sphere_kvp(GtkWidget *widget, gpointer *data);
static void save_file_chooser_kvp(gpointer *rotations);
static void open_wire_sphere_kvp(GtkWidget *button, gpointer *data);
static void open_solid_sphere_kvp(GtkWidget *button, gpointer *data);
static void save_solid_sphere_mesh_kvp(GtkWidget *widget, gpointer *data);
static void open_solid_sphere_mesh_kvp(GtkWidget *button, gpointer *data);
static void open_file_chooser_kvp(gpointer *data);
static void draw_surface(cairo_t *cr, gdouble width, gdouble height, gpointer *rotations, gboolean coords, guint surface_type);
//Save a morph sphere to a key-value pair text file.
static void get_wire_morph_sphere_kvp(gchar *file_name, gpointer *data);
static void get_solid_morph_sphere_kvp(gchar *file_name, gpointer *data);
//About the program.
static void about_dialog(GtkWidget *widget, gpointer data);
static GdkPixbuf* draw_icon();
static GPtrArray* create_pictures_array();
static void free_pictures(GPtrArray *pictures);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);
    
    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Cairo3d Shapes");
    gtk_window_set_default_size(GTK_WINDOW(window), 1000, 650);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_widget_set_app_paintable(window, TRUE);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    //Try to set transparency of main window.
    GdkScreen *screen=gtk_widget_get_screen(window); 
    if(gdk_screen_is_composited(screen))
      {
        GdkVisual *visual=gdk_screen_get_rgba_visual(screen);
        gtk_widget_set_visual(window, visual);
      }
    else
      {
        g_print("Can't set window transparency.\n");
      } 

    //Initialize the shape cache.
    start_shape_cache();
    
    //Draw a 256x256 sphere program icon.
    GdkPixbuf *icon=draw_icon();
    gtk_window_set_default_icon(icon);
    
    //Create an array of cairo images for testing.
    pictures=create_pictures_array();

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);

    GtkWidget *da_scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(da_scroll, TRUE);
    gtk_widget_set_vexpand(da_scroll, TRUE);
    gtk_widget_set_size_request(da, 1000, 1000);
    gtk_container_add(GTK_CONTAINER(da_scroll), da);

    GtkWidget *combo_rotate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_rotate, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 0, "1", "Draw Shape");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_rotate), 1, "2", "Animate Rotation");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_rotate), 0);

    /*
      The drawing_id is the first column and the initialize_id is the second column. The initialize_id
    is so that you can initialize and draw many different shapes. For 0-18 Cache_id
    is equal to initialize_id.
    */
    GtkWidget *combo_drawing=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_drawing, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "0", "Spring");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "1", "Solid Cylinder");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 2, "2", "Cube");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 3, "2", "Transparent Cube");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 4, "3", "Checkerboard");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 5, "6", "Ball");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 6, "6", "Wire Ball");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 7, "6", "Wire Ball2");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 8, "6", "No Anti Alias Wire Ball2");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 9, "7", "Wire Sphere");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 10, "8", "Solid Sphere");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 11, "9", "Wire Funnel");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 12, "10", "Solid Funnel");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 13, "11", "Gem");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 14, "12", "Wire Torus");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 15, "13", "Torus");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 16, "13", "4-Tori");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 17, "14", "Wire Twist");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 18, "15", "Solid Twist");
    //gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 19, "12", "Triangle Shadow & Light");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 19, "16", "Pyramid");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 20, "17", "Cone");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 21, "17", "Cone Shade");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 22, "18", "Text Ring");
    //Draw several shapes.
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 23, "30", "Ball, Gem, Twist and Cone");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 24, "30", "Ball, Gem, Twist and Cone2");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 25, "31", "Revolve and Rotate Ball");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 26, "32", "Planets");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 27, "33", "Ball and Cone");
    //A bezier sheet.
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 28, "4", "Wire Bezier Sheet");
    //Make initialize_id=100 to test smoothing a surface with a random array of z points.
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 29, "100", "Wire Bezier Random");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 30, "5", "Solid Bezier Sheet");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 31, "5", "Solid Bezier Sheet Mesh");
    //Make initialize_id=101 to test smoothing a surface with a random array of z points.
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 32, "101", "Solid Bezier Sheet Random");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 33, "19", "Wire Bezier Disc");
    //Make initialize_id=102 to test smoothing a surface with a random array of z points.
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 34, "102", "Wire Bezier Disc Random");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 35, "20", "Solid Bezier Disc");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 36, "20", "Solid Bezier Disc Mesh");
    //Make initialize_id=103 to test smoothing a surface with a random array of z points.
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 37, "103", "Solid Bezier Disc Random");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 38, "104", "Morph Wire Bezier Sphere");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 39, "105", "Morph Solid Bezier Sphere");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 40, "106", "Morph Solid Bezier Sphere Mesh");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 41, "107", "Explode A Sphere");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 42, "108", "Four Morph Spheres");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 43, "109", "Wrap Sphere");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 44, "110", "Wrap Sphere Image");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 45, "21", "Fish");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 46, "22", "Turtle");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
    g_signal_connect(combo_drawing, "changed", G_CALLBACK(change_drawing), da);  

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, FALSE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, FALSE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, FALSE);
    
    GtkWidget *scale_label=gtk_label_new("Scale");
    gtk_widget_set_hexpand(scale_label, FALSE);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_hexpand(pitch_slider, FALSE);
    gtk_widget_set_vexpand(pitch_slider, TRUE);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_hexpand(roll_slider, FALSE);
    gtk_widget_set_vexpand(roll_slider, TRUE);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_hexpand(yaw_slider, FALSE);
    gtk_widget_set_vexpand(yaw_slider, TRUE);

    GtkWidget *scale_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 2.0, 0.01);
    gtk_widget_set_hexpand(scale_slider, FALSE);
    gtk_widget_set_vexpand(scale_slider, TRUE);
    gtk_range_set_value(GTK_RANGE(scale_slider), 1.0);

    //Set initial rotations. Yaw, roll and pitch.
    gdouble plane_r[3]={0.0, 0.0, 0.0};

    //Setup callbacks.
    gpointer rotations[]={plane_r, da, window};
    g_signal_connect(da, "draw", G_CALLBACK(draw_main), rotations);
    g_signal_connect(combo_rotate, "changed", G_CALLBACK(rotate_drawing), rotations);
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_rotation_pitch), rotations);   
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_rotation_roll), rotations);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_rotation_yaw), rotations);
    g_signal_connect(scale_slider, "change-value", G_CALLBACK(set_scale), da);

    //Some checks for performance.
    GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
    gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
    g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), NULL);

    GtkWidget *check_functions=gtk_check_button_new_with_label("Show Function Time");
    gtk_widget_set_halign(check_functions, GTK_ALIGN_CENTER);
    g_signal_connect(check_functions, "toggled", G_CALLBACK(show_function_time), NULL);

    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_rotate, 0, 0, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_drawing, 0, 1, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale_label, 3, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale_slider, 3, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_frame, 0, 4, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_functions, 0, 5, 4, 1);

    GtkWidget *scroll1=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll1, TRUE);
    gtk_widget_set_vexpand(scroll1, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll1), grid1);

    GtkAdjustment *translate_x_adj=gtk_adjustment_new(0.0, -500.0, 500.0, 10.0, 0.0, 0.0);
    GtkAdjustment *translate_y_adj=gtk_adjustment_new(0.0, -500.0, 500.0, 10.0, 0.0, 0.0);
    GtkAdjustment *layout_width_adj=gtk_adjustment_new(500.0, 1.0, 2000.0, 50.0, 0.0, 0.0);
    GtkAdjustment *layout_height_adj=gtk_adjustment_new(500.0, 1.0, 2000.0, 50.0, 0.0, 0.0); 

    GtkWidget *translate_x_label=gtk_label_new("Translate x");
    gtk_widget_set_hexpand(translate_x_label, FALSE);
    GtkWidget *translate_x_spin=gtk_spin_button_new(translate_x_adj, 0.0, 1);
    gtk_widget_set_hexpand(translate_x_spin, FALSE);
    g_signal_connect(translate_x_spin, "value-changed", G_CALLBACK(translate_x_spin_changed), da); 

    GtkWidget *translate_y_label=gtk_label_new("Translate y");
    gtk_widget_set_hexpand(translate_y_label, FALSE);
    GtkWidget *translate_y_spin=gtk_spin_button_new(translate_y_adj, 0.0, 1);
    gtk_widget_set_hexpand(translate_y_spin, FALSE);
    g_signal_connect(translate_y_spin, "value-changed", G_CALLBACK(translate_y_spin_changed), da); 

    GtkWidget *layout_width_label=gtk_label_new("Layout Width");
    gtk_widget_set_hexpand(layout_width_label, FALSE);
    GtkWidget *layout_width_spin=gtk_spin_button_new(layout_width_adj, 0.0, 1);
    gtk_widget_set_hexpand(layout_width_spin, FALSE);
    g_signal_connect(layout_width_spin, "value-changed", G_CALLBACK(layout_width_spin_changed), da);

    GtkWidget *layout_height_label=gtk_label_new("Layout Height");
    gtk_widget_set_hexpand(layout_height_label, FALSE);
    GtkWidget *layout_height_spin=gtk_spin_button_new(layout_height_adj, 0.0, 1);
    gtk_widget_set_hexpand(layout_height_spin, FALSE);
    g_signal_connect(layout_height_spin, "value-changed", G_CALLBACK(layout_height_spin_changed), da);

    GtkWidget *background_label=gtk_label_new("Background Color");
    gtk_widget_set_hexpand(background_label, FALSE);
    GtkWidget *background_entry=gtk_entry_new();
    gtk_widget_set_hexpand(background_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(background_entry), "rgba(255, 255, 255, 1.0)");

    GtkWidget *background_button=gtk_button_new_with_label("Update Background Color");
    gtk_widget_set_hexpand(background_button, FALSE);
    gpointer w1[]={da, background_entry};
    g_signal_connect(background_button, "clicked", G_CALLBACK(set_background_color), w1);

    GtkAdjustment *bezier_row_adj=gtk_adjustment_new(10.0, 5.0, 30.0, 1.0, 0.0, 0.0);
    GtkAdjustment *bezier_column_adj=gtk_adjustment_new(10.0, 5.0, 30.0, 1.0, 0.0, 0.0);

    gpointer w2[2]={combo_drawing, da};

    GtkWidget *random_label=gtk_label_new("Random Bezier Drawings");
    gtk_widget_set_hexpand(random_label, FALSE);

    GtkWidget *bezier_row_label=gtk_label_new("Bezier Rows");
    gtk_widget_set_hexpand(bezier_row_label, FALSE);
    GtkWidget *bezier_row_spin=gtk_spin_button_new(bezier_row_adj, 0.0, 0);
    gtk_widget_set_hexpand(bezier_row_spin, FALSE);
    g_signal_connect(bezier_row_spin, "value-changed", G_CALLBACK(bezier_row_spin_changed), w2);

    GtkWidget *bezier_column_label=gtk_label_new("Bezier Columns");
    gtk_widget_set_hexpand(bezier_column_label, FALSE);
    GtkWidget *bezier_column_spin=gtk_spin_button_new(bezier_column_adj, 0.0, 0);
    gtk_widget_set_hexpand(bezier_column_spin, FALSE);
    g_signal_connect(bezier_column_spin, "value-changed", G_CALLBACK(bezier_column_spin_changed), w2);
    
    GtkWidget *space1=gtk_label_new("");

    GtkWidget *grid2=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid2), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid2), 8);
    gtk_grid_attach(GTK_GRID(grid2), translate_x_label, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), translate_y_label, 1, 0, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid2), translate_x_spin, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), translate_y_spin, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), layout_width_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), layout_height_label, 1, 2, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid2), layout_width_spin, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), layout_height_spin, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), background_label, 0, 4, 2, 1);
    gtk_grid_attach(GTK_GRID(grid2), background_entry, 0, 5, 2, 1);
    gtk_grid_attach(GTK_GRID(grid2), background_button, 0, 6, 2, 1);
    gtk_grid_attach(GTK_GRID(grid2), space1, 0, 7, 2, 1);
    gtk_grid_attach(GTK_GRID(grid2), random_label, 0, 8, 2, 1);
    gtk_grid_attach(GTK_GRID(grid2), bezier_row_label, 0, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), bezier_column_label, 1, 9, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid2), bezier_row_spin, 0, 10, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), bezier_column_spin, 1, 10, 1, 1);

    GtkWidget *scroll2=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll2, TRUE);
    gtk_widget_set_vexpand(scroll2, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll2), grid2);

    GtkAdjustment *sphere_row_adj=gtk_adjustment_new(10.0, 8.0, 30.0, 1.0, 0.0, 0.0);
    GtkAdjustment *sphere_column_adj=gtk_adjustment_new(10.0, 10.0, 30.0, 2.0, 0.0, 0.0);

    GtkWidget *morph_sphere_label=gtk_label_new("Morph Sphere Bezier Drawings");
    gtk_widget_set_hexpand(morph_sphere_label, FALSE);

    GtkWidget *sphere_row_label=gtk_label_new("Rings");
    gtk_widget_set_hexpand(sphere_row_label, FALSE);
    GtkWidget *sphere_row_spin=gtk_spin_button_new(sphere_row_adj, 0.0, 0);
    gtk_widget_set_hexpand(sphere_row_spin, FALSE);

    GtkWidget *sphere_column_label=gtk_label_new("Points Per Ring");
    gtk_widget_set_hexpand(sphere_column_label, FALSE);
    GtkWidget *sphere_column_spin=gtk_spin_button_new(sphere_column_adj, 0.0, 0);
    gtk_widget_set_hexpand(sphere_column_spin, FALSE);

    GtkWidget *check_box_longitude=gtk_check_button_new_with_label("Smooth Longitude");
    gtk_widget_set_halign(check_box_longitude, GTK_ALIGN_CENTER);
    g_signal_connect(check_box_longitude, "toggled", G_CALLBACK(check_longitude), w2);

    GtkWidget *check_box_latitude=gtk_check_button_new_with_label("Smooth Latitude");
    gtk_widget_set_halign(check_box_latitude, GTK_ALIGN_CENTER);
    g_signal_connect(check_box_latitude, "toggled", G_CALLBACK(check_latitude), w2);

    GtkWidget *combo_compare=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_compare, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_compare), 0, "1", "No Quad Sorting");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_compare), 1, "2", "Sort Quad Z");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_compare), 2, "3", "Sort Quad Cross Product");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_compare), 1);
    g_signal_connect(combo_compare, "changed", G_CALLBACK(combo_compare_quads), w2);
    GtkWidget *combo_alpha=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_alpha, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_alpha), 0, "1", "Alpha 1.0");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_alpha), 1, "2", "Alpha 0.5");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_alpha), 2, "3", "Alpha 0.3");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_alpha), 0);
    g_signal_connect(combo_alpha, "changed", G_CALLBACK(combo_alpha_quads), w2);
    
    //An array for adding to the points of the morph sphere.
    morph_points=g_array_sized_new(FALSE, TRUE, sizeof(struct morph_sphere_point), ((sphere_rows*2)*(sphere_columns/2+1))-1);
    g_array_set_size(morph_points, ((sphere_rows*2)*(sphere_columns/2+1))-1);
    
    GtkAdjustment *sphere_point_index_adj=gtk_adjustment_new(-1.0, -1.0, 119.0, 1.0, 0.0, 0.0);
    GtkAdjustment *sphere_point_x_adj=gtk_adjustment_new(0.0, -200.0, 200.0, 1.0, 0.0, 0.0);
    GtkAdjustment *sphere_point_y_adj=gtk_adjustment_new(0.0, -200.0, 200.0, 1.0, 0.0, 0.0);
    GtkAdjustment *sphere_point_z_adj=gtk_adjustment_new(-50.0, -200.0, 200.0, 1.0, 0.0, 0.0);
    GtkAdjustment *sphere_point_index_ray_adj=gtk_adjustment_new(-1.0, -1.0, 119.0, 1.0, 0.0, 0.0);
    GtkAdjustment *sphere_point_ray_adj=gtk_adjustment_new(30.0, -200.0, 200.0, 1.0, 0.0, 0.0);

    GtkWidget *sphere_point_label=gtk_label_new("Add to a Morphing Sphere Point");
    gtk_widget_set_hexpand(sphere_point_label, FALSE);
    
    GtkWidget *sphere_point_index_label=gtk_label_new("index");
    gtk_widget_set_hexpand(sphere_point_index_label, FALSE);
    GtkWidget *sphere_point_spin_index=gtk_spin_button_new(sphere_point_index_adj, 0.0, 0);
    gtk_widget_set_hexpand(sphere_point_spin_index, FALSE);

    GtkWidget *sphere_point_x_label=gtk_label_new("x");
    gtk_widget_set_hexpand(sphere_point_x_label, FALSE);
    GtkWidget *sphere_point_spin_x=gtk_spin_button_new(sphere_point_x_adj, 0.0, 0);
    gtk_widget_set_hexpand(sphere_point_spin_x, FALSE);
    
    GtkWidget *sphere_point_y_label=gtk_label_new("y");
    gtk_widget_set_hexpand(sphere_point_y_label, FALSE);
    GtkWidget *sphere_point_spin_y=gtk_spin_button_new(sphere_point_y_adj, 0.0, 0);
    gtk_widget_set_hexpand(sphere_point_spin_y, FALSE);
    
    GtkWidget *sphere_point_z_label=gtk_label_new("z");
    gtk_widget_set_hexpand(sphere_point_z_label, FALSE);
    GtkWidget *sphere_point_spin_z=gtk_spin_button_new(sphere_point_z_adj, 0.0, 0);
    gtk_widget_set_hexpand(sphere_point_spin_z, FALSE);
    
    GtkWidget *sphere_point_index_ray_label=gtk_label_new("index ray");
    gtk_widget_set_hexpand(sphere_point_index_ray_label, FALSE);
    GtkWidget *sphere_point_spin_index_ray=gtk_spin_button_new(sphere_point_index_ray_adj, 0.0, 0);
    gtk_widget_set_hexpand(sphere_point_spin_index_ray, FALSE);
    
    GtkWidget *sphere_point_ray_label=gtk_label_new("ray");
    gtk_widget_set_hexpand(sphere_point_ray_label, FALSE);
    GtkWidget *sphere_point_spin_ray=gtk_spin_button_new(sphere_point_ray_adj, 0.0, 0);
    gtk_widget_set_hexpand(sphere_point_spin_ray, FALSE);
    
    gpointer w3[8]={sphere_point_spin_index, sphere_point_spin_x, sphere_point_spin_y, sphere_point_spin_z, combo_drawing, da, sphere_point_spin_index_ray, sphere_point_spin_ray};
    index_id=g_signal_connect(sphere_point_spin_index, "value-changed", G_CALLBACK(sphere_point_spin_changed), w3);
    g_signal_connect(sphere_point_spin_x, "value-changed", G_CALLBACK(sphere_point_spin_changed), w3);
    g_signal_connect(sphere_point_spin_y, "value-changed", G_CALLBACK(sphere_point_spin_changed), w3);
    g_signal_connect(sphere_point_spin_z, "value-changed", G_CALLBACK(sphere_point_spin_changed), w3); 
    index_ray_id=g_signal_connect(sphere_point_spin_index_ray, "value-changed", G_CALLBACK(sphere_point_spin_ray_changed), w3); 
    g_signal_connect(sphere_point_spin_ray, "value-changed", G_CALLBACK(sphere_point_spin_ray_changed), w3); 
    
    //Need to adjust the index spinner top value with these signals.
    g_signal_connect(sphere_row_spin, "value-changed", G_CALLBACK(sphere_row_spin_changed), w3); 
    g_signal_connect(sphere_column_spin, "value-changed", G_CALLBACK(sphere_column_spin_changed), w3); 
    
    GtkWidget *clear_points=gtk_button_new_with_label("Clear Points");
    gtk_widget_set_hexpand(clear_points, FALSE);
    gtk_widget_set_halign(clear_points, GTK_ALIGN_CENTER);
    g_signal_connect(clear_points, "clicked", G_CALLBACK(clear_morph_points), da); 
    
    GtkWidget *space2=gtk_label_new("");

    GtkWidget *grid3=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid3), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid3), 8);
    gtk_grid_attach(GTK_GRID(grid3), morph_sphere_label, 0, 0, 2, 1);
    gtk_grid_attach(GTK_GRID(grid3), sphere_row_label, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), sphere_column_label, 1, 1, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid3), sphere_row_spin, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), sphere_column_spin, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), check_box_longitude, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), check_box_latitude, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), combo_compare, 0, 5, 2, 1);
    gtk_grid_attach(GTK_GRID(grid3), combo_alpha, 0, 6, 2, 1);   
    gtk_grid_attach(GTK_GRID(grid3), space2, 0, 7, 2, 1);   
    gtk_grid_attach(GTK_GRID(grid3), sphere_point_label, 0, 8, 2, 1);
    gtk_grid_attach(GTK_GRID(grid3), sphere_point_index_label, 0, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), sphere_point_x_label, 1, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), sphere_point_spin_index, 0, 10, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), sphere_point_spin_x, 1, 10, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid3), sphere_point_y_label, 0, 11, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), sphere_point_z_label, 1, 11, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), sphere_point_spin_y, 0, 12, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), sphere_point_spin_z, 1, 12, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid3), sphere_point_index_ray_label, 0, 13, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), sphere_point_ray_label, 1, 13, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), sphere_point_spin_index_ray, 0, 14, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), sphere_point_spin_ray, 1, 14, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid3), clear_points, 0, 15, 2, 1);

    GtkWidget *scroll3=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll3, TRUE);
    gtk_widget_set_vexpand(scroll3, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll3), grid3);
    
    GtkWidget *save_wire_morph_sphere=gtk_button_new_with_label("Save Wire Morph Sphere");
    gtk_widget_set_hexpand(save_wire_morph_sphere, FALSE);
    gtk_widget_set_halign(save_wire_morph_sphere, GTK_ALIGN_CENTER);
    g_signal_connect(save_wire_morph_sphere, "clicked", G_CALLBACK(save_wire_sphere_kvp), rotations); 
    
    GtkWidget *get_wire_morph_sphere=gtk_button_new_with_label("Restore Wire Morph Sphere");
    gtk_widget_set_hexpand(get_wire_morph_sphere, FALSE);
    gtk_widget_set_halign(get_wire_morph_sphere, GTK_ALIGN_CENTER);
    gpointer w4[4]={sphere_row_spin, sphere_column_spin, da, window};
    g_signal_connect(get_wire_morph_sphere, "clicked", G_CALLBACK(open_wire_sphere_kvp), w4); 
    
    GtkWidget *save_solid_morph_sphere=gtk_button_new_with_label("Save Solid Morph Sphere");
    gtk_widget_set_hexpand(save_solid_morph_sphere, FALSE);
    gtk_widget_set_halign(save_solid_morph_sphere, GTK_ALIGN_CENTER);
    g_signal_connect(save_solid_morph_sphere, "clicked", G_CALLBACK(save_solid_sphere_kvp), rotations); 
    
    GtkWidget *get_solid_morph_sphere=gtk_button_new_with_label("Restore Solid Morph Sphere");
    gtk_widget_set_hexpand(get_solid_morph_sphere, FALSE);
    gtk_widget_set_halign(get_solid_morph_sphere, GTK_ALIGN_CENTER);
    g_signal_connect(get_solid_morph_sphere, "clicked", G_CALLBACK(open_solid_sphere_kvp), w4); 
    
    GtkWidget *save_solid_morph_sphere_mesh=gtk_button_new_with_label("Save Solid Morph Sphere Mesh");
    gtk_widget_set_hexpand(save_solid_morph_sphere_mesh, FALSE);
    gtk_widget_set_halign(save_solid_morph_sphere_mesh, GTK_ALIGN_CENTER);
    g_signal_connect(save_solid_morph_sphere_mesh, "clicked", G_CALLBACK(save_solid_sphere_mesh_kvp), rotations); 
    
    GtkWidget *get_solid_morph_sphere_mesh=gtk_button_new_with_label("Restore Solid Morph Sphere Mesh");
    gtk_widget_set_hexpand(get_solid_morph_sphere_mesh, FALSE);
    gtk_widget_set_halign(get_solid_morph_sphere_mesh, GTK_ALIGN_CENTER);
    g_signal_connect(get_solid_morph_sphere_mesh, "clicked", G_CALLBACK(open_solid_sphere_mesh_kvp), w4); 
    
    GtkWidget *grid4=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid4), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid4), 8);
    gtk_grid_attach(GTK_GRID(grid4), save_wire_morph_sphere, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), get_wire_morph_sphere, 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), save_solid_morph_sphere, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), get_solid_morph_sphere, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), save_solid_morph_sphere_mesh, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), get_solid_morph_sphere_mesh, 1, 2, 1, 1);
    
    GtkWidget *scroll4=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll4, TRUE);
    gtk_widget_set_vexpand(scroll4, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll4), grid4);

    GtkWidget *nb_label1=gtk_label_new("Cairo3d Shapes");
    GtkWidget *nb_label2=gtk_label_new("Options1");
    GtkWidget *nb_label3=gtk_label_new("Options2");
    GtkWidget *nb_label4=gtk_label_new("Options3");
    GtkWidget *notebook=gtk_notebook_new();
    gtk_container_set_border_width(GTK_CONTAINER(notebook), 15);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll1, nb_label1);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll2, nb_label2);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll3, nb_label3);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll4, nb_label4);
    
    GtkWidget *scroll5=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll5, TRUE);
    gtk_widget_set_vexpand(scroll5, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll5), notebook);
   
    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), scroll5, TRUE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da_scroll, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 350);

    GtkWidget *menu1=gtk_menu_new();
    GtkWidget *menu1item1=gtk_menu_item_new_with_label("Save as .svg");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item1);
    GtkWidget *menu1item2=gtk_menu_item_new_with_label("Save as .pdf");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item2);
    GtkWidget *menu1item3=gtk_menu_item_new_with_label("Save as .ps");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item3);
    GtkWidget *menu1item4=gtk_menu_item_new_with_label("Save as .png");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item4);
    GtkWidget *title1=gtk_menu_item_new_with_label("Save Drawing");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(title1), menu1);

    GtkWidget *menu2=gtk_menu_new();
    GtkWidget *menu2item1=gtk_menu_item_new_with_label("Cairo3d Shapes");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu2), menu2item1);
    GtkWidget *title2=gtk_menu_item_new_with_label("About");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(title2), menu2);

    GtkWidget *menu_bar=gtk_menu_bar_new();
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), title1);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), title2);

    g_signal_connect(menu1item1, "activate", G_CALLBACK(draw_svg), rotations);
    g_signal_connect(menu1item2, "activate", G_CALLBACK(draw_pdf), rotations);
    g_signal_connect(menu1item3, "activate", G_CALLBACK(draw_ps), rotations);
    g_signal_connect(menu1item4, "activate", G_CALLBACK(draw_png), rotations);
    g_signal_connect(menu2item1, "activate", G_CALLBACK(about_dialog), window);

    GtkWidget *grid5=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid5), menu_bar, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid5), paned1, 0, 1, 1, 1);
   
    gtk_container_add(GTK_CONTAINER(window), grid5);

    //Draw background window based on the paned window splitter.
    g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), paned1);

    gchar *css_string=g_strdup("#about_dialog{background: rgba(127,127,255,1.0); font-size: 20px; font-weight: bold;} #about_dialog label selection{background: rgba(0,50,220,0.5);}");
    GError *css_error=NULL;
    GtkCssProvider *provider=gtk_css_provider_new();
    gtk_css_provider_load_from_data(provider, css_string, -1, &css_error);
    gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    if(css_error!=NULL)
      {
        g_print("CSS loader error %s\n", css_error->message);
        g_error_free(css_error);
      }
    g_object_unref(provider);
    g_free(css_string);

    gtk_widget_show_all(window);

    gtk_main();

    //Free the shape cache.
    free_shape_cache();
    
    free_pictures(pictures);
    g_array_free(morph_points, TRUE);
    g_object_unref(icon);

    return 0;  
  }
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data)
  {
    //Paint background of drawing area.
    cairo_set_source_rgba(cr, background_color[0], background_color[1], background_color[2], background_color[3]);
    cairo_paint(cr);
    //Paint the background under the grid.
    cairo_set_source_rgba(cr, 0.5, 0.5, 1.0, 1.0);
    gint width=gtk_paned_get_position(GTK_PANED(data));
    gint height=gtk_widget_get_allocated_height(widget);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_rectangle(cr, width-4, 0.0, 4, height);
    cairo_fill(cr);
    return FALSE;
  }
static void show_frame_rate(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_function_time(GtkToggleButton *button, gpointer data)
  {
    if(gtk_toggle_button_get_active(button)) function_time=TRUE;
    else function_time=FALSE;
  }
static void check_longitude(GtkToggleButton *button, gpointer *data)
  {
    if(gtk_toggle_button_get_active(button)) smooth_longitude=TRUE;
    else smooth_longitude=FALSE;
    initialize_id=atoi(gtk_combo_box_get_active_id(GTK_COMBO_BOX(data[0])));
    g_idle_add_full(G_PRIORITY_HIGH_IDLE, initialize_bezier_surfaces, (gpointer)(&initialize_id), NULL);
    gtk_widget_queue_draw(GTK_WIDGET(data[1]));
  }
static void check_latitude(GtkToggleButton *button, gpointer *data)
  {
    if(gtk_toggle_button_get_active(button)) smooth_latitude=TRUE;
    else smooth_latitude=FALSE;
    initialize_id=atoi(gtk_combo_box_get_active_id(GTK_COMBO_BOX(data[0])));
    g_idle_add_full(G_PRIORITY_HIGH_IDLE, initialize_bezier_surfaces, (gpointer)(&initialize_id), NULL);
    gtk_widget_queue_draw(GTK_WIDGET(data[1]));
  }
static void combo_compare_quads(GtkComboBox *combo, gpointer *data)
  {
    compare_quads=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data[1]));
  }
static void combo_alpha_quads(GtkComboBox *combo, gpointer *data)
  {
    gint combo_id=gtk_combo_box_get_active(combo);
    if(combo_id==1) alpha=0.5;
    else if(combo_id==2) alpha=0.3;
    else alpha=1.0;
    gtk_widget_queue_draw(GTK_WIDGET(data[1]));
  }
static void rotate_drawing(GtkComboBox *combo, gpointer *rotations)
  {
    rotate=gtk_combo_box_get_active(combo);
    //Reset rotations for animation.
    ((gdouble*)(rotations[0]))[0]=0.0;
    ((gdouble*)(rotations[0]))[1]=0.0;
    ((gdouble*)(rotations[0]))[2]=0.0;
    
    if(rotate==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(rotations[1]), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(rotations[1]));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(rotations[1]), (GtkTickCallback)animate_drawing, rotations, NULL);
          }
      }
    
  }
static void change_drawing(GtkComboBox *combo, gpointer data)
  {
    initialize_id=atoi(gtk_combo_box_get_active_id(combo));

    g_idle_add_full(G_PRIORITY_HIGH_IDLE, initialize_bezier_surfaces, (gpointer)(&initialize_id), NULL);
    
    drawing_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static gboolean initialize_bezier_surfaces(gpointer data)
  {
    /*
      Explicitly initialize the bezier shapes so that they can be changed before drawn. The
      other shapes are initialized in their draw function the first time they are used.
    */
    gint initialize_id=*((gint*)data);
    
    clear_shape_cache();

    if(initialize_id==WIRE_BEZIER_SHEET)
      {
        initialize_wire_bezier_sheet(17, 17, 250, 250);   
        set_cosine_wave(WIRE_BEZIER_SHEET, 17, 17, 125.0, 40.0);
      }
    if(initialize_id==100)
      {
        initialize_wire_bezier_sheet(bezier_rows, bezier_columns, 250, 250);

        gint i=0;
        //Add 1 for line count.
        gint len=(bezier_rows+1)*(bezier_columns+1);
        GArray *add_points=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), len);
        g_array_set_size(add_points, len);
        struct point3d *p1=&g_array_index(add_points, struct point3d, 0);
        for(i=0;i<len;i++)
          {
             (p1->z)=g_random_double_range(-30.0, 30.0);
             p1++;
          }
        set_bezier_sheet_points(WIRE_BEZIER_SHEET, bezier_rows, bezier_columns, add_points, -30.0, 30.0);
        g_array_free(add_points, TRUE);
      }
    if(initialize_id==SOLID_BEZIER_SHEET)
      {
        initialize_solid_bezier_sheet(17, 17, 250, 250);
        set_cosine_wave(SOLID_BEZIER_SHEET, 17, 17, 125.0, 40.0);
      }
    if(initialize_id==101)
      {
        initialize_solid_bezier_sheet(bezier_rows, bezier_columns, 250, 250);

        gint i=0;
        //Add 1 for line count.
        gint len=(bezier_rows+1)*(bezier_columns+1);
        GArray *add_points=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), len);
        g_array_set_size(add_points, len);
        struct point3d *p1=&g_array_index(add_points, struct point3d, 0);
        for(i=0;i<len;i++)
          {
             (p1->z)=g_random_double_range(-30.0, 30.0);
             p1++;
          }
        set_bezier_sheet_points(SOLID_BEZIER_SHEET, bezier_rows, bezier_columns, add_points, -30.0, 30.0);
        g_array_free(add_points, TRUE);
      }
    if(initialize_id==WIRE_BEZIER_DISC)
      {
        initialize_wire_bezier_disc(16, 16, 150);
        set_cosine_wave(WIRE_BEZIER_DISC, 16, 16, 150.0, 40.0);
      }
    if(initialize_id==102)
      {
        initialize_wire_bezier_disc(bezier_rows, bezier_columns, 150);

        gint i=0;
        gint j=0;
        gint len=(bezier_rows+1)*(bezier_columns+1);
        GArray *add_points=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), len);
        g_array_set_size(add_points, len);
        struct point3d *p1=&g_array_index(add_points, struct point3d, 0);
        for(i=0;i<bezier_columns+1;i++)
          {
            for(j=0;j<bezier_rows+1;j++)
              {
                //Zero first 3 rings.
                if(j<3)
                  {
                    p1->z=0.0;
                  }
                else
                  {
                    p1->z=g_random_double_range(-30.0, 30.0);
                  }
                p1++;
              }
           }
        set_bezier_disc_points(WIRE_BEZIER_DISC, bezier_rows, bezier_columns, add_points, -30.0, 30.0);
        g_array_free(add_points, TRUE);
      }
    if(initialize_id==SOLID_BEZIER_DISC)
      {
        initialize_solid_bezier_disc(16, 16, 150);
        set_cosine_wave(SOLID_BEZIER_DISC, 16, 16, 150.0, 40.0);
      }
    if(initialize_id==103)
      {
        initialize_solid_bezier_disc(bezier_rows, bezier_columns, 150);

        gint i=0;
        gint len=(bezier_rows+1)*(bezier_columns+1);
        GArray *add_points=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), len);
        g_array_set_size(add_points, len);
        struct point3d *p1=&g_array_index(add_points, struct point3d, 0);
        for(i=0;i<len;i++)
          {
            //Zero the two center rings.
            if(i<(bezier_columns+1)*3)
              {
                p1->z=0.0;
              }
            else
              {
                p1->z=g_random_double_range(-30.0, 30.0);
              }
            p1++;
          }
        set_bezier_disc_points(SOLID_BEZIER_DISC, bezier_rows, bezier_columns, add_points, -30.0, 30.0);
        g_array_free(add_points, TRUE);
      }
    if(initialize_id==104)
      {
        initialize_wire_bezier_sphere(sphere_rows, sphere_columns, 150);

        gint i=0;         
        if(morph_points->len>0)
          {
            struct morph_sphere_point *p1=&g_array_index(morph_points, struct morph_sphere_point, 0);
            for(i=0;i<morph_points->len;i++)
              {
                if(i==0||p1->index!=0) set_bezier_sphere_single_point(WIRE_BEZIER_SPHERE, sphere_rows, sphere_columns, p1, -1);
                p1++;
              }
          }
        if(smooth_longitude) smooth_wire_bezier_sphere_longitude(WIRE_BEZIER_SPHERE, sphere_rows, sphere_columns, -1);
        if(smooth_latitude) smooth_wire_bezier_sphere_latitude(WIRE_BEZIER_SPHERE, sphere_rows, sphere_columns, -1); 
      }
    if(initialize_id==105)
      {
        gint i=0;
        initialize_solid_bezier_sphere(sphere_rows, sphere_columns, 150);
          
        if(morph_points->len>0)
          {
            struct morph_sphere_point *p1=&g_array_index(morph_points, struct morph_sphere_point, 0);
            for(i=0;i<morph_points->len;i++)
              {
                if(i==0||p1->index!=0) set_bezier_sphere_single_point(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, p1, -1);
                p1++;
              }
          }
        if(smooth_longitude) smooth_solid_bezier_sphere_longitude(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, -1);
        if(smooth_latitude) smooth_solid_bezier_sphere_latitude(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, -1); 
      }
    if(initialize_id==106)
      {
        gint i=0;
        initialize_solid_bezier_sphere(sphere_rows, sphere_columns, 150);
        
        if(morph_points->len>0)
          {
            struct morph_sphere_point *p1=&g_array_index(morph_points, struct morph_sphere_point, 0);
            for(i=0;i<morph_points->len;i++)
              {
                if(i==0||p1->index!=0) set_bezier_sphere_single_point(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, p1, -1);
                p1++;
              }
          }
        if(smooth_longitude) smooth_solid_bezier_sphere_longitude(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, -1);
        if(smooth_latitude) smooth_solid_bezier_sphere_latitude(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, -1); 
      }
    if(initialize_id==107)
      {
        initialize_solid_bezier_sphere(20, 20, 100);
      }
    if(initialize_id==108)
      {
        //Draw four spheres of the same size but morphed differently.
        gint i=0;
        initialize_solid_bezier_sphere(sphere_rows, sphere_columns, 70);
        initialize_solid_bezier_sphere(sphere_rows, sphere_columns, 70);
        initialize_solid_bezier_sphere(sphere_rows, sphere_columns, 70);
        initialize_solid_bezier_sphere(sphere_rows, sphere_columns, 70);
        //print_shapes_in_cache();
        //g_print("Shape Count %i\n", shapes_in_shape_cache());
        
        struct morph_sphere_point point;
        gint len=sphere_rows*2;
        gint ring=sphere_columns/2;
        gint ring_index=(ring-1)*len;
        
        //Morph first sphere.
        for(i=0;i<len;i++)
          {  
            point.index=ring_index+i;
            point.x=0;
            point.y=0;        
            point.z=50.0;
            point.ray=0;
            set_bezier_sphere_single_point(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, &point, 0);              
          } 
        if(smooth_longitude) smooth_solid_bezier_sphere_longitude(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, 0);
        if(smooth_latitude) smooth_solid_bezier_sphere_latitude(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, 0);         
        
        //Morph second sphere.
        ring_index=(ring-2)*len;
        for(i=0;i<len;i++)
          {  
            point.index=ring_index+i;
            point.x=0;
            point.y=0;        
            point.z=50.0;
            point.ray=0;
            set_bezier_sphere_single_point(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, &point, 1);              
          }
        if(smooth_longitude) smooth_solid_bezier_sphere_longitude(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, 1);
        if(smooth_latitude) smooth_solid_bezier_sphere_latitude(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, 1);       
        
        //Morph third sphere.
        ring_index=(ring-3)*len;
        for(i=0;i<len;i++)
          {  
            point.index=ring_index+i;
            point.x=0;
            point.y=0;        
            point.z=50.0;
            point.ray=0;
            set_bezier_sphere_single_point(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, &point, 2);              
          } 
        if(smooth_longitude) smooth_solid_bezier_sphere_longitude(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, 2);
        if(smooth_latitude) smooth_solid_bezier_sphere_latitude(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, 2);      
        
        //Morph fourth sphere.
        ring_index=(ring-4)*len;
        for(i=0;i<len;i++)
          {  
            point.index=ring_index+i;
            point.x=0;
            point.y=0;        
            point.z=50.0;
            point.ray=0;
            set_bezier_sphere_single_point(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, &point, 3);              
          }
        if(smooth_longitude) smooth_solid_bezier_sphere_longitude(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, 3);
        if(smooth_latitude) smooth_solid_bezier_sphere_latitude(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, 3);       
      }
    if(initialize_id==109||initialize_id==110)
      {
        initialize_solid_bezier_sphere(10, 10, 200);
        initialize_solid_bezier_sheet(5, 20, 500, 200);
      }
      
    return FALSE;
  }
static void set_rotation_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations)
  {
    if(value>360.0) ((gdouble*)(rotations[0]))[0]=360.0;
    else if(value<0.0) ((gdouble*)(rotations[0]))[0]=0.0;
    else ((gdouble*)(rotations[0]))[0]=value; 

    gtk_widget_queue_draw(GTK_WIDGET(rotations[1]));
  }
static void set_rotation_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations)
  {
    if(value>360.0) ((gdouble*)(rotations[0]))[1]=360.0;
    else if(value<0.0) ((gdouble*)(rotations[0]))[1]=0.0;
    else ((gdouble*)(rotations[0]))[1]=value; 

    gtk_widget_queue_draw(GTK_WIDGET(rotations[1]));
  }
static void set_rotation_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer *rotations)
  {
    if(value>360.0) ((gdouble*)(rotations[0]))[2]=360.0;
    else if(value<0.0) ((gdouble*)(rotations[0]))[2]=0.0;
    else ((gdouble*)(rotations[0]))[2]=value; 

    gtk_widget_queue_draw(GTK_WIDGET(rotations[1]));
  }
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_scale=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void translate_x_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    translate_x=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void translate_y_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    translate_y=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void layout_width_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    layout_width=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void layout_height_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    layout_height=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void bezier_row_spin_changed(GtkSpinButton *spin_button, gpointer *data)
  {
    bezier_rows=gtk_spin_button_get_value_as_int(spin_button);
    initialize_id=atoi(gtk_combo_box_get_active_id(GTK_COMBO_BOX(data[0])));
    g_idle_add_full(G_PRIORITY_HIGH_IDLE, initialize_bezier_surfaces, (gpointer)(&initialize_id), NULL);
    gtk_widget_queue_draw(GTK_WIDGET(data[1]));
  }
static void bezier_column_spin_changed(GtkSpinButton *spin_button, gpointer *data)
  {
    bezier_columns=gtk_spin_button_get_value_as_int(spin_button);
    initialize_id=atoi(gtk_combo_box_get_active_id(GTK_COMBO_BOX(data[0])));
    g_idle_add_full(G_PRIORITY_HIGH_IDLE, initialize_bezier_surfaces, (gpointer)(&initialize_id), NULL);
    gtk_widget_queue_draw(GTK_WIDGET(data[1]));
  }
static void sphere_row_spin_changed(GtkSpinButton *spin_button, gpointer *data)
  {
    gint i=0;
    sphere_rows=gtk_spin_button_get_value_as_int(spin_button);
    gint len=((sphere_rows*2)*(sphere_columns/2+1))-1;
    gint diff=len-morph_points->len;
    
    //Set top of spin button index and index ray values.
    g_signal_handler_block(data[0], index_id);
    g_signal_handler_block(data[6], index_ray_id);
    GtkAdjustment *adj=gtk_spin_button_get_adjustment(data[0]);
    gtk_adjustment_set_upper(adj, len);    
    adj=gtk_spin_button_get_adjustment(data[6]);
    gtk_adjustment_set_upper(adj, len);
    if(gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(data[0]))>len) gtk_spin_button_set_value(GTK_SPIN_BUTTON(data[0]), (gdouble)len);
    if(gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(data[6]))>len) gtk_spin_button_set_value(GTK_SPIN_BUTTON(data[6]), (gdouble)len); 
    g_signal_handler_unblock(data[0], index_id);
    g_signal_handler_unblock(data[6], index_ray_id);     
    
    if(morph_points->len>0)
      { 
        //Remove points if the new array is shorter.
        if(diff<0)
          {
            gint end=morph_points->len-1;
            for(i=0;i>diff+1;i--)
              {
                g_array_remove_index_fast(morph_points, end+i);
              }
           }
           
         //Add points if the new array is longer.
         if(diff>0)
           {
             struct morph_sphere_point point={.index=0, .x=0, .y=0, .z=0, .ray=0};
             for(i=0;i<diff+1;i++)
               {
                 g_array_append_val(morph_points, point);
               }
           }
       }
       
    initialize_id=atoi(gtk_combo_box_get_active_id(GTK_COMBO_BOX(data[4])));
    g_idle_add_full(G_PRIORITY_HIGH_IDLE, initialize_bezier_surfaces, (gpointer)(&initialize_id), NULL);
    gtk_widget_queue_draw(GTK_WIDGET(data[5]));
  }
static void sphere_column_spin_changed(GtkSpinButton *spin_button, gpointer *data)
  {
    gint i=0;
    sphere_columns=gtk_spin_button_get_value_as_int(spin_button);
    gint len=((sphere_rows*2)*(sphere_columns/2+1))-1;
    gint diff=len-(morph_points->len);
    
    //Set top of spin button index value.
    g_signal_handler_block(data[0], index_id);
    g_signal_handler_block(data[6], index_ray_id);
    GtkAdjustment *adj=gtk_spin_button_get_adjustment(data[0]);
    gtk_adjustment_set_upper(adj, len);
    adj=gtk_spin_button_get_adjustment(data[6]);
    gtk_adjustment_set_upper(adj, len);
    if(gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(data[0]))>len) gtk_spin_button_set_value(GTK_SPIN_BUTTON(data[0]), (gdouble)len);
    if(gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(data[6]))>len) gtk_spin_button_set_value(GTK_SPIN_BUTTON(data[6]), (gdouble)len); 
    g_signal_handler_unblock(data[0], index_id);
    g_signal_handler_unblock(data[6], index_ray_id); 
    
    if(morph_points->len>0)
      { 
        //Remove points if the new array is shorter.
        if(diff<0)
          {
            gint end=morph_points->len-1;
            for(i=0;i>diff+1;i--)
              {
                g_array_remove_index_fast(morph_points, end+i);
              }
           }
  
         //Add points if the new array is longer.
         if(diff>0)
           {
             struct morph_sphere_point point={.index=0, .x=0, .y=0, .z=0, .ray=0};
             for(i=0;i<diff+1;i++)
               {
                 g_array_append_val(morph_points, point);
               }
           }
      }
    
    initialize_id=atoi(gtk_combo_box_get_active_id(GTK_COMBO_BOX(data[4])));
    g_idle_add_full(G_PRIORITY_HIGH_IDLE, initialize_bezier_surfaces, (gpointer)(&initialize_id), NULL);
    gtk_widget_queue_draw(GTK_WIDGET(data[5]));
  }
//Don't need to recreate the sphere array here. Just add the single points.
static void sphere_point_spin_changed(GtkSpinButton *spin_button, gpointer *data)
  {
    struct morph_sphere_point point;
    
    point.index=gtk_spin_button_get_value_as_int(data[0]);
    point.x=gtk_spin_button_get_value_as_int(data[1]);
    point.y=gtk_spin_button_get_value_as_int(data[2]);
    point.z=gtk_spin_button_get_value_as_int(data[3]);
    point.ray=0;
    
    //Don't update on -1.
    if(point.index>-1)
      {
        struct morph_sphere_point *p1=&g_array_index(morph_points, struct morph_sphere_point, point.index);
        //Use += for the point to keep the saved .kvp file consistent.
        (p1->index)=point.index;
        (p1->x)+=point.x;
        (p1->y)+=point.y;
        (p1->z)+=point.z;
        (p1->ray)+=point.ray;
          
        if(initialize_id==104)
          {
            set_bezier_sphere_single_point(WIRE_BEZIER_SPHERE, sphere_rows, sphere_columns, &point, -1);
            if(smooth_longitude) smooth_wire_bezier_sphere_longitude(WIRE_BEZIER_SPHERE, sphere_rows, sphere_columns, -1);
            if(smooth_latitude) smooth_wire_bezier_sphere_latitude(WIRE_BEZIER_SPHERE, sphere_rows, sphere_columns, -1);
          }    
        if(initialize_id==105||initialize_id==106)
          {
            set_bezier_sphere_single_point(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, &point, -1);
            if(smooth_longitude) smooth_solid_bezier_sphere_longitude(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, -1);
            if(smooth_latitude) smooth_solid_bezier_sphere_latitude(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, -1);
          }
        gtk_widget_queue_draw(GTK_WIDGET(data[5]));     
      }
  }
static void sphere_point_spin_ray_changed(GtkSpinButton *spin_button, gpointer *data)
  {
    struct morph_sphere_point point;
    
    point.index=gtk_spin_button_get_value_as_int(data[6]);
    point.x=0;
    point.y=0;
    point.z=0;
    point.ray=gtk_spin_button_get_value_as_int(data[7]);
    
    //Don't update on -1.
    if(point.index>-1)
      {
        struct morph_sphere_point *p1=&g_array_index(morph_points, struct morph_sphere_point, point.index);            
        //Use += for the point to keep the saved .kvp file consistent.
        (p1->index)=point.index;
        (p1->x)+=point.x;
        (p1->y)+=point.y;
        (p1->z)+=point.z;
        (p1->ray)+=point.ray;
        
        if(initialize_id==104)
          {
            set_bezier_sphere_single_point(WIRE_BEZIER_SPHERE, sphere_rows, sphere_columns, &point, -1);
            if(smooth_longitude) smooth_solid_bezier_sphere_longitude(WIRE_BEZIER_SPHERE, sphere_rows, sphere_columns, -1);
            if(smooth_latitude) smooth_solid_bezier_sphere_latitude(WIRE_BEZIER_SPHERE, sphere_rows, sphere_columns, -1);
          }    
        if(initialize_id==105||initialize_id==106)
          {
            set_bezier_sphere_single_point(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, &point, -1);
            if(smooth_longitude) smooth_solid_bezier_sphere_longitude(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, -1);
            if(smooth_latitude) smooth_solid_bezier_sphere_latitude(SOLID_BEZIER_SPHERE, sphere_rows, sphere_columns, -1);
          }
        gtk_widget_queue_draw(GTK_WIDGET(data[5]));   
      }
  }
static void clear_morph_points(GtkWidget *button, gpointer data)
  {
    gint i=0;
    struct morph_sphere_point point={.index=0, .x=0, .y=0, .z=0, .ray=0};
    struct morph_sphere_point *p1=&g_array_index(morph_points, struct morph_sphere_point, 0);
    for(i=0;i<morph_points->len;i++)
      {
        p1->index=point.index;
        p1->x=point.x;
        p1->y=point.y;
        p1->z=point.z;
        p1->ray=point.ray;
        p1++;
      }
    
    g_idle_add_full(G_PRIORITY_HIGH_IDLE, initialize_bezier_surfaces, (gpointer)(&initialize_id), NULL);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_background_color(GtkWidget *button, gpointer *data)
  {
    GdkRGBA rgba;
    
    if(gdk_rgba_parse(&rgba, gtk_entry_get_text(GTK_ENTRY(data[1]))))
      {
        background_color[0]=rgba.red;
        background_color[1]=rgba.green;
        background_color[2]=rgba.blue;
        background_color[3]=rgba.alpha;
      }
    else
      {
        g_print("Background color string format error.\n");
      } 

    gtk_widget_queue_draw(GTK_WIDGET(data[0]));
  }
static gboolean animate_drawing(GtkWidget *drawing, GdkFrameClock *frame_clock, gpointer *rotations)
  {
    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    //Apply angles of rotation for animation.
    ((gdouble*)(rotations[0]))[0]+=0.5;
    ((gdouble*)(rotations[0]))[1]+=0.5;
    ((gdouble*)(rotations[0]))[2]+=0.5;

    gtk_widget_queue_draw(drawing);
    return G_SOURCE_CONTINUE;
  }
//The top drawing function.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer *rotations)
  {
    gdouble w1=layout_width/10.0;
    gdouble h1=layout_height/10.0;

    //Paint background.
    cairo_set_source_rgba(cr, background_color[0], background_color[1], background_color[2], background_color[3]);
    cairo_paint(cr);

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 1.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Layout bounding box.
    cairo_rectangle(cr, 0.0, 0.0, layout_width, layout_height);
    cairo_stroke(cr);

    GTimer *timer=g_timer_new();
    draw_surface(cr, layout_width, layout_height, rotations, TRUE, 4);
    if(function_time) g_print("Draw time %f\n", g_timer_elapsed(timer, NULL));
    g_timer_destroy(timer);

    return FALSE;
  }
static void draw_surface(cairo_t *cr, gdouble width, gdouble height, gpointer *rotations, gboolean coords, guint surface_type)
  {
    //Size he drawings at 50x50.
    gdouble w1=50.0;
    gdouble h1=50.0;
    gdouble yaw=((gdouble*)(rotations[0]))[0]*G_PI/180.0;
    gdouble roll=((gdouble*)(rotations[0]))[1]*G_PI/180.0;
    gdouble pitch=((gdouble*)(rotations[0]))[2]*G_PI/180.0;
    //Some points to revolve.
    gdouble x=0.0;
    gdouble y=0.0;
    gdouble z=0.0;
    gdouble points_scale=0.0;

    //Scale and translate drawing if the surface is in points or pixels.
    if(surface_type<3) points_scale=72.0/ppi;
    else points_scale=1.0;

    if(!coords)
      {
        //Paint background.
        cairo_set_source_rgba(cr, background_color[0], background_color[1], background_color[2], background_color[3]);
        cairo_paint(cr);
      }

    /*
      Draw in the center. The shapes are rotated first in 3d then translated and scaled in
      cairo 2d. You can also reset the rotation matrix with set_rotation3d_identity().
    */
    set_rotation3d(yaw, roll, pitch);
    cairo_translate(cr, layout_width*points_scale/2.0, layout_height*points_scale/2.0);
    cairo_translate(cr, translate_x, translate_y);
    if(surface_type<3) cairo_scale(cr, slider_scale*points_scale+0.01, slider_scale*points_scale+0.01);
    else cairo_scale(cr, slider_scale+0.01, slider_scale+0.01);
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND); 
    cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT);      
 
    if(drawing_id==0)
      {
        cairo_set_line_width(cr, 5.0); 
        draw_spring(cr, 2.0*w1, 2.0*h1);
      }
    else if(drawing_id==1)
      {
        cairo_set_line_width(cr, 3.0); 
        draw_solid_cylinder(cr, 2.0*w1, 2.0*h1, 16);
      } 
    else if(drawing_id==2)
      {
        cairo_set_line_width(cr, 1.0); 
        draw_cube(cr, 2.0*w1, 2.0*h1);
      } 
    else if(drawing_id==3)
      {
        cairo_set_line_width(cr, 1.0); 
        set_transparency(TRUE);
        draw_transparent_cube(cr, 2.0*w1, 2.0*h1);
        set_transparency(FALSE);
      } 
    else if(drawing_id==4)
      {
        cairo_set_line_width(cr, 2.0); 
        draw_checkerboard(cr, 3.0*w1, 3.0*h1, 8, 8);
      }
    else if(drawing_id==5)
      {
        draw_ball(cr, 3.0*w1, 3.0*h1, 30, 30);
      }
    else if(drawing_id==6)
      {
        cairo_set_line_width(cr, 2.0); 
        draw_wire_ball(cr, 3.0*w1, 3.0*h1, 30, 30);
      }
    else if(drawing_id==7)
      {
        cairo_set_line_width(cr, 2.0); 
        draw_wire_ball2(cr, 3.0*w1, 3.0*h1, 30, 30);
      }
    else if(drawing_id==8)
      {
        //See if it draws faster with anti aliasing turned off.
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        cairo_set_line_width(cr, 3.0); 
        draw_wire_ball2(cr, 3.0*w1, 3.0*h1, 30, 30);
      }
    else if(drawing_id==9)
      {
        cairo_set_line_width(cr, 2.0); 
        draw_wire_sphere(cr, 3.0*w1, 3.0*h1, 20, 24);
      }
    else if(drawing_id==10)
      {
        //cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        draw_solid_sphere(cr, 3.0*w1, 3.0*h1, 16, 16);
      }
    else if(drawing_id==11)
      {
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        cairo_set_line_width(cr, 4.0); 
        draw_wire_funnel(cr, 3.0*w1, 3.0*h1, 10, 12);
      }
    else if(drawing_id==12)
      {
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        draw_solid_funnel(cr, 3.0*w1, 3.0*h1, 10, 12);
      }
    else if(drawing_id==13)
      {
        cairo_set_line_width(cr, 1.0); 
        draw_gem(cr, 3.0*w1, 3.0*h1);
      }
    else if(drawing_id==14)
      {
        //cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        cairo_set_line_width(cr, 2.0); 
        draw_wire_torus(cr, 3.0*w1, 3.0*h1, 12, 16);
      }
    else if(drawing_id==15)
      {
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        draw_solid_torus(cr, 3.0*w1, 3.0*h1, 12, 16);
      }
    else if(drawing_id==16)
      {
        //The 4-torus drawing.
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        cairo_scale(cr, 0.5, 0.5);
        cairo_translate(cr, -4.0*w1, -4.0*h1); 
        draw_solid_torus(cr, 3.0*w1, 3.0*h1, 12, 16);

        cairo_translate(cr, 8.0*w1, 0.0); 
        draw_solid_torus(cr, 3.0*w1, 3.0*h1, 12, 16);

        cairo_translate(cr, -8.0*w1, 8.0*h1); 
        draw_solid_torus(cr, 3.0*w1, 3.0*h1, 12, 16);

        cairo_translate(cr, 8.0*w1, 0.0); 
        draw_solid_torus(cr, 3.0*w1, 3.0*h1, 12, 16);
      }
    else if(drawing_id==17)
      {
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        cairo_set_line_width(cr, 2.0); 
        draw_wire_twist(cr, w1, h1, 17);
      }
    else if(drawing_id==18)
      {
        draw_solid_twist(cr, w1, h1, 17);
      }
    else if(drawing_id==19)
      { 
        draw_pyramid(cr, 2.0*w1, 2.0*h1);
      }
    else if(drawing_id==20)
      {
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        cairo_set_line_width(cr, 1.0); 
        draw_cone(cr, 2.0*w1, 2.0*h1, 8, 16);
      }
    else if(drawing_id==21)
      {
        cairo_set_line_width(cr, 1.0); 
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        draw_cone_shade(cr, 2.0*w1, 2.0*h1, 8, 16);
      }
    else if(drawing_id==22)
      {
        cairo_set_line_width(cr, 4.0); 
        draw_text_ring(cr, 3.0*w1, 3.0*h1, 16);
      }
    else if(drawing_id==23)
      {
        cairo_scale(cr, 0.5, 0.5);
        cairo_translate(cr, -4.0*w1, -4.0*h1); 
        draw_ball(cr, 3.0*w1, 3.0*h1, 20, 20);

        cairo_set_line_width(cr, 1.0); 
        cairo_translate(cr, 8.0*w1, 0.0); 
        draw_gem(cr, 3.0*w1, 3.0*h1);

        cairo_translate(cr, -8.0*w1, 8.0*h1); 
        draw_solid_twist(cr, w1, h1, 17);

        cairo_translate(cr, 8.0*w1, 0.0);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); 
        draw_cone(cr, 2.0*w1, 2.0*h1, 8, 16);
      }
    else if(drawing_id==24)
      {
        //Rotate each drawing individually.
        set_rotation3d(yaw, roll, pitch);
        cairo_scale(cr, 0.5, 0.5);
        cairo_translate(cr, -4.0*w1, -4.0*h1);  
        draw_ball(cr, 3.0*w1, 3.0*h1, 20, 20);

        set_rotation3d(yaw+G_PI/4.0, roll, pitch);
        cairo_set_line_width(cr, 1.0); 
        cairo_translate(cr, 8.0*w1, 0.0); 
        draw_gem(cr, 3.0*w1, 3.0*h1);

        set_rotation3d(yaw, roll+G_PI/4.0, pitch);
        cairo_translate(cr, -8.0*w1, 8.0*h1); 
        draw_solid_twist(cr, w1, h1, 17);

        set_rotation3d(yaw, roll, pitch*G_PI/4.0);
        cairo_translate(cr, 8.0*w1, 0.0);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); 
        draw_cone(cr, 2.0*w1, 2.0*h1, 8, 16);
      }
    else if(drawing_id==25)
      {
        //Revolve and rotate a couple of balls.
        gdouble qrs[9];
        quaternion_rotation(yaw, roll, pitch, qrs);
        //Set revolve initial location to x=1,y=0,z=0.
        set_revolve_standard_point(1.0, 0.0, 0.0);
        revolve_standard_point(qrs);
        get_revolved_point(&x, &y, &z);
        cairo_scale(cr, 0.5, 0.5);
        cairo_translate(cr, -6.0*w1+x*12.0*w1, y*12.0*h1);  
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); 
        draw_ball(cr, 3.0*w1, 3.0*h1, 20, 20);

        cairo_translate(cr, 6.0*w1-x*12.0*w1, y*12.0*h1);  
        draw_ball(cr, 3.0*w1, 3.0*h1, 20, 20);
      }
    else if(drawing_id==26)
      {
        //Planets. Draw several different sizes of the same shape.
        cairo_scale(cr, 0.5, 0.5);
        gdouble qrs2[9];
        gdouble qrs3[9];
        //Set revolve initial location to x=1,y=0,z=0.
        set_revolve_standard_point(1.0, 0.0, 0.0);

        cairo_save(cr);
        cairo_scale(cr, 0.25, 0.25);
        quaternion_rotation(yaw+G_PI/2.0, roll-G_PI, pitch, qrs2);
        revolve_standard_point(qrs2); 
        get_revolved_point(&x, &y, &z);
        cairo_translate(cr, 32.0*x*w1, 32.0*y*h1);       
        set_rotation3d(yaw+G_PI/2.0, roll-G_PI, pitch);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);  
        draw_ball_mesh(cr, 3.0*w1, 3.0*h1, 16, 16);
        cairo_restore(cr);

        cairo_save(cr);
        cairo_scale(cr, 0.5, 0.5);
        quaternion_rotation(yaw, roll, pitch-G_PI, qrs3);
        revolve_standard_point(qrs3);
        get_revolved_point(&x, &y, &z);
        cairo_translate(cr, 16.0*x*w1, 16.0*y*h1);  
        set_rotation3d(yaw, roll, pitch-G_PI);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);  
        draw_ball_mesh(cr, 3.0*w1, 3.0*h1, 24, 24);
        cairo_restore(cr);

        //Center ball.
        set_rotation3d(yaw, roll, pitch);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);  
        draw_ball_mesh(cr, 3.0*w1, 3.0*h1, 30, 30);

        //Possibly draw twice if revolving planet is in the front.
        revolve_standard_point(qrs2); 
        get_revolved_point(&x, &y, &z);
        if(z>=0.0)
          {    
            cairo_save(cr);
            cairo_scale(cr, 0.25, 0.25);
            cairo_translate(cr, 32.0*x*w1, 32.0*y*h1); 
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);  
            draw_ball_mesh(cr, 3.0*w1, 3.0*h1, 16, 16);
            cairo_restore(cr);
          }

        revolve_standard_point(qrs3);
        get_revolved_point(&x, &y, &z); 
        if(z>=0.0)
          {    
            cairo_save(cr);
            cairo_scale(cr, 0.5, 0.5);
            cairo_translate(cr, 16.0*x*w1, 16.0*y*h1);       
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);  
            draw_ball_mesh(cr, 3.0*w1, 3.0*h1, 24, 24);
            cairo_restore(cr);
          }
             
      }
    else if(drawing_id==27)
      {
        //Ball and cone sychronized.
        cairo_scale(cr, 0.5, 0.5);
        gdouble qrs1[9];
        //Set revolve initial location to x=0,y=0,z=1.
        set_revolve_standard_point(0.0, 0.0, 1.0);
        quaternion_rotation(yaw, roll, pitch, qrs1);
        revolve_standard_point(qrs1);
        get_revolved_point(&x, &y, &z);

        //Draw cone twice for front and back of ball. 
        if(z<=0.0)
          {       
            cairo_save(cr);
            cairo_translate(cr, x*w1*4.0, y*h1*4.0);
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); 
            draw_cone(cr, 2.0*w1, 2.0*h1, 8, 16);
            cairo_restore(cr);
          }
 
        draw_ball(cr, 3.0*w1, 3.0*h1, 20, 20);
        //Draw cone second time.
        if(z>=0.0)
          {
            cairo_translate(cr, x*w1*4.0, y*h1*4.0);
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); 
            draw_cone(cr, 2.0*w1, 2.0*h1, 8, 16);
          }
      }
    else if(drawing_id==28)
      {
        cairo_set_line_width(cr, 3.0); 
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        draw_wire_bezier_sheet(cr, 17, 17);
      }
    else if(drawing_id==29)
      {
        //Random bezier sheet.
        cairo_set_line_width(cr, 3.0); 
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        draw_wire_bezier_sheet(cr, bezier_rows, bezier_columns);
      }
    else if(drawing_id==30)
      {
        draw_solid_bezier_sheet(cr, 17, 17);
      }
     else if(drawing_id==31)
      {
        cairo_set_line_width(cr, 1.0);
        draw_solid_bezier_sheet_mesh(cr, 17, 17);
      }
    else if(drawing_id==32)
      {
        //random bezier sheet.
        cairo_set_line_width(cr, 1.0);
        draw_solid_bezier_sheet_mesh(cr, bezier_rows, bezier_columns);
      }
    else if(drawing_id==33)
      {
        cairo_set_line_width(cr, 3.0);
        draw_wire_bezier_disc(cr, 16, 16);
      }
    else if(drawing_id==34)
      {
        //Random.
        cairo_set_line_width(cr, 3.0);
        draw_wire_bezier_disc(cr, bezier_rows, bezier_columns);
      }
    else if(drawing_id==35)
      {
        cairo_set_line_width(cr, 1.0);
        draw_solid_bezier_disc(cr, 16, 16);
      }
    else if(drawing_id==36)
      {
        //Test without mesh lines or change line width for mesh lines.
        cairo_set_line_width(cr, 0.0); 
        draw_solid_bezier_disc_mesh(cr, 16, 16);
      }
    else if(drawing_id==37)
      {
        //Random.
        cairo_set_line_width(cr, 2.0); 
        draw_solid_bezier_disc_mesh(cr, bezier_rows, bezier_columns);
      }
    else if(drawing_id==38)
      {
        cairo_set_line_width(cr, 2.0); 
        draw_wire_bezier_sphere(cr, sphere_rows, sphere_columns);
      }
    else if(drawing_id==39)
      {
        cairo_set_line_width(cr, 2.0); 
        //cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); 
        draw_solid_bezier_sphere(cr, sphere_rows, sphere_columns, compare_quads, alpha);
      }
    else if(drawing_id==40)
      {
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);  
        draw_solid_bezier_sphere_mesh(cr, sphere_rows, sphere_columns, compare_quads, alpha);
      }
    else if(drawing_id==41)
      {
        cairo_set_line_width(cr, 2.0);  
        draw_solid_bezier_sphere_explode(cr, 20, 20);
      }
    else if(drawing_id==42)
      {
        gint i=0;
        cairo_set_line_width(cr, 2.0); 
        cairo_translate(cr, -2.0*w1, -2.0*h1);
        for(i=0;i<4;i++)
          {
            if(i==1) cairo_translate(cr, 4.0*w1, 0.0); 
            if(i==2) cairo_translate(cr, -4.0*w1, 4.0*h1);
            if(i==3) cairo_translate(cr, 4.0*w1, 0.0); 
            draw_solid_bezier_sphere_index(cr, w1, h1, i, compare_quads, alpha);
          }
      }
    else if(drawing_id==43)
      { 
        cairo_set_line_width(cr, 2.0);  
        draw_wrap_sphere(cr, 10, 10);
      }
    else if(drawing_id==44)
      {   
        //set_rotation3d(0.0, 0.0, 0.0);
        draw_wrap_sphere_image(cr, 10, 10, pictures);
      }
    else if(drawing_id==45)
      { 
        draw_fish(cr, w1, h1);
      }
    else
      {
        draw_turtle(cr, w1, h1);
      }     
  }
static gchar* save_file(gpointer *rotations, gint surface_type)
  {
    int result=0;

    GtkWidget *dialog=gtk_file_chooser_dialog_new("Save File", GTK_WINDOW(rotations[2]), GTK_FILE_CHOOSER_ACTION_SAVE, "Cancel", GTK_RESPONSE_CANCEL, "Save", GTK_RESPONSE_ACCEPT, NULL);
    GtkFileFilter *filter=gtk_file_filter_new();    
    gtk_file_chooser_set_filter(GTK_FILE_CHOOSER(dialog), filter);
    
    if(surface_type==0)
      {
        gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".svg");
        gtk_file_filter_add_pattern(filter, "*.svg");
      }
    else if(surface_type==1)
      {
        gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".pdf");
        gtk_file_filter_add_pattern(filter, "*.pdf");
      }
    else if(surface_type==2)
      {
        gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".ps");
        gtk_file_filter_add_pattern(filter, "*.ps");
      }
    else if(surface_type==3)
      {
        gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".png");
        gtk_file_filter_add_pattern(filter, "*.png");
      } 
    else
      { 
        gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".kvp");
        gtk_file_filter_add_pattern(filter, "*.kvp"); 
      }

    result=gtk_dialog_run(GTK_DIALOG(dialog));
    gchar *file_name=NULL;
    if(result==GTK_RESPONSE_ACCEPT)
      {

        file_name=gtk_file_chooser_get_current_name(GTK_FILE_CHOOSER(dialog));
      }

    gtk_widget_destroy(dialog);

    return file_name;
  }
static void draw_svg(GtkWidget *widget, gpointer *rotations)
  {
    gchar *file_name=save_file(rotations, 0);
    
    //Scale factor to convert to points.
    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_svg_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, layout_width, layout_height, rotations, FALSE, 0);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name);
  }
static void draw_pdf(GtkWidget *widget, gpointer *rotations)
  {
    gchar *file_name=save_file(rotations, 1);
    
    //Scale factor to convert to points.
    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_pdf_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, layout_width, layout_height, rotations, FALSE, 1);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name);
  }
static void draw_ps(GtkWidget *widget, gpointer *rotations)
  {
    gchar *file_name=save_file(rotations, 2);
    
    //Scale factor to convert to points.
    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_ps_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, layout_width, layout_height, rotations, FALSE, 2);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name);
  }
static void draw_png(GtkWidget *widget, gpointer *rotations)
  {
    gchar *file_name=save_file(rotations, 3);

    if(file_name!=NULL)
      {
        cairo_surface_t *surface=cairo_image_surface_create(CAIRO_FORMAT_ARGB32, layout_width, layout_height);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, layout_width, layout_height, rotations, FALSE, 3);
        cairo_surface_write_to_png(surface, file_name);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name);    
  }
static void save_wire_sphere_kvp(GtkWidget *widget, gpointer *rotations)
  {
    if(initialize_id==104)
      {
        save_file_chooser_kvp(rotations);
      }
    else g_print("Select the Wire Morph Bezier Sphere drawing before saving the .kvp file.\n"); 
  }
static void save_solid_sphere_kvp(GtkWidget *widget, gpointer *rotations) 
  {
    if(initialize_id==105)
      {
        save_file_chooser_kvp(rotations);
      }
    else g_print("Select the Solid Morph Bezier Sphere drawing before saving the .kvp file.\n");   
  }
static void save_solid_sphere_mesh_kvp(GtkWidget *widget, gpointer *rotations) 
  {
    if(initialize_id==106)
      {
        save_file_chooser_kvp(rotations);
      }
    else g_print("Select the Solid Morph Bezier Sphere Mesh drawing before saving the .kvp file.\n");   
  }
static void save_file_chooser_kvp(gpointer *rotations)
  {
    gchar *file_name=save_file(rotations, 4);
    if(file_name!=NULL)
      {
        if(initialize_id==104) save_wire_morph_sphere_to_file(file_name, morph_points, 150, sphere_rows, sphere_columns);
        if(initialize_id==105) save_solid_morph_sphere_to_file(file_name, morph_points, 150, sphere_rows, sphere_columns);
        if(initialize_id==106) save_solid_morph_sphere_to_file(file_name, morph_points, 150, sphere_rows, sphere_columns);
      }
    g_free(file_name); 
  }
static void open_wire_sphere_kvp(GtkWidget *button, gpointer *data)
  {
    if(initialize_id==104)
      {
        open_file_chooser_kvp(data);
      }
    else g_print("Select the Wire Morph Bezier Sphere drawing before opening the .kvp file.\n"); 
  }
static void open_solid_sphere_kvp(GtkWidget *button, gpointer *data)
  { 
    if(initialize_id==105)
      {
        open_file_chooser_kvp(data);
      }
    else g_print("Select the Solid Morph Bezier Sphere drawing before opening the .kvp file.\n");  
  }
static void open_solid_sphere_mesh_kvp(GtkWidget *button, gpointer *data)
  { 
    if(initialize_id==106)
      {
        open_file_chooser_kvp(data);
      }
    else g_print("Select the Solid Morph Bezier Sphere Mesh drawing before opening the .kvp file.\n");  
  }
static void open_file_chooser_kvp(gpointer *data)
  {
    gint result=0;
    GtkWidget *dialog=gtk_file_chooser_dialog_new("Open File", GTK_WINDOW(data[3]), GTK_FILE_CHOOSER_ACTION_OPEN, "Cancel", GTK_RESPONSE_CANCEL, "Open", GTK_RESPONSE_ACCEPT, NULL);
    GtkFileFilter *filter=gtk_file_filter_new();
    gtk_file_chooser_set_filter(GTK_FILE_CHOOSER(dialog), filter);
    gtk_file_filter_add_pattern(filter, "*.kvp"); 

    result=gtk_dialog_run(GTK_DIALOG(dialog));
    gchar *file_name=NULL;
    if(result==GTK_RESPONSE_ACCEPT)
      {
        file_name=gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
        if(initialize_id==104) get_wire_morph_sphere_kvp(file_name, data);
        if(initialize_id==105) get_solid_morph_sphere_kvp(file_name, data);
        if(initialize_id==106) get_solid_morph_sphere_kvp(file_name, data);
        g_free(file_name);
      }

    gtk_widget_destroy(dialog);
  }
static void get_wire_morph_sphere_kvp(gchar *file_name, gpointer *data)
  {    
    gint i=0;
    gint cache_id=0;
    gint radius=0;
    gint rings=0;
    gint points_per_ring=0;
    GArray *difference=get_wire_morph_sphere_from_file(file_name, &cache_id, &radius, &rings, &points_per_ring);
    
    if(difference!=NULL) 
     {          
        //Clear morph points array/
        for(i=morph_points->len;i>0;i--) g_array_remove_index_fast(morph_points, i-1);
   
        //Reload morph_points array.
        struct morph_sphere_point *p1=&g_array_index(difference, struct morph_sphere_point, 0);
        for(i=0;i<difference->len;i++)
          {
            //Append points with a value.
            g_array_append_val(morph_points, *p1);
            p1++;
          }
          
        sphere_rows=rings;
        sphere_columns=points_per_ring;
        gtk_spin_button_set_value(GTK_SPIN_BUTTON(data[0]), (gdouble)sphere_rows);
        gtk_spin_button_set_value(GTK_SPIN_BUTTON(data[1]), (gdouble)sphere_columns);
        //g_print("radius %i, rings %i, points_per_ring %i\n", radius, rings, points_per_ring);
    
        g_array_free(difference, TRUE); 
        g_idle_add_full(G_PRIORITY_HIGH_IDLE, initialize_bezier_surfaces, (gpointer)(&initialize_id), NULL); 
        gtk_widget_queue_draw(GTK_WIDGET(data[2]));         
      }
    else g_warning("Null pointer returned from get_wire_morph_sphere_from_file().\n");     
  }
static void get_solid_morph_sphere_kvp(gchar *file_name, gpointer *data)
  {
    gint i=0;
    gint cache_id=0;
    gint radius=0;
    gint rings=0;
    gint points_per_ring=0;
    GArray *difference=get_solid_morph_sphere_from_file(file_name, &cache_id, &radius, &rings, &points_per_ring);
    
    if(difference!=NULL)
      {
        //Clear morph points array/
        for(i=morph_points->len;i>0;i--) g_array_remove_index_fast(morph_points, i-1);
    
        //Reload morph_points array.
        struct morph_sphere_point *p1=&g_array_index(difference, struct morph_sphere_point, 0);
        for(i=0;i<difference->len;i++)
          {
            g_array_append_val(morph_points, *p1);
            p1++;
          }
      
        sphere_rows=rings;
        sphere_columns=points_per_ring;
        gtk_spin_button_set_value(GTK_SPIN_BUTTON(data[0]), (gdouble)sphere_rows);
        gtk_spin_button_set_value(GTK_SPIN_BUTTON(data[1]), (gdouble)sphere_columns);
        //g_print("radius %i, rings %i, points_per_ring %i\n", radius, rings, points_per_ring);
    
        g_array_free(difference, TRUE);
        g_idle_add_full(G_PRIORITY_HIGH_IDLE, initialize_bezier_surfaces, (gpointer)(&initialize_id), NULL);
        gtk_widget_queue_draw(GTK_WIDGET(data[2]));
      }
    else g_warning("Null pointer returned from get_solid_morph_sphere_from_file().\n");
  }
static void about_dialog(GtkWidget *widget, gpointer data)
  {
    GtkWidget *dialog=gtk_about_dialog_new();
    gtk_widget_set_name(dialog, "about_dialog");
    gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(data));
    gtk_about_dialog_set_logo(GTK_ABOUT_DIALOG(dialog), NULL);
    gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(dialog), "Cairo3d Shapes");
    gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog), "Test Version 1.0");
    gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(dialog), "3d shapes with cairo.");
    gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(dialog), "(C) 2019 C. Eric Cashon");

    gtk_widget_show_all(dialog);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
  }
static GdkPixbuf* draw_icon()
  {
    //Create a surface to draw a 256x256 icon. 
    cairo_surface_t *surface=cairo_image_surface_create(CAIRO_FORMAT_ARGB32, 256, 256);
    cairo_t *cr=cairo_create(surface);
    
    //Paint the background.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.0);
    cairo_paint(cr);

    cairo_translate(cr, 128.0, 128.0);
    draw_solid_sphere(cr, 128.0, 128.0, 16, 16);

    GdkPixbuf *icon=gdk_pixbuf_get_from_surface(surface, 0, 0, 256, 256);

    cairo_destroy(cr);
    cairo_surface_destroy(surface); 
    return icon;
  }  
static GPtrArray* create_pictures_array()
  {
    gint i=0;
    gint j=0;
    pictures=g_ptr_array_new();
    cairo_surface_t *picture=cairo_image_surface_create(CAIRO_FORMAT_ARGB32, 500, 200);
    cairo_pattern_t *pattern=NULL;
    cairo_t *cr=cairo_create(picture);
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    cairo_paint(cr);
    cairo_set_line_width(cr, 3.0);
    
    cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
    for(i=0;i<5;i++)
      {
        cairo_move_to(cr, 0.0, 40.0*(gdouble)i+20.0);
        cairo_line_to(cr, 500.0, 40.0*(gdouble)i+20.0);
        cairo_stroke(cr);
      }
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    for(i=0;i<20;i++)
      {
        cairo_move_to(cr, 25.0*(gdouble)i+12.5, 0.0);
        cairo_line_to(cr, 25.0*(gdouble)i+12.5, 200.0);
        cairo_stroke(cr);
      }
     
    for(i=0;i<5;i++)
      {
        for(j=0;j<20;j++)
          {
            cairo_surface_t *rect=cairo_surface_create_for_rectangle(picture, 25.0*(gdouble)(j), 40.0*(gdouble)(i), 25.0, 40.0);
            pattern=cairo_pattern_create_for_surface(rect);
            cairo_pattern_set_extend(pattern, CAIRO_EXTEND_PAD);
            if(cairo_pattern_status(pattern)!=CAIRO_STATUS_SUCCESS) 
              {
                g_print("%s\n", cairo_status_to_string(cairo_pattern_status(pattern)));
              }
            g_ptr_array_add(pictures, (gpointer)pattern);
            cairo_surface_destroy(rect);
          }
      } 
      
    cairo_destroy(cr);
    cairo_surface_destroy(picture); 
    return pictures;
  }
static void free_pictures(GPtrArray *pictures)
  {
    gint i=0;
    for(i=0;i<pictures->len;i++)
      {
        cairo_pattern_destroy((cairo_pattern_t*)g_ptr_array_index(pictures, i));
      }
    g_ptr_array_free(pictures, TRUE);
  } 
  
  
  
  
  
