/* 
    See histogram_chart3d_main.c for the test program.
    
    gcc -Wall -c histogram_chart3d.c -o histogram_chart3d.o `pkg-config --cflags --libs glib-2.0` `pkg-config --cflags --libs cairo` -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include"histogram_chart3d.h"

//Gradient color stops for base, middle and top.
static gdouble b_c[4]={1.0, 0.0, 1.0, 1.0};
static gdouble m_c[4]={0.0, 1.0, 1.0, 1.0};
static gdouble t_c[4]={1.0, 1.0, 0.0, 1.0};

static void initialize_base(struct histogram_chart *hc, guint rows, guint columns, gdouble outside_square);
//Draw the shapes.
static void chart_data_draw_cubes(struct chart_data *cd, cairo_t *cr, gdouble chart_z);
static void chart_data_draw_cubes_grad(struct chart_data *cd, cairo_t *cr, gdouble chart_z);
static void chart_data_draw_cubes_grad_mesh(struct chart_data *cd, cairo_t *cr, gdouble chart_z);
static void chart_data_draw_cubes_grad2(struct chart_data *cd, cairo_t *cr, gdouble chart_z);
static void chart_data_draw_cubes_mesh_grad2(struct chart_data *cd, cairo_t *cr, gdouble chart_z);
static void chart_data_draw_lines(struct chart_data *cd, cairo_t *cr);
static void chart_data_draw_smooth(struct chart_data *cd, cairo_t *cr);
static void chart_data_draw_points(struct chart_data *cd, cairo_t *cr);
static void chart_data_draw_cylinders(struct chart_data_aligned *cd, cairo_t *cr, gdouble chart_z);
static void chart_data_draw_cylinders_grad(struct chart_data_aligned *cd, cairo_t *cr, gdouble chart_z);
static void chart_data_draw_cylinders_grad_mesh(struct chart_data_aligned *cd, cairo_t *cr, gdouble chart_z);
static void chart_data_reset_cubes(struct chart_data *cd);
static void chart_data_reset(struct chart_data *cd);
static void chart_data_reset_aligned(struct chart_data_aligned *cd);
static void chart_data_rotate_cubes(struct chart_data *cd, gdouble qrs[16]);
static void chart_data_rotate(struct chart_data *cd, gdouble qrs[16]);
static void chart_data_rotate_aligned(struct chart_data_aligned *cd, gdouble qrs[16]);
static int compare_cubes(const void *a, const void *b);
//Get bezier control points for curve coordinates.
static GArray* control_points_from_coords_3d(const GArray *data_points3d);
//Generate bezier points and coordinates for a ring.
static void initialize_bezier_ring(struct histogram_chart *hc, struct histo_arc **ring, struct histo_arc **ring_t, gint sections, gdouble radius, gdouble max_z);
static int compare_cylinders16(const void *a, const void *b);
static int compare_cylinders24(const void *a, const void *b);
//Get colors for gradient.
static void get_gradient_color_bt(gdouble percent, gdouble *r, gdouble *g, gdouble *b);
static void get_gradient_color_bm(gdouble percent, gdouble *r, gdouble *g, gdouble *b);
static void get_gradient_color_mt(gdouble percent, gdouble *r, gdouble *g, gdouble *b);
//Get chart data for chart data groups from a keyfile.
static struct chart_data* get_key_file_group(GKeyFile *key_file, const gchar *group, gdouble line_color_array[4], gdouble point_color_array[4], gdouble *line_width_value);
//For the linear gradient control line.
static void intersection(double A1, double A2, double B1, double B2, double C1, double C2, double D1, double D2, double *out_x, double *out_y);

//The chart functions.
struct histogram_chart* histogram_chart_new(guint rows, guint columns, gdouble outside_square)
  {
    //Check the variables.
    if(rows<1)
      {
        rows=1;
        g_warning("Chart rows set to 1.\n");
      }
    else if(columns<1)
      {
        columns=1;
        g_warning("Chart columns set to 1.\n");
      }
    else if(outside_square<10)
      {
        outside_square=10.0;
        g_warning("Outside square set to 10.0.\n");
      }

    struct histogram_chart *hc=g_new(struct histogram_chart, 1);

    hc->rows=rows;
    hc->columns=columns;
    hc->outside_square=outside_square;
    initialize_base(hc, rows, columns, outside_square);
    hc->back_height=200.0;
    hc->back_rows=10;
    hc->back_color[0]=0.0;
    hc->back_color[1]=0.0;
    hc->back_color[2]=1.0;
    hc->back_color[3]=1.0;
    //Initialize axis reference vectors.
    hc->rx[0]=1.0;
    hc->rx[1]=0.0;
    hc->rx[2]=0.0;
    hc->rx_t[0]=1.0;
    hc->rx_t[1]=0.0;
    hc->rx_t[2]=0.0;
    hc->ry[0]=0.0;
    hc->ry[1]=1.0;
    hc->ry[2]=0.0;
    hc->ry_t[0]=0.0;
    hc->ry_t[1]=1.0;
    hc->ry_t[2]=0.0;
    hc->rz[0]=0.0;
    hc->rz[1]=0.0;
    hc->rz[2]=1.0;
    hc->rz_t[0]=0.0;
    hc->rz_t[1]=0.0;
    hc->rz_t[2]=1.0;
    //Initialize the 3 top corners.
    hc->c1[0]=-(columns*outside_square)/2.0;
    hc->c1[1]=(rows*outside_square)/2.0;
    hc->c1[2]=(hc->back_height);
    hc->c1_t[0]=0.0;
    hc->c1_t[1]=0.0;
    hc->c1_t[2]=0.0;
    hc->c2[0]=-(columns*outside_square)/2.0;
    hc->c2[1]=-(rows*outside_square)/2.0;
    hc->c2[2]=(hc->back_height);
    hc->c2_t[0]=0.0;
    hc->c2_t[1]=1.0;
    hc->c2_t[2]=0.0;
    hc->c3[0]=(columns*outside_square)/2.0;
    hc->c3[1]=-(rows*outside_square)/2.0;
    hc->c3[2]=(hc->back_height);
    hc->c3_t[0]=0.0;
    hc->c3_t[1]=0.0;
    hc->c3_t[2]=1.0;
    hc->labels_xr=g_ptr_array_new_with_free_func(g_free);
    hc->labels_yr=g_ptr_array_new_with_free_func(g_free);
    hc->labels_xl=g_ptr_array_new_with_free_func(g_free);
    hc->labels_yl=g_ptr_array_new_with_free_func(g_free);
    hc->labels_zr=g_ptr_array_new_with_free_func(g_free);

    return hc;
  }
//initialize base is static or internal.
static void initialize_base(struct histogram_chart *hc, guint rows, guint columns, gdouble outside_square)
  {
    gint i=0;
    gint j=0;
    gint total=rows*columns;
    //Center points.
    gdouble move_x=-(columns*outside_square)/2.0;
    gdouble move_y=-(rows*outside_square)/2.0;

    hc->base=g_array_sized_new(FALSE, TRUE, sizeof(struct rectangle3d), total);
    g_array_set_size(hc->base, total);
    hc->base_t=g_array_sized_new(FALSE, TRUE, sizeof(struct rectangle3d), total);
    g_array_set_size(hc->base_t, total);

    struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, 0);

    for(i=0;i<rows;i++)
      {
        for(j=0;j<columns;j++)
          {
            (p1->b1).x=move_x;
            (p1->b1).y=move_y;
            (p1->b1).z=0.0;
            (p1->b2).x=move_x+outside_square;
            (p1->b2).y=move_y;
            (p1->b2).z=0.0;
            (p1->b3).x=move_x+outside_square;
            (p1->b3).y=move_y+outside_square;
            (p1->b3).z=0.0;
            (p1->b4).x=move_x;
            (p1->b4).y=move_y+outside_square;
            (p1->b4).z=0.0;
            //Initialize color.
            (p1->cl).r=0.0;(p1->cl).g=0.0;(p1->cl).b=1.0;(p1->cl).a=1.0;
            move_x+=outside_square;
            p1++;
          }
        move_x=-(columns*outside_square)/2.0;
        move_y+=outside_square;
      }
  }
void histogram_chart_set_base_color(struct histogram_chart *hc, guint index, gdouble rgba[4])
  {
    if(index>(hc->base->len)-1)
      {
        g_warning("The base index %i isn't valid. No color was set.\n", index);
      }
    else
      {
        struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, index);
        (p1->cl).r=rgba[0];(p1->cl).g=rgba[1];(p1->cl).b=rgba[2];(p1->cl).a=rgba[3];
      }
  }
void histogram_chart_set_base_color_all(struct histogram_chart *hc, gdouble rgba[4])
  {
    gint i=0;
    struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, 0);

    for(i=0;i<(hc->base->len);i++)
      {
        (p1->cl).r=rgba[0];(p1->cl).g=rgba[1];(p1->cl).b=rgba[2];(p1->cl).a=rgba[3];
        p1++;
      }
  }
void histogram_chart_set_back_height(struct histogram_chart *hc, gdouble height)
  {
    if(height>=0.0) hc->back_height=height;
    else g_warning("The back height wasn't set. height>=0.\n");
  }
void histogram_chart_append_label_xr(struct histogram_chart *hc, const gchar *label)
  {    
    gchar *string=g_strdup(label);
    g_ptr_array_add(hc->labels_xr, (gpointer)string);
  }
void histogram_chart_append_label_yr(struct histogram_chart *hc, const gchar *label)
  {
    gchar *string=g_strdup(label);
    g_ptr_array_add(hc->labels_yr, (gpointer)string);
  }
void histogram_chart_append_label_xl(struct histogram_chart *hc, const gchar *label)
  {
    gchar *string=g_strdup(label);
    g_ptr_array_add(hc->labels_xl, (gpointer)string);
  }
void histogram_chart_append_label_yl(struct histogram_chart *hc, const gchar *label)
  {
    gchar *string=g_strdup(label);
    g_ptr_array_add(hc->labels_yl, (gpointer)string);
  }
void histogram_chart_append_label_zr(struct histogram_chart *hc, const gchar *label)
  {   
    gchar *string=g_strdup(label);
    g_ptr_array_add(hc->labels_zr, (gpointer)string);
  }
void histogram_chart_clear_labels(struct histogram_chart *hc)
  {
    gint i=0;
    gint len=0;

    if((hc->labels_xr->len)>0)
      {
        len=(hc->labels_xr->len);
        for(i=len;i>0;i--) g_ptr_array_remove_index_fast(hc->labels_xr, i-1);
      } 

    if((hc->labels_yr->len)>0)
      {
        len=(hc->labels_yr->len);
        for(i=len;i>0;i--) g_ptr_array_remove_index_fast(hc->labels_yr, i-1);
      } 

    if((hc->labels_xl->len)>0)
      {
        len=(hc->labels_xl->len);
        for(i=len;i>0;i--) g_ptr_array_remove_index_fast(hc->labels_xl, i-1);
      } 

    if((hc->labels_yl->len)>0)
      {
        len=(hc->labels_yl->len);
        for(i=len;i>0;i--) g_ptr_array_remove_index_fast(hc->labels_yl, i-1);
      }

    if((hc->labels_zr->len)>0)
      {
        len=(hc->labels_zr->len);
        for(i=len;i>0;i--) g_ptr_array_remove_index_fast(hc->labels_zr, i-1);
      }  
  }
void histogram_chart_free(struct histogram_chart *hc)
  {
    if((hc->base)!=NULL) g_array_free((hc->base), TRUE);
    if((hc->base_t)!=NULL) g_array_free((hc->base_t), TRUE);
    if((hc->labels_xr)!=NULL) g_ptr_array_free((hc->labels_xr), TRUE);
    if((hc->labels_yr)!=NULL) g_ptr_array_free((hc->labels_yr), TRUE);
    if((hc->labels_xl)!=NULL) g_ptr_array_free((hc->labels_xl), TRUE);
    if((hc->labels_yl)!=NULL) g_ptr_array_free((hc->labels_yl), TRUE);
    if((hc->labels_zr)!=NULL) g_ptr_array_free((hc->labels_zr), TRUE);
    g_free(hc);
  }
struct chart_data* chart_cubes_new(struct histogram_chart *hc, gdouble inside_square, guint inside_square_position)
  {
    gint i=0;
    gint j=0;
    gint rows=(hc->rows);
    gint columns=(hc->columns);
    gdouble outside_square=(hc->outside_square);
    gint total=rows*columns;
    gdouble gap=outside_square-inside_square;
    gdouble position=0.0;
    if(inside_square_position==CUBE_CENTER) position=gap/2.0;
    //Center points.
    gdouble move_x=-(columns*(inside_square+gap))/2.0+position;
    gdouble move_y=-(rows*(inside_square+gap))/2.0+position;

    struct chart_data *cubes=g_new(struct chart_data, 1);

    cubes->d=g_array_sized_new(FALSE, TRUE, sizeof(struct cube3d), total);
    g_array_set_size(cubes->d, total);
    cubes->d_t=g_array_sized_new(FALSE, TRUE, sizeof(struct cube3d), total);
    g_array_set_size(cubes->d_t, total);

    cubes->data_id=0;
    cubes->max_z=0.0;
    
    struct cube3d *p1=&g_array_index(cubes->d, struct cube3d, 0);

    for(i=0;i<rows;i++)
      {
        for(j=0;j<columns;j++)
          {
            p1->id=i*columns+j;
            (p1->c1).x=move_x;
            (p1->c1).y=move_y;
            (p1->c1).z=0.0;
            (p1->c2).x=move_x+inside_square;
            (p1->c2).y=move_y;
            (p1->c2).z=0.0;
            (p1->c3).x=move_x+inside_square;
            (p1->c3).y=move_y+inside_square;
            (p1->c3).z=0.0;
            (p1->c4).x=move_x;
            (p1->c4).y=move_y+inside_square;
            (p1->c4).z=0.0;
            (p1->c5).x=move_x;
            (p1->c5).y=move_y;
            (p1->c5).z=0.0;
            (p1->c6).x=move_x+inside_square;
            (p1->c6).y=move_y;
            (p1->c6).z=0.0;
            (p1->c7).x=move_x+inside_square;
            (p1->c7).y=move_y+inside_square;
            (p1->c7).z=0.0;
            (p1->c8).x=move_x;
            (p1->c8).y=move_y+inside_square;
            (p1->c8).z=0.0;
            //Initialize color to green.
            (p1->cl).r=0.0;(p1->cl).g=1.0;(p1->cl).b=0.0;(p1->cl).a=1.0;
            move_x+=inside_square+gap;
            p1++;
          }
        move_x=-(columns*(inside_square+gap))/2.0+position;
        move_y+=inside_square+gap;
      }

    return cubes;
  }
void cube_chart_set_bar_color(struct chart_data *cd, guint index, gdouble rgba[4])
  {
    if(index>(cd->d->len)-1)
      {
        g_warning("The cube index %i isn't valid. No color was set.\n", index);
      }
    else
      {
        struct cube3d *p1=&g_array_index(cd->d, struct cube3d, index);
        (p1->cl).r=rgba[0];(p1->cl).g=rgba[1];(p1->cl).b=rgba[2];(p1->cl).a=rgba[3];
      }
  }
void cube_chart_set_bar_height(struct chart_data *cd, guint index, gdouble height)
  {
    if(index>(cd->d->len)-1)
      {
        g_warning("The cube index %i isn't valid. No height was set.\n", index);
      }
    else
      {
        struct cube3d *p1=&g_array_index(cd->d, struct cube3d, index);
        (p1->c5).z=height;
        (p1->c6).z=height;
        (p1->c7).z=height;
        (p1->c8).z=height;
        if((cd->max_z)<height) (cd->max_z)=height;
      }
  }
double cube_chart_get_bar_height(struct chart_data *cd, guint index)
  {
    gdouble ret=0.0;

    if(index>(cd->d->len)-1)
      {
        g_warning("The cube index %i isn't valid. The returned height is 0.\n", index);
      }
    else
      {
        struct cube3d *p1=&g_array_index(cd->d, struct cube3d, index);
        ret=(p1->c5).z;
      }

    return ret;
  }
double cube_chart_get_max_z(struct chart_data *cd)
  {
    gint i=0;
    gdouble max_value=0.0;
    struct cube3d *p1=NULL;

    for(i=0;i<(cd->d->len);i++)
      {
        p1=&g_array_index(cd->d, struct cube3d, i);
        if((p1->c5).z>max_value) max_value=(p1->c5).z;
      }

    return max_value;
  }
void cube_chart_free(struct chart_data *cd)
  {
    g_array_free(cd->d, TRUE);
    g_array_free(cd->d_t, TRUE);
    g_free(cd);
  }
struct chart_data_aligned* chart_cylinders_new(struct histogram_chart *hc, gint sections, gdouble radius)
  {
    if(!(sections==8||sections==12))
      {
        g_warning("Sections can be either 8 or 12. Sections set to 8.\n");
        sections=8;
      }
   
    struct chart_data_aligned *cylinders=g_new(struct chart_data_aligned, 1);
    cylinders->data_id=0;
    cylinders->ring_size=sections;
    cylinders->len=sections*2.0*(hc->rows)*(hc->columns);
    cylinders->max_z=0.0;
    initialize_bezier_ring(hc, &(cylinders->d), &(cylinders->d_t), sections, radius, 0.0);

    return cylinders;
  }
void cylinder_chart_set_bar_color(struct chart_data_aligned *cd, guint index, gdouble rgba[4])
  {
    gint i=0;
    gint offset=((cd->ring_size)*2);
    gint cylinders=(cd->len)/offset;

    if(index>(cylinders-1))
      {
        g_warning("The cylinder index %i isn't valid. No color was set.\n", index);
      }
    else
      {
        struct histo_arc *p1=(cd->d)+(index*offset);
        for(i=0;i<offset;i++)
          {
            (p1->cl).r=rgba[0];(p1->cl).g=rgba[1];(p1->cl).b=rgba[2];(p1->cl).a=rgba[3];
            p1++;
          }
      }
  }
void cylinder_chart_set_bar_height(struct chart_data_aligned *cd, guint index, gdouble height)
  {
    gint i=0;
    gint offset=((cd->ring_size)*2);
    gint cylinders=(cd->len)/offset;

    if(index>(cylinders-1))
      {
        g_warning("The cylinder index %i isn't valid. No height was set.\n", index);
      }
    else
      {
        struct histo_arc *p1=(cd->d)+(index*offset)+(cd->ring_size);
        for(i=0;i<(cd->ring_size);i++)
          {
            (p1->a1).z=height;
            (p1->a2).z=height;
            (p1->a3).z=height;
            (p1->a4).z=height;
            if((cd->max_z)<height) (cd->max_z)=height;
            p1++;
          }
      }
  }
double cylinder_chart_get_bar_height(struct chart_data_aligned *cd, guint index)
  {
    gint offset=((cd->ring_size)*2);
    gint cylinders=(cd->len)/offset;
    gdouble ret=0.0;

    if(index>(cylinders-1))
      {
        g_warning("The cylinder index %i isn't valid. The returned height is 0.\n", index);
      }
    else
      {
        struct histo_arc *p1=(cd->d)+(index*offset)+(cd->ring_size);
        ret=(p1->a1).z;
      }

    return ret;
  }
double cylinder_chart_get_max_z(struct chart_data_aligned *cd)
  {
    gint i=0;
    gint offset=((cd->ring_size)*2);
    gint cylinders=(cd->len)/offset;
    
    gdouble max_value=0.0;
    struct histo_arc *p1=(cd->d)+(cd->ring_size);
        
    if(p1!=NULL)
      {
        for(i=0;i<cylinders;i++)
          {
            if((p1->a1).z>max_value) max_value=(p1->a1).z;
            p1+=offset;
          }
      }

    return max_value;
  }
void cylinder_chart_free(struct chart_data_aligned *cd)
  {
    free(cd->d);
    free(cd->d_t);
    free(cd);
  }
void set_gradient_color_bottom(gdouble r, gdouble g, gdouble b)
  {
    if(r>1.0) b_c[0]=1.0;
    else if(r<0.0) b_c[0]=0.0;
    else b_c[0]=r;

    if(g>1.0) b_c[1]=1.0;
    else if(g<0.0) b_c[1]=0.0;
    else b_c[1]=g;

    if(b>1.0) b_c[2]=1.0;
    else if(b<0.0) b_c[2]=0.0;
    else b_c[2]=b;
  }
void set_gradient_color_middle(gdouble r, gdouble g, gdouble b)
  {
    if(r>1.0) m_c[0]=1.0;
    else if(r<0.0) m_c[0]=0.0;
    else m_c[0]=r;

    if(g>1.0) m_c[1]=1.0;
    else if(g<0.0) m_c[1]=0.0;
    else m_c[1]=g;

    if(b>1.0) m_c[2]=1.0;
    else if(b<0.0) m_c[2]=0.0;
    else m_c[2]=b;
  }
void set_gradient_color_top(gdouble r, gdouble g, gdouble b)
  {
    if(r>1.0) t_c[0]=1.0;
    else if(r<0.0) t_c[0]=0.0;
    else t_c[0]=r;

    if(g>1.0) t_c[1]=1.0;
    else if(g<0.0) t_c[1]=0.0;
    else t_c[1]=g;

    if(b>1.0) t_c[2]=1.0;
    else if(b<0.0) t_c[2]=0.0;
    else t_c[2]=b;
  }
void cube_chart_draw(struct histogram_chart *hc, struct chart_data *cd, cairo_t *cr, gdouble qrs[16], guint draw_type, guint font_size, guint font_position)
  {
    histogram_chart_reset_base(hc);
    histogram_chart_rotate_base(hc, qrs);
    chart_data_reset_cubes(cd);    
    chart_data_rotate_cubes(cd, qrs);

    if(hc->rz_t[2]>0.0)
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_labels_zr(hc, cr, qrs, font_size, font_position);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_labels_xl(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_yl(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_xr(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_yr(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_base(hc, cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_set_line_width(cr, 4.0);
        histogram_chart_draw_base_lines(hc, cr);     
        g_array_sort(cd->d_t, compare_cubes);
        if(draw_type==SOLID_CUBES) chart_data_draw_cubes(cd, cr, hc->rz_t[2]);
        else if(draw_type==GRADIENT_CUBES) chart_data_draw_cubes_grad(cd, cr, hc->rz_t[2]);
        else if(draw_type==GRADIENT_CUBES_MESH) chart_data_draw_cubes_grad_mesh(cd, cr, hc->rz_t[2]);
        else if(draw_type==GRADIENT_CUBES2) chart_data_draw_cubes_grad2(cd, cr, hc->rz_t[2]);
        else chart_data_draw_cubes_mesh_grad2(cd, cr, hc->rz_t[2]);
        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_labels_zr(hc, cr, qrs, font_size, font_position);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
      }
    else
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_labels_zr(hc, cr, qrs, font_size, font_position);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr); 
        g_array_sort(cd->d_t, compare_cubes);
        if(draw_type==SOLID_CUBES) chart_data_draw_cubes(cd, cr, hc->rz_t[2]);
        else if(draw_type==GRADIENT_CUBES) chart_data_draw_cubes_grad(cd, cr, hc->rz_t[2]);
        else if(draw_type==GRADIENT_CUBES_MESH) chart_data_draw_cubes_grad_mesh(cd, cr, hc->rz_t[2]);
        else if(draw_type==GRADIENT_CUBES2) chart_data_draw_cubes_grad2(cd, cr, hc->rz_t[2]);
        else chart_data_draw_cubes_mesh_grad2(cd, cr, hc->rz_t[2]);
        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_labels_zr(hc, cr, qrs, font_size, font_position);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_base(hc, cr); 
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_set_line_width(cr, 4.0);
        histogram_chart_draw_base_lines(hc, cr);        
        histogram_chart_draw_labels_xl(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_yl(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_xr(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_yr(hc, cr, qrs, font_size, font_position);
      }
  }
void chart_data_draw(struct histogram_chart *hc, struct chart_data *cd, cairo_t *cr, gdouble qrs[16], gint draw_option, gdouble line_color[4], gdouble point_color[4], gdouble line_width, guint font_size, guint font_position)
  {
    histogram_chart_reset_base(hc);
    histogram_chart_rotate_base(hc, qrs);    
    chart_data_reset(cd);    
    chart_data_rotate(cd, qrs);

    if(hc->rz_t[2]>0.0)
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_labels_zr(hc, cr, qrs, font_size, font_position);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_labels_xl(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_yl(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_xr(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_yr(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_base(hc, cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_set_line_width(cr, 4.0);
        histogram_chart_draw_base_lines(hc, cr); 

        //Draw data.
        cairo_set_line_width(cr, line_width);
        if(draw_option==DRAW_LINES)
          {
            cairo_set_source_rgba(cr, line_color[0], line_color[1], line_color[2], line_color[3]);
            chart_data_draw_lines(cd, cr);
            cairo_set_source_rgba(cr, point_color[0], point_color[1], point_color[2], point_color[3]);
            chart_data_draw_points(cd, cr);
          }
        else if(draw_option==DRAW_SMOOTH)
          {
            cairo_set_source_rgba(cr, line_color[0], line_color[1], line_color[2], line_color[3]);
            chart_data_draw_smooth(cd, cr);
            cairo_set_source_rgba(cr, point_color[0], point_color[1], point_color[2], point_color[3]);
            chart_data_draw_points(cd, cr);
          }
        else
          {
            cairo_set_source_rgba(cr, point_color[0], point_color[1], point_color[2], point_color[3]);
            chart_data_draw_points(cd, cr);
          }
       
        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_labels_zr(hc, cr, qrs, font_size, font_position);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
      }
    else
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_labels_zr(hc, cr, qrs, font_size, font_position);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr); 

        //Draw data.
        cairo_set_line_width(cr, line_width);
        if(draw_option==DRAW_LINES)
          {
            cairo_set_source_rgba(cr, line_color[0], line_color[1], line_color[2], line_color[3]);
            chart_data_draw_lines(cd, cr);
            cairo_set_source_rgba(cr, point_color[0], point_color[1], point_color[2], point_color[3]);
            chart_data_draw_points(cd, cr);
          }
        else if(draw_option==DRAW_SMOOTH)
          {
            cairo_set_source_rgba(cr, line_color[0], line_color[1], line_color[2], line_color[3]);
            chart_data_draw_smooth(cd, cr);
            cairo_set_source_rgba(cr, point_color[0], point_color[1], point_color[2], point_color[3]);
            chart_data_draw_points(cd, cr);
          }
        else
          {
            cairo_set_source_rgba(cr, point_color[0], point_color[1], point_color[2], point_color[3]);
            chart_data_draw_points(cd, cr);
          }

        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_labels_zr(hc, cr, qrs, font_size, font_position);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_base(hc, cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_set_line_width(cr, 4.0); 
        histogram_chart_draw_base_lines(hc, cr);        
        histogram_chart_draw_labels_xl(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_yl(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_xr(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_yr(hc, cr, qrs, font_size, font_position);  
      }
  }
void chart_data_draw_groups(struct histogram_chart *hc, GPtrArray *chart_groups, cairo_t *cr, gdouble qrs[16], gint draw_option, guint font_size, guint font_position)
  {
    gint i=0;
    histogram_chart_reset_base(hc);
    histogram_chart_rotate_base(hc, qrs);    

    if(hc->rz_t[2]>0.0)
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_labels_zr(hc, cr, qrs, font_size, font_position);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_labels_xl(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_yl(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_xr(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_yr(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_base(hc, cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_set_line_width(cr, 4.0);
        histogram_chart_draw_base_lines(hc, cr); 

        //Draw data.
        struct chart_data_group *p1=NULL;
        for(i=0;i<(chart_groups->len);i++)
          {
            p1=g_ptr_array_index(chart_groups, i);
            chart_data_reset(p1->cd);    
            chart_data_rotate(p1->cd, qrs);
            cairo_set_line_width(cr, p1->line_width);
            if(draw_option==DRAW_LINES)
              {
                cairo_set_source_rgba(cr, p1->line_color[0], p1->line_color[1], p1->line_color[2], p1->line_color[3]);
                chart_data_draw_lines(p1->cd, cr);
                cairo_set_source_rgba(cr, p1->point_color[0], p1->point_color[1], p1->point_color[2], p1->point_color[3]);
                chart_data_draw_points(p1->cd, cr);
              }
            else if(draw_option==DRAW_SMOOTH)
              {
                cairo_set_source_rgba(cr, p1->line_color[0], p1->line_color[1], p1->line_color[2], p1->line_color[3]);
                if(p1->cd->d_t->len<3) chart_data_draw_lines(p1->cd, cr);
                else chart_data_draw_smooth(p1->cd, cr);
                cairo_set_source_rgba(cr, p1->point_color[0], p1->point_color[1], p1->point_color[2], p1->point_color[3]);
                chart_data_draw_points(p1->cd, cr);
              }
            else
              {
                cairo_set_source_rgba(cr, p1->point_color[0], p1->point_color[1], p1->point_color[2], p1->point_color[3]);
                chart_data_draw_points(p1->cd, cr);
              }
          }
       
        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_labels_zr(hc, cr, qrs, font_size, font_position);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
      }
    else
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_labels_zr(hc, cr, qrs, font_size, font_position);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr); 

        //Draw data.
        struct chart_data_group *p1=NULL;
        for(i=0;i<(chart_groups->len);i++)
          {
            p1=g_ptr_array_index(chart_groups, i);
            chart_data_reset(p1->cd);    
            chart_data_rotate(p1->cd, qrs);
            cairo_set_line_width(cr, p1->line_width);
            if(draw_option==DRAW_LINES)
              {
                cairo_set_source_rgba(cr, p1->line_color[0], p1->line_color[1], p1->line_color[2], p1->line_color[3]);
                chart_data_draw_lines(p1->cd, cr);
                cairo_set_source_rgba(cr, p1->point_color[0], p1->point_color[1], p1->point_color[2], p1->point_color[3]);
                chart_data_draw_points(p1->cd, cr);
              }
            else if(draw_option==DRAW_SMOOTH)
              {
                cairo_set_source_rgba(cr, p1->line_color[0], p1->line_color[1], p1->line_color[2], p1->line_color[3]);
                if(p1->cd->d_t->len<3) chart_data_draw_lines(p1->cd, cr);
                else chart_data_draw_smooth(p1->cd, cr);
                cairo_set_source_rgba(cr, p1->point_color[0], p1->point_color[1], p1->point_color[2], p1->point_color[3]);
                chart_data_draw_points(p1->cd, cr);
              }
            else
              {
                cairo_set_source_rgba(cr, p1->point_color[0], p1->point_color[1], p1->point_color[2], p1->point_color[3]);
                chart_data_draw_points(p1->cd, cr);
              }
          }

        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_labels_zr(hc, cr, qrs, font_size, font_position);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_base(hc, cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_set_line_width(cr, 4.0); 
        histogram_chart_draw_base_lines(hc, cr);        
        histogram_chart_draw_labels_xl(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_yl(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_xr(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_yr(hc, cr, qrs, font_size, font_position);  
      }
  }
void chart_data_draw_aligned(struct histogram_chart *hc, struct chart_data_aligned *cd, cairo_t *cr, gdouble qrs[16], gint draw_option, guint font_size, guint font_position)
  {
    histogram_chart_reset_base(hc);
    histogram_chart_rotate_base(hc, qrs);    
    chart_data_reset_aligned(cd);    
    chart_data_rotate_aligned(cd, qrs);

    if(hc->rz_t[2]>0.0)
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_labels_zr(hc, cr, qrs, font_size, font_position);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_labels_xl(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_yl(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_xr(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_yr(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_base(hc, cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_set_line_width(cr, 4.0);
        histogram_chart_draw_base_lines(hc, cr);
        cairo_set_line_width(cr, 2.0); 

        //Draw data.
        cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);        
        if(cd->ring_size==8) qsort(cd->d_t, ((cd->len)/16), sizeof(struct cylinder16), compare_cylinders16);
        else qsort(cd->d_t, ((cd->len)/24), sizeof(struct cylinder24), compare_cylinders24);
        if(draw_option==SOLID_CYLINDERS) chart_data_draw_cylinders(cd, cr, hc->rz_t[2]);
        else if(draw_option==GRADIENT_CYLINDERS) chart_data_draw_cylinders_grad(cd, cr, hc->rz_t[2]);
        else chart_data_draw_cylinders_grad_mesh(cd, cr, hc->rz_t[2]);
       
        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_labels_zr(hc, cr, qrs, font_size, font_position);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
      }
    else
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_labels_zr(hc, cr, qrs, font_size, font_position);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr); 

        //Draw data.
        cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
        if(cd->ring_size==8)qsort(cd->d_t, ((cd->len)/16), sizeof(struct cylinder16), compare_cylinders16);
        else qsort(cd->d_t, ((cd->len)/24), sizeof(struct cylinder24), compare_cylinders24);
        if(draw_option==SOLID_CYLINDERS) chart_data_draw_cylinders(cd, cr, hc->rz_t[2]);
        else if(draw_option==GRADIENT_CYLINDERS) chart_data_draw_cylinders_grad(cd, cr, hc->rz_t[2]);
        else chart_data_draw_cylinders_grad_mesh(cd, cr, hc->rz_t[2]);
        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
            histogram_chart_draw_labels_zr(hc, cr, qrs, font_size, font_position);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_base(hc, cr);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_set_line_width(cr, 4.0); 
        histogram_chart_draw_base_lines(hc, cr);       
        histogram_chart_draw_labels_xl(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_yl(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_xr(hc, cr, qrs, font_size, font_position);
        histogram_chart_draw_labels_yr(hc, cr, qrs, font_size, font_position);
      }
  }
void set_projection_matrix(gdouble qrs[16], gdouble angle, gdouble near, gdouble far)
  {
    gdouble scale=1.0/tan(angle*0.5*G_PI/180.0);
    
    qrs[0]=scale;qrs[1]=0.0;qrs[2]=0.0;qrs[3]=0.0;
    qrs[4]=0.0;qrs[5]=scale;qrs[6]=0.0;qrs[7]=0.0;
    qrs[8]=0.0;qrs[9]=0.0;qrs[10]=near/far;qrs[11]=0.0;
    qrs[12]=0.0;qrs[13]=0.0;qrs[14]=-(1.0-near/far);qrs[15]=1.0;
  }
void histo_quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[16])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);

    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=0.0;

    qrs[4]=2.0*(qi*qj+qk*qr);
    qrs[5]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[6]=2.0*(qj*qk-qi*qr);
    qrs[7]=0.0;

    qrs[8]=2.0*(qi*qk-qj*qr);
    qrs[9]=2.0*(qj*qk+qi*qr);
    qrs[10]=1.0-2.0*(qi*qi+qj*qj);
    qrs[11]=0.0;

    qrs[12]=0.0;
    qrs[13]=0.0;
    qrs[14]=0.0;
    qrs[15]=1.0;
  }
void histo_matrix_multiply(const gdouble in1[16], const gdouble in2[16], gdouble out[16])
  {
    out[0]=in1[0]*in2[0]+in1[4]*in2[1]+in1[8]*in2[2]+in1[12]*in2[3];
    out[1]=in1[1]*in2[0]+in1[5]*in2[1]+in1[9]*in2[2]+in1[13]*in2[3];
    out[2]=in1[2]*in2[0]+in1[6]*in2[1]+in1[10]*in2[2]+in1[14]*in2[3];
    out[3]=in1[3]*in2[0]+in1[7]*in2[1]+in1[11]*in2[2]+in1[15]*in2[3];

    out[4]=in1[0]*in2[4]+in1[4]*in2[5]+in1[8]*in2[6]+in1[12]*in2[7];
    out[5]=in1[1]*in2[4]+in1[5]*in2[5]+in1[9]*in2[6]+in1[13]*in2[7];
    out[6]=in1[2]*in2[4]+in1[6]*in2[5]+in1[10]*in2[6]+in1[14]*in2[7];
    out[7]=in1[3]*in2[4]+in1[7]*in2[5]+in1[11]*in2[6]+in1[15]*in2[7];

    out[8]=in1[0]*in2[8]+in1[4]*in2[9]+in1[8]*in2[10]+in1[12]*in2[11];
    out[9]=in1[1]*in2[8]+in1[5]*in2[9]+in1[9]*in2[10]+in1[13]*in2[11];
    out[10]=in1[2]*in2[8]+in1[6]*in2[9]+in1[10]*in2[10]+in1[14]*in2[11];
    out[11]=in1[3]*in2[8]+in1[7]*in2[9]+in1[11]*in2[10]+in1[15]*in2[11];

    out[12]=in1[0]*in2[12]+in1[4]*in2[13]+in1[8]*in2[14]+in1[12]*in2[15];
    out[13]=in1[1]*in2[12]+in1[5]*in2[13]+in1[9]*in2[14]+in1[13]*in2[15];
    out[14]=in1[2]*in2[12]+in1[6]*in2[13]+in1[10]*in2[14]+in1[14]*in2[15];
    out[15]=in1[3]*in2[12]+in1[7]*in2[13]+in1[11]*in2[14]+in1[15]*in2[15];
  }
//Public draw chart functioms.
void histogram_chart_draw_base(struct histogram_chart *hc, cairo_t *cr)
  {
    gint i=0;
    struct rectangle3d *p1=&g_array_index(hc->base_t, struct rectangle3d, 0);

    for(i=0;i<(hc->base_t->len);i++)
      {
        cairo_set_source_rgba(cr, (p1->cl).r, (p1->cl).g, (p1->cl).b, (p1->cl).a);
        cairo_move_to(cr, (p1->b1).x, (p1->b1).y);
        cairo_line_to(cr, (p1->b2).x, (p1->b2).y);
        cairo_line_to(cr, (p1->b3).x, (p1->b3).y);
        cairo_line_to(cr, (p1->b4).x, (p1->b4).y);
        cairo_close_path(cr);
        cairo_fill(cr);
        p1++;
      }
  }
void histogram_chart_draw_base_lines(struct histogram_chart *hc, cairo_t *cr)
  {
    gint i=0;
    struct rectangle3d *p1=&g_array_index(hc->base_t, struct rectangle3d, 0);
    struct rectangle3d *p2=&g_array_index(hc->base_t, struct rectangle3d, (hc->columns)-1);

    for(i=0;i<(hc->rows);i++)
      {
        cairo_move_to(cr, (p1->b1).x, (p1->b1).y);
        cairo_line_to(cr, (p2->b2).x, (p2->b2).y);
        cairo_stroke(cr);
        p1+=(hc->columns);p2+=(hc->columns);
      }
    p1-=(hc->columns);p2-=(hc->columns);
    cairo_move_to(cr, (p1->b4).x, (p1->b4).y);
    cairo_line_to(cr, (p2->b3).x, (p2->b3).y);
    cairo_stroke(cr);

    p1=&g_array_index(hc->base_t, struct rectangle3d, 0);
    p2=&g_array_index(hc->base_t, struct rectangle3d, (hc->rows)*(hc->columns)-(hc->columns));
    for(i=0;i<(hc->columns);i++)
      {
        cairo_move_to(cr, (p1->b1).x, (p1->b1).y);
        cairo_line_to(cr, (p2->b4).x, (p2->b4).y);
        cairo_stroke(cr);
        p1++;p2++;
      }
    p1--;p2--;
    cairo_move_to(cr, (p1->b2).x, (p1->b2).y);
    cairo_line_to(cr, (p2->b3).x, (p2->b3).y);
    cairo_stroke(cr);
  }
void histogram_chart_draw_labels_xr(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[16], guint font_size, guint font_position)
  {
    gint i=0;
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    gdouble font_shift=(hc->outside_square)/2.0;
    gdouble cr1=1.0;
    gdouble cr2=1.0;
    gdouble cr3=1.0;
    gdouble w=0.0;

    struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, hc->columns-1);

    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, font_size);
    for(i=0;i<(hc->rows);i++)
      {
        cr1=(p1->b2).x;
        cr2=(p1->b2).y;
        cr3=0.0;
        if(i<(hc->labels_xr->len))
          {
            cairo_save(cr);
            cairo_text_extents(cr, (gchar*)g_ptr_array_index(hc->labels_xr, i), &extents);
            w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
            cairo_matrix_init(&matrix1, qrs[0]/w, qrs[4]/w, qrs[1]/w, qrs[5]/w, 0.0, 0.0);
            cairo_transform(cr, &matrix1);
            if(font_position==FONT_MIDDLE) cairo_move_to(cr, (p1->b2).x+10.0, (p1->b2).y+font_shift+extents.height/2.0);
            else cairo_move_to(cr, (p1->b2).x+10.0, (p1->b2).y+extents.height/2.0);
            cairo_show_text(cr, (gchar*)g_ptr_array_index(hc->labels_xr, i));
            cairo_restore(cr);
          }
        p1+=(hc->columns);
      }
  }
void histogram_chart_draw_labels_yr(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[16], guint font_size, guint font_position)
  {
    gint i=0;
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    gdouble font_shift=(hc->outside_square)/2.0;
    gdouble cr1=1.0;
    gdouble cr2=1.0;
    gdouble cr3=1.0;
    gdouble w=0.0;

    struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, (hc->rows)*(hc->columns)-(hc->columns));

    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, font_size);
    for(i=0;i<(hc->columns);i++)
      {
        cr1=(p1->b4).x;
        cr2=(p1->b4).y;
        cr3=0.0;
        if(i<(hc->labels_yr->len))
          {
            cairo_save(cr);
            cairo_text_extents(cr, (gchar*)g_ptr_array_index(hc->labels_yr, i), &extents);
            w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
            cairo_matrix_init(&matrix1, qrs[0]/w, qrs[4]/w, qrs[1]/w, qrs[5]/w, 0.0, 0.0);
            cairo_transform(cr, &matrix1);
            if(font_position==FONT_MIDDLE) cairo_move_to(cr, (p1->b4).x+font_shift, (p1->b4).y+extents.height);
            else cairo_move_to(cr, (p1->b4).x, (p1->b4).y+extents.height);
            cairo_rotate(cr, G_PI/4.0);
            cairo_show_text(cr, (gchar*)g_ptr_array_index(hc->labels_yr, i));
            cairo_restore(cr);
          }
        p1++;
      }
  }
void histogram_chart_draw_labels_xl(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[16], guint font_size, guint font_position)
  {
    gint i=0;
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    gdouble font_shift=(hc->outside_square)/2.0;
    gdouble cr1=1.0;
    gdouble cr2=1.0;
    gdouble cr3=1.0;
    gdouble w=0.0;

    struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, 0);

    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, font_size);
    for(i=0;i<(hc->rows);i++)
      {
        cr1=(p1->b1).x;
        cr2=(p1->b1).y;
        cr3=0.0;
        if(i<(hc->labels_xl->len))
          {
            cairo_save(cr);
            cairo_text_extents(cr, (gchar*)g_ptr_array_index(hc->labels_xl, i), &extents);
            w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
            cairo_matrix_init(&matrix1, qrs[0]/w, qrs[4]/w, qrs[1]/w, qrs[5]/w, 0.0, 0.0);
            cairo_transform(cr, &matrix1); 
            if(font_position==FONT_MIDDLE) cairo_move_to(cr, (p1->b1).x-extents.width-10.0, (p1->b1).y+font_shift+extents.height/2.0);
            else cairo_move_to(cr, (p1->b1).x-extents.width-10.0, (p1->b1).y+extents.height/2.0);
            cairo_show_text(cr, (gchar*)g_ptr_array_index(hc->labels_xl, i));
            cairo_restore(cr);
          }
        p1+=(hc->columns);
      }
  }
void histogram_chart_draw_labels_yl(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[16], guint font_size, guint font_position)
  {
    gint i=0;
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    gdouble font_shift=(hc->outside_square)/2.0;
    gdouble cr1=1.0;
    gdouble cr2=1.0;
    gdouble cr3=1.0;
    gdouble w=0.0;

    struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, 0);

    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, font_size);
    for(i=0;i<(hc->columns);i++)
      {
        cr1=(p1->b1).x;
        cr2=(p1->b1).y;
        cr3=0.0;
        if(i<(hc->labels_yl->len))
          {
            cairo_save(cr);
            cairo_text_extents(cr, (gchar*)g_ptr_array_index(hc->labels_yl, i), &extents);
            w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
            cairo_matrix_init(&matrix1, qrs[0]/w, qrs[4]/w, qrs[1]/w, qrs[5]/w, 0.0, 0.0);
            cairo_transform(cr, &matrix1); 
            if(font_position==FONT_MIDDLE) cairo_move_to(cr, (p1->b1).x+font_shift, (p1->b1).y-extents.height/2.0);
            else cairo_move_to(cr, (p1->b1).x, (p1->b1).y-extents.height/2.0);
            cairo_rotate(cr, -G_PI/4.0);
            cairo_show_text(cr, (gchar*)g_ptr_array_index(hc->labels_yl, i));
            cairo_restore(cr);
          }
        p1++;
      }
  }
void histogram_chart_draw_labels_zr(struct histogram_chart *hc, cairo_t *cr, gdouble qrs[16], guint font_size, guint font_position)
  {
    //Needs to use font position to shift font.
    gint i=0;
    gdouble slope_x1=0.0;
    gdouble slope_y1=0.0;
    cairo_matrix_t matrix1;
    gdouble r[16];
    gdouble out[16];

    cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, font_size);

    histo_quaternion_rotation(0.0, 0.0, -G_PI/2.0, r);
    histo_matrix_multiply(r, qrs, out);

    struct rectangle3d *p1=&g_array_index(hc->base_t, struct rectangle3d, (hc->columns)-1);

    slope_x1=(hc->c3_t[0]-(p1->b2).x);
    slope_y1=(hc->c3_t[1]-(p1->b2).y);
    slope_x1/=hc->back_rows;
    slope_y1/=hc->back_rows;

    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    for(i=0;i<(hc->back_rows);i++)
      {
        if(hc->rx_t[0]>=0.0)
          {
            cairo_move_to(cr, (p1->b2).x+(gdouble)(i)*slope_x1+8.0, (p1->b2).y+(gdouble)(i)*slope_y1);
          }
        else
          {
            cairo_move_to(cr, (p1->b2).x+(gdouble)(i)*slope_x1-8.0, (p1->b2).y+(gdouble)(i)*slope_y1);
          }
        if(i<(hc->labels_zr->len))
          {
            cairo_save(cr);
            //Rotate the text in the center but have it drawn at the move to position.
            cairo_matrix_init(&matrix1, out[0], out[4], out[1], out[5], 0.0, 0.0);
            cairo_transform(cr, &matrix1); 
            cairo_show_text(cr, (gchar*)g_ptr_array_index(hc->labels_zr, i));
            cairo_restore(cr);
          }
      }
  }
void histogram_chart_draw_back(struct histogram_chart *hc, cairo_t *cr)
  {
    gint i=0;
    gdouble slope_x1=0.0;
    gdouble slope_x2=0.0;
    gdouble slope_y1=0.0;
    gdouble slope_y2=0.0;
    struct rectangle3d *p1=&g_array_index(hc->base_t, struct rectangle3d, 0);
    struct rectangle3d *p2=&g_array_index(hc->base_t, struct rectangle3d, hc->columns-1);   

    slope_x1=(hc->c2_t[0]-(p1->b1).x);
    slope_y1=(hc->c2_t[1]-(p1->b1).y);
    slope_x2=(hc->c3_t[0]-(p2->b2).x);
    slope_y2=(hc->c3_t[1]-(p2->b2).y);

    cairo_set_source_rgba(cr, (hc->back_color)[0], (hc->back_color)[1], (hc->back_color)[2], (hc->back_color)[3]);
    cairo_set_line_width(cr, 3.0);

    cairo_move_to(cr, (p1->b1).x, (p1->b1).y);
    cairo_line_to(cr, (p1->b1).x+slope_x1, (p1->b1).y+slope_y1);
    cairo_line_to(cr, (p2->b2).x+slope_x2, (p2->b2).y+slope_y2);
    cairo_line_to(cr, (p2->b2).x, (p2->b2).y);
    cairo_stroke(cr);

    slope_x1/=(hc->back_rows);
    slope_y1/=(hc->back_rows);
    slope_x2/=(hc->back_rows);
    slope_y2/=(hc->back_rows);

    for(i=0;i<(hc->back_rows);i++)
      {
        cairo_move_to(cr, (p1->b1).x+(gdouble)(i+1)*slope_x1, (p1->b1).y+(gdouble)(i+1)*slope_y1);
        cairo_line_to(cr, (p2->b2).x+(gdouble)(i+1)*slope_x2, (p2->b2).y+(gdouble)(i+1)*slope_y2); 
        cairo_stroke(cr);
      }

    cairo_set_line_width(cr, 1.0);
  }
void histogram_chart_draw_side(struct histogram_chart *hc, cairo_t *cr)
  {
    gint i=0;
    gdouble slope_x1=0.0;
    gdouble slope_x2=0.0;
    gdouble slope_y1=0.0;
    gdouble slope_y2=0.0;
    struct rectangle3d *p1=&g_array_index(hc->base_t, struct rectangle3d, 0);
    struct rectangle3d *p2=&g_array_index(hc->base_t, struct rectangle3d, (hc->rows)*(hc->columns)-(hc->columns));  

    slope_x1=(hc->c1_t[0]-(p2->b4).x);
    slope_y1=(hc->c1_t[1]-(p2->b4).y);
    slope_x2=(hc->c2_t[0]-(p1->b1).x);
    slope_y2=(hc->c2_t[1]-(p1->b1).y);

    cairo_set_source_rgba(cr, (hc->back_color)[0], (hc->back_color)[1], (hc->back_color)[2], (hc->back_color)[3]);
    cairo_set_line_width(cr, 3.0);

    cairo_move_to(cr, (p2->b4).x, (p2->b4).y);
    cairo_line_to(cr, (p2->b4).x+slope_x1, (p2->b4).y+slope_y1);
    cairo_line_to(cr, (p1->b1).x+slope_x2, (p1->b1).y+slope_y2);
    cairo_line_to(cr, (p1->b1).x, (p1->b1).y);
    cairo_stroke(cr);

    slope_x1/=hc->back_rows;
    slope_y1/=hc->back_rows;
    slope_x2/=hc->back_rows;
    slope_y2/=hc->back_rows;

    for(i=0;i<(hc->back_rows);i++)
      {
        cairo_move_to(cr, (p2->b4).x+(gdouble)(i+1)*slope_x1, (p2->b4).y+(gdouble)(i+1)*slope_y1);
        cairo_line_to(cr, (p1->b1).x+(gdouble)(i+1)*slope_x2, (p1->b1).y+(gdouble)(i+1)*slope_y2); 
        cairo_stroke(cr);
      }

    cairo_set_line_width(cr, 1.0);
  }
void histogram_chart_reset_base(struct histogram_chart *hc)
  {
    struct rectangle3d *p1=&g_array_index(hc->base, struct rectangle3d, 0);
    struct rectangle3d *p2=&g_array_index(hc->base_t, struct rectangle3d, 0);
    memcpy(p2, p1, sizeof(struct rectangle3d)*(hc->base->len));
  }
static void chart_data_reset_cubes(struct chart_data *cd)
  {
    struct cube3d *p1=&g_array_index(cd->d, struct cube3d, 0);
    struct cube3d *p2=&g_array_index(cd->d_t, struct cube3d, 0);
    memcpy(p2, p1, sizeof(struct cube3d)*(cd->d->len));
  }
static void chart_data_reset(struct chart_data *cd)
  {   
    struct chart_point3d *p1=&g_array_index(cd->d, struct chart_point3d, 0);
    struct chart_point3d *p2=&g_array_index(cd->d_t, struct chart_point3d, 0);
    memcpy(p2, p1, sizeof(struct chart_point3d)*(cd->d->len));
  }
static void chart_data_reset_aligned(struct chart_data_aligned *cd)
  {
    struct histo_arc *p1=(struct histo_arc*)(cd->d);
    struct histo_arc *p2=(struct histo_arc*)(cd->d_t);
    memcpy(p2, p1, sizeof(struct histo_arc)*(cd->len));
  }
static void chart_data_draw_cubes(struct chart_data *cd, cairo_t *cr, gdouble chart_z)
  {
    gint i=0;
    struct cube3d *p1=&g_array_index(cd->d_t, struct cube3d, 0);

    for(i=0;i<(cd->d_t->len);i++)
      {
        cairo_set_source_rgba(cr, (p1->cl).r, (p1->cl).g, (p1->cl).b, (p1->cl).a);
        //Bottom of cube.
        cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
        cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
        cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
        cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        //Sides of cube
        cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
        cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
        cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
        cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        cairo_move_to(cr, (p1->c2).x, (p1->c2).y);
        cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
        cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
        cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        cairo_move_to(cr, (p1->c3).x, (p1->c3).y);
        cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
        cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
        cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        cairo_move_to(cr, (p1->c4).x, (p1->c4).y);
        cairo_line_to(cr, (p1->c1).x, (p1->c1).y);
        cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
        cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        //Top of cube
        cairo_move_to(cr, (p1->c5).x, (p1->c5).y);
        cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
        cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
        cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
        cairo_close_path(cr);
        if(chart_z>0.0)
          {
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_set_line_width(cr, 4.0);
            cairo_save(cr);
            cairo_clip_preserve(cr); 
            cairo_stroke(cr);
            cairo_restore(cr);
          }
        else
          {
            cairo_fill(cr);
          }
        p1++;
      }
  }
static void chart_data_draw_cubes_grad(struct chart_data *cd, cairo_t *cr, gdouble chart_z)
  {
    /*
        These cubes have been sorted at this point so need to calculate the distance
        from the top and bottom square.
    */
    gint i=0;
    struct cube3d *p1=&g_array_index(cd->d_t, struct cube3d, 0);
    struct cube3d *p2=NULL;
    gdouble distance=0.0;
    gdouble max_val=(cd->max_z);
    gdouble dm=0.0;
    cairo_pattern_t *pattern1=NULL;
    //For reduced cross product. Just need if z is positive or negative.
    gdouble a[2]={0.0, 0.0};
    gdouble b[2]={0.0, 0.0};
    gdouble z=0;
    gdouble r1=0.0;
    gdouble g1=0.0;
    gdouble b1=0.0;
    //For finding gradient control line. 
    gdouble m_x=0.0;
    gdouble m_y=0.0;
    gdouble out_x=0.0;
    gdouble out_y=0.0; 

    for(i=0;i<(cd->d_t->len);i++)
      {
        //Bottom of cube. Always draw the base.
        cairo_set_source_rgba(cr, b_c[0], b_c[1], b_c[2], b_c[3]);
        cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
        cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
        cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
        cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        /*
          Get the original height of the cube to standardize the perspective. Remember the
          tranformed cubes are sorted so get the cube with the id.
        */
        p2=&g_array_index(cd->d, struct cube3d, p1->id);
        distance=(p2->c5).z;     
        dm=distance/max_val;
        get_gradient_color_bt(dm, &r1, &g1, &b1);

        /*
          Cross product. Just need to know if the z value is positive or negative before drawing
          the side gradient of the cube for solid colors.
        */
        a[0]=(p1->c2).x-(p1->c1).x;
        a[1]=(p1->c2).y-(p1->c1).y;
        b[0]=(p1->c5).x-(p1->c1).x;
        b[1]=(p1->c5).y-(p1->c1).y;
        z=a[0]*b[1]-a[1]*b[0];
        if(z>0.0)
          {
            //90 degree line slope.
            m_x=-1.0*((p1->c1).x-(p1->c2).x);
            m_y=(p1->c1).y-(p1->c2).y;
            //Preset out_x and out_y. 
            out_x=(p1->c6).x;
            out_y=(p1->c6).y;
            //intersect the 90 degree line with the opposite parallel line to run the gradient along.
            intersection((p1->c5).x, (p1->c5).y, (p1->c6).x, (p1->c6).y, (p1->c2).x, (p1->c2).y, (p1->c2).x+m_y, (p1->c2).y+m_x, &out_x, &out_y);
            pattern1=cairo_pattern_create_linear((p1->c2).x, (p1->c2).y, out_x, out_y); 
            cairo_pattern_add_color_stop_rgba(pattern1, 0.0, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_pattern_add_color_stop_rgba(pattern1, 1.0, r1, g1, b1, t_c[3]); 
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
            cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
            cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
            cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        a[0]=(p1->c3).x-(p1->c2).x;
        a[1]=(p1->c3).y-(p1->c2).y;
        b[0]=(p1->c6).x-(p1->c2).x;
        b[1]=(p1->c6).y-(p1->c2).y;
        z=a[0]*b[1]-a[1]*b[0];       
        if(z>0.0)
          {
            m_x=-1.0*((p1->c2).x-(p1->c3).x);
            m_y=(p1->c2).y-(p1->c3).y; 
            out_x=(p1->c7).x;
            out_y=(p1->c7).y;
            intersection((p1->c7).x, (p1->c7).y, (p1->c6).x, (p1->c6).y, (p1->c3).x, (p1->c3).y, (p1->c3).x+m_y, (p1->c3).y+m_x, &out_x, &out_y);
            pattern1=cairo_pattern_create_linear((p1->c3).x, (p1->c3).y, out_x, out_y); 
            cairo_pattern_add_color_stop_rgba(pattern1, 0.0, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_pattern_add_color_stop_rgba(pattern1, 1.0, r1, g1, b1, t_c[3]); 
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->c2).x, (p1->c2).y);
            cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
            cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
            cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        a[0]=(p1->c4).x-(p1->c3).x;
        a[1]=(p1->c4).y-(p1->c3).y;
        b[0]=(p1->c7).x-(p1->c3).x;
        b[1]=(p1->c7).y-(p1->c3).y;
        z=a[0]*b[1]-a[1]*b[0];
        if(z>0.0)
          {
            m_x=-1.0*((p1->c3).x-(p1->c4).x);
            m_y=(p1->c3).y-(p1->c4).y;
            out_x=(p1->c8).x;
            out_y=(p1->c8).y; 
            intersection((p1->c7).x, (p1->c7).y, (p1->c8).x, (p1->c8).y, (p1->c4).x, (p1->c4).y, (p1->c4).x+m_y, (p1->c4).y+m_x, &out_x, &out_y);
            pattern1=cairo_pattern_create_linear((p1->c4).x, (p1->c4).y, out_x, out_y); 
            cairo_pattern_add_color_stop_rgba(pattern1, 0.0, b_c[0], b_c[1], b_c[2], b_c[3]); 
            cairo_pattern_add_color_stop_rgba(pattern1, 1.0, r1, g1, b1, t_c[3]); 
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->c3).x, (p1->c3).y);
            cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
            cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
            cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        a[0]=(p1->c1).x-(p1->c4).x;
        a[1]=(p1->c1).y-(p1->c4).y;
        b[0]=(p1->c8).x-(p1->c4).x;
        b[1]=(p1->c8).y-(p1->c4).y;
        z=a[0]*b[1]-a[1]*b[0];
        if(z>0.0)
          {
            m_x=-1.0*((p1->c4).x-(p1->c1).x);
            m_y=(p1->c4).y-(p1->c1).y; 
            out_x=(p1->c5).x;
            out_y=(p1->c5).y;
            intersection((p1->c5).x, (p1->c5).y, (p1->c8).x, (p1->c8).y, (p1->c1).x, (p1->c1).y, (p1->c1).x+m_y, (p1->c1).y+m_x, &out_x, &out_y);
            pattern1=cairo_pattern_create_linear((p1->c1).x, (p1->c1).y, out_x, out_y); 
            cairo_pattern_add_color_stop_rgba(pattern1, 0.0, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_pattern_add_color_stop_rgba(pattern1, 1.0, r1, g1, b1, t_c[3]); 
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->c4).x, (p1->c4).y);
            cairo_line_to(cr, (p1->c1).x, (p1->c1).y);
            cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
            cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        //Top of cube
        a[0]=(p1->c6).x-(p1->c5).x;
        a[1]=(p1->c6).y-(p1->c5).y;
        b[0]=(p1->c8).x-(p1->c5).x;
        b[1]=(p1->c8).y-(p1->c5).y;
        z=a[0]*b[1]-a[1]*b[0];
        if(z>0.0)
          {
            cairo_set_source_rgba(cr, r1, g1, b1, t_c[3]); 
            cairo_move_to(cr, (p1->c5).x, (p1->c5).y);
            cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
            cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
            cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
            cairo_close_path(cr);
            if(chart_z>0.0)
              {
                cairo_fill_preserve(cr);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_set_line_width(cr, 4.0);
                cairo_save(cr);
                cairo_clip_preserve(cr); 
                cairo_stroke(cr);
                cairo_restore(cr);
              }
            else
              {
                cairo_fill(cr);
              }
           }

        p1++;
      }
  }
//It might draw better than a linear gradient but the output gets rasterized.
static void chart_data_draw_cubes_grad_mesh(struct chart_data *cd, cairo_t *cr, gdouble chart_z)
  {
    /*
        These cubes have been sorted at this point so need to calculate the distance
        from the top and bottom square.
    */
    gint i=0;
    struct cube3d *p1=&g_array_index(cd->d_t, struct cube3d, 0);
    struct cube3d *p2=NULL;
    gdouble distance=0.0;
    gdouble max_val=(cd->max_z);
    gdouble dm=0.0;
    cairo_pattern_t *pattern1=NULL;
    gdouble a[2]={0.0, 0.0};
    gdouble b[2]={0.0, 0.0};
    gdouble z=0; 
    gdouble r1=0.0;
    gdouble g1=0.0;
    gdouble b1=0.0; 
   
    for(i=0;i<(cd->d_t->len);i++)
      {
        //Bottom of cube.
        cairo_set_source_rgba(cr, b_c[0], b_c[1], b_c[2], b_c[3]);
        cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
        cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
        cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
        cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        /*
          Get the original height of the cube to standardize the perspective. Remember the
          tranformed cubes are sorted so get the cube with the id.
        */
        p2=&g_array_index(cd->d, struct cube3d, p1->id);
        distance=(p2->c5).z;      
        dm=distance/max_val;
        get_gradient_color_bt(dm, &r1, &g1, &b1);

        a[0]=(p1->c2).x-(p1->c1).x;
        a[1]=(p1->c2).y-(p1->c1).y;
        b[0]=(p1->c5).x-(p1->c1).x;
        b[1]=(p1->c5).y-(p1->c1).y;
        z=a[0]*b[1]-a[1]*b[0];
        if(z>0.0)
          {
            pattern1=cairo_pattern_create_mesh();
            cairo_mesh_pattern_begin_patch(pattern1);
            cairo_mesh_pattern_move_to(pattern1, (p1->c1).x, (p1->c1).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c2).x, (p1->c2).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c6).x, (p1->c6).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c5).x, (p1->c5).y);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, r1, g1, b1, t_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, r1, g1, b1, t_c[3]);
            cairo_mesh_pattern_end_patch(pattern1);
            cairo_set_source(cr, pattern1);
            cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
            cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
            cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
            cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        a[0]=(p1->c3).x-(p1->c2).x;
        a[1]=(p1->c3).y-(p1->c2).y;
        b[0]=(p1->c6).x-(p1->c2).x;
        b[1]=(p1->c6).y-(p1->c2).y;
        z=a[0]*b[1]-a[1]*b[0];       
        if(z>0.0)
          {
            pattern1=cairo_pattern_create_mesh();
            cairo_mesh_pattern_begin_patch(pattern1);
            cairo_mesh_pattern_move_to(pattern1, (p1->c2).x, (p1->c2).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c3).x, (p1->c3).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c7).x, (p1->c7).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c6).x, (p1->c6).y);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, r1, g1, b1, t_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, r1, g1, b1, t_c[3]);
            cairo_mesh_pattern_end_patch(pattern1);
            cairo_set_source(cr, pattern1);
            cairo_move_to(cr, (p1->c2).x, (p1->c2).y);
            cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
            cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
            cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        a[0]=(p1->c4).x-(p1->c3).x;
        a[1]=(p1->c4).y-(p1->c3).y;
        b[0]=(p1->c7).x-(p1->c3).x;
        b[1]=(p1->c7).y-(p1->c3).y;
        z=a[0]*b[1]-a[1]*b[0];
        if(z>0.0)
          {
            pattern1=cairo_pattern_create_mesh();
            cairo_mesh_pattern_begin_patch(pattern1);
            cairo_mesh_pattern_move_to(pattern1, (p1->c3).x, (p1->c3).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c4).x, (p1->c4).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c8).x, (p1->c8).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c7).x, (p1->c7).y);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, r1, g1, b1, t_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, r1, g1, b1, t_c[3]);
            cairo_mesh_pattern_end_patch(pattern1);
            cairo_set_source(cr, pattern1);
            cairo_move_to(cr, (p1->c3).x, (p1->c3).y);
            cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
            cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
            cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        a[0]=(p1->c1).x-(p1->c4).x;
        a[1]=(p1->c1).y-(p1->c4).y;
        b[0]=(p1->c8).x-(p1->c4).x;
        b[1]=(p1->c8).y-(p1->c4).y;
        z=a[0]*b[1]-a[1]*b[0];
        if(z>0.0)
          {
            pattern1=cairo_pattern_create_mesh();
            cairo_mesh_pattern_begin_patch(pattern1);
            cairo_mesh_pattern_move_to(pattern1, (p1->c4).x, (p1->c4).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c1).x, (p1->c1).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c5).x, (p1->c5).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c8).x, (p1->c8).y);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, r1, g1, b1, t_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, r1, g1, b1, t_c[3]);
            cairo_mesh_pattern_end_patch(pattern1);
            cairo_set_source(cr, pattern1);
            cairo_move_to(cr, (p1->c4).x, (p1->c4).y);
            cairo_line_to(cr, (p1->c1).x, (p1->c1).y);
            cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
            cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        //Top of cube
        a[0]=(p1->c6).x-(p1->c5).x;
        a[1]=(p1->c6).y-(p1->c5).y;
        b[0]=(p1->c8).x-(p1->c5).x;
        b[1]=(p1->c8).y-(p1->c5).y;
        z=a[0]*b[1]-a[1]*b[0];
        if(z>0.0)
          {
            cairo_set_source_rgba(cr, r1, g1, b1, t_c[3]); 
            cairo_move_to(cr, (p1->c5).x, (p1->c5).y);
            cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
            cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
            cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
            cairo_close_path(cr);
            if(chart_z>0.0)
              {
                cairo_fill_preserve(cr);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_set_line_width(cr, 4.0);
                cairo_save(cr);
                cairo_clip_preserve(cr); 
                cairo_stroke(cr);
                cairo_restore(cr);
              }
            else
              {
                cairo_fill(cr);
              }
           }

        p1++;
      }
  }
static void chart_data_draw_cubes_grad2(struct chart_data *cd, cairo_t *cr, gdouble chart_z)
  {
    //Try a 3 color gradient.
    gint i=0;
    struct cube3d *p1=&g_array_index(cd->d_t, struct cube3d, 0);
    struct cube3d *p2=NULL;
    gdouble distance=0.0;
    gdouble max_val=(cd->max_z);
    gdouble dm=0.0;
    cairo_pattern_t *pattern1=NULL;
    gdouble a[2]={0.0, 0.0};
    gdouble b[2]={0.0, 0.0};
    gdouble z=0; 
    gdouble r1=0.0;
    gdouble g1=0.0;
    gdouble b1=0.0;
    gdouble m_x=0.0;
    gdouble m_y=0.0;
    gdouble out_x=0.0;
    gdouble out_y=0.0;

    for(i=0;i<(cd->d_t->len);i++)
      {
        //Bottom of cube.
        cairo_set_source_rgba(cr, b_c[0], b_c[1], b_c[2], b_c[3]);
        cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
        cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
        cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
        cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        /*
          Get the original height of the cube to standardize the perspective. Remember the
          tranformed cubes are sorted so get the cube with the id.
        */
        p2=&g_array_index(cd->d, struct cube3d, p1->id);
        distance=(p2->c5).z;     
        dm=distance/max_val;
        if(dm<=0.5) get_gradient_color_bm(2.0*dm, &r1, &g1, &b1);
        else get_gradient_color_mt(dm, &r1, &g1, &b1);

        a[0]=(p1->c2).x-(p1->c1).x;
        a[1]=(p1->c2).y-(p1->c1).y;
        b[0]=(p1->c5).x-(p1->c1).x;
        b[1]=(p1->c5).y-(p1->c1).y;
        z=a[0]*b[1]-a[1]*b[0];
        if(z>0.0)
          {
            m_x=-1.0*((p1->c1).x-(p1->c2).x);
            m_y=(p1->c1).y-(p1->c2).y; 
            out_x=(p1->c6).x;
            out_y=(p1->c6).y;
            intersection((p1->c5).x, (p1->c5).y, (p1->c6).x, (p1->c6).y, (p1->c2).x, (p1->c2).y, (p1->c2).x+m_y, (p1->c2).y+m_x, &out_x, &out_y);
            pattern1=cairo_pattern_create_linear((p1->c2).x, (p1->c2).y, out_x, out_y); 
            if(dm<=0.5)
              { 
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, b_c[0], b_c[1], b_c[2], b_c[3]); 
                cairo_pattern_add_color_stop_rgba(pattern1, 1.0, r1, g1, b1, m_c[3]);
              }
            else
              {
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, b_c[0], b_c[1], b_c[2], b_c[3]);  
                cairo_pattern_add_color_stop_rgba(pattern1, 0.5, m_c[0], m_c[1], m_c[2], m_c[3]); 
                cairo_pattern_add_color_stop_rgba(pattern1, dm, r1, g1, b1, t_c[3]);
              } 
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
            cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
            cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
            cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        a[0]=(p1->c3).x-(p1->c2).x;
        a[1]=(p1->c3).y-(p1->c2).y;
        b[0]=(p1->c6).x-(p1->c2).x;
        b[1]=(p1->c6).y-(p1->c2).y;
        z=a[0]*b[1]-a[1]*b[0];       
        if(z>0.0)
          {
            m_x=-1.0*((p1->c2).x-(p1->c3).x);
            m_y=(p1->c2).y-(p1->c3).y; 
            out_x=(p1->c7).x;
            out_y=(p1->c7).y;
            intersection((p1->c7).x, (p1->c7).y, (p1->c6).x, (p1->c6).y, (p1->c3).x, (p1->c3).y, (p1->c3).x+m_y, (p1->c3).y+m_x, &out_x, &out_y);
            pattern1=cairo_pattern_create_linear((p1->c3).x, (p1->c3).y, out_x, out_y); 
            if(dm<=0.5)
              { 
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, b_c[0], b_c[1], b_c[2], b_c[3]); 
                cairo_pattern_add_color_stop_rgba(pattern1, 1.0, r1, g1, b1, m_c[3]);
              }
            else
              {
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, b_c[0], b_c[1], b_c[2], b_c[3]);  
                cairo_pattern_add_color_stop_rgba(pattern1, 0.5, m_c[0], m_c[1], m_c[2], m_c[3]); 
                cairo_pattern_add_color_stop_rgba(pattern1, dm, r1, g1, b1, t_c[3]);
              }  
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->c2).x, (p1->c2).y);
            cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
            cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
            cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        a[0]=(p1->c4).x-(p1->c3).x;
        a[1]=(p1->c4).y-(p1->c3).y;
        b[0]=(p1->c7).x-(p1->c3).x;
        b[1]=(p1->c7).y-(p1->c3).y;
        z=a[0]*b[1]-a[1]*b[0];
        if(z>0.0)
          {
            m_x=-1.0*((p1->c3).x-(p1->c4).x);
            m_y=(p1->c3).y-(p1->c4).y;
            out_x=(p1->c8).x;
            out_y=(p1->c8).y; 
            intersection((p1->c7).x, (p1->c7).y, (p1->c8).x, (p1->c8).y, (p1->c4).x, (p1->c4).y, (p1->c4).x+m_y, (p1->c4).y+m_x, &out_x, &out_y);
            pattern1=cairo_pattern_create_linear((p1->c4).x, (p1->c4).y, out_x, out_y); 
            if(dm<=0.5)
              { 
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, b_c[0], b_c[1], b_c[2], b_c[3]); 
                cairo_pattern_add_color_stop_rgba(pattern1, 1.0, r1, g1, b1, m_c[3]);
              }
            else
              {
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, b_c[0], b_c[1], b_c[2], b_c[3]);  
                cairo_pattern_add_color_stop_rgba(pattern1, 0.5, m_c[0], m_c[1], m_c[2], m_c[3]); 
                cairo_pattern_add_color_stop_rgba(pattern1, dm, r1, g1, b1, t_c[3]);
              }   
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->c3).x, (p1->c3).y);
            cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
            cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
            cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        a[0]=(p1->c1).x-(p1->c4).x;
        a[1]=(p1->c1).y-(p1->c4).y;
        b[0]=(p1->c8).x-(p1->c4).x;
        b[1]=(p1->c8).y-(p1->c4).y;
        z=a[0]*b[1]-a[1]*b[0];
        if(z>0.0)
          {
            m_x=-1.0*((p1->c4).x-(p1->c1).x);
            m_y=(p1->c4).y-(p1->c1).y; 
            out_x=(p1->c5).x;
            out_y=(p1->c5).y;
            intersection((p1->c5).x, (p1->c5).y, (p1->c8).x, (p1->c8).y, (p1->c1).x, (p1->c1).y, (p1->c1).x+m_y, (p1->c1).y+m_x, &out_x, &out_y);
            pattern1=cairo_pattern_create_linear((p1->c1).x, (p1->c1).y, out_x, out_y); 
            if(dm<=0.5)
              { 
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, b_c[0], b_c[1], b_c[2], b_c[3]); 
                cairo_pattern_add_color_stop_rgba(pattern1, 1.0, r1, g1, b1, m_c[3]);
              }
            else
              {
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, b_c[0], b_c[1], b_c[2], b_c[3]);  
                cairo_pattern_add_color_stop_rgba(pattern1, 0.5, m_c[0], m_c[1], m_c[2], m_c[3]); 
                cairo_pattern_add_color_stop_rgba(pattern1, dm, r1, g1, b1, t_c[3]);
              }  
            cairo_set_source(cr, pattern1); 
            cairo_move_to(cr, (p1->c4).x, (p1->c4).y);
            cairo_line_to(cr, (p1->c1).x, (p1->c1).y);
            cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
            cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
          }

        //Top of cube
        a[0]=(p1->c6).x-(p1->c5).x;
        a[1]=(p1->c6).y-(p1->c5).y;
        b[0]=(p1->c8).x-(p1->c5).x;
        b[1]=(p1->c8).y-(p1->c5).y;
        z=a[0]*b[1]-a[1]*b[0];
        if(z>0.0)
          {
            if(dm<=0.5) cairo_set_source_rgba(cr, r1, g1, b1, m_c[3]);
            else cairo_set_source_rgba(cr, r1, g1, b1, t_c[3]);
            cairo_move_to(cr, (p1->c5).x, (p1->c5).y);
            cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
            cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
            cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_set_line_width(cr, 4.0);
            cairo_save(cr);
            cairo_clip_preserve(cr); 
            cairo_stroke(cr);
            cairo_restore(cr);
          }

        p1++;
      }
  }
static void chart_data_draw_cubes_mesh_grad2(struct chart_data *cd, cairo_t *cr, gdouble chart_z)
  {
    //Try a 3 color gradient.
    gint i=0;
    struct cube3d *p1=&g_array_index(cd->d_t, struct cube3d, 0);
    struct cube3d *p2=NULL;
    gdouble distance=0.0;
    gdouble max_val=(cd->max_z);
    gdouble dm=0.0;
    cairo_pattern_t *pattern1=NULL;
    gdouble a[2]={0.0, 0.0};
    gdouble b[2]={0.0, 0.0};
    gdouble z=0;
    //Need two slopes for perspective.  
    gdouble m1_x=0.0;
    gdouble m1_y=0.0;
    gdouble m2_x=0.0;
    gdouble m2_y=0.0;
    gdouble dm_x1=0.0;
    gdouble dm_y1=0.0;
    gdouble dm_x2=0.0;
    gdouble dm_y2=0.0; 
    gdouble em_x1=0.0;
    gdouble em_y1=0.0;
    gdouble em_x2=0.0;
    gdouble em_y2=0.0; 
    gdouble r1=0.0;
    gdouble g1=0.0;
    gdouble b1=0.0; 
    gdouble r2=0.0;
    gdouble g2=0.0;
    gdouble b2=0.0;         

    for(i=0;i<(cd->d_t->len);i++)
      {
        //Bottom of cube.
        cairo_set_source_rgba(cr, b_c[0], b_c[1], b_c[2], b_c[3]);
        cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
        cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
        cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
        cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
        cairo_close_path(cr);
        cairo_fill(cr);

        /*
          Get the original height of the cube to standardize the perspective. Remember the
          tranformed cubes are sorted so get the cube with the id.
        */
        p2=&g_array_index(cd->d, struct cube3d, p1->id);
        distance=(p2->c5).z; 
        dm=distance/max_val;
        get_gradient_color_bm(2.0*dm, &r1, &g1, &b1);
        get_gradient_color_mt(dm, &r2, &g2, &b2);
       
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);

        //Move gradients along side lines.
        m1_x=((p1->c5).x-(p1->c1).x);
        m1_y=((p1->c5).y-(p1->c1).y);
        m2_x=((p1->c6).x-(p1->c2).x);
        m2_y=((p1->c6).y-(p1->c2).y);
        if(dm<=0.5)
          {
            dm_x1=m1_x*(0.5-dm);
            dm_y1=m1_y*(0.5-dm);
            dm_x2=m2_x*(0.5-dm);
            dm_y2=m2_y*(0.5-dm);
          }
        else
          {
            dm_x1=-m1_x*(dm-0.5);
            dm_y1=-m1_y*(dm-0.5);
            dm_x2=-m2_x*(dm-0.5);
            dm_y2=-m2_y*(dm-0.5);
            //Second gradient
            em_x1=m1_x*(1.0-(dm-0.5));
            em_y1=m1_y*(1.0-(dm-0.5));
            em_x2=m2_x*(1.0-(dm-0.5));
            em_y2=m2_y*(1.0-(dm-0.5));
          }

        a[0]=(p1->c2).x-(p1->c1).x;
        a[1]=(p1->c2).y-(p1->c1).y;
        b[0]=(p1->c5).x-(p1->c1).x;
        b[1]=(p1->c5).y-(p1->c1).y;
        z=a[0]*b[1]-a[1]*b[0];
        if(z>0.0)
          {
            pattern1=cairo_pattern_create_mesh();
            cairo_mesh_pattern_begin_patch(pattern1);
            cairo_mesh_pattern_move_to(pattern1, (p1->c1).x, (p1->c1).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c2).x, (p1->c2).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c6).x+dm_x2, (p1->c6).y+dm_y2);
            cairo_mesh_pattern_line_to(pattern1, (p1->c5).x+dm_x1, (p1->c5).y+dm_y1);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, r1, g1, b1, t_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, r1, g1, b1, t_c[3]);
            cairo_mesh_pattern_end_patch(pattern1);
            cairo_set_source(cr, pattern1);
            cairo_move_to(cr, (p1->c1).x, (p1->c1).y);
            cairo_line_to(cr, (p1->c2).x, (p1->c2).y);
            cairo_line_to(cr, (p1->c6).x+dm_x2, (p1->c6).y+dm_y2);
            cairo_line_to(cr, (p1->c5).x+dm_x1, (p1->c5).y+dm_y1);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);

            if(dm>0.5)
              {
                pattern1=cairo_pattern_create_mesh();
                cairo_mesh_pattern_begin_patch(pattern1);
                cairo_mesh_pattern_move_to(pattern1, (p1->c1).x+em_x1, (p1->c1).y+em_y1);
                cairo_mesh_pattern_line_to(pattern1, (p1->c2).x+em_x2, (p1->c2).y+em_y2);
                cairo_mesh_pattern_line_to(pattern1, (p1->c6).x, (p1->c6).y);
                cairo_mesh_pattern_line_to(pattern1, (p1->c5).x, (p1->c5).y);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, m_c[0], m_c[1], m_c[2], m_c[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, m_c[0], m_c[1], m_c[2], m_c[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, r2, g2, b2, t_c[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, r2, g2, b2, t_c[3]);
                cairo_mesh_pattern_end_patch(pattern1);
                cairo_set_source(cr, pattern1);
                cairo_move_to(cr, (p1->c1).x+em_x1, (p1->c1).y+em_y1);
                cairo_line_to(cr, (p1->c2).x+em_x2, (p1->c2).y+em_y2);
                cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
                cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
                cairo_close_path(cr);
                cairo_fill(cr);
                cairo_pattern_destroy(pattern1);
              }
          }

        m1_x=(p1->c6).x-(p1->c2).x;
        m1_y=(p1->c6).y-(p1->c2).y;
        m2_x=(p1->c7).x-(p1->c3).x;
        m2_y=(p1->c7).y-(p1->c3).y;
        if(dm<=0.5)
          {
            dm_x1=m1_x*(0.5-dm);
            dm_y1=m1_y*(0.5-dm);
            dm_x2=m2_x*(0.5-dm);
            dm_y2=m2_y*(0.5-dm);
          }
        else
          {
            dm_x1=-m1_x*(dm-0.5);
            dm_y1=-m1_y*(dm-0.5);
            dm_x2=-m2_x*(dm-0.5);
            dm_y2=-m2_y*(dm-0.5);
            em_x1=m1_x*(1.0-(dm-0.5));
            em_y1=m1_y*(1.0-(dm-0.5));
            em_x2=m2_x*(1.0-(dm-0.5));
            em_y2=m2_y*(1.0-(dm-0.5));
          }

        a[0]=(p1->c3).x-(p1->c2).x;
        a[1]=(p1->c3).y-(p1->c2).y;
        b[0]=(p1->c6).x-(p1->c2).x;
        b[1]=(p1->c6).y-(p1->c2).y;
        z=a[0]*b[1]-a[1]*b[0];       
        if(z>0.0)
          {
            pattern1=cairo_pattern_create_mesh();
            cairo_mesh_pattern_begin_patch(pattern1);
            cairo_mesh_pattern_move_to(pattern1, (p1->c2).x, (p1->c2).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c3).x, (p1->c3).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c7).x+dm_x2, (p1->c7).y+dm_y2);
            cairo_mesh_pattern_line_to(pattern1, (p1->c6).x+dm_x1, (p1->c6).y+dm_y1);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, r1, g1, b1, t_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, r1, g1, b1, t_c[3]);
            cairo_mesh_pattern_end_patch(pattern1);
            cairo_set_source(cr, pattern1);
            cairo_move_to(cr, (p1->c2).x, (p1->c2).y);
            cairo_line_to(cr, (p1->c3).x, (p1->c3).y);
            cairo_line_to(cr, (p1->c7).x+dm_x2, (p1->c7).y+dm_y2);
            cairo_line_to(cr, (p1->c6).x+dm_x1, (p1->c6).y+dm_y1);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);

            if(dm>0.5)
              {
                pattern1=cairo_pattern_create_mesh();
                cairo_mesh_pattern_begin_patch(pattern1);
                cairo_mesh_pattern_move_to(pattern1, (p1->c2).x+em_x1, (p1->c2).y+em_y1);
                cairo_mesh_pattern_line_to(pattern1, (p1->c3).x+em_x2, (p1->c3).y+em_y2);
                cairo_mesh_pattern_line_to(pattern1, (p1->c7).x, (p1->c7).y);
                cairo_mesh_pattern_line_to(pattern1, (p1->c6).x, (p1->c6).y);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, m_c[0], m_c[1], m_c[2], m_c[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, m_c[0], m_c[1], m_c[2], m_c[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, r2, g2, b2, t_c[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, r2, g2, b2, t_c[3]);
                cairo_mesh_pattern_end_patch(pattern1);
                cairo_set_source(cr, pattern1);
                cairo_move_to(cr, (p1->c2).x+em_x1, (p1->c2).y+em_y1);
                cairo_line_to(cr, (p1->c3).x+em_x2, (p1->c3).y+em_y2);
                cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
                cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
                cairo_close_path(cr);
                cairo_fill(cr);
                cairo_pattern_destroy(pattern1);
              }
          }

        m1_x=(p1->c7).x-(p1->c3).x;
        m1_y=(p1->c7).y-(p1->c3).y;
        m2_x=(p1->c8).x-(p1->c4).x;
        m2_y=(p1->c8).y-(p1->c4).y;
        if(dm<=0.5)
          {
            dm_x1=m1_x*(0.5-dm);
            dm_y1=m1_y*(0.5-dm);
            dm_x2=m2_x*(0.5-dm);
            dm_y2=m2_y*(0.5-dm);
          }
        else
          {
            dm_x1=-m1_x*(dm-0.5);
            dm_y1=-m1_y*(dm-0.5);
            dm_x2=-m2_x*(dm-0.5);
            dm_y2=-m2_y*(dm-0.5);
            em_x1=m1_x*(1.0-(dm-0.5));
            em_y1=m1_y*(1.0-(dm-0.5));
            em_x2=m2_x*(1.0-(dm-0.5));
            em_y2=m2_y*(1.0-(dm-0.5));
          }

        a[0]=(p1->c4).x-(p1->c3).x;
        a[1]=(p1->c4).y-(p1->c3).y;
        b[0]=(p1->c7).x-(p1->c3).x;
        b[1]=(p1->c7).y-(p1->c3).y;
        z=a[0]*b[1]-a[1]*b[0];
        if(z>0.0)
          {
            pattern1=cairo_pattern_create_mesh();
            cairo_mesh_pattern_begin_patch(pattern1);
            cairo_mesh_pattern_move_to(pattern1, (p1->c3).x, (p1->c3).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c4).x, (p1->c4).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c8).x+dm_x2, (p1->c8).y+dm_y2);
            cairo_mesh_pattern_line_to(pattern1, (p1->c7).x+dm_x1, (p1->c7).y+dm_y1);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, r1, g1, b1, t_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, r1, g1, b1, t_c[3]);
            cairo_mesh_pattern_end_patch(pattern1);
            cairo_set_source(cr, pattern1);
            cairo_move_to(cr, (p1->c3).x, (p1->c3).y);
            cairo_line_to(cr, (p1->c4).x, (p1->c4).y);
            cairo_line_to(cr, (p1->c8).x+dm_x2, (p1->c8).y+dm_y2);
            cairo_line_to(cr, (p1->c7).x+dm_x1, (p1->c7).y+dm_y1);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);

            if(dm>0.5)
              {
                pattern1=cairo_pattern_create_mesh();
                cairo_mesh_pattern_begin_patch(pattern1);
                cairo_mesh_pattern_move_to(pattern1, (p1->c3).x+em_x1, (p1->c3).y+em_y1);
                cairo_mesh_pattern_line_to(pattern1, (p1->c4).x+em_x2, (p1->c4).y+em_y2);
                cairo_mesh_pattern_line_to(pattern1, (p1->c8).x, (p1->c8).y);
                cairo_mesh_pattern_line_to(pattern1, (p1->c7).x, (p1->c7).y);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, m_c[0], m_c[1], m_c[2], m_c[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, m_c[0], m_c[1], m_c[2], m_c[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, r2, g2, b2, t_c[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, r2, g2, b2, t_c[3]);
                cairo_mesh_pattern_end_patch(pattern1);
                cairo_set_source(cr, pattern1);
                cairo_move_to(cr, (p1->c3).x+em_x1, (p1->c3).y+em_y1);
                cairo_line_to(cr, (p1->c4).x+em_x2, (p1->c4).y+em_y2);
                cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
                cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
                cairo_close_path(cr);
                cairo_fill(cr);
                cairo_pattern_destroy(pattern1);
              }
          }

        m1_x=(p1->c5).x-(p1->c1).x;
        m1_y=(p1->c5).y-(p1->c1).y;
        m2_x=(p1->c8).x-(p1->c4).x;
        m2_y=(p1->c8).y-(p1->c4).y;
        if(dm<=0.5)
          {
            dm_x1=m1_x*(0.5-dm);
            dm_y1=m1_y*(0.5-dm);
            dm_x2=m2_x*(0.5-dm);
            dm_y2=m2_y*(0.5-dm);
          }
        else
          {
            dm_x1=-m1_x*(dm-0.5);
            dm_y1=-m1_y*(dm-0.5);
            dm_x2=-m2_x*(dm-0.5);
            dm_y2=-m2_y*(dm-0.5);
            em_x1=m1_x*(1.0-(dm-0.5));
            em_y1=m1_y*(1.0-(dm-0.5));
            em_x2=m2_x*(1.0-(dm-0.5));
            em_y2=m2_y*(1.0-(dm-0.5));
          }

        a[0]=(p1->c1).x-(p1->c4).x;
        a[1]=(p1->c1).y-(p1->c4).y;
        b[0]=(p1->c8).x-(p1->c4).x;
        b[1]=(p1->c8).y-(p1->c4).y;
        z=a[0]*b[1]-a[1]*b[0];
        if(z>0.0)
          {
            pattern1=cairo_pattern_create_mesh();
            cairo_mesh_pattern_begin_patch(pattern1);
            cairo_mesh_pattern_move_to(pattern1, (p1->c4).x, (p1->c4).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c1).x, (p1->c1).y);
            cairo_mesh_pattern_line_to(pattern1, (p1->c5).x+dm_x1, (p1->c5).y+dm_y1);
            cairo_mesh_pattern_line_to(pattern1, (p1->c8).x+dm_x2, (p1->c8).y+dm_y2);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, b_c[0], b_c[1], b_c[2], b_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, r1, g1, b1, t_c[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, r1, g1, b1, t_c[3]);
            cairo_mesh_pattern_end_patch(pattern1);
            cairo_set_source(cr, pattern1);
            cairo_move_to(cr, (p1->c4).x, (p1->c4).y);
            cairo_line_to(cr, (p1->c1).x, (p1->c1).y);
            cairo_line_to(cr, (p1->c5).x+dm_x1, (p1->c5).y+dm_y1);
            cairo_line_to(cr, (p1->c8).x+dm_x2, (p1->c8).y+dm_y2);
            cairo_close_path(cr);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
     
            if(dm>0.5)
              {
                pattern1=cairo_pattern_create_mesh();
                cairo_mesh_pattern_begin_patch(pattern1);
                cairo_mesh_pattern_move_to(pattern1, (p1->c4).x+em_x2, (p1->c4).y+em_y2);
                cairo_mesh_pattern_line_to(pattern1, (p1->c1).x+em_x1, (p1->c1).y+em_y1);
                cairo_mesh_pattern_line_to(pattern1, (p1->c5).x, (p1->c5).y);
                cairo_mesh_pattern_line_to(pattern1, (p1->c8).x, (p1->c8).y);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, m_c[0], m_c[1], m_c[2], m_c[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, m_c[0], m_c[1], m_c[2], m_c[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, r2, g2, b2, t_c[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, r2, g2, b2, t_c[3]);
                cairo_mesh_pattern_end_patch(pattern1);
                cairo_set_source(cr, pattern1);
                cairo_move_to(cr, (p1->c4).x+em_x2, (p1->c4).y+em_y2);
                cairo_line_to(cr, (p1->c1).x+em_x1, (p1->c1).y+em_y1);
                cairo_line_to(cr, (p1->c5).x, (p1->c5).y);
                cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
                cairo_close_path(cr);
                cairo_fill(cr);
                cairo_pattern_destroy(pattern1);
              }
          }

        cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT);

        //Top of cube
        a[0]=(p1->c6).x-(p1->c5).x;
        a[1]=(p1->c6).y-(p1->c5).y;
        b[0]=(p1->c8).x-(p1->c5).x;
        b[1]=(p1->c8).y-(p1->c5).y;
        z=a[0]*b[1]-a[1]*b[0];
        if(z>0.0)
          {
            if(dm<0.5) cairo_set_source_rgba(cr, r1, g1, b1, m_c[3]);
            else cairo_set_source_rgba(cr, r2, g2, b2, t_c[3]);
            cairo_move_to(cr, (p1->c5).x, (p1->c5).y);
            cairo_line_to(cr, (p1->c6).x, (p1->c6).y);
            cairo_line_to(cr, (p1->c7).x, (p1->c7).y);
            cairo_line_to(cr, (p1->c8).x, (p1->c8).y);
            cairo_close_path(cr);            
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_set_line_width(cr, 4.0);
            cairo_save(cr);
            cairo_clip_preserve(cr); 
            cairo_stroke(cr);
            cairo_restore(cr);
           }

        p1++;
      }
  }
static void chart_data_draw_lines(struct chart_data *cd, cairo_t *cr)
  {
    gint i=0;
    gint j=0;
    gint len=0;
    struct chart_point3d *p1=&g_array_index(cd->d_t, struct chart_point3d, 0);
    struct chart_point3d *p2=NULL;

    for(i=0;i<(cd->d_t->len);i++)
      {
        if(p1->move_id==C_LINE_TO_STROKE||p1->move_id==C_CURVE_TO_STROKE)
          {
            GArray *temp=g_array_new(FALSE, FALSE, sizeof(struct chart_point3d));
            g_array_append_vals(temp, p1-len, len+1);
            p2=&g_array_index(temp, struct chart_point3d, 0);
            cairo_move_to(cr, p2->x, p2->y);
            p2++;
            for(j=1;j<(temp->len);j++)
              {       
                cairo_line_to(cr, p2->x, p2->y);  
                p2++;
              }
            cairo_stroke(cr);
            if(temp!=NULL) g_array_free(temp, TRUE);
            len=0;
            p1++;
          }
        else 
          {
            p1++;len++;
          }
      }
  }
static void chart_data_draw_smooth(struct chart_data *cd, cairo_t *cr)
  {
    gint i=0;
    gint j=0;
    gint len=0;
    struct histo_controls2d *c1=NULL;
    struct chart_point3d *p1=&g_array_index(cd->d_t, struct chart_point3d, 0);
    struct chart_point3d *p2=NULL;

    for(i=0;i<(cd->d_t->len);i++)
      {
        if(p1->move_id==C_LINE_TO_STROKE||p1->move_id==C_CURVE_TO_STROKE)
          {
            GArray *temp=g_array_new(FALSE, FALSE, sizeof(struct chart_point3d));
            g_array_append_vals(temp, p1-len, len+1);
            GArray *control1=control_points_from_coords_3d(temp);
            c1=&g_array_index(control1, struct histo_controls2d, 0);
            p2=&g_array_index(temp, struct chart_point3d, 0);
            cairo_move_to(cr, p2->x, p2->y);
            for(j=1;j<(temp->len);j++)
              {
                p2++;
                cairo_curve_to(cr, c1->x1, c1->y1, c1->x2, c1->y2, p2->x, p2->y);
                c1++; 
              }
            cairo_stroke(cr);
            if(temp!=NULL) g_array_free(temp, TRUE);
            if(control1!=NULL) g_array_free(control1, TRUE);
            len=0;
            p1++;
          }
        else 
          {
            p1++;len++;
          }
      }
  }
static void chart_data_draw_points(struct chart_data *cd, cairo_t *cr)
  {
    gint i=0;
    gint j=0;
    gint len=0;
    struct chart_point3d *p1=&g_array_index(cd->d_t, struct chart_point3d, 0);
    struct chart_point3d *p2=NULL;

    for(i=0;i<(cd->d_t->len);i++)
      {
        if(p1->move_id==C_LINE_TO_STROKE||p1->move_id==C_CURVE_TO_STROKE)
          {
            GArray *temp=g_array_new(FALSE, FALSE, sizeof(struct chart_point3d));
            g_array_append_vals(temp, p1-len, len+1);
            p2=&g_array_index(temp, struct chart_point3d, 0);
            for(j=0;j<(temp->len);j++)
              {       
                cairo_move_to(cr, p2->x, p2->y);
                cairo_line_to(cr, p2->x, p2->y);
                cairo_stroke(cr); 
                p2++;
              }
            if(temp!=NULL) g_array_free(temp, TRUE);
            len=0;
            p1++;
          }
        else 
          {
            p1++;len++;
          }
      }
  }
static void chart_data_draw_cylinders(struct chart_data_aligned *cd, cairo_t *cr, gdouble chart_z)
  {
    gint i=0;
    gint j=0;
    gint cylinders=(cd->len)/((cd->ring_size)*2.0);
    struct histo_arc *p1=(struct histo_arc*)(cd->d_t);
    struct histo_arc *p2=(struct histo_arc*)(cd->d_t);

    for(i=0;i<cylinders;i++)
      { 
        //Bottom of cylinder.
        cairo_set_source_rgba(cr, (p1->cl).r, (p1->cl).g, (p1->cl).b, (p1->cl).a);
        cairo_move_to(cr, (p1->a1).x, (p1->a1).y); 
        for(j=0;j<(cd->ring_size);j++)
          { 
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            p1++;
          }
        cairo_fill(cr);

        //The sides of the cylinder.
        cairo_set_source_rgba(cr, (p1->cl).r, (p1->cl).g, (p1->cl).b, (p1->cl).a);
        p2=p1-(cd->ring_size); 
        for(j=0;j<(cd->ring_size);j++)
          { 
            cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
            cairo_line_to(cr, (p2->a1).x, (p2->a1).y);
            cairo_curve_to(cr, (p2->a2).x, (p2->a2).y, (p2->a3).x, (p2->a3).y, (p2->a4).x, (p2->a4).y);
            cairo_line_to(cr, (p1->a4).x, (p1->a4).y);
            cairo_curve_to(cr, (p1->a3).x, (p1->a3).y, (p1->a2).x, (p1->a2).y, (p1->a1).x, (p1->a1).y);
            cairo_fill(cr);
            p1++;p2++;
          }
        
        //Top of cylinder.
        cairo_move_to(cr, (p2->a1).x, (p2->a1).y); 
        for(j=0;j<(cd->ring_size);j++)
          { 
            cairo_curve_to(cr, (p2->a2).x, (p2->a2).y, (p2->a3).x, (p2->a3).y, (p2->a4).x, (p2->a4).y);
            p2++;
          }
        if(chart_z>0.0)
          {
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_set_line_width(cr, 4.0);
            cairo_save(cr);
            cairo_clip_preserve(cr); 
            cairo_stroke(cr);
            cairo_restore(cr);
          }
        else
          {
            cairo_fill(cr);
          }
      }
  }
static void chart_data_draw_cylinders_grad(struct chart_data_aligned *cd, cairo_t *cr, gdouble chart_z)
  {
    gint i=0;
    gint j=0;
    gdouble dm=0.0;
    gdouble distance=0.0;
    gint cylinders=(cd->len)/((cd->ring_size)*2.0);
    struct histo_arc *p1=(struct histo_arc*)(cd->d_t);
    struct histo_arc *p2=(struct histo_arc*)(cd->d_t);
    struct histo_arc *p3=NULL;
    cairo_pattern_t *pattern1=NULL;
    gdouble a[2]={0.0, 0.0};
    gdouble b[2]={0.0, 0.0};
    gdouble z=0; 
    gdouble r1=0.0;
    gdouble g1=0.0;
    gdouble b1=0.0;
    gdouble m_x=0.0;
    gdouble m_y=0.0;
    gdouble out_x=0.0;
    gdouble out_y=0.0;

    for(i=0;i<cylinders;i++)
      { 
        //Bottom of cylinder.
        cairo_set_source_rgba(cr, b_c[0], b_c[1], b_c[2], b_c[3]);
        cairo_move_to(cr, (p1->a1).x, (p1->a1).y); 
        for(j=0;j<(cd->ring_size);j++)
          { 
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            p1++;
          }
        cairo_fill(cr);
       
        //The sides of the cylinder.
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        p2=p1-(cd->ring_size); 

        //Standardize gradient on unsorted cylinder because of perspective.
        p3=((struct histo_arc*)(cd->d))+((p1->id)*(cd->ring_size)*2+(cd->ring_size));
        distance=(p3->a1).z; 
        dm=distance/(cd->max_z);
        get_gradient_color_bt(dm, &r1, &g1, &b1);

        for(j=0;j<(cd->ring_size);j++)
          { 
            a[0]=(p1->a4).x-(p1->a1).x;
            a[1]=(p1->a4).y-(p1->a1).y;
            b[0]=(p2->a1).x-(p1->a1).x;
            b[1]=(p2->a1).y-(p1->a1).y;
            z=a[0]*b[1]-a[1]*b[0];
            if(z>0.0)
              {
                m_x=-1.0*((p1->a1).x-(p1->a4).x);
                m_y=(p1->a1).y-(p1->a4).y; 
                out_x=(p2->a4).x;
                out_y=(p2->a4).y;
                intersection((p2->a1).x, (p2->a1).y, (p2->a4).x, (p2->a4).y, (p1->a4).x, (p1->a4).y, (p1->a4).x+m_y, (p1->a4).y+m_x, &out_x, &out_y);
                pattern1=cairo_pattern_create_linear((p1->a4).x, (p1->a4).y, out_x, out_y); 
                cairo_pattern_add_color_stop_rgba(pattern1, 0.0, r1, g1, b1, t_c[3]);
                cairo_pattern_add_color_stop_rgba(pattern1, 1.0, b_c[0], b_c[1], b_c[2], b_c[3]); 
                cairo_set_source(cr, pattern1); 
                cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
                cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
                cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
                cairo_curve_to(cr, (p2->a3).x, (p2->a3).y, (p2->a2).x, (p2->a2).y, (p2->a1).x, (p2->a1).y);
                cairo_close_path(cr);
                cairo_fill(cr);
                cairo_pattern_destroy(pattern1);
              }

            p1++;p2++;
          }
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT);
        
        //Top of cylinder.
        if(chart_z>0)
          {
            cairo_set_source_rgba(cr, r1, g1, b1, t_c[3]);  
            cairo_move_to(cr, (p2->a1).x, (p2->a1).y); 
            for(j=0;j<(cd->ring_size);j++)
              { 
                cairo_curve_to(cr, (p2->a2).x, (p2->a2).y, (p2->a3).x, (p2->a3).y, (p2->a4).x, (p2->a4).y);
                p2++;
              }  
             cairo_fill_preserve(cr);
             cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
             cairo_set_line_width(cr, 4.0);
             cairo_save(cr);
             cairo_clip_preserve(cr); 
             cairo_stroke(cr);
             cairo_restore(cr);
          }
      }
  }
static void chart_data_draw_cylinders_grad_mesh(struct chart_data_aligned *cd, cairo_t *cr, gdouble chart_z)
  {
    gint i=0;
    gint j=0;
    gdouble dm=0.0;
    gdouble distance=0.0;
    gint cylinders=(cd->len)/((cd->ring_size)*2.0);
    struct histo_arc *p1=(struct histo_arc*)(cd->d_t);
    struct histo_arc *p2=(struct histo_arc*)(cd->d_t);
    struct histo_arc *p3=NULL;
    cairo_pattern_t *pattern1=NULL;
    gdouble a[2]={0.0, 0.0};
    gdouble b[2]={0.0, 0.0};
    gdouble z=0;
    gdouble r1=0.0;
    gdouble g1=0.0;
    gdouble b1=0.0;

    for(i=0;i<cylinders;i++)
      { 
        //Bottom of cylinder.
        cairo_set_source_rgba(cr, b_c[0], b_c[1], b_c[2], b_c[3]);
        cairo_move_to(cr, (p1->a1).x, (p1->a1).y); 
        for(j=0;j<(cd->ring_size);j++)
          { 
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            p1++;
          }
        cairo_fill(cr);

        //The sides of the cylinder.
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        p2=p1-(cd->ring_size);

        //Standardize gradient on unsorted cylinder because of perspective.
        p3=((struct histo_arc*)(cd->d))+((p1->id)*(cd->ring_size)*2+(cd->ring_size));
        distance=(p3->a1).z;  
        dm=distance/(cd->max_z);
        get_gradient_color_bt(dm, &r1, &g1, &b1);
        
        for(j=0;j<(cd->ring_size);j++)
          { 
            a[0]=(p1->a4).x-(p1->a1).x;
            a[1]=(p1->a4).y-(p1->a1).y;
            b[0]=(p2->a1).x-(p1->a1).x;
            b[1]=(p2->a1).y-(p1->a1).y;
            z=a[0]*b[1]-a[1]*b[0];
            if(z>0.0)
              {
                pattern1=cairo_pattern_create_mesh();
                cairo_mesh_pattern_begin_patch(pattern1);
                cairo_mesh_pattern_move_to(pattern1, (p1->a1).x, (p1->a1).y);
                cairo_mesh_pattern_curve_to(pattern1, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
                cairo_mesh_pattern_line_to(pattern1, (p2->a4).x, (p2->a4).y);
                cairo_mesh_pattern_curve_to(pattern1, (p2->a3).x, (p2->a3).y, (p2->a2).x, (p2->a2).y, (p2->a1).x, (p2->a1).y);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, r1, g1, b1, t_c[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, r1, g1, b1, t_c[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, b_c[0], b_c[1], b_c[2], b_c[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, b_c[0], b_c[1], b_c[2], b_c[3]);
                cairo_mesh_pattern_end_patch(pattern1);
                cairo_set_source(cr, pattern1);
                cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
                cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
                cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
                cairo_curve_to(cr, (p2->a3).x, (p2->a3).y, (p2->a2).x, (p2->a2).y, (p2->a1).x, (p2->a1).y);
                cairo_close_path(cr);
                cairo_fill(cr);
                cairo_pattern_destroy(pattern1);
              }
            p1++;p2++;
          }
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT);
        
        //Top of cylinder.
        if(chart_z>0)
          {
            cairo_set_source_rgba(cr, r1, g1, b1, t_c[3]); 
            cairo_move_to(cr, (p2->a1).x, (p2->a1).y); 
            for(j=0;j<(cd->ring_size);j++)
              { 
                cairo_curve_to(cr, (p2->a2).x, (p2->a2).y, (p2->a3).x, (p2->a3).y, (p2->a4).x, (p2->a4).y);
                p2++;
              }
             cairo_fill_preserve(cr);
             cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
             cairo_set_line_width(cr, 4.0);
             cairo_save(cr);
             cairo_clip_preserve(cr); 
             cairo_stroke(cr);
             cairo_restore(cr);
          }          
      }
  }
void histogram_chart_rotate_base(struct histogram_chart *hc, gdouble qrs[16])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0; 
    gdouble w=0.0; 
    gdouble limit=0.1; 
    struct rectangle3d *p1=&g_array_index(hc->base_t, struct rectangle3d, 0);

    for(i=0;i<(hc->base_t->len);i++)
      {
        cr1=(p1->b1).x;
        cr2=(p1->b1).y;
        cr3=(p1->b1).z;         
        (p1->b1).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->b1).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->b1).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        ((p1->b1).x)/=w;
        ((p1->b1).y)/=w;
        ((p1->b1).z)/=w;
        cr1=(p1->b2).x;
        cr2=(p1->b2).y;
        cr3=(p1->b2).z;         
        (p1->b2).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->b2).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->b2).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        ((p1->b2).x)/=w;
        ((p1->b2).y)/=w;
        ((p1->b2).z)/=w;
        cr1=(p1->b3).x;
        cr2=(p1->b3).y;
        cr3=(p1->b3).z;         
        (p1->b3).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->b3).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->b3).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        ((p1->b3).x)/=w;
        ((p1->b3).y)/=w;
        ((p1->b3).z)/=w;
        cr1=(p1->b4).x;
        cr2=(p1->b4).y;
        cr3=(p1->b4).z;         
        (p1->b4).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->b4).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->b4).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        ((p1->b4).x)/=w;
        ((p1->b4).y)/=w;
        ((p1->b4).z)/=w;
        p1++;
      }  

    //Rotate reference vector.
    cr1=hc->rx[0];
    cr2=hc->rx[1];
    cr3=hc->rx[2];          
    hc->rx_t[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
    hc->rx_t[1]=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6]);
    hc->rx_t[2]=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10]);

    cr1=hc->ry[0];
    cr2=hc->ry[1];
    cr3=hc->ry[2];          
    hc->ry_t[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
    hc->ry_t[1]=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6]);
    hc->ry_t[2]=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10]); 

    cr1=hc->rz[0];
    cr2=hc->rz[1];
    cr3=hc->rz[2];          
    hc->rz_t[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
    hc->rz_t[1]=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6]);
    hc->rz_t[2]=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10]); 

    //Rotate top points.
    cr1=hc->c1[0];
    cr2=hc->c1[1];
    cr3=hc->c1[2];          
    hc->c1_t[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
    hc->c1_t[1]=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
    hc->c1_t[2]=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
    w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
    if(w<limit) w=limit;
    (hc->c1_t[0])/=w;
    (hc->c1_t[1])/=w;
    (hc->c1_t[2])/=w;

    cr1=hc->c2[0];
    cr2=hc->c2[1];
    cr3=hc->c2[2];          
    hc->c2_t[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
    hc->c2_t[1]=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
    hc->c2_t[2]=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
    w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
    if(w<limit) w=limit;
    (hc->c2_t[0])/=w;
    (hc->c2_t[1])/=w;
    (hc->c2_t[2])/=w; 

    cr1=hc->c3[0];
    cr2=hc->c3[1];
    cr3=hc->c3[2];          
    hc->c3_t[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
    hc->c3_t[1]=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
    hc->c3_t[2]=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
    w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
    if(w<limit) w=limit;
    (hc->c3_t[0])/=w;
    (hc->c3_t[1])/=w;
    (hc->c3_t[2])/=w; 
  }
static void chart_data_rotate_cubes(struct chart_data *cd, gdouble qrs[16])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0; 
    gdouble w=0.0;  
    gdouble limit=0.1;
    struct cube3d *p1=&g_array_index(cd->d_t, struct cube3d, 0);
    
    for(i=0;i<(cd->d_t->len);i++)
      {
        cr1=(p1->c1).x;
        cr2=(p1->c1).y;
        cr3=(p1->c1).z;         
        (p1->c1).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->c1).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->c1).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        ((p1->c1).x)/=w;
        ((p1->c1).y)/=w;
        ((p1->c1).z)/=w;
        cr1=(p1->c2).x;
        cr2=(p1->c2).y;
        cr3=(p1->c2).z;         
        (p1->c2).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->c2).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->c2).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        ((p1->c2).x)/=w;
        ((p1->c2).y)/=w;
        ((p1->c2).z)/=w;
        cr1=(p1->c3).x;
        cr2=(p1->c3).y;
        cr3=(p1->c3).z;         
        (p1->c3).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->c3).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->c3).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        ((p1->c3).x)/=w;
        ((p1->c3).y)/=w;
        ((p1->c3).z)/=w;
        cr1=(p1->c4).x;
        cr2=(p1->c4).y;
        cr3=(p1->c4).z;         
        (p1->c4).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->c4).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->c4).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        ((p1->c4).x)/=w;
        ((p1->c4).y)/=w;
        ((p1->c4).z)/=w;
        cr1=(p1->c5).x;
        cr2=(p1->c5).y;
        cr3=(p1->c5).z;         
        (p1->c5).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->c5).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->c5).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        ((p1->c5).x)/=w;
        ((p1->c5).y)/=w;
        ((p1->c5).z)/=w;
        cr1=(p1->c6).x;
        cr2=(p1->c6).y;
        cr3=(p1->c6).z;         
        (p1->c6).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->c6).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->c6).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        ((p1->c6).x)/=w;
        ((p1->c6).y)/=w;
        ((p1->c6).z)/=w;
        cr1=(p1->c7).x;
        cr2=(p1->c7).y;
        cr3=(p1->c7).z;         
        (p1->c7).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->c7).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->c7).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        ((p1->c7).x)/=w;
        ((p1->c7).y)/=w;
        ((p1->c7).z)/=w;
        cr1=(p1->c8).x;
        cr2=(p1->c8).y;
        cr3=(p1->c8).z;         
        (p1->c8).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->c8).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->c8).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        ((p1->c8).x)/=w;
        ((p1->c8).y)/=w;
        ((p1->c8).z)/=w;
        p1++;
      } 
  }
static void chart_data_rotate(struct chart_data *cd, gdouble qrs[16])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0; 
    gdouble w=0.0;
    gdouble limit=0.1;  

    struct chart_point3d *p1=&g_array_index(cd->d_t, struct chart_point3d, 0);    
    for(i=0;i<(cd->d_t->len);i++)
      {
        cr1=(p1->x);
        cr2=(p1->y);
        cr3=(p1->z);         
        (p1->x)=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->y)=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->z)=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        (p1->x)/=w;
        (p1->y)/=w;
        (p1->z)/=w;
        p1++;
      }    
  }
static void chart_data_rotate_aligned(struct chart_data_aligned *cd, gdouble qrs[16])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0; 
    gdouble w=0.0;
    gdouble limit=0.1;  

    struct histo_arc *p1=(struct histo_arc*)(cd->d_t);
    for(i=0;i<(cd->len);i++)
      {
        cr1=(p1->a1).x;
        cr2=(p1->a1).y;
        cr3=(p1->a1).z;         
        (p1->a1).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->a1).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->a1).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        ((p1->a1).x)/=w;
        ((p1->a1).y)/=w;
        ((p1->a1).z)/=w;
        cr1=(p1->a2).x;
        cr2=(p1->a2).y;
        cr3=(p1->a2).z;         
        (p1->a2).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->a2).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->a2).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        ((p1->a2).x)/=w;
        ((p1->a2).y)/=w;
        ((p1->a2).z)/=w;
        cr1=(p1->a3).x;
        cr2=(p1->a3).y;
        cr3=(p1->a3).z;         
        (p1->a3).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->a3).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->a3).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        ((p1->a3).x)/=w;
        ((p1->a3).y)/=w;
        ((p1->a3).z)/=w;
        cr1=(p1->a4).x;
        cr2=(p1->a4).y;
        cr3=(p1->a4).z;         
        (p1->a4).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2])+qrs[3];
        (p1->a4).y=(cr1*qrs[4])+(cr2*qrs[5])+(cr3*qrs[6])+qrs[7];
        (p1->a4).z=(cr1*qrs[8])+(cr2*qrs[9])+(cr3*qrs[10])+qrs[11];
        w=(cr1*qrs[12])+(cr2*qrs[13])+(cr3*qrs[14])+qrs[15];
        if(w<limit) w=limit;
        ((p1->a4).x)/=w;
        ((p1->a4).y)/=w;
        ((p1->a4).z)/=w;
        p1++;
     }
  }
int compare_cubes(const void *a, const void *b)
  {
    return ((((struct cube3d*)a)->c1.z)-(((struct cube3d*)b)->c1.z));
  }
/*
    This is some exellent work done by Ramsundar Shandilya. Note the following for the original work
    and the rational behind it.
    
    https://medium.com/@ramshandilya/draw-smooth-curves-through-a-set-of-points-in-ios-34f6d73c8f9

    https://github.com/Ramshandilya/Bezier

    The MIT License (MIT)

    Copyright (c) 2015 Ramsundar Shandilya

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
  
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

    This is a translation of the original Swift code to C. It makes the function easy to use with GTK+,
    cairo and glib.

    For arrays with 3d points. Just calculate x and y.
*/
static GArray* control_points_from_coords_3d(const GArray *data_points3d)
  {  
    gint i=0;
    GArray *control_points=NULL;      
    //Number of Segments
    gint count=0;
    if(data_points3d!=NULL) count=data_points3d->len-1;
    gdouble *fCP=NULL;
    gdouble *sCP=NULL;

    if(count>0)
      {
        fCP=(gdouble*)g_malloc(2*count*sizeof(gdouble));
        sCP=(gdouble*)g_malloc(2*count*sizeof(gdouble));
      }
        
    //P0, P1, P2, P3 are the points for each segment, where P0 & P3 are the knots and P1, P2 are the control points.
    if(count<1||data_points3d==NULL)
      {
        //Return NULL.
        control_points=NULL;
        g_warning("Can't get control points from coordinates. NULL returned.\n");
      }
    else if(count==1)
      {
        struct chart_point3d P0=g_array_index(data_points3d, struct chart_point3d, 0);
        struct chart_point3d P3=g_array_index(data_points3d, struct chart_point3d, 1);

        //Calculate First Control Point
        //3P1 = 2P0 + P3
        struct chart_point3d P1;
        P1.x=(2.0*P0.x+P3.x)/3.0;
        P1.y=(2.0*P0.y+P3.y)/3.0;

        *(fCP)=P1.x;
        *(fCP+1)=P1.y;

        //Calculate second Control Point
        //P2 = 2P1 - P0
        struct chart_point3d P2;
        P2.x=(2.0*P1.x-P0.x);
        P2.y=(2.0*P1.y-P0.x);

        *(sCP)=P2.x;
        *(sCP+1)=P2.y;     
      }
    else
      {
        gdouble *rhs=(gdouble*)g_malloc(2*count*sizeof(gdouble));
        gdouble *a=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *b=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *c=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble rhsValueX=0;
        gdouble rhsValueY=0;
        struct chart_point3d P0;
        struct chart_point3d P3;        
        gdouble m=0;
        gdouble b1=0;
        gdouble r2x=0;
        gdouble r2y=0;
        gdouble P1_x=0;
        gdouble P1_y=0;
   
        for(i=0;i<count;i++)
          {
            P0=g_array_index(data_points3d, struct chart_point3d, i);
            P3=g_array_index(data_points3d, struct chart_point3d, i+1);

            if(i==0)
              {
                *(a)=0.0;
                *(b)=2.0;
                *(c)=1.0;

                //rhs for first segment
                rhsValueX=P0.x+2.0*P3.x;
                rhsValueY=P0.y+2.0*P3.y;
              }
            else if(i==count-1)
              {
                *(a+i)=2.0;
                *(b+i)=7.0;
                *(c+i)=0.0;

                //rhs for last segment
                rhsValueX=8.0*P0.x+P3.x;
                rhsValueY=8.0*P0.y+P3.y;
              }
            else
              {
                *(a+i)=1.0;
                *(b+i)=4.0;
                *(c+i)=1.0;

                rhsValueX=4.0*P0.x+2.0*P3.x;
                rhsValueY=4.0*P0.y+2.0*P3.y;
              }
            *(rhs+i*2)=rhsValueX;
            *(rhs+i*2+1)=rhsValueY;
          }

        //Solve Ax=B. Use Tridiagonal matrix algorithm a.k.a Thomas Algorithm
        for(i=1;i<count;i++)
          {
            m=(*(a+i))/(*(b+i-1));

            b1=(*(b+i))-m*(*(c+i-1));
            *(b+i)=b1;

            r2x=(*(rhs+i*2))-m*(*(rhs+(i-1)*2));
            r2y=(*(rhs+i*2+1))-m*(*(rhs+(i-1)*2+1));

            *(rhs+i*2)=r2x;
            *(rhs+i*2+1)=r2y;
          }
        
        //Last control Point
        gdouble lastControlPointX=(*(rhs+2*count-2))/(*(b+count-1));
        gdouble lastControlPointY=(*(rhs+2*count-1))/(*(b+count-1));

        *(fCP+2*count-2)=lastControlPointX;
        *(fCP+2*count-1)=lastControlPointY;

        gdouble controlPointX=0;
        gdouble controlPointY=0;

        for(i=count-2;i>=0;--i)
          {
            controlPointX=(*(rhs+i*2)-(*(c+i))*(*(fCP+(i+1)*2)))/(*(b+i));
            controlPointY=(*(rhs+i*2+1)-(*(c+i))*(*(fCP+(i+1)*2+1)))/(*(b+i));

             *(fCP+i*2)=controlPointX;
             *(fCP+i*2+1)=controlPointY; 
          }

        //Compute second Control Points from first.
        for(i=0;i<count;i++)
          {
            if(i==count-1)
              {
                P3=g_array_index(data_points3d, struct chart_point3d, i+1);
                P1_x=(*(fCP+i*2));
                P1_y=(*(fCP+i*2+1));

                controlPointX=(P3.x+P1_x)/2.0;
                controlPointY=(P3.y+P1_y)/2.0;

                *(sCP+count*2-2)=controlPointX;
                *(sCP+count*2-1)=controlPointY;
              }
            else
              {
                P3=g_array_index(data_points3d, struct chart_point3d, i+1);                
                P1_x=(*(fCP+(i+1)*2));
                P1_y=(*(fCP+(i+1)*2+1));

                controlPointX=2.0*P3.x-P1_x;
                controlPointY=2.0*P3.y-P1_y;

                *(sCP+i*2)=controlPointX;
                *(sCP+i*2+1)=controlPointY;
              }

          }

        control_points=g_array_sized_new(FALSE, FALSE, sizeof(struct histo_controls2d), count);
        g_array_set_size(control_points, count);
        struct histo_controls2d *cp=&g_array_index(control_points, struct histo_controls2d, 0);;
        for(i=0;i<count;i++)
          {
            (cp->x1)=(*(fCP+i*2));
            (cp->y1)=(*(fCP+i*2+1));
            (cp->x2)=(*(sCP+i*2));
            (cp->y2)=(*(sCP+i*2+1));
            cp++;
          }

        g_free(rhs);
        g_free(a);
        g_free(b);
        g_free(c);
     }

    if(fCP!=NULL) g_free(fCP);
    if(sCP!=NULL) g_free(sCP);

    return control_points;
  }
static void initialize_bezier_ring(struct histogram_chart *hc, struct histo_arc **ring, struct histo_arc **ring_t, gint sections, gdouble radius, gdouble max_z)
  {
    /*
         Bezier control points. 
         https://en.wikipedia.org/wiki/Composite_B%C3%A9zier_curve#Approximating_circular_arcs 

         Get the points in one arc of the ring and rotate them for the other points in the ring.
      Make one complete ring and copy that to make all the cylinders. Need points for each cylinder
      since the perspective matrix will change individual points in each cylinder. Non affine.
    */  
    gint i=0;
    gint j=0;
    gint k=0;
    //Translate x and y points for each ring.
    gdouble move_x=-((hc->columns)*(hc->outside_square))/2.0+(hc->outside_square)/2.0;
    gdouble move_y=-((hc->rows)*(hc->outside_square))/2.0+(hc->outside_square)/2.0;
    //Get one section arc.
    gdouble arc_top=-(2.0*G_PI/sections)/2.0;
    gdouble arc_offset=-2.0*G_PI/sections;
    gdouble start_x=radius*cos(arc_top);
    gdouble start_y=radius*sin(arc_top);
    gdouble end_x=start_x;
    gdouble end_y=-start_y;
    gdouble c1_x=(4.0*radius-start_x)/3.0;
    gdouble c1_y=((radius-start_x)*(3.0*radius-start_x))/(3.0*start_y);
    gdouble c2_x=c1_x;
    gdouble c2_y=-c1_y;
    gdouble offset1=0.0;
    gdouble offset2=0.0;

    //Just get one ring.
    GArray *one_ring=g_array_sized_new(FALSE, FALSE, sizeof(struct histo_arc), sections);
    g_array_set_size(one_ring, sections);

    *ring=(struct histo_arc*)aligned_alloc(8, sizeof(struct histo_arc)*(hc->rows)*(hc->columns)*sections*2);
    *ring_t=(struct histo_arc*)aligned_alloc(8, sizeof(struct histo_arc)*(hc->rows)*(hc->columns)*sections*2);
    if((one_ring->data)==NULL||*ring==NULL||*ring_t==NULL)
      {
        g_warning("Cylinder allocation error in initialize_bezier_ring().\n");
        goto end;
      }

    struct histo_arc *p1=&g_array_index(one_ring, struct histo_arc, 0);
    struct histo_arc *p2=&g_array_index(one_ring, struct histo_arc, 1);

    //Rotate the points in the section to align with the zero point on the unit circle.
    offset1=cos(arc_top);
    offset2=sin(arc_top);
    (p1->a4).x=start_x*offset1-start_y*offset2;
    (p1->a4).y=start_x*offset2+start_y*offset1;
    (p1->a4).z=0.0;
    (p1->a3).x=c1_x*offset1-c1_y*offset2;
    (p1->a3).y=c1_x*offset2+c1_y*offset1;
    (p1->a3).z=0.0;
    (p1->a2).x=c2_x*offset1-c2_y*offset2;
    (p1->a2).y=c2_x*offset2+c2_y*offset1;
    (p1->a2).z=0.0;
    (p1->a1).x=end_x*offset1-end_y*offset2;
    (p1->a1).y=end_x*offset2+end_y*offset1;
    (p1->a1).z=0.0;

    //Get a single ring.
    offset1=cos(arc_offset);
    offset2=sin(arc_offset);
    for(i=1;i<sections;i++)
      {
        (p2->a1).x=(p1->a1).x*offset1-(p1->a1).y*offset2;
        (p2->a1).y=(p1->a1).x*offset2+(p1->a1).y*offset1;
        (p2->a1).z=0.0;
        (p2->a2).x=(p1->a2).x*offset1-(p1->a2).y*offset2;
        (p2->a2).y=(p1->a2).x*offset2+(p1->a2).y*offset1;
        (p2->a2).z=0.0;
        (p2->a3).x=(p1->a3).x*offset1-(p1->a3).y*offset2;
        (p2->a3).y=(p1->a3).x*offset2+(p1->a3).y*offset1;
        (p2->a3).z=0.0;
        (p2->a4).x=(p1->a4).x*offset1-(p1->a4).y*offset2;
        (p2->a4).y=(p1->a4).x*offset2+(p1->a4).y*offset1;
        (p2->a4).z=0.0;
        p1++;p2++;
      }

    //Fill arrays to be graphed.
    struct histo_arc *p3=(*ring);
    for(i=0;i<(hc->rows);i++)
      {
        for(j=0;j<(hc->columns);j++)
          {
            p1=&g_array_index(one_ring, struct histo_arc, 0);
            for(k=0;k<(one_ring->len);k++)
              {
                //Bottom circle.
                p3->id=i*(hc->columns)+j;
                (p3->a1).x=(p1->a1).x+move_x;
                (p3->a1).y=(p1->a1).y+move_y;
                (p3->a1).z=(p1->a1).z;
                (p3->a2).x=(p1->a2).x+move_x;
                (p3->a2).y=(p1->a2).y+move_y;
                (p3->a2).z=(p1->a2).z;
                (p3->a3).x=(p1->a3).x+move_x;
                (p3->a3).y=(p1->a3).y+move_y;
                (p3->a3).z=(p1->a3).z;
                (p3->a4).x=(p1->a4).x+move_x;
                (p3->a4).y=(p1->a4).y+move_y;
                (p3->a4).z=(p1->a4).z;
                (p3->a4).z=(p1->a4).z;
                (p3->cl).r=0.0;
                (p3->cl).g=1.0;
                (p3->cl).b=0.0;
                (p3->cl).a=1.0;
                p1++;p3++;
              }
            p1=&g_array_index(one_ring, struct histo_arc, 0);
            for(k=0;k<(one_ring->len);k++)
              {
                //Top circle.
                p3->id=i*(hc->columns)+j;
                (p3->a1).x=(p1->a1).x+move_x;
                (p3->a1).y=(p1->a1).y+move_y;
                (p3->a1).z=(p1->a1).z;
                (p3->a2).x=(p1->a2).x+move_x;
                (p3->a2).y=(p1->a2).y+move_y;
                (p3->a2).z=(p1->a2).z;
                (p3->a3).x=(p1->a3).x+move_x;
                (p3->a3).y=(p1->a3).y+move_y;
                (p3->a3).z=(p1->a3).z;
                (p3->a4).x=(p1->a4).x+move_x;
                (p3->a4).y=(p1->a4).y+move_y;
                (p3->a4).z=(p1->a4).z;
                (p3->cl).r=0.0;
                (p3->cl).g=1.0;
                (p3->cl).b=0.0;
                (p3->cl).a=1.0;
                p1++;p3++;
              }
             move_x+=(hc->outside_square);
           }
         move_x-=(hc->outside_square)*(hc->columns);
         move_y+=(hc->outside_square);
       }
       
    g_array_free(one_ring, TRUE);

    end:
    return;
  }
int compare_cylinders16(const void *a, const void *b)
  {
    return (((((struct cylinder16*)a)->ar1).a1.z)-((((struct cylinder16*)b)->ar1).a1.z));
  }
int compare_cylinders24(const void *a, const void *b)
  {
    return (((((struct cylinder24*)a)->ar1).a1.z)-((((struct cylinder24*)b)->ar1).a1.z));
  }
static void get_gradient_color_bt(gdouble percent, gdouble *r, gdouble *g, gdouble *b)
  {
    *r=b_c[0]+percent*(t_c[0]-b_c[0]);
    *g=b_c[1]+percent*(t_c[1]-b_c[1]);
    *b=b_c[2]+percent*(t_c[2]-b_c[2]);
  }
static void get_gradient_color_bm(gdouble percent, gdouble *r, gdouble *g, gdouble *b)
  {
    *r=b_c[0]+percent*(m_c[0]-b_c[0]);
    *g=b_c[1]+percent*(m_c[1]-b_c[1]);
    *b=b_c[2]+percent*(m_c[2]-b_c[2]);
  }
static void get_gradient_color_mt(gdouble percent, gdouble *r, gdouble *g, gdouble *b)
  {
    *r=m_c[0]+percent*(t_c[0]-m_c[0]);
    *g=m_c[1]+percent*(t_c[1]-m_c[1]);
    *b=m_c[2]+percent*(t_c[2]-m_c[2]);
  }
void set_chart_data_groups(GPtrArray *chart_groups, const gchar *key_file_string)
  {
    gint i=0;
    gint len=strlen(key_file_string);
    GError *error1=NULL;
    GKeyFile *key_file=g_key_file_new();
    g_key_file_load_from_data(key_file, key_file_string, len, G_KEY_FILE_NONE, &error1);
    if(error1!=NULL)
      {
        g_print("Key File Error: %s\n", error1->message);
        g_error_free(error1);
      } 
    else
      {
        //Count groups in key file.
        gsize group_len=0;
        gchar **groups=g_key_file_get_groups(key_file, &group_len);

        gdouble line_color_array[4];
        gdouble point_color_array[4];
        gdouble line_width_value=0.0;
        struct chart_data *data=NULL;
        struct chart_data_group *p1=NULL;
        for(i=0;i<group_len;i++)
          {
            data=get_key_file_group(key_file, groups[i], line_color_array, point_color_array, &line_width_value);   
         
            p1=g_new(struct chart_data_group, 1);
            p1->line_color[0]=line_color_array[0];
            p1->line_color[1]=line_color_array[1];
            p1->line_color[2]=line_color_array[2];
            p1->line_color[3]=line_color_array[3];
            p1->point_color[0]=point_color_array[0];
            p1->point_color[1]=point_color_array[1];
            p1->point_color[2]=point_color_array[2];
            p1->point_color[3]=point_color_array[3];
            p1->line_width=line_width_value;
            p1->cd=data;
            g_ptr_array_add(chart_groups, p1);
          }

        g_strfreev(groups);
      }
    g_key_file_unref(key_file);
  }
void free_chart_data_groups(GPtrArray *chart_groups)
  {
    gint i=0;
    struct chart_data_group *p1=NULL;

    for(i=0;i<(chart_groups->len);i++)
      {
        p1=g_ptr_array_index(chart_groups, i);
        g_array_free(p1->cd->d, TRUE);
        g_array_free(p1->cd->d_t, TRUE);
        g_free(p1->cd);
        g_free(p1);
      }
    if(chart_groups!=NULL) g_ptr_array_free(chart_groups, TRUE);
  }
void clear_chart_data_groups(GPtrArray *chart_groups)
  {
    gint i=0;
    struct chart_data_group *p1=NULL;

    for(i=(chart_groups->len);i>0;i--)
      {
        p1=g_ptr_array_index(chart_groups, i-1);
        g_array_free(p1->cd->d, TRUE);
        g_array_free(p1->cd->d_t, TRUE);
        g_free(p1->cd);
        g_ptr_array_remove_index_fast(chart_groups, i-1);
        g_free(p1);
      }
  }
static struct chart_data* get_key_file_group(GKeyFile *key_file, const gchar *group, gdouble line_color_array[4], gdouble point_color_array[4], gdouble *line_width_value)
  {
    gint i=0;
    gsize line_color_len=0;
    GError *line_color_err=NULL;
    gdouble *line_color=g_key_file_get_double_list(key_file, group, "line_color", &line_color_len, &line_color_err);
    if(line_color_err!=NULL)
      {
        g_print("line_color error: %s\n", line_color_err->message);
        g_error_free(line_color_err);
        line_color_array[0]=0.0;
        line_color_array[1]=0.0;
        line_color_array[2]=0.0;
        line_color_array[3]=1.0;
      }
    else
      {
        gdouble *p1=line_color;
        for(i=0;i<line_color_len;i++)
          {
            line_color_array[i]=*p1;
            p1++;
          }
      }
    g_free(line_color);

    gsize point_color_len=0;
    GError *point_color_err=NULL;
    gdouble *point_color=g_key_file_get_double_list(key_file, group, "point_color", &point_color_len, &point_color_err);
    if(point_color_err!=NULL)
      {
        g_print("point_color error: %s\n", point_color_err->message);
        g_error_free(point_color_err);
        point_color_array[0]=0.0;
        point_color_array[1]=0.0;
        point_color_array[2]=0.0;
        point_color_array[3]=1.0;
      }
    else
      {
        gdouble *p1=point_color;
        for(i=0;i<point_color_len;i++)
          {
            point_color_array[i]=*p1;
            p1++;
          }
      }
    g_free(point_color);

    gdouble line_width=0.0;
    GError *line_width_err=NULL;
    line_width=g_key_file_get_double(key_file, group, "line_width", &line_width_err);
    if(line_width_err!=NULL)
      {
        g_print("Error: %s\n", line_width_err->message);
        g_error_free(line_width_err);
        *line_width_value=2.0;
      }
    else
      {
        *line_width_value=line_width;
      }

    gsize data_len=0;
    GError *data_err=NULL;
    gdouble *data1=g_key_file_get_double_list(key_file, group, "data", &data_len, &data_err);
    if(data_err!=NULL)
      {
        g_print("Error: %s\n", data_err->message);
        g_error_free(data_err);
      }

    struct chart_data *data2=g_new(struct chart_data, 1);
    //Initialize empty arrays.
    data2->data_id=0;
    data2->max_z=0.0;
    data2->d=g_array_new(FALSE, TRUE, sizeof(struct chart_point3d));
    data2->d_t=g_array_new(FALSE, TRUE, sizeof(struct chart_point3d));

    //Copy the values.
    if(data1!=NULL&&data_len>3)
      {
        if(data_len%3==0)
          {
            gdouble *p1=data1;
            struct chart_point3d point;
            for(i=0;i<data_len;i+=3)
              {
                point.x=*p1;
                p1++;
                //Reverse y for standard cartesian coordinates. With cairo +y is down.
                point.y=-(*p1);
                p1++;
                point.z=*p1;
                p1++;
                point.move_id=C_MOVE_TO_R;
                g_array_append_val(data2->d, point);
                g_array_append_val(data2->d_t, point);
              }
            //Set the last move to stroke.
            struct chart_point3d *c1=&g_array_index(data2->d, struct chart_point3d, (data2->d->len)-1);
            struct chart_point3d *c2=&g_array_index(data2->d_t, struct chart_point3d, (data2->d_t->len)-1);
            c1->move_id=C_CURVE_TO_STROKE;
            c2->move_id=C_CURVE_TO_STROKE;
          }
        else
          {
            g_print("Key file dataset length isn't divisible by 3.\n");
          }
      }
    else
      {
        g_print("Key file Data Error: Need at least 3 points to plot.\n");
      }

    g_free(data1);
    return data2;
  }
static void intersection(double A1, double A2, double B1, double B2, double C1, double C2, double D1, double D2, double *out_x, double *out_y) 
  { 
    //line AB 
    double a1=B2-A2; 
    double b1=A1-B1; 
    double c1=a1*A1+b1*A2; 
  
    //line CD
    double a2=D2-C2; 
    double b2=C1-D1; 
    double c2=a2*C1+b2*C2; 
  
    double determinant=a1*b2-a2*b1; 

    //ignore determinant==0 since out_x and out_y are preset.  
    if(determinant!=0) 
     { 
       *out_x=(b2*c1-b1*c2)/determinant; 
       *out_y=(a1*c2-a2*c1)/determinant; 
     } 
  }

