
/*
  Combine a chart with a mesh and the other cairo3d.c shapes. Add some text and pie charts also.

  gcc -Wall cairo3d.c histogram_chart3d.c cairo3d_text.c pie_chart3d.c cairo3d_combo_UI.c -o cairo3d_combo `pkg-config --cflags --libs gtk+-3.0 freetype2` -lm

  Tested on Ubuntu20.04 and GTK3.24

  C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<cairo-svg.h>
#include<cairo-pdf.h>
#include<cairo-ps.h>
#include"cairo3d.h"
#include"histogram_chart3d.h"
#include"cairo3d_text.h"
#include"pie_chart3d.h"

/*
  Used for calculating points in .svg, .pdf and .ps output.
  Why 90. This works on the test computer. When a .svg is saved and then opened, the pixels
match the original drawing. Rsvg rsvg_handle_set_dpi() notes "Common values are 75, 90,
and 300 DPI." which it looks like 90 is being used as default to open the picture. 
  The ppi might need to be changed for matching ouput on different computers.
*/
static gdouble ppi=90.0;
//Some settings for the UI.
static gint drawing_id=0;
static gdouble pitch=0.0;
static gdouble roll=0.0;
static gdouble yaw=0.0;
static gdouble scale=1.0;
static gdouble perspective=0.0;
static guint tick_id=0;
static gboolean frame_rate=FALSE;
static gboolean time_it=FALSE;
static gdouble layout_width=500.0;
static gdouble layout_height=500.0;
static gdouble translate_x=0.0;
static gdouble translate_y=0.0;
static gdouble background_color[4]={1.0, 1.0, 1.0, 1.0};

struct z_sort{
    gdouble x;
    gdouble y;
    gdouble z;
    gint id;
   };

//The UI part.
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data);
static void set_animate_drawing(GtkComboBox *combo, gpointer *parts);
static void set_drawing_id(GtkComboBox *combo, gpointer da);
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void show_frame_rate(GtkToggleButton *check_frame, gpointer data);
static void show_drawing_time(GtkToggleButton *check_time, gpointer data);
static void layout_width_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void layout_height_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void translate_x_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void translate_y_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void set_background_color(GtkWidget *button, gpointer *data);
static gboolean animate_drawing(GtkWidget *da, GdkFrameClock *frame_clock, gpointer *parts);
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer *parts);
static void draw_svg(GtkWidget *widget, gpointer *parts);
static void draw_pdf(GtkWidget *widget, gpointer *parts);
static void draw_ps(GtkWidget *widget, gpointer *parts);
static void draw_png(GtkWidget *widget, gpointer *parts);
static void draw_surface(cairo_t *cr, gdouble width, gdouble height, gpointer *parts, guint surface_type);
//Initialize a histogram chart.
static struct histogram_chart* initialize_histogram_chart(guint rows, guint columns, gdouble outside_square);
static void draw_chart_with_mesh(struct histogram_chart *hc, cairo_t *cr, gdouble qrs1[9], gdouble qrs2[16], gpointer *parts);
//Test shapes on a chart.
static void draw_shapes1(cairo_t *cr, gdouble qrs[9]);
static void draw_shapes2(cairo_t *cr, gdouble qrs[9]);
static void draw_shapes3(cairo_t *cr, gdouble qrs[9]);
static void draw_shapes4(cairo_t *cr, gdouble qrs[9]);
static void draw_shapes5(cairo_t *cr, gdouble qrs[9]);
static void draw_bounding_boxes(struct text3d *t3d,  cairo_t *cr, gdouble qrs[9]);
static void draw_text3d(struct text3d *t3d,  cairo_t *cr, gdouble qrs[9]);
static void draw_shadow_text(struct text2d *t2d,  cairo_t *cr, gdouble qrs[9], gdouble light_position[3], gdouble plane_distance);
static void draw_char(cairo_t *cr, GArray *path1, GArray *path2, gdouble rgba1[4], gdouble rgba2[4]);
static void draw_shadow_char(cairo_t *cr, GArray *path1, GArray *shadow, gdouble rgba1[4], gdouble rgba2[4]);
static void draw_pie_charts(cairo_t *cr, gdouble qrs[9], gpointer *parts);
//Sort shapes.
static int compare_z(const void *a, const void *b);
//About the program.
static void about_dialog(GtkWidget *widget, gpointer data);

int main(int argc, char **argv)
  {
    FT_Library library; 
    FT_Error error;
    FT_Face face;
    /*
      Test fonts with different EM square value.
    */
    char *font="/usr/share/fonts/truetype/freefont/FreeSansBold.ttf";
    //char *font="/usr/share/fonts/opentype/cantarell/Cantarell-Bold.otf";
    //char *font="/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf";

    gtk_init(&argc, &argv);

    error=FT_Init_FreeType(&library);
    if(error) printf("Init_FreeType Error\n");

    error=FT_New_Face(library, font, 0, &face);
    if(error) printf("FT_New_Face Error.\n");

    gdouble point_size=140.0;
    gdouble font_scale1=0.0;
    gdouble font_scale2=0.0;
    //Added spacing between chars.
    gdouble spacing=2.0;
    //The 3d text depth.
    gdouble depth=40.0;
    //Space the width of the chars using the advanceX or the glyph box width.
    gboolean use_advance=FALSE;
    if(FT_IS_SCALABLE(face))
      {
        printf("EM square %i\n", face->units_per_EM);
        font_scale1=point_size/(gdouble)(face->units_per_EM);
        font_scale2=80.0/(gdouble)(face->units_per_EM);
      }
    else g_print("Font isn't scalable Error.");

    //The cairo3d text.
    GString *string=g_string_new("cairo3d");
    struct text3d *t3d=text3d_new(face, string, font_scale1, spacing, depth, use_advance);
    g_string_free(string, TRUE);

    //The cairo2d shadow text.
    GString *string2=g_string_new("shadow");
    struct text2d *t2d=text2d_new(face, string2, font_scale2, spacing+2.0, depth, use_advance);
    g_string_free(string2, TRUE);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Cairo3d Combo");
    gtk_window_set_default_size(GTK_WINDOW(window), 1000, 650);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_widget_set_app_paintable(window, TRUE);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    //Try to set transparency of main window.
    GdkScreen *screen=gtk_widget_get_screen(window); 
    if(gdk_screen_is_composited(screen))
      {
        GdkVisual *visual=gdk_screen_get_rgba_visual(screen);
        gtk_widget_set_visual(window, visual);
      }
    else
      {
        g_print("Can't set window transparency.\n");
      } 

    //Start the shape cache and initialize a shape that isn't automatically initialized.
    start_shape_cache();   
    initialize_wire_bezier_sheet(16, 16, 250, 250);    
    set_cosine_wave(WIRE_BEZIER_SHEET, 16, 16, 125.0, 40.0);

    //Initialize a histogram chart. 
    guint rows=7;
    guint columns=9;
    gdouble outside_square=50.0;
    struct histogram_chart *hc=initialize_histogram_chart(rows, columns, outside_square);

    //Initialize a pie chart.
    gdouble rgba[4]={1.0, 0.0, 0.0, 1.0};
    struct pie_chart *pc1=pie_chart_new(60, 50.0, 10.0);
    rgba[0]=1.0;rgba[1]=0.0;rgba[2]=0.0;rgba[3]=0.6;
    pie_chart_append_section(pc1, 9, rgba);
    rgba[0]=0.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.6;
    pie_chart_append_section(pc1, 29, rgba);
    rgba[0]=0.0;rgba[1]=0.0;rgba[2]=1.0;rgba[3]=0.6;
    pie_chart_append_section(pc1, 39, rgba);
    rgba[0]=1.0;rgba[1]=1.0;rgba[2]=0.0;rgba[3]=0.6;
    pie_chart_append_section(pc1, 49, rgba);
    rgba[0]=1.0;rgba[1]=0.0;rgba[2]=1.0;rgba[3]=0.6;
    pie_chart_append_section(pc1, 59, rgba);  

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);
    gpointer parts[]={da, hc, face, pc1, t3d, t2d, window};
    g_signal_connect(da, "draw", G_CALLBACK(draw_main), parts);

    GtkWidget *da_scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(da_scroll, TRUE);
    gtk_widget_set_vexpand(da_scroll, TRUE);
    gtk_widget_set_size_request(da, 1000, 1000);
    gtk_container_add(GTK_CONTAINER(da_scroll), da); 

    GtkWidget *combo_animate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_animate, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_animate), 0, "1", "Draw Chart");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_animate), 1, "2", "Animate Chart");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_animate), 0);
    g_signal_connect(combo_animate, "changed", G_CALLBACK(set_animate_drawing), parts);

    GtkWidget *combo_drawing=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_drawing, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "1", "Wire Sheet");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "2", "Solid Sheet");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 2, "3", "Mesh Sheet");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 3, "4", "Wire Disc");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 4, "5", "Solid Disc");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 5, "6", "Mesh Disc");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 6, "7", "Show Shapes1");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 7, "8", "Show Shapes2");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 8, "9", "Show Shapes3");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 9, "10", "Show Shapes4");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 10, "11", "Show Shapes5");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 11, "12", "Pie Charts");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 12, "13", "Draw Text3d");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 13, "14", "Draw Shadow Text");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
    g_signal_connect(combo_drawing, "changed", G_CALLBACK(set_drawing_id), da);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, FALSE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, FALSE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, FALSE);

    GtkWidget *scale_label=gtk_label_new("Scale");
    gtk_widget_set_hexpand(scale_label, FALSE);

    GtkWidget *perspective_label=gtk_label_new("Perspective");
    gtk_widget_set_hexpand(perspective_label, FALSE);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_hexpand(pitch_slider, FALSE);
    gtk_widget_set_vexpand(pitch_slider, TRUE);
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_pitch), da);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_hexpand(roll_slider, FALSE);
    gtk_widget_set_vexpand(roll_slider, TRUE);
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_roll), da);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_hexpand(yaw_slider, FALSE);
    gtk_widget_set_vexpand(yaw_slider, TRUE);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_yaw), da);  

    GtkWidget *scale_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 2.0, 0.01);
    gtk_widget_set_hexpand(scale_slider, FALSE);
    gtk_widget_set_vexpand(scale_slider, TRUE);
    gtk_range_set_value(GTK_RANGE(scale_slider), 1.0);
    g_signal_connect(scale_slider, "change-value", G_CALLBACK(set_scale), da);

    GtkWidget *perspective_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 1.0, 0.01);
    gtk_widget_set_hexpand(perspective_slider, FALSE);
    gtk_widget_set_vexpand(perspective_slider, TRUE);
    g_signal_connect(perspective_slider, "change-value", G_CALLBACK(set_perspective), da);

    GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
    gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
    g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), da);

    GtkWidget *check_time=gtk_check_button_new_with_label("Time Drawing");
    gtk_widget_set_halign(check_time, GTK_ALIGN_CENTER);
    g_signal_connect(check_time, "toggled", G_CALLBACK(show_drawing_time), da);
   
    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 8);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_animate, 0, 0, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_drawing, 0, 1, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale_label, 3, 2, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid1), perspective_label, 4, 2, 1, 1);     
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale_slider, 3, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), perspective_slider, 4, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_frame, 0, 4, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_time, 0, 5, 4, 1);

    GtkWidget *scroll1=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll1, TRUE);
    gtk_widget_set_vexpand(scroll1, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll1), grid1);

    GtkAdjustment *translate_x_adj=gtk_adjustment_new(0.0, -500.0, 500.0, 10.0, 0.0, 0.0);
    GtkAdjustment *translate_y_adj=gtk_adjustment_new(0.0, -500.0, 500.0, 10.0, 0.0, 0.0);
    GtkAdjustment *layout_width_adj=gtk_adjustment_new(500.0, 1.0, 2000.0, 50.0, 0.0, 0.0);
    GtkAdjustment *layout_height_adj=gtk_adjustment_new(500.0, 1.0, 2000.0, 50.0, 0.0, 0.0); 

    GtkWidget *translate_x_label=gtk_label_new("Translate x");
    gtk_widget_set_hexpand(translate_x_label, FALSE);
    GtkWidget *translate_x_spin=gtk_spin_button_new(translate_x_adj, 0.0, 1);
    gtk_widget_set_hexpand(translate_x_spin, FALSE);
    g_signal_connect(translate_x_spin, "value-changed", G_CALLBACK(translate_x_spin_changed), da); 

    GtkWidget *translate_y_label=gtk_label_new("Translate y");
    gtk_widget_set_hexpand(translate_y_label, FALSE);
    GtkWidget *translate_y_spin=gtk_spin_button_new(translate_y_adj, 0.0, 1);
    gtk_widget_set_hexpand(translate_y_spin, FALSE);
    g_signal_connect(translate_y_spin, "value-changed", G_CALLBACK(translate_y_spin_changed), da); 

    GtkWidget *layout_width_label=gtk_label_new("Layout Width");
    gtk_widget_set_hexpand(layout_width_label, FALSE);
    GtkWidget *layout_width_spin=gtk_spin_button_new(layout_width_adj, 500.0, 1);
    gtk_widget_set_hexpand(layout_width_spin, FALSE);
    g_signal_connect(layout_width_spin, "value-changed", G_CALLBACK(layout_width_spin_changed), da);

    GtkWidget *layout_height_label=gtk_label_new("Layout Height");
    gtk_widget_set_hexpand(layout_height_label, FALSE);
    GtkWidget *layout_height_spin=gtk_spin_button_new(layout_height_adj, 500.0, 1);
    gtk_widget_set_hexpand(layout_height_spin, FALSE);
    g_signal_connect(layout_height_spin, "value-changed", G_CALLBACK(layout_height_spin_changed), da);

    GtkWidget *background_label=gtk_label_new("Background Color");
    gtk_widget_set_hexpand(background_label, FALSE);
    GtkWidget *background_entry=gtk_entry_new();
    gtk_widget_set_hexpand(background_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(background_entry), "rgba(255, 255, 255, 1.0)");

    GtkWidget *background_button=gtk_button_new_with_label("Update Background Color");
    gtk_widget_set_hexpand(background_button, FALSE);
    gpointer w1[]={da, background_entry};
    g_signal_connect(background_button, "clicked", G_CALLBACK(set_background_color), w1);

    GtkWidget *grid2=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid2), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid2), 8);
    gtk_grid_attach(GTK_GRID(grid2), translate_x_label, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), translate_y_label, 1, 0, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid2), translate_x_spin, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), translate_y_spin, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), layout_width_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), layout_height_label, 1, 2, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid2), layout_width_spin, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), layout_height_spin, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), background_label, 0, 4, 2, 1);
    gtk_grid_attach(GTK_GRID(grid2), background_entry, 0, 5, 2, 1);
    gtk_grid_attach(GTK_GRID(grid2), background_button, 0, 6, 2, 1);

    GtkWidget *scroll2=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll2, TRUE);
    gtk_widget_set_vexpand(scroll2, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll2), grid2);

    GtkWidget *nb_label1=gtk_label_new("Cairo3d Combos");
    GtkWidget *nb_label2=gtk_label_new("Options1");
    GtkWidget *notebook=gtk_notebook_new();
    gtk_container_set_border_width(GTK_CONTAINER(notebook), 15);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll1, nb_label1);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll2, nb_label2);

    GtkWidget *menu1=gtk_menu_new();
    GtkWidget *menu1item1=gtk_menu_item_new_with_label("Save as .svg");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item1);
    GtkWidget *menu1item2=gtk_menu_item_new_with_label("Save as .pdf");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item2);
    GtkWidget *menu1item3=gtk_menu_item_new_with_label("Save as .ps");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item3);
    GtkWidget *menu1item4=gtk_menu_item_new_with_label("Save as .png");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item4);
    GtkWidget *title1=gtk_menu_item_new_with_label("Save Drawing");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(title1), menu1);

    GtkWidget *menu2=gtk_menu_new();
    GtkWidget *menu2item1=gtk_menu_item_new_with_label("Cairo3d Combo");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu2), menu2item1);
    GtkWidget *title2=gtk_menu_item_new_with_label("About");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(title2), menu2);

    GtkWidget *menu_bar=gtk_menu_bar_new();
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), title1);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), title2);

    g_signal_connect(menu1item1, "activate", G_CALLBACK(draw_svg), parts);
    g_signal_connect(menu1item2, "activate", G_CALLBACK(draw_pdf), parts);
    g_signal_connect(menu1item3, "activate", G_CALLBACK(draw_ps), parts);
    g_signal_connect(menu1item4, "activate", G_CALLBACK(draw_png), parts);
    g_signal_connect(menu2item1, "activate", G_CALLBACK(about_dialog), window);

    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), notebook, FALSE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da_scroll, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 375);

    GtkWidget *grid3=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid3), menu_bar, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), paned1, 0, 1, 1, 1);

    //Draw background window based on the paned window splitter.
    g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), paned1);

    gtk_container_add(GTK_CONTAINER(window), grid3);

    gchar *css_string=g_strdup("#about_dialog{background: rgba(127,127,255,1.0); font-size: 20px; font-weight: bold;} #about_dialog label selection{background: rgba(0,50,220,0.5);}");
    GError *css_error=NULL;
    GtkCssProvider *provider=gtk_css_provider_new();
    gtk_css_provider_load_from_data(provider, css_string, -1, &css_error);
    gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    if(css_error!=NULL)
      {
        g_print("CSS loader error %s\n", css_error->message);
        g_error_free(css_error);
      }
    g_object_unref(provider);

    gtk_widget_show_all(window);

    gtk_main();

    //Clean up.
    histogram_chart_free(hc);
    free_shape_cache();
    free_text3d(t3d);
    free_text2d(t2d);
    pie_chart_free(pc1);

    FT_Done_Face(face);
    FT_Done_FreeType(library);

    return 0;  
  }
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data)
  {
    //Paint background of drawing area.
    cairo_set_source_rgba(cr, background_color[0], background_color[1], background_color[2], background_color[3]);
    cairo_paint(cr);
    //Paint the background under the grid.
    cairo_set_source_rgba(cr, 0.5, 0.5, 1.0, 1.0);
    gint width=gtk_paned_get_position(GTK_PANED(data));
    gint height=gtk_widget_get_allocated_height(widget);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_rectangle(cr, width-4, 0.0, 4, height);
    cairo_fill(cr);
    return FALSE;
  }
static void set_animate_drawing(GtkComboBox *combo, gpointer *parts)
  {   
    pitch=0.0;
    roll=0.0;
    yaw=0.0;
 
    if(gtk_combo_box_get_active(combo)==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(parts[0]), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(parts[0]));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts[0]), (GtkTickCallback)animate_drawing, parts, NULL);
          }
      }
    
  }
static void set_drawing_id(GtkComboBox *combo, gpointer da)
  {  
    drawing_id=gtk_combo_box_get_active(combo);

    clear_shape_cache();

    //These shapes need to be initialized.
    if(drawing_id==0)
      {
        initialize_wire_bezier_sheet(16, 16, 250, 250);
        set_cosine_wave(WIRE_BEZIER_SHEET, 16, 16, 125.0, 40.0);
      }
    else if(drawing_id==1||drawing_id==2)
      {
        initialize_solid_bezier_sheet(16, 16, 250, 250);
        set_cosine_wave(SOLID_BEZIER_SHEET, 16, 16, 125.0, 40.0);
      }
    else if(drawing_id==3)
      {
        initialize_wire_bezier_disc(16, 16, 150);
        set_cosine_wave(WIRE_BEZIER_DISC, 16, 16, 150.0, 40.0);
      }
    else if(drawing_id==4||drawing_id==5||drawing_id==6)
      {
        initialize_solid_bezier_disc(16, 16, 150);
        set_cosine_wave(SOLID_BEZIER_DISC, 16, 16, 150.0, 40.0);
      }

    gtk_widget_queue_draw(GTK_WIDGET(da));
  }
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    pitch=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    roll=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    yaw=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>2.0) scale=2.0;
    else if(value<0.0) scale=0.0;
    else scale=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value<0.0) perspective=0.0;
    else if(value>100.0) perspective=100.0;
    else perspective=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void show_frame_rate(GtkToggleButton *check_frame, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_frame)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void translate_x_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    translate_x=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void translate_y_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    translate_y=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void layout_width_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    layout_width=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void layout_height_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    layout_height=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_background_color(GtkWidget *button, gpointer *data)
  {
    GdkRGBA rgba;
    
    if(gdk_rgba_parse(&rgba, gtk_entry_get_text(GTK_ENTRY(data[1]))))
      {
        background_color[0]=rgba.red;
        background_color[1]=rgba.green;
        background_color[2]=rgba.blue;
        background_color[3]=rgba.alpha;
      }
    else
      {
        g_print("Background color string format error.\n");
      } 

    gtk_widget_queue_draw(GTK_WIDGET(data[0]));
  }
static void show_drawing_time(GtkToggleButton *check_time, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_time)) time_it=TRUE;
    else time_it=FALSE;
  }
static gboolean animate_drawing(GtkWidget *da, GdkFrameClock *frame_clock, gpointer *parts)
  {
    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    if(pitch>359.0) pitch=0.0;
    else pitch+=0.5;
    if(roll>359.0) roll=0.0;
    else roll+=0.5;
    if(yaw>359.0) yaw=0.0;
    else yaw+=0.5;
    
    gtk_widget_queue_draw(GTK_WIDGET(da));
    return G_SOURCE_CONTINUE;
  }
static gboolean draw_main(GtkWidget *da, cairo_t *cr, gpointer *parts)
  {  
    gdouble w1=layout_width/10.0;
    gdouble h1=layout_height/10.0;
    struct histogram_chart *hc=parts[1];
   
    //Background.
    cairo_set_source_rgba(cr, background_color[0], background_color[1], background_color[2], background_color[3]);
    cairo_paint(cr); 

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 2.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Layout bounding box.
    cairo_rectangle(cr, 0.0, 0.0, layout_width, layout_height);
    cairo_stroke(cr);
  
    GTimer *timer=NULL; 
    if(time_it==TRUE) timer=g_timer_new();
 
    cairo_translate(cr, layout_width/2.0, layout_height/2.0); 
    cairo_translate(cr, translate_x, translate_y);
    cairo_scale(cr, scale+0.01, scale+0.01);
    //Keep the lines smooth.
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);        
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_set_line_width(cr, 1.0);

    //Chart Rotation.
    gdouble qrs[16];
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    histo_quaternion_rotation(yaw1, roll1, pitch1, qrs);

    //Perspective.
    gdouble pm[16];
    gdouble out[16];
    gdouble near=1.0;
    gdouble far=1.0+perspective*0.01;
    set_projection_matrix(pm, 90.0, near, far);  
    histo_matrix_multiply(qrs, pm, out); 
   
    //Mesh rotation. No perspective for the shapes. Causes trouble with using symetry to draw some shapes.
    gdouble qrs2[9];
    set_rotation3d(yaw1, roll1, pitch1);
    quaternion_rotation(yaw1, roll1, pitch1, qrs2);

    //Draw the chart.
    draw_chart_with_mesh(hc, cr, qrs2, out, parts);
     
    if(time_it==TRUE) 
     {
       g_print("Draw Time %f\n", g_timer_elapsed(timer, NULL));
       g_timer_destroy(timer);
     }

    return FALSE;
  }
static gchar* save_file(gpointer *parts, gint surface_type)
  {
    int result=0;

    GtkWidget *dialog=gtk_file_chooser_dialog_new("Save File", GTK_WINDOW(parts[6]), GTK_FILE_CHOOSER_ACTION_SAVE, "Cancel", GTK_RESPONSE_CANCEL, "Save", GTK_RESPONSE_ACCEPT, NULL);

    if(surface_type==0) gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".svg");
    else if(surface_type==1) gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".pdf");
    else if(surface_type==2) gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".ps");
    else gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".png"); 

    result=gtk_dialog_run(GTK_DIALOG(dialog));
    gchar *file_name=NULL;
    if(result==GTK_RESPONSE_ACCEPT)
      {

        file_name=gtk_file_chooser_get_current_name(GTK_FILE_CHOOSER(dialog));
      }

    gtk_widget_destroy(dialog);

    return file_name;
  }
static void draw_svg(GtkWidget *widget, gpointer *parts)
  {
    gchar *file_name=save_file(parts, 0);
    
    //Scale factor to convert to points.
    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_svg_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, layout_width, layout_height, parts, 0);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name);
  }
static void draw_pdf(GtkWidget *widget, gpointer *parts)
  {
    gchar *file_name=save_file(parts, 1);

    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_pdf_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, layout_width, layout_height, parts, 1);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }
    
    g_free(file_name);
  }
static void draw_ps(GtkWidget *widget, gpointer *parts)
  {
    gchar *file_name=save_file(parts, 2);

    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_ps_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, layout_width, layout_height, parts, 2);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name); 
  }
static void draw_png(GtkWidget *widget, gpointer *parts)
  {
    gchar *file_name=save_file(parts, 3);

    if(file_name!=NULL)
      {
        cairo_surface_t *surface=cairo_image_surface_create(CAIRO_FORMAT_ARGB32, layout_width, layout_height);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, layout_width, layout_height, parts, 3);
        cairo_surface_write_to_png(surface, file_name);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name);    
  }
static void draw_surface(cairo_t *cr, gdouble width, gdouble height, gpointer *parts, guint surface_type)
  {
    struct histogram_chart *hc=parts[1];
    gdouble points_scale=0.0;

    //Scale and translate drawing if the surface is in points or pixels.
    if(surface_type<3) points_scale=72.0/ppi;
    else points_scale=1.0;

    //Background.
    cairo_set_source_rgba(cr, background_color[0], background_color[1], background_color[2], background_color[3]);
    cairo_paint(cr); 

    cairo_translate(cr, layout_width*points_scale/2.0, layout_height*points_scale/2.0);
    cairo_translate(cr, translate_x, translate_y);
    cairo_scale(cr, scale*points_scale+0.01, scale*points_scale+0.01);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);        
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_set_line_width(cr, 1.0);

    //Rotation.
    gdouble qrs[16];
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    //Set chart rotation.
    histo_quaternion_rotation(yaw1, roll1, pitch1, qrs);

    //Mesh rotation.
    gdouble qrs2[9];
    set_rotation3d(yaw1, roll1, pitch1);
    quaternion_rotation(yaw1, roll1, pitch1, qrs2);

    //Perspective.
    gdouble pm[16];
    gdouble out[16];
    gdouble near=1.0;
    gdouble far=1.0+perspective*0.01;
    set_projection_matrix(pm, 90.0, near, far);  
    histo_matrix_multiply(qrs, pm, out);

    //Draw the chart.
    draw_chart_with_mesh(hc, cr, qrs2, out, parts);
  }
static struct histogram_chart* initialize_histogram_chart(guint rows, guint columns, gdouble outside_square)
  {
    struct histogram_chart *hc=histogram_chart_new(rows, columns, outside_square);

    //Reduce the back height. Default back height is the width of rows.
    histogram_chart_set_back_height(hc, hc->back_height/2.0);

    //Set histogram base square colors.
    gint i=0;
    gdouble total=rows*columns;
    gdouble rgba[4]={0.0, 0.0, 0.0, 1.0};
    for(i=0;i<total;i++)
      {
        if(i%2==0)
          {
            rgba[1]=0.0;rgba[2]=1.0;
          }
        else
          {
            rgba[1]=1.0;rgba[2]=1.0;
          }
        histogram_chart_set_base_color(hc, i, rgba);
      }
    
    return hc;
  }
static void draw_chart_with_mesh(struct histogram_chart *hc, cairo_t *cr, gdouble qrs1[9], gdouble qrs2[16], gpointer *parts)
  {
    gdouble x=0.0;
    gdouble y=0.0;
    gdouble z=0.0;
    histogram_chart_reset_base(hc);
    histogram_chart_rotate_base(hc, qrs2);

    set_revolve_standard_point(0.0, 0.0, 100.0);
    get_revolved_point(&x, &y, &z);   
    revolve_standard_point(qrs1);
    get_revolved_point(&x, &y, &z);    

    if(hc->rz_t[2]>0.0)
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_base(hc, cr); 
 
        //Draw reference vectors.
        cairo_set_line_width(cr, 8.0);
        cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
        cairo_move_to(cr, 0.0, 0.0);
        cairo_line_to(cr, 50.0*(hc->rx_t[0]), 50.0*(hc->rx_t[1]));
        cairo_stroke(cr);
        cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
        cairo_move_to(cr, 0.0, 0.0);
        cairo_line_to(cr, 50.0*(hc->ry_t[0]), 50.0*(hc->ry_t[1]));
        cairo_stroke(cr);
        cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
        cairo_move_to(cr, 0.0, 0.0);
        cairo_line_to(cr, 50.0*(hc->rz_t[0]), 50.0*(hc->rz_t[1]));
        cairo_stroke(cr);
 
        cairo_set_line_width(cr, 2.0);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
        cairo_save(cr);
        cairo_translate(cr, x, y);
        if(drawing_id==0) draw_wire_bezier_sheet(cr, 16, 16);
        else if(drawing_id==1) draw_solid_bezier_sheet(cr, 16, 16);
        else if(drawing_id==2) draw_solid_bezier_sheet_mesh(cr, 16, 16);
        else if(drawing_id==3) draw_wire_bezier_disc(cr, 16, 16);
        else if(drawing_id==4) draw_solid_bezier_disc(cr, 16, 16);
        else if(drawing_id==5) draw_solid_bezier_disc_mesh(cr, 16, 16);
        else if(drawing_id==6) draw_shapes1(cr, qrs1); 
        else if(drawing_id==7) draw_shapes2(cr, qrs1); 
        else if(drawing_id==8) draw_shapes3(cr, qrs1);
        else if(drawing_id==9) draw_shapes4(cr, qrs1);
        else if(drawing_id==10) draw_shapes5(cr, qrs1); 
        else if(drawing_id==11) draw_pie_charts(cr, qrs1, parts); 
        else if(drawing_id==12)
          {
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT);
            draw_text3d((struct text3d*)parts[4], cr, qrs1);
            if(FALSE) draw_bounding_boxes((struct text3d*)parts[4], cr, qrs1);
          }
        else if(drawing_id==13)
          {
            //The light position.
            gdouble light[3];
            gdouble plane=0.0;
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT);
            set_revolve_standard_point(0.0, -500.0, 500.0);
            revolve_standard_point(qrs1);
            get_revolved_point(&light[0], &light[1], &light[2]);         
            //Set the shadow plane with the chart.
            gdouble plane_points[9]={-1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, -1.0, 0.0};
            set_plane_points(plane_points);
            plane=sqrt(100.0*100.0+100.0*100.0);
            draw_shadow_text((struct text2d*)parts[5], cr, qrs1, light, plane);
            set_revolve_standard_point(-1000.0, 2000.0, 2000.0);
            revolve_standard_point(qrs1);
            get_revolved_point(&light[0], &light[1], &light[2]);
            plane=sqrt(50.0*50.0+100.0*100.0+100.0*100.0);
            draw_shadow_text((struct text2d*)parts[5], cr, qrs1, light, plane);   
          }    
        else g_print("Draw Error\n");
        cairo_restore(cr);

        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
      }
    else
      {
        if(hc->ry_t[2]>0.0)
          {
            histogram_chart_draw_back(hc, cr);
          }
        if(hc->rx_t[2]>0.0) histogram_chart_draw_side(hc, cr); 
        cairo_set_line_width(cr, 2.0);
        cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); 

        cairo_save(cr);
        cairo_translate(cr, x, y);
        if(drawing_id==0) draw_wire_bezier_sheet(cr, 16, 16);
        else if(drawing_id==1) draw_solid_bezier_sheet(cr, 16, 16);
        else if(drawing_id==2) draw_solid_bezier_sheet_mesh(cr, 16, 16);
        else if(drawing_id==3) draw_wire_bezier_disc(cr, 16, 16);
        else if(drawing_id==4) draw_solid_bezier_disc(cr, 16, 16);
        else if(drawing_id==5) draw_solid_bezier_disc_mesh(cr, 16, 16);
        else if(drawing_id==6) draw_shapes1(cr, qrs1);
        else if(drawing_id==7) draw_shapes2(cr, qrs1);
        else if(drawing_id==8) draw_shapes3(cr, qrs1); 
        else if(drawing_id==9) draw_shapes4(cr, qrs1);
        else if(drawing_id==10) draw_shapes5(cr, qrs1);
        else if(drawing_id==11) draw_pie_charts(cr, qrs1, parts); 
        else if(drawing_id==12)
          {
            draw_text3d((struct text3d*)parts[4], cr, qrs1);
            if(FALSE) draw_bounding_boxes((struct text3d*)parts[4], cr, qrs1);
          }
        else if(drawing_id==13)
          {
            gdouble light[3];
            gdouble plane=0.0;
            set_revolve_standard_point(0.0, -500.0, 500.0);
            revolve_standard_point(qrs1);
            get_revolved_point(&light[0], &light[1], &light[2]);         
            gdouble plane_points[9]={-1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, -1.0, 0.0};
            set_plane_points(plane_points);
            plane=sqrt(100.0*100.0+100.0*100.0);
            draw_shadow_text((struct text2d*)parts[5], cr, qrs1, light, plane);
            set_revolve_standard_point(-1000.0, 2000.0, 2000.0);
            revolve_standard_point(qrs1);
            get_revolved_point(&light[0], &light[1], &light[2]);
            plane=sqrt(50.0*50.0+100.0*100.0+100.0*100.0);
            draw_shadow_text((struct text2d*)parts[5], cr, qrs1, light, plane);  
          }             
        else g_print("Draw Error\n");
        cairo_restore(cr);

        if(hc->ry_t[2]<=0.0)
          {
            histogram_chart_draw_back(hc, cr);
          }
        if(hc->rx_t[2]<=0.0) histogram_chart_draw_side(hc, cr);
        histogram_chart_draw_base(hc, cr);        
      }
  }
static void draw_shapes1(cairo_t *cr, gdouble qrs[9])
  {
    //Draw and z sort some shapes.
    gint i=0;
    struct z_sort temp;
    GArray *positions=g_array_new(FALSE, FALSE, sizeof(struct z_sort));

    temp.id=0;
    set_revolve_standard_point(0.0, 0.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=1;
    set_revolve_standard_point(150.0, -100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);
  
    temp.id=2;
    set_revolve_standard_point(150.0, 100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=3;
    set_revolve_standard_point(-150.0, 100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=4;
    set_revolve_standard_point(-150.0, -100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    g_array_sort(positions, compare_z);
    struct z_sort *p1=&g_array_index(positions, struct z_sort, 0);
    for(i=0;i<positions->len;i++)
      {
        if((p1->id)==0)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            cairo_scale(cr, 0.5, 0.5);
            draw_solid_bezier_disc_mesh(cr, 16, 16);
            cairo_restore(cr);
          }
        else if((p1->id)==1)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_solid_sphere(cr, 50.0, 50.0, 16, 16);
            cairo_restore(cr);
          }
        else if((p1->id)==2)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_cube(cr, 40.0, 40.0);
            cairo_restore(cr);
          }
        else if((p1->id)==3)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_solid_torus(cr, 60.0, 60.0, 16, 16);
            cairo_restore(cr);
          }
        else if((p1->id)==4)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_text_ring(cr, 60.0, 60.0, 16);
            cairo_restore(cr);
          }
        else g_print("Error.\n");
        p1++;
      }

    g_array_free(positions, TRUE);           
  }
static void draw_shapes2(cairo_t *cr, gdouble qrs[9])
  {
    gint i=0;
    struct z_sort temp;
    GArray *positions=g_array_new(FALSE, FALSE, sizeof(struct z_sort));

    temp.id=0;
    set_revolve_standard_point(0.0, 0.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=1;
    set_revolve_standard_point(150.0, -100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);
  
    temp.id=2;
    set_revolve_standard_point(150.0, 100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=3;
    set_revolve_standard_point(-150.0, 100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=4;
    set_revolve_standard_point(-150.0, -100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    g_array_sort(positions, compare_z);
    struct z_sort *p1=&g_array_index(positions, struct z_sort, 0);
    for(i=0;i<positions->len;i++)
      {
        if((p1->id)==0)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_spring(cr, 50.0, 50.0);
            cairo_restore(cr);
          }
        else if((p1->id)==1)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            set_transparency(TRUE);
            draw_transparent_cube(cr, 50.0, 50.0);
            set_transparency(FALSE);
            cairo_restore(cr);
          }
        else if((p1->id)==2)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_checkerboard(cr, 50.0, 50.0, 8, 8);
            cairo_restore(cr);
          }
        else if((p1->id)==3)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_ball(cr, 50.0, 50.0, 16, 16);
            cairo_restore(cr);
          }
        else if((p1->id)==4)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_wire_ball(cr, 50.0, 50.0, 16, 16);
            cairo_restore(cr);
          }
        else g_print("Error.\n");
        p1++;
      }

    g_array_free(positions, TRUE);           
  }
static void draw_shapes3(cairo_t *cr, gdouble qrs[9])
  {
    gint i=0;
    struct z_sort temp;
    GArray *positions=g_array_new(FALSE, FALSE, sizeof(struct z_sort));

    temp.id=0;
    set_revolve_standard_point(0.0, 0.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=1;
    set_revolve_standard_point(150.0, -100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);
  
    temp.id=2;
    set_revolve_standard_point(150.0, 100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=3;
    set_revolve_standard_point(-150.0, 100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=4;
    set_revolve_standard_point(-150.0, -100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    g_array_sort(positions, compare_z);
    struct z_sort *p1=&g_array_index(positions, struct z_sort, 0);
    for(i=0;i<positions->len;i++)
      {
        if((p1->id)==0)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_wire_ball2(cr, 50.0, 50.0, 16, 16);
            cairo_restore(cr);
          }
        else if((p1->id)==1)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_wire_sphere(cr, 50.0, 50.0, 16, 16);
            cairo_restore(cr);
          }
        else if((p1->id)==2)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_wire_funnel(cr, 50.0, 50.0, 16, 16);
            cairo_restore(cr);
          }
        else if((p1->id)==3)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_solid_funnel(cr, 50.0, 50.0, 16, 16);
            cairo_restore(cr);
          }
        else if((p1->id)==4)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            cairo_set_line_width(cr, 1.0);
            draw_gem(cr, 50.0, 50.0);
            cairo_restore(cr);
          }
        else g_print("Error.\n");
        p1++;
      }

    g_array_free(positions, TRUE);           
  }
static void draw_shapes4(cairo_t *cr, gdouble qrs[9])
  {
    gint i=0;
    struct z_sort temp;
    GArray *positions=g_array_new(FALSE, FALSE, sizeof(struct z_sort));

    temp.id=0;
    set_revolve_standard_point(0.0, 0.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=1;
    set_revolve_standard_point(150.0, -100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);
  
    temp.id=2;
    set_revolve_standard_point(150.0, 100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=3;
    set_revolve_standard_point(-150.0, 100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=4;
    set_revolve_standard_point(-150.0, -100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=5;
    set_revolve_standard_point(0.0, -100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    g_array_sort(positions, compare_z);
    struct z_sort *p1=&g_array_index(positions, struct z_sort, 0);
    for(i=0;i<positions->len;i++)
      {
        if((p1->id)==0)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_wire_torus(cr, 50.0, 50.0, 16, 16);
            cairo_restore(cr);
          }
        else if((p1->id)==1)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_wire_twist(cr, 25.0, 25.0, 17);
            cairo_restore(cr);
          }
        else if((p1->id)==2)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_solid_twist(cr, 25.0, 25.0, 17);
            cairo_restore(cr);
          }
        else if((p1->id)==3)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_pyramid(cr, 50.0, 50.0);
            cairo_restore(cr);
          }
        else if((p1->id)==4)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_cone(cr, 50.0, 50.0, 8, 16);
            cairo_restore(cr);
          }
        else if((p1->id)==5)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_cone_shade(cr, 50.0, 50.0, 8, 16);
            cairo_restore(cr);
          }
        else g_print("Error.\n");
        p1++;
      }

    g_array_free(positions, TRUE);           
  }
static void draw_shapes5(cairo_t *cr, gdouble qrs[9])
  {
    gint i=0;
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    struct z_sort temp;
    GArray *positions=g_array_new(FALSE, FALSE, sizeof(struct z_sort));

    temp.id=0;
    set_revolve_standard_point(-125.0, 0.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=1;
    set_revolve_standard_point(125.0, 0.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=2;
    set_revolve_standard_point(-20.0, 100.0, -20.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=3;
    set_revolve_standard_point(0.0, -100.0, 75.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    g_array_sort(positions, compare_z);
    struct z_sort *p1=&g_array_index(positions, struct z_sort, 0);
    for(i=0;i<positions->len;i++)
      {
        if((p1->id)==0)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            set_rotation3d(yaw1, roll1, pitch1-G_PI/2.0);
            draw_fish(cr, 20.0, 20.0);
            set_rotation3d(yaw1, roll1, pitch1);
            cairo_restore(cr);
          }
        else if((p1->id)==1)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_turtle(cr, 25.0, 25.0);
            cairo_restore(cr);
          }
        else if((p1->id)==2)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            set_rotation3d(yaw1, roll1, pitch1-G_PI/2.0);
            draw_fish(cr, 20.0, 20.0);
            set_rotation3d(yaw1, roll1, pitch1);
            cairo_restore(cr);
          }  
        else if((p1->id)==3)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            set_rotation3d(yaw1, roll1, pitch1-G_PI/2.0);
            draw_fish(cr, 20.0, 20.0);
            set_rotation3d(yaw1, roll1, pitch1);
            cairo_restore(cr);
          }      
        else g_print("Error.\n");
        p1++;
      }

    g_array_free(positions, TRUE);           
  }
static void draw_bounding_boxes(struct text3d *t3d,  cairo_t *cr, gdouble qrs[9])
  {
    //Check the text layout with bounding boxes.
    gint i=0;
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_set_line_width(cr, 8.0);

    cairo_save(cr);
    cairo_matrix_t matrix1;
    cairo_matrix_init(&matrix1, qrs[0], qrs[3], qrs[1], qrs[4], 0.0, 0.0);
    cairo_transform(cr, &matrix1);
    gdouble total_width=0.0;
    gdouble max_height=0.0;
    for(i=0;i<(t3d->box_size->len);i+=2)
      {
        total_width+=g_array_index(t3d->box_size, gdouble, i);
        if(max_height<g_array_index(t3d->box_size, gdouble, i+1))
          {
            max_height=g_array_index(t3d->box_size, gdouble, i+1);
          }
      }

    cairo_set_line_width(cr, 6.0);
    gdouble move_x=-total_width/2.0-6.0*(t3d->spacing);
    gdouble move_y=0.0;
    gdouble height=0.0;
    for(i=0;i<(t3d->box_size->len); i+=2)
      {
        move_x+=g_array_index(t3d->box_size, gdouble, i)/2.0+(t3d->spacing); 
        height=g_array_index(t3d->box_size, gdouble, i+1);
        move_y=(max_height-height)/2.0;
        cairo_rectangle(cr, move_x-g_array_index(t3d->box_size, gdouble, i)/2.0, move_y-g_array_index(t3d->box_size, gdouble, i+1)/2.0, g_array_index(t3d->box_size, gdouble, i), g_array_index(t3d->box_size, gdouble, i+1));
        cairo_stroke(cr);
        move_x+=g_array_index(t3d->box_size, gdouble, i)/2.0+(t3d->spacing);
      } 
    cairo_restore(cr);
  }
static void draw_text3d(struct text3d *t3d, cairo_t *cr, gdouble qrs[9])
  {
    gint i=0;
    GArray *path1=NULL;
    GArray *path2=NULL;
    
    //Reset transformed text arrays.
    reset_text3d(t3d);

    //Rotate text.
    for(i=0;i<(t3d->cairo_text_t->len); i++)
      {
        path1=(GArray*)g_ptr_array_index(t3d->cairo_text_t, i);
        if(path1!=NULL) transform_letter(path1, qrs);
      }
  
    //Check the direction the font is facing. This isn't from cairo3d_text.
    gdouble top[3];
    set_revolve_standard_point(1.0, 0.0, 0.0);
    revolve_standard_point(qrs);
    get_revolved_point(&top[0], &top[1], &top[2]);

    //Draw the 3d text
    //g_print("%s\n", t3d->text->str);
    gdouble rgba1[4]={0.0, 1.0, 0.0, 0.7};
    gdouble rgba2[4]={0.0, 1.0, 0.0, 1.0};
    if(top[2]>=0.0)
      {
        //Draw left to right.
        for(i=0;i<(t3d->cairo_text_t->len); i+=2)
          {
            path1=(GArray*)g_ptr_array_index(t3d->cairo_text_t, i);
            path2=(GArray*)g_ptr_array_index(t3d->cairo_text_t, i+1);
       
            if(path1!=NULL&&path2!=NULL) draw_char(cr, path1, path2, rgba1, rgba2);

            //Some different colors for the letters.
            if(i==0)
              {
                rgba1[0]=0.0;rgba1[1]=0.0;rgba1[2]=1.0;rgba1[3]=0.7;
                rgba2[0]=0.0;rgba2[1]=0.0;rgba2[2]=1.0;rgba2[3]=1.0;
              }
            else if(i==2)
              {
                rgba1[0]=1.0;rgba1[1]=1.0;rgba1[2]=0.0;rgba1[3]=0.7;
                rgba2[0]=1.0;rgba2[1]=1.0;rgba2[2]=0.0;rgba2[3]=1.0;
              }
            else if(i==4)
              {
                rgba1[0]=0.0;rgba1[1]=1.0;rgba1[2]=1.0;rgba1[3]=0.7;
                rgba2[0]=0.0;rgba2[1]=1.0;rgba2[2]=1.0;rgba2[3]=1.0;
              }
            else if(i==6)
              {
                rgba1[0]=1.0;rgba1[1]=0.0;rgba1[2]=1.0;rgba1[3]=0.7;
                rgba2[0]=1.0;rgba2[1]=0.0;rgba2[2]=1.0;rgba2[3]=1.0;
              }
            else if(i==8)
              {
                rgba1[0]=1.0;rgba1[1]=0.0;rgba1[2]=0.0;rgba1[3]=0.7;
                rgba2[0]=1.0;rgba2[1]=0.0;rgba2[2]=0.0;rgba2[3]=1.0;
              }
            else if(i==10)
              {
                rgba1[0]=1.0;rgba1[1]=0.7;rgba1[2]=0.0;rgba1[3]=0.7;
                rgba2[0]=1.0;rgba2[1]=0.7;rgba2[2]=0.0;rgba2[3]=1.0;
              }
            else
              {
                rgba1[0]=1.0;rgba1[1]=0.0;rgba1[2]=0.0;rgba1[3]=0.7;
                rgba2[0]=1.0;rgba2[1]=0.0;rgba2[2]=0.0;rgba2[3]=1.0;
              }
          }
      }
    else
      {
        //Draw right to left.
        rgba1[0]=1.0;rgba1[1]=0.7;rgba1[2]=0.0;rgba1[3]=0.7;
        rgba2[0]=1.0;rgba2[1]=0.7;rgba2[2]=0.0;rgba2[3]=1.0;
        gint start=(t3d->cairo_text_t->len)-1;
        for(i=start;i>=0;i-=2)
          {
            path1=(GArray*)g_ptr_array_index(t3d->cairo_text_t, i-1);
            path2=(GArray*)g_ptr_array_index(t3d->cairo_text_t, i);
       
            if(path1!=NULL&&path2!=NULL) draw_char(cr, path1, path2, rgba1, rgba2);

            if(i==3)
              {
                rgba1[0]=0.0;rgba1[1]=1.0;rgba1[2]=0.0;rgba1[3]=0.7;
                rgba2[0]=0.0;rgba2[1]=1.0;rgba2[2]=0.0;rgba2[3]=1.0;
              }
            else if(i==5)
              {
                rgba1[0]=0.0;rgba1[1]=0.0;rgba1[2]=1.0;rgba1[3]=0.7;
                rgba2[0]=0.0;rgba2[1]=0.0;rgba2[2]=1.0;rgba2[3]=1.0;
              }
            else if(i==7)
              {
                rgba1[0]=1.0;rgba1[1]=1.0;rgba1[2]=0.0;rgba1[3]=0.7;
                rgba2[0]=1.0;rgba2[1]=1.0;rgba2[2]=0.0;rgba2[3]=1.0;
              }
            else if(i==9)
              {
                rgba1[0]=0.0;rgba1[1]=1.0;rgba1[2]=1.0;rgba1[3]=0.7;
                rgba2[0]=0.0;rgba2[1]=1.0;rgba2[2]=1.0;rgba2[3]=1.0;
              }
            else if(i==11)
              {
                rgba1[0]=1.0;rgba1[1]=0.0;rgba1[2]=1.0;rgba1[3]=0.7;
                rgba2[0]=1.0;rgba2[1]=0.0;rgba2[2]=1.0;rgba2[3]=1.0;
              }
            else if(i==13)
              {
                rgba1[0]=1.0;rgba1[1]=0.0;rgba1[2]=0.0;rgba1[3]=0.7;
                rgba2[0]=1.0;rgba2[1]=0.0;rgba2[2]=0.0;rgba2[3]=1.0;
              }
            else
              {
                rgba1[0]=1.0;rgba1[1]=0.0;rgba1[2]=0.0;rgba1[3]=0.7;
                rgba2[0]=1.0;rgba2[1]=0.0;rgba2[2]=0.0;rgba2[3]=1.0;
              }
          }
      } 
  }
static void draw_shadow_text(struct text2d *t2d, cairo_t *cr, gdouble qrs[9], gdouble light_position[3], gdouble plane_distance)
  {
    gint i=0;
    GArray *path1=NULL;
    
    //Reset transformed text arrays.
    reset_text2d(t2d);

    //Rotate text.
    for(i=0;i<(t2d->cairo_text_t->len); i++)
      {
        path1=(GArray*)g_ptr_array_index(t2d->cairo_text_t, i);
        if(path1!=NULL) transform_letter(path1, qrs);
      }

    //Check the direction the font is facing. This isn't from cairo3d_text.
    gdouble top[3];
    set_revolve_standard_point(1.0, 0.0, 0.0);
    revolve_standard_point(qrs);
    get_revolved_point(&top[0], &top[1], &top[2]);
  
    //Draw the shadow text.
    //g_print("%s\n", t2d->text->str);
    gdouble rgba1[4]={1.0, 1.0, 0.0, 0.7};
    gdouble rgba2[4]={1.0, 1.0, 0.0, 1.0};
    if(top[2]>=0.0)
      {
        //Draw left to right.
        for(i=0;i<(t2d->cairo_text_t->len); i++)
          {
            path1=(GArray*)g_ptr_array_index(t2d->cairo_text_t, i);
            if(path1!=NULL)
              {
                GArray *shadow=get_shadow_path(path1, light_position, plane_distance, qrs);
                draw_shadow_char(cr, path1, shadow, rgba1, rgba2);
                g_array_free(shadow, TRUE);
              }
            if((i+1)%2!=0)
              {
                rgba1[0]=1.0;rgba1[1]=0.0;rgba1[2]=1.0;rgba1[3]=0.7;
                rgba2[0]=1.0;rgba2[1]=0.0;rgba2[2]=1.0;rgba2[3]=1.0;
              }
            else
              {
                rgba1[0]=1.0;rgba1[1]=1.0;rgba1[2]=0.0;rgba1[3]=0.7;
                rgba2[0]=1.0;rgba2[1]=1.0;rgba2[2]=0.0;rgba2[3]=1.0;
              }            
          }
      }
    else
      {
        //Draw right to left.
        rgba1[0]=1.0;rgba1[1]=0.0;rgba1[2]=1.0;rgba1[3]=0.7;
        rgba2[0]=1.0;rgba2[1]=0.0;rgba2[2]=1.0;rgba2[3]=1.0;
        gint start=(t2d->cairo_text_t->len)-1;
        for(i=start;i>=0;i--)
          {
            path1=(GArray*)g_ptr_array_index(t2d->cairo_text_t, i);
            if(path1!=NULL)
              {
                GArray *shadow=get_shadow_path(path1, light_position, plane_distance, qrs);
                draw_shadow_char(cr, path1, shadow, rgba1, rgba2);
                g_array_free(shadow, TRUE);
              }
            if((i+1)%2==0)
              {
                rgba1[0]=1.0;rgba1[1]=1.0;rgba1[2]=0.0;rgba1[3]=0.7;
                rgba2[0]=1.0;rgba2[1]=1.0;rgba2[2]=0.0;rgba2[3]=1.0;
              }
            else
              {
                rgba1[0]=1.0;rgba1[1]=0.0;rgba1[2]=1.0;rgba1[3]=0.7;
                rgba2[0]=1.0;rgba2[1]=0.0;rgba2[2]=1.0;rgba2[3]=1.0;
              }
            
          }
      } 
  }
static void draw_char(cairo_t *cr, GArray *path1, GArray *path2, gdouble rgba1[4], gdouble rgba2[4])
  {
    gboolean blur=TRUE;
    gdouble rgba3[4];
    rgba3[0]=rgba1[0];rgba3[1]=rgba1[1];rgba3[2]=rgba1[2];rgba3[3]=0.0;

    if(standard_vector_is_top())
      {
        cairo_set_source_rgba(cr, rgba2[0], rgba2[1], rgba2[2], rgba2[3]);
        draw_path_contours_fill(cr, path2);
        cairo_set_source_rgba(cr, rgba2[0], rgba2[1], rgba2[2], rgba2[3]);
        if(blur) draw_path_side_fill_blur(cr, path1, path2, rgba1, rgba3);               
        else draw_path_side_fill(cr, path1, path2, rgba1);
        cairo_set_source_rgba(cr, rgba2[0], rgba2[1], rgba2[2], rgba2[3]);
        draw_path_contours_fill(cr, path1);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_set_line_width(cr, 8.0);
        draw_path_contours_line(cr, path1, TRUE);
      }
    else
      {
        cairo_set_source_rgba(cr, rgba2[0], rgba2[1], rgba2[2], rgba2[3]);
        draw_path_contours_fill(cr, path1);
        cairo_set_source_rgba(cr, rgba2[0], rgba2[1], rgba2[2], rgba2[3]);
        if(blur) draw_path_side_fill_blur(cr, path1, path2, rgba1, rgba3);
        else draw_path_side_fill(cr, path1, path2, rgba1);
        cairo_set_source_rgba(cr, rgba2[0], rgba2[1], rgba2[2], rgba2[3]);
        draw_path_contours_fill(cr, path2);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        cairo_set_line_width(cr, 8.0);
        draw_path_contours_line(cr, path2, TRUE);
      }
  }
static void draw_shadow_char(cairo_t *cr, GArray *path1, GArray *shadow, gdouble rgba1[4], gdouble rgba2[4])
  {
    gboolean blur=TRUE;
    gboolean shadow_lines=FALSE;
    gdouble rgba3[4];
    rgba3[0]=rgba1[0];rgba3[1]=rgba1[1];rgba3[2]=rgba1[2];rgba3[3]=0.0;

    if(standard_vector_is_top())
      {
        cairo_set_source_rgba(cr, rgba2[0], rgba2[1], rgba2[2], rgba2[3]);
        draw_path_contours_fill(cr, shadow); 
        if(blur) draw_path_side_fill_blur(cr, path1, shadow, rgba1, rgba3);  
        cairo_set_source_rgba(cr, rgba1[0], rgba1[1], rgba1[2], rgba1[3]);
        if(shadow_lines) draw_path_contours_connect(cr, path1, shadow);
        cairo_set_source_rgba(cr, rgba2[0], rgba2[1], rgba2[2], rgba2[3]);
        draw_path_contours_fill(cr, path1);
        cairo_set_line_width(cr, 4.0);
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        draw_path_contours_line(cr, path1, FALSE);
      }
    else
      {
        cairo_set_line_width(cr, 4.0);
        cairo_set_source_rgba(cr, rgba2[0], rgba2[1], rgba2[2], rgba2[3]);
        draw_path_contours_fill(cr, path1); 
        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
        draw_path_contours_line(cr, path1, FALSE);
        if(blur) draw_path_side_fill_blur(cr, path1, shadow, rgba1, rgba3);  
        cairo_set_line_width(cr, 2.0);
        cairo_set_source_rgba(cr, rgba1[0], rgba1[1], rgba1[2], rgba1[3]);
        if(shadow_lines) draw_path_contours_connect(cr, path1, shadow);
        cairo_set_source_rgba(cr, rgba2[0], rgba2[1], rgba2[2], rgba2[3]);
        draw_path_contours_fill(cr, shadow);
      }
         
  }
static void draw_pie_charts(cairo_t *cr, gdouble qrs[9], gpointer *parts)
  {
    gint i=0;
    struct pie_chart *pc1=parts[3];
    struct z_sort temp;
    GArray *positions=g_array_new(FALSE, FALSE, sizeof(struct z_sort));

    temp.id=0;
    set_revolve_standard_point(-100.0, -100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=1;
    set_revolve_standard_point(100.0, -100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);
  
    temp.id=2;
    set_revolve_standard_point(100.0, 100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    temp.id=3;
    set_revolve_standard_point(-100.0, 100.0, 50.0);
    revolve_standard_point(qrs);
    get_revolved_point(&temp.x, &temp.y, &temp.z); 
    g_array_append_val(positions, temp);

    g_array_sort(positions, compare_z);
    struct z_sort *p1=&g_array_index(positions, struct z_sort, 0);
    pc1->outline=TRUE; 
    for(i=0;i<positions->len;i++)
      {
        if((p1->id)==0)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_pie_chart(cr, pc1, qrs, FALSE);
            cairo_restore(cr);
          }
        else if((p1->id)==1)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_pie_chart_ring(cr, pc1, qrs, 0.5, FALSE);
            cairo_restore(cr);
          }
        else if((p1->id)==2)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_pie_chart(cr, pc1, qrs, TRUE);
            cairo_restore(cr);
          }
        else if((p1->id)==3)
          {
            cairo_save(cr);
            cairo_translate(cr, p1->x, p1->y);
            draw_pie_chart_ring(cr, pc1, qrs, 0.5, TRUE);
            cairo_restore(cr);
          }
        else g_print("Error.\n");
        p1++;
      }

    g_array_free(positions, TRUE);
  }
static int compare_z(const void *a, const void *b)
  {
    return(((struct z_sort*)a)->z-((struct z_sort*)b)->z);
  }
static void about_dialog(GtkWidget *widget, gpointer data)
  {
    GtkWidget *dialog=gtk_about_dialog_new();
    gtk_widget_set_name(dialog, "about_dialog");
    gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(data));
    //gtk_about_dialog_set_logo(GTK_ABOUT_DIALOG(dialog), NULL);
    gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(dialog), "Cairo3d Combo");
    gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog), "Test Version 1.0");
    gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(dialog), "3d shape combos with cairo.");
    gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(dialog), "(C) 2019 C. Eric Cashon");

    gtk_widget_show_all(dialog);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
  }
