
/*  
    Compile with cairo3d_UI.c.

    A shape first gets initialized. This creates the points that will be drawn. The basic shape
initialization functions are automatic. They are called if a shape is not currently in the cache
when the shape is drawn for the first time. The bezier sheet, disc and ball need to be initialized
since their points are going to be changed, probably right after initialization. Once a shape is
in the cache it can be redrawn without the overhead of generating original points. Instead the 
shape can just be transformed and drawn again.

    A shape can be made up of points, quads or bezier quads(bezier patches). The quads and bezier
quads get sorted before being drawn. This doesn't work as well as depth testing but the sorted
shape has the advantage that it's on screen appearance can be saved to svg or pdf.

    Test compile.
    gcc -Wall -c cairo3d.c -o cairo3d.o `pkg-config --cflags --libs glib-2.0` `pkg-config --cflags --libs cairo`-lm

    Tested on Ubuntu18.04 and GTK3.22

    C. Eric Cashon
*/

#include"cairo3d.h"
#include<string.h>

//String representations for the shape enumumeration in the header file.
static const char *shape_names[25]={"Spring", "Solid Cylinder", "Cube", "Checkerboard", "Wire Bezier Sheet", "Solid Bezier Sheet", "Ball", "Wire Sphere", "Solid Sphere", "Wire Funnel", "Solid Funnel", "Gem", "Wire Torus", "Solid Torus", "Wire Twist", "Solid Twist", "Pyramid", "Cone", "Text Ring", "Wire Bezier Disc", "Solid Bezier Disc", "Wire Bezier Sphere", "Solid Bezier Sphere", "Fish", "Turtle"}; 

//The array to hold the shapes in.
static GPtrArray *shape_cache=NULL;

//The rotation matrix to rotate the shapes.
static gdouble rm[9]={1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0};

//A spherical point to revolve or rotate on a spherical path with quaternions.
static gdouble revolve[3]={1.0, 0.0, 0.0};
static gdouble revolve_r[3]={1.0, 0.0, 0.0};

//Set if the shape should be drawn with transparency. True also means the whole shape is drawn.
static gboolean transparent=FALSE;

//Initialization functions for some basic shapes that are automatically initialized when drawn.
static void initialize_shape(gint cache_id, gint rings, gint points_per_ring);
static void initialize_spring();
static void initialize_solid_cylinder(gint points_per_ring);
static void initialize_cube();
static void initialize_checkerboard(gint rows, gint coloumns);
static void initialize_ball(gint rings, gint points_per_ring);
static void initialize_wire_sphere(gint rings, gint points_per_ring);
static void initialize_solid_sphere(gint rings, gint points_per_ring);
static void initialize_wire_funnel(gint rings, gint points_per_ring);
static void initialize_solid_funnel(gint rings, gint points_per_ring);
static void initialize_gem();
static void initialize_wire_torus(gint rings, gint points_per_ring);
static void initialize_solid_torus(gint rings, gint points_per_ring);
static void initialize_wire_twist(gint strands);
static void initialize_solid_twist(gint strands);
static void initialize_pyramid();
static void initialize_cone(gint rings, gint points_per_ring);
static void initialize_text_ring(gint points_per_ring);
static void initialize_fish();
static void initialize_turtle();
//Some linear algebra for transforms.
static void matrix_multiply_rotations(const gdouble qrs1[9], const gdouble qrs2[9], gdouble qrs3[9]);
static void rotate_shape(const struct shape *s1, gdouble qrs[9]);
static void rotate_shape_xyz(const struct shape *s1, gdouble qrs[9]);
static void rotate_quad_plane(const struct shape *s1, gdouble qrs[9]);
static void rotate_bezier_quad(const struct shape *s1, gdouble qrs[9]);
//Get a drawing from the cache.
static struct shape* get_drawing_from_shape_cache(gint cache_id, gint rings, gint points_per_ring);
static struct shape* get_drawing_from_shape_cache_index(gint cache_id, gint array_index);
//For comparing z values of planes and bezier quads. Painters algorithm.
static int compare_planes(const void *a, const void *b);
static int compare_quads(const void *a, const void *b);
static int compare_quads2(const void *a, const void *b);
static int compare_quads_cross_product(const void *a, const void *b);
static int compare_quads_bezier(const void *a, const void *b);
static int compare_quads_bezier_cross_product(const void *a, const void *b);
/*
  Get bezier control points. The 2d version takes an array of 3d points
  with z ignored and returns an array of x and y control points.
  The 3d version takes an array of 3d points and returns an array of 
  x, y and z bezier control points.
*/
static GArray* control_points_from_coords_2d(const GArray *data_points3d);
static GArray* control_points_from_coords_3d(const GArray *data_points3d);
//Generate a ring or circle with bezier control points.
static GArray* get_bezier_ring(gdouble radius, gint points_per_ring, gboolean half_ring);
//HSV colors for a mesh gradient.
static void hsv2rgb(double h, double s, double v, double *r, double *g, double *b);
//Set quad points for the bezier sphere drawing.
static void set_tri_points_1(struct bezier_quad *q1, struct point3d *p1, struct point3d *p2, struct point3d *p3);
static void set_tri_points_2(struct bezier_quad *q1, struct point3d *p1, struct point3d *p2, struct point3d *p3);
static void set_quad_points(struct bezier_quad *q1, struct point3d *p1, struct point3d *p2, struct point3d *p3, struct point3d *p4);

void start_shape_cache()
  {
    shape_cache=g_ptr_array_new();
  }
guint64 bytes_in_shape_cache()
  {
    gint i=0;
    gint length=shape_cache->len;
    struct shape *p=NULL;
    guint64 bytes=0;
    guint struct_shape=sizeof(struct shape);
    for(i=0;i<length;i++)
      {
        p=g_ptr_array_index(shape_cache, 0);
        bytes+=2*(p->s->len)*sizeof(struct point3d)+struct_shape;        
      }
    return bytes;
  }
gint shapes_in_shape_cache()
  {
    return shape_cache->len;
  }
void print_shapes_in_cache()
  {
    guint i=0;
    
    if((shape_cache->len)>0)
      {
        struct shape *s1=g_ptr_array_index(shape_cache, 0);
        for(i=0;i<(shape_cache->len);i++)
          {
            s1=g_ptr_array_index(shape_cache, i);
            g_print("ShapeID %i, %s, Rings %i Points Per Ring %i\n", s1->cache_id, shape_names[s1->cache_id], s1->rings, s1->points_per_ring);      
          }
      }
    else
      {
        g_print("Empty Shape Cache\n");
      }

  }
void clear_shape_cache()
  {
    gint i=0;
    gint length=shape_cache->len;
    struct shape *p=NULL;
    for(i=0;i<length;i++)
      {
        p=g_ptr_array_index(shape_cache, 0);
        g_array_free(p->s, TRUE);
        g_array_free(p->sr, TRUE);
        g_free(p);
        g_ptr_array_remove_index_fast(shape_cache, 0);
      }
  }
void free_shape_cache()
  {
    clear_shape_cache();
    g_ptr_array_free(shape_cache, TRUE);
  }
void set_rotation3d(gdouble yaw, gdouble roll, gdouble pitch)
  {
    quaternion_rotation(yaw, roll, pitch, rm);
  }
void set_rotation3d_identity()
  {
    rm[0]=1.0;rm[1]=0.0;rm[2]=0.0;
    rm[3]=0.0;rm[4]=1.0;rm[5]=0.0;
    rm[6]=0.0;rm[7]=0.0;rm[8]=1.0;
  }
static void initialize_shape(gint cache_id, gint rings, gint points_per_ring)
  {
    switch(cache_id)
      {
        case SPRING:
          initialize_spring();
          break;
        case SOLID_CYLINDER:
          initialize_solid_cylinder(points_per_ring);
        case CUBE:
          initialize_cube();
          break;
        case CHECKERBOARD:
          initialize_checkerboard(rings, points_per_ring);
          break;
        case WIRE_BEZIER_SHEET:
          g_warning("The wire bezier sheet needs to be initialized manually in code.\n");
          break;
        case SOLID_BEZIER_SHEET:
          g_warning("The solid bezier sheet needs to be initialized manually in code.\n");
          break;
        case BALL:
          initialize_ball(rings, points_per_ring);
          break;
        case WIRE_SPHERE:
          initialize_wire_sphere(rings, points_per_ring);
          break;
        case SOLID_SPHERE:
          initialize_solid_sphere(rings, points_per_ring);
          break;
        case WIRE_FUNNEL:
          initialize_wire_funnel(rings, points_per_ring);
          break;
        case SOLID_FUNNEL:
          initialize_solid_funnel(rings, points_per_ring);
        case GEM:
          initialize_gem();
          break;
        case WIRE_TORUS:
          initialize_wire_torus(rings, points_per_ring);
          break;
        case SOLID_TORUS:
          initialize_solid_torus(rings, points_per_ring);
          break;
        case WIRE_TWIST:
          initialize_wire_twist(points_per_ring);
          break;
        case SOLID_TWIST:
          initialize_solid_twist(points_per_ring);
          break;
        case PYRAMID:
          initialize_pyramid();
          break;
        case CONE:
          initialize_cone(rings, points_per_ring);
          break;
        case TEXT_RING:
          initialize_text_ring(points_per_ring);
          break;
        case WIRE_BEZIER_DISC:
          g_warning("The wire bezier disc needs to be initialized manually in code.\n");
          break;
        case SOLID_BEZIER_DISC:
          g_warning("The solid bezier disc needs to be initialized manually in code.\n");
          break;
        case WIRE_BEZIER_SPHERE:
          g_warning("The wire bezier sphere needs to be initialized manually in code.\n");
          break;
        case SOLID_BEZIER_SPHERE:
          g_warning("The solid bezier sphere needs to be initialized manually in code.\n");
          break;
        case FISH:
          initialize_fish();
          break; 
        case TURTLE:
          initialize_turtle();
          break; 
        default:
          g_print("Error: Undefined Shape!\n");
      }
  }
static void initialize_spring()
  {
    gint i=0;
    gdouble radius=0.20;
    GArray *spring=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 80);
    g_array_set_size(spring, 80);
    GArray *spring_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 80);
    g_array_set_size(spring_rotated, 80);

    struct point3d *p1=&g_array_index(spring, struct point3d, 0);
    for(i=0;i<80;i++)
      {
        (*p1).x=radius*cos(45.0*(gdouble)i*G_PI/180.0);
        (*p1).y=radius*sin(45.0*(gdouble)i*G_PI/180.0);
        (*p1).z=(gdouble)i/40.0;
        radius+=0.01;
        p1++;
      }

    p1=&g_array_index(spring, struct point3d, 0);
    struct point3d *p2=&g_array_index(spring_rotated, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(spring->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=SPRING;
    p->s=spring;
    p->sr=spring_rotated;
    p->rings=0;
    p->points_per_ring=0;
    p->default_drawing=TRUE;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
static void initialize_solid_cylinder(gint points_per_ring)
  {
    //Only 2 rings in the cylinder.
    gint i=0;
    gint j=0;
    gboolean def=FALSE;

    if(points_per_ring<10||points_per_ring>50)
      {
        points_per_ring=16;
        def=TRUE;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=16.\n");
      }

    GArray *ring=get_bezier_ring(1.0, points_per_ring, FALSE);

    gint points=2*4*(ring->len);
    GArray *cylinder=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), points);
    g_array_set_size(cylinder, points);    
    
    struct arc *p1=NULL;
    struct point3d *p2=&g_array_index(cylinder, struct point3d, 0);
    gdouble z_value=1.0;
    for(i=0;i<2;i++)
      {
        p1=&g_array_index(ring, struct arc, 0);
        for(j=0;j<ring->len;j++)
          {
            (p2->x)=(p1->a1).x;
            (p2->y)=(p1->a1).y;
            (p2->z)=z_value;
            p2++;
            (p2->x)=(p1->a2).x;
            (p2->y)=(p1->a2).y;
            (p2->z)=z_value;
            p2++;
            (p2->x)=(p1->a3).x;
            (p2->y)=(p1->a3).y;
            (p2->z)=z_value;
            p2++;
            (p2->x)=(p1->a4).x;
            (p2->y)=(p1->a4).y;
            (p2->z)=z_value;            
            p1++;p2++;
          }
        z_value=-1.0;
      }

    g_array_free(ring, TRUE);

    struct point3d *p3=&g_array_index(cylinder, struct point3d, 0);
    struct point3d *p4=&g_array_index(cylinder, struct point3d, points_per_ring*4);

    gint quad_len=(cylinder->len)/8;
    GArray *quad=g_array_sized_new(FALSE, TRUE, sizeof(struct bezier_quad), quad_len);
    g_array_set_size(quad, quad_len);
    GArray *quad_r=g_array_sized_new(FALSE, TRUE, sizeof(struct bezier_quad), quad_len);
    g_array_set_size(quad_r, quad_len);
    struct bezier_quad *q1=&g_array_index(quad, struct bezier_quad, 0);

    gint len=(cylinder->len)/2;
    gint counter=0;
    for(i=0;i<len;i+=4)
      {
        (q1->x1)=(p3->x); 
        (q1->y1)=(p3->y);
        (q1->z1)=(p3->z);
        (q1->bx1)=((p3+1)->x);
        (q1->by1)=((p3+1)->y);
        (q1->bz1)=((p3+1)->z);
        (q1->bx2)=((p3+2)->x);
        (q1->by2)=((p3+2)->y);
        (q1->bz2)=((p3+2)->z);
        (q1->x2)=((p3+3)->x); 
        (q1->y2)=((p3+3)->y);
        (q1->z2)=((p3+3)->z);
        //line points.
        (q1->bx3)=((p3+3)->x);
        (q1->by3)=((p3+3)->y);
        (q1->bz3)=((p3+3)->z);
        (q1->bx4)=((p4+3)->x);
        (q1->by4)=((p4+3)->y);
        (q1->bz4)=((p4+3)->z);
        (q1->x3)=((p4+3)->x); 
        (q1->y3)=((p4+3)->y);
        (q1->z3)=((p4+3)->z);
        (q1->bx5)=((p4+2)->x);
        (q1->by5)=((p4+2)->y);
        (q1->bz5)=((p4+2)->z);
        (q1->bx6)=((p4+1)->x);
        (q1->by6)=((p4+1)->y);
        (q1->bz6)=((p4+1)->z);
        (q1->x4)=(p4->x); 
        (q1->y4)=(p4->y);
        (q1->z4)=(p4->z);
        (q1->bx7)=(p4->x);
        (q1->by7)=(p4->y);
        (q1->bz7)=(p4->z);
        (q1->bx8)=(p3->x);
        (q1->by8)=(p3->y);
        (q1->bz8)=(p3->z);  
        q1->index=counter;      
        p3+=4;p4+=4;q1++;counter++;    
      }

    g_array_free(cylinder, TRUE);

    q1=&g_array_index(quad, struct bezier_quad, 0);
    struct bezier_quad *q2=&g_array_index(quad_r, struct bezier_quad, 0);
    memcpy(q2, q1, sizeof(struct bezier_quad)*(quad->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=SOLID_CYLINDER;
    p->s=quad;
    p->sr=quad_r;
    p->rings=0;
    p->points_per_ring=points_per_ring;
    p->default_drawing=def;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
static void initialize_cube()
  {
    GArray *cube=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 8);
    g_array_set_size(cube, 8);
    GArray *cube_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 8);
    g_array_set_size(cube_rotated, 8);

    struct point3d *p1=&g_array_index(cube, struct point3d, 0);
    (*p1).x=1.0;
    (*p1).y=1.0;
    (*p1).z=1.0;
    (*(p1+1)).x=1.0;
    (*(p1+1)).y=-1.0;
    (*(p1+1)).z=1.0;
    (*(p1+2)).x=-1.0;
    (*(p1+2)).y=-1.0;
    (*(p1+2)).z=1.0;
    (*(p1+3)).x=-1.0;
    (*(p1+3)).y=1.0;
    (*(p1+3)).z=1.0;
    (*(p1+4)).x=1.0;
    (*(p1+4)).y=1.0;
    (*(p1+4)).z=-1.0;
    (*(p1+5)).x=1.0;
    (*(p1+5)).y=-1.0;
    (*(p1+5)).z=-1.0;
    (*(p1+6)).x=-1.0;
    (*(p1+6)).y=-1.0;
    (*(p1+6)).z=-1.0;
    (*(p1+7)).x=-1.0;
    (*(p1+7)).y=1.0;
    (*(p1+7)).z=-1.0;

    p1=&g_array_index(cube, struct point3d, 0);
    struct point3d *p2=&g_array_index(cube_rotated, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(cube->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=CUBE;
    p->s=cube;
    p->sr=cube_rotated;
    p->rings=0;
    p->points_per_ring=0;
    p->default_drawing=TRUE;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
static void initialize_checkerboard(gint rows, gint columns)
  {
    gint i=0;
    gint j=0;
    gboolean def=FALSE;

    if(rows<1||rows>50)
      {
        rows=9;
        def=TRUE;
        g_print("Range for rows is 1<=x<=50. Set default rows=9.\n");
      }
    if(columns<1||columns>50)
      {
        columns=9;
        def=TRUE;
        g_print("Range for columns is 1<=x<=50. Set default columns=9.\n");
      }

    gint array_size=(rows+1)*(columns+1);
    GArray *checkerboard=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(checkerboard, array_size);
    GArray *checkerboard_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(checkerboard_rotated, array_size);

    gdouble move_x=2.0/(gdouble)columns;
    gdouble move_y=2.0/(gdouble)rows;
    struct point3d *p1=&g_array_index(checkerboard, struct point3d, 0);
    for(i=0;i<(rows+1);i++)
      {
        for(j=0;j<(columns+1);j++)
          {
            (*p1).x=-1.0+(gdouble)i*move_x;
            (*p1).y=-1.0+(gdouble)j*move_y;
            (*p1).z=0.0;
            p1++;
          }
      }

    p1=&g_array_index(checkerboard, struct point3d, 0);
    struct point3d *p2=&g_array_index(checkerboard_rotated, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(checkerboard->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=CHECKERBOARD;
    p->s=checkerboard;
    p->sr=checkerboard_rotated;
    p->rings=rows;
    p->points_per_ring=columns;
    p->default_drawing=def;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
static void initialize_ball(gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;
    gboolean def=FALSE;

    if(rings<5||rings>50)
      {
        rings=5;
        def=TRUE;
        g_print("Range for rings is 5<=x<=50. Set default rings=5.\n");
      }
    if(points_per_ring<10||points_per_ring>50)
      {
        points_per_ring=16;
        def=TRUE;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=16.\n");
      }
   
    gint array_size=rings*(points_per_ring+1)+2;
    struct point3d *p1=NULL;

    GArray *ball=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(ball, array_size);
    p1=&g_array_index(ball, struct point3d, 0);
          
    GArray *ball_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(ball_rotated, array_size);
          
    gint ring_points=points_per_ring+1;
    gdouble arc1=180.0/(gdouble)(rings+1);
    gdouble arc2=360.0/(gdouble)(points_per_ring);
    for(i=0;i<rings;i++)
      {
        for(j=0;j<ring_points;j++)
          {
            (*p1).x=sin(arc1*(gdouble)(i+1)*G_PI/180.0)*cos(arc2*(gdouble)j*G_PI/180.0);
            (*p1).y=sin(arc1*(gdouble)(i+1)*G_PI/180.0)*sin(arc2*(gdouble)j*G_PI/180.0);
            (*p1).z=cos(arc1*(gdouble)(i+1)*G_PI/180.0);
            p1++;
          }
      }
    //Top.
    (*p1).x=0.0;
    (*p1).y=0.0;
    (*p1).z=1.0;
    p1++;
    //Bottom.
    (*p1).x=0.0;
    (*p1).y=0.0;
    (*p1).z=-1.0;

    p1=&g_array_index(ball, struct point3d, 0);
    struct point3d *p2=&g_array_index(ball_rotated, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(ball->len));
    
    struct shape *p=g_new(struct shape, 1);
    p->cache_id=BALL;
    p->s=ball;
    p->sr=ball_rotated;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    p->default_drawing=def;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
static void initialize_wire_sphere(gint rings, gint points_per_ring)
  {
    /*
      How it works. Generate one longitude ring. Rotate that ring "rings" times. Generate
      a latitude ring. This needs to have rings*2 points to line up with the longitude points.
      Translate the latitude rings to position. Save all the points and bezier points into
      a sphere array.
    */
    gint i=0;
    gint j=0;
    gdouble radius=1.0;
    gboolean def=FALSE;

    if(rings<8||rings>50)
      {
        rings=10;
        def=TRUE;
        g_print("Range for rings is 8<=x<=50. Set default rings=10.\n");
      }
    if(points_per_ring<10||points_per_ring>50)
      {
        points_per_ring=16;
        def=TRUE;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=16.\n");
      }
    if(points_per_ring%2!=0)
      {
        points_per_ring=16;
        def=TRUE;
        g_print("The sphere needs an even number for points_per_ring. Set default points_per_ring=16.\n");
      }

    //Longitude ring.
    GArray *one_ring=get_bezier_ring(radius, points_per_ring, FALSE);    

    //Initialize some sphere arrays to hold the data.
    gint array_size=rings*(4*points_per_ring)+(points_per_ring/2-1)*(4*rings*2);
    GArray *sphere=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(sphere, array_size);
    struct point3d *pt=&g_array_index(sphere, struct point3d, 0);
          
    GArray *sphere_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(sphere_rotated, array_size);
    
    gdouble qrs[9];
    gdouble qrs1[9];
    gdouble qrs2[9];
    struct arc *p1=NULL;
    gdouble rotate_ring=G_PI/(gdouble)rings;
    quaternion_rotation(0.0, 0.0, G_PI/2.0, qrs2);
    //Save Longitude rings to the sphere array.
    for(i=0;i<rings;i++)
      { 
        //Transform to be able to draw the sphere top down. 
        quaternion_rotation(-G_PI/2.0, 0.0, (gdouble)i*rotate_ring, qrs1);
        matrix_multiply_rotations(qrs1, qrs2, qrs); 
        p1=&g_array_index(one_ring, struct arc, 0);   
        for(j=0;j<points_per_ring;j++)
          {
            //Z is 0 in the one_ring so last term is 0.
            (pt->x)=((p1->a1).x*qrs[0])+((p1->a1).y*qrs[1]); //+((p1->a1).z*qrs[2]);
            (pt->y)=((p1->a1).x*qrs[3])+((p1->a1).y*qrs[4]); //+((p1->a1).z*qrs[5]);
            (pt->z)=((p1->a1).x*qrs[6])+((p1->a1).y*qrs[7]); //+((p1->a1).z*qrs[8]);
            pt++;
            (pt->x)=((p1->a2).x*qrs[0])+((p1->a2).y*qrs[1]); //+((p1->a2).z*qrs[2]);
            (pt->y)=((p1->a2).x*qrs[3])+((p1->a2).y*qrs[4]); //+((p1->a2).z*qrs[5]);
            (pt->z)=((p1->a2).x*qrs[6])+((p1->a2).y*qrs[7]); //+((p1->a2).z*qrs[8]);
            pt++;
            (pt->x)=((p1->a3).x*qrs[0])+((p1->a3).y*qrs[1]); //+((p1->a3).z*qrs[2]);
            (pt->y)=((p1->a3).x*qrs[3])+((p1->a3).y*qrs[4]); //+((p1->a3).z*qrs[5]);
            (pt->z)=((p1->a3).x*qrs[6])+((p1->a3).y*qrs[7]); //+((p1->a3).z*qrs[8]);
            pt++;
            (pt->x)=((p1->a4).x*qrs[0])+((p1->a4).y*qrs[1]); //+((p1->a4).z*qrs[2]);
            (pt->y)=((p1->a4).x*qrs[3])+((p1->a4).y*qrs[4]); //+((p1->a4).z*qrs[5]);
            (pt->z)=((p1->a4).x*qrs[6])+((p1->a4).y*qrs[7]); //+((p1->a4).z*qrs[8]);
            pt++;p1++;
          }
      }

    //Latitude ring.
    GArray *two_ring=get_bezier_ring(radius, rings*2, FALSE);
    
    //Save latitude rings to the sphere array.
    gint len1=points_per_ring/2-1;
    gint len2=rings*2;
    struct point3d *t1=&g_array_index(sphere, struct point3d, 4);
    gdouble translate=0.0;
    gdouble scale=1.0;
    quaternion_rotation(-G_PI, 0.0, 0.0, qrs); 
    for(i=0;i<len1;i++)
      {  
        p1=&g_array_index(two_ring, struct arc, 0);
        translate=t1->z;
        t1+=4;
        scale=radius/sqrt(radius*radius-translate*translate);  
        for(j=0;j<len2;j++)
          {
            (pt->x)=(((p1->a1).x*qrs[0])+((p1->a1).y*qrs[1]))/scale;
            (pt->y)=(((p1->a1).x*qrs[3])+((p1->a1).y*qrs[4]))/scale;
            (pt->z)=(((p1->a1).x*qrs[6])+((p1->a1).y*qrs[7]))/scale+translate;
            pt++;
            (pt->x)=(((p1->a2).x*qrs[0])+((p1->a2).y*qrs[1]))/scale;
            (pt->y)=(((p1->a2).x*qrs[3])+((p1->a2).y*qrs[4]))/scale;
            (pt->z)=(((p1->a2).x*qrs[6])+((p1->a2).y*qrs[7]))/scale+translate;
            pt++;
            (pt->x)=(((p1->a3).x*qrs[0])+((p1->a3).y*qrs[1]))/scale;
            (pt->y)=(((p1->a3).x*qrs[3])+((p1->a3).y*qrs[4]))/scale;
            (pt->z)=(((p1->a3).x*qrs[6])+((p1->a3).y*qrs[7]))/scale+translate;
            pt++;
            (pt->x)=(((p1->a4).x*qrs[0])+((p1->a4).y*qrs[1]))/scale;
            (pt->y)=(((p1->a4).x*qrs[3])+((p1->a4).y*qrs[4]))/scale;
            (pt->z)=(((p1->a4).x*qrs[6])+((p1->a4).y*qrs[7]))/scale+translate;
            pt++;p1++;
          }
      }

    pt=&g_array_index(sphere, struct point3d, 0);
    struct point3d *pt2=&g_array_index(sphere_rotated, struct point3d, 0);
    memcpy(pt2, pt, sizeof(struct point3d)*(sphere->len));
    
    struct shape *p=g_new(struct shape, 1);
    p->cache_id=WIRE_SPHERE;
    p->s=sphere;
    p->sr=sphere_rotated;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    p->default_drawing=def;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");

    g_array_free(one_ring, TRUE);
    g_array_free(two_ring, TRUE);
  }
static void initialize_solid_sphere(gint rings, gint points_per_ring)
  {
    //The same as initialize_solid_bezier_sphere() but used in automatic initialization.
    gint i=0;
    gint j=0;
    gboolean def=FALSE;
    gdouble radius=1.0;

    if(rings<8||rings>50)
      {
        rings=10;
        def=TRUE;
        g_print("Range for rings is 8<=x<=50. Set default rings=10.\n");
      }
    if(points_per_ring<10||points_per_ring>50)
      {
        points_per_ring=16;
        def=TRUE;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=16.\n");
      }
    if(points_per_ring%2!=0)
      {
        points_per_ring=16;
        def=TRUE;
        g_print("The sphere needs an even number for points_per_ring. Set default points_per_ring=16.\n");
      }

    //Get a half ring and rotate it 360 degrees to keep the quad drawing direction the same.
    GArray *one_ring=get_bezier_ring(radius, points_per_ring, TRUE);    

    //Initialize some sphere arrays to hold the data.
    gint array_size=(2*rings+1)*(2*points_per_ring)+(points_per_ring/2-1)*(4*rings*2);
    GArray *sphere=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(sphere, array_size);
    struct point3d *pt=&g_array_index(sphere, struct point3d, 0);
    
    gdouble qrs[9];
    gdouble qrs1[9];
    gdouble qrs2[9];
    struct arc *pa1=NULL;
    gdouble rotate_ring=G_PI/(gdouble)rings;
    quaternion_rotation(0.0, 0.0, G_PI/2.0, qrs2);
    //Save Longitude rings to the sphere array.
    for(i=0;i<rings*2+1;i++)
      {  
        quaternion_rotation(-G_PI/2.0, 0.0, (gdouble)i*rotate_ring, qrs1);
        matrix_multiply_rotations(qrs1, qrs2, qrs);  
        pa1=&g_array_index(one_ring, struct arc, 0);   
        for(j=0;j<points_per_ring/2;j++)
          {
            //Z is 0 in the one_ring so last term is 0.
            (pt->x)=((pa1->a1).x*qrs[0])+((pa1->a1).y*qrs[1]);
            (pt->y)=((pa1->a1).x*qrs[3])+((pa1->a1).y*qrs[4]);
            (pt->z)=((pa1->a1).x*qrs[6])+((pa1->a1).y*qrs[7]);
            pt++;
            (pt->x)=((pa1->a2).x*qrs[0])+((pa1->a2).y*qrs[1]);
            (pt->y)=((pa1->a2).x*qrs[3])+((pa1->a2).y*qrs[4]);
            (pt->z)=((pa1->a2).x*qrs[6])+((pa1->a2).y*qrs[7]);
            pt++;
            (pt->x)=((pa1->a3).x*qrs[0])+((pa1->a3).y*qrs[1]);
            (pt->y)=((pa1->a3).x*qrs[3])+((pa1->a3).y*qrs[4]);
            (pt->z)=((pa1->a3).x*qrs[6])+((pa1->a3).y*qrs[7]);
            pt++;
            (pt->x)=((pa1->a4).x*qrs[0])+((pa1->a4).y*qrs[1]);
            (pt->y)=((pa1->a4).x*qrs[3])+((pa1->a4).y*qrs[4]);
            (pt->z)=((pa1->a4).x*qrs[6])+((pa1->a4).y*qrs[7]);
            pt++;pa1++;
          }
      }

    //Latitude ring.
    GArray *two_ring=get_bezier_ring(radius, rings*2, FALSE);
    
    //Save latitude rings to the sphere array.
    gint len1=points_per_ring/2-1;
    gint len2=rings*2;
    struct point3d *t1=&g_array_index(sphere, struct point3d, 4);
    gdouble translate=0.0;
    gdouble scale=1.0;
    quaternion_rotation(-G_PI, 0.0, 0.0, qrs); 
    for(i=0;i<len1;i++)
      {  
        pa1=&g_array_index(two_ring, struct arc, 0);
        translate=t1->z;
        t1+=4;
        scale=(gdouble)radius/sqrt((gdouble)radius*(gdouble)radius-translate*translate);  
        for(j=0;j<len2;j++)
          {
            (pt->x)=(((pa1->a1).x*qrs[0])+((pa1->a1).y*qrs[1]))/scale;
            (pt->y)=(((pa1->a1).x*qrs[3])+((pa1->a1).y*qrs[4]))/scale;
            (pt->z)=(((pa1->a1).x*qrs[6])+((pa1->a1).y*qrs[7]))/scale+translate;
            pt++;
            (pt->x)=(((pa1->a2).x*qrs[0])+((pa1->a2).y*qrs[1]))/scale;
            (pt->y)=(((pa1->a2).x*qrs[3])+((pa1->a2).y*qrs[4]))/scale;
            (pt->z)=(((pa1->a2).x*qrs[6])+((pa1->a2).y*qrs[7]))/scale+translate;
            pt++;
            (pt->x)=(((pa1->a3).x*qrs[0])+((pa1->a3).y*qrs[1]))/scale;
            (pt->y)=(((pa1->a3).x*qrs[3])+((pa1->a3).y*qrs[4]))/scale;
            (pt->z)=(((pa1->a3).x*qrs[6])+((pa1->a3).y*qrs[7]))/scale+translate;
            pt++;
            (pt->x)=(((pa1->a4).x*qrs[0])+((pa1->a4).y*qrs[1]))/scale;
            (pt->y)=(((pa1->a4).x*qrs[3])+((pa1->a4).y*qrs[4]))/scale;
            (pt->z)=(((pa1->a4).x*qrs[6])+((pa1->a4).y*qrs[7]))/scale+translate;
            pt++;pa1++;
          }
      }
  
    GArray *quad=g_array_sized_new(FALSE, TRUE, sizeof(struct bezier_quad), rings*points_per_ring);
    g_array_set_size(quad, rings*points_per_ring);
    GArray *quad_r=g_array_sized_new(FALSE, TRUE, sizeof(struct bezier_quad), rings*points_per_ring);
    g_array_set_size(quad_r, rings*points_per_ring);
    struct bezier_quad *q1=&g_array_index(quad, struct bezier_quad, 0);

    struct point3d *p1=NULL;
    struct point3d *p2=NULL;
    struct point3d *p3=NULL;
    struct point3d *p4=NULL;

    //Half a longitude.
    len1=points_per_ring/2;
    //Half a latitude.
    len2=2*rings;
    //The last or bottom ring to draw.
    gint len3=points_per_ring/2-1;
    gint counter=0;
   
    for(i=0;i<len1;i++)
      {     
        if(i==0)
          {
            //Get one end of triangles of the sphere.
            p1=&g_array_index(sphere, struct point3d, 0);
            p2=&g_array_index(sphere, struct point3d, (2*rings+1)*(2*points_per_ring));
            p3=&g_array_index(sphere, struct point3d, 2*points_per_ring);
            for(j=0;j<len2;j++)
              {   
                set_tri_points_1(q1, p1, p2, p3);   
                q1->index=counter;
                q1++;counter++;              
                p1+=2*points_per_ring;p3+=2*points_per_ring;
                p2+=4;
              } 
          }
        else if(i==len3)
          {
            //Get the other end of triangles of the sphere.
            p1=&g_array_index(sphere, struct point3d, 4*(i));
            p2=&g_array_index(sphere, struct point3d, (2*rings+1)*(2*points_per_ring)+4*(i-1)*rings*2);
            p3=&g_array_index(sphere, struct point3d, 2*points_per_ring+4*(i)); 
            for(j=0;j<len2;j++)
              {   
                set_tri_points_2(q1, p1, p2, p3);   
                q1->index=counter;
                q1++;counter++;              
                p1+=2*points_per_ring;p3+=2*points_per_ring;
                p2+=4;
              } 
          }
        else
          {
            //Get the middle quads of sphere.
            p1=&g_array_index(sphere, struct point3d, 4*(i));
            p2=&g_array_index(sphere, struct point3d, 2*points_per_ring+4*(i));
            p3=&g_array_index(sphere, struct point3d, (2*rings+1)*(2*points_per_ring)+4*(i-1)*rings*2); 
            p4=&g_array_index(sphere, struct point3d, (2*rings+1)*(2*points_per_ring)+4*(i)*rings*2);
            for(j=0;j<len2;j++)
              {   
                set_quad_points(q1, p1, p2, p3, p4);   
                q1->index=counter;
                q1++;counter++;              
                p1+=2*points_per_ring;p2+=2*points_per_ring;p3+=4;p4+=4;
              }             
          }    
      }  
     
    q1=&g_array_index(quad, struct bezier_quad, 0);
    struct bezier_quad *q2=&g_array_index(quad_r, struct bezier_quad, 0);
    memcpy(q2, q1, sizeof(struct bezier_quad)*(quad->len));
    
    struct shape *p=g_new(struct shape, 1);
    p->cache_id=SOLID_SPHERE;
    p->s=quad;
    p->sr=quad_r;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    p->default_drawing=def;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");

    g_array_free(sphere, TRUE);
    g_array_free(one_ring, TRUE);
    g_array_free(two_ring, TRUE);
  }
static void initialize_wire_funnel(gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;
    gboolean def=FALSE;

    if(rings<5||rings>50)
      {
        rings=10;
        def=TRUE;
        g_print("Range for rings is 5<=x<=50. Set default rings=10.\n");
      }
    if(points_per_ring<10||points_per_ring>50)
      {
        points_per_ring=12;
        def=TRUE;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=12.\n");
      }
   
    gint array_size=rings*(points_per_ring*4);
   
    GArray *funnel=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(funnel, array_size);
    struct point3d *p1=&g_array_index(funnel, struct point3d, 0);
          
    GArray *funnel_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(funnel_rotated, array_size);

    gdouble radius=1.0;
    gdouble z=1.0;
    gdouble z_step=2.0/rings;
    gdouble curve=1.0+0.25*(10.0/rings);
    struct arc *pa=NULL;
    for(i=0;i<rings;i++)
      {
        GArray *one_ring=get_bezier_ring(radius, points_per_ring, FALSE);
        pa=&g_array_index(one_ring, struct arc, 0);
        for(j=0;j<points_per_ring;j++)
          {
            (*p1).x=(pa->a1).x;
            (*p1).y=(pa->a1).y;
            (*p1).z=z;
            p1++;
            (*p1).x=(pa->a2).x;
            (*p1).y=(pa->a2).y;
            (*p1).z=z;
            p1++;
            (*p1).x=(pa->a3).x;
            (*p1).y=(pa->a3).y;
            (*p1).z=z;
            p1++;
            (*p1).x=(pa->a4).x;
            (*p1).y=(pa->a4).y;
            (*p1).z=z;
            p1++;pa++;
          }
        g_array_free(one_ring, TRUE);
        z-=z_step;
        radius*=1.0/curve;        
      }
    
    p1=&g_array_index(funnel, struct point3d, 0);
    struct point3d *p2=&g_array_index(funnel_rotated, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(funnel->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=WIRE_FUNNEL;
    p->s=funnel;
    p->sr=funnel_rotated;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    p->default_drawing=def;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
static void initialize_solid_funnel(gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;
    gboolean def=FALSE;

    if(rings<5||rings>50)
      {
        rings=10;
        def=TRUE;
        g_print("Range for rings is 5<=x<=50. Set default rings=10.\n");
      }
    if(points_per_ring<10||points_per_ring>50)
      {
        points_per_ring=12;
        def=TRUE;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=12.\n");
      }
   
    gint array_size=rings*(points_per_ring*4);
   
    GArray *funnel=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(funnel, array_size);
    struct point3d *p1=&g_array_index(funnel, struct point3d, 0);
          
    gdouble radius=1.0;
    gdouble z=1.0;
    gdouble z_step=2.0/rings;
    gdouble curve=1.0+0.25*(10.0/rings);
    struct arc *pa=NULL;
    for(i=0;i<rings;i++)
      {
        GArray *one_ring=get_bezier_ring(radius, points_per_ring, FALSE);
        pa=&g_array_index(one_ring, struct arc, 0);
        for(j=0;j<points_per_ring;j++)
          {
            (*p1).x=(pa->a1).x;
            (*p1).y=(pa->a1).y;
            (*p1).z=z;
            p1++;
            (*p1).x=(pa->a2).x;
            (*p1).y=(pa->a2).y;
            (*p1).z=z;
            p1++;
            (*p1).x=(pa->a3).x;
            (*p1).y=(pa->a3).y;
            (*p1).z=z;
            p1++;
            (*p1).x=(pa->a4).x;
            (*p1).y=(pa->a4).y;
            (*p1).z=z;
            p1++;pa++;
          }
        g_array_free(one_ring, TRUE);
        z-=z_step;
        radius*=1.0/curve;        
      }

    p1=&g_array_index(funnel, struct point3d, 0);
    struct point3d *p2=&g_array_index(funnel, struct point3d, points_per_ring*4);

    gint quad_len=(rings-1)*(points_per_ring);
    GArray *quad=g_array_sized_new(FALSE, TRUE, sizeof(struct bezier_quad), quad_len);
    g_array_set_size(quad, quad_len);
    GArray *quad_r=g_array_sized_new(FALSE, TRUE, sizeof(struct bezier_quad), quad_len);
    g_array_set_size(quad_r, quad_len);
    struct bezier_quad *q1=&g_array_index(quad, struct bezier_quad, 0);

    //Fill the quad struct mesh array.
    gint counter=0;
    for(i=0;i<((rings)-1);i++)
      {
        for(j=0;j<(points_per_ring);j++)
          {             
            q1->x1=((*(p1)).x);
            q1->y1=((*(p1)).y);
            q1->z1=((*(p1)).z);
            q1->bx1=((*(p1+1)).x);
            q1->by1=((*(p1+1)).y); 
            q1->bz1=((*(p1+1)).z);
            q1->bx2=((*(p1+2)).x);
            q1->by2=((*(p1+2)).y);
            q1->bz2=((*(p1+2)).z);            
            q1->x2=((*(p1+3)).x);
            q1->y2=((*(p1+3)).y);
            q1->z2=((*(p1+3)).z);         
            q1->x3=((*(p2+3)).x);
            q1->y3=((*(p2+3)).y);
            q1->z3=((*(p2+3)).z);
            q1->bx5=((*(p2+2)).x);
            q1->by5=((*(p2+2)).y); 
            q1->bz5=((*(p2+2)).z);            
            q1->bx6=((*(p2+1)).x);
            q1->by6=((*(p2+1)).y);
            q1->bz6=((*(p2+1)).z);            
            q1->x4=((*(p2)).x);
            q1->y4=((*(p2)).y);
            q1->z4=((*(p2)).z);
            q1->index=counter;
            q1++;p1+=4;p2+=4;counter++;
          }
      }

    //Add the missing bezier control points.
    GArray *strand1=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), rings);
    g_array_set_size(strand1, rings);
    GArray *strand2=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), rings);
    g_array_set_size(strand2, rings);
    struct point3d *st1=&g_array_index(strand1, struct point3d, 0);
    struct point3d *st2=&g_array_index(strand2, struct point3d, 0);

    //Get the first strand bezier control points.
    st1=&g_array_index(strand1, struct point3d, 0);
    p1=&g_array_index(funnel, struct point3d, 0);
    for(i=0;i<rings;i++)
      {
        *st1=*p1;
        st1++;p1+=4*points_per_ring;
      }
    GArray *controls1=control_points_from_coords_3d(strand1); 

    //Get the other quad bezier control points.
    for(i=0;i<points_per_ring;i++)
      {
        st2=&g_array_index(strand2, struct point3d, 0);
        if(i==points_per_ring-1) p1=&g_array_index(funnel, struct point3d, 0);
        else p1=&g_array_index(funnel, struct point3d, 4*(i+1));
        for(j=0;j<rings;j++)
          {
            *st2=*p1;
            st2++;p1+=4*points_per_ring;
          }

        GArray *controls2=control_points_from_coords_3d(strand2);
        struct controls3d *c1=&g_array_index(controls1, struct controls3d, 0);
        struct controls3d *c2=&g_array_index(controls2, struct controls3d, 0);
        q1=&g_array_index(quad, struct bezier_quad, i);
        for(j=0;j<rings-1;j++)
          {
            q1->bx3=c2->x1;
            q1->by3=c2->y1;
            q1->bz3=c2->z1;        
            q1->bx4=c2->x2;
            q1->by4=c2->y2;
            q1->bz4=c2->z2;
            q1->bx7=c1->x2;
            q1->by7=c1->y2;
            q1->bz7=c1->z2;         
            q1->bx8=c1->x1;
            q1->by8=c1->y1;
            q1->bz8=c1->z1;
            q1+=points_per_ring;c1++;c2++;
            counter++;
          }
        //Save forward array to back array.
        st1=&g_array_index(strand1, struct point3d, 0);
        st2=&g_array_index(strand2, struct point3d, 0);
        memcpy(st1, st2, sizeof(struct point3d)*(strand1->len));
        c1=&g_array_index(controls1, struct controls3d, 0);
        c2=&g_array_index(controls2, struct controls3d, 0);
        memcpy(c1, c2, sizeof(struct controls3d)*(controls1->len));
        g_array_free(controls2, TRUE);
      }
    g_array_free(strand1, TRUE);
    g_array_free(strand2, TRUE);
    g_array_free(controls1, TRUE);

    g_array_free(funnel, TRUE);

    q1=&g_array_index(quad, struct bezier_quad, 0);
    struct bezier_quad *q2=&g_array_index(quad_r, struct bezier_quad, 0);
    memcpy(q2, q1, sizeof(struct bezier_quad)*(quad->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=SOLID_FUNNEL;
    p->s=quad;
    p->sr=quad_r;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    p->default_drawing=def;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
static void initialize_gem()
  {
    gint i=0;
    gint j=0;
   
    GArray *gem=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 27);
    g_array_set_size(gem, 27);
    struct point3d *p1=&g_array_index(gem, struct point3d, 0);
          
    GArray *gem_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 27);
    g_array_set_size(gem_rotated, 27);
          
    for(i=0;i<2;i++)
      {
        for(j=0;j<13;j++)
          {
            (*p1).x=(0.5+(gdouble)i*0.25)*cos(30.0*(gdouble)(j)*G_PI/180.0);
            (*p1).y=(0.5+(gdouble)i*0.25)*sin(30.0*(gdouble)(j)*G_PI/180.0);
            (*p1).z=0.5-(gdouble)i*0.25;                        
            p1++;
          }
      }
    //Bottom of the gem.
    (*p1).x=0.0;
    (*p1).y=0.0;
    (*p1).z=-0.5;

    p1=&g_array_index(gem, struct point3d, 0);
    struct point3d *p2=&g_array_index(gem_rotated, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(gem->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=GEM;
    p->s=gem;
    p->sr=gem_rotated;
    p->rings=0;
    p->points_per_ring=0;
    p->default_drawing=TRUE;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");    
  }
static void initialize_wire_torus(gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;
    gboolean def=FALSE;

    if(rings<5||rings>50)
      {
        rings=12;
        def=TRUE;
        g_print("Range for rings_torus is 5<=x<=50. Set default rings_torus=12.\n");
      }
    if(points_per_ring<5||points_per_ring>50)
      {
        points_per_ring=16;
        def=TRUE;
        g_print("Range for points_per_ring_torus is 5<=x<=50. Set default points_per_ring_torus=16.\n");
      }
   
    gint array_size=((rings+1)*4*(points_per_ring))+((rings)*4*(points_per_ring+1));
    struct point3d *p1=NULL;
    struct point3d *p2=NULL;

    GArray *torus=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(torus, array_size);
    p1=&g_array_index(torus, struct point3d, 0);
          
    GArray *torus_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(torus_rotated, array_size);
    
    //Draw one ring and then rotate it.      
    GArray *one_ring=get_bezier_ring(0.3, points_per_ring, FALSE);
    struct arc *pa=&g_array_index(one_ring, struct arc, 0);

    //Translate points along x-axis. Outside edge of torus is at 1.
    for(i=0;i<one_ring->len;i++)
      {
        (pa->a1).x+=0.7;
        (pa->a2).x+=0.7;
        (pa->a3).x+=0.7;
        (pa->a4).x+=0.7;
        pa++;
      }

    //Rotate the one ring to get the cross section rings.
    p1=&g_array_index(torus, struct point3d, 0);
    gdouble arc2=360.0/(gdouble)(rings);
    gdouble qrs[9];
    gdouble qrs1[9];
    gdouble qrs2[9];
    quaternion_rotation(0.0, 0.0, G_PI/2.0, qrs2);
    for(i=0;i<rings+1;i++)
      {
        pa=&g_array_index(one_ring, struct arc, 0);
        quaternion_rotation(0.0, (gdouble)i*arc2*G_PI/180.0, 0.0, qrs1);
        matrix_multiply_rotations(qrs1, qrs2, qrs);
        for(j=0;j<points_per_ring;j++)
          {
            (*p1).x=(pa->a1).x*qrs[0]+(pa->a1).y*qrs[1]+(pa->a1).z*qrs[2];
            (*p1).y=(pa->a1).x*qrs[3]+(pa->a1).y*qrs[4]+(pa->a1).z*qrs[5];
            (*p1).z=(pa->a1).x*qrs[6]+(pa->a1).y*qrs[7]+(pa->a1).z*qrs[8];
            p1++;
            (*p1).x=(pa->a2).x*qrs[0]+(pa->a2).y*qrs[1]+(pa->a2).z*qrs[2];
            (*p1).y=(pa->a2).x*qrs[3]+(pa->a2).y*qrs[4]+(pa->a2).z*qrs[5];
            (*p1).z=(pa->a2).x*qrs[6]+(pa->a2).y*qrs[7]+(pa->a2).z*qrs[8];
            p1++;
            (*p1).x=(pa->a3).x*qrs[0]+(pa->a3).y*qrs[1]+(pa->a3).z*qrs[2];
            (*p1).y=(pa->a3).x*qrs[3]+(pa->a3).y*qrs[4]+(pa->a3).z*qrs[5];
            (*p1).z=(pa->a3).x*qrs[6]+(pa->a3).y*qrs[7]+(pa->a3).z*qrs[8];
            p1++;
            (*p1).x=(pa->a4).x*qrs[0]+(pa->a4).y*qrs[1]+(pa->a4).z*qrs[2];
            (*p1).y=(pa->a4).x*qrs[3]+(pa->a4).y*qrs[4]+(pa->a4).z*qrs[5];
            (*p1).z=(pa->a4).x*qrs[6]+(pa->a4).y*qrs[7]+(pa->a4).z*qrs[8];
            p1++;pa++;
          }
      }
    g_array_free(one_ring, TRUE);

    //Get the outside rings.
    gdouble radius=0.0;
    gdouble translate=0.0;
    quaternion_rotation(0.0, 0.0, G_PI, qrs);
    p2=&g_array_index(torus, struct point3d, 0);
    for(i=0;i<points_per_ring+1;i++)
      {
        if(i==points_per_ring) p2=&g_array_index(torus, struct point3d, 0);
        radius=p2->x;
        translate=p2->z;
        GArray *two_ring=get_bezier_ring(radius, rings, FALSE);
        pa=&g_array_index(two_ring, struct arc, 0);
        for(j=0;j<rings;j++)
          { 
            (*p1).x=(pa->a1).x*qrs[0]+(pa->a1).y*qrs[1]+(pa->a1).z*qrs[2];
            (*p1).y=((pa->a1).x*qrs[3]+(pa->a1).y*qrs[4]+(pa->a1).z*qrs[5]);
            (*p1).z=((pa->a1).x*qrs[6]+(pa->a1).y*qrs[7]+(pa->a1).z*qrs[8])+translate;
            p1++;
            (*p1).x=(pa->a2).x*qrs[0]+(pa->a2).y*qrs[1]+(pa->a2).z*qrs[2];
            (*p1).y=((pa->a2).x*qrs[3]+(pa->a2).y*qrs[4]+(pa->a2).z*qrs[5]);
            (*p1).z=((pa->a2).x*qrs[6]+(pa->a2).y*qrs[7]+(pa->a2).z*qrs[8])+translate;
            p1++;
            (*p1).x=(pa->a3).x*qrs[0]+(pa->a3).y*qrs[1]+(pa->a3).z*qrs[2];
            (*p1).y=((pa->a3).x*qrs[3]+(pa->a3).y*qrs[4]+(pa->a3).z*qrs[5]);
            (*p1).z=((pa->a3).x*qrs[6]+(pa->a3).y*qrs[7]+(pa->a3).z*qrs[8])+translate;
            p1++;
            (*p1).x=(pa->a4).x*qrs[0]+(pa->a4).y*qrs[1]+(pa->a4).z*qrs[2];
            (*p1).y=((pa->a4).x*qrs[3]+(pa->a4).y*qrs[4]+(pa->a4).z*qrs[5]);
            (*p1).z=((pa->a4).x*qrs[6]+(pa->a4).y*qrs[7]+(pa->a4).z*qrs[8])+translate;
            p1++;pa++;
          }
        p2+=4;
        g_array_free(two_ring, TRUE);
      }

    p1=&g_array_index(torus, struct point3d, 0);
    p2=&g_array_index(torus_rotated, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(torus->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=WIRE_TORUS;
    p->s=torus;
    p->sr=torus_rotated;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    p->default_drawing=def;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");    
  }
static void initialize_solid_torus(gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;
    gboolean def=FALSE;

    if(rings<5||rings>50)
      {
        rings=12;
        def=TRUE;
        g_print("Range for rings_torus is 5<=x<=50. Set default rings_torus=12.\n");
      }
    if(points_per_ring<5||points_per_ring>50)
      {
        points_per_ring=16;
        def=TRUE;
        g_print("Range for points_per_ring_torus is 5<=x<=50. Set default points_per_ring_torus=16.\n");
      }
   
    gint array_size=((rings+1)*4*(points_per_ring))+((rings)*4*(points_per_ring+1));
    struct point3d *p1=NULL;
    struct point3d *p2=NULL;
    struct point3d *p3=NULL;
    struct point3d *p4=NULL;

    GArray *torus=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(torus, array_size);
    p1=&g_array_index(torus, struct point3d, 0);
    
    //Draw one ring and then rotate it.      
    GArray *one_ring=get_bezier_ring(0.3, points_per_ring, FALSE);
    struct arc *pa=&g_array_index(one_ring, struct arc, 0);

    //Translate points along x-axis. Outside edge of torus is at 1.
    for(i=0;i<one_ring->len;i++)
      {
        (pa->a1).x+=0.7;
        (pa->a2).x+=0.7;
        (pa->a3).x+=0.7;
        (pa->a4).x+=0.7;
        pa++;
      }

    //Rotate the one ring to get the cross section rings.
    p1=&g_array_index(torus, struct point3d, 0);
    gdouble arc2=360.0/(gdouble)(rings);
    gdouble qrs[9];
    gdouble qrs1[9];
    gdouble qrs2[9];
    quaternion_rotation(0.0, 0.0, G_PI/2.0, qrs2);
    for(i=0;i<rings+1;i++)
      {
        pa=&g_array_index(one_ring, struct arc, 0);
        quaternion_rotation(0.0, (gdouble)i*arc2*G_PI/180.0, 0.0, qrs1);
        matrix_multiply_rotations(qrs1, qrs2, qrs);
        for(j=0;j<points_per_ring;j++)
          {
            (*p1).x=(pa->a1).x*qrs[0]+(pa->a1).y*qrs[1]+(pa->a1).z*qrs[2];
            (*p1).y=(pa->a1).x*qrs[3]+(pa->a1).y*qrs[4]+(pa->a1).z*qrs[5];
            (*p1).z=(pa->a1).x*qrs[6]+(pa->a1).y*qrs[7]+(pa->a1).z*qrs[8];
            p1++;
            (*p1).x=(pa->a2).x*qrs[0]+(pa->a2).y*qrs[1]+(pa->a2).z*qrs[2];
            (*p1).y=(pa->a2).x*qrs[3]+(pa->a2).y*qrs[4]+(pa->a2).z*qrs[5];
            (*p1).z=(pa->a2).x*qrs[6]+(pa->a2).y*qrs[7]+(pa->a2).z*qrs[8];
            p1++;
            (*p1).x=(pa->a3).x*qrs[0]+(pa->a3).y*qrs[1]+(pa->a3).z*qrs[2];
            (*p1).y=(pa->a3).x*qrs[3]+(pa->a3).y*qrs[4]+(pa->a3).z*qrs[5];
            (*p1).z=(pa->a3).x*qrs[6]+(pa->a3).y*qrs[7]+(pa->a3).z*qrs[8];
            p1++;
            (*p1).x=(pa->a4).x*qrs[0]+(pa->a4).y*qrs[1]+(pa->a4).z*qrs[2];
            (*p1).y=(pa->a4).x*qrs[3]+(pa->a4).y*qrs[4]+(pa->a4).z*qrs[5];
            (*p1).z=(pa->a4).x*qrs[6]+(pa->a4).y*qrs[7]+(pa->a4).z*qrs[8];
            p1++;pa++;
          }
      }
    g_array_free(one_ring, TRUE);

    //Get the outside rings.
    gdouble radius=0.0;
    gdouble translate=0.0;
    quaternion_rotation(0.0, 0.0, G_PI, qrs);
    p2=&g_array_index(torus, struct point3d, 0);
    for(i=0;i<points_per_ring+1;i++)
      {
        if(i==points_per_ring) p2=&g_array_index(torus, struct point3d, 0);
        radius=p2->x;
        translate=p2->z;
        GArray *two_ring=get_bezier_ring(radius, rings, FALSE);
        pa=&g_array_index(two_ring, struct arc, 0);
        for(j=0;j<rings;j++)
          { 
            (*p1).x=(pa->a1).x*qrs[0]+(pa->a1).y*qrs[1]+(pa->a1).z*qrs[2];
            (*p1).y=((pa->a1).x*qrs[3]+(pa->a1).y*qrs[4]+(pa->a1).z*qrs[5]);
            (*p1).z=((pa->a1).x*qrs[6]+(pa->a1).y*qrs[7]+(pa->a1).z*qrs[8])+translate;
            p1++;
            (*p1).x=(pa->a2).x*qrs[0]+(pa->a2).y*qrs[1]+(pa->a2).z*qrs[2];
            (*p1).y=((pa->a2).x*qrs[3]+(pa->a2).y*qrs[4]+(pa->a2).z*qrs[5]);
            (*p1).z=((pa->a2).x*qrs[6]+(pa->a2).y*qrs[7]+(pa->a2).z*qrs[8])+translate;
            p1++;
            (*p1).x=(pa->a3).x*qrs[0]+(pa->a3).y*qrs[1]+(pa->a3).z*qrs[2];
            (*p1).y=((pa->a3).x*qrs[3]+(pa->a3).y*qrs[4]+(pa->a3).z*qrs[5]);
            (*p1).z=((pa->a3).x*qrs[6]+(pa->a3).y*qrs[7]+(pa->a3).z*qrs[8])+translate;
            p1++;
            (*p1).x=(pa->a4).x*qrs[0]+(pa->a4).y*qrs[1]+(pa->a4).z*qrs[2];
            (*p1).y=((pa->a4).x*qrs[3]+(pa->a4).y*qrs[4]+(pa->a4).z*qrs[5]);
            (*p1).z=((pa->a4).x*qrs[6]+(pa->a4).y*qrs[7]+(pa->a4).z*qrs[8])+translate;
            p1++;pa++;
          }
        p2+=4;
        g_array_free(two_ring, TRUE);
      }

    gint counter=0;
    gint offset1=4*points_per_ring;
    gint offset2=4*rings;
    gint start=4*(rings+1)*points_per_ring;
    gint quad_len=rings*points_per_ring;
    GArray *quad=g_array_sized_new(FALSE, TRUE, sizeof(struct bezier_quad), quad_len);
    g_array_set_size(quad, quad_len);
    GArray *quad_r=g_array_sized_new(FALSE, TRUE, sizeof(struct bezier_quad), quad_len);
    g_array_set_size(quad_r, quad_len);
    struct bezier_quad *q1=&g_array_index(quad, struct bezier_quad, 0);
    p1=&g_array_index(torus, struct point3d, 0);
    p2=&g_array_index(torus, struct point3d, offset1);
    for(i=0;i<rings;i++)
      {
        p3=&g_array_index(torus, struct point3d, start+4*i);
        p4=&g_array_index(torus, struct point3d, start+offset2+4*i);
        for(j=0;j<points_per_ring;j++)
          {
            (q1->x1)=(p1->x);
            (q1->y1)=(p1->y);
            (q1->z1)=(p1->z);
            (q1->bx1)=((p1+1)->x);
            (q1->by1)=((p1+1)->y);
            (q1->bz1)=((p1+1)->z);
            (q1->bx2)=((p1+2)->x);
            (q1->by2)=((p1+2)->y);
            (q1->bz2)=((p1+2)->z);
            (q1->x2)=((p1+3)->x);
            (q1->y2)=((p1+3)->y);
            (q1->z2)=((p1+3)->z);
            (q1->bx3)=((p4+1)->x);
            (q1->by3)=((p4+1)->y);
            (q1->bz3)=((p4+1)->z);
            (q1->bx4)=((p4+2)->x);
            (q1->by4)=((p4+2)->y);
            (q1->bz4)=((p4+2)->z);
            (q1->x3)=((p4+3)->x);
            (q1->y3)=((p4+3)->y);
            (q1->z3)=((p4+3)->z);
            (q1->bx5)=((p2+2)->x);
            (q1->by5)=((p2+2)->y);
            (q1->bz5)=((p2+2)->z);
            (q1->bx6)=((p2+1)->x);
            (q1->by6)=((p2+1)->y);
            (q1->bz6)=((p2+1)->z);
            (q1->x4)=(p2->x);
            (q1->y4)=(p2->y);
            (q1->z4)=(p2->z);
            (q1->bx7)=((p3+2)->x);
            (q1->by7)=((p3+2)->y);
            (q1->bz7)=((p3+2)->z);
            (q1->bx8)=((p3+1)->x);
            (q1->by8)=((p3+1)->y);
            (q1->bz8)=((p3+1)->z);
            (q1->index)=counter;
            p1+=4;p2+=4;p3+=offset2;p4+=offset2;counter++;q1++;       
          }
         p3+=4;p4+=4; 
       }
    g_array_free(torus, TRUE);

    q1=&g_array_index(quad, struct bezier_quad, 0);
    struct bezier_quad *q2=&g_array_index(quad_r, struct bezier_quad, 0);
    memcpy(q2, q1, sizeof(struct bezier_quad)*(quad->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=SOLID_TORUS;
    p->s=quad;
    p->sr=quad_r;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    p->default_drawing=def;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");    
  }
static void initialize_wire_twist(gint strands)
  {
    gint i=0;
    gint j=0;
    gboolean def=FALSE;

    if(strands<6||strands>25)
      {
        strands=17;
        def=TRUE;
        g_print("Range for twist strands is 6<=x<=25. Set default twist strands=17.\n");
      }

    //Keep square.
    gint len=strands*strands;
    gdouble qrs[9];
    gdouble arc=360.0/(strands-1);
    GArray *twist=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), len);
    g_array_set_size(twist, len);
    GArray *twist_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), len);
    g_array_set_size(twist_rotated, len);

    //Get the points for the twist.
    struct point3d *p1=&g_array_index(twist, struct point3d, 0);
    for(i=0;i<strands;i++)
      {
        p1=&g_array_index(twist, struct point3d, i);
        quaternion_rotation(0.0, (gdouble)i*arc*G_PI/180.0, 0.0, qrs);
        for(j=0;j<strands;j++)
          {
            //Based on a 16 rows and 16 columns twist.
            (*p1).x=-2.0+(gdouble)j*0.25*(16.0/((gdouble)strands-1.0));
            (*p1).y=-4.0+(gdouble)i*0.5*(16.0/((gdouble)strands-1.0));
            (*p1).z=0.0;
            (*p1).x=((*p1).x*qrs[0])+((*p1).y*qrs[1])+((*p1).z*qrs[2]);
            (*p1).y=((*p1).x*qrs[3])+((*p1).y*qrs[4])+((*p1).z*qrs[5]);
            (*p1).z=((*p1).x*qrs[6])+((*p1).y*qrs[7])+((*p1).z*qrs[8]);
            p1+=strands;
          }
      }

    p1=&g_array_index(twist, struct point3d, 0);
    struct point3d *p2=&g_array_index(twist_rotated, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(twist->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=WIRE_TWIST;
    p->s=twist;
    p->sr=twist_rotated;
    p->rings=0;
    p->points_per_ring=strands;
    p->default_drawing=def;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
static void initialize_solid_twist(gint strands)
  {
    gint i=0;
    gint j=0;
    gboolean def=FALSE;

    if(strands<6||strands>25)
      {
        strands=17;
        def=TRUE;
        g_print("Range for twist strands is 6<=x<=25. Set default twist strands=17.\n");
      }

    //Keep square.
    gint len=strands*strands;
    gdouble qrs[9];
    gdouble arc=360.0/(strands-1);
    GArray *twist=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), len);
    g_array_set_size(twist, len);

    //Get the points for the twist.
    struct point3d *p1=&g_array_index(twist, struct point3d, 0);
    for(i=0;i<strands;i++)
      {
        p1=&g_array_index(twist, struct point3d, i);
        quaternion_rotation(0.0, (gdouble)i*arc*G_PI/180.0, 0.0, qrs);
        for(j=0;j<strands;j++)
          {
            //Based on a 16 rows and 16 columns twist.
            (*p1).x=-2.0+(gdouble)j*0.25*(16.0/((gdouble)strands-1.0));
            (*p1).y=-4.0+(gdouble)i*0.5*(16.0/((gdouble)strands-1.0));
            (*p1).z=0.0;
            (*p1).x=((*p1).x*qrs[0])+((*p1).y*qrs[1])+((*p1).z*qrs[2]);
            (*p1).y=((*p1).x*qrs[3])+((*p1).y*qrs[4])+((*p1).z*qrs[5]);
            (*p1).z=((*p1).x*qrs[6])+((*p1).y*qrs[7])+((*p1).z*qrs[8]);
            p1+=strands;
          }
      }

    p1=&g_array_index(twist, struct point3d, 0);
    struct point3d *p2=&g_array_index(twist, struct point3d, strands);

    //Arrays used for getting bezier points.
    GArray *strand1=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), strands);
    g_array_set_size(strand1, strands);
    GArray *strand2=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), strands);
    g_array_set_size(strand2, strands);
    struct point3d *st1=&g_array_index(strand1, struct point3d, 0);
    struct point3d *st2=&g_array_index(strand2, struct point3d, 0);

    //Array of quads to sort
    gint quad_len=strands*(strands-1);
    GArray *quad=g_array_sized_new(FALSE, TRUE, sizeof(struct bezier_quad), quad_len);
    g_array_set_size(quad, quad_len);
    GArray *quad_r=g_array_sized_new(FALSE, TRUE, sizeof(struct bezier_quad), quad_len);
    g_array_set_size(quad_r, quad_len);
    struct bezier_quad *q1=&g_array_index(quad, struct bezier_quad, 0);

    //Get the first strand and it's bezier points.
    for(i=0;i<strands;i++)
      {  
        (st1->x)=(p1->x);
        (st1->y)=(p1->y);
        (st1->z)=(p1->z);          
        st1++;p1++;
      }

    GArray *controls1=control_points_from_coords_3d(strand1);

    gint counter=0;
    for(i=1;i<strands;i++)
      {
        //get a second or leading strand.
        p2=&g_array_index(twist, struct point3d, strands*i);
        st2=&g_array_index(strand2, struct point3d, 0);
        for(j=0;j<strands;j++)
          {  
            (st2->x)=(p2->x);
            (st2->y)=(p2->y);
            (st2->z)=(p2->z);          
            st2++;p2++;
          }

        //Get bezier points.
        st1=&g_array_index(strand1, struct point3d, 0);
        st2=&g_array_index(strand2, struct point3d, 0);
        GArray *controls2=control_points_from_coords_3d(strand2);
        struct controls3d *c1=&g_array_index(controls1, struct controls3d, 0);
        struct controls3d *c2=&g_array_index(controls2, struct controls3d, 0);
        p1=&g_array_index(twist, struct point3d, strands*i-strands);
        p2=&g_array_index(twist, struct point3d, strands*i);
        for(j=0;j<(strands-1);j++)
          {   
            (q1->x1)=(p2->x);
            (q1->y1)=(p2->y);
            (q1->z1)=(p2->z);
            (q1->bx1)=(c2->x1);
            (q1->by1)=(c2->y1);
            (q1->bz1)=(c2->z1);
            (q1->bx2)=(c2->x2);
            (q1->by2)=(c2->y2);
            (q1->bz2)=(c2->z2);
            (q1->x2)=((p2+1)->x);
            (q1->y2)=((p2+1)->y);
            (q1->z2)=((p2+1)->z);
            //line
            (q1->bx3)=((p2+1)->x);
            (q1->by3)=((p2+1)->y);
            (q1->bz3)=((p2+1)->z);
            (q1->bx4)=((p1+1)->x);
            (q1->by4)=((p1+1)->y);
            (q1->bz4)=((p1+1)->z);
            (q1->x3)=((p1+1)->x);
            (q1->y3)=((p1+1)->y);
            (q1->z3)=((p1+1)->z);
            (q1->bx5)=(c1->x2);
            (q1->by5)=(c1->y2);
            (q1->bz5)=(c1->z2);
            (q1->bx6)=(c1->x1);
            (q1->by6)=(c1->y1);
            (q1->bz6)=(c1->z1);
            (q1->x4)=(p1->x);
            (q1->y4)=(p1->y);
            (q1->z4)=(p1->z);
            //line
            (q1->bx7)=(p1->x);
            (q1->by7)=(p1->y);
            (q1->bz7)=(p1->z);
            (q1->bx8)=(p2->x);
            (q1->by8)=(p2->y);
            (q1->bz8)=(p2->z);
            q1->index=counter;
            p1++;p2++;c1++;c2++;q1++;counter++;
          }
        //Save forward array to back array.
        st1=&g_array_index(strand1, struct point3d, 0);
        st2=&g_array_index(strand2, struct point3d, 0);
        memcpy(st1, st2, sizeof(struct point3d)*(strand1->len));
        c1=&g_array_index(controls1, struct controls3d, 0);
        c2=&g_array_index(controls2, struct controls3d, 0);
        memcpy(c1, c2, sizeof(struct controls3d)*(controls1->len));
        g_array_free(controls2, TRUE);
      }
    g_array_free(strand1, TRUE);
    g_array_free(strand2, TRUE);
    g_array_free(controls1, TRUE);

    g_array_free(twist, TRUE);

    q1=&g_array_index(quad, struct bezier_quad, 0);
    struct bezier_quad *q2=&g_array_index(quad_r, struct bezier_quad, 0);
    memcpy(q2, q1, sizeof(struct bezier_quad)*(quad->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=SOLID_TWIST;
    p->s=quad;
    p->sr=quad_r;
    p->rings=0;
    p->points_per_ring=strands;
    p->default_drawing=def;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
static void initialize_pyramid()
  {
    GArray *pyramid=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 5);
    g_array_set_size(pyramid, 5);
    GArray *pyramid_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 5);
    g_array_set_size(pyramid_rotated, 5);

    struct point3d *p1=&g_array_index(pyramid, struct point3d, 0);
    //Top point.
    (*p1).x=0.0;
    (*p1).y=0.0;
    (*p1).z=1.0;
    p1++;
    //Bottom square.
    (*p1).x=-1.0;
    (*p1).y=-1.0;
    (*p1).z=-1.0;
    p1++;
    (*p1).x=1.0;
    (*p1).y=-1.0;
    (*p1).z=-1.0;
    p1++;
    (*p1).x=1.0;
    (*p1).y=1.0;
    (*p1).z=-1.0;
    p1++;
    (*p1).x=-1.0;
    (*p1).y=1.0;
    (*p1).z=-1.0;

    p1=&g_array_index(pyramid, struct point3d, 0);
    struct point3d *p2=&g_array_index(pyramid_rotated, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(pyramid->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=PYRAMID;
    p->s=pyramid;
    p->sr=pyramid_rotated;
    p->rings=0;
    p->points_per_ring=0;
    p->default_drawing=TRUE;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
static void initialize_cone(gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;
    gboolean def=FALSE;

    if(rings<5||rings>50)
      {
        rings=5;
        def=TRUE;
        g_print("Range for rings is 5<=x<=50. Set default rings=5.\n");
      }
    if(points_per_ring<10||points_per_ring>50)
      {
        points_per_ring=16;
        def=TRUE;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=16.\n");
      }
   
    gint array_size=rings*(points_per_ring+1)+1;
    struct point3d *p1=NULL;

    GArray *cone=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(cone, array_size);
    p1=&g_array_index(cone, struct point3d, 0);
          
    gdouble height=2.0/(gdouble)rings;
    gdouble radius=1.0/rings;
    gdouble arc1=360.0/(gdouble)(points_per_ring);
    for(i=0;i<rings+1;i++)
      {
        for(j=0;j<points_per_ring+1;j++)
          {
            (*p1).x=(1.0-radius*(gdouble)i)*cos(arc1*(gdouble)j*G_PI/180.0);
            (*p1).y=(1.0-radius*(gdouble)i)*sin(arc1*(gdouble)j*G_PI/180.0);
            (*p1).z=-1.0+height*(gdouble)i;
            p1++;
          }
      }

    gint array_len=rings*points_per_ring;
    gint offset1=points_per_ring+2;
    gint offset2=points_per_ring+1;

    p1=&g_array_index(cone, struct point3d, 0);

    GArray *quad=g_array_sized_new(FALSE, TRUE, sizeof(struct quad_plane2), array_len);
    g_array_set_size(quad, array_len);
    GArray *quad_r=g_array_sized_new(FALSE, TRUE, sizeof(struct quad_plane2), array_len);
    g_array_set_size(quad_r, array_len);
    struct quad_plane2 *q1=&g_array_index(quad, struct quad_plane2, 0);

    gint counter=0;
    //Fill the quad struct array.
    for(i=0;i<rings;i++)
      {
        for(j=0;j<points_per_ring;j++)
          {            
            q1->x1=((*(p1)).x);
            q1->y1=((*(p1)).y);
            q1->z1=((*(p1)).z);
            q1->x2=((*(p1+1)).x);
            q1->y2=((*(p1+1)).y);
            q1->z2=((*(p1+1)).z);            
            q1->x3=((*(p1+offset1)).x);
            q1->y3=((*(p1+offset1)).y);
            q1->z3=((*(p1+offset1)).z);
            q1->x4=((*(p1+offset2)).x);
            q1->y4=((*(p1+offset2)).y);
            q1->z4=((*(p1+offset2)).z);  
            q1->index=counter;            
            q1++;p1++;counter++;
          }
        p1++;
      }

    g_array_free(cone, TRUE);

    q1=&g_array_index(quad, struct quad_plane2, 0);
    struct quad_plane2 *q2=&g_array_index(quad_r, struct quad_plane2, 0);
    memcpy(q2, q1, sizeof(struct quad_plane2)*(quad->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=CONE;
    p->s=quad;
    p->sr=quad_r;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    p->default_drawing=def;
    p->z_max=1.0;
    p->z_min=-1.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");  
  }
static void initialize_text_ring(gint points_per_ring)
  {
    //Only 2 rings in the text ring.
    gint i=0;
    gint j=0;
    gboolean def=FALSE;

    if(points_per_ring<5||points_per_ring>30)
      {
        points_per_ring=16;
        def=TRUE;
        g_print("Range for points_per_ring is 5<=x<=30. Set default points_per_ring=16.\n");
      }

    gint points=2*(points_per_ring+1);
    GArray *text_ring=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), points);
    g_array_set_size(text_ring, points);    
    
    struct point3d *p1=&g_array_index(text_ring, struct point3d, 0);
    gdouble arc1=360.0/(gdouble)(points_per_ring);
    gdouble z_value=0.4;
    for(i=0;i<2;i++)
      {
        for(j=0;j<points_per_ring+1;j++)
          {
            (*p1).x=cos(arc1*(gdouble)j*G_PI/180.0);
            (*p1).y=sin(arc1*(gdouble)j*G_PI/180.0);
            (*p1).z=z_value;
            p1++;
          }
        z_value=-0.4;
      }

    gint array_len=(points_per_ring);
    GArray *quad=g_array_sized_new(FALSE, TRUE, sizeof(struct quad_plane2), array_len);
    g_array_set_size(quad, array_len);
    GArray *quad_r=g_array_sized_new(FALSE, TRUE, sizeof(struct quad_plane2), array_len);
    g_array_set_size(quad_r, array_len);
    struct quad_plane2 *q1=&g_array_index(quad, struct quad_plane2, 0);

    gint counter=0;
    p1=&g_array_index(text_ring, struct point3d, 0);
    struct point3d *p2=&g_array_index(text_ring, struct point3d, points_per_ring+1);

    for(i=0;i<array_len;i++)
      {            
        (q1->x1)=(p1->x);
        (q1->y1)=(p1->y);
        (q1->z1)=(p1->z); 
        (q1->x2)=(p2->x);
        (q1->y2)=(p2->y);
        (q1->z2)=(p2->z); 
        (q1->x3)=((p2+1)->x);
        (q1->y3)=((p2+1)->y);
        (q1->z3)=((p2+1)->z); 
        (q1->x4)=((p1+1)->x);
        (q1->y4)=((p1+1)->y);
        (q1->z4)=((p1+1)->z); 
        q1->index=counter;
        p1++;p2++;q1++;counter++;             
      }

    g_array_free(text_ring, TRUE);

    q1=&g_array_index(quad, struct quad_plane2, 0);
    struct quad_plane2 *q2=&g_array_index(quad_r, struct quad_plane2, 0);
    memcpy(q2, q1, sizeof(struct quad_plane2)*(quad->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=TEXT_RING;
    p->s=quad;
    p->sr=quad_r;
    p->rings=0;
    p->points_per_ring=points_per_ring;
    p->default_drawing=def;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
static void initialize_fish()
  {
    gint i=0;
    gint j=0;
    gboolean def=TRUE;
    gint rings=17;
    gint points_per_ring=17;
   
    //Extra points for head, tail and fins.
    gint array_size=rings*(points_per_ring+1)+9;
    struct point3d *p1=NULL;

    GArray *fish=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(fish, array_size);
    p1=&g_array_index(fish, struct point3d, 0);
          
    GArray *fish_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(fish_rotated, array_size);
          
    gint ring_points=points_per_ring+1;
    gdouble arc1=180.0/(gdouble)(rings+1);
    gdouble arc2=360.0/(gdouble)(points_per_ring);
    for(i=0;i<rings;i++)
      {
        for(j=0;j<ring_points;j++)
          {
            (*p1).x=3.0*cos(arc1*(gdouble)(i+1)*G_PI/180.0);
            (*p1).y=2.0*sin(arc1*(gdouble)(i+1)*G_PI/180.0)*cos(arc2*(gdouble)j*G_PI/180.0);
            (*p1).z=0.5*sin(arc1*(gdouble)(i+1)*G_PI/180.0)*sin(arc2*(gdouble)j*G_PI/180.0);
            p1++;
          }
      }
    //head of ellipse.
    (*p1).x=3.0;
    (*p1).y=0.0;
    (*p1).z=0.0;
    p1++;
    //tail of ellipse.
    (*p1).x=-3.0;
    (*p1).y=0.0;
    (*p1).z=0.0;
    p1++;
    //triangle tail.
    (*p1).x=-3.0;
    (*p1).y=0.0;
    (*p1).z=0.0;
    p1++;
    (*p1).x=-4.0;
    (*p1).y=-2.0;
    (*p1).z=0.0;
    p1++;
    (*p1).x=-4.0;
    (*p1).y=2.0;
    (*p1).z=0.0;
    p1++;
    //Top fin.
    (*p1).x=-0.7;
    (*p1).y=-2.5;
    (*p1).z=0.0;
    p1++;
    (*p1).x=0.0;
    (*p1).y=-1.95;
    (*p1).z=0.0;
    p1++;
    //Bottom fin.
    (*p1).x=-0.7;
    (*p1).y=2.5;
    (*p1).z=0.0;
    p1++;
    (*p1).x=0.0;
    (*p1).y=1.95;
    (*p1).z=0.0;
    
    struct shape *p=g_new(struct shape, 1);
    p->cache_id=FISH;
    p->s=fish;
    p->sr=fish_rotated;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    p->default_drawing=def;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
static void initialize_turtle()
  {
    gint i=0;
    gint j=0;
    gboolean def=TRUE;
    gint rings=16;
    gint points_per_ring=16;
   
    //Extra points for head, tail and fins.
    gint array_size=rings*(points_per_ring+1)+18;
    struct point3d *p1=NULL;

    GArray *turtle=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(turtle, array_size);
    p1=&g_array_index(turtle, struct point3d, 0);
          
    GArray *turtle_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(turtle_rotated, array_size);
          
    gint ring_points=points_per_ring+1;
    gdouble arc1=180.0/(gdouble)(rings+1);
    gdouble arc2=360.0/(gdouble)(points_per_ring);
    for(i=0;i<rings;i++)
      {
        for(j=0;j<ring_points;j++)
          {
            if(i==0)
              {
                (*p1).x=3.2*cos(arc1*1.2*G_PI/180.0);
                (*p1).y=1.2*sin(25.0*G_PI/180.0)*cos(arc2*(gdouble)j*G_PI/180.0);
                (*p1).z=0.9*sin(20.0*G_PI/180.0)*sin(arc2*(gdouble)j*G_PI/180.0);
              }
            else if(i<4)
              {
                (*p1).x=3.2*cos(arc1*((gdouble)(i)+1.8)*G_PI/180.0);
                (*p1).y=1.2*sin(30.0*G_PI/180.0)*cos(arc2*(gdouble)j*G_PI/180.0);
                (*p1).z=0.9*sin(30.0*G_PI/180.0)*sin(arc2*(gdouble)j*G_PI/180.0);
              }
            else
              {
                (*p1).x=3.0*cos(arc1*(gdouble)(i+1)*G_PI/180.0);
                (*p1).y=2.0*sin(arc1*(gdouble)(i+1)*G_PI/180.0)*cos(arc2*(gdouble)j*G_PI/180.0);
                (*p1).z=1.0*sin(arc1*(gdouble)(i+1)*G_PI/180.0)*sin(arc2*(gdouble)j*G_PI/180.0);
              }
            p1++;
          }
      }
    //head of ellipse.
    (*p1).x=3.5;
    (*p1).y=0.0;
    (*p1).z=0.0;
    p1++;
    //tail of ellipse.
    (*p1).x=-4.0;
    (*p1).y=0.0;
    (*p1).z=0.0;
    p1++;
    //Right front flipper.
    (*p1).x=2.7;
    (*p1).y=-2.0;
    (*p1).z=0.0;
    p1++;
    (*p1).x=3.2;
    (*p1).y=-4.2;
    (*p1).z=0.0;
    p1++;
    (*p1).x=2.4;
    (*p1).y=-3.7;
    (*p1).z=0.0;
    p1++;
    (*p1).x=2.2;
    (*p1).y=-2.5;
    (*p1).z=0.0;
    p1++;
    //Left front flipper.
    (*p1).x=2.7;
    (*p1).y=2.0;
    (*p1).z=0.0;
    p1++;
    (*p1).x=3.2;
    (*p1).y=4.2;
    (*p1).z=0.0;
    p1++;
    (*p1).x=2.4;
    (*p1).y=3.7;
    (*p1).z=0.0;
    p1++;
    (*p1).x=2.2;
    (*p1).y=2.5;
    (*p1).z=0.0;
    p1++;
    //Right back flipper.
    (*p1).x=-2.5;
    (*p1).y=-2.0;
    (*p1).z=0.0;
    p1++;
    (*p1).x=-2.6;
    (*p1).y=-2.0;
    (*p1).z=0.0;
    p1++;
    (*p1).x=-2.8;
    (*p1).y=-2.0;
    (*p1).z=0.0;
    p1++;
    (*p1).x=-3.0;
    (*p1).y=-1.3;
    (*p1).z=0.0;
    p1++;
    //Left back flipper.
    (*p1).x=-2.5;
    (*p1).y=2.0;
    (*p1).z=0.0;
    p1++;
    (*p1).x=-2.6;
    (*p1).y=2.0;
    (*p1).z=0.0;
    p1++;
    (*p1).x=-2.8;
    (*p1).y=2.0;
    (*p1).z=0.0;
    p1++;
    (*p1).x=-3.0;
    (*p1).y=1.3;
    (*p1).z=0.0;
        
    struct shape *p=g_new(struct shape, 1);
    p->cache_id=TURTLE;
    p->s=turtle;
    p->sr=turtle_rotated;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    p->default_drawing=def;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
void initialize_wire_bezier_sheet(gint rows, gint columns, gint sheet_width, gint sheet_height)
  {
    gint i=0;
    gint j=0;
    gdouble x=0.0;
    gdouble y=0.0;
    gboolean def=FALSE;
    gdouble sheet_width1=0.0;
    gdouble sheet_height1=0.0;
    gdouble sheet_width2=0.0;
    gdouble sheet_height2=0.0;

    if(rows<5||rows>50)
      {
        rows=15;
        def=TRUE;
        g_print("Range for rows is 5<=x<=50. Set default rows=15.\n");
      }
    if(columns<5||columns>50)
      {
        columns=15;
        def=TRUE;
        g_print("Range for columns is 5<=x<=50. Set default columns=15.\n");
      }
    if(sheet_width<5||sheet_width>1500)
      {
        sheet_width=10;
        def=TRUE;
        g_print("Range for sheet_width is 5<=x<=1500. Set default sheet_width=10.\n");
      }
    if(sheet_height<5||sheet_height>1500)
      {
        sheet_height=10;
        def=TRUE;
        g_print("Range for sheet_height is 5<=x<=1500. Set default sheet_height=10.\n");
      }
    sheet_width1=(gdouble)sheet_width;
    sheet_height1=(gdouble)sheet_height;
    sheet_width2=sheet_width1/2.0;
    sheet_height2=sheet_height/2.0;

    //Lines for rows and columns.
    rows++; columns++;
    gint array_size=2*rows*columns*3;
    GArray *bezier_sheet=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(bezier_sheet, array_size);
    GArray *bezier_sheet_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(bezier_sheet_rotated, array_size);

    struct point3d *p1=&g_array_index(bezier_sheet, struct point3d, 0);
    struct point3d *p2=&g_array_index(bezier_sheet, struct point3d, 0);

    //Points and bezier points for horizontal lines.
    for(i=0;i<rows;i++)
      {
        for(j=0;j<columns;j++)
          {
            if(j==0)
              {
                (*p1).x=sheet_width1*(gdouble)j/(gdouble)(columns-1)-sheet_width2;
                (*p1).y=sheet_height1*(gdouble)i/(gdouble)(rows-1)-sheet_height2;
              }
            else
              {
                (*p1).x=sheet_width1*(gdouble)j/(gdouble)(columns-1)-sheet_width2;
                (*p1).y=sheet_height1*(gdouble)i/(gdouble)(rows-1)-sheet_height2;
                p2=p1-3;
                x=(p1->x)-(p2->x);
                y=(p1->y)-(p2->y);
                ((p1-2)->x)=(p2->x)+x/3.0;
                ((p1-2)->y)=(p2->y)+y/3.0;
                ((p1-1)->x)=(p2->x)+2.0*x/3.0;
                ((p1-1)->y)=(p2->y)+2.0*y/3.0;
              }
            p1+=3;
          }
      }

    //For vertical lines.
    for(i=0;i<columns;i++)
      {
        for(j=0;j<rows;j++)
          {
            if(j==0)
              {
                (*p1).x=sheet_width1*(gdouble)i/(gdouble)(columns-1)-sheet_width2;
                (*p1).y=sheet_height1*(gdouble)j/(gdouble)(rows-1)-sheet_height2;
              }
            else
              {
                (*p1).x=sheet_width1*(gdouble)i/(gdouble)(columns-1)-sheet_width2;
                (*p1).y=sheet_height1*(gdouble)j/(gdouble)(rows-1)-sheet_height2;
                p2=p1-3;
                x=(p1->x)-(p2->x);
                y=(p1->y)-(p2->y);
                ((p1-2)->x)=(p2->x)+x/3.0;
                ((p1-2)->y)=(p2->y)+y/3.0;
                ((p1-1)->x)=(p2->x)+2.0*x/3.0;
                ((p1-1)->y)=(p2->y)+2.0*y/3.0;
              }
            p1+=3;
          }
      }

    p1=&g_array_index(bezier_sheet, struct point3d, 0);
    p2=&g_array_index(bezier_sheet_rotated, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(bezier_sheet->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=WIRE_BEZIER_SHEET;
    p->s=bezier_sheet;
    p->sr=bezier_sheet_rotated;
    p->rings=--rows;
    p->points_per_ring=--columns;
    p->default_drawing=def;
    p->z_max=1.0;
    p->z_min=-1.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
void initialize_solid_bezier_sheet(gint rows, gint columns, gint sheet_width, gint sheet_height)
  {
    //Put the bezier sheet in quad format to make the solid color drawing easier.
    gint i=0;
    gint j=0;
    gint counter=0;
    gdouble x=0.0;
    gdouble y=0.0;
    gint row1=(rows+1)*3;
    gint col1=(columns+1)*3;
    gboolean def=FALSE;
    gdouble sheet_width1=0.0;
    gdouble sheet_height1=0.0;
    gdouble sheet_width2=0.0;
    gdouble sheet_height2=0.0;

    if(rows<5||rows>50)
      {
        rows=15;
        def=TRUE;
        g_print("Range for rows is 5<=x<=50. Set default rows=15.\n");
      }
    if(columns<5||columns>50)
      {
        columns=15;
        def=TRUE;
        g_print("Range for columns is 5<=x<=50. Set default columns=15.\n");
      }
    if(sheet_width<5||sheet_width>1500)
      {
        sheet_width=10;
        def=TRUE;
        g_print("Range for sheet_width is 5<=x<=1500. Set default sheet_width=10.\n");
      }
    if(sheet_height<5||sheet_height>1500)
      {
        sheet_height=10;
        def=TRUE;
        g_print("Range for sheet_height is 5<=x<=1500. Set default sheet_height=10.\n");
      }
    sheet_width1=(gdouble)sheet_width;
    sheet_height1=(gdouble)sheet_height;
    sheet_width2=sheet_width1/2.0;
    sheet_height2=sheet_height/2.0;

    //Lines for rows and columns.
    rows++; columns++;
    gint array_size=2*rows*columns*3;
    GArray *bezier_sheet=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(bezier_sheet, array_size);

    struct point3d *p1=&g_array_index(bezier_sheet, struct point3d, 0);
    struct point3d *p2=&g_array_index(bezier_sheet, struct point3d, 0);
    struct point3d *p3=&g_array_index(bezier_sheet, struct point3d, 0);
    struct point3d *p4=&g_array_index(bezier_sheet, struct point3d, 0);

    //Points and bezier points for horizontal lines.
    for(i=0;i<rows;i++)
      {
        for(j=0;j<columns;j++)
          {
            if(j==0)
              {
                (*p1).x=sheet_width1*(gdouble)j/(gdouble)(columns-1)-sheet_width2;
                (*p1).y=sheet_height1*(gdouble)i/(gdouble)(rows-1)-sheet_height2;
              }
            else
              {
                (*p1).x=sheet_width1*(gdouble)j/(gdouble)(columns-1)-sheet_width2;
                (*p1).y=sheet_height1*(gdouble)i/(gdouble)(rows-1)-sheet_height2;
                p2=p1-3;
                x=(p1->x)-(p2->x);
                y=(p1->y)-(p2->y);
                ((p1-2)->x)=(p2->x)+x/3.0;
                ((p1-2)->y)=(p2->y)+y/3.0;
                ((p1-1)->x)=(p2->x)+2.0*x/3.0;
                ((p1-1)->y)=(p2->y)+2.0*y/3.0;
              }
            p1+=3;
          }
      }

    //For vertical lines.
    for(i=0;i<columns;i++)
      {
        for(j=0;j<rows;j++)
          {
            if(j==0)
              {
                (*p1).x=sheet_width1*(gdouble)i/(gdouble)(columns-1)-sheet_width2;
                (*p1).y=sheet_height1*(gdouble)j/(gdouble)(rows-1)-sheet_height2;
              }
            else
              {
                (*p1).x=sheet_width1*(gdouble)i/(gdouble)(columns-1)-sheet_width2;
                (*p1).y=sheet_height1*(gdouble)j/(gdouble)(rows-1)-sheet_height2;
                p2=p1-3;
                x=(p1->x)-(p2->x);
                y=(p1->y)-(p2->y);
                ((p1-2)->x)=(p2->x)+x/3.0;
                ((p1-2)->y)=(p2->y)+y/3.0;
                ((p1-1)->x)=(p2->x)+2.0*x/3.0;
                ((p1-1)->y)=(p2->y)+2.0*y/3.0;
              }
            p1+=3;
          }
      }

    //quads for rows and columns.
    rows--; columns--;

    //Copy points to a quad array
    GArray *quad=g_array_sized_new(FALSE, TRUE, sizeof(struct bezier_quad), rows*columns);
    g_array_set_size(quad, rows*columns);
    GArray *quad_r=g_array_sized_new(FALSE, TRUE, sizeof(struct bezier_quad), rows*columns);
    g_array_set_size(quad_r, rows*columns);
    struct bezier_quad *p5=&g_array_index(quad, struct bezier_quad, 0);
    for(i=0;i<rows;i++)
      {
        p1=&g_array_index(bezier_sheet, struct point3d, i*col1);
        p2=&g_array_index(bezier_sheet, struct point3d, i*col1+col1);
        p3=&g_array_index(bezier_sheet, struct point3d, (bezier_sheet->len)/2+i*3);
        p4=&g_array_index(bezier_sheet, struct point3d, (bezier_sheet->len)/2+i*3+row1);
        for(j=0;j<columns;j++)
          {
            (p5->x1)=(p1->x); 
            (p5->y1)=(p1->y);
            (p5->z1)=(p1->z);
            (p5->bx1)=((p1+1)->x);
            (p5->by1)=((p1+1)->y);
            (p5->bz1)=((p1+1)->z);
            (p5->bx2)=((p1+2)->x);
            (p5->by2)=((p1+2)->y);
            (p5->bz2)=((p1+2)->z);
            (p5->x2)=((p1+3)->x); 
            (p5->y2)=((p1+3)->y);
            (p5->z2)=((p1+3)->z);
            (p5->bx3)=((p4+1)->x);
            (p5->by3)=((p4+1)->y);
            (p5->bz3)=((p4+1)->z);
            (p5->bx4)=((p4+2)->x);
            (p5->by4)=((p4+2)->y);
            (p5->bz4)=((p4+2)->z);
            (p5->x3)=((p4+3)->x); 
            (p5->y3)=((p4+3)->y);
            (p5->z3)=((p4+3)->z);
            (p5->bx5)=((p2+2)->x);
            (p5->by5)=((p2+2)->y);
            (p5->bz5)=((p2+2)->z);
            (p5->bx6)=((p2+1)->x);
            (p5->by6)=((p2+1)->y);
            (p5->bz6)=((p2+1)->z);
            (p5->x4)=(p2->x); 
            (p5->y4)=(p2->y);
            (p5->z4)=(p2->z);
            (p5->bx7)=((p3+2)->x);
            (p5->by7)=((p3+2)->y);
            (p5->bz7)=((p3+2)->z);
            (p5->bx8)=((p3+1)->x);
            (p5->by8)=((p3+1)->y);
            (p5->bz8)=((p3+1)->z);
            (p5->index)=counter;
            p1+=3;p2+=3;p3+=row1;p4+=row1;p5++;counter++;
          }
      }

    g_array_free(bezier_sheet, TRUE);

    p5=&g_array_index(quad, struct bezier_quad, 0);
    struct bezier_quad *p6=&g_array_index(quad_r, struct bezier_quad, 0);
    memcpy(p6, p5, sizeof(struct bezier_quad)*(quad->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=SOLID_BEZIER_SHEET;
    p->s=quad;
    p->sr=quad_r;
    p->rings=rows;
    p->points_per_ring=columns;
    p->default_drawing=def;
    p->z_max=1.0;
    p->z_min=-1.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
void initialize_wire_bezier_disc(gint rings, gint points_per_ring, gint radius)
  {
    gint i=0;
    gint j=0;
    gdouble arc=0.0;
    gdouble x=0.0;
    gdouble y=0.0;
    gboolean def=FALSE;
    gdouble radius1=0.0;
    gdouble radius2=0.0;

    if(rings<4||rings>50)
      {
        rings=10;
        def=TRUE;
        g_print("Range for rings is 5<=x<=50. Set default rings=10.\n");
      }
    if(points_per_ring<4||points_per_ring>50)
      {
        points_per_ring=12;
        def=TRUE;
        g_print("Range for points_per_ring is 5<=x<=50. Set default points_per_ring=12.\n");
      }
    if(radius<5||radius>500)
      {
        radius=250;
        def=TRUE;
        g_print("Range for radius is 5<=x<=500. Set default radius=250.\n");
      }
    radius1=(gdouble)radius;
   
    gint array_size=(3*(rings+1)*(points_per_ring)+3*(rings)*(points_per_ring+1));
   
    GArray *disc=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(disc, array_size);
    struct point3d *p1=&g_array_index(disc, struct point3d, 0);
    struct point3d *p2=&g_array_index(disc, struct point3d, 0);
          
    GArray *disc_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(disc_rotated, array_size);

    //Ray lines for the mesh.
    for(i=0;i<points_per_ring;i++)
      {
        for(j=0;j<rings+1;j++)
          {
            radius2=radius1*(gdouble)(j)/rings;
            arc=-(gdouble)i/points_per_ring*2.0*G_PI;
            if(j==0)
              {
                (*p1).x=radius2*cos(arc);
                (*p1).y=radius2*sin(arc);
              }
            else
              {
                (*p1).x=radius2*cos(arc);
                (*p1).y=radius2*sin(arc);
                p2=p1-3;
                x=(p1->x)-(p2->x);
                y=(p1->y)-(p2->y);
                ((p1-2)->x)=(p2->x)+x/3.0;
                ((p1-2)->y)=(p2->y)+y/3.0;
                ((p1-1)->x)=(p2->x)+2.0*x/3.0;
                ((p1-1)->y)=(p2->y)+2.0*y/3.0;
              }
            p1+=3;
          }
      }

    GArray *one_ring=get_bezier_ring(1.0, points_per_ring, FALSE); 
    struct arc *pt=NULL;
 
    for(i=0;i<rings;i++)
      {  
        pt=&g_array_index(one_ring, struct arc, 0);
        radius2=radius1*(gdouble)(i+1)/rings;  
        for(j=0;j<points_per_ring+1;j++)
          {
            if(j==0)
              {
                (p1->x)=((pt->a1).x)*radius2;
                (p1->y)=((pt->a1).y)*radius1;
              }
            else
              {
                (*p1).x=((pt->a4).x)*radius2;
                (*p1).y=((pt->a4).y)*radius2;
                ((p1-1)->x)=((pt->a3).x)*radius2;
                ((p1-1)->y)=((pt->a3).y)*radius2;               
                ((p1-2)->x)=((pt->a2).x)*radius2;
                ((p1-2)->y)=((pt->a2).y)*radius2;
                pt++;
              }
            p1+=3;
          }
      }
    g_array_free(one_ring, TRUE);

    p1=&g_array_index(disc, struct point3d, 0);
    p2=&g_array_index(disc_rotated, struct point3d, 0);
    memcpy(p2, p1, sizeof(struct point3d)*(disc->len));

    struct shape *p=g_new(struct shape, 1);
    p->cache_id=WIRE_BEZIER_DISC;
    p->s=disc;
    p->sr=disc_rotated;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    p->default_drawing=def;
    p->z_max=1.0;
    p->z_min=-1.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
void initialize_solid_bezier_disc(gint rings, gint points_per_ring, gint radius)
  {
    gint i=0;
    gint j=0;
    gdouble arc=0.0;
    gdouble x=0.0;
    gdouble y=0.0;
    gint counter=0;
    gboolean def=FALSE;
    gdouble radius1=0.0;
    gdouble radius2=0.0;

    if(rings<4||rings>50)
      {
        rings=10;
        def=TRUE;
        g_print("Range for rings is 4<=x<=50. Set default rings=10.\n");
      }
    if(points_per_ring<4||points_per_ring>50)
      {
        points_per_ring=12;
        def=TRUE;
        g_print("Range for points_per_ring is 4<=x<=50. Set default points_per_ring=12.\n");
      }
    if(radius<5||radius>500)
      {
        radius=250;
        def=TRUE;
        g_print("Range for radius is 5<=x<=500. Set default radius=250.\n");
      }
    radius1=(gdouble)radius;
   
    gint array_size=(3*(rings+1)*(points_per_ring+1))+(3*(rings+1)*(points_per_ring+1));
   
    GArray *disc=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(disc, array_size);
    struct point3d *p1=&g_array_index(disc, struct point3d, 0);
    struct point3d *p2=&g_array_index(disc, struct point3d, 0);
    struct point3d *p3=&g_array_index(disc, struct point3d, 0);
    struct point3d *p4=&g_array_index(disc, struct point3d, 0);

    //Ray lines for the mesh. Generate an extra ray line for completing quads.
    for(i=0;i<points_per_ring+1;i++)
      {
        arc=-(gdouble)i/points_per_ring*2.0*G_PI;
        for(j=0;j<rings+1;j++)
          {
            radius2=radius1*(gdouble)(j)/rings;
            if(j==0)
              {
                (*p1).x=radius2*cos(arc);
                (*p1).y=radius2*sin(arc);
              }
            else
              {
                (*p1).x=radius2*cos(arc);
                (*p1).y=radius2*sin(arc);
                p2=p1-3;
                x=(p1->x)-(p2->x);
                y=(p1->y)-(p2->y);
                ((p1-2)->x)=(p2->x)+x/3.0;
                ((p1-2)->y)=(p2->y)+y/3.0;
                ((p1-1)->x)=(p2->x)+2.0*x/3.0;
                ((p1-1)->y)=(p2->y)+2.0*y/3.0;
              }
            p1+=3;
          }
      }

    //Rings. Generate an extra ring.
    GArray *one_ring=get_bezier_ring(1.0, points_per_ring, FALSE); 
    struct arc *pt=NULL;
    for(i=0;i<rings+1;i++)
      {  
        pt=&g_array_index(one_ring, struct arc, 0);
        radius2=radius1*(gdouble)(i)/rings;  
        for(j=0;j<points_per_ring+1;j++)
          {
            if(j==0)
              {
                (p1->x)=((pt->a1).x)*radius2;
                (p1->y)=((pt->a1).y)*radius2;
              }
            else
              {
                (*p1).x=((pt->a4).x)*radius2;
                (*p1).y=((pt->a4).y)*radius2;
                ((p1-1)->x)=((pt->a3).x)*radius2;
                ((p1-1)->y)=((pt->a3).y)*radius2;               
                ((p1-2)->x)=((pt->a2).x)*radius2;
                ((p1-2)->y)=((pt->a2).y)*radius2;
                pt++;
              }
            p1+=3;
          }
      }
    g_array_free(one_ring, TRUE);

    //Copy points to a quad array
    gint line=(rings+1)*3;
    gint ppr=(points_per_ring+1)*3;
    GArray *quad=g_array_sized_new(FALSE, TRUE, sizeof(struct bezier_quad), rings*points_per_ring);
    g_array_set_size(quad, rings*points_per_ring);
    GArray *quad_r=g_array_sized_new(FALSE, TRUE, sizeof(struct bezier_quad), rings*points_per_ring);
    g_array_set_size(quad_r, rings*points_per_ring);
    struct bezier_quad *p5=&g_array_index(quad, struct bezier_quad, 0);
    for(i=0;i<rings;i++)
      {
        p1=&g_array_index(disc, struct point3d, (3*(rings+1)*(points_per_ring+1))+i*ppr);
        p2=&g_array_index(disc, struct point3d, (3*(rings+1)*(points_per_ring+1))+i*ppr+ppr);
        p3=&g_array_index(disc, struct point3d, i*line-line*i+3*i);
        p4=&g_array_index(disc, struct point3d, i*line+line-line*i+3*i);
        
        for(j=0;j<points_per_ring;j++)
          {
            (p5->x1)=(p1->x); 
            (p5->y1)=(p1->y);
            (p5->z1)=(p1->z);
            (p5->bx1)=((p1+1)->x);
            (p5->by1)=((p1+1)->y);
            (p5->bz1)=((p1+1)->z);
            (p5->bx2)=((p1+2)->x);
            (p5->by2)=((p1+2)->y);
            (p5->bz2)=((p1+2)->z);
            (p5->x2)=((p1+3)->x); 
            (p5->y2)=((p1+3)->y);
            (p5->z2)=((p1+3)->z);

            (p5->bx3)=((p4+1)->x);
            (p5->by3)=((p4+1)->y);
            (p5->bz3)=((p4+1)->z);
            (p5->bx4)=((p4+2)->x);
            (p5->by4)=((p4+2)->y);
            (p5->bz4)=((p4+2)->z);
            (p5->x3)=((p4+3)->x); 
            (p5->y3)=((p4+3)->y);
            (p5->z3)=((p4+3)->z);

            (p5->bx5)=((p2+2)->x);
            (p5->by5)=((p2+2)->y);
            (p5->bz5)=((p2+2)->z);
            (p5->bx6)=((p2+1)->x);
            (p5->by6)=((p2+1)->y);
            (p5->bz6)=((p2+1)->z);
            (p5->x4)=(p2->x); 
            (p5->y4)=(p2->y);
            (p5->z4)=(p2->z);

            (p5->bx7)=((p3+2)->x);
            (p5->by7)=((p3+2)->y);
            (p5->bz7)=((p3+2)->z);
            (p5->bx8)=((p3+1)->x);
            (p5->by8)=((p3+1)->y);
            (p5->bz8)=((p3+1)->z);
            (p5->index)=counter;
            p1+=3;p2+=3;p3+=line;p4+=line;p5++;counter++;
          }
      }
    g_array_free(disc, TRUE);

    p5=&g_array_index(quad, struct bezier_quad, 0);
    struct bezier_quad *p6=&g_array_index(quad_r, struct bezier_quad, 0);
    memcpy(p6, p5, sizeof(struct bezier_quad)*(quad->len));
    
    struct shape *p=g_new(struct shape, 1);
    p->cache_id=SOLID_BEZIER_DISC;
    p->s=quad;
    p->sr=quad_r;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    p->default_drawing=def;
    p->z_max=1.0;
    p->z_min=-1.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");
  }
void initialize_wire_bezier_sphere(gint rings, gint points_per_ring, gint radius)
  {
    //Duplicate of initialize sphere code.
    gint i=0;
    gint j=0;
    gboolean def=FALSE;

    if(rings<8||rings>50)
      {
        rings=10;
        def=TRUE;
        g_print("Range for rings is 8<=x<=50. Set default rings=10.\n");
      }
    if(points_per_ring<10||points_per_ring>50)
      {
        points_per_ring=16;
        def=TRUE;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=16.\n");
      }
    if(points_per_ring%2!=0)
      {
        points_per_ring=16;
        def=TRUE;
        g_print("The sphere needs an even number for points_per_ring. Set default points_per_ring=16.\n");
      }

    //Get a half ring and rotate it 360 degrees to keep the quad drawing direction the same.
    GArray *one_ring=get_bezier_ring(radius, points_per_ring, TRUE);    

    //Initialize some sphere arrays to hold the data.
    gint array_size=(2*rings)*(2*points_per_ring)+(points_per_ring/2-1)*(4*rings*2);
    GArray *sphere=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(sphere, array_size);
    GArray *sphere_rotated=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(sphere_rotated, array_size);
    struct point3d *pt=&g_array_index(sphere, struct point3d, 0);
    
    gdouble qrs[9];
    gdouble qrs1[9];
    gdouble qrs2[9];
    struct arc *pa1=NULL;
    gdouble rotate_ring=G_PI/(gdouble)rings;
    quaternion_rotation(0.0, 0.0, G_PI/2.0, qrs2);
    //Save Longitude rings to the sphere array.
    for(i=0;i<rings*2;i++)
      {  
        quaternion_rotation(-G_PI/2.0, 0.0, (gdouble)i*rotate_ring, qrs1);
        matrix_multiply_rotations(qrs1, qrs2, qrs);  
        pa1=&g_array_index(one_ring, struct arc, 0);   
        for(j=0;j<points_per_ring/2;j++)
          {
            //Z is 0 in the one_ring so last term is 0.
            (pt->x)=((pa1->a1).x*qrs[0])+((pa1->a1).y*qrs[1]);
            (pt->y)=((pa1->a1).x*qrs[3])+((pa1->a1).y*qrs[4]);
            (pt->z)=((pa1->a1).x*qrs[6])+((pa1->a1).y*qrs[7]);
            pt++;
            (pt->x)=((pa1->a2).x*qrs[0])+((pa1->a2).y*qrs[1]);
            (pt->y)=((pa1->a2).x*qrs[3])+((pa1->a2).y*qrs[4]);
            (pt->z)=((pa1->a2).x*qrs[6])+((pa1->a2).y*qrs[7]);
            pt++;
            (pt->x)=((pa1->a3).x*qrs[0])+((pa1->a3).y*qrs[1]);
            (pt->y)=((pa1->a3).x*qrs[3])+((pa1->a3).y*qrs[4]);
            (pt->z)=((pa1->a3).x*qrs[6])+((pa1->a3).y*qrs[7]);
            pt++;
            (pt->x)=((pa1->a4).x*qrs[0])+((pa1->a4).y*qrs[1]);
            (pt->y)=((pa1->a4).x*qrs[3])+((pa1->a4).y*qrs[4]);
            (pt->z)=((pa1->a4).x*qrs[6])+((pa1->a4).y*qrs[7]);
            pt++;pa1++;
          }
      }

    //Latitude ring.
    GArray *two_ring=get_bezier_ring(radius, rings*2, FALSE);
    
    //Save latitude rings to the sphere array.
    gint len1=points_per_ring/2-1;
    gint len2=rings*2;
    struct point3d *t1=&g_array_index(sphere, struct point3d, 4);
    gdouble translate=0.0;
    gdouble scale=1.0;
    quaternion_rotation(-G_PI, 0.0, 0.0, qrs); 
    for(i=0;i<len1;i++)
      {  
        pa1=&g_array_index(two_ring, struct arc, 0);
        translate=t1->z;
        t1+=4;
        scale=(gdouble)radius/sqrt((gdouble)radius*(gdouble)radius-translate*translate);  
        for(j=0;j<len2;j++)
          {
            (pt->x)=(((pa1->a1).x*qrs[0])+((pa1->a1).y*qrs[1]))/scale;
            (pt->y)=(((pa1->a1).x*qrs[3])+((pa1->a1).y*qrs[4]))/scale;
            (pt->z)=(((pa1->a1).x*qrs[6])+((pa1->a1).y*qrs[7]))/scale+translate;
            pt++;
            (pt->x)=(((pa1->a2).x*qrs[0])+((pa1->a2).y*qrs[1]))/scale;
            (pt->y)=(((pa1->a2).x*qrs[3])+((pa1->a2).y*qrs[4]))/scale;
            (pt->z)=(((pa1->a2).x*qrs[6])+((pa1->a2).y*qrs[7]))/scale+translate;
            pt++;
            (pt->x)=(((pa1->a3).x*qrs[0])+((pa1->a3).y*qrs[1]))/scale;
            (pt->y)=(((pa1->a3).x*qrs[3])+((pa1->a3).y*qrs[4]))/scale;
            (pt->z)=(((pa1->a3).x*qrs[6])+((pa1->a3).y*qrs[7]))/scale+translate;
            pt++;
            (pt->x)=(((pa1->a4).x*qrs[0])+((pa1->a4).y*qrs[1]))/scale;
            (pt->y)=(((pa1->a4).x*qrs[3])+((pa1->a4).y*qrs[4]))/scale;
            (pt->z)=(((pa1->a4).x*qrs[6])+((pa1->a4).y*qrs[7]))/scale+translate;
            pt++;pa1++;
          }
      }

    pt=&g_array_index(sphere, struct point3d, 0);
    struct point3d *pt2=&g_array_index(sphere_rotated, struct point3d, 0);
    memcpy(pt2, pt, sizeof(struct point3d)*(sphere->len));
    
    struct shape *p=g_new(struct shape, 1);
    p->cache_id=WIRE_BEZIER_SPHERE;
    p->s=sphere;
    p->sr=sphere_rotated;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    p->default_drawing=def;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");

    g_array_free(one_ring, TRUE);
    g_array_free(two_ring, TRUE);
  }
void initialize_solid_bezier_sphere(gint rings, gint points_per_ring, gint radius)
  {
    //Initialize a sphere and build an array of quads.
    gint i=0;
    gint j=0;
    gboolean def=FALSE;

    if(rings<8||rings>50)
      {
        rings=10;
        def=TRUE;
        g_print("Range for rings is 8<=x<=50. Set default rings=10.\n");
      }
    if(points_per_ring<10||points_per_ring>50)
      {
        points_per_ring=16;
        def=TRUE;
        g_print("Range for points_per_ring is 10<=x<=50. Set default points_per_ring=16.\n");
      }
    if(points_per_ring%2!=0)
      {
        points_per_ring=16;
        def=TRUE;
        g_print("The sphere needs an even number for points_per_ring. Set default points_per_ring=16.\n");
      }

    //Get a half ring and rotate it 360 degrees to keep the quad drawing direction the same.
    GArray *one_ring=get_bezier_ring(radius, points_per_ring, TRUE);    

    //Initialize some sphere arrays to hold the data.
    gint array_size=(2*rings+1)*(2*points_per_ring)+(points_per_ring/2-1)*(4*rings*2);
    GArray *sphere=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), array_size);
    g_array_set_size(sphere, array_size);
    struct point3d *pt=&g_array_index(sphere, struct point3d, 0);
    
    gdouble qrs[9];
    gdouble qrs1[9];
    gdouble qrs2[9];
    struct arc *pa1=NULL;
    gdouble rotate_ring=G_PI/(gdouble)rings;
    quaternion_rotation(0.0, 0.0, G_PI/2.0, qrs2);
    //Save Longitude rings to the sphere array.
    for(i=0;i<rings*2+1;i++)
      {  
        quaternion_rotation(-G_PI/2.0, 0.0, (gdouble)i*rotate_ring, qrs1);
        matrix_multiply_rotations(qrs1, qrs2, qrs);  
        pa1=&g_array_index(one_ring, struct arc, 0);   
        for(j=0;j<points_per_ring/2;j++)
          {
            //Z is 0 in the one_ring so last term is 0.
            (pt->x)=((pa1->a1).x*qrs[0])+((pa1->a1).y*qrs[1]);
            (pt->y)=((pa1->a1).x*qrs[3])+((pa1->a1).y*qrs[4]);
            (pt->z)=((pa1->a1).x*qrs[6])+((pa1->a1).y*qrs[7]);
            pt++;
            (pt->x)=((pa1->a2).x*qrs[0])+((pa1->a2).y*qrs[1]);
            (pt->y)=((pa1->a2).x*qrs[3])+((pa1->a2).y*qrs[4]);
            (pt->z)=((pa1->a2).x*qrs[6])+((pa1->a2).y*qrs[7]);
            pt++;
            (pt->x)=((pa1->a3).x*qrs[0])+((pa1->a3).y*qrs[1]);
            (pt->y)=((pa1->a3).x*qrs[3])+((pa1->a3).y*qrs[4]);
            (pt->z)=((pa1->a3).x*qrs[6])+((pa1->a3).y*qrs[7]);
            pt++;
            (pt->x)=((pa1->a4).x*qrs[0])+((pa1->a4).y*qrs[1]);
            (pt->y)=((pa1->a4).x*qrs[3])+((pa1->a4).y*qrs[4]);
            (pt->z)=((pa1->a4).x*qrs[6])+((pa1->a4).y*qrs[7]);
            pt++;pa1++;
          }
      }

    //Latitude ring.
    GArray *two_ring=get_bezier_ring(radius, rings*2, FALSE);
    
    //Save latitude rings to the sphere array.
    gint len1=points_per_ring/2-1;
    gint len2=rings*2;
    struct point3d *t1=&g_array_index(sphere, struct point3d, 4);
    gdouble translate=0.0;
    gdouble scale=1.0;
    quaternion_rotation(-G_PI, 0.0, 0.0, qrs); 
    for(i=0;i<len1;i++)
      {  
        pa1=&g_array_index(two_ring, struct arc, 0);
        translate=t1->z;
        t1+=4;
        scale=(gdouble)radius/sqrt((gdouble)radius*(gdouble)radius-translate*translate);  
        for(j=0;j<len2;j++)
          {
            (pt->x)=(((pa1->a1).x*qrs[0])+((pa1->a1).y*qrs[1]))/scale;
            (pt->y)=(((pa1->a1).x*qrs[3])+((pa1->a1).y*qrs[4]))/scale;
            (pt->z)=(((pa1->a1).x*qrs[6])+((pa1->a1).y*qrs[7]))/scale+translate;
            pt++;
            (pt->x)=(((pa1->a2).x*qrs[0])+((pa1->a2).y*qrs[1]))/scale;
            (pt->y)=(((pa1->a2).x*qrs[3])+((pa1->a2).y*qrs[4]))/scale;
            (pt->z)=(((pa1->a2).x*qrs[6])+((pa1->a2).y*qrs[7]))/scale+translate;
            pt++;
            (pt->x)=(((pa1->a3).x*qrs[0])+((pa1->a3).y*qrs[1]))/scale;
            (pt->y)=(((pa1->a3).x*qrs[3])+((pa1->a3).y*qrs[4]))/scale;
            (pt->z)=(((pa1->a3).x*qrs[6])+((pa1->a3).y*qrs[7]))/scale+translate;
            pt++;
            (pt->x)=(((pa1->a4).x*qrs[0])+((pa1->a4).y*qrs[1]))/scale;
            (pt->y)=(((pa1->a4).x*qrs[3])+((pa1->a4).y*qrs[4]))/scale;
            (pt->z)=(((pa1->a4).x*qrs[6])+((pa1->a4).y*qrs[7]))/scale+translate;
            pt++;pa1++;
          }
      }
  
    //Build the quads.
    GArray *quad=g_array_sized_new(FALSE, TRUE, sizeof(struct bezier_quad), rings*points_per_ring);
    g_array_set_size(quad, rings*points_per_ring);
    GArray *quad_r=g_array_sized_new(FALSE, TRUE, sizeof(struct bezier_quad), rings*points_per_ring);
    g_array_set_size(quad_r, rings*points_per_ring);
    struct bezier_quad *q1=&g_array_index(quad, struct bezier_quad, 0);

    struct point3d *p1=NULL;
    struct point3d *p2=NULL;
    struct point3d *p3=NULL;
    struct point3d *p4=NULL;

    //Half a longitude.
    len1=points_per_ring/2;
    //Half a latitude.
    len2=2*rings;
    //The last or bottom ring to draw.
    gint len3=points_per_ring/2-1;
    gint counter=0;
   
    for(i=0;i<len1;i++)
      {     
        if(i==0)
          {
            //Get one end of triangles of the sphere.
            p1=&g_array_index(sphere, struct point3d, 0);
            p2=&g_array_index(sphere, struct point3d, (2*rings+1)*(2*points_per_ring));
            p3=&g_array_index(sphere, struct point3d, 2*points_per_ring);
            for(j=0;j<len2;j++)
              {   
                set_tri_points_1(q1, p1, p2, p3);   
                q1->index=counter;
                q1++;counter++;              
                p1+=2*points_per_ring;p3+=2*points_per_ring;
                p2+=4;
              } 
          }
        else if(i==len3)
          {
            //Get the other end of triangles of the sphere.
            p1=&g_array_index(sphere, struct point3d, 4*(i));
            p2=&g_array_index(sphere, struct point3d, (2*rings+1)*(2*points_per_ring)+4*(i-1)*rings*2);
            p3=&g_array_index(sphere, struct point3d, 2*points_per_ring+4*(i)); 
            for(j=0;j<len2;j++)
              {   
                set_tri_points_2(q1, p1, p2, p3);   
                q1->index=counter;
                q1++;counter++;              
                p1+=2*points_per_ring;p3+=2*points_per_ring;
                p2+=4;
              } 
          }
        else
          {
            //Get the middle quads of sphere.
            p1=&g_array_index(sphere, struct point3d, 4*(i));
            p2=&g_array_index(sphere, struct point3d, 2*points_per_ring+4*(i));
            p3=&g_array_index(sphere, struct point3d, (2*rings+1)*(2*points_per_ring)+4*(i-1)*rings*2); 
            p4=&g_array_index(sphere, struct point3d, (2*rings+1)*(2*points_per_ring)+4*(i)*rings*2);
            for(j=0;j<len2;j++)
              {   
                set_quad_points(q1, p1, p2, p3, p4);   
                q1->index=counter;
                q1++;counter++;              
                p1+=2*points_per_ring;p2+=2*points_per_ring;p3+=4;p4+=4;
              } 
            
          }    
      }  
           
    q1=&g_array_index(quad, struct bezier_quad, 0);
    struct bezier_quad *q2=&g_array_index(quad_r, struct bezier_quad, 0);
    memcpy(q2, q1, sizeof(struct bezier_quad)*(quad->len));
    
    struct shape *p=g_new(struct shape, 1);
    p->cache_id=SOLID_BEZIER_SPHERE;
    p->s=quad;
    p->sr=quad_r;
    p->rings=rings;
    p->points_per_ring=points_per_ring;
    p->default_drawing=def;
    p->z_max=0.0;
    p->z_min=0.0;
    if(shape_cache!=NULL) g_ptr_array_add(shape_cache, p);
    else g_error("The 3d shape cache isn't initialized.");

    g_array_free(sphere, TRUE);
    g_array_free(one_ring, TRUE);
    g_array_free(two_ring, TRUE);
  }
void set_cosine_wave(gint cache_id, gint rings, gint points_per_ring, gdouble radius, gdouble z_height)
  {
    gint i=0;

    if(cache_id==WIRE_BEZIER_SHEET)
      {
        struct shape *s1=get_drawing_from_shape_cache(WIRE_BEZIER_SHEET, rings, points_per_ring);        
        if(s1!=NULL)
          {
            (s1->z_max)=z_height;
            (s1->z_min)=-z_height;
            struct point3d *p1=&g_array_index(s1->s, struct point3d, 0);
            for(i=0;i<s1->s->len;i++)
              {
                (p1->z)=z_height*cos(sqrt((p1->x)*(p1->x)/radius+(p1->y)*(p1->y)/radius));
                p1++;
              }
          }
      }
    else if(cache_id==WIRE_BEZIER_DISC)
      {
        struct shape *s1=get_drawing_from_shape_cache(WIRE_BEZIER_DISC, rings, points_per_ring);
        if(s1!=NULL)
          {
            (s1->z_max)=z_height;
            (s1->z_min)=-z_height;
            struct point3d *p1=&g_array_index(s1->s, struct point3d, 0);
            for(i=0;i<(s1->s->len);i++)
              {
                (p1->z)=z_height*cos(sqrt((p1->x)*(p1->x)/radius+(p1->y)*(p1->y)/radius));
                p1++;
              }
          }
      }
    else if(cache_id==SOLID_BEZIER_SHEET)
      {
        struct shape *s1=get_drawing_from_shape_cache(SOLID_BEZIER_SHEET, rings, points_per_ring);
        if(s1!=NULL)
          {
            (s1->z_max)=z_height;
            (s1->z_min)=-z_height;
            struct bezier_quad *p1=&g_array_index(s1->s, struct bezier_quad, 0);
            for(i=0;i<(s1->s->len);i++)
              {
                (p1->z1)=z_height*cos(sqrt((p1->x1)*(p1->x1)/radius+(p1->y1)*(p1->y1)/radius));
                (p1->bz1)=z_height*cos(sqrt((p1->bx1)*(p1->bx1)/radius+(p1->by1)*(p1->by1)/radius));
                (p1->bz2)=z_height*cos(sqrt((p1->bx2)*(p1->bx2)/radius+(p1->by2)*(p1->by2)/radius));
                (p1->z2)=z_height*cos(sqrt((p1->x2)*(p1->x2)/radius+(p1->y2)*(p1->y2)/radius));
                (p1->bz3)=z_height*cos(sqrt((p1->bx3)*(p1->bx3)/radius+(p1->by3)*(p1->by3)/radius));
                (p1->bz4)=z_height*cos(sqrt((p1->bx4)*(p1->bx4)/radius+(p1->by4)*(p1->by4)/radius));
                (p1->z3)=z_height*cos(sqrt((p1->x3)*(p1->x3)/radius+(p1->y3)*(p1->y3)/radius));
                (p1->bz5)=z_height*cos(sqrt((p1->bx5)*(p1->bx5)/radius+(p1->by5)*(p1->by5)/radius));
                (p1->bz6)=z_height*cos(sqrt((p1->bx6)*(p1->bx6)/radius+(p1->by6)*(p1->by6)/radius));
                (p1->z4)=z_height*cos(sqrt((p1->x4)*(p1->x4)/radius+(p1->y4)*(p1->y4)/radius));
                (p1->bz7)=z_height*cos(sqrt((p1->bx7)*(p1->bx7)/radius+(p1->by7)*(p1->by7)/radius));
                (p1->bz8)=z_height*cos(sqrt((p1->bx8)*(p1->bx8)/radius+(p1->by8)*(p1->by8)/radius));        
                p1++;
              }
          }
      }    
    else if(cache_id==SOLID_BEZIER_DISC)
      {
        struct shape *s1=get_drawing_from_shape_cache(SOLID_BEZIER_DISC, rings, points_per_ring);
        if(s1!=NULL)
          {
            (s1->z_max)=z_height;
            (s1->z_min)=-z_height;
            struct bezier_quad *p1=&g_array_index(s1->s, struct bezier_quad, 0);
            for(i=0;i<(s1->s->len);i++)
              {
                (p1->z1)=z_height*cos(sqrt((p1->x1)*(p1->x1)/radius+(p1->y1)*(p1->y1)/radius));
                (p1->bz1)=z_height*cos(sqrt((p1->bx1)*(p1->bx1/radius)+(p1->by1)*(p1->by1)/radius));
                (p1->bz2)=z_height*cos(sqrt((p1->bx2)*(p1->bx2)/radius+(p1->by2)*(p1->by2)/radius));
                (p1->z2)=z_height*cos(sqrt((p1->x2)*(p1->x2)/radius+(p1->y2)*(p1->y2)/radius));
                (p1->bz3)=z_height*cos(sqrt((p1->bx3)*(p1->bx3)/radius+(p1->by3)*(p1->by3)/radius));
                (p1->bz4)=z_height*cos(sqrt((p1->bx4)*(p1->bx4)/radius+(p1->by4)*(p1->by4)/radius));
                (p1->z3)=z_height*cos(sqrt((p1->x3)*(p1->x3)/radius+(p1->y3)*(p1->y3)/radius));
                (p1->bz5)=z_height*cos(sqrt((p1->bx5)*(p1->bx5)/radius+(p1->by5)*(p1->by5)/radius));
                (p1->bz6)=z_height*cos(sqrt((p1->bx6)*(p1->bx6)/radius+(p1->by6)*(p1->by6)/radius));
                (p1->z4)=z_height*cos(sqrt((p1->x4)*(p1->x4)/radius+(p1->y4)*(p1->y4)/radius));
                (p1->bz7)=z_height*cos(sqrt((p1->bx7)*(p1->bx7)/radius+(p1->by7)*(p1->by7)/radius));
                (p1->bz8)=z_height*cos(sqrt((p1->bx8)*(p1->bx8)/radius+(p1->by8)*(p1->by8)/radius));       
                p1++;
              }
          }
      }
    else g_warning("Couldn't set a cosine wave.");
  }
void set_shape_z_min_max(gint cache_id, gint rings, gint points_per_ring, gdouble min, gdouble max, gint shape_cache_index)
  {
    struct shape *s1=NULL;
    if(shape_cache_index==-1) s1=get_drawing_from_shape_cache(cache_id, rings, points_per_ring);
    else s1=get_drawing_from_shape_cache_index(cache_id, shape_cache_index);
    if(s1!=NULL)
      {
        s1->z_min=min;
        s1->z_max=max;
      }
  }
void set_bezier_sheet_points(gint cache_id, gint rings, gint points_per_ring, GArray *add_points, gdouble z_min, gdouble z_max)
  {
    gint i=0;
    gint j=0;
    gint rows=rings+1;
    gint columns=points_per_ring+1;

    if((cache_id==SOLID_BEZIER_SHEET)&&(add_points->len!=rows*columns)) 
      {
        g_print("Couldn't set z-points for solid bezier sheet. rows%i * columns%i = len%i\n", rows, columns, add_points->len);
      }
    else if((cache_id==WIRE_BEZIER_SHEET)&&(add_points->len!=rows*columns)) 
      {
        g_print("Couldn't set z-points for wire bezier sheet. (rows%i * columns%i) = len%i\n", rows, columns, add_points->len);
      }
    else if(z_max<z_min) 
      {
        g_print("Couldn't set z-points for bezier sheet. z_max%f < z_min%f\n", z_max, z_min);
      }
    else if(cache_id==WIRE_BEZIER_SHEET)
      {
        struct point3d *z1=&g_array_index(add_points, struct point3d, 0);
        struct shape *s1=get_drawing_from_shape_cache(WIRE_BEZIER_SHEET, rings, points_per_ring);
        if(s1!=NULL)
          {
            (s1->z_max)=z_max;
            (s1->z_min)=z_min;
            struct point3d *p1=&g_array_index(s1->s, struct point3d, 0);
            struct point3d *p2=NULL;

            //Add new values for non bezier control points.
            for(i=0;i<rows;i++)
              {
                for(j=0;j<columns;j++)
                  {
                    (p1->x)+=(z1->x);
                    (p1->y)+=(z1->y);
                    (p1->z)+=(z1->z);
                    p1+=3;z1++;
                  }
               }
            for(i=0;i<columns;i++)
              {
                z1=&g_array_index(add_points, struct point3d, i);
                for(j=0;j<rows;j++)
                  {
                    (p1->x)+=(z1->x);
                    (p1->y)+=(z1->y);
                    (p1->z)+=(z1->z);
                    p1+=3;z1+=columns;
                  }
               }

            //Temp storage for non bezier control points to be used to get the bezier control points.
            GArray *one_row=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), columns);
            g_array_set_size(one_row, columns);
            GArray *one_column=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), rows);
            g_array_set_size(one_column, rows);
            struct point3d *rp1=&g_array_index(one_row, struct point3d, 0);
            struct point3d *cp1=&g_array_index(one_column, struct point3d, 0);

            p1=&g_array_index(s1->s, struct point3d, 0);
            for(i=0;i<rows;i++)
              {
                rp1=&g_array_index(one_row, struct point3d, 0);
                p2=p1;
                for(j=0;j<columns;j++)
                  {
                    rp1->x=p1->x;
                    rp1->y=p1->y;
                    rp1->z=p1->z;
                    p1+=3;rp1++;                
                  }
                GArray *controls=control_points_from_coords_3d(one_row);
                struct controls3d *c1=&g_array_index(controls, struct controls3d, 0);
                p2+=3;
                for(j=0;j<columns-1;j++)
                  {
                    (p2-2)->x=c1->x1;
                    (p2-2)->y=c1->y1;
                    (p2-2)->z=c1->z1;
                    (p2-1)->x=c1->x2;
                    (p2-1)->y=c1->y2;
                    (p2-1)->z=c1->z2;
                    p2+=3;c1++;                
                  } 
                g_array_free(controls, TRUE);            
              }
            for(i=0;i<columns;i++)
              {
                cp1=&g_array_index(one_column, struct point3d, 0);
                p2=p1;
                for(j=0;j<rows;j++)
                  {
                    cp1->x=p1->x;
                    cp1->y=p1->y;
                    cp1->z=p1->z;
                    p1+=3;cp1++;                
                  }
                GArray *controls=control_points_from_coords_3d(one_column);
                struct controls3d *c1=&g_array_index(controls, struct controls3d, 0);
                p2+=3;
                for(j=0;j<rows-1;j++)
                  {
                    (p2-2)->x=c1->x1;
                    (p2-2)->y=c1->y1;
                    (p2-2)->z=c1->z1;
                    (p2-1)->x=c1->x2;
                    (p2-1)->y=c1->y2;
                    (p2-1)->z=c1->z2;
                    p2+=3;c1++;                
                  }
                g_array_free(controls, TRUE);            
              }
            g_array_free(one_row, TRUE);
            g_array_free(one_column, TRUE); 
         } 
      }
    else if(cache_id==SOLID_BEZIER_SHEET)
      {
        //Get the corners of the quad and find the bezier control points.
        struct shape *s1=get_drawing_from_shape_cache(SOLID_BEZIER_SHEET, rings, points_per_ring);
        if(s1!=NULL)
          {
            rows=rings;
            columns=points_per_ring;
            (s1->z_max)=z_max;
            (s1->z_min)=z_min;
            struct bezier_quad *p1=&g_array_index(s1->s, struct bezier_quad, 0);
            struct bezier_quad *p2=NULL;

            //Change z points on the corners of the quad.
            struct point3d *z1=NULL;
            struct point3d *z2=NULL;
            struct point3d *z3=NULL;
            struct point3d *z4=NULL;
            for(i=0;i<rows;i++)
              {
                //Quad are in rows and z points are the lines of the rows and columns so add 1 for the lines.
                z1=&g_array_index(add_points, struct point3d, i*(columns+1));
                z2=&g_array_index(add_points, struct point3d, i*(columns+1)+1);
                z3=&g_array_index(add_points, struct point3d, (i+1)*(columns+1));
                z4=&g_array_index(add_points, struct point3d, (i+1)*(columns+1)+1);
                for(j=0;j<columns;j++)
                  {
                    (p1->x1)+=(z1->x);
                    (p1->y1)+=(z1->y);
                    (p1->z1)+=(z1->z);
                    (p1->x2)+=(z2->x);
                    (p1->y2)+=(z2->y);
                    (p1->z2)+=(z2->z);
                    (p1->x3)+=(z4->x);
                    (p1->y3)+=(z4->y);
                    (p1->z3)+=(z4->z);
                    (p1->x4)+=(z3->x);
                    (p1->y4)+=(z3->y);
                    (p1->z4)+=(z3->z);                 
                    p1++;z1++;z2++;z3++;z4++;
                  }
              }

            GArray *one_row=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), columns+1);
            g_array_set_size(one_row, columns+1);
            GArray *one_row2=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), columns+1);
            g_array_set_size(one_row2, columns+1);
            GArray *one_column=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), rows+1);
            g_array_set_size(one_column, rows+1);
            GArray *one_column2=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), rows+1);
            g_array_set_size(one_column2, rows+1);
            struct point3d *rp1=&g_array_index(one_row, struct point3d, 0);
            struct point3d *rp2=&g_array_index(one_row2, struct point3d, 0);
            struct point3d *cp1=&g_array_index(one_column, struct point3d, 0);
            struct point3d *cp2=&g_array_index(one_column2, struct point3d, 0);

            //Get bezier control points for the rows and fill them in.
            p1=&g_array_index(s1->s, struct bezier_quad, 0);
            for(i=0;i<rows;i++)
              {
                rp1=&g_array_index(one_row, struct point3d, 0);
                rp2=&g_array_index(one_row2, struct point3d, 0);
                p2=p1;
                for(j=0;j<columns;j++)
                  {
                    if(j==0)
                      {
                        rp1->x=p1->x1;
                        rp1->y=p1->y1;
                        rp1->z=p1->z1;
                        rp2->x=p1->x4;
                        rp2->y=p1->y4;
                        rp2->z=p1->z4;
                        rp1++;rp2++;
                        rp1->x=p1->x2;
                        rp1->y=p1->y2;
                        rp1->z=p1->z2; 
                        rp2->x=p1->x3;
                        rp2->y=p1->y3; 
                        rp2->z=p1->z3; 
                        rp1++;rp2++;                   
                      }
                    else
                      {
                        rp1->x=p1->x2;
                        rp1->y=p1->y2;
                        rp1->z=p1->z2;
                        rp2->x=p1->x3;
                        rp2->y=p1->y3;
                        rp2->z=p1->z3; 
                        rp1++;rp2++;
                      } 
                    p1++;
                  }
                GArray *controls=control_points_from_coords_3d(one_row);
                struct controls3d *c1=&g_array_index(controls, struct controls3d, 0);
                GArray *controls2=control_points_from_coords_3d(one_row2);
                struct controls3d *c2=&g_array_index(controls2, struct controls3d, 0);
                for(j=0;j<columns;j++)
                  {
                    p2->bx1=c1->x1;
                    p2->by1=c1->y1;
                    p2->bz1=c1->z1;
                    p2->bx2=c1->x2;
                    p2->by2=c1->y2;
                    p2->bz2=c1->z2;
                    p2->bx6=c2->x1;
                    p2->by6=c2->y1;
                    p2->bz6=c2->z1;
                    p2->bx5=c2->x2;
                    p2->by5=c2->y2;
                    p2->bz5=c2->z2;
                    p2++;c1++;c2++;
                  } 
                g_array_free(controls, TRUE);
                g_array_free(controls2, TRUE);
              }

            //Get bezier control points for the columns and fill them in.
            for(i=0;i<columns;i++)
              {
                cp1=&g_array_index(one_column, struct point3d, 0);
                cp2=&g_array_index(one_column2, struct point3d, 0);
                p1=&g_array_index(s1->s, struct bezier_quad, i);
                for(j=0;j<rows;j++)
                  {
                    if(j==0)
                      {
                        cp1->x=p1->x1;
                        cp1->y=p1->y1;
                        cp1->z=p1->z1;
                        cp2->x=p1->x2;
                        cp2->y=p1->y2;
                        cp2->z=p1->z2;
                        cp1++;cp2++;
                        cp1->x=p1->x4;
                        cp1->y=p1->y4;
                        cp1->z=p1->z4;  
                        cp2->x=p1->x3;
                        cp2->y=p1->y3;
                        cp2->z=p1->z3;
                        cp1++;cp2++;                    
                      }
                    else
                      {
                        cp1->x=p1->x4;
                        cp1->y=p1->y4;
                        cp1->z=p1->z4;
                        cp2->x=p1->x3;
                        cp2->y=p1->y3; 
                        cp2->z=p1->z3; 
                        cp1++;cp2++;
                      } 
                    p1+=columns;
                  }
                GArray *controls=control_points_from_coords_3d(one_column);
                struct controls3d *c1=&g_array_index(controls, struct controls3d, 0);
                GArray *controls2=control_points_from_coords_3d(one_column2);
                struct controls3d *c2=&g_array_index(controls2, struct controls3d, 0);
                p2=&g_array_index(s1->s, struct bezier_quad, i);
                for(j=0;j<rows;j++)
                  {
                    p2->bx8=c1->x1;
                    p2->by8=c1->y1;
                    p2->bz8=c1->z1;
                    p2->bx7=c1->x2;
                    p2->by7=c1->y2;
                    p2->bz7=c1->z2;
                    p2->bx3=c2->x1;
                    p2->by3=c2->y1;
                    p2->bz3=c2->z1;
                    p2->bx4=c2->x2;
                    p2->by4=c2->y2;
                    p2->bz4=c2->z2;
                    p2+=columns;c1++;c2++;
                  } 
                g_array_free(controls, TRUE);
                g_array_free(controls2, TRUE);
              }

            g_array_free(one_row, TRUE);
            g_array_free(one_column, TRUE); 
            g_array_free(one_row2, TRUE);
            g_array_free(one_column2, TRUE);  
         }
      }    
    else g_warning("Error in set_bezier_sheet_points().");
  }
/*
  Similar to set_bezier_sheet_points() but the rows=rings and columns=points_per_ring. Also the
rings need to be connected and it gets 3d bezier control points. It doesn't hold a variable constant
like set_bezier_sheet_points().
*/
void set_bezier_disc_points(gint cache_id, gint rings, gint points_per_ring, GArray *add_points, gdouble z_min, gdouble z_max)
  {
    gint i=0;
    gint j=0;
    gint points=((rings+1)*(points_per_ring+1));

    if((cache_id==SOLID_BEZIER_DISC)&&(add_points->len!=points)) 
      {
        g_print("Couldn't set z-points for solid bezier disc. (rings%i + 1) * (points_per_ring%i + 1) = len%i\n", rings, points_per_ring, add_points->len);
      }
    else if((cache_id==WIRE_BEZIER_DISC)&&(add_points->len!=points)) 
      {
        g_print("Couldn't set z-points for wire bezier disc. (rings%i + 1) * (points_per_ring%i + 1) = len%i\n", rings, points_per_ring, add_points->len);
      }
    else if(z_max<z_min) 
      {
        g_print("Couldn't set z-points for bezier disc. z_max%f < z_min%f\n", z_max, z_min);
      }
    else if(cache_id==WIRE_BEZIER_DISC)
      {
        struct point3d *z1=&g_array_index(add_points, struct point3d, 0);
        struct shape *s1=get_drawing_from_shape_cache(WIRE_BEZIER_DISC, rings, points_per_ring);
        if(s1!=NULL)
          {
            (s1->z_max)=z_max;
            (s1->z_min)=z_min;
            struct point3d *p1=&g_array_index(s1->s, struct point3d, 0);
            struct point3d *p2=NULL;

            //Add z value for non bezier control points.
            for(i=0;i<points_per_ring;i++)
              {
                for(j=0;j<rings+1;j++)
                  {
                    (p1->x)+=(z1->x);
                    (p1->y)+=(z1->y);
                    (p1->z)+=(z1->z);
                    p1+=3;z1++;
                  }
               }
            for(i=0;i<rings;i++)
              {
                z1=&g_array_index(add_points, struct point3d, i+1);
                for(j=0;j<points_per_ring;j++)
                  {
                    (p1->x)+=(z1->x);
                    (p1->y)+=(z1->y);
                    (p1->z)+=(z1->z);
                    p1+=3;z1+=(rings+1);
                  }
                 z1=&g_array_index(add_points, struct point3d, i+1);
                 (p1->x)+=(z1->x);
                 (p1->y)+=(z1->y);
                 (p1->z)+=(z1->z);
                 p1+=3;
               }

            //Temp storage for non bezier control points to be used to get the bezier control points.
            GArray *one_ray=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), rings+1);
            g_array_set_size(one_ray, rings+1);
            GArray *one_ring=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), points_per_ring+1);
            g_array_set_size(one_ring, points_per_ring+1);
            struct point3d *rp1=&g_array_index(one_ray, struct point3d, 0);
            struct point3d *cp1=&g_array_index(one_ring, struct point3d, 0);

            //Control points for rays.
            p1=&g_array_index(s1->s, struct point3d, 0);
            for(i=0;i<points_per_ring;i++)
              {
                rp1=&g_array_index(one_ray, struct point3d, 0);
                p2=p1;
                for(j=0;j<rings+1;j++)
                  {
                    rp1->x=p1->x;
                    rp1->y=p1->y;
                    rp1->z=p1->z;
                    p1+=3;rp1++;                
                  }
                GArray *controls=control_points_from_coords_3d(one_ray);
                struct controls3d *c1=&g_array_index(controls, struct controls3d, 0);
                p2+=3;
                for(j=0;j<rings;j++)
                  {
                    (p2-2)->x=c1->x1;
                    (p2-2)->y=c1->y1;
                    (p2-2)->z=c1->z1;
                    (p2-1)->x=c1->x2;
                    (p2-1)->y=c1->y2;
                    (p2-1)->z=c1->z2;
                    p2+=3;c1++;                
                  } 
                g_array_free(controls, TRUE);            
              }

            //Control points for rings.
            for(i=0;i<rings;i++)
              {
                cp1=&g_array_index(one_ring, struct point3d, 0);
                p2=p1;
                for(j=0;j<points_per_ring+1;j++)
                  {
                    cp1->x=p1->x;
                    cp1->y=p1->y;
                    cp1->z=p1->z;
                    p1+=3;cp1++;                
                  }
                GArray *controls=control_points_from_coords_3d(one_ring);
                struct controls3d *c1=&g_array_index(controls, struct controls3d, 0);
                p2+=3;
                for(j=0;j<points_per_ring;j++)
                  {
                    (p2-2)->x=c1->x1;
                    (p2-2)->y=c1->y1;
                    (p2-2)->z=c1->z1;
                    (p2-1)->x=c1->x2;
                    (p2-1)->y=c1->y2;
                    (p2-1)->z=c1->z2;
                    p2+=3;c1++;                
                  }
                g_array_free(controls, TRUE);            
              }

            g_array_free(one_ray, TRUE);
            g_array_free(one_ring, TRUE);
         }  
      }
    else if(cache_id==SOLID_BEZIER_DISC)
      {
        //Get the corners of the quad and find the bezier control points.
        struct shape *s1=get_drawing_from_shape_cache(SOLID_BEZIER_DISC, rings, points_per_ring);
        if(s1!=NULL)
          {
            (s1->z_max)=z_max;
            (s1->z_min)=z_min;
            struct bezier_quad *p1=&g_array_index(s1->s, struct bezier_quad, 0);
            struct bezier_quad *p2=NULL;
     
            //Change z points on the corners of the quad.
            struct point3d *z1=NULL;
            struct point3d *z2=NULL;
            struct point3d *z3=NULL;
            struct point3d *z4=NULL;
            for(i=0;i<rings;i++)
              {
                z1=&g_array_index(add_points, struct point3d, i*(points_per_ring+1));
                z2=&g_array_index(add_points, struct point3d, i*(points_per_ring+1)+1);
                z3=&g_array_index(add_points, struct point3d, (i+1)*(points_per_ring+1));
                z4=&g_array_index(add_points, struct point3d, (i+1)*(points_per_ring+1)+1);
                p2=p1;
                for(j=0;j<points_per_ring;j++)
                  {
                    (p1->x1)+=(z1->x);
                    (p1->y1)+=(z1->y);
                    (p1->z1)+=(z1->z);
                    (p1->x2)+=(z2->x);
                    (p1->y2)+=(z2->y);
                    (p1->z2)+=(z2->z);
                    (p1->x3)+=(z4->x);
                    (p1->y3)+=(z4->y);
                    (p1->z3)+=(z4->z);
                    (p1->x4)+=(z3->x);
                    (p1->y4)+=(z3->y);
                    (p1->z4)+=(z3->z);                 
                    p1++;z1++;z2++;z3++;z4++;
                  } 
                //Connect the ring.
                p1--;
                (p1->x2)=(p2->x1);
                (p1->y2)=(p2->y1);
                (p1->z2)=(p2->z1);
                (p1->x3)=(p2->x4);
                (p1->y3)=(p2->y4);
                (p1->z3)=(p2->z4);
                p1++;  
              }

            GArray *one_row=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), points_per_ring+1);
            g_array_set_size(one_row, points_per_ring+1);
            GArray *one_row2=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), points_per_ring+1);
            g_array_set_size(one_row2, points_per_ring+1);
            GArray *one_column=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), rings+1);
            g_array_set_size(one_column, rings+1);
            GArray *one_column2=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), rings+1);
            g_array_set_size(one_column2, rings+1);
            struct point3d *rp1=&g_array_index(one_row, struct point3d, 0);
            struct point3d *rp2=&g_array_index(one_row2, struct point3d, 0);
            struct point3d *cp1=&g_array_index(one_column, struct point3d, 0);
            struct point3d *cp2=&g_array_index(one_column2, struct point3d, 0);

            //Get bezier control points for the rings and fill them in.
            p1=&g_array_index(s1->s, struct bezier_quad, 0);
            for(i=0;i<rings;i++)
              {
                rp1=&g_array_index(one_row, struct point3d, 0);
                rp2=&g_array_index(one_row2, struct point3d, 0);
                p2=p1;
                for(j=0;j<points_per_ring;j++)
                  {
                    if(j==0)
                      {
                        rp1->x=p1->x1;
                        rp1->y=p1->y1;
                        rp1->z=p1->z1;
                        rp2->x=p1->x4;
                        rp2->y=p1->y4;
                        rp2->z=p1->z4;
                        rp1++;rp2++;
                        rp1->x=p1->x2;
                        rp1->y=p1->y2; 
                        rp1->z=p1->z2;
                        rp2->x=p1->x3;
                        rp2->y=p1->y3; 
                        rp2->z=p1->z3; 
                        rp1++;rp2++;                   
                      }
                    else
                      {
                        rp1->x=p1->x2;
                        rp1->y=p1->y2;
                        rp1->z=p1->z2;
                        rp2->x=p1->x3;
                        rp2->y=p1->y3;
                        rp2->z=p1->z3; 
                        rp1++;rp2++;
                      } 
                    p1++;
                  }
                //Get full 3d bezier control points.
                GArray *controls=control_points_from_coords_3d(one_row);
                struct controls3d *c1=&g_array_index(controls, struct controls3d, 0);
                GArray *controls2=control_points_from_coords_3d(one_row2);
                struct controls3d *c2=&g_array_index(controls2, struct controls3d, 0);
                for(j=0;j<points_per_ring;j++)
                  {
                    p2->bx1=c1->x1;
                    p2->by1=c1->y1;
                    p2->bz1=c1->z1;
                    p2->bx2=c1->x2;
                    p2->by2=c1->y2;
                    p2->bz2=c1->z2;
                    p2->bx6=c2->x1;
                    p2->by6=c2->y1;
                    p2->bz6=c2->z1;
                    p2->bx5=c2->x2;
                    p2->by5=c2->y2;
                    p2->bz5=c2->z2;
                    p2++;c1++;c2++;
                  } 
                g_array_free(controls, TRUE);
                g_array_free(controls2, TRUE);
              }

            //Get bezier control points for the columns and fill them in.
            for(i=0;i<points_per_ring;i++)
              {
                cp1=&g_array_index(one_column, struct point3d, 0);
                cp2=&g_array_index(one_column2, struct point3d, 0);
                p1=&g_array_index(s1->s, struct bezier_quad, i);
                for(j=0;j<rings;j++)
                  {
                    if(j==0)
                      {
                        cp1->x=p1->x1;
                        cp1->y=p1->y1;
                        cp1->z=p1->z1;
                        cp2->x=p1->x2;
                        cp2->y=p1->y2;
                        cp2->z=p1->z2;
                        cp1++;cp2++;
                        cp1->x=p1->x4;
                        cp1->y=p1->y4; 
                        cp1->z=p1->z4; 
                        cp2->x=p1->x3;
                        cp2->y=p1->y3;
                        cp2->z=p1->z3;
                        cp1++;cp2++;                    
                      }
                    else
                      {
                        cp1->x=p1->x4;
                        cp1->y=p1->y4;
                        cp1->z=p1->z4;
                        cp2->x=p1->x3;
                        cp2->y=p1->y3; 
                        cp2->z=p1->z3; 
                        cp1++;cp2++;
                      } 
                    p1+=points_per_ring;
                  }
                GArray *controls=control_points_from_coords_3d(one_column);
                struct controls3d *c1=&g_array_index(controls, struct controls3d, 0);
                GArray *controls2=control_points_from_coords_3d(one_column2);
                struct controls3d *c2=&g_array_index(controls2, struct controls3d, 0);
                p2=&g_array_index(s1->s, struct bezier_quad, i);
                for(j=0;j<rings;j++)
                  {
                    p2->bx8=c1->x1;
                    p2->by8=c1->y1;
                    p2->bz8=c1->z1;
                    p2->bx7=c1->x2;
                    p2->by7=c1->y2;
                    p2->bz7=c1->z2;
                    p2->bx3=c2->x1;
                    p2->by3=c2->y1;
                    p2->bz3=c2->z1;
                    p2->bx4=c2->x2;
                    p2->by4=c2->y2;
                    p2->bz4=c2->z2;
                    p2+=points_per_ring;c1++;c2++;
                  } 
                g_array_free(controls, TRUE);
                g_array_free(controls2, TRUE);
              }

            g_array_free(one_row, TRUE);
            g_array_free(one_column, TRUE); 
            g_array_free(one_row2, TRUE);
            g_array_free(one_column2, TRUE);  
         }
      }    
    else g_warning("Error in set_bezier_disc_points().");
  }
void set_bezier_sphere_single_point(gint cache_id, gint rings, gint points_per_ring, struct morph_sphere_point *point, gint shape_cache_index)
  {
    //Set a point on the sphere.
    gint i=0;
    gint j=0;
    gdouble hyp=0.0;
    gint len=((rings*2)*(points_per_ring/2+1))-1;
  
    if(point->index<0||point->index>len) 
      {
        g_warning("Point %i outside of range 0<=point<=%i and couldn't be set.\n", point->index, len);
      }
    else if(cache_id==WIRE_BEZIER_SPHERE)
      {
        struct shape *s1=NULL;
        if(shape_cache_index==-1) s1=get_drawing_from_shape_cache(WIRE_BEZIER_SPHERE, rings, points_per_ring);
        else s1=get_drawing_from_shape_cache_index(WIRE_BEZIER_SPHERE, shape_cache_index);
        if(s1!=NULL)
          {
            gint index=0;
            gint move1=rings*2;
            gint move2=points_per_ring/2+1;
            index=(point->index)/(move1)+(point->index+move1)%(move1)*(move2); 
            gint offset=((point->index)+(rings*2))%(rings*2);
            struct point3d *p1=&g_array_index(s1->s, struct point3d, 0);
         
            //Update longitude.
            if((point->index)<(rings*2))
              {
                //Bottom points.
                if(index==0) p1=&g_array_index(s1->s, struct point3d, 0);
                else p1=&g_array_index(s1->s, struct point3d, index*4-4*offset);
                (p1->x)+=(gdouble)(point->x);
                (p1->y)+=(gdouble)(point->y);
                (p1->z)+=(gdouble)(point->z);
                if((point->ray)!=0)
                  {
                    hyp=sqrt((p1->x)*(p1->x)+(p1->y)*(p1->y)+(p1->z)*(p1->z));
                    (p1->x)+=(gdouble)(point->ray)*(p1->x)/hyp;
                    (p1->y)+=(gdouble)(point->ray)*(p1->y)/hyp;
                    (p1->z)+=(gdouble)(point->ray)*(p1->z)/hyp;
                  }
              }
            else
              {
                p1=&g_array_index(s1->s, struct point3d, index*4-4*(offset+1));
                ((p1+3)->x)+=(gdouble)(point->x);
                ((p1+3)->y)+=(gdouble)(point->y);
                ((p1+3)->z)+=(gdouble)(point->z); 
                if((point->ray)!=0)
                  {
                    hyp=sqrt(((p1+3)->x)*((p1+3)->x)+((p1+3)->y)*((p1+3)->y)+((p1+3)->z)*((p1+3)->z));
                    ((p1+3)->x)+=(gdouble)(point->ray)*((p1+3)->x)/hyp;
                    ((p1+3)->y)+=(gdouble)(point->ray)*((p1+3)->y)/hyp;
                    ((p1+3)->z)+=(gdouble)(point->ray)*((p1+3)->z)/hyp;
                  }
              }   
           
           //Update latitude point.
           if((point->index)>(rings*2)&&(point->index)<(points_per_ring/2)*(rings*2)+1)
             { 
               index=(2*rings)*(2*points_per_ring)+((point->index)-(rings*2))*4-4;      
               p1=&g_array_index(s1->s, struct point3d, index);
               //Ring connection.           
               if(((point->index)+(rings*2))%(rings*2)==1)
                  {
                    (p1->x)+=(gdouble)(point->x);
                    (p1->y)+=(gdouble)(point->y);
                    (p1->z)+=(gdouble)(point->z);
                    if((point->ray)!=0)
                      {
                        hyp=sqrt((p1->x)*(p1->x)+(p1->y)*(p1->y)+(p1->z)*(p1->z));
                        (p1->x)+=(gdouble)(point->ray)*(p1->x)/hyp;
                        (p1->y)+=(gdouble)(point->ray)*(p1->y)/hyp;
                        (p1->z)+=(gdouble)(point->ray)*(p1->z)/hyp;
                      }
                    ((p1+3)->x)+=(gdouble)(point->x);
                    ((p1+3)->y)+=(gdouble)(point->y);
                    ((p1+3)->z)+=(gdouble)(point->z); 
                    if((point->ray)!=0)
                      {
                        hyp=sqrt(((p1+3)->x)*((p1+3)->x)+((p1+3)->y)*((p1+3)->y)+((p1+3)->z)*((p1+3)->z));
                        ((p1+3)->x)+=(gdouble)(point->ray)*((p1+3)->x)/hyp;
                        ((p1+3)->y)+=(gdouble)(point->ray)*((p1+3)->y)/hyp;
                        ((p1+3)->z)+=(gdouble)(point->ray)*((p1+3)->z)/hyp;
                      }
                  }
                else
                  {
                    ((p1+3)->x)+=(gdouble)(point->x);
                    ((p1+3)->y)+=(gdouble)(point->y);
                    ((p1+3)->z)+=(gdouble)(point->z); 
                    if((point->ray)!=0)
                      {
                        hyp=sqrt(((p1+3)->x)*((p1+3)->x)+((p1+3)->y)*((p1+3)->y)+((p1+3)->z)*((p1+3)->z));
                        ((p1+3)->x)+=(gdouble)(point->ray)*((p1+3)->x)/hyp;
                        ((p1+3)->y)+=(gdouble)(point->ray)*((p1+3)->y)/hyp;
                        ((p1+3)->z)+=(gdouble)(point->ray)*((p1+3)->z)/hyp;
                      }
                  }              
              }
          }
      }
    else if(cache_id==SOLID_BEZIER_SPHERE)
      {
        struct shape *s1=NULL;
        if(shape_cache_index==-1) s1=get_drawing_from_shape_cache(SOLID_BEZIER_SPHERE, rings, points_per_ring);
        else s1=get_drawing_from_shape_cache_index(SOLID_BEZIER_SPHERE, shape_cache_index);
        
        if(s1!=NULL)
          {
            struct bezier_quad *p1=NULL;
            struct bezier_quad *p2=NULL;
            struct bezier_quad *p3=NULL;
            struct bezier_quad *p4=NULL;

            //Add array values to the sphere.
            if((point->index)<(rings*points_per_ring))
              {       
                p1=&g_array_index(s1->s, struct bezier_quad, point->index);
                (p1->x1)+=(gdouble)(point->x);
                (p1->y1)+=(gdouble)(point->y);
                (p1->z1)+=(gdouble)(point->z);
                if((point->ray)!=0)
                  {
                    hyp=sqrt((p1->x1)*(p1->x1)+(p1->y1)*(p1->y1)+(p1->z1)*(p1->z1));
                    (p1->x1)+=(gdouble)(point->ray)*(p1->x1)/hyp;
                    (p1->y1)+=(gdouble)(point->ray)*(p1->y1)/hyp;
                    (p1->z1)+=(gdouble)(point->ray)*(p1->z1)/hyp; 
                  }
               }

            //Add the top of the sphere points.
            if((point->index)>rings*points_per_ring-1)
              {
                p1=&g_array_index(s1->s, struct bezier_quad, (point->index)-2*rings);
                (p1->x2)+=(gdouble)(point->x);
                (p1->y2)+=(gdouble)(point->y);
                (p1->z2)+=(gdouble)(point->z);
                //Update degenerate quad points for the triangle.
                (p1->bx3)+=(gdouble)(point->x);
                (p1->by3)+=(gdouble)(point->y);
                (p1->bz3)+=(gdouble)(point->z);
                (p1->bx4)+=(gdouble)(point->x);
                (p1->by4)+=(gdouble)(point->y);
                (p1->bz4)+=(gdouble)(point->z);
                (p1->x3)+=(gdouble)(point->x);
                (p1->y3)+=(gdouble)(point->y);
                (p1->z3)+=(gdouble)(point->z);
                if((point->ray)!=0)
                  {
                    hyp=sqrt((p1->x2)*(p1->x2)+(p1->y2)*(p1->y2)+(p1->z2)*(p1->z2));
                    (p1->x2)+=(gdouble)(point->ray)*(p1->x2)/hyp;
                    (p1->y2)+=(gdouble)(point->ray)*(p1->y2)/hyp;
                    (p1->z2)+=(gdouble)(point->ray)*(p1->z2)/hyp; 
                    (p1->bx3)=(p1->x2);
                    (p1->by3)=(p1->y2);
                    (p1->bz3)=(p1->z2);
                    (p1->bx4)=(p1->x2);
                    (p1->by4)=(p1->y2);
                    (p1->bz4)=(p1->z2);
                    (p1->x3)=(p1->x2);
                    (p1->y3)=(p1->y2);
                    (p1->z3)=(p1->z2);
                  }
              }

            //Update bottom degenrate quad points of the triangles.
            if((point->index)<2*rings)
              {
                p1=&g_array_index(s1->s, struct bezier_quad, point->index);
                (p1->x4)+=(gdouble)(point->x);
                (p1->y4)+=(gdouble)(point->y);
                (p1->z4)+=(gdouble)(point->z);
                (p1->bx7)+=(gdouble)(point->x);
                (p1->by7)+=(gdouble)(point->y);
                (p1->bz7)+=(gdouble)(point->z);
                (p1->bx8)+=(gdouble)(point->x);
                (p1->by8)+=(gdouble)(point->y);
                (p1->bz8)+=(gdouble)(point->z);
                if((point->ray)!=0)
                  {
                    hyp=sqrt((p1->x4)*(p1->x4)+(p1->y4)*(p1->y4)+(p1->z4)*(p1->z4));
                    (p1->x4)+=(gdouble)(point->ray)*(p1->x4)/hyp;
                    (p1->y4)+=(gdouble)(point->ray)*(p1->y4)/hyp;
                    (p1->z4)+=(gdouble)(point->ray)*(p1->z4)/hyp; 
                    (p1->bx7)+=(gdouble)(point->ray)*(p1->x4)/hyp;
                    (p1->by7)+=(gdouble)(point->ray)*(p1->y4)/hyp;
                    (p1->bz7)+=(gdouble)(point->ray)*(p1->z4)/hyp; 
                    (p1->bx8)+=(gdouble)(point->ray)*(p1->x4)/hyp;
                    (p1->by8)+=(gdouble)(point->ray)*(p1->y4)/hyp;
                    (p1->bz8)+=(gdouble)(point->ray)*(p1->z4)/hyp; 
                  }
              }
                
            //Update 4 adjacent quads.
            gint len2=points_per_ring/2-1;
            for(i=0;i<len2;i++)
              { 
                p1=&g_array_index(s1->s, struct bezier_quad, i*rings*2);
                p2=&g_array_index(s1->s, struct bezier_quad, i*rings*2+1);
                p3=&g_array_index(s1->s, struct bezier_quad, (i+1)*rings*2);
                p4=&g_array_index(s1->s, struct bezier_quad, (i+1)*rings*2+1);
                for(j=0;j<rings*2-1;j++)
                  {
                    p1->x3=p4->x1;
                    p1->y3=p4->y1;
                    p1->z3=p4->z1;
                    p3->x4=p4->x1;
                    p3->y4=p4->y1;
                    p3->z4=p4->z1;
                    p2->x2=p4->x1;
                    p2->y2=p4->y1;
                    p2->z2=p4->z1;
                    p1++;p2++;p3++;p4++;
                  }
                 //Ring connection.
                 p2--;p4--;
                 p1=&g_array_index(s1->s, struct bezier_quad, i*rings*2);
                 p3=&g_array_index(s1->s, struct bezier_quad, (i+1)*rings*2);
                 p1->x2=p3->x1;
                 p1->y2=p3->y1;
                 p1->z2=p3->z1;
                 p2->x3=p3->x1;
                 p2->y3=p3->y1;
                 p2->z3=p3->z1;
                 p4->x4=p3->x1;
                 p4->y4=p3->y1;
                 p4->z4=p3->z1;         
               } 
           }   
      }
    else g_warning("Couldn't set morphing sphere point.");
  }
void quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
void set_revolve_standard_point(gdouble x, gdouble y, gdouble z)
  {
    revolve[0]=x;
    revolve[1]=y;
    revolve[2]=z;
  }
void get_revolved_point(gdouble *x, gdouble *y, gdouble *z)
  {
    *x=revolve_r[0];
    *y=revolve_r[1];
    *z=revolve_r[2];
  }
void revolve_standard_point(gdouble qrs[9])
  {
    revolve_r[0]=(revolve[0]*qrs[0])+(revolve[1]*qrs[1])+(revolve[2]*qrs[2]);
    revolve_r[1]=(revolve[0]*qrs[3])+(revolve[1]*qrs[4])+(revolve[2]*qrs[5]);
    revolve_r[2]=(revolve[0]*qrs[6])+(revolve[1]*qrs[7])+(revolve[2]*qrs[8]);
  }
void rotate_cache_shapes(gdouble qrs[9])
  {
    guint i=0;

    const struct shape *s1=NULL;
    for(i=0;i<(shape_cache->len);i++)
      {
        s1=g_ptr_array_index(shape_cache, i); 
        rotate_shape(s1, qrs);  
      } 
  }
void rotate_cache_shape_index(gdouble qrs[9], guint index)
  {
    const struct shape *s1=NULL;

    if(index<(shape_cache->len))
      {
        s1=g_ptr_array_index(shape_cache, index); 
        rotate_shape(s1, qrs);  
      } 
    else g_print("Warning: The shape at the index wasn't found!\n");
  }
void save_wire_morph_sphere_to_file(const gchar *file_name, GArray *morph_points, gint radius, gint rings, gint points_per_ring)
  {
    guint i=0;
    
    //Build a key file. 
    GKeyFile *key=g_key_file_new();
    g_key_file_set_integer(key, "shape", "cache_id", WIRE_BEZIER_SPHERE); 
    g_key_file_set_integer(key, "shape", "radius", radius); 
    g_key_file_set_integer(key, "shape", "rings", rings); 
    g_key_file_set_integer(key, "shape", "points_per_ring", points_per_ring); 
        
    //Get the shape data.
    GArray *shape_data=NULL;
    if(morph_points->len>0)
      {
        shape_data=g_array_new(FALSE, FALSE, sizeof(gint));
        struct morph_sphere_point *p1=&g_array_index(morph_points, struct morph_sphere_point, 0);
        for(i=0;i<morph_points->len;i++)
          {
            g_array_append_val(shape_data, p1->index);
            g_array_append_val(shape_data, p1->x);
            g_array_append_val(shape_data, p1->y);
            g_array_append_val(shape_data, p1->z);
            g_array_append_val(shape_data, p1->ray);
            p1++;
          }         
        g_key_file_set_integer_list(key, "shape", "shape_data", (gint*)(shape_data->data), shape_data->len);
      }
    else g_print("There are 0 morph points to add to the solid sphere.\n");
        
    //Save to file.
    GError *error=NULL;
    g_key_file_save_to_file(key, file_name, &error);
    if(error!=NULL)
      {
        g_print("Error writing key-value file. %s\n", error->message);
        g_error_free(error);
      }
        
    if(shape_data!=NULL) g_array_free(shape_data, TRUE);
    g_key_file_free(key);
  }
GArray* get_wire_morph_sphere_from_file(const gchar *file_name, gint *cache_id, gint *radius, gint *rings, gint *points_per_ring)
  {
    gint i=0;
    GArray *difference=NULL;
    GError *error=NULL;
    GKeyFile *key=g_key_file_new();
    g_key_file_load_from_file(key, file_name, G_KEY_FILE_NONE, &error);
    if(error!=NULL)
      {
        g_print("Error reading key-value file. %s\n", error->message);
        g_error_free(error);
        goto exit;
      }
          
    gint cache_id1=g_key_file_get_integer(key, "shape", "cache_id", &error);
    if(error!=NULL)
      {
        g_print("%s\n", error->message);
        g_error_free(error);
        goto exit;
      }
      
    //Is this a wire bezier sphere?
    if(cache_id1!=WIRE_BEZIER_SPHERE)
      {
        g_print("This isn't a wire bezier sphere file. File not opened.\n"); 
        goto exit;
      }
      
    gint radius1=g_key_file_get_integer(key, "shape", "radius", &error);
    if(error!=NULL)
      {
        g_print("%s\n", error->message);
        g_error_free(error);
        goto exit;
      }
      
    gint rings1=g_key_file_get_integer(key, "shape", "rings", &error);
    if(error!=NULL)
      {
        g_print("%s\n", error->message);
        g_error_free(error);
        goto exit;
      }
      
    gint points_per_ring1=g_key_file_get_integer(key, "shape", "points_per_ring", &error);
    if(error!=NULL)
      {
        g_print("%s\n", error->message);
        g_error_free(error);
        goto exit;
      }
      
    *cache_id=cache_id1;
    *radius=radius1;
    *rings=rings1;
    *points_per_ring=points_per_ring1;
          
    //Get the key-value data.
    gsize length=0;
    gint *shape_data=g_key_file_get_integer_list(key, "shape", "shape_data", &length, &error);
    if(error!=NULL)
      {
        g_print("%s\n", error->message);
        g_error_free(error);
        goto exit;
      }
      
    //Put the data in a morph_sphere_points array.
    if(length>0&&length%5==0)
      {
        difference=g_array_sized_new(FALSE, TRUE, sizeof(struct morph_sphere_point), length/5);
        g_array_set_size(difference, length/5);
        gint *p1=shape_data;
        struct morph_sphere_point *p2=&g_array_index(difference, struct morph_sphere_point, 0);
        for(i=0;i<length;i+=5)
          {
            p2->index=(*p1);
            p2->x=(*(p1+1));
            p2->y=(*(p1+2));
            p2->z=(*(p1+3));
            p2->ray=(*(p1+4));
            p1+=5;p2++;
          } 
        g_free(shape_data);
      }
    else
      {
        g_warning("Couldn't load the difference array with the file shape data.\n");
        goto exit;
      }  
    
    exit:
    g_key_file_free(key);
    return difference;
  }
void save_solid_morph_sphere_to_file(const gchar *file_name, GArray *morph_points, gint radius, gint rings, gint points_per_ring)
  {
    guint i=0;
    
    //Build a key file. 
    GKeyFile *key=g_key_file_new();
    g_key_file_set_integer(key, "shape", "cache_id", SOLID_BEZIER_SPHERE); 
    g_key_file_set_integer(key, "shape", "radius", radius); 
    g_key_file_set_integer(key, "shape", "rings", rings); 
    g_key_file_set_integer(key, "shape", "points_per_ring", points_per_ring); 
        
    //Get the shape data.
    GArray *shape_data=NULL;
    if(morph_points->len>0)
      {
        shape_data=g_array_new(FALSE, FALSE, sizeof(gint));
        struct morph_sphere_point *p1=&g_array_index(morph_points, struct morph_sphere_point, 0);
        for(i=0;i<morph_points->len;i++)
          {
            g_array_append_val(shape_data, p1->index);
            g_array_append_val(shape_data, p1->x);
            g_array_append_val(shape_data, p1->y);
            g_array_append_val(shape_data, p1->z);
            g_array_append_val(shape_data, p1->ray);
            p1++;
          }          
        g_key_file_set_integer_list(key, "shape", "shape_data", (int*)(shape_data->data), shape_data->len);
      }
    else g_print("There are 0 morph points to add to the solid sphere.\n");
        
    //Save to file.
    GError *error=NULL;
    g_key_file_save_to_file(key, file_name, &error);
    if(error!=NULL)
      {
        g_print("Error writing key-value file. %s\n", error->message);
        g_error_free(error);
      }
        
    if(shape_data!=NULL) g_array_free(shape_data, TRUE);
    g_key_file_free(key);    
  }
GArray* get_solid_morph_sphere_from_file(const gchar *file_name, gint *cache_id, gint *radius, gint *rings, gint *points_per_ring)
  {
    gint i=0;
    GArray *difference=NULL;
    GError *error=NULL;
    GKeyFile *key=g_key_file_new();
    g_key_file_load_from_file(key, file_name, G_KEY_FILE_NONE, &error);
    if(error!=NULL)
      {
        g_print("Error reading key-value file. %s\n", error->message);
        g_error_free(error);
        goto exit;
      }
          
    gint cache_id1=g_key_file_get_integer(key, "shape", "cache_id", &error);
    if(error!=NULL)
      {
        g_print("%s\n", error->message);
        g_error_free(error);
        goto exit;
      }
    
    //Is this a solid bezier sphere?
    if(cache_id1!=SOLID_BEZIER_SPHERE)
      {
        g_print("This isn't a solid bezier sphere file. File not opened.\n"); 
        goto exit;
      }
     
    gint radius1=g_key_file_get_integer(key, "shape", "radius", &error);
    if(error!=NULL)
      {
        g_print("%s\n", error->message);
        g_error_free(error);
        goto exit;
      }
      
    gint rings1=g_key_file_get_integer(key, "shape", "rings", &error);
    if(error!=NULL)
      {
        g_print("%s\n", error->message);
        g_error_free(error);
        goto exit;
      }
      
    gint points_per_ring1=g_key_file_get_integer(key, "shape", "points_per_ring", &error);
    if(error!=NULL)
      {
        g_print("%s\n", error->message);
        g_error_free(error);
        goto exit;
      }
      
    *cache_id=cache_id1;
    *radius=radius1;
    *rings=rings1;
    *points_per_ring=points_per_ring1;
           
    //Get the key-value data.
    gsize length=0;
    gint *shape_data=g_key_file_get_integer_list(key, "shape", "shape_data", &length, &error);
    if(error!=NULL)
      {
        g_print("%s\n", error->message);
        g_error_free(error);
        goto exit;
      } 
      
    //Put the data in a morph_sphere_points array.
    if(length>0&&length%5==0)
      {
        difference=g_array_sized_new(FALSE, TRUE, sizeof(struct morph_sphere_point), length/5);
        g_array_set_size(difference, length/5);
        gint *p1=shape_data;
        struct morph_sphere_point *p2=&g_array_index(difference, struct morph_sphere_point, 0);
        for(i=0;i<length;i+=5)
          {
            p2->index=(*p1);
            p2->x=(*(p1+1));
            p2->y=(*(p1+2));
            p2->z=(*(p1+3));
            p2->ray=(*(p1+4));
            p1+=5;p2++;
          } 
        g_free(shape_data);
      }
    else
      {
        g_warning("Couldn't load the difference array with the file shape data.\n");
        goto exit;
      }  

    exit:
    g_key_file_free(key);
    return difference;
  }
static void matrix_multiply_rotations(const gdouble qrs1[9], const gdouble qrs2[9], gdouble qrs3[9])
  {
    //Multiply two 3x3 matrices.
    qrs3[0]=qrs1[0]*qrs2[0]+qrs1[3]*qrs2[1]+qrs1[6]*qrs2[2];
    qrs3[1]=qrs1[1]*qrs2[0]+qrs1[4]*qrs2[1]+qrs1[7]*qrs2[2];
    qrs3[2]=qrs1[2]*qrs2[0]+qrs1[5]*qrs2[1]+qrs1[8]*qrs2[2];

    qrs3[3]=qrs1[0]*qrs2[3]+qrs1[3]*qrs2[4]+qrs1[6]*qrs2[5];
    qrs3[4]=qrs1[1]*qrs2[3]+qrs1[4]*qrs2[4]+qrs1[7]*qrs2[5];
    qrs3[5]=qrs1[2]*qrs2[3]+qrs1[5]*qrs2[4]+qrs1[8]*qrs2[5];

    qrs3[6]=qrs1[0]*qrs2[6]+qrs1[3]*qrs2[7]+qrs1[6]*qrs2[8];
    qrs3[7]=qrs1[1]*qrs2[6]+qrs1[4]*qrs2[7]+qrs1[7]*qrs2[8];
    qrs3[8]=qrs1[2]*qrs2[6]+qrs1[5]*qrs2[7]+qrs1[8]*qrs2[8];
  }
static void rotate_shape(const struct shape *s1, gdouble qrs[9])
  {
    guint i=0;

    if(s1->cache_id==CHECKERBOARD)
      {
        /*
          Need z for perspective. Test code that needs to be removed. Make the checkboard
          a little more interesting.
        */
        gdouble z=0;
        struct point3d *p1=&g_array_index(s1->s, struct point3d, 0);
        struct point3d *p2=&g_array_index(s1->sr, struct point3d, 0);
        for(i=0;i<(s1->s->len);i++)
          {
            z=((*p1).x*qrs[6])+((*p1).y*qrs[7]);
            //Test changing the perspective of checkerboard at end of expression. *(1.0...
            (*p2).x=(((*p1).x*qrs[0])+((*p1).y*qrs[1]))*(1.0+(0.60*z));
            (*p2).y=(((*p1).x*qrs[3])+((*p1).y*qrs[4]))*(1.0+(0.60*z));
            p1++;p2++;
          }
      }
    else if(s1->cache_id==CONE)
      {
        rotate_quad_plane(s1, qrs);
      }
    else if(s1->cache_id==TEXT_RING)
      {
        rotate_quad_plane(s1, qrs);
      }
    else if(s1->cache_id==SOLID_CYLINDER)
      {
        rotate_bezier_quad(s1, qrs);
      }
    else if(s1->cache_id==SOLID_SPHERE)
      {
        rotate_bezier_quad(s1, qrs);
      }
    else if(s1->cache_id==SOLID_FUNNEL)
      {
        rotate_bezier_quad(s1, qrs);
      }
    else if(s1->cache_id==SOLID_TWIST)
      {
        rotate_bezier_quad(s1, qrs);
      }
    else if(s1->cache_id==SOLID_TORUS)
      {
        rotate_bezier_quad(s1, qrs);
      }
    else if(s1->cache_id==SOLID_BEZIER_SHEET)
      {
        rotate_bezier_quad(s1, qrs);
      }
    else if(s1->cache_id==SOLID_BEZIER_DISC)
      {
        rotate_bezier_quad(s1, qrs);
      }
    else if(s1->cache_id==SOLID_BEZIER_SPHERE)
      {
        rotate_bezier_quad(s1, qrs);
      }
    else if(s1->cache_id<SHAPE_LIST_END)
      {
        rotate_shape_xyz(s1, qrs);
      }
    else
      {
        g_print("Warning: Couldn't find shape to rotate!\n");
      }
  }
static void rotate_shape_xyz(const struct shape *s1, gdouble qrs[9])
  {
    guint i=0;
    struct point3d *p1=&g_array_index(s1->s, struct point3d, 0);
    struct point3d *p2=&g_array_index(s1->sr, struct point3d, 0);
    for(i=0;i<(s1->s->len);i++)
      {
        (*p2).x=((*p1).x*qrs[0])+((*p1).y*qrs[1])+((*p1).z*qrs[2]);
        (*p2).y=((*p1).x*qrs[3])+((*p1).y*qrs[4])+((*p1).z*qrs[5]);
        (*p2).z=((*p1).x*qrs[6])+((*p1).y*qrs[7])+((*p1).z*qrs[8]);
        p1++,p2++;
      }
  }
static void rotate_quad_plane(const struct shape *s1, gdouble qrs[9])
  {
    guint i=0;
    struct quad_plane2 *p1=&g_array_index(s1->s, struct quad_plane2, 0);
    struct quad_plane2 *p2=&g_array_index(s1->sr, struct quad_plane2, 0);
    for(i=0;i<(s1->s->len);i++)
      {
        (*p2).x1=((*p1).x1*qrs[0])+((*p1).y1*qrs[1])+((*p1).z1*qrs[2]);
        (*p2).y1=((*p1).x1*qrs[3])+((*p1).y1*qrs[4])+((*p1).z1*qrs[5]);
        (*p2).z1=((*p1).x1*qrs[6])+((*p1).y1*qrs[7])+((*p1).z1*qrs[8]); 

        (*p2).x2=((*p1).x2*qrs[0])+((*p1).y2*qrs[1])+((*p1).z2*qrs[2]);
        (*p2).y2=((*p1).x2*qrs[3])+((*p1).y2*qrs[4])+((*p1).z2*qrs[5]);
        (*p2).z2=((*p1).x2*qrs[6])+((*p1).y2*qrs[7])+((*p1).z2*qrs[8]);

        (*p2).x3=((*p1).x3*qrs[0])+((*p1).y3*qrs[1])+((*p1).z3*qrs[2]);
        (*p2).y3=((*p1).x3*qrs[3])+((*p1).y3*qrs[4])+((*p1).z3*qrs[5]);
        (*p2).z3=((*p1).x3*qrs[6])+((*p1).y3*qrs[7])+((*p1).z3*qrs[8]);
        
        (*p2).x4=((*p1).x4*qrs[0])+((*p1).y4*qrs[1])+((*p1).z4*qrs[2]);
        (*p2).y4=((*p1).x4*qrs[3])+((*p1).y4*qrs[4])+((*p1).z4*qrs[5]);
        (*p2).z4=((*p1).x4*qrs[6])+((*p1).y4*qrs[7])+((*p1).z4*qrs[8]); 

        (*p2).index=(*p1).index;
        
        p1++,p2++;
      }
  }
static void rotate_bezier_quad(const struct shape *s1, gdouble qrs[9])
  {
    guint i=0;
    struct bezier_quad *p1=&g_array_index(s1->s, struct bezier_quad, 0);
    struct bezier_quad *p2=&g_array_index(s1->sr, struct bezier_quad, 0);
    for(i=0;i<(s1->s->len);i++)
      {
        (*p2).x1=((*p1).x1*qrs[0])+((*p1).y1*qrs[1])+((*p1).z1*qrs[2]);
        (*p2).y1=((*p1).x1*qrs[3])+((*p1).y1*qrs[4])+((*p1).z1*qrs[5]);
        (*p2).z1=((*p1).x1*qrs[6])+((*p1).y1*qrs[7])+((*p1).z1*qrs[8]); 
        (*p2).bx1=((*p1).bx1*qrs[0])+((*p1).by1*qrs[1])+((*p1).bz1*qrs[2]);
        (*p2).by1=((*p1).bx1*qrs[3])+((*p1).by1*qrs[4])+((*p1).bz1*qrs[5]);
        (*p2).bz1=((*p1).bx1*qrs[6])+((*p1).by1*qrs[7])+((*p1).bz1*qrs[8]);
        (*p2).bx2=((*p1).bx2*qrs[0])+((*p1).by2*qrs[1])+((*p1).bz2*qrs[2]);
        (*p2).by2=((*p1).bx2*qrs[3])+((*p1).by2*qrs[4])+((*p1).bz2*qrs[5]);
        (*p2).bz2=((*p1).bx2*qrs[6])+((*p1).by2*qrs[7])+((*p1).bz2*qrs[8]);

        (*p2).x2=((*p1).x2*qrs[0])+((*p1).y2*qrs[1])+((*p1).z2*qrs[2]);
        (*p2).y2=((*p1).x2*qrs[3])+((*p1).y2*qrs[4])+((*p1).z2*qrs[5]);
        (*p2).z2=((*p1).x2*qrs[6])+((*p1).y2*qrs[7])+((*p1).z2*qrs[8]);
        (*p2).bx3=((*p1).bx3*qrs[0])+((*p1).by3*qrs[1])+((*p1).bz3*qrs[2]);
        (*p2).by3=((*p1).bx3*qrs[3])+((*p1).by3*qrs[4])+((*p1).bz3*qrs[5]);
        (*p2).bz3=((*p1).bx3*qrs[6])+((*p1).by3*qrs[7])+((*p1).bz3*qrs[8]);
        (*p2).bx4=((*p1).bx4*qrs[0])+((*p1).by4*qrs[1])+((*p1).bz4*qrs[2]);
        (*p2).by4=((*p1).bx4*qrs[3])+((*p1).by4*qrs[4])+((*p1).bz4*qrs[5]);
        (*p2).bz4=((*p1).bx4*qrs[6])+((*p1).by4*qrs[7])+((*p1).bz4*qrs[8]);

        (*p2).x3=((*p1).x3*qrs[0])+((*p1).y3*qrs[1])+((*p1).z3*qrs[2]);
        (*p2).y3=((*p1).x3*qrs[3])+((*p1).y3*qrs[4])+((*p1).z3*qrs[5]);
        (*p2).z3=((*p1).x3*qrs[6])+((*p1).y3*qrs[7])+((*p1).z3*qrs[8]);
        (*p2).bx5=((*p1).bx5*qrs[0])+((*p1).by5*qrs[1])+((*p1).bz5*qrs[2]);
        (*p2).by5=((*p1).bx5*qrs[3])+((*p1).by5*qrs[4])+((*p1).bz5*qrs[5]);
        (*p2).bz5=((*p1).bx5*qrs[6])+((*p1).by5*qrs[7])+((*p1).bz5*qrs[8]);
        (*p2).bx6=((*p1).bx6*qrs[0])+((*p1).by6*qrs[1])+((*p1).bz6*qrs[2]);
        (*p2).by6=((*p1).bx6*qrs[3])+((*p1).by6*qrs[4])+((*p1).bz6*qrs[5]);
        (*p2).bz6=((*p1).bx6*qrs[6])+((*p1).by6*qrs[7])+((*p1).bz6*qrs[8]);

        (*p2).x4=((*p1).x4*qrs[0])+((*p1).y4*qrs[1])+((*p1).z4*qrs[2]);
        (*p2).y4=((*p1).x4*qrs[3])+((*p1).y4*qrs[4])+((*p1).z4*qrs[5]);
        (*p2).z4=((*p1).x4*qrs[6])+((*p1).y4*qrs[7])+((*p1).z4*qrs[8]); 
        (*p2).bx7=((*p1).bx7*qrs[0])+((*p1).by7*qrs[1])+((*p1).bz7*qrs[2]);
        (*p2).by7=((*p1).bx7*qrs[3])+((*p1).by7*qrs[4])+((*p1).bz7*qrs[5]);
        (*p2).bz7=((*p1).bx7*qrs[6])+((*p1).by7*qrs[7])+((*p1).bz7*qrs[8]);
        (*p2).bx8=((*p1).bx8*qrs[0])+((*p1).by8*qrs[1])+((*p1).bz8*qrs[2]);
        (*p2).by8=((*p1).bx8*qrs[3])+((*p1).by8*qrs[4])+((*p1).bz8*qrs[5]);
        (*p2).bz8=((*p1).bx8*qrs[6])+((*p1).by8*qrs[7])+((*p1).bz8*qrs[8]);

        (*p2).index=(*p1).index;
        
        p1++,p2++;
      }
  }
/*
  This function can return null for an uninitialized bezier shape. 
*/
static struct shape* get_drawing_from_shape_cache(gint cache_id, gint rings, gint points_per_ring)
  {
    guint i=0;
    gboolean shape_found=FALSE;

    //Get the drawing from the shape cache.
    struct shape *s1=NULL;
    if(cache_id>=0&&cache_id<SHAPE_LIST_END)
      {
        for(i=0;i<shape_cache->len;i++)
          {
            s1=g_ptr_array_index(shape_cache, i);
            if((s1->cache_id==cache_id)&&(s1->rings==rings)&&(s1->points_per_ring==points_per_ring))
              {
                shape_found=TRUE;
                rotate_shape(s1, rm);
                break;
              }
          }

        //Check if there is a default shape after checking for a defined shape.
        if(shape_found==FALSE)
          {
            for(i=0;i<shape_cache->len;i++)
              { 
                if((s1->cache_id==cache_id)&&(s1->default_drawing==TRUE))
                  {
                    shape_found=TRUE;
                    rotate_shape(s1, rm);
                    break;
                  }         
              }
           }

        //Couldn't find the drawing in the cache so initialize it and try again.
        if(shape_found==FALSE)
          {
            initialize_shape(cache_id, rings, points_per_ring);
            //The shape should have been added to the end.
            for(i=shape_cache->len-1;i>=0;i--)
              {
                s1=g_ptr_array_index(shape_cache, i);
                if(s1->cache_id==cache_id&&s1->rings==rings&&s1->points_per_ring==points_per_ring)
                  {
                    rotate_shape(s1, rm);
                    break;
                  }
                else if(s1->cache_id==cache_id&&s1->default_drawing==TRUE)
                  {
                    rotate_shape(s1, rm);
                    break;
                  }
              }
          }
      }
    else g_print("Warning: Invalid shape cache_id requested! Null returned from get_drawing_from_shape_cache().\n");

    return s1;             
  }
static struct shape* get_drawing_from_shape_cache_index(gint cache_id, gint array_index)
  {
    struct shape *s1=NULL;
    
    if(array_index<(shape_cache->len))
      {
        s1=g_ptr_array_index(shape_cache, array_index); 
        rotate_shape(s1, rm);  
      } 
    else g_print("Warning: The shape at the index wasn't found! Null returned from get_drawing_from_shape_cache_index().\n");
    
    return s1; 
  }
void set_transparency(gboolean value)
  {
    transparent=value;
  }
void draw_spring(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;
    struct point3d d1;
    struct point3d d2;
    struct controls2d c1; 
    gint length=0;

    const struct shape *s1=get_drawing_from_shape_cache(SPRING, 0, 0);

    if(s1!=NULL)
      {
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        GArray *control1=control_points_from_coords_2d(s1->sr);
        length=(s1->sr->len)-1;
        for(i=0;i<length;i++)
          {
            d1=g_array_index(s1->sr, struct point3d, i);
            d2=g_array_index(s1->sr, struct point3d, i+1);
            c1=g_array_index(control1, struct controls2d, i);
            cairo_move_to(cr, w1*d1.x, h1*d1.y);
            cairo_curve_to(cr, w1*c1.x1, h1*c1.y1, w1*c1.x2, h1*c1.y2, w1*d2.x, h1*d2.y);
            cairo_stroke(cr);
          }
        g_array_free(control1, TRUE);
      }
  }
void draw_solid_cylinder(cairo_t *cr, gdouble w1, gdouble h1, gint points_per_ring)
  {
    gint i=0; 
    //Line width of 3.
    gdouble ux=3;
    gdouble uy=3;
    gdouble c1[4]={1.0, 0.0, 1.0, 1.0};
    gdouble c2[4]={1.0, 1.0, 0.0, 1.0};

    const struct shape *s1=get_drawing_from_shape_cache(SOLID_CYLINDER, 0, points_per_ring);
    
    if(s1!=NULL)
      {    
        struct bezier_quad *p1=&g_array_index(s1->sr, struct bezier_quad, 0);
        g_array_sort(s1->sr, compare_quads_bezier);

        cairo_save(cr);
        cairo_scale(cr, w1, h1); 
        cairo_device_to_user_distance(cr, &ux, &uy);
        if(ux<uy) ux=uy; 
        cairo_set_line_width(cr, ux);      
        for(i=0;i<(s1->sr->len);i++)
          {
            cairo_move_to(cr, p1->x1, p1->y1);
            cairo_curve_to(cr, p1->bx1, p1->by1, p1->bx2, p1->by2, p1->x2, p1->y2);
            cairo_line_to(cr, p1->x3, p1->y3);
            cairo_curve_to(cr, p1->bx5, p1->by5, p1->bx6, p1->by6, p1->x4, p1->y4);
            cairo_close_path(cr);
            if((p1->index)%2) cairo_set_source_rgba(cr, c1[0], c1[1], c1[2], c1[3]); 
            else cairo_set_source_rgba(cr, c2[0], c2[1], c2[2], c2[3]); 
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr); 
            p1++;    
          }
        cairo_restore(cr);
      }
  }
void draw_cube(cairo_t *cr, gdouble w1, gdouble h1)
  {
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    gdouble alpha=1.0;

    const struct shape *s1=get_drawing_from_shape_cache(CUBE, 0, 0);

    if(s1!=NULL)
      {
        cairo_select_font_face(cr, "courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
        cairo_set_font_size(cr, 36.0*w1/100.0);

        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
        struct point3d *p2=&g_array_index(s1->sr, struct point3d, 1);
        struct point3d *p3=&g_array_index(s1->sr, struct point3d, 2);
        struct point3d *p4=&g_array_index(s1->sr, struct point3d, 3);
        struct point3d *p5=&g_array_index(s1->sr, struct point3d, 4);
        struct point3d *p6=&g_array_index(s1->sr, struct point3d, 5);
        struct point3d *p7=&g_array_index(s1->sr, struct point3d, 6);
        struct point3d *p8=&g_array_index(s1->sr, struct point3d, 7);

        /*
          Just draw the 3 top planes of the cube for solid colors. If tranparent=TRUE the planes
          are not drawn from back to front. The draw_cube_transparent() will draw from back
          to front.
        */
        if(transparent||((p1->z)+(p2->z)+(p3->z)+(p4->z)>0.0))
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, alpha);
            cairo_move_to(cr, w1*(p1->x), h1*(p1->y));
            cairo_line_to(cr, w1*(p2->x), h1*(p2->y));
            cairo_line_to(cr, w1*(p3->x), h1*(p3->y));
            cairo_line_to(cr, w1*(p4->x), h1*(p4->y));
            cairo_close_path(cr);
            cairo_fill(cr);

            //Add some text to the green square.
            cairo_save(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, alpha);
            cairo_matrix_init(&matrix1, rm[0], rm[3], rm[1], rm[4], w1*((p1->x)+(p2->x)+(p3->x)+(p4->x))/4.0, h1*((p1->y)+(p2->y)+(p3->y)+(p4->y))/4.0);
            cairo_transform(cr, &matrix1);
            cairo_text_extents(cr, "Cairo3d", &extents);
            cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
            cairo_show_text(cr, "Cairo3d");
            cairo_restore(cr);
          }
        if(transparent||((p5->z)+(p6->z)+(p7->z)+(p8->z)>0.0))
          {
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, alpha);
            cairo_move_to(cr, w1*(p5->x), h1*(p5->y));
            cairo_line_to(cr, w1*(p6->x), h1*(p6->y));
            cairo_line_to(cr, w1*(p7->x), h1*(p7->y));
            cairo_line_to(cr, w1*(p8->x), h1*(p8->y));
            cairo_close_path(cr);
            cairo_fill(cr);
          }
        if(transparent||((p1->z)+(p5->z)+(p6->z)+(p2->z)>0.0))
          {
            cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, alpha);
            cairo_move_to(cr, w1*(p1->x), h1*(p1->y));
            cairo_line_to(cr, w1*(p5->x), h1*(p5->y));
            cairo_line_to(cr, w1*(p6->x), h1*(p6->y));
            cairo_line_to(cr, w1*(p2->x), h1*(p2->y));
            cairo_close_path(cr);
            cairo_fill(cr);
          }
        if(transparent||((p2->z)+(p6->z)+(p7->z)+(p3->z)>0.0))
          {
            cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, alpha);
            cairo_move_to(cr, w1*(p2->x), h1*(p2->y));
            cairo_line_to(cr, w1*(p6->x), h1*(p6->y));
            cairo_line_to(cr, w1*(p7->x), h1*(p7->y));
            cairo_line_to(cr, w1*(p3->x), h1*(p3->y));
            cairo_close_path(cr);
            cairo_fill(cr);
          }
        if(transparent||((p3->z)+(p7->z)+(p8->z)+(p4->z)>0.0))
          {
            cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, alpha);
            cairo_move_to(cr, w1*(p3->x), h1*(p3->y));
            cairo_line_to(cr, w1*(p7->x), h1*(p7->y));
            cairo_line_to(cr, w1*(p8->x), h1*(p8->y));
            cairo_line_to(cr, w1*(p4->x), h1*(p4->y));
            cairo_close_path(cr);
            cairo_fill(cr);
          }
        if(transparent||((p4->z)+(p8->z)+(p5->z)+(p1->z)>0.0))
          {
            cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, alpha);
            cairo_move_to(cr, w1*(p4->x), h1*(p4->y));
            cairo_line_to(cr, w1*(p8->x), h1*(p8->y));
            cairo_line_to(cr, w1*(p5->x), h1*(p5->y));
            cairo_line_to(cr, w1*(p1->x), h1*(p1->y));
            cairo_close_path(cr);
            cairo_fill(cr);
          }  
      }   
  }
void draw_transparent_cube(cairo_t *cr, gdouble w1, gdouble h1)
  {
    /*
        Draw the cube with painters algorithm or sorting the faces of the cube. Draw from back
    to front. This way you can draw with transparency and have the back facing fonts a different
    color than the front facing fonts. If you just want the front facing sides set the 
    transparency=FALSE and change to alpha=1.0.
    */
    gint i=0;
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;    
    gdouble qrs1[9];
    gdouble qrs2[9];
    gdouble alpha=0.3;

    const struct shape *s1=get_drawing_from_shape_cache(CUBE, 0, 0);

    if(s1!=NULL)
      {
        cairo_select_font_face(cr, "courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
        cairo_set_font_size(cr, 50.0*w1/100.0);
        struct point3d d1=g_array_index(s1->sr, struct point3d, 0);
        struct point3d d2=g_array_index(s1->sr, struct point3d, 1);
        struct point3d d3=g_array_index(s1->sr, struct point3d, 2);
        struct point3d d4=g_array_index(s1->sr, struct point3d, 3);
        struct point3d d5=g_array_index(s1->sr, struct point3d, 4);
        struct point3d d6=g_array_index(s1->sr, struct point3d, 5);
        struct point3d d7=g_array_index(s1->sr, struct point3d, 6);
        struct point3d d8=g_array_index(s1->sr, struct point3d, 7);

        //Sort the planes.
        struct plane_order po[6];
        po[0].id=0;
        po[0].value=d1.z+d2.z+d3.z+d4.z;
        po[1].id=1;
        po[1].value=d5.z+d6.z+d7.z+d8.z;
        po[2].id=2;
        po[2].value=d3.z+d7.z+d8.z+d4.z;
        po[3].id=3;
        po[3].value=d2.z+d6.z+d7.z+d3.z;
        po[4].id=4;
        po[4].value=d1.z+d5.z+d6.z+d2.z;
        po[5].id=5;
        po[5].value=d4.z+d8.z+d5.z+d1.z;
        qsort(po, 6, sizeof(struct plane_order), compare_planes);

        //Draw cube faces in z-order.
        for(i=0;i<6;i++)
          {
            if(po[i].id==0&&(transparent||po[i].value>0.0))
              {
                cairo_save(cr);        
                cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, alpha);
                cairo_move_to(cr, w1*d1.x, h1*d1.y);
                cairo_line_to(cr, w1*d2.x, h1*d2.y);
                cairo_line_to(cr, w1*d3.x, h1*d3.y);
                cairo_line_to(cr, w1*d4.x, h1*d4.y);
                cairo_close_path(cr);
                cairo_fill_preserve(cr);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_stroke(cr);

                cairo_matrix_init(&matrix1, rm[0], rm[3], rm[1], rm[4], w1*(d1.x+d2.x+d3.x+d4.x)/4.0, h1*(d1.y+d2.y+d3.y+d4.y)/4.0);
                cairo_transform(cr, &matrix1);

                cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
                cairo_rectangle(cr, -60.0*w1/100.0, -60.0*h1/100.0, 120.0*w1/100.0, 120.0*h1/100.0);
                cairo_stroke(cr);  

                if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
                cairo_text_extents(cr, "4", &extents);
                cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
                cairo_show_text(cr, "4");
                cairo_restore(cr);
              }
      
            else if(po[i].id==1&&(transparent||po[i].value>0.0))
              {
                cairo_save(cr);
                cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, alpha);
                cairo_move_to(cr, w1*d5.x, h1*d5.y);
                cairo_line_to(cr, w1*d6.x, h1*d6.y);
                cairo_line_to(cr, w1*d7.x, h1*d7.y);
                cairo_line_to(cr, w1*d8.x, h1*d8.y);
                cairo_close_path(cr);
                cairo_fill_preserve(cr);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_stroke(cr);

                cairo_matrix_init(&matrix1, rm[0], rm[3], rm[1], rm[4], w1*(d5.x+d6.x+d7.x+d8.x)/4.0, h1*(d5.y+d6.y+d7.y+d8.y)/4.0);
                cairo_transform(cr, &matrix1);

                cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
                cairo_rectangle(cr, -60.0*w1/100.0, -60.0*h1/100.0, 120.0*w1/100.0, 120.0*h1/100.0);
                cairo_stroke(cr);  

                if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
                cairo_text_extents(cr, "3", &extents);
                cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
                cairo_show_text(cr, "3");
                cairo_restore(cr);
              }

            else if(po[i].id==2&&(transparent||po[i].value>0.0))
              {
                cairo_save(cr);
                cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, alpha);
                cairo_move_to(cr, w1*d3.x, h1*d3.y);
                cairo_line_to(cr, w1*d7.x, h1*d7.y);
                cairo_line_to(cr, w1*d8.x, h1*d8.y);
                cairo_line_to(cr, w1*d4.x, h1*d4.y);
                cairo_close_path(cr);
                cairo_fill_preserve(cr);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_stroke(cr);

                quaternion_rotation(0.0, -G_PI/2.0, 0.0, qrs1);
                matrix_multiply_rotations(qrs1, rm, qrs2);
                cairo_matrix_init(&matrix1, qrs2[0], qrs2[3], qrs2[1], qrs2[4], w1*(d3.x+d7.x+d8.x+d4.x)/4.0, h1*(d3.y+d7.y+d8.y+d4.y)/4.0);
                cairo_transform(cr, &matrix1);

                cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
                cairo_rectangle(cr, -60.0*w1/100.0, -60.0*h1/100.0, 120.0*w1/100.0, 120.0*h1/100.0);
                cairo_stroke(cr);  

                if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
                cairo_text_extents(cr, "1", &extents);
                cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
                cairo_show_text(cr, "1");
                cairo_restore(cr);
              }

            else if(po[i].id==3&&(transparent||po[i].value>0.0))
              {
                cairo_save(cr);
                cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, alpha);
                cairo_move_to(cr, w1*d2.x, h1*d2.y);
                cairo_line_to(cr, w1*d6.x, h1*d6.y);
                cairo_line_to(cr, w1*d7.x, h1*d7.y);
                cairo_line_to(cr, w1*d3.x, h1*d3.y);
                cairo_close_path(cr);
                cairo_fill_preserve(cr);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_stroke(cr);

                quaternion_rotation(0.0, 0.0, -G_PI/2.0, qrs1);
                matrix_multiply_rotations(qrs1, rm, qrs2);
                cairo_matrix_init(&matrix1, qrs2[0], qrs2[3], qrs2[1], qrs2[4], w1*(d2.x+d6.x+d7.x+d3.x)/4.0, h1*(d2.y+d6.y+d7.y+d3.y)/4.0);
                cairo_transform(cr, &matrix1);

                cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
                cairo_rectangle(cr, -60.0*w1/100.0, -60.0*h1/100.0, 120.0*w1/100.0, 120.0*h1/100.0);
                cairo_stroke(cr);  

                if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
                cairo_text_extents(cr, "5", &extents);
                cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
                cairo_show_text(cr, "5");
                cairo_restore(cr);
              }

            else if(po[i].id==4&&(transparent||po[i].value>0.0))
              {
                cairo_save(cr);
                cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, alpha);
                cairo_move_to(cr, w1*d1.x, h1*d1.y);
                cairo_line_to(cr, w1*d5.x, h1*d5.y);
                cairo_line_to(cr, w1*d6.x, h1*d6.y);
                cairo_line_to(cr, w1*d2.x, h1*d2.y);
                cairo_close_path(cr);
                cairo_fill_preserve(cr);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_stroke(cr);

                quaternion_rotation(0.0, G_PI/2.0, 0.0, qrs1);
                matrix_multiply_rotations(qrs1, rm, qrs2);
                cairo_matrix_init(&matrix1, qrs2[0], qrs2[3], qrs2[1], qrs2[4], w1*(d1.x+d5.x+d6.x+d2.x)/4.0, h1*(d1.y+d5.y+d6.y+d2.y)/4.0);
                cairo_transform(cr, &matrix1);

                cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
                cairo_rectangle(cr, -60.0*w1/100.0, -60.0*h1/100.0, 120.0*w1/100.0, 120.0*h1/100.0);
                cairo_stroke(cr);  

                if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
                cairo_text_extents(cr, "6", &extents);
                cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
                cairo_show_text(cr, "6");
                cairo_restore(cr);
              }

            else if(po[i].id==5&&(transparent||po[i].value>0.0))
              {
                cairo_save(cr);
                cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, alpha);
                cairo_move_to(cr, w1*d4.x, h1*d4.y);
                cairo_line_to(cr, w1*d8.x, h1*d8.y);
                cairo_line_to(cr, w1*d5.x, h1*d5.y);
                cairo_line_to(cr, w1*d1.x, h1*d1.y);
                cairo_close_path(cr);
                cairo_fill_preserve(cr);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_stroke(cr);

                quaternion_rotation(0.0, 0.0, -G_PI/2.0, qrs1);
                matrix_multiply_rotations(qrs1, rm, qrs2);
                cairo_matrix_init(&matrix1, qrs2[0], qrs2[3], qrs2[1], qrs2[4], w1*(d4.x+d8.x+d5.x+d1.x)/4.0, h1*(d4.y+d8.y+d5.y+d1.y)/4.0);
                cairo_transform(cr, &matrix1);

                cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
                cairo_rectangle(cr, -60.0*w1/100.0, -60.0*h1/100.0, 120.0*w1/100.0, 120.0*h1/100.0);
                cairo_stroke(cr);  

                if(po[i].value>0.0) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                else cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1.0);
                cairo_text_extents(cr, "2", &extents);
                cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
                cairo_show_text(cr, "2");
                cairo_restore(cr);
              }
          } 
      }   
  }
void draw_checkerboard(cairo_t *cr, gdouble w1, gdouble h1, gint rows, gint columns)
  {
    gint i=0;
    gint j=0;
    gint counter=0;
    gdouble c1[4]={0.0, 0.0, 1.0, 1.0};
    gdouble c2[4]={0.0, 1.0, 1.0, 1.0};
    gint offset1=columns+1;
    gint offset2=columns+2;

    const struct shape *s1=get_drawing_from_shape_cache(CHECKERBOARD, rows, columns);

    if(s1!=NULL)
      {
        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
        
        //Draw the 64 squares of the checkerboard.
        for(i=0;i<rows;i++)
          {
            for(j=0;j<columns;j++)
              {
                counter++;
                if(counter%2==0) cairo_set_source_rgba(cr, c1[0], c1[1], c1[2], c1[3]);
                else cairo_set_source_rgba(cr, c2[0], c2[1], c2[2], c2[3]);
                cairo_move_to(cr, w1*((*p1).x), h1*((*p1).y));
                cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
                cairo_line_to(cr, w1*((*(p1+offset2)).x), h1*((*(p1+offset2)).y));
                cairo_line_to(cr, w1*((*(p1+offset1)).x), h1*((*(p1+offset1)).y));
                cairo_close_path(cr);
                cairo_fill_preserve(cr);
                cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
                cairo_stroke(cr);
                p1++;
              }
            p1++;
            counter++;
          }
      }
  }
void draw_wire_bezier_sheet(cairo_t *cr, gint rows, gint columns)
  {
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(WIRE_BEZIER_SHEET, rows, columns);
    
    if(s1!=NULL)
      {
        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);

        //Add one for rows and columns to number of lines in the sheet.
        rows++;

        //Draw horizontal lines.
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        for(i=0;i<rows;i++)
          {
            cairo_move_to(cr, ((*p1).x), ((*p1).y)); 
            for(j=0;j<columns;j++)
              {
                cairo_curve_to(cr, ((*(p1+1)).x), ((*(p1+1)).y), ((*(p1+2)).x), ((*(p1+2)).y), ((*(p1+3)).x), ((*(p1+3)).y));
                p1+=3;
              }
            cairo_stroke(cr);
            p1+=3;
          } 

        //Draw vertical lines.
        cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
        rows--;columns++;
        p1=&g_array_index(s1->sr, struct point3d, (s1->sr->len)/2);
        for(i=0;i<columns;i++)
          {
            cairo_move_to(cr, ((*p1).x), ((*p1).y)); 
            for(j=0;j<rows;j++)
              {
                cairo_curve_to(cr, ((*(p1+1)).x), ((*(p1+1)).y), ((*(p1+2)).x), ((*(p1+2)).y), ((*(p1+3)).x), ((*(p1+3)).y));
                p1+=3;
              }
            cairo_stroke(cr);
            p1+=3;
          }
      } 
  }
void draw_solid_bezier_sheet(cairo_t *cr, gint rows, gint columns)
  {
    gint i=0;

    const struct shape *s1=get_drawing_from_shape_cache(SOLID_BEZIER_SHEET, rows, columns);

    if(s1!=NULL)
      {
        g_array_sort(s1->sr, compare_quads_bezier);

        struct bezier_quad *p1=&g_array_index(s1->sr, struct bezier_quad, 0);
         
        for(i=0;i<(s1->sr->len);i++)
          {
            cairo_move_to(cr, (p1->x1), (p1->y1));
            cairo_curve_to(cr, (p1->bx1), (p1->by1), (p1->bx2), (p1->by2), (p1->x2), (p1->y2));
            cairo_curve_to(cr, (p1->bx3), (p1->by3), (p1->bx4), (p1->by4), (p1->x3), (p1->y3));
               
            cairo_curve_to(cr, (p1->bx5), (p1->by5), (p1->bx6), (p1->by6), (p1->x4), (p1->y4));
            cairo_curve_to(cr, (p1->bx7), (p1->by7), (p1->bx8), (p1->by8), (p1->x1), (p1->y1));
            cairo_close_path(cr);
            
            if(((p1->index)+1)%2==0) cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0); 
            cairo_fill(cr);
            p1++;    
          } 
      }   
  }
void draw_solid_bezier_sheet_mesh(cairo_t *cr, gint rows, gint columns)
  {
    //Drawing the lines on the gradients smooths the boundries between the quads.
    gint i=0;
    gint idx=0;
    gdouble r;
    gdouble g;
    gdouble b;

    const struct shape *s1=get_drawing_from_shape_cache(SOLID_BEZIER_SHEET, rows, columns);

    if(s1!=NULL)
      {
        g_array_sort(s1->sr, compare_quads_bezier);

        struct bezier_quad *p1=&g_array_index(s1->sr, struct bezier_quad, 0);
        struct bezier_quad *p2=NULL;

        //Draw each quad separately.
        for(i=0;i<(s1->sr->len);i++)
          {
            cairo_pattern_t *pattern=cairo_pattern_create_mesh();
       
            idx=(p1->index);
            p2=&g_array_index(s1->s, struct bezier_quad, idx);

            cairo_mesh_pattern_begin_patch(pattern);
            cairo_mesh_pattern_move_to(pattern, (p1->x1), (p1->y1));
            cairo_mesh_pattern_curve_to(pattern, (p1->bx1), (p1->by1), (p1->bx2), (p1->by2), (p1->x2), (p1->y2));
            cairo_mesh_pattern_curve_to(pattern, (p1->bx3), (p1->by3), (p1->bx4), (p1->by4), (p1->x3), (p1->y3));
            cairo_mesh_pattern_curve_to(pattern, (p1->bx5), (p1->by5), (p1->bx6), (p1->by6), (p1->x4), (p1->y4));
            cairo_mesh_pattern_curve_to(pattern, (p1->bx7), (p1->by7), (p1->bx8), (p1->by8), (p1->x1), (p1->y1));

            //Test hsv.
            gint hsv=1;
            gdouble diff=(s1->z_max)-(s1->z_min);
            gdouble max=(s1->z_max);
            if(hsv==0)
              {
                 hsv2rgb((max-(p2->z1))/diff*240.0, 1.0, 1.0, &r, &g, &b);
                 cairo_mesh_pattern_set_corner_color_rgba(pattern, 0, r, g, b, 1.0);
                 hsv2rgb((max-(p2->z2))/diff*240.0, 1.0, 1.0, &r, &g, &b);
                 cairo_mesh_pattern_set_corner_color_rgba(pattern, 1, r, g, b, 1.0);
                 hsv2rgb((max-(p2->z3))/diff*240.0, 1.0, 1.0, &r, &g, &b);
                 cairo_mesh_pattern_set_corner_color_rgba(pattern, 2, r, g, b, 1.0);
                 hsv2rgb((max-(p2->z4))/diff*240.0, 1.0, 1.0, &r, &g, &b);
                 cairo_mesh_pattern_set_corner_color_rgba(pattern, 3, r, g, b, 1.0);       
                 cairo_mesh_pattern_end_patch(pattern);
              }
            else if(hsv==1)
              {
                cairo_mesh_pattern_set_corner_color_rgba(pattern, 0, 1.0-(max-(p2->z1))/diff, 1.0-(max-(p2->z1))/diff, (max-(p2->z1))/diff, 1.0);
                cairo_mesh_pattern_set_corner_color_rgba(pattern, 1, 1.0-(max-(p2->z2))/diff, 1.0-(max-(p2->z2))/diff, (max-(p2->z2))/diff, 1.0);
                cairo_mesh_pattern_set_corner_color_rgba(pattern, 2, 1.0-(max-(p2->z3))/diff, 1.0-(max-(p2->z3))/diff, (max-(p2->z3))/diff, 1.0);
                cairo_mesh_pattern_set_corner_color_rgba(pattern, 3, 1.0-(max-(p2->z4))/diff, 1.0-(max-(p2->z4))/diff, (max-(p2->z4))/diff, 1.0);
                cairo_mesh_pattern_end_patch(pattern);
              }
            else
              {
                cairo_mesh_pattern_set_corner_color_rgba(pattern, 0, 0.0, 0.0, 1.0, 1.0);
                cairo_mesh_pattern_set_corner_color_rgba(pattern, 1, 1.0, 1.0, 0.0, 1.0);
                cairo_mesh_pattern_set_corner_color_rgba(pattern, 2, 0.0, 1.0, 1.0, 1.0);
                cairo_mesh_pattern_set_corner_color_rgba(pattern, 3, 0.0, 1.0, 1.0, 1.0);
                cairo_mesh_pattern_end_patch(pattern);
              }

            cairo_set_source(cr, pattern);
            //Fill with paint.
            cairo_paint(cr);

            cairo_path_t *path=cairo_mesh_pattern_get_path(pattern, 0);
            cairo_append_path(cr, path);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
            cairo_path_destroy(path);

            cairo_pattern_destroy(pattern);

            p1++;    
          }
      }
  }
void draw_ball(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;
    gdouble c1[4]={0.0, 0.0, 0.0, 1.0};
    gdouble c2[4]={0.0, 1.0, 1.0, 1.0};
    gint counter=1;

    const struct shape *s1=get_drawing_from_shape_cache(BALL, rings, points_per_ring);

    if(s1!=NULL)
      {
        gint array_len=s1->sr->len;
        gint rings_c=(s1->rings)-1;
        gint offset1=(s1->points_per_ring)+2;
        gint offset2=(s1->points_per_ring)+1;
        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
        struct point3d top=g_array_index(s1->sr, struct point3d, array_len-2);
        struct point3d bottom=g_array_index(s1->sr, struct point3d, array_len-1);

        //Draw the top triangles.
        for(i=0;i<(s1->points_per_ring);i++)
          {
            if(counter%2==0) cairo_set_source_rgba(cr, c1[0], c1[1], c1[2], c1[3]);
            else cairo_set_source_rgba(cr, c2[0], c2[1], c2[2], c2[3]);
            if(transparent||(top.z+(*(p1)).z+(*(p1+1)).z>0.0))
              {
                cairo_move_to(cr, w1*top.x, h1*top.y);
                cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
                cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
                cairo_close_path(cr);
                cairo_fill(cr);
              }
            counter++;
            p1++;
          }

        //Draw middle quads.
        counter++;
        p1=&g_array_index(s1->sr, struct point3d, 0);
        for(i=0;i<rings_c;i++)
          {
            for(j=0;j<(s1->points_per_ring);j++)
              {
                if(counter%2==0) cairo_set_source_rgba(cr, c1[0], c1[1], c1[2], c1[3]);
                else cairo_set_source_rgba(cr, c2[0], c2[1], c2[2], c2[3]);
                if(transparent||((*(p1)).z+(*(p1+1)).z+(*(p1+offset1)).z+(*(p1+offset2)).z>0.0))
                  {
                    cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
                    cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
                    cairo_line_to(cr, w1*((*(p1+offset1)).x), h1*((*(p1+offset1)).y));
                    cairo_line_to(cr, w1*((*(p1+offset2)).x), h1*((*(p1+offset2)).y));
                    cairo_close_path(cr);
                    cairo_fill(cr);
                  }
                counter++;
                p1++;
              }
            counter++;
            p1++;
          }

        //Draw the bottom triangles.
        p1=&g_array_index(s1->sr, struct point3d, ((s1->rings)-1)*((s1->points_per_ring)+1));
        for(i=0;i<(s1->points_per_ring);i++)
          {
            if(counter%2==0) cairo_set_source_rgba(cr, c1[0], c1[1], c1[2], c1[3]);
            else cairo_set_source_rgba(cr, c2[0], c2[1], c2[2], c2[3]);
            if(transparent||(bottom.z+(*(p1)).z+(*(p1+1)).z>0.0))
              {
                cairo_move_to(cr, w1*bottom.x, h1*bottom.y);
                cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
                cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
                cairo_close_path(cr);
                cairo_fill(cr);
              }
            counter++;
            p1++;
          }
      }
  }
void draw_ball_mesh(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    /*
        Draw a ball with a mesh gradient. Use the symetry of the ball to draw the quads. That
      way there isn't a need to sort the quads before drawing like in the funnel.
    */
    gint i=0;
    gint j=0;
    gdouble c1[4]={0.0, 0.0, 1.0, 1.0};
    gdouble c2[4]={0.0, 0.0, 1.0, 1.0};

    const struct shape *s1=get_drawing_from_shape_cache(BALL, rings, points_per_ring);

    if(s1!=NULL)
      {
        gint array_len=s1->sr->len;
        gint rings_c=(s1->rings)-1;
        gint offset1=(s1->points_per_ring)+2;
        gint offset2=(s1->points_per_ring)+1;
        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
        struct point3d top=g_array_index(s1->sr, struct point3d, array_len-2);
        struct point3d bottom=g_array_index(s1->sr, struct point3d, array_len-1);

        //Draw the top triangles cyan.
        cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
        for(i=0;i<(s1->points_per_ring);i++)
          {
            if(top.z+(*(p1)).z+(*(p1+1)).z>0.0)
              {
                cairo_move_to(cr, w1*top.x, h1*top.y);
                cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
                cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
                cairo_close_path(cr);
                cairo_fill(cr);
              }
            p1++;
          }

        //Draw middle quads.
        p1=&g_array_index(s1->sr, struct point3d, 0);
        gdouble den=(gdouble)rings_c/2.0;
        c2[1]+=1.0/den;
        c2[2]-=1.0/den;
        for(i=0;i<rings_c;i++)
          {
            for(j=0;j<(s1->points_per_ring);j++)
              {
                if((*(p1)).z+(*(p1+1)).z+(*(p1+offset1)).z+(*(p1+offset2)).z>0.0)
                  {
                    cairo_pattern_t *pattern1=cairo_pattern_create_mesh();
                    cairo_mesh_pattern_begin_patch(pattern1);
                    cairo_mesh_pattern_move_to(pattern1, w1*((*(p1)).x), h1*((*(p1)).y));
                    cairo_mesh_pattern_line_to(pattern1, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
                    cairo_mesh_pattern_line_to(pattern1, w1*((*(p1+offset1)).x), h1*((*(p1+offset1)).y));
                    cairo_mesh_pattern_line_to(pattern1, w1*((*(p1+offset2)).x), h1*((*(p1+offset2)).y));
                    cairo_mesh_pattern_line_to(pattern1, w1*((*(p1)).x), h1*((*(p1)).y));        
                    cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, c1[0], c1[1], c1[2], c1[3]);
                    cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, c1[0], c1[1], c1[2], c1[3]);
                    cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, c2[0], c2[1], c2[2], c2[3]);
                    cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, c2[0], c2[1], c2[2], c2[3]);
                    cairo_mesh_pattern_end_patch(pattern1);
                    cairo_set_source(cr, pattern1);
                    cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
                    cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
                    cairo_line_to(cr, w1*((*(p1+offset1)).x), h1*((*(p1+offset1)).y));
                    cairo_line_to(cr, w1*((*(p1+offset2)).x), h1*((*(p1+offset2)).y));
                    cairo_close_path(cr);
                    cairo_fill(cr);
                    cairo_pattern_destroy(pattern1);
                  }
                p1++;
              }
            if(i<(gint)(den-1.0))
              {
                c1[1]+=1.0/den;
                c1[2]-=1.0/den;
                c2[1]+=1.0/den;
                c2[2]-=1.0/den;
              }
            else if(i==(gint)(den-1))
              {
                c1[0]=0.0;c1[1]=1.0;c1[2]=0.0;
                c2[0]=1.0/den;c2[1]=1.0-1.0/den;c2[2]=0.0;
              }
            else
              {
                c1[0]+=1.0/den;
                c1[1]-=1.0/den;
                c2[0]+=1.0/den;
                c2[1]-=1.0/den;
              }
            if(c2[0]>1.0)
              {
                c2[0]=1.0;c2[1]=0.0;c2[2]=0.0;
              }
            p1++;
          }

        //Draw the bottom triangles yellow.
        p1=&g_array_index(s1->sr, struct point3d, ((s1->rings)-1)*((s1->points_per_ring)+1));
        cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
        for(i=0;i<(s1->points_per_ring);i++)
          {
            if(bottom.z+(*(p1)).z+(*(p1+1)).z>0.0)
              {
                cairo_move_to(cr, w1*bottom.x, h1*bottom.y);
                cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
                cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
                cairo_close_path(cr);
                cairo_fill(cr);
              }
            p1++;
          }
      }
  }
void draw_wire_ball(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;
    gint counter=1;
    gdouble c1[4]={1.0, 0.0, 1.0, 1.0};
    gdouble c2[4]={0.0, 0.0, 1.0, 1.0};

    const struct shape *s1=get_drawing_from_shape_cache(BALL, rings, points_per_ring);

    if(s1!=NULL)
      {
        gint array_len=s1->sr->len;
        gint rings_c=(s1->rings)-1;
        gint offset1=(s1->points_per_ring)+2;
        gint offset2=(s1->points_per_ring)+1;
        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
        struct point3d top=g_array_index(s1->sr, struct point3d, array_len-2);
        struct point3d bottom=g_array_index(s1->sr, struct point3d, array_len-1);
        
        cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
        //Draw the top triangles.
        for(i=0;i<(s1->points_per_ring);i++)
          {
            if(counter%2==0) cairo_set_source_rgba(cr, c1[0], c1[1], c1[2], c1[3]);
            else cairo_set_source_rgba(cr, c2[0], c2[1], c2[2], c2[3]);
            cairo_move_to(cr, w1*top.x, h1*top.y);
            cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            cairo_stroke(cr);
            counter++;
            p1++;
          }

        //Draw middle quads.
        p1=&g_array_index(s1->sr, struct point3d, 0);
        for(i=0;i<rings_c;i++)
          {
            for(j=0;j<(s1->points_per_ring);j++)
              {
                if(counter%2==0) cairo_set_source_rgba(cr, c1[0], c1[1], c1[2], c1[3]);
                else cairo_set_source_rgba(cr, c2[0], c2[1], c2[2], c2[3]);
                cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
                cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
                cairo_line_to(cr, w1*((*(p1+offset1)).x), h1*((*(p1+offset1)).y));
                cairo_line_to(cr, w1*((*(p1+offset2)).x), h1*((*(p1+offset2)).y));
                cairo_stroke(cr);
                counter++;
                p1++;
              }
            counter++;
            p1++;
          }

        //Draw the bottom triangles.
        p1=&g_array_index(s1->sr, struct point3d, ((s1->rings)-1)*((s1->points_per_ring)+1));
        for(i=0;i<(s1->points_per_ring);i++)
          {
            if(counter%2==0) cairo_set_source_rgba(cr, c1[0], c1[1], c1[2], c1[3]);
            else cairo_set_source_rgba(cr, c2[0], c2[1], c2[2], c2[3]);
            cairo_move_to(cr, w1*bottom.x, h1*bottom.y);
            cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));      
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            cairo_stroke(cr); 
            counter++;       
            p1++;
          }
      }
  }
void draw_wire_ball2(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    /*
       Reduce the number of lines drawn by drawing the rings of the circle instead of
       the patches. A little better performance.
    */
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(BALL, rings, points_per_ring);
   
    if(s1!=NULL)
      {
        gint array_len=s1->sr->len;
        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
        struct point3d top=g_array_index(s1->sr, struct point3d, array_len-2);
        struct point3d bottom=g_array_index(s1->sr, struct point3d, array_len-1);

        //Draw the latitude rings.
        cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
        for(i=0;i<(s1->rings);i++)
          {
            cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
            for(j=0;j<(s1->points_per_ring);j++)
              {
                cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
                p1++;
              }
             cairo_stroke(cr);
             p1++;  
           }  

        //Draw the longitude arcs from pole to pole.
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        gint offset1=(s1->points_per_ring)+1;
        gint offset2=0;
        p1=&g_array_index(s1->sr, struct point3d, 0);
        for(i=0;i<(s1->points_per_ring);i++)
          {
            cairo_move_to(cr, w1*top.x, h1*top.y);
            for(j=0;j<(s1->rings);j++)
              {
                offset2=offset1*(j)+i;
                cairo_line_to(cr, w1*((*(p1+offset2)).x), h1*((*(p1+offset2)).y));
              }
            cairo_line_to(cr, w1*bottom.x, h1*bottom.y);
            cairo_stroke(cr);  
          }
      }      
  }
void draw_wire_sphere(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(WIRE_SPHERE, rings, points_per_ring);
   
    if(s1!=NULL)
      {
        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);

        //Draw the longitude rings.
        for(i=0;i<rings;i++)
          {
            if(i==0) cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
            cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
            for(j=0;j<points_per_ring;j++)
              {
                cairo_curve_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y), w1*((*(p1+2)).x), h1*((*(p1+2)).y), w1*((*(p1+3)).x), h1*((*(p1+3)).y));
                 p1+=4;
              }
            cairo_stroke(cr);  
          }  

        //Draw latitude rings.
        gint len=points_per_ring/2-1;
        for(i=0;i<len;i++)
          {
            cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
            cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
            for(j=0;j<rings*2;j++)
              {
                cairo_curve_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y), w1*((*(p1+2)).x), h1*((*(p1+2)).y), w1*((*(p1+3)).x), h1*((*(p1+3)).y));
                 p1+=4;
              }
            cairo_stroke(cr);  
          } 
      }
  }
void draw_solid_sphere(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(SOLID_SPHERE, rings, points_per_ring);

    if(s1!=NULL)
      {
        struct bezier_quad *p1=&g_array_index(s1->sr, struct bezier_quad, 0);

        g_array_sort(s1->sr, compare_quads_bezier);

        cairo_save(cr);
        cairo_scale(cr, w1, h1);
        cairo_set_line_width(cr, 2.0/w1);
        for(i=0;i<s1->sr->len;i++)
          {
            cairo_move_to(cr, (p1->x1), (p1->y1));
            cairo_curve_to(cr, (p1->bx1), (p1->by1), (p1->bx2), (p1->by2), (p1->x2), (p1->y2));
            cairo_curve_to(cr, (p1->bx3), (p1->by3), (p1->bx4), (p1->by4), (p1->x3), (p1->y3));
            cairo_curve_to(cr, (p1->bx5), (p1->by5), (p1->bx6), (p1->by6), (p1->x4), (p1->y4));
            cairo_curve_to(cr, (p1->bx7), (p1->by7), (p1->bx8), (p1->by8), (p1->x1), (p1->y1));
            //Add an extra offset for each latitude ring to get the even and odd colors.
            j=(p1->index)/(rings*2); 
            if(((p1->index)+j)%2) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
            cairo_fill_preserve(cr);        
            cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
            cairo_stroke(cr);
            p1++;    
          }
        cairo_restore(cr);
     }
  }
void draw_solid_bezier_sphere_explode(cairo_t *cr, gint rings, gint points_per_ring)
  {
    gint i=0;
    //For the cross products and normal vectors.
    gdouble a1=0.0;
    gdouble a2=0.0;
    gdouble a3=0.0;
    gdouble b1=0.0;
    gdouble b2=0.0;
    gdouble b3=0.0;
    gdouble explode_x=0.0;
    gdouble explode_y=0.0;
    gdouble explode_z=0.0;
    gdouble mag=0.0;

    const struct shape *s1=get_drawing_from_shape_cache(SOLID_BEZIER_SPHERE, rings, points_per_ring);

    if(s1!=NULL)
      {
        struct bezier_quad *p1=&g_array_index(s1->sr, struct bezier_quad, 0);

        g_array_sort(s1->sr, compare_quads_bezier);

        //Draw each quad separately.
        for(i=0;i<s1->sr->len;i++)
          {
            //Compensation for the first set of triangles.
            if((p1->index)<rings*2)
              {
                a1=((p1->x1)-(p1->x2));
                a2=((p1->y1)-(p1->y2));
                a3=((p1->z1)-(p1->z2));
                b1=((p1->x1)-(p1->x3));
                b2=((p1->y1)-(p1->y3));
                b3=((p1->z1)-(p1->z3));
              }
            //Vectors for the rest of the quads.
            else
              {
                a1=((p1->x1)-(p1->x2));
                a2=((p1->y1)-(p1->y2));
                a3=((p1->z1)-(p1->z2));
                b1=((p1->x1)-(p1->x4));
                b2=((p1->y1)-(p1->y4));
                b3=((p1->z1)-(p1->z4));
              }
            //Normal vector.
            explode_x=(a2*b3-a3*b2);
            explode_y=-(a1*b3-a3*b1);
            explode_z=(a1*b2-a2*b1);
            mag=sqrt(explode_x*explode_x+explode_y*explode_y+explode_z*explode_z);
            cairo_save(cr);
            if(mag>0.0) cairo_translate(cr, 100.0*explode_x/mag, 100.0*explode_y/mag);
            cairo_move_to(cr, (p1->x1), (p1->y1));
            cairo_curve_to(cr, (p1->bx1), (p1->by1), (p1->bx2), (p1->by2), (p1->x2), (p1->y2));
            cairo_curve_to(cr, (p1->bx3), (p1->by3), (p1->bx4), (p1->by4), (p1->x3), (p1->y3));
            cairo_curve_to(cr, (p1->bx5), (p1->by5), (p1->bx6), (p1->by6), (p1->x4), (p1->y4));
            cairo_curve_to(cr, (p1->bx7), (p1->by7), (p1->bx8), (p1->by8), (p1->x1), (p1->y1));
            if((p1->index)%2) cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_fill(cr);
            cairo_restore(cr);
            p1++;    
          }
      }
  }
void draw_wrap_sphere(cairo_t *cr, gint rings, gint points_per_ring)
  {
    //The rotations need some work to get right.
    gint i=0;
    gdouble move=0;
    gdouble den=360.0;
    static gint inc=0;
    static gboolean up=TRUE;
    static gint stall=0;
    cairo_matrix_t matrix1;
    cairo_matrix_t matrix2;
    cairo_matrix_t matrix3;
    cairo_matrix_t matrix4;
    gdouble translate_x=0.0;
    gdouble translate_y=0.0;
    cairo_text_extents_t extents;
    cairo_set_font_size(cr, 14);
    gdouble a1=0.0;
    gdouble a2=0.0;
    gdouble a3=0.0;
    gdouble b1=0.0;
    gdouble b2=0.0;
    gdouble b3=0.0;
    gdouble c1=0.0;
    gdouble c2=0.0;
    gdouble nx=0.0;
    gdouble ny=0.0;
    gdouble nz=0.0;
    gdouble mag=0.0;
    gdouble tol=0.0000001;
    gdouble qrs2[9]={0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    gint step=rings*2;
    
    if(inc==0) up=TRUE;
    if(inc==360) up=FALSE;
    
    move=(gdouble)(inc)/den;

    const struct shape *s1=get_drawing_from_shape_cache(SOLID_BEZIER_SHEET, 5, 20);
    const struct shape *s2=get_drawing_from_shape_cache(SOLID_BEZIER_SPHERE, rings, points_per_ring);

    if(s1!=NULL&&s2!=NULL)
      {
        struct bezier_quad *p1=&g_array_index(s1->sr, struct bezier_quad, 0);
        struct bezier_quad *p2=&g_array_index(s2->sr, struct bezier_quad, 0);
        
        //Move the sheet quads to the sphere location.
        for(i=0;i<(s1->sr->len);i++)
          {
            //The sphere draws from the opposite end so match top of sheet with "bottom" of the sphere.
            p2=&g_array_index(s2->sr, struct bezier_quad, ((s2->sr->len-1)-(step)*((step+i)/(step))+1)+((step+i)%(step)));
            (p1->x1)-=((p1->x1)-(p2->x2))*move;
            (p1->y1)-=((p1->y1)-(p2->y2))*move;
            (p1->z1)-=((p1->z1)-(p2->z2))*move;
            (p1->bx1)-=((p1->bx1)-(p2->bx3))*move;
            (p1->by1)-=((p1->by1)-(p2->by3))*move;
            (p1->bz1)-=((p1->bz1)-(p2->bz3))*move;
            (p1->bx2)-=((p1->bx2)-(p2->bx4))*move;
            (p1->by2)-=((p1->by2)-(p2->by4))*move;
            (p1->bz2)-=((p1->bz2)-(p2->bz4))*move;
            
            (p1->x2)-=((p1->x2)-(p2->x3))*move;
            (p1->y2)-=((p1->y2)-(p2->y3))*move;
            (p1->z2)-=((p1->z2)-(p2->z3))*move;
            (p1->bx3)-=((p1->bx3)-(p2->bx5))*move;
            (p1->by3)-=((p1->by3)-(p2->by5))*move;
            (p1->bz3)-=((p1->bz3)-(p2->bz5))*move;
            (p1->bx4)-=((p1->bx4)-(p2->bx6))*move;
            (p1->by4)-=((p1->by4)-(p2->by6))*move;
            (p1->bz4)-=((p1->bz4)-(p2->bz6))*move;          
            
            (p1->x3)-=((p1->x3)-(p2->x4))*move;
            (p1->y3)-=((p1->y3)-(p2->y4))*move;
            (p1->z3)-=((p1->z3)-(p2->z4))*move;
            (p1->bx5)-=((p1->bx5)-(p2->bx7))*move;
            (p1->by5)-=((p1->by5)-(p2->by7))*move;
            (p1->bz5)-=((p1->bz5)-(p2->bz7))*move;
            (p1->bx6)-=((p1->bx6)-(p2->bx8))*move;
            (p1->by6)-=((p1->by6)-(p2->by8))*move;
            (p1->bz6)-=((p1->bz6)-(p2->bz8))*move;
                        
            (p1->x4)-=((p1->x4)-(p2->x1))*move;
            (p1->y4)-=((p1->y4)-(p2->y1))*move;
            (p1->z4)-=((p1->z4)-(p2->z1))*move;
            (p1->bx7)-=((p1->bx7)-(p2->bx1))*move;
            (p1->by7)-=((p1->by7)-(p2->by1))*move;
            (p1->bz7)-=((p1->bz7)-(p2->bz1))*move;
            (p1->bx8)-=((p1->bx8)-(p2->bx2))*move;
            (p1->by8)-=((p1->by8)-(p2->by2))*move;
            (p1->bz8)-=((p1->bz8)-(p2->bz2))*move;
            p1++;
          }
        
        //Increment for next time drawing. Stall on the beginning sheet and ending sphere.
        if(inc==0&&stall<100) stall++;
        else if(inc==360&&stall<100) stall++;
        else
          {
            stall=0;
            if(up==TRUE) inc++;
            else inc--;
          } 
        
        g_array_sort(s1->sr, compare_quads_bezier);
       
        p1=&g_array_index(s1->sr, struct bezier_quad, 0);
        for(i=0;i<(s1->sr->len);i++)
          {
            if((p1->index)<20) cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
            else if((p1->index)%5==0) cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0); 
            cairo_move_to(cr, (p1->x1), (p1->y1));
            cairo_curve_to(cr, (p1->bx1), (p1->by1), (p1->bx2), (p1->by2), (p1->x2), (p1->y2));
            cairo_curve_to(cr, (p1->bx3), (p1->by3), (p1->bx4), (p1->by4), (p1->x3), (p1->y3));               
            cairo_curve_to(cr, (p1->bx5), (p1->by5), (p1->bx6), (p1->by6), (p1->x4), (p1->y4));
            cairo_curve_to(cr, (p1->bx7), (p1->by7), (p1->bx8), (p1->by8), (p1->x1), (p1->y1));
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
                                          
            a1=((p1->x1)-(p1->x2));
            a2=((p1->y1)-(p1->y2));
            a3=((p1->z1)-(p1->z2));
            b1=((p1->x1)-(p1->x4));
            b2=((p1->y1)-(p1->y4));
            b3=((p1->z1)-(p1->z4));
            //Normal vector. 
            nx=(a2*b3-a3*b2);
            ny=-(a1*b3-a3*b1);
            nz=(a1*b2-a2*b1);
            mag=sqrt(nx*nx+ny*ny+nz*nz);
            if(mag>tol) nx=nx/mag;
            else nx=0.0;
            if(mag>tol) ny=ny/mag;
            else ny=0.0;
            if(mag>tol) nz=nz/mag;
            else nz=0.0;
              
            cairo_save(cr); 
            //Get the current transformation matrix for the current translation.
            cairo_get_matrix(cr, &matrix1);
            cairo_identity_matrix(cr);
            //Get the middle of the quad.
            translate_x=((p1->x1)+(p1->x2)+(p1->x3)+(p1->x4))/4.0;
            translate_y=((p1->y1)+(p1->y2)+(p1->y3)+(p1->y4))/4.0;
            cairo_matrix_init_translate(&matrix2, matrix1.x0, matrix1.y0); 
            cairo_transform(cr, &matrix2);
            cairo_matrix_init_scale(&matrix3, matrix1.xx, matrix1.yy);
            cairo_transform(cr, &matrix3);                
            //Use atan2 to get the rotation angles for the quad. For the top quads of the sphere use lower part of quad for the yaw.  
            if((p1->index)>step) quaternion_rotation(atan2(-a2, -a1), asin(nx), -atan2(ny, nz), qrs2); 
            else
              {
                c1=((p1->x4)-(p1->x3));
                c2=((p1->y4)-(p1->y3));
                quaternion_rotation(atan2(-c2, -c1), asin(nx), -atan2(ny, nz), qrs2); 
              } 
            cairo_matrix_init(&matrix4, qrs2[0], qrs2[3], qrs2[1], qrs2[4], translate_x, translate_y);             
            cairo_transform(cr, &matrix4);
                
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            gchar *number=g_strdup_printf("%i", (p1->index));
            cairo_text_extents(cr, number, &extents);
            cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
            cairo_show_text(cr, number);
            g_free(number);
            cairo_restore(cr);
            
            p1++;    
          }         
      }     
  }
void draw_wrap_sphere_image(cairo_t *cr, gint rings, gint points_per_ring, GPtrArray *pictures)
  {
    gint i=0;
    gdouble move=0;
    gdouble den=360.0;
    static gint inc=0;
    static gboolean up=TRUE;
    static gint stall=0;
    cairo_matrix_t matrix1;
    cairo_matrix_t matrix2;
    cairo_matrix_t matrix3;
    gdouble translate_x=0.0;
    gdouble translate_y=0.0;
    gdouble a1=0.0;
    gdouble a2=0.0;
    gdouble a3=0.0;
    gdouble b1=0.0;
    gdouble b2=0.0;
    gdouble b3=0.0;
    gdouble c1=0.0;
    gdouble c2=0.0;
    gdouble nx=0.0;
    gdouble ny=0.0;
    gdouble nz=0.0;
    gdouble nz2=0.0;
    gdouble mag=0.0;
    gdouble tol=0.0000001;
    gdouble qrs2[9]={0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    gint step=rings*2;
    gdouble yaw=0.0;
    gdouble roll=0.0;
    gdouble pitch=0.0;
    
    if(inc==0) up=TRUE;
    if(inc==360) up=FALSE;
    
    move=(gdouble)(inc)/den;

    const struct shape *s1=get_drawing_from_shape_cache(SOLID_BEZIER_SHEET, 5, 20);
    const struct shape *s2=get_drawing_from_shape_cache(SOLID_BEZIER_SPHERE, rings, points_per_ring);

    if(s1!=NULL&&s2!=NULL)
      {
        struct bezier_quad *p1=&g_array_index(s1->sr, struct bezier_quad, 0);
        struct bezier_quad *p2=&g_array_index(s2->sr, struct bezier_quad, 0);
        
        //Move the sheet quads to the sphere location.
        for(i=0;i<(s1->sr->len);i++)
          {
            //The sphere draws from the opposite end so match top of sheet with "bottom" of the sphere.
            p2=&g_array_index(s2->sr, struct bezier_quad, ((s2->sr->len-1)-(step)*((step+i)/(step))+1)+((step+i)%(step)));
            (p1->x1)-=((p1->x1)-(p2->x2))*move;
            (p1->y1)-=((p1->y1)-(p2->y2))*move;
            (p1->z1)-=((p1->z1)-(p2->z2))*move;
            (p1->bx1)-=((p1->bx1)-(p2->bx3))*move;
            (p1->by1)-=((p1->by1)-(p2->by3))*move;
            (p1->bz1)-=((p1->bz1)-(p2->bz3))*move;
            (p1->bx2)-=((p1->bx2)-(p2->bx4))*move;
            (p1->by2)-=((p1->by2)-(p2->by4))*move;
            (p1->bz2)-=((p1->bz2)-(p2->bz4))*move;
            
            (p1->x2)-=((p1->x2)-(p2->x3))*move;
            (p1->y2)-=((p1->y2)-(p2->y3))*move;
            (p1->z2)-=((p1->z2)-(p2->z3))*move;
            (p1->bx3)-=((p1->bx3)-(p2->bx5))*move;
            (p1->by3)-=((p1->by3)-(p2->by5))*move;
            (p1->bz3)-=((p1->bz3)-(p2->bz5))*move;
            (p1->bx4)-=((p1->bx4)-(p2->bx6))*move;
            (p1->by4)-=((p1->by4)-(p2->by6))*move;
            (p1->bz4)-=((p1->bz4)-(p2->bz6))*move;          
            
            (p1->x3)-=((p1->x3)-(p2->x4))*move;
            (p1->y3)-=((p1->y3)-(p2->y4))*move;
            (p1->z3)-=((p1->z3)-(p2->z4))*move;
            (p1->bx5)-=((p1->bx5)-(p2->bx7))*move;
            (p1->by5)-=((p1->by5)-(p2->by7))*move;
            (p1->bz5)-=((p1->bz5)-(p2->bz7))*move;
            (p1->bx6)-=((p1->bx6)-(p2->bx8))*move;
            (p1->by6)-=((p1->by6)-(p2->by8))*move;
            (p1->bz6)-=((p1->bz6)-(p2->bz8))*move;
                        
            (p1->x4)-=((p1->x4)-(p2->x1))*move;
            (p1->y4)-=((p1->y4)-(p2->y1))*move;
            (p1->z4)-=((p1->z4)-(p2->z1))*move;
            (p1->bx7)-=((p1->bx7)-(p2->bx1))*move;
            (p1->by7)-=((p1->by7)-(p2->by1))*move;
            (p1->bz7)-=((p1->bz7)-(p2->bz1))*move;
            (p1->bx8)-=((p1->bx8)-(p2->bx2))*move;
            (p1->by8)-=((p1->by8)-(p2->by2))*move;
            (p1->bz8)-=((p1->bz8)-(p2->bz2))*move;
            p1++;
          }
        
        //Increment for next time drawing. Stall on the beginning sheet and ending sphere.
        if(inc==0&&stall<100) stall++;
        else if(inc==360&&stall<100) stall++;
        else
          {
            stall=0;
            if(up==TRUE) inc++;
            else inc--;
          } 
        
        g_array_sort(s1->sr, compare_quads_bezier);
       
        p1=&g_array_index(s1->sr, struct bezier_quad, 0);
        for(i=0;i<(s1->sr->len);i++)
          {  
            //Account for the top and bottom triangles in the sphere.
            if(p1->index>=80) 
              { 
                a1=((p1->x1)-(p1->x2));
                a2=((p1->y1)-(p1->y2));
                a3=((p1->z1)-(p1->z2));
                b1=((p1->x1)-(p1->x4));
                b2=((p1->y1)-(p1->y4));
                b3=((p1->z1)-(p1->z4));
              }
            else
              {
                a1=((p1->x3)-(p1->x4));
                a2=((p1->y3)-(p1->y4));
                a3=((p1->z3)-(p1->z4));
                b1=((p1->x3)-(p1->x2));
                b2=((p1->y3)-(p1->y2));
                b3=((p1->z3)-(p1->z2));
              }
            //Normal vector. 
            nx=(a2*b3-a3*b2);
            ny=-(a1*b3-a3*b1);
            nz=(a1*b2-a2*b1);
            mag=sqrt(nx*nx+ny*ny+nz*nz);
            if(mag>tol) nx=nx/mag;
            else nx=0.0;
            if(mag>tol) ny=ny/mag;
            else ny=0.0;
            //Make z always positive since the pattern can only be drawn with positive z.
            if(mag>tol) nz=fabs(nz/mag);
            else nz=0.0;    
              
            //Calculate yaw based on a line threw the center from side to side.
            c1=(((p1->x2)-(p1->x1))+((p1->x3)-(p1->x4)))/4.0;
            c2=(((p1->y2)-(p1->y1))+((p1->y3)-(p1->y4)))/4.0;
            
            //Translate from center.
            translate_x=((p1->x1)+(p1->x2)+(p1->x3)+(p1->x4))/4.0;
            translate_y=((p1->y1)+(p1->y2)+(p1->y3)+(p1->y4))/4.0;
            
            //Flip signs when z is negative.
            if(nz2<0.0) nx=-nx;  
            if(nz2<0.0) ny=-ny;             
            //Use atan2 to get the rotation angles for the quad.  
            yaw=-atan2(c2, c1);
            roll=-asin(nx);
            pitch=-atan2(ny, nz);
            quaternion_rotation(yaw, roll, pitch, qrs2);
            //Rotate and translate the pattern quad.
            cairo_matrix_init(&matrix1, qrs2[0], qrs2[3], qrs2[1], qrs2[4], 12.5, 20.0);                                 
            cairo_matrix_init_translate(&matrix2, -translate_x, -translate_y); 
            //Pattern matrix is inverse. See cairo_pattern_t documentation.
            cairo_matrix_multiply(&matrix2, &matrix2, &matrix1); 
            matrix3=matrix2;          
            
            /*
               A problem with cairo returning error no memory when the pattern is facing -z. Patterns only rotate 180 degrees.
               //https://gitlab.freedesktop.org/cairo/cairo/-/issues/238  
            */
            cairo_surface_t *surface1=NULL;
            cairo_t *cr1=NULL;
            cairo_pattern_t *pattern=NULL;
            if(cairo_matrix_invert(&matrix3)==CAIRO_STATUS_SUCCESS&&nz>0.0001)
              {              
                surface1=cairo_image_surface_create(CAIRO_FORMAT_ARGB32, 50.0, 80.0);
                cr1=cairo_create(surface1);
                cairo_identity_matrix(cr1);
                cairo_set_source(cr1, (cairo_pattern_t*)g_ptr_array_index(pictures, p1->index)); 
                cairo_rectangle(cr1, 0.0, 0.0, 50.0, 80.0);          
                cairo_fill(cr1); 
                pattern=cairo_pattern_create_for_surface(surface1);
                cairo_pattern_set_extend(pattern, CAIRO_EXTEND_PAD);
                cairo_pattern_set_filter(pattern, CAIRO_FILTER_NEAREST);
                cairo_pattern_set_matrix(pattern, &matrix2);
                //if(cairo_pattern_status(pattern)!=0) g_print("pattern: %s\n", cairo_status_to_string(cairo_pattern_status(pattern)));
              }
            //else g_print("CAIRO_STATUS_INVALID_MATRIX\n"); 
                   
            cairo_move_to(cr, (p1->x1), (p1->y1));
            cairo_curve_to(cr, (p1->bx1), (p1->by1), (p1->bx2), (p1->by2), (p1->x2), (p1->y2));
            cairo_curve_to(cr, (p1->bx3), (p1->by3), (p1->bx4), (p1->by4), (p1->x3), (p1->y3));               
            cairo_curve_to(cr, (p1->bx5), (p1->by5), (p1->bx6), (p1->by6), (p1->x4), (p1->y4));
            cairo_curve_to(cr, (p1->bx7), (p1->by7), (p1->bx8), (p1->by8), (p1->x1), (p1->y1));
            cairo_close_path(cr);
            if(pattern!=NULL) cairo_set_source(cr, pattern);
            if(pattern!=NULL) cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr); 
             
            if(pattern!=NULL) cairo_pattern_destroy(pattern);
            if(cr1!=NULL) cairo_destroy(cr1);
            if(surface1!=NULL) cairo_surface_destroy(surface1);
                       
            p1++;    
          }         
      }     
  }
void draw_wire_funnel(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    //Draw some blue rings and green rays for wire funnel.
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(WIRE_FUNNEL, rings, points_per_ring);
  
    if(s1!=NULL)
      {
        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
        struct point3d *p2=&g_array_index(s1->sr, struct point3d, points_per_ring*4);

        //The blue rings on the funnel.
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0); 
        for(i=0;i<(s1->rings);i++)
          { 
            for(j=0;j<(s1->points_per_ring);j++)
              {   
                cairo_move_to(cr, w1*(p1->x), h1*(p1->y));
                cairo_curve_to(cr, w1*((p1+1)->x), h1*((p1+1)->y), w1*((p1+2)->x), h1*((p1+2)->y), w1*((p1+3)->x), h1*((p1+3)->y));
                p1+=4;        
              }
            cairo_stroke(cr);  
          }

       
        //The green lines into the funnel.
        cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0); 
        p1=&g_array_index(s1->sr, struct point3d, 0);
        for(i=0;i<(s1->points_per_ring);i++)
          {  
            cairo_move_to(cr, w1*(p1->x), h1*(p1->y));     
            for(j=1;j<(s1->rings);j++)
              {   
                cairo_line_to(cr, w1*(p2->x), h1*(p2->y));
                p2+=(s1->points_per_ring)*4;      
              } 
            cairo_stroke(cr);
            p1+=4;
            p2=p1+(s1->points_per_ring)*4;
          }
      }    
  }
void draw_solid_funnel(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;
    gdouble c1[4]={0.0, 0.0, 1.0, 1.0};
    gdouble c2[4]={1.0, 1.0, 0.0, 1.0};
    gdouble c3[4]={0.0, 0.0, 1.0, 1.0};
    gdouble c4[4]={0.0, 0.0, 1.0, 1.0};
    gdouble color1=0.0;
    gdouble color2=0.0;

    const struct shape *s1=get_drawing_from_shape_cache(SOLID_FUNNEL, rings, points_per_ring);
    
    if(s1!=NULL)
      {
        struct bezier_quad *p1=&g_array_index(s1->sr, struct bezier_quad, 0);

        g_array_sort(s1->sr, compare_quads_bezier);

        cairo_save(cr);
        cairo_scale(cr, w1, h1);
        for(i=0;i<((s1->rings)-1);i++)
          {
            for(j=0;j<(s1->points_per_ring);j++)
              {            
                cairo_pattern_t *pattern1=cairo_pattern_create_mesh();
                cairo_mesh_pattern_begin_patch(pattern1);
                cairo_mesh_pattern_move_to(pattern1, p1->x1, p1->y1);
                cairo_mesh_pattern_curve_to(pattern1, p1->bx1, p1->by1, p1->bx2, p1->by2, p1->x2, p1->y2);
                cairo_mesh_pattern_curve_to(pattern1, p1->bx3, p1->by3, p1->bx4, p1->by4, p1->x3, p1->y3);
                cairo_mesh_pattern_curve_to(pattern1, p1->bx5, p1->by5, p1->bx6, p1->by6, p1->x4, p1->y4);
                cairo_mesh_pattern_curve_to(pattern1, p1->bx7, p1->by7, p1->bx8, p1->by8, p1->x1, p1->y1);
                //Color fade.
                color1=(gdouble)(p1->index/points_per_ring)/(gdouble)(rings-1); 
                color2=(gdouble)((p1->index+points_per_ring)/points_per_ring)/(gdouble)(rings-1);  
                c1[1]=color1;
                c3[1]=color2;
                c4[1]=color2;     
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, c1[0], c1[1], c1[2], c1[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, c2[0], c2[1], c2[2], c2[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, c3[0], c3[1], c3[2], c3[3]);
                cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, c4[0], c4[1], c4[2], c4[3]);
                cairo_mesh_pattern_end_patch(pattern1);
                cairo_set_source(cr, pattern1);            
                cairo_move_to(cr, p1->x1, p1->y1);
                cairo_curve_to(cr, p1->bx1, p1->by1, p1->bx2, p1->by2, p1->x2, p1->y2);
                cairo_curve_to(cr, p1->bx3, p1->by3, p1->bx4, p1->by4, p1->x3, p1->y3);
                cairo_curve_to(cr, p1->bx5, p1->by5, p1->bx6, p1->by6, p1->x4, p1->y4);
                cairo_curve_to(cr, p1->bx7, p1->by7, p1->bx8, p1->by8, p1->x1, p1->y1);
                cairo_fill(cr);
                cairo_pattern_destroy(pattern1);
                p1++;
              }
          } 
        cairo_restore(cr);
     }   
  }
void draw_gem(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;

    const struct shape *s1=get_drawing_from_shape_cache(GEM, 0, 0);
   
    if(s1!=NULL)
      {
        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
        struct point3d *p2=&g_array_index(s1->sr, struct point3d, 0);

        //Draw the top polygon of the gem.
        cairo_pattern_t *radial1=cairo_pattern_create_radial(0.0, 0.0, 0.5*w1, 0.0, 0.0, 2.0*w1);  
        cairo_pattern_add_color_stop_rgba(radial1, 0.0, 1.0, 0.0, 1.0, 0.3);
        cairo_pattern_add_color_stop_rgba(radial1, 1.0, 0.0, 0.0, 1.0, 0.5);
        cairo_set_source(cr, radial1);
        cairo_move_to(cr, w1*(*p1).x, h1*(*p1).y);  
        for(i=0;i<12;i++)
          { 
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            cairo_stroke_preserve(cr);
            p1++;        
          }
        cairo_close_path(cr);
        cairo_fill(cr);
        cairo_pattern_destroy(radial1);  

        //Draw quad ring.
        p1=&g_array_index(s1->sr, struct point3d, 0);
        p2=&g_array_index(s1->sr, struct point3d, 13); 
        for(i=0;i<12;i++)
          { 
            cairo_pattern_t *pattern1=cairo_pattern_create_mesh();
            cairo_mesh_pattern_begin_patch(pattern1);
            cairo_mesh_pattern_move_to(pattern1, w1*(*p1).x, h1*(*p1).y);
            cairo_mesh_pattern_line_to(pattern1, w1*((*(p2)).x), h1*((*(p2)).y));
            cairo_mesh_pattern_line_to(pattern1, w1*((*(p2+1)).x), h1*((*(p2+1)).y));
            cairo_mesh_pattern_line_to(pattern1, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, 0.0, 0.0, 1.0, 0.4);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, 0.0, 0.0, 1.0, 0.4);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, 1.0, 0.0, 1.0, 0.4);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, 1.0, 0.0, 1.0, 0.4);
            cairo_mesh_pattern_end_patch(pattern1);
            cairo_set_source(cr, pattern1);
            cairo_paint(cr);   
            cairo_pattern_destroy(pattern1);

            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0); 
            cairo_move_to(cr, w1*(*p1).x, h1*(*p1).y); 
            cairo_line_to(cr, w1*((*(p2)).x), h1*((*(p2)).y));
            cairo_line_to(cr, w1*((*(p2+1)).x), h1*((*(p2+1)).y));
            cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
            cairo_close_path(cr);
            cairo_stroke(cr);
            p1++;p2++;        
          }  

        //Bottom triangles.
        p1=&g_array_index(s1->sr, struct point3d, 26);
        p2=&g_array_index(s1->sr, struct point3d, 13);  
        for(i=0;i<12;i++)
          { 
            cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 0.2);
            cairo_move_to(cr, w1*(*p2).x, h1*(*p2).y); 
            cairo_line_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));
            cairo_line_to(cr, w1*((*(p2+1)).x), h1*((*(p2+1)).y));
            cairo_close_path(cr);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 0.3);
            cairo_stroke(cr);
            p2++;        
          } 
      }    
  }
void draw_wire_torus(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(WIRE_TORUS, rings, points_per_ring);

    if(s1!=NULL)
      {
        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
        
        //Draw the cross section rings.
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        for(i=0;i<(s1->rings);i++)
          {
            for(j=0;j<(s1->points_per_ring);j++)
              {
                cairo_move_to(cr, w1*((*p1).x), h1*((*p1).y));
                cairo_curve_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y), w1*((*(p1+2)).x), h1*((*(p1+2)).y), w1*((*(p1+3)).x), h1*((*(p1+3)).y));
                p1+=4;        
              }
             cairo_stroke(cr);
           }

        //Draw the outside rings.
        p1=&g_array_index(s1->sr, struct point3d, 4*(s1->rings+1)*(s1->points_per_ring));
        cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
        for(i=0;i<(s1->points_per_ring);i++)
          {
            for(j=0;j<(s1->rings);j++)
              {
                cairo_move_to(cr, w1*((*p1).x), h1*((*p1).y));
                cairo_curve_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y), w1*((*(p1+2)).x), h1*((*(p1+2)).y), w1*((*(p1+3)).x), h1*((*(p1+3)).y));
                p1+=4;        
              }
             cairo_stroke(cr);
           }
       }
  }
void draw_solid_torus(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    gint i=0;

    const struct shape *s1=get_drawing_from_shape_cache(SOLID_TORUS, rings, points_per_ring);

    if(s1!=NULL)
      {
        struct bezier_quad *p1=&g_array_index(s1->sr, struct bezier_quad, 0);

        g_array_sort(s1->sr, compare_quads_bezier);

        cairo_save(cr);
        cairo_scale(cr, w1, h1);
        for(i=0;i<s1->sr->len;i++)
          {
            cairo_move_to(cr, (p1->x1), (p1->y1));
            cairo_curve_to(cr, (p1->bx1), (p1->by1), (p1->bx2), (p1->by2), (p1->x2), (p1->y2));
            cairo_curve_to(cr, (p1->bx3), (p1->by3), (p1->bx4), (p1->by4), (p1->x3), (p1->y3));
            cairo_curve_to(cr, (p1->bx5), (p1->by5), (p1->bx6), (p1->by6), (p1->x4), (p1->y4));
            cairo_curve_to(cr, (p1->bx7), (p1->by7), (p1->bx8), (p1->by8), (p1->x1), (p1->y1));
            if(p1->index%2) cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
            cairo_fill(cr);
            p1++;    
          }
        cairo_restore(cr);
     }
  }
void draw_wire_twist(cairo_t *cr, gdouble w1, gdouble h1, gint strands)
  {
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(WIRE_TWIST, 0, strands);

    if(s1!=NULL)
      {
        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);

        GArray *strand=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), strands);
        g_array_set_size(strand, strands);
        struct point3d *p2=&g_array_index(strand, struct point3d, 0);

        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        for(i=0;i<strands;i++)
          {
            //get a strand.
            p1=&g_array_index(s1->sr, struct point3d, strands*i);
            p2=&g_array_index(strand, struct point3d, 0);
            for(j=0;j<strands;j++)
              {  
                (p2->x)=(p1->x);
                (p2->y)=(p1->y);
                (p2->z)=(p1->z);          
                p1++;p2++;
              }
            //Smooth the strand points and draw.
            p2=&g_array_index(strand, struct point3d, 0);
            GArray *controls=control_points_from_coords_2d(strand);
            struct controls2d *c1=&g_array_index(controls, struct controls2d, 0);
            cairo_move_to(cr, w1*((*(p2)).x), h1*((*(p2)).y));
            p2++;
            for(j=0;j<(strands-1);j++)
              {            
                cairo_curve_to(cr,  w1*(c1->x1), h1*(c1->y1),  w1*(c1->x2), h1*(c1->y2), w1*((*(p2)).x), h1*((*(p2)).y));
                p2++;c1++;
              }
            cairo_stroke(cr);
            g_array_free(controls, TRUE);
          }
        g_array_free(strand, TRUE);
     }
  }
void draw_solid_twist(cairo_t *cr, gdouble w1, gdouble h1, gint strands)
  {
    //Draw the twist with sorting the quads.
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(SOLID_TWIST, 0, strands); 
    
    if(s1!=NULL)
      {
        struct bezier_quad *p1=&g_array_index(s1->sr, struct bezier_quad, 0);

        g_array_sort(s1->sr, compare_quads_bezier);
        
        cairo_save(cr);
        cairo_scale(cr, w1, h1);
        for(i=0;i<(s1->sr->len);i++)
          {
            cairo_move_to(cr, p1->x1, p1->y1);
            cairo_curve_to(cr, p1->bx1, p1->by1, p1->bx2, p1->by2, p1->x2, p1->y2);
            cairo_line_to(cr, p1->x3, p1->y3);
            cairo_curve_to(cr, p1->bx5, p1->by5, p1->bx6, p1->by6, p1->x4, p1->y4);
            cairo_close_path(cr);
            //Color lengthwise.
            j=(p1->index)/(strands-1); 
            if(j%2) cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            else cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);            
            cairo_fill(cr);
            p1++;    
          }
        cairo_restore(cr);
     }
  }
void draw_pyramid(cairo_t *cr, gdouble w1, gdouble h1)
  {  
    gint i=0;
    gdouble alpha=1.0;

    const struct shape *s1=get_drawing_from_shape_cache(PYRAMID, 0, 0);
    
    if(s1!=NULL)
      {
        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
        struct plane_order po[4];
        po[0].id=0;
        po[0].value=2.0*((*(p1)).z)+((*(p1+1)).z)+((*(p1+2)).z);
        po[1].id=1;
        po[1].value=2.0*((*(p1)).z)+((*(p1+2)).z)+((*(p1+3)).z);
        po[2].id=2;
        po[2].value=2.0*((*(p1)).z)+((*(p1+3)).z)+((*(p1+4)).z);
        po[3].id=3;
        po[3].value=2.0*((*(p1)).z)+((*(p1+4)).z)+((*(p1+1)).z);

        qsort(po, 4, sizeof(struct plane_order), compare_planes);

        //Draw four triangle faces in z-order.
        for(i=0;i<4;i++)
          {
            if(po[i].id==0)
              {
                cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, alpha);
                cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));         
                cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
                cairo_line_to(cr, w1*((*(p1+2)).x), h1*((*(p1+2)).y));
                cairo_close_path(cr);
                cairo_fill(cr);
              }
            else if(po[i].id==1)
              {
                cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, alpha);
                cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));         
                cairo_line_to(cr, w1*((*(p1+2)).x), h1*((*(p1+2)).y));
                cairo_line_to(cr, w1*((*(p1+3)).x), h1*((*(p1+3)).y));
                cairo_close_path(cr);
                cairo_fill(cr);
              }   
            else if(po[i].id==2)
              {
                cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, alpha);
                cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));         
                cairo_line_to(cr, w1*((*(p1+3)).x), h1*((*(p1+3)).y));
                cairo_line_to(cr, w1*((*(p1+4)).x), h1*((*(p1+4)).y));
                cairo_close_path(cr);
                cairo_fill(cr);
              }    
            else
              {
                cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, alpha);
                cairo_move_to(cr, w1*((*(p1)).x), h1*((*(p1)).y));         
                cairo_line_to(cr, w1*((*(p1+4)).x), h1*((*(p1+4)).y));
                cairo_line_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));
                cairo_close_path(cr);
                cairo_fill(cr);
              }
          }

        //Base square.
        if(((*(p1+1)).z)+((*(p1+2)).z)+((*(p1+3)).z)+((*(p1+4)).z)>0.0)
          {
            cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, alpha);
            cairo_move_to(cr, w1*((*(p1+1)).x), h1*((*(p1+1)).y));         
            cairo_line_to(cr, w1*((*(p1+2)).x), h1*((*(p1+2)).y));
            cairo_line_to(cr, w1*((*(p1+3)).x), h1*((*(p1+3)).y));
            cairo_line_to(cr, w1*((*(p1+4)).x), h1*((*(p1+4)).y));
            cairo_close_path(cr);
            cairo_fill(cr);
          }
      }                        
  }
void draw_cone(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(CONE, rings, points_per_ring);

    if(s1!=NULL)
      {
        g_array_sort(s1->sr, compare_quads2);
        struct quad_plane2 *p1=&g_array_index(s1->sr, struct quad_plane2, 0);

        cairo_save(cr);
        cairo_scale(cr, w1, h1);
        for(i=0;i<(s1->rings);i++)
          {
            for(j=0;j<(s1->points_per_ring);j++)
              {
                cairo_move_to(cr, p1->x1, p1->y1);
                cairo_line_to(cr, p1->x2, p1->y2);
                cairo_line_to(cr, p1->x3, p1->y3);
                cairo_line_to(cr, p1->x4, p1->y4);
                cairo_close_path(cr);
                if(p1->index%2) cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
                else cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
                cairo_fill(cr);
                p1++;
              }
          }
         cairo_restore(cr); 
     }  
  }
void draw_cone_shade(cairo_t *cr, gdouble w1, gdouble h1, gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;
    gdouble c1[4]={0.0, 0.0, 1.0, 1.0};
    gdouble distance=0.0;

    const struct shape *s1=get_drawing_from_shape_cache(CONE, rings, points_per_ring);
    
    if(s1!=NULL)
      {
        g_array_sort(s1->sr, compare_quads2);
        struct quad_plane2 *p1=&g_array_index(s1->sr, struct quad_plane2, 0);

        cairo_save(cr);
        cairo_scale(cr, w1, h1);
        for(i=0;i<(s1->rings);i++)
          {
            for(j=0;j<(s1->points_per_ring);j++)
              {
                /*
                  Standardize numbers for shading and draw. Change around for 
                  different effects. https://en.wikipedia.org/wiki/Shading
                */
                distance=(((s1->z_max)-(p1->z1))/((s1->z_max)-(s1->z_min)))*2.5;
                distance=1.0/(1.0+(distance*distance));
                cairo_set_source_rgba(cr, c1[0], distance, c1[2], c1[3]);
                cairo_move_to(cr, p1->x1, p1->y1);
                cairo_line_to(cr, p1->x2, p1->y2);
                cairo_line_to(cr, p1->x3, p1->y3);
                cairo_line_to(cr, p1->x4, p1->y4);
                cairo_close_path(cr);
                cairo_fill(cr);
                p1++;
              }
          }
        cairo_restore(cr);
     }   
  }
void draw_text_ring(cairo_t *cr, gdouble w1, gdouble h1, gint points_per_ring)
  {
    gint i=0; 
    cairo_matrix_t matrix1;
    cairo_text_extents_t extents;
    cairo_set_font_size(cr, 20.0*w1/100.0);
    gdouble qrs2[9];
    gdouble qrs3[9];
    gdouble translate_x=0.0;
    gdouble translate_y=0.0;
    gdouble advance=0.0;

    const struct shape *s1=get_drawing_from_shape_cache(TEXT_RING, 0, points_per_ring); 

    if(s1!=NULL)
      {
        g_array_sort(s1->sr, compare_quads2);
        struct quad_plane2 *p1=&g_array_index(s1->sr, struct quad_plane2, 0);
        
        cairo_set_line_width(cr, 2.0/w1);
        for(i=0;i<s1->sr->len;i++)
          {
            cairo_save(cr);
            cairo_scale(cr, w1, h1);  
            cairo_move_to(cr, p1->x1, p1->y1);        
            cairo_line_to(cr, p1->x2, p1->y2);
            cairo_line_to(cr, p1->x3, p1->y3);
            cairo_line_to(cr, p1->x4, p1->y4);
            cairo_close_path(cr);
            if(p1->index%2) cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr); 
            cairo_restore(cr);

            cairo_save(cr);
            advance=(((gdouble)(p1->index)+0.5)/points_per_ring)*(2.0*G_PI)-(G_PI/2.0);
            quaternion_rotation(advance, 0.0, 3.0*G_PI/2.0, qrs2);
            //Draw numbers aligned vertically with the ring.
            matrix_multiply_rotations(qrs2, rm, qrs3);
            translate_x=w1*((p1->x1)+(p1->x2)+(p1->x3)+(p1->x4))/4.0;
            translate_y=h1*((p1->y1)+(p1->y2)+(p1->y3)+(p1->y4))/4.0;
            cairo_matrix_init(&matrix1, qrs3[0], qrs3[3], qrs3[1], qrs3[4], translate_x, translate_y);
            cairo_transform(cr, &matrix1); 

            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            //Draw numbers clockwise or counter clockwise around the ring.
            gchar *number=g_strdup_printf("%i", points_per_ring-(p1->index));
            cairo_text_extents(cr, number, &extents);
            cairo_move_to(cr, -extents.width/2.0, extents.height/2.0);
            cairo_show_text(cr, number);
            g_free(number);
            cairo_restore(cr);

            p1++;      
          } 
      }
  }
void draw_wire_bezier_disc(cairo_t *cr, gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(WIRE_BEZIER_DISC, rings, points_per_ring);
    
    if(s1!=NULL)
      {
        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);

        //Draw ray lines.
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        for(i=0;i<points_per_ring;i++)
          {
            cairo_move_to(cr, ((*p1).x), ((*p1).y));  
            for(j=0;j<rings;j++)
              {
                cairo_curve_to(cr, ((*(p1+1)).x), ((*(p1+1)).y), ((*(p1+2)).x), ((*(p1+2)).y), ((*(p1+3)).x), ((*(p1+3)).y));
                p1+=3;
              }
            cairo_stroke(cr);
            p1+=3;
          } 

        //Draw circles
        cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
        for(i=0;i<rings;i++)
          {
            cairo_move_to(cr, ((*p1).x), ((*p1).y)); 
            for(j=0;j<points_per_ring;j++)
              {
                cairo_curve_to(cr, ((*(p1+1)).x), ((*(p1+1)).y), ((*(p1+2)).x), ((*(p1+2)).y), ((*(p1+3)).x), ((*(p1+3)).y));
                p1+=3;
              }
            cairo_stroke(cr);
            p1+=3;
          } 
      }
  }
void draw_solid_bezier_disc(cairo_t *cr, gint rings, gint points_per_ring)
  {
    gint i=0;

    const struct shape *s1=get_drawing_from_shape_cache(SOLID_BEZIER_DISC, rings, points_per_ring);

    if(s1!=NULL)
      {
        struct bezier_quad *p1=&g_array_index(s1->sr, struct bezier_quad, 0);

        g_array_sort(s1->sr, compare_quads_bezier);

        //Draw each quad separately.
        cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
        for(i=0;i<(s1->sr->len);i++)
          {
            if((p1->index)%2) cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
            cairo_move_to(cr, (p1->x1), (p1->y1));
            cairo_curve_to(cr, (p1->bx1), (p1->by1), (p1->bx2), (p1->by2), (p1->x2), (p1->y2));
            cairo_curve_to(cr, (p1->bx3), (p1->by3), (p1->bx4), (p1->by4), (p1->x3), (p1->y3));
            cairo_curve_to(cr, (p1->bx5), (p1->by5), (p1->bx6), (p1->by6), (p1->x4), (p1->y4));
            cairo_curve_to(cr, (p1->bx7), (p1->by7), (p1->bx8), (p1->by8), (p1->x1), (p1->y1));
            cairo_fill(cr);
           
            //draw the lines around the quad.
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_move_to(cr, (p1->x1), (p1->y1));
            cairo_curve_to(cr, (p1->bx1), (p1->by1), (p1->bx2), (p1->by2), (p1->x2), (p1->y2));
            cairo_curve_to(cr, (p1->bx3), (p1->by3), (p1->bx4), (p1->by4), (p1->x3), (p1->y3));       
            cairo_curve_to(cr, (p1->bx5), (p1->by5), (p1->bx6), (p1->by6), (p1->x4), (p1->y4));
            cairo_curve_to(cr, (p1->bx7), (p1->by7), (p1->bx8), (p1->by8), (p1->x1), (p1->y1));
            cairo_close_path(cr);
            cairo_stroke(cr);

            p1++;    
          }
      }
  }
void draw_solid_bezier_disc_mesh(cairo_t *cr, gint rings, gint points_per_ring)
  {
    //Compare a circular cosine function on a disc and sheet.
    gint i=0;
    gint idx=0;
    gdouble r;
    gdouble g;
    gdouble b;

    const struct shape *s1=get_drawing_from_shape_cache(SOLID_BEZIER_DISC, rings, points_per_ring);

    if(s1!=NULL)
      {
        g_array_sort(s1->sr, compare_quads_bezier);

        struct bezier_quad *p1=&g_array_index(s1->sr, struct bezier_quad, 0);
        struct bezier_quad *p2=NULL;

        //Draw each quad separately.
        for(i=0;i<(s1->sr->len);i++)
          {
            cairo_pattern_t *pattern=cairo_pattern_create_mesh();
       
            idx=(p1->index);
            p2=&g_array_index(s1->s, struct bezier_quad, idx);
            cairo_mesh_pattern_begin_patch(pattern);
            cairo_mesh_pattern_move_to(pattern, (p1->x1), (p1->y1));
            cairo_mesh_pattern_curve_to(pattern, (p1->bx1), (p1->by1), (p1->bx2), (p1->by2), (p1->x2), (p1->y2));
            cairo_mesh_pattern_curve_to(pattern, (p1->bx3), (p1->by3), (p1->bx4), (p1->by4), (p1->x3), (p1->y3));
            cairo_mesh_pattern_curve_to(pattern, (p1->bx5), (p1->by5), (p1->bx6), (p1->by6), (p1->x4), (p1->y4));
            cairo_mesh_pattern_curve_to(pattern, (p1->bx7), (p1->by7), (p1->bx8), (p1->by8), (p1->x1), (p1->y1));

            //Test hsv. Needs work. Just standardize on the -1 to 1 z range of the cosine function.
            gboolean hsv=TRUE;
            gdouble diff=(s1->z_max)-(s1->z_min);
            gdouble max=(s1->z_max);
            if(hsv)
              {
                 //hsv from red to blue or 0-240.
                 hsv2rgb((max-(p2->z1))/diff*240.0, 1.0, 1.0, &r, &g, &b);
                 cairo_mesh_pattern_set_corner_color_rgba(pattern, 0, r, g, b, 1.0);
                 hsv2rgb((max-(p2->z2))/diff*240.0, 1.0, 1.0, &r, &g, &b);
                 cairo_mesh_pattern_set_corner_color_rgba(pattern, 1, r, g, b, 1.0);
                 hsv2rgb((max-(p2->z3))/diff*240.0, 1.0, 1.0, &r, &g, &b);
                 cairo_mesh_pattern_set_corner_color_rgba(pattern, 2, r, g, b, 1.0);
                 hsv2rgb((max-(p2->z4))/diff*240.0, 1.0, 1.0, &r, &g, &b);
                 cairo_mesh_pattern_set_corner_color_rgba(pattern, 3, r, g, b, 1.0);       
                 cairo_mesh_pattern_end_patch(pattern);
              }
            else
              {
                //Test cosine function z values.
                cairo_mesh_pattern_set_corner_color_rgba(pattern, 0, 1.0-(max-(p2->z1))/diff, 1.0-(max-(p2->z1))/diff, (max-(p2->z1))/diff, 1.0);
                cairo_mesh_pattern_set_corner_color_rgba(pattern, 1, 1.0-(max-(p2->z2))/diff, 1.0-(max-(p2->z2))/diff, (max-(p2->z2))/diff, 1.0);
                cairo_mesh_pattern_set_corner_color_rgba(pattern, 2, 1.0-(max-(p2->z3))/diff, 1.0-(max-(p2->z3))/diff, (max-(p2->z3))/diff, 1.0);
                cairo_mesh_pattern_set_corner_color_rgba(pattern, 3, 1.0-(max-(p2->z4))/diff, 1.0-(max-(p2->z4))/diff, (max-(p2->z4))/diff, 1.0);
                cairo_mesh_pattern_end_patch(pattern);
              }

            cairo_set_source(cr, pattern);
            //Paint for filling.
            cairo_paint(cr);

            cairo_path_t *path=cairo_mesh_pattern_get_path(pattern, 0);
            cairo_append_path(cr, path);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
            cairo_path_destroy(path);
            
            cairo_pattern_destroy(pattern);

            p1++;    
          }
      }
  }
void draw_wire_bezier_sphere(cairo_t *cr, gint rings, gint points_per_ring)
  {
    gint i=0;
    gint j=0;

    const struct shape *s1=get_drawing_from_shape_cache(WIRE_BEZIER_SPHERE, rings, points_per_ring);
   
    if(s1!=NULL)
      {
        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
        
        //Draw the longitude rings.
        for(i=0;i<rings*2;i++)
          {  
            if(i==0) cairo_set_source_rgba(cr, 1.0, 0.0, 0.0, 1.0);
            else cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, 1.0);
            cairo_move_to(cr, ((*(p1)).x), ((*(p1)).y));
            for(j=0;j<points_per_ring/2;j++)
              {
                cairo_curve_to(cr, ((*(p1+1)).x), ((*(p1+1)).y), ((*(p1+2)).x), ((*(p1+2)).y), ((*(p1+3)).x), ((*(p1+3)).y));
                p1+=4;
              }
            cairo_stroke(cr); 
          }
         
        //Draw latitude rings.
        gint len1=points_per_ring/2-1;
        gint len2=rings*2;
        for(i=0;i<len1;i++)
          {  
            cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
            cairo_move_to(cr, ((*(p1)).x), ((*(p1)).y));
            for(j=0;j<len2;j++)
              {
                cairo_curve_to(cr, ((*(p1+1)).x), ((*(p1+1)).y), ((*(p1+2)).x), ((*(p1+2)).y), ((*(p1+3)).x), ((*(p1+3)).y));
                p1+=4;
              }
            cairo_stroke(cr);  
          }
      }
  }
void draw_solid_bezier_sphere(cairo_t *cr, gint rings, gint points_per_ring, gint compare_quads, gdouble alpha)
  {
    gint i=0;

    const struct shape *s1=get_drawing_from_shape_cache(SOLID_BEZIER_SPHERE, rings, points_per_ring);

    if(s1!=NULL)
      {
        struct bezier_quad *p1=&g_array_index(s1->sr, struct bezier_quad, 0);

        if(compare_quads==1) g_array_sort(s1->sr, compare_quads_bezier);
        else if(compare_quads==2) g_array_sort(s1->sr, compare_quads_bezier_cross_product);

        //Draw each quad separately.
        for(i=0;i<s1->sr->len;i++)
          {
            cairo_move_to(cr, (p1->x1), (p1->y1));
            cairo_curve_to(cr, (p1->bx1), (p1->by1), (p1->bx2), (p1->by2), (p1->x2), (p1->y2));
            cairo_curve_to(cr, (p1->bx3), (p1->by3), (p1->bx4), (p1->by4), (p1->x3), (p1->y3));
            cairo_curve_to(cr, (p1->bx5), (p1->by5), (p1->bx6), (p1->by6), (p1->x4), (p1->y4));
            cairo_curve_to(cr, (p1->bx7), (p1->by7), (p1->bx8), (p1->by8), (p1->x1), (p1->y1));
            cairo_close_path(cr);
            if((p1->index)%2) cairo_set_source_rgba(cr, 0.0, 1.0, 0.0, alpha);
            else cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, alpha);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
            p1++;    
          } 
      }  
  }
void draw_solid_bezier_sphere_mesh(cairo_t *cr, gint rings, gint points_per_ring, gint compare_quads, gdouble alpha)
  {
    gint i=0;
    gdouble c1[4]={1.0, 0.0, 0.0, alpha};
    gdouble c2[4]={1.0, 0.0, 0.0, alpha};
    gdouble c3[4]={0.0, 0.0, 1.0, alpha};
    gdouble c4[4]={0.0, 0.0, 1.0, alpha};
    gdouble color1=0.0;
    gdouble color2=0.0;
    
    const struct shape *s1=get_drawing_from_shape_cache(SOLID_BEZIER_SPHERE, rings, points_per_ring);

    if(s1!=NULL)
      {
        struct bezier_quad *p1=&g_array_index(s1->sr, struct bezier_quad, 0);

        if(compare_quads==1) g_array_sort(s1->sr, compare_quads_bezier);
        else if(compare_quads==2) g_array_sort(s1->sr, compare_quads_bezier_cross_product);

        for(i=0;i<s1->sr->len;i++)
          {       
            cairo_pattern_t *pattern1=cairo_pattern_create_mesh();
            cairo_mesh_pattern_begin_patch(pattern1);
            cairo_mesh_pattern_move_to(pattern1, p1->x1, p1->y1);
            cairo_mesh_pattern_curve_to(pattern1, p1->bx1, p1->by1, p1->bx2, p1->by2, p1->x2, p1->y2);
            cairo_mesh_pattern_curve_to(pattern1, p1->bx3, p1->by3, p1->bx4, p1->by4, p1->x3, p1->y3);
            cairo_mesh_pattern_curve_to(pattern1, p1->bx5, p1->by5, p1->bx6, p1->by6, p1->x4, p1->y4);
            cairo_mesh_pattern_curve_to(pattern1, p1->bx7, p1->by7, p1->bx8, p1->by8, p1->x1, p1->y1);
            //Color fade.
            color1=(gdouble)(p1->index/(rings*2))/(gdouble)(points_per_ring/2); 
            color2=(gdouble)((p1->index+rings*2)/(rings*2))/(gdouble)(points_per_ring/2); 
            c3[0]=color2;
            c4[0]=color1;     
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, c1[0], c1[1], c1[2], c1[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, c2[0], c2[1], c2[2], c2[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, c3[0], c3[1], c3[2], c3[3]);
            cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, c4[0], c4[1], c4[2], c4[3]);
            cairo_mesh_pattern_end_patch(pattern1);
            cairo_set_source(cr, pattern1);            
            cairo_move_to(cr, p1->x1, p1->y1);
            cairo_curve_to(cr, p1->bx1, p1->by1, p1->bx2, p1->by2, p1->x2, p1->y2);
            cairo_curve_to(cr, p1->bx3, p1->by3, p1->bx4, p1->by4, p1->x3, p1->y3);
            cairo_curve_to(cr, p1->bx5, p1->by5, p1->bx6, p1->by6, p1->x4, p1->y4);
            cairo_curve_to(cr, p1->bx7, p1->by7, p1->bx8, p1->by8, p1->x1, p1->y1);
            cairo_fill(cr);
            cairo_pattern_destroy(pattern1);
            p1++;
          } 
      }   
  }
void draw_solid_bezier_sphere_index(cairo_t *cr, gint w1, gint h1, gint shape_cache_index, gint compare_quads, gdouble alpha)
  {
    gint i=0;
    
    const struct shape *s1=get_drawing_from_shape_cache_index(SOLID_BEZIER_SPHERE, shape_cache_index);
    
    if(s1!=NULL)
      {
        struct bezier_quad *p1=&g_array_index(s1->sr, struct bezier_quad, 0);
        if(compare_quads==1) g_array_sort(s1->sr, compare_quads_bezier);
        else if(compare_quads==2) g_array_sort(s1->sr, compare_quads_bezier_cross_product);
        for(i=0;i<s1->sr->len;i++)
          {
            cairo_move_to(cr, (p1->x1), (p1->y1));
            cairo_curve_to(cr, (p1->bx1), (p1->by1), (p1->bx2), (p1->by2), (p1->x2), (p1->y2));
            cairo_curve_to(cr, (p1->bx3), (p1->by3), (p1->bx4), (p1->by4), (p1->x3), (p1->y3));
            cairo_curve_to(cr, (p1->bx5), (p1->by5), (p1->bx6), (p1->by6), (p1->x4), (p1->y4));
            cairo_curve_to(cr, (p1->bx7), (p1->by7), (p1->bx8), (p1->by8), (p1->x1), (p1->y1));
            cairo_close_path(cr);
            if((p1->index)%2) cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, alpha);
            else cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, alpha);
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
            p1++;    
          } 
      } 
  }
void draw_fish(cairo_t *cr, gdouble w1, gdouble h1)
  {
    gint i=0;
    gint j=0;
    gint counter=0;
    gint rings=17;
    gint points_per_ring=17;

    const struct shape *s1=get_drawing_from_shape_cache(FISH, rings, points_per_ring);

    if(s1!=NULL)
      {
        gint array_len=s1->sr->len;
        gint offset1=(s1->points_per_ring)+2;
        gint offset2=(s1->points_per_ring)+1;
        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
        struct point3d head=g_array_index(s1->sr, struct point3d, array_len-9);
        struct point3d tail=g_array_index(s1->sr, struct point3d, array_len-8);
        struct point3d *fin=&g_array_index(s1->sr, struct point3d, array_len-4);

        //An array to sort quads.
        struct quad_plane *quads_start=g_malloc((s1->rings+1)*(s1->points_per_ring)*sizeof(struct quad_plane));
        struct quad_plane *quads=quads_start;

        //Fill the quad struct array.
        for(i=0;i<(s1->rings+1);i++)
          {
            for(j=0;j<(s1->points_per_ring);j++)
              {   
                quads->index=counter;
                counter++;         
                if(i==0)
                  {
                    quads->x1=((*(p1)).x);
                    quads->y1=((*(p1)).y);
                    quads->z1=((*(p1)).z);
                    quads->x2=((*(p1+1)).x);
                    quads->y2=((*(p1+1)).y);
                    quads->z2=((*(p1+1)).z);            
                    quads->x3=head.x;
                    quads->y3=head.y;
                    quads->z3=((*(p1)).z);
                    quads->x4=head.x;
                    quads->y4=head.y;
                    quads->z4=0.0; 
                    quads->r=0.0;
                    quads->g=0.0;
                    quads->b=0.0;
                    quads->a=1.0;
                  }
                else if(i==(s1->rings))
                  {
                    quads->x1=((*(p1)).x);
                    quads->y1=((*(p1)).y);
                    quads->z1=((*(p1)).z);
                    quads->x2=((*(p1+1)).x);
                    quads->y2=((*(p1+1)).y);
                    quads->z2=((*(p1+1)).z);            
                    quads->x3=tail.x;
                    quads->y3=tail.y;
                    quads->z3=tail.z;
                    quads->x4=tail.x;
                    quads->y4=tail.y;
                    quads->z4=tail.z;
                    quads->r=1.0;
                    quads->g=1.0;
                    quads->b=0.0;
                    quads->a=1.0;              
                  }
                else
                  {                
                    quads->x1=((*(p1)).x);
                    quads->y1=((*(p1)).y);
                    quads->z1=((*(p1)).z);
                    quads->x2=((*(p1+1)).x);
                    quads->y2=((*(p1+1)).y);
                    quads->z2=((*(p1+1)).z);            
                    quads->x3=((*(p1+offset1)).x);
                    quads->y3=((*(p1+offset1)).y);
                    quads->z3=((*(p1+offset1)).z);
                    quads->x4=((*(p1+offset2)).x);
                    quads->y4=((*(p1+offset2)).y);
                    quads->z4=((*(p1+offset2)).z);
                    //The first if sets blue=1 which is then selected for the eye radial pattern.
                    if((j==5||j==11)&&i==4)
                      {
                        quads->r=0.0;
                        quads->g=0.0;
                        quads->b=1.0;
                        quads->a=1.0;
                      }
                    else if(j==2||j==4||j==6||j==10||j==12||j==14)
                      {
                        quads->r=0.8;
                        quads->g=0.0;
                        quads->b=0.8;
                        quads->a=1.0;
                      }
                    else if(j==3||j==5||j==11||j==13)
                      {
                        quads->r=1.0;
                        quads->g=1.0;
                        quads->b=0.0;
                        quads->a=1.0;
                      }
                    else
                      {
                        quads->r=1.0;
                        quads->g=0.0;
                        quads->b=0.9;
                        quads->a=0.3;
                      }
                  }
                quads++;
                p1++;
              }
            ///Draw first ring twice.
            if(i==0) p1=&g_array_index(s1->sr, struct point3d, 0);
            else p1++;
          }
        
        //Sort the quad plane array based on z values.
        qsort(quads_start, (s1->rings+1)*(s1->points_per_ring), sizeof(struct quad_plane), compare_quads_cross_product);

        //Draw the sorted quads.
        quads=quads_start;
        cairo_pattern_t *radial1=NULL;
        gdouble eye_x=0.0;
        gdouble eye_y=0.0;
        gboolean pattern_set=FALSE;
        for(i=0;i<(s1->rings+1);i++)
          {
            for(j=0;j<(s1->points_per_ring);j++)
              {
                if(quads->b>=0.95)
                  {
                    eye_x=w1*(quads->x1+quads->x2+quads->x3+quads->x4)/4.0;
                    eye_y=h1*(quads->y1+quads->y2+quads->y3+quads->y4)/4.0;
                    radial1=cairo_pattern_create_radial(eye_x, eye_y, 3.0, eye_x, eye_y, 6.0);  
                    cairo_pattern_add_color_stop_rgba(radial1, 0.0, 0.0, 0.0, 0.0, 1.0);
                    cairo_pattern_add_color_stop_rgba(radial1, 1.0, 1.0, 1.0, 0.0, 1.0);
                    cairo_set_source(cr, radial1);
                    pattern_set=TRUE;
                  }
                else cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
                cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
                cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_close_path(cr);
                //If transparent quads just fill. Otherwise fill and stroke to keep the quads smooth.
                if(pattern_set==FALSE&&(quads->a)<0.90)
                  {
                    cairo_fill(cr);
                  }
                else
                  {
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);
                  }
                quads++;
                if(pattern_set)
                  {
                    cairo_pattern_destroy(radial1);
                    pattern_set=FALSE;
                  } 
              }
          }

        //Draw tail.
        p1=&g_array_index(s1->sr, struct point3d, array_len-7);
        cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
        cairo_move_to(cr, w1*p1->x, h1*p1->y);
        cairo_line_to(cr, w1*((p1+1)->x), h1*((p1+1)->y));
        cairo_line_to(cr, w1*((p1+2)->x), h1*((p1+2)->y));
        cairo_close_path(cr);
        cairo_fill(cr);
        
        //Draw top fin has three triangles.
        p1=&g_array_index(s1->sr, struct point3d, 170);
        cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
        cairo_move_to(cr, w1*p1->x, h1*p1->y);
        cairo_line_to(cr, w1*(fin->x), h1*(fin->y));
        cairo_line_to(cr, w1*((fin+1)->x), h1*((fin+1)->y));
        cairo_close_path(cr);
        cairo_fill(cr);

        cairo_move_to(cr, w1*((p1+1)->x), h1*((p1+1)->y));
        cairo_line_to(cr, w1*(fin->x), h1*(fin->y));
        cairo_line_to(cr, w1*((fin+1)->x), h1*((fin+1)->y));
        cairo_close_path(cr);
        cairo_fill(cr);

        cairo_move_to(cr, w1*p1->x, h1*p1->y);
        cairo_line_to(cr, w1*(fin->x), h1*(fin->y));
        cairo_line_to(cr, w1*((p1+1)->x), h1*((p1+1)->y));
        cairo_close_path(cr);
        cairo_fill(cr);

        //Bottom fin has one triangle.
        p1=&g_array_index(s1->sr, struct point3d, 179);
        fin=&g_array_index(s1->sr, struct point3d, array_len-2);
        cairo_move_to(cr, w1*p1->x, h1*p1->y);
        cairo_line_to(cr, w1*(fin->x), h1*(fin->y));
        cairo_line_to(cr, w1*((fin+1)->x), h1*((fin+1)->y));
        cairo_close_path(cr);
        cairo_fill(cr);

        g_free(quads_start);
     }    
  }
void draw_turtle(cairo_t *cr, gdouble w1, gdouble h1)
  {
    /*
        Some difficulty sorting and drawing extended quads and triangles such as the front 
        flippers.
    */
    gint i=0;
    gint j=0;
    gint counter=0;
    gint rings=16;
    gint points_per_ring=16;

    const struct shape *s1=get_drawing_from_shape_cache(TURTLE, rings, points_per_ring);

    if(s1!=NULL)
      {
        gint array_len=s1->sr->len;
        gint offset1=(s1->points_per_ring)+2;
        gint offset2=(s1->points_per_ring)+1;
        struct point3d *p1=&g_array_index(s1->sr, struct point3d, 0);
        struct point3d head=g_array_index(s1->sr, struct point3d, array_len-18);
        struct point3d tail=g_array_index(s1->sr, struct point3d, array_len-17);
        //Front flippers
        struct point3d flipper1=g_array_index(s1->sr, struct point3d, array_len-16);
        struct point3d flipper1_1=g_array_index(s1->sr, struct point3d, array_len-15);
        struct point3d flipper1_2=g_array_index(s1->sr, struct point3d, array_len-14);
        struct point3d flipper1_3=g_array_index(s1->sr, struct point3d, array_len-13);
        struct point3d flipper2=g_array_index(s1->sr, struct point3d, array_len-12);
        struct point3d flipper2_1=g_array_index(s1->sr, struct point3d, array_len-11);
        struct point3d flipper2_2=g_array_index(s1->sr, struct point3d, array_len-10);
        struct point3d flipper2_3=g_array_index(s1->sr, struct point3d, array_len-9);
        //Back flippers.
        struct point3d flipper3=g_array_index(s1->sr, struct point3d, array_len-8);
        struct point3d flipper3_1=g_array_index(s1->sr, struct point3d, array_len-7);
        struct point3d flipper3_2=g_array_index(s1->sr, struct point3d, array_len-6);
        struct point3d flipper3_3=g_array_index(s1->sr, struct point3d, array_len-5);
        struct point3d flipper4=g_array_index(s1->sr, struct point3d, array_len-4);
        struct point3d flipper4_1=g_array_index(s1->sr, struct point3d, array_len-3);
        struct point3d flipper4_2=g_array_index(s1->sr, struct point3d, array_len-2);
        struct point3d flipper4_3=g_array_index(s1->sr, struct point3d, array_len-1);

        //An array to sort quads.
        struct quad_plane *quads_start=g_malloc((s1->rings+1)*(s1->points_per_ring)*sizeof(struct quad_plane));
        struct quad_plane *quads=quads_start;

        //Fill the quad struct array.
        for(i=0;i<(s1->rings+1);i++)
          {
            for(j=0;j<(s1->points_per_ring);j++)
              { 
                quads->index=counter;
                counter++;             
                if(i==0)
                  {
                    quads->x1=((*(p1)).x);
                    quads->y1=((*(p1)).y);
                    quads->z1=((*(p1)).z);
                    quads->x2=((*(p1+1)).x);
                    quads->y2=((*(p1+1)).y);
                    quads->z2=((*(p1+1)).z);            
                    quads->x3=head.x;
                    quads->y3=head.y;
                    quads->z3=head.z;
                    quads->x4=head.x;
                    quads->y4=head.y;
                    quads->z4=head.z;
                    quads->r=0.0;
                    quads->g=1.0;
                    quads->b=0.0;
                    quads->a=1.0;
                  }
                else if(i==(s1->rings))
                  {
                    quads->x1=((*(p1)).x);
                    quads->y1=((*(p1)).y);
                    quads->z1=((*(p1)).z);
                    quads->x2=((*(p1+1)).x);
                    quads->y2=((*(p1+1)).y);
                    quads->z2=((*(p1+1)).z);            
                    quads->x3=tail.x;
                    quads->y3=tail.y;
                    quads->z3=tail.z;
                    quads->x4=tail.x;
                    quads->y4=tail.y;
                    quads->z4=tail.z;
                    quads->r=0.85;
                    quads->g=1.0;
                    quads->b=0.0;
                    quads->a=1.0;              
                  }
                else
                  {                
                    quads->x1=((*(p1)).x);
                    quads->y1=((*(p1)).y);
                    quads->z1=((*(p1)).z);
                    quads->x2=((*(p1+1)).x);
                    quads->y2=((*(p1+1)).y);
                    quads->z2=((*(p1+1)).z);            
                    quads->x3=((*(p1+offset1)).x);
                    quads->y3=((*(p1+offset1)).y);
                    quads->z3=((*(p1+offset1)).z);
                    quads->x4=((*(p1+offset2)).x);
                    quads->y4=((*(p1+offset2)).y);
                    quads->z4=((*(p1+offset2)).z);
                    //Front flippers
                    if((j==8||j==15)&&i==4)
                      {
                        quads->r=0.85;
                        quads->g=1.0;
                        quads->b=0.0;
                        quads->a=1.0;
                      }
                    //Back flippers
                    else if((j==8||j==15)&&i==13)
                      {
                        quads->r=0.85;
                        quads->g=1.0;
                        quads->b=0.0;
                        quads->a=1.0;
                      }
                    //Top
                    else if(j>=0&&j<8)
                      {
                        quads->r=0.0;
                        quads->g=1.0;
                        quads->b=0.0;
                        quads->a=1.0;
                      }
                    //Bottom
                    else
                      {
                        quads->r=1.0;
                        quads->g=1.0;
                        quads->b=0.0;
                        quads->a=1.0;
                      }
                  }
                quads++;
                p1++;
              }
            //Draw first ring twice.
            if(i==0) p1=&g_array_index(s1->sr, struct point3d, 0);
            else p1++;
          }
        
        //Sort the quad plane array based on z values.
        qsort(quads_start, (s1->rings+1)*(s1->points_per_ring), sizeof(struct quad_plane), compare_quads);

        //Draw the sorted quads.
        quads=quads_start;
        cairo_pattern_t *radial1=NULL;
        gdouble eye_x=0.0;
        gdouble eye_y=0.0;
        gboolean pattern_set=FALSE;
        cairo_set_line_width(cr, 2.0); 
        for(i=0;i<(s1->rings+1);i++)
          {
            for(j=0;j<(s1->points_per_ring);j++)
              {
                if(quads->index==18||quads->index==21)
                  {
                    eye_x=w1*(quads->x1+quads->x2+quads->x3+quads->x4)/4.0;
                    eye_y=h1*(quads->y1+quads->y2+quads->y3+quads->y4)/4.0;
                    radial1=cairo_pattern_create_radial(eye_x, eye_y, 3.0, eye_x, eye_y, 6.0);  
                    cairo_pattern_add_color_stop_rgba(radial1, 0.0, 0.0, 0.0, 0.0, 1.0);
                    cairo_pattern_add_color_stop_rgba(radial1, 1.0, 0.0, 1.0, 0.0, 1.0);
                    cairo_set_source(cr, radial1);
                    pattern_set=TRUE;
                  }
                else cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
                cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
                cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
                cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                cairo_close_path(cr);
                //Add lines for the shell.
                if((quads->index)>79&&(quads->index)<240)
                  {
                    cairo_fill_preserve(cr);
                    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);         
                    cairo_stroke(cr);
                  }
                else
                  {
                    cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); 
                    cairo_fill(cr);
                    cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT); 
                  }

                /*
                  Draw the flippers. Flippers might need to be moved to be drawn last since they
                  don't sort well.
                */
                if(quads->index==72)
                  {
                    cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                    cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
                    cairo_line_to(cr, w1*flipper1.x, h1*flipper1.y);
                    cairo_curve_to(cr, w1*flipper1_1.x, h1*flipper1_1.y, w1*flipper1_2.x, h1*flipper1_2.y, w1*flipper1_3.x, h1*flipper1_3.y);
                    cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
                    cairo_close_path(cr);
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);

                    cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
                    cairo_line_to(cr, w1*flipper1.x, h1*flipper1.y);
                    cairo_curve_to(cr, w1*flipper1_1.x, h1*flipper1_1.y, w1*flipper1_2.x, h1*flipper1_2.y, w1*flipper1_3.x, h1*flipper1_3.y);
                    cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                    cairo_close_path(cr);
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);
           
                    cairo_move_to(cr, w1*quads->x4, h1*quads->y4);
                    cairo_line_to(cr, w1*flipper1.x, h1*flipper1.y);
                    cairo_curve_to(cr, w1*flipper1_1.x, h1*flipper1_1.y, w1*flipper1_2.x, h1*flipper1_2.y, w1*flipper1_3.x, h1*flipper1_3.y);
                    cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
                    cairo_close_path(cr);
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);

                    cairo_move_to(cr, w1*quads->x3, h1*quads->y3);
                    cairo_line_to(cr, w1*flipper1.x, h1*flipper1.y);
                    cairo_curve_to(cr, w1*flipper1_1.x, h1*flipper1_1.y, w1*flipper1_2.x, h1*flipper1_2.y, w1*flipper1_3.x, h1*flipper1_3.y);
                    cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
                    cairo_close_path(cr);
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);
                  }
                if(quads->index==79)
                  {
                    cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                    cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
                    cairo_line_to(cr, w1*flipper2.x, h1*flipper2.y);
                    cairo_curve_to(cr, w1*flipper2_1.x, h1*flipper2_1.y, w1*flipper2_2.x, h1*flipper2_2.y, w1*flipper2_3.x, h1*flipper2_3.y);
                    cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
                    cairo_close_path(cr);
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);

                    cairo_move_to(cr, w1*quads->x2, h1*quads->y2);
                    cairo_line_to(cr, w1*flipper2.x, h1*flipper2.y);
                    cairo_curve_to(cr, w1*flipper2_1.x, h1*flipper2_1.y, w1*flipper2_2.x, h1*flipper2_2.y, w1*flipper2_3.x, h1*flipper2_3.y);
                    cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
                    cairo_close_path(cr);
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);
           
                    cairo_move_to(cr, w1*quads->x3, h1*quads->y3);
                    cairo_line_to(cr, w1*flipper2.x, h1*flipper2.y);
                    cairo_curve_to(cr, w1*flipper2_1.x, h1*flipper2_1.y, w1*flipper2_2.x, h1*flipper2_2.y, w1*flipper2_3.x, h1*flipper2_3.y);
                    cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                    cairo_close_path(cr);
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);

                    cairo_move_to(cr, w1*quads->x4, h1*quads->y4);
                    cairo_line_to(cr, w1*flipper2.x, h1*flipper2.y);
                    cairo_curve_to(cr, w1*flipper2_1.x, h1*flipper2_1.y, w1*flipper2_2.x, h1*flipper2_2.y, w1*flipper2_3.x, h1*flipper2_3.y);
                    cairo_line_to(cr, w1*quads->x1, h1*quads->y1);
                    cairo_close_path(cr);
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);
                  }
                //Back flipper.
                if(quads->index==216)
                  {
                    cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                    cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
                    cairo_line_to(cr, w1*flipper3.x, h1*flipper3.y);
                    cairo_curve_to(cr, w1*flipper3_1.x, h1*flipper3_1.y, w1*flipper3_2.x, h1*flipper3_2.y, w1*flipper3_3.x, h1*flipper3_3.y);
                    cairo_line_to(cr, w1*quads->x2, h1*quads->y2);
                    cairo_close_path(cr);
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);
     
                    cairo_move_to(cr, w1*quads->x2, h1*quads->y2);
                    cairo_line_to(cr, w1*flipper3.x, h1*flipper3.y);
                    cairo_curve_to(cr, w1*flipper3_1.x, h1*flipper3_1.y, w1*flipper3_2.x, h1*flipper3_2.y, w1*flipper3_3.x, h1*flipper3_3.y);
                    cairo_line_to(cr, w1*quads->x3, h1*quads->y3);
                    cairo_close_path(cr);
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);
            
                    cairo_move_to(cr, w1*quads->x3, h1*quads->y3);
                    cairo_line_to(cr, w1*flipper3.x, h1*flipper3.y);
                    cairo_curve_to(cr, w1*flipper3_1.x, h1*flipper3_1.y, w1*flipper3_2.x, h1*flipper3_2.y, w1*flipper3_3.x, h1*flipper3_3.y);
                    cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                    cairo_close_path(cr);
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);

                    cairo_move_to(cr, w1*quads->x4, h1*quads->y4);
                    cairo_line_to(cr, w1*flipper3.x, h1*flipper3.y);
                    cairo_curve_to(cr, w1*flipper3_1.x, h1*flipper3_1.y, w1*flipper3_2.x, h1*flipper3_2.y, w1*flipper3_3.x, h1*flipper3_3.y);
                    cairo_line_to(cr, w1*quads->x1, h1*quads->y1);
                    cairo_close_path(cr);
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);
                  }
                if(quads->index==223)
                  {
                    cairo_set_source_rgba(cr, quads->r, quads->g, quads->b, quads->a);
                    cairo_move_to(cr, w1*quads->x1, h1*quads->y1);
                    cairo_line_to(cr, w1*flipper4.x, h1*flipper4.y);
                    cairo_curve_to(cr, w1*flipper4_1.x, h1*flipper4_1.y, w1*flipper4_2.x, h1*flipper4_2.y, w1*flipper4_3.x, h1*flipper4_3.y);
                    cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                    cairo_close_path(cr);
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);
     
                    cairo_move_to(cr, w1*quads->x2, h1*quads->y2);
                    cairo_line_to(cr, w1*flipper4.x, h1*flipper4.y);
                    cairo_curve_to(cr, w1*flipper4_1.x, h1*flipper4_1.y, w1*flipper4_2.x, h1*flipper4_2.y, w1*flipper4_3.x, h1*flipper4_3.y);
                    cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                    cairo_close_path(cr);
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);
            
                    cairo_move_to(cr, w1*quads->x3, h1*quads->y3);
                    cairo_line_to(cr, w1*flipper4.x, h1*flipper4.y);
                    cairo_curve_to(cr, w1*flipper4_1.x, h1*flipper4_1.y, w1*flipper4_2.x, h1*flipper4_2.y, w1*flipper4_3.x, h1*flipper4_3.y);
                    cairo_line_to(cr, w1*quads->x4, h1*quads->y4);
                    cairo_close_path(cr);
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);

                    cairo_move_to(cr, w1*quads->x4, h1*quads->y4);
                    cairo_line_to(cr, w1*flipper4.x, h1*flipper4.y);
                    cairo_curve_to(cr, w1*flipper4_1.x, h1*flipper4_1.y, w1*flipper4_2.x, h1*flipper4_2.y, w1*flipper4_3.x, h1*flipper4_3.y);
                    cairo_line_to(cr, w1*quads->x1, h1*quads->y1);
                    cairo_close_path(cr);
                    cairo_fill_preserve(cr);
                    cairo_stroke(cr);
                  }
                quads++;
                if(pattern_set)
                  {
                    cairo_pattern_destroy(radial1);
                    pattern_set=FALSE;
                  } 
              }
          }

        g_free(quads_start); 
     }   
  }  
static int compare_planes(const void *a, const void *b)
  {
    gdouble a2=((struct plane_order*)a)->value;
    gdouble b2=((struct plane_order*)b)->value;
    if(a2<b2) return -1;
    else if(a2==b2) return 0;
    else return 1;
  }
static int compare_quads(const void *a, const void *b)
  {
    gdouble a2=(((struct quad_plane*)a)->z1+((struct quad_plane*)a)->z2+((struct quad_plane*)a)->z3+((struct quad_plane*)a)->z4);
    gdouble b2=(((struct quad_plane*)b)->z1+((struct quad_plane*)b)->z2+((struct quad_plane*)b)->z3+((struct quad_plane*)b)->z4);
    if(a2<b2) return -1;
    else if(a2==b2) return 0;
    else return 1;
  }
static int compare_quads2(const void *a, const void *b)
  {
    gdouble a2=(((struct quad_plane2*)a)->z1+((struct quad_plane2*)a)->z2+((struct quad_plane2*)a)->z3+((struct quad_plane2*)a)->z4);
    gdouble b2=(((struct quad_plane2*)b)->z1+((struct quad_plane2*)b)->z2+((struct quad_plane2*)b)->z3+((struct quad_plane2*)b)->z4);
    if(a2<b2) return -1;
    else if(a2==b2) return 0;
    else return 1;
  }
/*
    Used for the fish drawing. A few more calculation than compare_quads(). It may or may not work
    better. Looks to work better for the fish but not so good for the turtle.
*/
static int compare_quads_cross_product(const void *a, const void *b)
  {
    const struct quad_plane *a1=a;
    const struct quad_plane *b1=b;
    gdouble a2=(((a1->x1)-(a1->x2))*((a1->y1)-(a1->y4)))-(((a1->y1)-(a1->y2))*((a1->x1)-(a1->x4)));
    gdouble b2=(((b1->x1)-(b1->x2))*((b1->y1)-(b1->y4)))-(((b1->y1)-(b1->y2))*((b1->x1)-(b1->x4)));
    if(b2<a2) return -1;
    else if(a2==b2) return 0;
    else return 1;
  }
static int compare_quads_bezier(const void *a, const void *b)
  {
    gdouble a2=(((struct bezier_quad*)a)->z1+((struct bezier_quad*)a)->z2+((struct bezier_quad*)a)->z3+((struct bezier_quad*)a)->z4+((struct bezier_quad*)a)->bz1+((struct bezier_quad*)a)->bz2+((struct bezier_quad*)a)->bz3+((struct bezier_quad*)a)->bz4+((struct bezier_quad*)a)->bz5+((struct bezier_quad*)a)->bz6+((struct bezier_quad*)a)->bz7+((struct bezier_quad*)a)->bz8);
    gdouble b2=(((struct bezier_quad*)b)->z1+((struct bezier_quad*)b)->z2+((struct bezier_quad*)b)->z3+((struct bezier_quad*)b)->z4+((struct bezier_quad*)b)->bz1+((struct bezier_quad*)b)->bz2+((struct bezier_quad*)b)->bz3+((struct bezier_quad*)b)->bz4+((struct bezier_quad*)b)->bz5+((struct bezier_quad*)b)->bz6+((struct bezier_quad*)b)->bz7+((struct bezier_quad*)b)->bz8);

    if(a2<b2) return -1;
    else if(a2==b2) return 0;
    else return 1;
  }
static int compare_quads_bezier_cross_product(const void *a, const void *b)
  {
    const struct bezier_quad *a1=a;
    const struct bezier_quad *b1=b;
    gdouble a2=(((a1->x1)-(a1->x2))*((a1->y1)-(a1->y4)))-(((a1->y1)-(a1->y2))*((a1->x1)-(a1->x4)));
    gdouble b2=(((b1->x1)-(b1->x2))*((b1->y1)-(b1->y4)))-(((b1->y1)-(b1->y2))*((b1->x1)-(b1->x4)));
    if(b2>a2) return -1;
    else if(a2==b2) return 0;
    else return 1;
  }
/*
    This is some exellent work done by Ramsundar Shandilya. Note the following for the original work
    and the rational behind it.
    
    https://medium.com/@ramshandilya/draw-smooth-curves-through-a-set-of-points-in-ios-34f6d73c8f9

    https://github.com/Ramshandilya/Bezier

    The MIT License (MIT)

    Copyright (c) 2015 Ramsundar Shandilya

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
  
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

    This is a translation of the original Swift code to C. It makes the function easy to use with GTK+,
    cairo and glib.

    This is adjusted to calculate 2d points from 3d points. Can use this from a rectangular 
    bezier sheet by holding one variable constant. Saves a few calculations.
*/
static GArray* control_points_from_coords_2d(const GArray *data_points3d)
  {  
    gint i=0;
    GArray *control_points=NULL;      
    //Number of Segments
    gint count=0;
    if(data_points3d!=NULL) count=data_points3d->len-1;
    gdouble *fCP=NULL;
    gdouble *sCP=NULL;

    if(count>0)
      {
        fCP=(gdouble*)g_malloc(2*count*sizeof(gdouble));
        sCP=(gdouble*)g_malloc(2*count*sizeof(gdouble));
      }
        
    //P0, P1, P2, P3 are the points for each segment, where P0 & P3 are the knots and P1, P2 are the control points.
    if(count<1||data_points3d==NULL)
      {
        //Return NULL.
        control_points=NULL;
        g_warning("Can't get control points from coordinates. NULL returned.\n");
      }
    else if(count==1)
      {
        struct point3d P0=g_array_index(data_points3d, struct point3d, 0);
        struct point3d P3=g_array_index(data_points3d, struct point3d, 1);

        //Calculate First Control Point
        //3P1 = 2P0 + P3
        struct point3d P1;
        P1.x=(2.0*P0.x+P3.x)/3.0;
        P1.y=(2.0*P0.y+P3.y)/3.0;

        *(fCP)=P1.x;
        *(fCP+1)=P1.y;

        //Calculate second Control Point
        //P2 = 2P1 - P0
        struct point3d P2;
        P2.x=(2.0*P1.x-P0.x);
        P2.y=(2.0*P1.y-P0.x);

        *(sCP)=P2.x;
        *(sCP+1)=P2.y;     
      }
    else
      {
        gdouble *rhs=(gdouble*)g_malloc(2*count*sizeof(gdouble));
        gdouble *a=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *b=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *c=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble rhsValueX=0;
        gdouble rhsValueY=0;
        struct point3d P0;
        struct point3d P3;        
        gdouble m=0;
        gdouble b1=0;
        gdouble r2x=0;
        gdouble r2y=0;
        gdouble P1_x=0;
        gdouble P1_y=0;
   
        for(i=0;i<count;i++)
          {
            P0=g_array_index(data_points3d, struct point3d, i);
            P3=g_array_index(data_points3d, struct point3d, i+1);

            if(i==0)
              {
                *(a)=0.0;
                *(b)=2.0;
                *(c)=1.0;

                //rhs for first segment
                rhsValueX=P0.x+2.0*P3.x;
                rhsValueY=P0.y+2.0*P3.y;
              }
            else if(i==count-1)
              {
                *(a+i)=2.0;
                *(b+i)=7.0;
                *(c+i)=0.0;

                //rhs for last segment
                rhsValueX=8.0*P0.x+P3.x;
                rhsValueY=8.0*P0.y+P3.y;
              }
            else
              {
                *(a+i)=1.0;
                *(b+i)=4.0;
                *(c+i)=1.0;

                rhsValueX=4.0*P0.x+2.0*P3.x;
                rhsValueY=4.0*P0.y+2.0*P3.y;
              }
            *(rhs+i*2)=rhsValueX;
            *(rhs+i*2+1)=rhsValueY;
          }

        //Solve Ax=B. Use Tridiagonal matrix algorithm a.k.a Thomas Algorithm
        for(i=1;i<count;i++)
          {
            m=(*(a+i))/(*(b+i-1));

            b1=(*(b+i))-m*(*(c+i-1));
            *(b+i)=b1;

            r2x=(*(rhs+i*2))-m*(*(rhs+(i-1)*2));
            r2y=(*(rhs+i*2+1))-m*(*(rhs+(i-1)*2+1));

            *(rhs+i*2)=r2x;
            *(rhs+i*2+1)=r2y;
          }
        
        //Last control Point
        gdouble lastControlPointX=(*(rhs+2*count-2))/(*(b+count-1));
        gdouble lastControlPointY=(*(rhs+2*count-1))/(*(b+count-1));

        *(fCP+2*count-2)=lastControlPointX;
        *(fCP+2*count-1)=lastControlPointY;

        gdouble controlPointX=0;
        gdouble controlPointY=0;

        for(i=count-2;i>=0;--i)
          {
            controlPointX=(*(rhs+i*2)-(*(c+i))*(*(fCP+(i+1)*2)))/(*(b+i));
            controlPointY=(*(rhs+i*2+1)-(*(c+i))*(*(fCP+(i+1)*2+1)))/(*(b+i));

             *(fCP+i*2)=controlPointX;
             *(fCP+i*2+1)=controlPointY; 
          }

        //Compute second Control Points from first.
        for(i=0;i<count;i++)
          {
            if(i==count-1)
              {
                P3=g_array_index(data_points3d, struct point3d, i+1);
                P1_x=(*(fCP+i*2));
                P1_y=(*(fCP+i*2+1));

                controlPointX=(P3.x+P1_x)/2.0;
                controlPointY=(P3.y+P1_y)/2.0;

                *(sCP+count*2-2)=controlPointX;
                *(sCP+count*2-1)=controlPointY;
              }
            else
              {
                P3=g_array_index(data_points3d, struct point3d, i+1);                
                P1_x=(*(fCP+(i+1)*2));
                P1_y=(*(fCP+(i+1)*2+1));

                controlPointX=2.0*P3.x-P1_x;
                controlPointY=2.0*P3.y-P1_y;

                *(sCP+i*2)=controlPointX;
                *(sCP+i*2+1)=controlPointY;
              }

          }

        control_points=g_array_sized_new(FALSE, FALSE, sizeof(struct controls2d), count);
        g_array_set_size(control_points, count);
        struct controls2d *cp=&g_array_index(control_points, struct controls2d, 0);;
        for(i=0;i<count;i++)
          {
            (cp->x1)=(*(fCP+i*2));
            (cp->y1)=(*(fCP+i*2+1));
            (cp->x2)=(*(sCP+i*2));
            (cp->y2)=(*(sCP+i*2+1));
            cp++;
          }

        g_free(rhs);
        g_free(a);
        g_free(b);
        g_free(c);
     }

    if(fCP!=NULL) g_free(fCP);
    if(sCP!=NULL) g_free(sCP);

    return control_points;
  }
/*
  This is adjusted for calculating 3d control points from 3d points. Need this for the bezier
  surface disc.
*/
static GArray* control_points_from_coords_3d(const GArray *data_points3d)
  {  
    gint i=0;
    GArray *control_points=NULL;      
    //Number of Segments
    gint count=0;
    if(data_points3d!=NULL) count=data_points3d->len-1;
    gdouble *fCP=NULL;
    gdouble *sCP=NULL;

    if(count>0)
      {
        fCP=(gdouble*)g_malloc(3*count*sizeof(gdouble));
        sCP=(gdouble*)g_malloc(3*count*sizeof(gdouble));
      }
        
    //P0, P1, P2, P3 are the points for each segment, where P0 & P3 are the knots and P1, P2 are the control points.
    if(count<1||data_points3d==NULL)
      {
        //Return NULL.
        control_points=NULL;
        g_warning("Can't get control points from coordinates. NULL returned.\n");
      }
    else if(count==1)
      {
        struct point3d P0=g_array_index(data_points3d, struct point3d, 0);
        struct point3d P3=g_array_index(data_points3d, struct point3d, 1);

        //Calculate First Control Point
        //3P1 = 2P0 + P3
        struct point3d P1;
        P1.x=(2.0*P0.x+P3.x)/3.0;
        P1.y=(2.0*P0.y+P3.y)/3.0;
        P1.z=(2.0*P0.z+P3.z)/3.0;

        *(fCP)=P1.x;
        *(fCP+1)=P1.y;
        *(fCP+2)=P1.z;

        //Calculate second Control Point
        //P2 = 2P1 - P0
        struct point3d P2;
        P2.x=(2.0*P1.x-P0.x);
        P2.y=(2.0*P1.y-P0.x);
        P2.z=(2.0*P1.z-P0.x);

        *(sCP)=P2.x;
        *(sCP+1)=P2.y;
        *(sCP+2)=P2.z;     
      }
    else
      {
        gdouble *rhs=(gdouble*)g_malloc(3*count*sizeof(gdouble));
        gdouble *a=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *b=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble *c=(gdouble*)g_malloc(count*sizeof(gdouble));
        gdouble rhsValueX=0;
        gdouble rhsValueY=0;
        gdouble rhsValueZ=0;
        struct point3d P0;
        struct point3d P3;        
        gdouble m=0;
        gdouble b1=0;
        gdouble r2x=0;
        gdouble r2y=0;
        gdouble r2z=0;
        gdouble P1_x=0;
        gdouble P1_y=0;
        gdouble P1_z=0;
   
        for(i=0;i<count;i++)
          {
            P0=g_array_index(data_points3d, struct point3d, i);
            P3=g_array_index(data_points3d, struct point3d, i+1);

            if(i==0)
              {
                *(a)=0.0;
                *(b)=2.0;
                *(c)=1.0;

                //rhs for first segment
                rhsValueX=P0.x+2.0*P3.x;
                rhsValueY=P0.y+2.0*P3.y;
                rhsValueZ=P0.z+2.0*P3.z;
              }
            else if(i==count-1)
              {
                *(a+i)=2.0;
                *(b+i)=7.0;
                *(c+i)=0.0;

                //rhs for last segment
                rhsValueX=8.0*P0.x+P3.x;
                rhsValueY=8.0*P0.y+P3.y;
                rhsValueZ=8.0*P0.z+P3.z;
              }
            else
              {
                *(a+i)=1.0;
                *(b+i)=4.0;
                *(c+i)=1.0;

                rhsValueX=4.0*P0.x+2.0*P3.x;
                rhsValueY=4.0*P0.y+2.0*P3.y;
                rhsValueZ=4.0*P0.z+2.0*P3.z;
              }
            *(rhs+i*3)=rhsValueX;
            *(rhs+i*3+1)=rhsValueY;
            *(rhs+i*3+2)=rhsValueZ;
          }

        //Solve Ax=B. Use Tridiagonal matrix algorithm a.k.a Thomas Algorithm
        for(i=1;i<count;i++)
          {
            m=(*(a+i))/(*(b+i-1));

            b1=(*(b+i))-m*(*(c+i-1));
            *(b+i)=b1;

            r2x=(*(rhs+i*3))-m*(*(rhs+(i-1)*3));
            r2y=(*(rhs+i*3+1))-m*(*(rhs+(i-1)*3+1));
            r2z=(*(rhs+i*3+2))-m*(*(rhs+(i-1)*3+2));

            *(rhs+i*3)=r2x;
            *(rhs+i*3+1)=r2y;
            *(rhs+i*3+2)=r2z;
          }
        
        //Last control Point
        gdouble lastControlPointX=(*(rhs+3*count-3))/(*(b+count-1));
        gdouble lastControlPointY=(*(rhs+3*count-2))/(*(b+count-1));
        gdouble lastControlPointZ=(*(rhs+3*count-1))/(*(b+count-1));

        *(fCP+3*count-3)=lastControlPointX;
        *(fCP+3*count-2)=lastControlPointY;
        *(fCP+3*count-1)=lastControlPointZ;

        gdouble controlPointX=0;
        gdouble controlPointY=0;
        gdouble controlPointZ=0;

        for(i=count-2;i>=0;--i)
          {
            controlPointX=(*(rhs+i*3)-(*(c+i))*(*(fCP+(i+1)*3)))/(*(b+i));
            controlPointY=(*(rhs+i*3+1)-(*(c+i))*(*(fCP+(i+1)*3+1)))/(*(b+i));
            controlPointZ=(*(rhs+i*3+2)-(*(c+i))*(*(fCP+(i+1)*3+2)))/(*(b+i));

             *(fCP+i*3)=controlPointX;
             *(fCP+i*3+1)=controlPointY;
             *(fCP+i*3+2)=controlPointZ; 
          }

        //Compute second Control Points from first.
        for(i=0;i<count;i++)
          {
            if(i==count-1)
              {
                P3=g_array_index(data_points3d, struct point3d, i+1);
                P1_x=(*(fCP+i*3));
                P1_y=(*(fCP+i*3+1));
                P1_z=(*(fCP+i*3+2));

                controlPointX=(P3.x+P1_x)/2.0;
                controlPointY=(P3.y+P1_y)/2.0;
                controlPointZ=(P3.z+P1_z)/2.0;

                *(sCP+count*3-3)=controlPointX;
                *(sCP+count*3-2)=controlPointY;
                *(sCP+count*3-1)=controlPointZ;
              }
            else
              {
                P3=g_array_index(data_points3d, struct point3d, i+1);                
                P1_x=(*(fCP+(i+1)*3));
                P1_y=(*(fCP+(i+1)*3+1));
                P1_z=(*(fCP+(i+1)*3+2));

                controlPointX=2.0*P3.x-P1_x;
                controlPointY=2.0*P3.y-P1_y;
                controlPointZ=2.0*P3.z-P1_z;

                *(sCP+i*3)=controlPointX;
                *(sCP+i*3+1)=controlPointY;
                *(sCP+i*3+2)=controlPointZ;
              }

          }

        control_points=g_array_sized_new(FALSE, FALSE, sizeof(struct controls3d), count);
        g_array_set_size(control_points, count);
        struct controls3d *cp=&g_array_index(control_points, struct controls3d, 0);;
        for(i=0;i<count;i++)
          {
            (cp->x1)=(*(fCP+i*3));
            (cp->y1)=(*(fCP+i*3+1));
            (cp->z1)=(*(fCP+i*3+2));
            (cp->x2)=(*(sCP+i*3));
            (cp->y2)=(*(sCP+i*3+1));
            (cp->z2)=(*(sCP+i*3+2));
            cp++;
          }

        g_free(rhs);
        g_free(a);
        g_free(b);
        g_free(c);
     }

    if(fCP!=NULL) g_free(fCP);
    if(sCP!=NULL) g_free(sCP);

    return control_points;
  }
static GArray* get_bezier_ring(gdouble radius, gint points_per_ring, gboolean half_ring)
  {
    gint i=0;
    gdouble arc_top=-G_PI/(gdouble)points_per_ring;
    gdouble arc_offset=-2.0*G_PI/(gdouble)points_per_ring;
    gdouble start_x=radius*cos(arc_top);
    gdouble start_y=radius*sin(arc_top);
    gdouble end_x=start_x;
    gdouble end_y=-start_y;
    gdouble c1_x=(4.0*radius-start_x)/3.0;
    gdouble c1_y=((radius-start_x)*(3.0*radius-start_x))/(3.0*start_y);
    gdouble c2_x=c1_x;
    gdouble c2_y=-c1_y;
    gdouble offset1=0.0;
    gdouble offset2=0.0;

    if(half_ring) points_per_ring=points_per_ring/2;

    GArray *one_ring=g_array_sized_new(FALSE, FALSE, sizeof(struct arc), points_per_ring);
    g_array_set_size(one_ring, points_per_ring);
    struct arc *p1=&g_array_index(one_ring, struct arc, 0);
    struct arc *p2=&g_array_index(one_ring, struct arc, 1);

    //Rotate the points in the section to align with the zero point on the unit circle.
    offset1=cos(arc_top);
    offset2=sin(arc_top);
    (p1->a4).x=start_x*offset1-start_y*offset2;
    (p1->a4).y=start_x*offset2+start_y*offset1;
    (p1->a4).z=0.0;
    (p1->a3).x=c1_x*offset1-c1_y*offset2;
    (p1->a3).y=c1_x*offset2+c1_y*offset1;
    (p1->a3).z=0.0;
    (p1->a2).x=c2_x*offset1-c2_y*offset2;
    (p1->a2).y=c2_x*offset2+c2_y*offset1;
    (p1->a2).z=0.0;
    (p1->a1).x=end_x*offset1-end_y*offset2;
    (p1->a1).y=end_x*offset2+end_y*offset1;
    (p1->a1).z=0.0;

    //Add the rest of the sections.
    offset1=cos(arc_offset);
    offset2=sin(arc_offset);
    for(i=1;i<points_per_ring;i++)
      {
        (p2->a1).x=(p1->a1).x*offset1-(p1->a1).y*offset2;
        (p2->a1).y=(p1->a1).x*offset2+(p1->a1).y*offset1;
        (p2->a1).z=0.0;
        (p2->a2).x=(p1->a2).x*offset1-(p1->a2).y*offset2;
        (p2->a2).y=(p1->a2).x*offset2+(p1->a2).y*offset1;
        (p2->a2).z=0.0;
        (p2->a3).x=(p1->a3).x*offset1-(p1->a3).y*offset2;
        (p2->a3).y=(p1->a3).x*offset2+(p1->a3).y*offset1;
        (p2->a3).z=0.0;
        (p2->a4).x=(p1->a4).x*offset1-(p1->a4).y*offset2;
        (p2->a4).y=(p1->a4).x*offset2+(p1->a4).y*offset1;
        (p2->a4).z=0.0;
        p1++;p2++;
      }
      
    return one_ring;
  }
/*
    From
    https://stackoverflow.com/questions/3018313/algorithm-to-convert-rgb-to-hsv-and-hsv-to-rgb-in-range-0-255-for-both
*/
static void hsv2rgb(double h, double s, double v, double *r, double *g, double *b)
  {
    double hh=0.0;
    double p=0.0;
    double q=0.0;
    double t=0.0;
    double ff=0.0;
    long i=0;

    hh = h;
    if(hh >= 360.0) hh = 0.0;
    hh /= 60.0;
    i = (long)hh;
    ff = hh - i;
    p = v * (1.0 - s);
    q = v * (1.0 - (s * ff));
    t = v * (1.0 - (s * (1.0 - ff)));

    switch(i) {
    case 0:
        *r = v;
        *g = t;
        *b = p;
        break;
    case 1:
        *r = q;
        *g = v;
        *b = p;
        break;
    case 2:
        *r = p;
        *g = v;
        *b = t;
        break;
    case 3:
        *r = p;
        *g = q;
        *b = v;
        break;
    case 4:
        *r = t;
        *g = p;
        *b = v;
        break;
    case 5:
    default:
        *r = v;
        *g = p;
        *b = q;
        break;
    }     
  }
static void set_tri_points_1(struct bezier_quad *q1, struct point3d *p1, struct point3d *p2, struct point3d *p3)
  {
    (*q1).x1=p1->x;        (*q1).x2=(p1+3)->x;        
    (*q1).y1=p1->y;        (*q1).y2=(p1+3)->y;
    (*q1).z1=p1->z;        (*q1).z2=(p1+3)->z;
    (*q1).bx1=(p1+1)->x;   (*q1).bx3=(p2+1)->x;
    (*q1).by1=(p1+1)->y;   (*q1).by3=(p2+1)->y;
    (*q1).bz1=(p1+1)->z;   (*q1).bz3=(p2+1)->z;
    (*q1).bx2=(p1+2)->x;   (*q1).bx4=(p2+2)->x;
    (*q1).by2=(p1+2)->y;   (*q1).by4=(p2+2)->y;
    (*q1).bz2=(p1+2)->z;   (*q1).bz4=(p2+2)->z;

    (*q1).x3=(p2+3)->x;    (*q1).x4=p1->x;
    (*q1).y3=(p2+3)->y;    (*q1).y4=p1->y;
    (*q1).z3=(p2+3)->z;    (*q1).z4=p1->z;
    (*q1).bx5=(p3+2)->x;   (*q1).bx7=p1->x;
    (*q1).by5=(p3+2)->y;   (*q1).by7=p1->y;
    (*q1).bz5=(p3+2)->z;   (*q1).bz7=p1->z;
    (*q1).bx6=(p3+1)->x;   (*q1).bx8=p1->x;
    (*q1).by6=(p3+1)->y;   (*q1).by8=p1->y;
    (*q1).bz6=(p3+1)->z;   (*q1).bz8=p1->z;
  }
static void set_tri_points_2(struct bezier_quad *q1, struct point3d *p1, struct point3d *p2, struct point3d *p3)
  {
    (*q1).x1=p1->x;        (*q1).x2=(p1+3)->x;        
    (*q1).y1=p1->y;        (*q1).y2=(p1+3)->y;
    (*q1).z1=p1->z;        (*q1).z2=(p1+3)->z;
    (*q1).bx1=(p1+1)->x;   (*q1).bx3=(p1+3)->x;
    (*q1).by1=(p1+1)->y;   (*q1).by3=(p1+3)->y;
    (*q1).bz1=(p1+1)->z;   (*q1).bz3=(p1+3)->z;
    (*q1).bx2=(p1+2)->x;   (*q1).bx4=(p1+3)->x;
    (*q1).by2=(p1+2)->y;   (*q1).by4=(p1+3)->y;
    (*q1).bz2=(p1+2)->z;   (*q1).bz4=(p1+3)->z;
 
    (*q1).x3=(p1+3)->x;    (*q1).x4=(p2+3)->x;
    (*q1).y3=(p1+3)->y;    (*q1).y4=(p2+3)->y;
    (*q1).z3=(p1+3)->z;    (*q1).z4=(p2+3)->z;
    (*q1).bx5=(p3+2)->x;   (*q1).bx7=(p2+2)->x;
    (*q1).by5=(p3+2)->y;   (*q1).by7=(p2+2)->y;
    (*q1).bz5=(p3+2)->z;   (*q1).bz7=(p2+2)->z;
    (*q1).bx6=(p3+1)->x;   (*q1).bx8=(p2+1)->x;
    (*q1).by6=(p3+1)->y;   (*q1).by8=(p2+1)->y;
    (*q1).bz6=(p3+1)->z;   (*q1).bz8=(p2+1)->z;
  }
static void set_quad_points(struct bezier_quad *q1, struct point3d *p1, struct point3d *p2, struct point3d *p3, struct point3d *p4)
  {
    (*q1).x1=p1->x;        (*q1).x2=(p1+3)->x;        
    (*q1).y1=p1->y;        (*q1).y2=(p1+3)->y;
    (*q1).z1=p1->z;        (*q1).z2=(p1+3)->z;
    (*q1).bx1=(p1+1)->x;   (*q1).bx3=(p4+1)->x;
    (*q1).by1=(p1+1)->y;   (*q1).by3=(p4+1)->y;
    (*q1).bz1=(p1+1)->z;   (*q1).bz3=(p4+1)->z;
    (*q1).bx2=(p1+2)->x;   (*q1).bx4=(p4+2)->x;
    (*q1).by2=(p1+2)->y;   (*q1).by4=(p4+2)->y;
    (*q1).bz2=(p1+2)->z;   (*q1).bz4=(p4+2)->z;

    (*q1).x3=(p4+3)->x;    (*q1).x4=p2->x;
    (*q1).y3=(p4+3)->y;    (*q1).y4=p2->y;
    (*q1).z3=(p4+3)->z;    (*q1).z4=p2->z;
    (*q1).bx5=(p2+2)->x;   (*q1).bx7=(p3+2)->x;
    (*q1).by5=(p2+2)->y;   (*q1).by7=(p3+2)->y;
    (*q1).bz5=(p2+2)->z;   (*q1).bz7=(p3+2)->z;
    (*q1).bx6=(p2+1)->x;   (*q1).bx8=(p3+1)->x;
    (*q1).by6=(p2+1)->y;   (*q1).by8=(p3+1)->y;
    (*q1).bz6=(p2+1)->z;   (*q1).bz8=(p3+1)->z;
  }
void smooth_wire_bezier_sphere_longitude(gint cache_id, gint rings, gint points_per_ring, gint shape_cache_index)
  {
    gint i=0;
    gint j=0;
    struct shape *s1=NULL;
    if(shape_cache_index==-1) s1=get_drawing_from_shape_cache(WIRE_BEZIER_SPHERE, rings, points_per_ring);
    else s1=get_drawing_from_shape_cache_index(WIRE_BEZIER_SPHERE, shape_cache_index);
    
    if(s1!=NULL)
      {
        GArray *one_long=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), points_per_ring/2+1);
        g_array_set_size(one_long, points_per_ring/2+1);
        struct point3d *rp1=NULL;
        struct point3d *p1=&g_array_index(s1->s, struct point3d, 0);
        struct point3d *p2=&g_array_index(s1->s, struct point3d, 0);
        for(i=0;i<rings*2;i++)
          {
            rp1=&g_array_index(one_long, struct point3d, 0);
            p2=p1;
            rp1->x=p1->x;
            rp1->y=p1->y;
            rp1->z=p1->z;
            rp1++;
            for(j=0;j<points_per_ring/2;j++)
              {
                rp1->x=(p1+3)->x;
                rp1->y=(p1+3)->y;
                rp1->z=(p1+3)->z;
                p1+=4;rp1++;                
              }
                  
            GArray *controls=control_points_from_coords_3d(one_long);
            struct controls3d *c1=&g_array_index(controls, struct controls3d, 0);
            for(j=0;j<points_per_ring/2;j++)
              {
                (p2+1)->x=c1->x1;
                (p2+1)->y=c1->y1;
                (p2+1)->z=c1->z1;
                (p2+2)->x=c1->x2;
                (p2+2)->y=c1->y2;
                (p2+2)->z=c1->z2;
                p2+=4;c1++;                
              } 
            g_array_free(controls, TRUE);            
          }
         g_array_free(one_long, TRUE);
     }
  }
void smooth_wire_bezier_sphere_latitude(gint cache_id, gint rings, gint points_per_ring, gint shape_cache_index)
  {
    gint i=0;
    gint j=0;
    struct shape *s1=NULL;
    if(shape_cache_index==-1) s1=get_drawing_from_shape_cache(WIRE_BEZIER_SPHERE, rings, points_per_ring);
    else s1=get_drawing_from_shape_cache_index(WIRE_BEZIER_SPHERE, shape_cache_index);
    
    if(s1!=NULL)
      {
        gint len1=points_per_ring/2-1;
        gint len2=rings*2;
        struct point3d *p1=&g_array_index(s1->s, struct point3d, 4*(rings*2)*(points_per_ring/2));
        struct point3d *p2=&g_array_index(s1->s, struct point3d, 0);
        GArray *one_lat=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 2*rings+1);
        g_array_set_size(one_lat, 2*rings+1);
        struct point3d *cp1=NULL;
        for(i=0;i<len1;i++)
          {
            cp1=&g_array_index(one_lat, struct point3d, 0);
            p2=p1;
            cp1->x=p1->x;
            cp1->y=p1->y;
            cp1->z=p1->z;
            cp1++;  
            for(j=0;j<len2;j++)
              {
                cp1->x=(p1+3)->x;
                cp1->y=(p1+3)->y;
                cp1->z=(p1+3)->z;
                p1+=4;cp1++;                
              }
            GArray *controls=control_points_from_coords_3d(one_lat);
            struct controls3d *c1=&g_array_index(controls, struct controls3d, 0);
            for(j=0;j<rings*2;j++)
              {
                (p2+1)->x=c1->x1;
                (p2+1)->y=c1->y1;
                (p2+1)->z=c1->z1;
                (p2+2)->x=c1->x2;
                (p2+2)->y=c1->y2;
                (p2+2)->z=c1->z2;
                p2+=4;c1++;                
              }
            g_array_free(controls, TRUE);            
          }
        g_array_free(one_lat, TRUE);
     }
  }
void smooth_solid_bezier_sphere_longitude(gint cache_id, gint rings, gint points_per_ring, gint shape_cache_index)
  {
    gint i=0;
    gint j=0;
    struct shape *s1=NULL;
    if(shape_cache_index==-1) s1=get_drawing_from_shape_cache(SOLID_BEZIER_SPHERE, rings, points_per_ring);
    else s1=get_drawing_from_shape_cache_index(SOLID_BEZIER_SPHERE, shape_cache_index);
    
    if(s1!=NULL)
      {
        GArray *one_long=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), points_per_ring/2+1);
        g_array_set_size(one_long, points_per_ring/2+1);
        struct bezier_quad *p1=&g_array_index(s1->s, struct bezier_quad, 0);
        struct bezier_quad *p2=&g_array_index(s1->s, struct bezier_quad, 0);
        struct point3d *rp1=NULL;
        for(i=0;i<rings*2;i++)
          { 
            p1=&g_array_index(s1->s, struct bezier_quad, i);
            rp1=&g_array_index(one_long, struct point3d, 0);
            //Start at one end of the sphere.
            for(j=0;j<points_per_ring/2;j++)
              {
                rp1->x=(p1->x1);
                rp1->y=(p1->y1);
                rp1->z=(p1->z1); 
                rp1++;p1+=rings*2; 
              }
            //End of arc.
            p1-=rings*2;
            rp1->x=p1->x2;
            rp1->y=p1->y2;
            rp1->z=p1->z2;

            GArray *controls=control_points_from_coords_3d(one_long);
            struct controls3d *c1=&g_array_index(controls, struct controls3d, 0);
            //Add the control points.
            p1=&g_array_index(s1->s, struct bezier_quad, i);            
            for(j=0;j<points_per_ring/2;j++)
              {
                p1->bx1=c1->x1;
                p1->by1=c1->y1;
                p1->bz1=c1->z1; 
                p1->bx2=c1->x2;
                p1->by2=c1->y2;
                p1->bz2=c1->z2; 
                p1+=rings*2;c1++; 
              }
               
            //Update adjacent longitude quads.
            if(i>0)
              {
                p1=&g_array_index(s1->s, struct bezier_quad, i); 
                p2=&g_array_index(s1->s, struct bezier_quad, i-1);
                for(j=0;j<points_per_ring/2;j++)
                  {
                    p2->bx6=p1->bx1;
                    p2->by6=p1->by1;
                    p2->bz6=p1->bz1; 
                    p2->bx5=p1->bx2;
                    p2->by5=p1->by2;
                    p2->bz5=p1->bz2; 
                    p1+=rings*2;p2+=rings*2;
                  }
               }
              
            //Update arc connections.
            if(i==rings-1)
              {
                p1=&g_array_index(s1->s, struct bezier_quad, 0); 
                p2=&g_array_index(s1->s, struct bezier_quad, 2*rings-1);
                for(j=0;j<points_per_ring/2;j++)
                  {
                    p2->bx6=p1->bx1;
                    p2->by6=p1->by1;
                    p2->bz6=p1->bz1; 
                    p2->bx5=p1->bx2;
                    p2->by5=p1->by2;
                    p2->bz5=p1->bz2; 
                    p1+=rings*2;p2+=rings*2;
                  } 
               }
            g_array_free(controls, TRUE);
          } 
        g_array_free(one_long, TRUE);
     } 
  }
void smooth_solid_bezier_sphere_latitude(gint cache_id, gint rings, gint points_per_ring, gint shape_cache_index)
  {
    gint i=0;
    gint j=0;
    struct shape *s1=NULL;
    if(shape_cache_index==-1) s1=get_drawing_from_shape_cache(SOLID_BEZIER_SPHERE, rings, points_per_ring);
    else s1=get_drawing_from_shape_cache_index(SOLID_BEZIER_SPHERE, shape_cache_index);
    
    if(s1!=NULL)
      {
        GArray *one_lat=g_array_sized_new(FALSE, TRUE, sizeof(struct point3d), 2*rings+1);
        g_array_set_size(one_lat, 2*rings+1);
        struct bezier_quad *p1=&g_array_index(s1->s, struct bezier_quad, 0);
        struct bezier_quad *p2=&g_array_index(s1->s, struct bezier_quad, 0);
        struct point3d *cp1=NULL;
        gint len2=points_per_ring/2-1;
        for(i=0;i<len2;i++)
          {
            p1=&g_array_index(s1->s, struct bezier_quad, (i+1)*rings*2);
            cp1=&g_array_index(one_lat, struct point3d, 0); 
            for(j=0;j<rings;j++)
              {
                cp1->x=p1->x1;
                cp1->y=p1->y1;
                cp1->z=p1->z1;
                p1++;cp1++;                
              }
            for(j=0;j<rings;j++)
              {
                cp1->x=p1->x1;
                cp1->y=p1->y1;
                cp1->z=p1->z1;
                p1++;cp1++;                
              }
            p1=&g_array_index(s1->s, struct bezier_quad, (i+1)*rings*2);
            cp1->x=p1->x1;
            cp1->y=p1->y1;
            cp1->z=p1->z1;

            GArray *controls=control_points_from_coords_3d(one_lat);
            struct controls3d *c1=&g_array_index(controls, struct controls3d, 0);
            p1=&g_array_index(s1->s, struct bezier_quad, (i)*rings*2);
            p2=&g_array_index(s1->s, struct bezier_quad, (i+1)*rings*2);
            //Update control values.
            for(j=0;j<rings*2;j++)
              {
                (p1->bx3)=c1->x1;
                (p1->by3)=c1->y1;
                (p1->bz3)=c1->z1;
                (p1->bx4)=c1->x2;
                (p1->by4)=c1->y2;
                (p1->bz4)=c1->z2;
                (p2->bx8)=c1->x1;
                (p2->by8)=c1->y1;
                (p2->bz8)=c1->z1;
                (p2->bx7)=c1->x2;
                (p2->by7)=c1->y2;
                (p2->bz7)=c1->z2;
                p1++;p2++;c1++;                
              }
            g_array_free(controls, TRUE);            
          }
        g_array_free(one_lat, TRUE);
     }
  }
