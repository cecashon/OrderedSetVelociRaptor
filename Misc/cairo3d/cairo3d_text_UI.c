
/*
  Some more 3d text testing. Draw a single line 3d string. Output to .pdf, .svg and .ps.
This is tested with 

    char *font="/usr/share/fonts/truetype/freefont/FreeSansBold.ttf"; 

which is in main.

  If the drawing has a mesh gradient it will be rasterized. Mesh gradients aren't supported in svg1.0.
Linear gradients are supported so the linear gradient doensn't get rasterized but saved as text in
svg.

  gcc -Wall cairo3d_text.c cairo3d_text_UI.c -o shadow_font `pkg-config --cflags --libs gtk+-3.0 freetype2` -lm

  Tested on Ubuntu20.04 and GTK3.24

  C. Eric Cashon
*/

#include<gtk/gtk.h>
#include<math.h>
#include<cairo-svg.h>
#include<cairo-pdf.h>
#include<cairo-ps.h>
#include"cairo3d_text.h"

/*
  Used for calculating points in .svg, .pdf and .ps output.
  Why 90. This works on the test computer. When a .svg is saved and then opened, the pixels
match the original drawing. Rsvg rsvg_handle_set_dpi() notes "Common values are 75, 90,
and 300 DPI." which it looks like 90 is being used as default to open the picture. 
  The ppi might need to be changed for matching ouput on different computers.
*/
static gdouble ppi=90.0;
//Some settings for the UI.
static gboolean font_set=FALSE;
static gint drawing_id=0;
static gdouble pitch=0.0;
static gdouble roll=0.0;
static gdouble yaw=0.0;
static gdouble scale=1.0;
static gdouble perspective=0.0;
static guint tick_id=0;
static gboolean frame_rate=FALSE;
static gboolean time_it=FALSE;
static gdouble line_width=4.0;
static gboolean clip_line=TRUE;
//Shadow plane rotation.
static gdouble slider_pitch2=0.0;
static gdouble slider_roll2=0.0;
static gdouble slider_yaw2=0.0;
static gdouble slider_distance=1.0;
static gdouble light[3]={0.0, -500.0, 500.0};
//Font Options.
static gdouble point_size=100.0;
static gdouble font_scale=0.0;
static gdouble spacing=0.0;
static gdouble translate_x=0.0;
static gdouble translate_y=0.0;
static gdouble layout_width=450.0;
static gdouble layout_height=450.0;
static gdouble t3d_front_color[4]={0.0, 1.0, 0.0, 1.0};
static gdouble t3d_side_color1[4]={0.0, 1.0, 0.0, 0.5};
static gdouble t3d_side_color2[4]={0.0, 1.0, 0.0, 0.0};
static gdouble t3d_back_color[4]={0.0, 1.0, 0.0, 1.0};
static gdouble t2d_front_color[4]={1.0, 0.0, 1.0, 1.0};
static gdouble t2d_side_color1[4]={1.0, 0.0, 1.0, 0.5};
static gdouble t2d_side_color2[4]={1.0, 0.0, 1.0, 0.0};
static gdouble t2d_back_color[4]={1.0, 0.0, 1.0, 1.0};
static gdouble line_color[4]={0.0, 0.0, 0.0, 1.0};
static gdouble window_color[4]={1.0, 1.0, 1.0, 1.0};
static gboolean show_bounding_box=FALSE;
//For t3d font.
static gdouble extrude=40.0;
//For t2d font.
static gdouble depth=40.0;
static gboolean use_advance=TRUE;
//Draw the front facing quads for the blur or all the quads.
static gboolean with_normal=FALSE;

//Some parts to pass to functions.
struct some_parts{
  GtkWidget *window;
  GtkWidget *da;
  FT_Library library;
  FT_Face face;
  GtkWidget *entry;
  struct text3d *t3d;
  struct text2d *t2d;
};

//The UI part.
static gboolean set_font(FT_Library library, FT_Face *face, const gchar *font);
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data);
static void set_drawing_id(GtkComboBox *combo, gpointer data);
static void set_animate_drawing(GtkComboBox *combo, struct some_parts *parts);
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_extrude(GtkRange *range, GtkScrollType scroll, gdouble value, struct some_parts *parts);
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_text(GtkWidget *button, struct some_parts *parts);
static void free_and_set_new_text(struct some_parts *parts);
static void show_frame_rate(GtkToggleButton *check_frame, gpointer data);
static void show_drawing_time(GtkToggleButton *check_time, gpointer data);
static void set_pitch2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_roll2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_yaw2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void set_distance(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data);
static void x_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void y_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void z_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void layout_width_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void layout_height_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void line_width_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void set_clip_line(GtkToggleButton *check_line, gpointer data);
static void set_normal(GtkToggleButton *check_normal, gpointer data);
static void select_new_font(GtkWidget *button, struct some_parts *parts);
static void spacing_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void translate_x_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void translate_y_spin_changed(GtkSpinButton *spin_button, gpointer data);
static void set_t3d_color(GtkWidget *button, gpointer *data);
static void set_t2d_color(GtkWidget *button, gpointer *data);
static void set_line_color(GtkWidget *button, gpointer *data);
static void set_back_window_color(GtkWidget *button, gpointer *data);
static void set_advance(GtkToggleButton *advance_check, gpointer data);
static void set_bounding_box(GtkToggleButton *bounding_box_check, gpointer data);
static gboolean animate_drawing(GtkWidget *da, GdkFrameClock *frame_clock, struct some_parts *parts);
//Drawing part.
static gboolean draw_main(GtkWidget *da, cairo_t *cr, struct some_parts *parts);
static gchar* save_file(struct some_parts *parts, gint surface_type);
static void draw_svg(GtkWidget *widget, struct some_parts *parts);
static void draw_pdf(GtkWidget *widget, struct some_parts *parts);
static void draw_ps(GtkWidget *widget, struct some_parts *parts);
static void draw_png(GtkWidget *widget, struct some_parts *parts);
static void draw_surface(cairo_t *cr, struct some_parts *parts, gint surface_type);
static void draw_text3d(struct text3d *t3d,  cairo_t *cr, gdouble qrs[9]);
static void draw_char(cairo_t *cr, GArray *path1, GArray *path2, gdouble front_rgba[4], gdouble side1_rgba[4], gdouble side2_rgba[4], gdouble back_rgba[4]);
static void draw_bounding_box_t3d(struct text3d *t3d,  cairo_t *cr, gdouble qrs[9]);
static void draw_bounding_box_t2d(struct text2d *t2d,  cairo_t *cr, gdouble qrs[9]);
static void draw_shadow_text(struct text2d *t2d,  cairo_t *cr, gdouble qrs[9], gdouble light_position[3], gdouble plane_distance);
static void draw_shadow_char(cairo_t *cr, GArray *path1, GArray *shadow, gdouble front_rgba[4], gdouble side1_rgba[4], gdouble side2_rgba[4], gdouble back_rgba[4]);
static void extrude_text3d(struct text3d *t3d, gdouble z_value);
static void perspective_text3d(struct text3d *t3d, gdouble value);
//About dialog.
static void about_dialog(GtkWidget *widget, struct some_parts *parts);

int main(int argc, char **argv)
  { 
    FT_Error error;

    //Starting font.
    char *font="/usr/share/fonts/truetype/freefont/FreeSansBold.ttf";

    gtk_init(&argc, &argv);

    struct some_parts parts={.window=NULL, .da=NULL, .library=NULL, .face=NULL, .entry=NULL, .t3d=NULL, .t2d=NULL};

    error=FT_Init_FreeType(&parts.library);
    if(error) printf("Init_FreeType Error\n");

    //Set the font face.
    font_set=set_font(parts.library, &parts.face, font);

    //The cairo3d text and shadow text.
    if(font_set)
      {
        GString *string=g_string_new("cairo3d");
        parts.t3d=text3d_new(parts.face, string, font_scale, spacing, extrude, use_advance);
        parts.t2d=text2d_new(parts.face, string, font_scale, spacing, depth, use_advance);
        g_string_free(string, TRUE);
      }
    else g_print("Default font didn't open.\n");

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Shadow Font");
    gtk_window_set_default_size(GTK_WINDOW(window), 1000, 650);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_widget_set_app_paintable(window, TRUE);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    parts.window=window;

    //Try to set transparency of main window.
    GdkScreen *screen=gtk_widget_get_screen(window); 
    if(gdk_screen_is_composited(screen))
      {
        GdkVisual *visual=gdk_screen_get_rgba_visual(screen);
        gtk_widget_set_visual(window, visual);
      }
    else
      {
        g_print("Can't set window transparency.\n");
      } 

    GtkWidget *da=gtk_drawing_area_new();
    gtk_widget_set_hexpand(da, TRUE);
    gtk_widget_set_vexpand(da, TRUE);
    parts.da=da;

    GtkWidget *da_scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(da_scroll, TRUE);
    gtk_widget_set_vexpand(da_scroll, TRUE);
    gtk_widget_set_size_request(da, 1000, 1000);
    gtk_container_add(GTK_CONTAINER(da_scroll), da);

    GtkWidget *combo_animate=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_animate, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_animate), 0, "1", "Draw Text");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_animate), 1, "2", "Animate Text");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_animate), 0);

    GtkWidget *combo_drawing=gtk_combo_box_text_new();
    gtk_widget_set_hexpand(combo_drawing, FALSE);
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 0, "1", "Draw Blur");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 1, "2", "Draw Lines");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 2, "3", "Draw Stencil");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 3, "4", "Draw Fill");
    gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(combo_drawing), 4, "5", "Draw Shadow");
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_drawing), 0);
    g_signal_connect(combo_drawing, "changed", G_CALLBACK(set_drawing_id), da);

    GtkWidget *pitch_label=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label, FALSE);

    GtkWidget *roll_label=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label, FALSE);

    GtkWidget *yaw_label=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label, FALSE);

    GtkWidget *extrude_label=gtk_label_new("Extrude");
    gtk_widget_set_hexpand(extrude_label, FALSE);

    GtkWidget *scale_label=gtk_label_new("Scale");
    gtk_widget_set_hexpand(scale_label, FALSE);

    GtkWidget *perspective_label=gtk_label_new("Perspective");
    gtk_widget_set_hexpand(perspective_label, FALSE);

    GtkWidget *pitch_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(pitch_slider, TRUE);
    gtk_widget_set_hexpand(pitch_slider, FALSE);
    g_signal_connect(pitch_slider, "change-value", G_CALLBACK(set_pitch), da);

    GtkWidget *roll_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(roll_slider, TRUE);
    gtk_widget_set_hexpand(roll_slider, FALSE);
    g_signal_connect(roll_slider, "change-value", G_CALLBACK(set_roll), da);

    GtkWidget *yaw_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_vexpand(yaw_slider, TRUE);
    gtk_widget_set_hexpand(yaw_slider, FALSE);
    g_signal_connect(yaw_slider, "change-value", G_CALLBACK(set_yaw), da); 

    GtkWidget *extrude_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 100.0, 1.0);
    gtk_widget_set_vexpand(extrude_slider, TRUE);
    gtk_widget_set_hexpand(extrude_slider, FALSE);
    gtk_range_set_value(GTK_RANGE(extrude_slider), 40);
    g_signal_connect(extrude_slider, "change-value", G_CALLBACK(set_extrude), &parts);  

    GtkWidget *scale_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 2.0, 0.01);
    gtk_widget_set_vexpand(scale_slider, TRUE);
    gtk_widget_set_hexpand(scale_slider, FALSE);
    gtk_range_set_value(GTK_RANGE(scale_slider), 1.0);
    g_signal_connect(scale_slider, "change-value", G_CALLBACK(set_scale), da);

    GtkWidget *perspective_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 1.0, 0.01);
    gtk_widget_set_vexpand(perspective_slider, TRUE);
    gtk_widget_set_hexpand(perspective_slider, FALSE);
    g_signal_connect(perspective_slider, "change-value", G_CALLBACK(set_perspective), da);

    GtkWidget *entry=gtk_entry_new();
    gtk_widget_set_hexpand(entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(entry), "cairo3d");
    parts.entry=entry;

    g_signal_connect(da, "draw", G_CALLBACK(draw_main), &parts);
    g_signal_connect(combo_animate, "changed", G_CALLBACK(set_animate_drawing), &parts);
    
    GtkWidget *button=gtk_button_new_with_label("Set Text");
    gtk_widget_set_hexpand(button, FALSE);
    g_signal_connect(button, "clicked", G_CALLBACK(set_text), &parts);

    GtkWidget *check_frame=gtk_check_button_new_with_label("Show Frame Rate");
    gtk_widget_set_halign(check_frame, GTK_ALIGN_CENTER);
    g_signal_connect(check_frame, "toggled", G_CALLBACK(show_frame_rate), da);

    GtkWidget *check_time=gtk_check_button_new_with_label("Time Drawing");
    gtk_widget_set_halign(check_time, GTK_ALIGN_CENTER);
    g_signal_connect(check_time, "toggled", G_CALLBACK(show_drawing_time), da);
   
    GtkWidget *grid1=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid1), 15);
    gtk_grid_set_column_spacing(GTK_GRID(grid1), 5);
    gtk_grid_set_row_spacing(GTK_GRID(grid1), 8);
    gtk_grid_attach(GTK_GRID(grid1), combo_animate, 1, 0, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), combo_drawing, 1, 1, 4, 1);
    gtk_grid_attach(GTK_GRID(grid1), entry, 1, 2, 3, 1);
    gtk_grid_attach(GTK_GRID(grid1), button, 4, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), pitch_label, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_label, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_label, 2, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), extrude_label, 3, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale_label, 4, 3, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid1), perspective_label, 5, 3, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid1), pitch_slider, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), roll_slider, 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), yaw_slider, 2, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), extrude_slider, 3, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid1), scale_slider, 4, 4, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid1), perspective_slider, 5, 4, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid1), check_frame, 1, 5, 3, 1);
    gtk_grid_attach(GTK_GRID(grid1), check_time, 1, 6, 3, 1);

    GtkWidget *pitch_label2=gtk_label_new("Pitch");
    gtk_widget_set_hexpand(pitch_label2, FALSE);

    GtkWidget *roll_label2=gtk_label_new("Roll");
    gtk_widget_set_hexpand(roll_label2, FALSE);

    GtkWidget *yaw_label2=gtk_label_new("Yaw");
    gtk_widget_set_hexpand(yaw_label2, FALSE);

    GtkWidget *distance_label=gtk_label_new("Distance");
    gtk_widget_set_hexpand(distance_label, FALSE);

    GtkWidget *pitch_slider2=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_hexpand(pitch_slider2, FALSE);
    gtk_widget_set_vexpand(pitch_slider2, TRUE);

    GtkWidget *roll_slider2=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_hexpand(roll_slider2, FALSE);
    gtk_widget_set_vexpand(roll_slider2, TRUE);

    GtkWidget *yaw_slider2=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.0, 360.0, 1.0);
    gtk_widget_set_hexpand(yaw_slider2, FALSE);
    gtk_widget_set_vexpand(yaw_slider2, TRUE);

    GtkWidget *distance_slider=gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0.3, 2.0, 0.1);
    gtk_widget_set_hexpand(distance_slider, FALSE);
    gtk_widget_set_vexpand(distance_slider, TRUE);
    gtk_range_set_value(GTK_RANGE(distance_slider), 1.0);

    g_signal_connect(pitch_slider2, "change-value", G_CALLBACK(set_pitch2), da);
    g_signal_connect(roll_slider2, "change-value", G_CALLBACK(set_roll2), da);
    g_signal_connect(yaw_slider2, "change-value", G_CALLBACK(set_yaw2), da);
    g_signal_connect(distance_slider, "change-value", G_CALLBACK(set_distance), da);

    GtkAdjustment *x_adj=gtk_adjustment_new(0.0, -2000.0, 2000.0, 100.0, 0.0, 0.0);
    GtkAdjustment *y_adj=gtk_adjustment_new(-500.0, -2000.0, 2000.0, 100.0, 0.0, 0.0);
    GtkAdjustment *z_adj=gtk_adjustment_new(500.0, -2000.0, 2000.0, 100.0, 0.0, 0.0);     

    GtkWidget *light_label=gtk_label_new("Light Position xyz");
    gtk_widget_set_hexpand(light_label, FALSE);

    GtkWidget *x_spin=gtk_spin_button_new(x_adj, 0.0, 1);
    gtk_widget_set_hexpand(x_spin, FALSE);
    g_signal_connect(x_spin, "value-changed", G_CALLBACK(x_spin_changed), da);

    GtkWidget *y_spin=gtk_spin_button_new(y_adj, -500.0, 1);
    g_signal_connect(y_spin, "value-changed", G_CALLBACK(y_spin_changed), da);

    GtkWidget *z_spin=gtk_spin_button_new(z_adj, 500.0, 1);
    gtk_widget_set_hexpand(y_spin, FALSE);
    g_signal_connect(z_spin, "value-changed", G_CALLBACK(z_spin_changed), da);

    GtkWidget *grid2=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid2), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid2), 8); 
    gtk_grid_attach(GTK_GRID(grid2), pitch_label2, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), roll_label2, 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), yaw_label2, 2, 0, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid2), distance_label, 3, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), pitch_slider2, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), roll_slider2, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), yaw_slider2, 2, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), distance_slider, 3, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), light_label, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid2), x_spin, 0, 3, 2, 1);
    gtk_grid_attach(GTK_GRID(grid2), y_spin, 2, 3, 2, 1);
    gtk_grid_attach(GTK_GRID(grid2), z_spin, 0, 4, 2, 1);

    GtkAdjustment *spacing_adj=gtk_adjustment_new(0.0, 0.0, 20.0, 1.0, 0.0, 0.0);
    GtkAdjustment *translate_x_adj=gtk_adjustment_new(0.0, -500.0, 500.0, 10.0, 0.0, 0.0);
    GtkAdjustment *translate_y_adj=gtk_adjustment_new(0.0, -500.0, 500.0, 10.0, 0.0, 0.0);
    
    GtkWidget *spacing_label=gtk_label_new("Spacing");
    gtk_widget_set_hexpand(spacing_label, FALSE);
    GtkWidget *spacing_spin=gtk_spin_button_new(spacing_adj, 0.0, 1);
    gtk_widget_set_hexpand(spacing_spin, FALSE);
    g_signal_connect(spacing_spin, "value-changed", G_CALLBACK(spacing_spin_changed), NULL);    

    GtkWidget *update_font=gtk_button_new_with_label("Update Font");
    gtk_widget_set_hexpand(update_font, FALSE);
    g_signal_connect(update_font, "clicked", G_CALLBACK(set_text), &parts);

    GtkWidget *advance_check=gtk_check_button_new_with_label("Use Advance");
    gtk_widget_set_halign(advance_check, GTK_ALIGN_CENTER);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(advance_check), TRUE);
    g_signal_connect(advance_check, "toggled", G_CALLBACK(set_advance), NULL);

    GtkWidget *translate_x_label=gtk_label_new("Translate x");
    gtk_widget_set_hexpand(translate_x_label, FALSE);
    GtkWidget *translate_x_spin=gtk_spin_button_new(translate_x_adj, 0.0, 1);
    gtk_widget_set_hexpand(translate_x_spin, FALSE);
    g_signal_connect(translate_x_spin, "value-changed", G_CALLBACK(translate_x_spin_changed), da); 

    GtkWidget *translate_y_label=gtk_label_new("Translate y");
    gtk_widget_set_hexpand(translate_y_label, FALSE);
    GtkWidget *translate_y_spin=gtk_spin_button_new(translate_y_adj, 0.0, 1);
    gtk_widget_set_hexpand(translate_y_spin, FALSE);
    g_signal_connect(translate_y_spin, "value-changed", G_CALLBACK(translate_y_spin_changed), da);

    GtkWidget *bounding_box_check=gtk_check_button_new_with_label("Show Bounding Box");
    gtk_widget_set_halign(bounding_box_check, GTK_ALIGN_CENTER);
    g_signal_connect(bounding_box_check, "toggled", G_CALLBACK(set_bounding_box), da);

    GtkAdjustment *layout_width_adj=gtk_adjustment_new(450.0, 1.0, 2000.0, 50.0, 0.0, 0.0);
    GtkAdjustment *layout_height_adj=gtk_adjustment_new(450.0, 1.0, 2000.0, 50.0, 0.0, 0.0);
    GtkAdjustment *line_width_adj=gtk_adjustment_new(4.0, 0.0, 20.0, 1.0, 0.0, 0.0); 

    GtkWidget *layout_width_label=gtk_label_new("Layout Width");
    gtk_widget_set_hexpand(layout_width_label, FALSE);
    GtkWidget *layout_width_spin=gtk_spin_button_new(layout_width_adj, 450.0, 1);
    gtk_widget_set_hexpand(layout_width_spin, FALSE);
    g_signal_connect(layout_width_spin, "value-changed", G_CALLBACK(layout_width_spin_changed), da);

    GtkWidget *layout_height_label=gtk_label_new("Layout Height");
    gtk_widget_set_hexpand(layout_height_label, FALSE);
    GtkWidget *layout_height_spin=gtk_spin_button_new(layout_height_adj, 450.0, 1);
    gtk_widget_set_hexpand(layout_height_spin, FALSE);
    g_signal_connect(layout_height_spin, "value-changed", G_CALLBACK(layout_height_spin_changed), da);

    GtkWidget *line_width_label=gtk_label_new("Line Width");
    gtk_widget_set_hexpand(line_width_label, FALSE);
    GtkWidget *line_width_spin=gtk_spin_button_new(line_width_adj, 450.0, 1);
    gtk_widget_set_hexpand(line_width_spin, FALSE);
    g_signal_connect(line_width_spin, "value-changed", G_CALLBACK(line_width_spin_changed), da);

    GtkWidget *clip_line_check=gtk_check_button_new_with_label("Clip Line");
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(clip_line_check), TRUE);
    gtk_widget_set_halign(clip_line_check, GTK_ALIGN_CENTER);
    g_signal_connect(clip_line_check, "toggled", G_CALLBACK(set_clip_line), da);

    GtkWidget *select_font=gtk_button_new_with_label("Select Font File");
    gtk_widget_set_hexpand(select_font, FALSE);
    g_signal_connect(select_font, "clicked", G_CALLBACK(select_new_font), &parts);
    
    GtkWidget *normal_check=gtk_check_button_new_with_label("Draw Blur Front Quads Only");
    gtk_widget_set_halign(normal_check, GTK_ALIGN_CENTER);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(normal_check), FALSE);
    g_signal_connect(normal_check, "toggled", G_CALLBACK(set_normal), da);

    GtkWidget *grid3=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid3), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid3), 8);
    gtk_grid_attach(GTK_GRID(grid3), spacing_label, 0, 0, 1, 1);    
    gtk_grid_attach(GTK_GRID(grid3), spacing_spin, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), advance_check, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), update_font, 0, 2, 2, 1); 
    gtk_grid_attach(GTK_GRID(grid3), translate_x_label, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), translate_y_label, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), translate_x_spin, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), translate_y_spin, 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), bounding_box_check, 0, 5, 2, 1);
    gtk_grid_attach(GTK_GRID(grid3), layout_width_label, 0, 6, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), layout_height_label, 1, 6, 1, 1); 
    gtk_grid_attach(GTK_GRID(grid3), layout_width_spin, 0, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), layout_height_spin, 1, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), line_width_label, 0, 8, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), line_width_spin, 0, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), clip_line_check, 1, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid3), select_font, 0, 10, 2, 1);  
    gtk_grid_attach(GTK_GRID(grid3), normal_check, 0, 11, 2, 1);  

    GtkWidget *t3d_color_label=gtk_label_new("3d Font Color");
    gtk_widget_set_hexpand(t3d_color_label, FALSE);

    GtkWidget *t3d_front_color_label=gtk_label_new("Front Color");
    gtk_widget_set_hexpand(t3d_front_color_label, FALSE);
    GtkWidget *t3d_front_color_entry=gtk_entry_new();
    gtk_widget_set_hexpand(t3d_front_color_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(t3d_front_color_entry), "rgba(0, 255, 0, 1.0)");

    GtkWidget *t3d_side_color1_label=gtk_label_new("Side Color1");
    gtk_widget_set_hexpand(t3d_side_color1_label, FALSE);
    GtkWidget *t3d_side_color1_entry=gtk_entry_new();
    gtk_widget_set_hexpand(t3d_side_color1_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(t3d_side_color1_entry), "rgba(0, 255, 0, 0.5)");

    GtkWidget *t3d_side_color2_label=gtk_label_new("Side Color2");
    gtk_widget_set_hexpand(t3d_side_color2_label, FALSE);
    GtkWidget *t3d_side_color2_entry=gtk_entry_new();
    gtk_widget_set_hexpand(t3d_side_color2_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(t3d_side_color2_entry), "rgba(0, 255, 0, 0.0)");

    GtkWidget *t3d_back_color_label=gtk_label_new("Back Color");
    gtk_widget_set_hexpand(t3d_back_color_label, FALSE);
    GtkWidget *t3d_back_color_entry=gtk_entry_new();
    gtk_widget_set_hexpand(t3d_back_color_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(t3d_back_color_entry), "rgba(0, 255, 0, 1.0)");

    GtkWidget *t3d_color_button=gtk_button_new_with_label("Update 3d Font Color");
    gtk_widget_set_hexpand(t3d_color_button, FALSE);
    gpointer w1[]={da, t3d_front_color_entry, t3d_side_color1_entry, t3d_side_color2_entry, t3d_back_color_entry};
    g_signal_connect(t3d_color_button, "clicked", G_CALLBACK(set_t3d_color), w1);

    GtkWidget *t2d_color_label=gtk_label_new("Shadow Font Color");
    gtk_widget_set_hexpand(t2d_color_label, FALSE);

    GtkWidget *t2d_front_color_label=gtk_label_new("Front Color");
    gtk_widget_set_hexpand(t2d_front_color_label, FALSE);
    GtkWidget *t2d_front_color_entry=gtk_entry_new();
    gtk_widget_set_hexpand(t2d_front_color_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(t2d_front_color_entry), "rgba(255, 0, 255, 1.0)");

    GtkWidget *t2d_side_color1_label=gtk_label_new("Side Color1");
    gtk_widget_set_hexpand(t2d_side_color1_label, FALSE);
    GtkWidget *t2d_side_color1_entry=gtk_entry_new();
    gtk_widget_set_hexpand(t2d_side_color1_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(t2d_side_color1_entry), "rgba(255, 0, 255, 0.5)");

    GtkWidget *t2d_side_color2_label=gtk_label_new("Side Color2");
    gtk_widget_set_hexpand(t2d_side_color2_label, FALSE);
    GtkWidget *t2d_side_color2_entry=gtk_entry_new();
    gtk_widget_set_hexpand(t2d_side_color2_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(t2d_side_color2_entry), "rgba(255, 0, 255, 0.0)");

    GtkWidget *t2d_back_color_label=gtk_label_new("Back Color");
    gtk_widget_set_hexpand(t2d_back_color_label, FALSE);
    GtkWidget *t2d_back_color_entry=gtk_entry_new();
    gtk_widget_set_hexpand(t2d_back_color_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(t2d_back_color_entry), "rgba(255, 0, 255, 1.0)");

    GtkWidget *t2d_color_button=gtk_button_new_with_label("Update Shadow Font Color");
    gtk_widget_set_hexpand(t2d_color_button, FALSE);
    gpointer w2[]={da, t2d_front_color_entry, t2d_side_color1_entry, t2d_side_color2_entry, t2d_back_color_entry};
    g_signal_connect(t2d_color_button, "clicked", G_CALLBACK(set_t2d_color), w2);

    GtkWidget *line_color_label=gtk_label_new("Line Color");
    gtk_widget_set_hexpand(line_color_label, FALSE);
    GtkWidget *line_color_entry=gtk_entry_new();
    gtk_widget_set_hexpand(line_color_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(line_color_entry), "rgba(0, 0, 0, 1.0)");

    GtkWidget *line_color_button=gtk_button_new_with_label("Update Line Color");
    gtk_widget_set_hexpand(line_color_button, FALSE);
    gpointer w3[]={da, line_color_entry};
    g_signal_connect(line_color_button, "clicked", G_CALLBACK(set_line_color), w3);

    GtkWidget *back_window_label=gtk_label_new("Back Window Color");
    gtk_widget_set_hexpand(back_window_label, FALSE);
    GtkWidget *back_window_entry=gtk_entry_new();
    gtk_widget_set_hexpand(back_window_entry, FALSE);
    gtk_entry_set_text(GTK_ENTRY(back_window_entry), "rgba(255, 255, 255, 1.0)");

    GtkWidget *back_window_button=gtk_button_new_with_label("Update Back Window Color");
    gtk_widget_set_hexpand(back_window_button, FALSE);
    gpointer w4[]={da, back_window_entry};
    g_signal_connect(back_window_button, "clicked", G_CALLBACK(set_back_window_color), w4);

    GtkWidget *grid4=gtk_grid_new();
    gtk_container_set_border_width(GTK_CONTAINER(grid4), 15);
    gtk_grid_set_row_spacing(GTK_GRID(grid4), 8);
    gtk_grid_attach(GTK_GRID(grid4), t3d_color_label, 0, 0, 2, 1);
    gtk_grid_attach(GTK_GRID(grid4), t3d_front_color_label, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), t3d_side_color1_label, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), t3d_front_color_entry, 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), t3d_side_color1_entry, 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), t3d_side_color2_label, 0, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), t3d_back_color_label, 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), t3d_side_color2_entry, 0, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), t3d_back_color_entry, 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), t3d_color_button, 0, 5, 2, 1);
    gtk_grid_attach(GTK_GRID(grid4), t2d_color_label, 0, 6, 2, 1);
    gtk_grid_attach(GTK_GRID(grid4), t2d_front_color_label, 0, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), t2d_side_color1_label, 1, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), t2d_front_color_entry, 0, 8, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), t2d_side_color1_entry, 1, 8, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), t2d_side_color2_label, 0, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), t2d_back_color_label, 1, 9, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), t2d_side_color2_entry, 0, 10, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), t2d_back_color_entry, 1, 10, 1, 1);
    gtk_grid_attach(GTK_GRID(grid4), t2d_color_button, 0, 11, 2, 1); 
    gtk_grid_attach(GTK_GRID(grid4), line_color_label, 0, 12, 2, 1);
    gtk_grid_attach(GTK_GRID(grid4), line_color_entry, 0, 13, 2, 1);
    gtk_grid_attach(GTK_GRID(grid4), line_color_button, 0, 14, 2, 1);   
    gtk_grid_attach(GTK_GRID(grid4), back_window_label, 0, 15, 2, 1);
    gtk_grid_attach(GTK_GRID(grid4), back_window_entry, 0, 16, 2, 1);
    gtk_grid_attach(GTK_GRID(grid4), back_window_button, 0, 17, 2, 1);   

    GtkWidget *menu1=gtk_menu_new();
    GtkWidget *menu1item1=gtk_menu_item_new_with_label("Save as .svg");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item1);
    GtkWidget *menu1item2=gtk_menu_item_new_with_label("Save as .pdf");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item2);
    GtkWidget *menu1item3=gtk_menu_item_new_with_label("Save as .ps");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item3);
    GtkWidget *menu1item4=gtk_menu_item_new_with_label("Save as .png");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu1), menu1item4);
    GtkWidget *title1=gtk_menu_item_new_with_label("Save Drawing");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(title1), menu1);

    GtkWidget *menu2=gtk_menu_new();
    GtkWidget *menu2item1=gtk_menu_item_new_with_label("Shadow Font");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu2), menu2item1);
    GtkWidget *title2=gtk_menu_item_new_with_label("About");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(title2), menu2);

    GtkWidget *menu_bar=gtk_menu_bar_new();
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), title1);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), title2);

    g_signal_connect(menu1item1, "activate", G_CALLBACK(draw_svg), &parts);
    g_signal_connect(menu1item2, "activate", G_CALLBACK(draw_pdf), &parts);
    g_signal_connect(menu1item3, "activate", G_CALLBACK(draw_ps), &parts);
    g_signal_connect(menu1item4, "activate", G_CALLBACK(draw_png), &parts);
    g_signal_connect(menu2item1, "activate", G_CALLBACK(about_dialog), &parts);

    GtkWidget *scroll1=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll1, TRUE);
    gtk_widget_set_vexpand(scroll1, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll1), grid1);

    GtkWidget *scroll2=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll2, TRUE);
    gtk_widget_set_vexpand(scroll2, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll2), grid2);

    GtkWidget *scroll3=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll3, TRUE);
    gtk_widget_set_vexpand(scroll3, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll3), grid3);

    GtkWidget *scroll4=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll4, TRUE);
    gtk_widget_set_vexpand(scroll4, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll4), grid4);

    GtkWidget *nb_label1=gtk_label_new("Text3d");
    GtkWidget *nb_label2=gtk_label_new("Shadow Plane"); 
    GtkWidget *nb_label3=gtk_label_new("Font Options");
    GtkWidget *nb_label4=gtk_label_new("Font Colors");     
    GtkWidget *notebook=gtk_notebook_new();
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll1, nb_label1);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll2, nb_label2);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll3, nb_label3);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scroll4, nb_label4);

    GtkWidget *scroll5=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scroll5, TRUE);
    gtk_widget_set_vexpand(scroll5, TRUE);
    gtk_container_add(GTK_CONTAINER(scroll5), notebook);

    GtkWidget *paned1=gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_paned_pack1(GTK_PANED(paned1), scroll5, FALSE, TRUE);
    gtk_paned_pack2(GTK_PANED(paned1), da_scroll, TRUE, TRUE);
    gtk_paned_set_position(GTK_PANED(paned1), 410);

    GtkWidget *grid5=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid5), menu_bar, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid5), paned1, 0, 1, 1, 1);

    //Draw background window based on the paned window splitter.
    g_signal_connect(window, "draw", G_CALLBACK(draw_main_window), paned1);

    gtk_container_add(GTK_CONTAINER(window), grid5);

    gchar *css_string=g_strdup("#about_dialog{background: rgba(127,127,255,1.0); font-size: 20px; font-weight: bold;} #about_dialog label selection{background: rgba(0,50,220,0.5);}");
    GError *css_error=NULL;
    GtkCssProvider *provider=gtk_css_provider_new();
    gtk_css_provider_load_from_data(provider, css_string, -1, &css_error);
    gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    if(css_error!=NULL)
      {
        g_print("CSS loader error %s\n", css_error->message);
        g_error_free(css_error);
      }
    g_object_unref(provider);
    g_free(css_string);

    gtk_widget_show_all(window);

    if(!font_set)
      {
        GtkWidget *select_font_label=gtk_bin_get_child(GTK_BIN(select_font));
        gtk_label_set_markup(GTK_LABEL(select_font_label), "<span foreground=\"red\" font=\"12\" weight=\"bold\">Select Font File</span>");
        gtk_notebook_next_page(GTK_NOTEBOOK(notebook));
        gtk_notebook_next_page(GTK_NOTEBOOK(notebook));
      }

    gtk_main();

    //Clean up.
    free_text3d(parts.t3d);
    free_text2d(parts.t2d);

    if(parts.face!=NULL) FT_Done_Face(parts.face);
    if(parts.library!=NULL) FT_Done_FreeType(parts.library);

    return 0;  
  }
static gboolean set_font(FT_Library library, FT_Face *face, const gchar *font)
  {
    FT_Error error;

    error=FT_New_Face(library, font, 0, face);
    if(error==FT_Err_Ok)
      {
         printf("New font face; %s\n", (*face)->family_name);
         if(FT_IS_SCALABLE(*face))
           {
             printf("EM square %i\n", (*face)->units_per_EM);
             font_scale=point_size/(gdouble)((*face)->units_per_EM);
           }
          else g_print("Font isn't scalable Error.");
          return TRUE;
      }
    else
      {
        if(error==FT_Err_Cannot_Open_Resource) printf("Cannot open resource.\n");
        else if(error==FT_Err_Unknown_File_Format) printf("Unknown file format.\n");
        else if(error==FT_Err_Invalid_File_Format) printf("Invalid file format.\n");
        else if(error!=FT_Err_Ok) printf("FT_New_Face error.\n");
        else printf("Unknown error.\n");
        return FALSE;
      }
  }
static gboolean draw_main_window(GtkWidget *widget, cairo_t *cr, gpointer data)
  {
    //Paint background of drawing area.
    cairo_set_source_rgba(cr, window_color[0], window_color[1], window_color[2], window_color[3]);
    cairo_paint(cr);
    //Paint the background under the grid.
    cairo_set_source_rgba(cr, 0.5, 0.5, 1.0, 1.0);
    gint width=gtk_paned_get_position(GTK_PANED(data));
    gint height=gtk_widget_get_allocated_height(widget);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_fill(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 0.0, 1.0);
    cairo_rectangle(cr, width-4, 0.0, 4, height);
    cairo_fill(cr);
    return FALSE;
  }
static void set_drawing_id(GtkComboBox *combo, gpointer data)
  {   
    drawing_id=gtk_combo_box_get_active(combo);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_animate_drawing(GtkComboBox *combo, struct some_parts *parts)
  {   
    pitch=0.0;
    roll=0.0;
    yaw=0.0;
 
    if(gtk_combo_box_get_active(combo)==0)
      {
        if(tick_id!=0) gtk_widget_remove_tick_callback(GTK_WIDGET(parts->da), tick_id);
        tick_id=0;
        gtk_widget_queue_draw(GTK_WIDGET(parts->da));
      }
    else
      {
        if(tick_id==0)
          {
            tick_id=gtk_widget_add_tick_callback(GTK_WIDGET(parts->da), (GtkTickCallback)animate_drawing, parts, NULL);
          }
      }
    
  }
static void set_pitch(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    pitch=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    roll=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    yaw=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_extrude(GtkRange *range, GtkScrollType scroll, gdouble value, struct some_parts *parts)
  {
    extrude=value;
    extrude_text3d(parts->t3d, extrude);
    gtk_widget_queue_draw(GTK_WIDGET(parts->da));
  }
static void set_scale(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>2.0) scale=2.0;
    else if(value<0.0) scale=0.0;
    else scale=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_perspective(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>1.0) perspective=1.0;
    else if(value<0.0) perspective=0.0;
    else perspective=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_text(GtkWidget *button, struct some_parts *parts)
  {
    if(font_set) free_and_set_new_text(parts);
  }
static void free_and_set_new_text(struct some_parts *parts)
  {
    free_text3d(parts->t3d);
    free_text2d(parts->t2d);
    GString *string=g_string_new(gtk_entry_get_text(GTK_ENTRY(parts->entry)));
    parts->t3d=text3d_new((FT_Face)parts->face, string, font_scale, spacing, extrude, use_advance);
    parts->t2d=text2d_new((FT_Face)parts->face, string, font_scale, spacing, depth, use_advance);
    g_string_free(string, TRUE);
    gtk_widget_queue_draw(GTK_WIDGET(parts->da));
  }
static void show_frame_rate(GtkToggleButton *check_frame, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_frame)) frame_rate=TRUE;
    else frame_rate=FALSE;
  }
static void show_drawing_time(GtkToggleButton *check_time, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_time)) time_it=TRUE;
    else time_it=FALSE;
  }
static void set_pitch2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_pitch2=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_roll2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_roll2=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_yaw2(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    slider_yaw2=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_distance(GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
  {
    if(value>2.0) slider_distance=2.0;
    else if(value<0.3) slider_distance=0.3;
    else slider_distance=value;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void x_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    light[0]=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void y_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    light[1]=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void z_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    light[2]=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void layout_width_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    layout_width=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void layout_height_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    layout_height=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void line_width_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    line_width=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_clip_line(GtkToggleButton *check_line, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_line)) clip_line=TRUE;
    else clip_line=FALSE;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_normal(GtkToggleButton *check_normal, gpointer data)
  {
    if(gtk_toggle_button_get_active(check_normal)) with_normal=TRUE;
    else with_normal=FALSE;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void select_new_font(GtkWidget *button, struct some_parts *parts)
  {
    gint result=0;

    GtkWidget *dialog=gtk_file_chooser_dialog_new("Open File", GTK_WINDOW(parts->window), GTK_FILE_CHOOSER_ACTION_OPEN, "Cancel", GTK_RESPONSE_CANCEL, "Open", GTK_RESPONSE_ACCEPT, NULL);
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog), "/usr/share/fonts/truetype/");

    GtkFileFilter *filter=gtk_file_filter_new();
    gtk_file_filter_add_pattern(filter, "*.ttf");
    gtk_file_filter_add_pattern(filter, "*.otf");
    gtk_file_chooser_set_filter(GTK_FILE_CHOOSER(dialog), filter);

    result=gtk_dialog_run(GTK_DIALOG(dialog));
    if(result==GTK_RESPONSE_ACCEPT)
      {

        gchar *filename=gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
        if(parts->face!=NULL) FT_Done_Face(parts->face);
        font_set=set_font(parts->library, &parts->face, filename);
        if(font_set) free_and_set_new_text(parts);
        g_free(filename);
      }

    if(font_set) gtk_button_set_label(GTK_BUTTON(button), "Select Font File");
    gtk_widget_destroy(dialog);
  }
static void spacing_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    spacing=gtk_spin_button_get_value(spin_button);
  }
static void translate_x_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    translate_x=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void translate_y_spin_changed(GtkSpinButton *spin_button, gpointer data)
  {
    translate_y=gtk_spin_button_get_value(spin_button);
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static void set_t3d_color(GtkWidget *button, gpointer *data)
  {
    GdkRGBA rgba;
    
    if(gdk_rgba_parse(&rgba, gtk_entry_get_text(GTK_ENTRY(data[1]))))
      {
        t3d_front_color[0]=rgba.red;
        t3d_front_color[1]=rgba.green;
        t3d_front_color[2]=rgba.blue;
        t3d_front_color[3]=rgba.alpha;
      }
    else
      {
        g_print("Font front color string format error.\n");
      } 

    if(gdk_rgba_parse(&rgba, gtk_entry_get_text(GTK_ENTRY(data[2]))))
      {  
        t3d_side_color1[0]=rgba.red;
        t3d_side_color1[1]=rgba.green;
        t3d_side_color1[2]=rgba.blue;
        t3d_side_color1[3]=rgba.alpha;
      }
    else
      {
        g_print("Font side color1 color string format error.\n");
      } 

    if(gdk_rgba_parse(&rgba, gtk_entry_get_text(GTK_ENTRY(data[3]))))
      {
        t3d_side_color2[0]=rgba.red;
        t3d_side_color2[1]=rgba.green;
        t3d_side_color2[2]=rgba.blue;
        t3d_side_color2[3]=rgba.alpha;
      }
    else
      {
        g_print("Font side color2 string format error.\n");
      } 

    if(gdk_rgba_parse(&rgba, gtk_entry_get_text(GTK_ENTRY(data[4]))))
      {
        t3d_back_color[0]=rgba.red;
        t3d_back_color[1]=rgba.green;
        t3d_back_color[2]=rgba.blue;
        t3d_back_color[3]=rgba.alpha;
      }
    else
      {
        g_print("Font back color string format error.\n");
      } 
   
    gtk_widget_queue_draw(GTK_WIDGET(data[0]));
  }
static void set_t2d_color(GtkWidget *button, gpointer *data)
  {
    GdkRGBA rgba;
    
    if(gdk_rgba_parse(&rgba, gtk_entry_get_text(GTK_ENTRY(data[1]))))
      {
        t2d_front_color[0]=rgba.red;
        t2d_front_color[1]=rgba.green;
        t2d_front_color[2]=rgba.blue;
        t2d_front_color[3]=rgba.alpha;
      }
    else
      {
        g_print("Font front color string format error.\n");
      } 

    if(gdk_rgba_parse(&rgba, gtk_entry_get_text(GTK_ENTRY(data[2]))))
      {  
        t2d_side_color1[0]=rgba.red;
        t2d_side_color1[1]=rgba.green;
        t2d_side_color1[2]=rgba.blue;
        t2d_side_color1[3]=rgba.alpha;
      }
    else
      {
        g_print("Font side color1 color string format error.\n");
      } 

    if(gdk_rgba_parse(&rgba, gtk_entry_get_text(GTK_ENTRY(data[3]))))
      {
        t2d_side_color2[0]=rgba.red;
        t2d_side_color2[1]=rgba.green;
        t2d_side_color2[2]=rgba.blue;
        t2d_side_color2[3]=rgba.alpha;
      }
    else
      {
        g_print("Font side color2 string format error.\n");
      } 

    if(gdk_rgba_parse(&rgba, gtk_entry_get_text(GTK_ENTRY(data[4]))))
      {
        t2d_back_color[0]=rgba.red;
        t2d_back_color[1]=rgba.green;
        t2d_back_color[2]=rgba.blue;
        t2d_back_color[3]=rgba.alpha;
      }
    else
      {
        g_print("Font back color string format error.\n");
      } 
   
    gtk_widget_queue_draw(GTK_WIDGET(data[0]));
  }
static void set_line_color(GtkWidget *button, gpointer *data)
  {
    GdkRGBA rgba;
    
    if(gdk_rgba_parse(&rgba, gtk_entry_get_text(GTK_ENTRY(data[1]))))
      {
        line_color[0]=rgba.red;
        line_color[1]=rgba.green;
        line_color[2]=rgba.blue;
        line_color[3]=rgba.alpha;
      }
    else
      {
        g_print("Line color string format error.\n");
      } 

    gtk_widget_queue_draw(GTK_WIDGET(data[0]));
  }
static void set_back_window_color(GtkWidget *button, gpointer *data)
  {
    GdkRGBA rgba;
    
    if(gdk_rgba_parse(&rgba, gtk_entry_get_text(GTK_ENTRY(data[1]))))
      {
        window_color[0]=rgba.red;
        window_color[1]=rgba.green;
        window_color[2]=rgba.blue;
        window_color[3]=rgba.alpha;
      }
    else
      {
        g_print("Back window color string format error.\n");
      } 

    gtk_widget_queue_draw(GTK_WIDGET(data[0]));
  }
static void set_advance(GtkToggleButton *advance_check, gpointer data)
  {
    if(gtk_toggle_button_get_active(advance_check)) use_advance=TRUE;
    else use_advance=FALSE;
  }
static void set_bounding_box(GtkToggleButton *bounding_box_check, gpointer data)
  {
    if(gtk_toggle_button_get_active(bounding_box_check)) show_bounding_box=TRUE;
    else show_bounding_box=FALSE;
    gtk_widget_queue_draw(GTK_WIDGET(data));
  }
static gboolean animate_drawing(GtkWidget *da, GdkFrameClock *frame_clock, struct some_parts *parts)
  {
    if(frame_rate)
      {
        //Check frame rate.
        gint64 frame=gdk_frame_clock_get_frame_counter(frame_clock);
        gint64 current_time=gdk_frame_clock_get_frame_time(frame_clock);
        gint64 start = gdk_frame_clock_get_history_start(frame_clock);
        gint64 history_len=frame-start;
        GdkFrameTimings *previous_timings=gdk_frame_clock_get_timings(frame_clock, frame-history_len);
        gint64 previous_frame_time=gdk_frame_timings_get_frame_time(previous_timings);
        g_print("Frame %i, %f fps\n", (gint)frame, (gdouble)(history_len)*G_USEC_PER_SEC/(gdouble)(current_time-previous_frame_time));
      }

    if(pitch>359.0) pitch=0.0;
    else pitch+=0.5;
    if(roll>359.0) roll=0.0;
    else roll+=0.5;
    if(yaw>359.0) yaw=0.0;
    else yaw+=0.5;
    
    gtk_widget_queue_draw(GTK_WIDGET(da));
    return G_SOURCE_CONTINUE;
  }
static gboolean draw_main(GtkWidget *da, cairo_t *cr, struct some_parts *parts)
  {  
    gdouble w1=layout_width/10.0;
    gdouble h1=layout_height/10.0;
   
    //Background.
    cairo_set_source_rgba(cr, window_color[0], window_color[1], window_color[2], window_color[3]);
    cairo_paint(cr); 

    //Cartesian coordinates for drawing.
    cairo_set_line_width(cr, 2.0);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, w1, h1, 8.0*w1, 8.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 1.0*w1, 5.0*h1);
    cairo_line_to(cr, 9.0*w1, 5.0*h1);
    cairo_stroke(cr);
    cairo_move_to(cr, 5.0*w1, 1.0*h1);
    cairo_line_to(cr, 5.0*w1, 9.0*h1);
    cairo_stroke(cr);

    //Layout bounding box.
    cairo_rectangle(cr, 0.0, 0.0, layout_width, layout_height);
    cairo_stroke(cr);
  
    GTimer *timer=NULL; 
    if(time_it==TRUE) timer=g_timer_new();
 
    cairo_translate(cr, layout_width/2.0, layout_height/2.0);
    cairo_translate(cr, translate_x, translate_y);
    cairo_scale(cr, scale+0.01, scale+0.01);
    //Keep the lines smooth.
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);        
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_set_line_width(cr, 2.0);

    gdouble qrs[9];
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    quaternion_rotation_text(yaw1, roll1, pitch1, qrs);

    if(font_set)
      {
        if(drawing_id<4)
          {
            draw_text3d(parts->t3d, cr, qrs);
            if(show_bounding_box) draw_bounding_box_t3d(parts->t3d, cr, qrs);
          }
        else
          {
            gdouble plane_distance=slider_distance*100.0;
            gdouble light2[3];
            light2[0]=light[0];light2[1]=light[1];light2[2]=light[2];
            set_revolve_standard_point_text(light2[0], light2[1], light2[2]);
            revolve_standard_point_text(qrs);
            get_revolved_point_text(&light2[0], &light2[1], &light2[2]);         
            gdouble plane_points[9]={-1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, -1.0, 0.0};
            set_plane_points(plane_points);
            draw_shadow_text(parts->t2d, cr, qrs, light2, plane_distance);
            if(show_bounding_box) draw_bounding_box_t2d(parts->t2d, cr, qrs);
          }
      }
     
    if(time_it==TRUE) 
     {
       g_print("Draw Time %f\n", g_timer_elapsed(timer, NULL));
       g_timer_destroy(timer);
     }

    return FALSE;
  }
static gchar* save_file(struct some_parts *parts, gint surface_type)
  {
    int result=0;

    GtkWidget *dialog=gtk_file_chooser_dialog_new("Save File", GTK_WINDOW(parts->window), GTK_FILE_CHOOSER_ACTION_SAVE, "Cancel", GTK_RESPONSE_CANCEL, "Save", GTK_RESPONSE_ACCEPT, NULL);

    if(surface_type==0) gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".svg");
    else if(surface_type==1) gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".pdf");
    else if(surface_type==2) gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".ps");
    else gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), ".png"); 

    result=gtk_dialog_run(GTK_DIALOG(dialog));
    gchar *file_name=NULL;
    if(result==GTK_RESPONSE_ACCEPT)
      {

        file_name=gtk_file_chooser_get_current_name(GTK_FILE_CHOOSER(dialog));
      }

    gtk_widget_destroy(dialog);

    return file_name;
  }
static void draw_svg(GtkWidget *widget, struct some_parts *parts)
  {
    gchar *file_name=save_file(parts, 0);
    
    //Scale factor to convert to points.
    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_svg_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, parts, 0);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name);
  }
static void draw_pdf(GtkWidget *widget, struct some_parts *parts)
  {
    gchar *file_name=save_file(parts, 1);

    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_pdf_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, parts, 1);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }
    
    g_free(file_name);
  }
static void draw_ps(GtkWidget *widget, struct some_parts *parts)
  {
    gchar *file_name=save_file(parts, 2);

    if(file_name!=NULL)
      {
        gdouble points_scale=72.0/ppi;
        cairo_surface_t *surface=cairo_ps_surface_create(file_name, layout_width*points_scale, layout_height*points_scale);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, parts, 2);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name);    
  }
static void draw_png(GtkWidget *widget, struct some_parts *parts)
  {
    gchar *file_name=save_file(parts, 3);

    if(file_name!=NULL)
      {
        cairo_surface_t *surface=cairo_image_surface_create(CAIRO_FORMAT_ARGB32, layout_width, layout_height);
        cairo_t *cr=cairo_create(surface);
        draw_surface(cr, parts, 3);
        cairo_surface_write_to_png(surface, file_name);
        cairo_destroy(cr);
        cairo_surface_destroy(surface);
        g_print("%s saved\n", file_name);
      }

    g_free(file_name);    
  }
static void draw_surface(cairo_t *cr, struct some_parts *parts, gint surface_type)
  {
    gdouble points_scale=0.0;

    if(surface_type<3) points_scale=72.0/ppi;
    else points_scale=1.0;

    //Background.
    cairo_set_source_rgba(cr, window_color[0], window_color[1], window_color[2], window_color[3]);
    cairo_paint(cr); 

    cairo_translate(cr, layout_width*points_scale/2.0, layout_height*points_scale/2.0);
    cairo_translate(cr, translate_x*points_scale, translate_y*points_scale);
    cairo_scale(cr, scale*points_scale+0.01, scale*points_scale+0.01);
    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);        
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    cairo_set_line_width(cr, 2.0);

    //Rotation.
    gdouble qrs[9];
    gdouble pitch1=pitch*G_PI/180.0;
    gdouble roll1=roll*G_PI/180.0;
    gdouble yaw1=yaw*G_PI/180.0;
    quaternion_rotation_text(yaw1, roll1, pitch1, qrs);

    if(font_set)
      {
        if(drawing_id<4) draw_text3d(parts->t3d, cr, qrs);
        else
          {
            gdouble plane_distance=slider_distance*100.0;
            gdouble light2[3];
            light2[0]=light[0];light2[1]=light[1];light2[2]=light[2];
            set_revolve_standard_point_text(light2[0], light2[1], light2[2]);
            revolve_standard_point_text(qrs);
            get_revolved_point_text(&light2[0], &light2[1], &light2[2]);         
            gdouble plane_points[9]={-1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, -1.0, 0.0};
            set_plane_points(plane_points);
            draw_shadow_text(parts->t2d, cr, qrs, light2, plane_distance);
          }
      }
  }
static void draw_text3d(struct text3d *t3d, cairo_t *cr, gdouble qrs[9])
  {
    gint i=0;
    GArray *path1=NULL;
    GArray *path2=NULL;
    
    //Reset transformed text arrays.
    reset_text3d(t3d);

    //Set perspective.
    if(perspective>0.0)
      {
        gdouble perspective1=1.0-perspective;
        perspective_text3d(t3d, perspective1);
      }

    //Rotate text.
    for(i=0;i<(t3d->cairo_text_t->len); i++)
      {
        path1=(GArray*)g_ptr_array_index(t3d->cairo_text_t, i);
        if(path1!=NULL) transform_letter(path1, qrs);
      }

    gdouble top[3];
    set_revolve_standard_point_text(1.0, 0.0, 0.0);
    revolve_standard_point_text(qrs);
    get_revolved_point_text(&top[0], &top[1], &top[2]);

    //Draw the 3d text
    gdouble front_rgba[4]={0.0, 0.0, 0.0, 0.0};
    gdouble side1_rgba[4]={0.0, 0.0, 0.0, 0.0};
    gdouble side2_rgba[4]={0.0, 0.0, 0.0, 0.0};
    gdouble back_rgba[4]={0.0, 0.0, 0.0, 0.0};
    front_rgba[0]=t3d_front_color[0]; front_rgba[1]=t3d_front_color[1]; front_rgba[2]=t3d_front_color[2]; front_rgba[3]=t3d_front_color[3];
    side1_rgba[0]=t3d_side_color1[0]; side1_rgba[1]=t3d_side_color1[1]; side1_rgba[2]=t3d_side_color1[2]; side1_rgba[3]=t3d_side_color1[3];
    side2_rgba[0]=t3d_side_color2[0]; side2_rgba[1]=t3d_side_color2[1]; side2_rgba[2]=t3d_side_color2[2]; side2_rgba[3]=t3d_side_color2[3];
    back_rgba[0]=t3d_back_color[0]; back_rgba[1]=t3d_back_color[1]; back_rgba[2]=t3d_back_color[2]; back_rgba[3]=t3d_back_color[3];
    if(top[2]>=0.0)
      {
        //Draw left to right.
        for(i=0;i<(t3d->cairo_text_t->len); i+=2)
          {
            path1=(GArray*)g_ptr_array_index(t3d->cairo_text_t, i);
            path2=(GArray*)g_ptr_array_index(t3d->cairo_text_t, i+1);       
            if(path1!=NULL&&path2!=NULL) draw_char(cr, path1, path2, front_rgba, side1_rgba, side2_rgba, back_rgba);            
          }
      }
    else
      {
        //Draw right to left.
        gint start=(t3d->cairo_text_t->len)-1;
        for(i=start;i>=0;i-=2)
          {
            path1=(GArray*)g_ptr_array_index(t3d->cairo_text_t, i-1);
            path2=(GArray*)g_ptr_array_index(t3d->cairo_text_t, i);       
            if(path1!=NULL&&path2!=NULL) draw_char(cr, path1, path2, front_rgba, side1_rgba, side2_rgba, back_rgba);            
          }
      } 
  }
static void draw_char(cairo_t *cr, GArray *path1, GArray *path2, gdouble front_rgba[4], gdouble side1_rgba[4], gdouble side2_rgba[4], gdouble back_rgba[4])
  {
    if(standard_vector_is_top())
      {
        cairo_set_source_rgba(cr, back_rgba[0], back_rgba[1], back_rgba[2], back_rgba[3]);
        if(drawing_id==0||drawing_id==3) draw_path_contours_fill(cr, path2);
        else draw_path_contours_line(cr, path2, FALSE);

        cairo_set_source_rgba(cr, side1_rgba[0], side1_rgba[1], side1_rgba[2], side1_rgba[3]);
        if(drawing_id==0) draw_path_side_fill_blur_linear(cr, path1, path2, side1_rgba, side2_rgba, with_normal); 
        else if(drawing_id==1) draw_path_contours_connect(cr, path1, path2);             
        else draw_path_side_fill(cr, path1, path2, side1_rgba);

        cairo_set_source_rgba(cr, front_rgba[0], front_rgba[1], front_rgba[2], front_rgba[3]);
        if(drawing_id==0||drawing_id==3) draw_path_contours_fill(cr, path1);
        if(drawing_id==0||drawing_id==3)
          {
            cairo_set_source_rgba(cr, line_color[0], line_color[1], line_color[2], line_color[3]);
            cairo_set_line_width(cr, line_width);
            if(clip_line) draw_path_contours_line(cr, path1, TRUE);
            else draw_path_contours_line(cr, path1, FALSE);
          }
        else draw_path_contours_line(cr, path1, FALSE);
      }
    else
      {
        cairo_set_source_rgba(cr, front_rgba[0], front_rgba[1], front_rgba[2], front_rgba[3]);
        if(drawing_id==0||drawing_id==3) draw_path_contours_fill(cr, path1);
        else draw_path_contours_line(cr, path1, FALSE);

        cairo_set_source_rgba(cr, side1_rgba[0], side1_rgba[1], side1_rgba[2], side1_rgba[3]);
        if(drawing_id==0) draw_path_side_fill_blur_linear(cr, path1, path2, side1_rgba, side2_rgba, with_normal); 
        else if(drawing_id==1) draw_path_contours_connect(cr, path1, path2);
        else draw_path_side_fill(cr, path1, path2, side1_rgba);

        cairo_set_source_rgba(cr, back_rgba[0], back_rgba[1], back_rgba[2], back_rgba[3]);
        if(drawing_id==0||drawing_id==3) draw_path_contours_fill(cr, path2);
        if(drawing_id==0||drawing_id==3)
          {
            cairo_set_source_rgba(cr, line_color[0], line_color[1], line_color[2], line_color[3]);
            cairo_set_line_width(cr, line_width);
            if(clip_line) draw_path_contours_line(cr, path2, TRUE);
            else draw_path_contours_line(cr, path2, FALSE);
          }
        else draw_path_contours_line(cr, path2, FALSE);
      }
  }
static void draw_bounding_box_t3d(struct text3d *t3d,  cairo_t *cr, gdouble qrs[9])
  {
    gdouble total_width=0.0;
    gdouble max_height=0.0;
    cairo_matrix_t matrix1;
    gint index=t3d->box_size->len-1;

    if(index>0)
      {
        total_width=g_array_index(t3d->box_size, gdouble, index-1);
        max_height=g_array_index(t3d->box_size, gdouble, index);
      }         

    cairo_save(cr);
    cairo_set_line_width(cr, 4.0);
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    cairo_matrix_init(&matrix1, qrs[0], qrs[3], qrs[1], qrs[4], 0.0, 0.0);
    cairo_transform(cr, &matrix1);
    cairo_rectangle (cr, -total_width/2.0, -max_height/2.0, total_width, max_height);
    cairo_stroke(cr);
    //The width and height of the box.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    gchar *string=g_strdup_printf("w=%.0f h=%.0f", total_width*scale, max_height*scale);
    cairo_move_to(cr, -total_width/2.0, -max_height/2.0-10.0);
    cairo_set_font_size(cr, 20.0);
    cairo_show_text(cr, string);
    g_free(string);
    cairo_restore(cr);
  }
static void draw_bounding_box_t2d(struct text2d *t2d,  cairo_t *cr, gdouble qrs[9])
  {
    gdouble total_width=0.0;
    gdouble max_height=0.0;
    cairo_matrix_t matrix1;
    gint index=t2d->box_size->len-1;

    if(index>0)
      {
        total_width=g_array_index(t2d->box_size, gdouble, index-1);
        max_height=g_array_index(t2d->box_size, gdouble, index);
      }
    
    cairo_save(cr);
    cairo_set_line_width(cr, 4.0);
    cairo_set_source_rgba(cr, 0.0, 0.0, 1.0, 1.0);
    cairo_matrix_init(&matrix1, qrs[0], qrs[3], qrs[1], qrs[4], 0.0, 0.0);
    cairo_transform(cr, &matrix1);
    cairo_rectangle (cr, -total_width/2.0, -max_height/2.0, total_width, max_height);
    cairo_stroke(cr);
    //The width and height of the box.
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    gchar *string=g_strdup_printf("w=%.0f h=%.0f", total_width*scale, max_height*scale);
    cairo_move_to(cr, -total_width/2.0, -max_height/2.0-10.0);
    cairo_set_font_size(cr, 20.0);
    cairo_show_text(cr, string);
    g_free(string);
    cairo_restore(cr);
  }
static void draw_shadow_text(struct text2d *t2d, cairo_t *cr, gdouble qrs[9], gdouble light_position[3], gdouble plane_distance)
  {
    gint i=0;
    GArray *path1=NULL;
    
    //Reset transformed text arrays.
    reset_text2d(t2d);

    //Rotate text.
    for(i=0;i<(t2d->cairo_text_t->len); i++)
      {
        path1=(GArray*)g_ptr_array_index(t2d->cairo_text_t, i);
        if(path1!=NULL) transform_letter(path1, qrs);
      }

    //Check the direction the font is facing. This isn't from cairo3d_text.
    gdouble top[3];
    set_revolve_standard_point_text(1.0, 0.0, 0.0);
    revolve_standard_point_text(qrs);
    get_revolved_point_text(&top[0], &top[1], &top[2]);
  
    //Draw the shadow text.
    gdouble front_rgba[4]={0.0, 0.0, 0.0, 0.0};
    gdouble side1_rgba[4]={0.0, 0.0, 0.0, 0.0};
    gdouble side2_rgba[4]={0.0, 0.0, 0.0, 0.0};
    gdouble back_rgba[4]={0.0, 0.0, 0.0, 0.0};
    front_rgba[0]=t2d_front_color[0]; front_rgba[1]=t2d_front_color[1]; front_rgba[2]=t2d_front_color[2]; front_rgba[3]=t2d_front_color[3];
    side1_rgba[0]=t2d_side_color1[0]; side1_rgba[1]=t2d_side_color1[1]; side1_rgba[2]=t2d_side_color1[2]; side1_rgba[3]=t2d_side_color1[3];
    side2_rgba[0]=t2d_side_color2[0]; side2_rgba[1]=t2d_side_color2[1]; side2_rgba[2]=t2d_side_color2[2]; side2_rgba[3]=t2d_side_color2[3];
    back_rgba[0]=t2d_back_color[0]; back_rgba[1]=t2d_back_color[1]; back_rgba[2]=t2d_back_color[2]; back_rgba[3]=t2d_back_color[3];
    if(top[2]>=0.0)
      {
        //Draw left to right.
        for(i=0;i<(t2d->cairo_text_t->len); i++)
          {
            path1=(GArray*)g_ptr_array_index(t2d->cairo_text_t, i);
            if(path1!=NULL)
              {
                gdouble qrs_plane[9];
                gdouble out[9];
                quaternion_rotation_text(slider_yaw2*G_PI/180.0, slider_roll2*G_PI/180.0, slider_pitch2*G_PI/180.0, qrs_plane);
                matrix_multiply(qrs_plane, qrs, out);
                GArray *shadow=get_shadow_path(path1, light_position, plane_distance, out);
                draw_shadow_char(cr, path1, shadow, front_rgba, side1_rgba, side2_rgba, back_rgba);
                g_array_free(shadow, TRUE);
              }      
          }
      }
    else
      {
        //Draw right to left.
        gint start=(t2d->cairo_text_t->len)-1;
        for(i=start;i>=0;i--)
          {
            path1=(GArray*)g_ptr_array_index(t2d->cairo_text_t, i);
            if(path1!=NULL)
              {
                gdouble qrs_plane[9];
                gdouble out[9];
                quaternion_rotation_text(slider_yaw2*G_PI/180.0, slider_roll2*G_PI/180.0, slider_pitch2*G_PI/180.0, qrs_plane);
                matrix_multiply(qrs_plane, qrs, out);
                GArray *shadow=get_shadow_path(path1, light_position, plane_distance, out);
                draw_shadow_char(cr, path1, shadow, front_rgba, side1_rgba, side2_rgba, back_rgba);
                g_array_free(shadow, TRUE);
              }
          }
      } 
  }
static void draw_shadow_char(cairo_t *cr, GArray *path1, GArray *shadow, gdouble front_rgba[4], gdouble side1_rgba[4], gdouble side2_rgba[4], gdouble back_rgba[4])
  {
    if(standard_vector_is_top())
      {
        cairo_set_source_rgba(cr, back_rgba[0], back_rgba[1], back_rgba[2], back_rgba[3]);
        draw_path_contours_fill(cr, shadow); 
        draw_path_side_fill_blur_linear(cr, path1, shadow, side1_rgba, side2_rgba, with_normal);   
        cairo_set_source_rgba(cr, front_rgba[0], front_rgba[1], front_rgba[2], front_rgba[3]);
        draw_path_contours_fill(cr, path1);
        cairo_set_line_width(cr, line_width);
        cairo_set_source_rgba(cr, line_color[0], line_color[1], line_color[2], line_color[3]);
        if(clip_line) draw_path_contours_line(cr, path1, TRUE);
        else draw_path_contours_line(cr, path1, FALSE);
      }
    else
      {
        cairo_set_line_width(cr, line_width);
        cairo_set_source_rgba(cr, front_rgba[0], front_rgba[1], front_rgba[2], front_rgba[3]);
        draw_path_contours_fill(cr, path1); 
        cairo_set_source_rgba(cr, line_color[0], line_color[1], line_color[2], line_color[3]);
        if(clip_line) draw_path_contours_line(cr, path1, TRUE);
        else draw_path_contours_line(cr, path1, FALSE);
        draw_path_side_fill_blur_linear(cr, path1, shadow, side1_rgba, side2_rgba, with_normal);   
        cairo_set_source_rgba(cr, back_rgba[0], back_rgba[1], back_rgba[2], back_rgba[3]);
        draw_path_contours_fill(cr, shadow);
      }
         
  }
static void extrude_text3d(struct text3d *t3d, gdouble z_value)
  {
    gint i=0;
    gint j=0;
    GArray *path1=NULL;
    GArray *path2=NULL;
    struct segment2 *p1=NULL;
    struct segment2 *p2=NULL;

    for(i=0;i<(t3d->cairo_text->len); i+=2)
      {
        path1=(GArray*)g_ptr_array_index(t3d->cairo_text, i);
        if(path1!=NULL) p1=&g_array_index(path1, struct segment2, 0);
        path2=(GArray*)g_ptr_array_index(t3d->cairo_text, i+1);
        if(path2!=NULL) p2=&g_array_index(path2, struct segment2, 0);
        if(path1!=NULL&&path2!=NULL)
          {
            for(j=0;j<path1->len;j++)
              {
                (p1->z1)=z_value;
                (p1->z2)=z_value;
                (p1->z3)=z_value;
                (p2->z1)=-z_value;
                (p2->z2)=-z_value;
                (p2->z3)=-z_value;
                p1++;p2++;
              }
          }
      }
  }
static void perspective_text3d(struct text3d *t3d, gdouble value)
  {
    gint i=0;
    gint j=0;
    GArray *path2=NULL;
    struct segment2 *p2=NULL;

    for(i=1;i<(t3d->cairo_text_t->len); i+=2)
      {
        path2=(GArray*)g_ptr_array_index(t3d->cairo_text_t, i);
        if(path2!=NULL) p2=&g_array_index(path2, struct segment2, 0);
        if(path2!=NULL)
          {
            for(j=0;j<path2->len;j++)
              {
                (p2->x1)*=value;
                (p2->x2)*=value;
                (p2->x3)*=value;
                (p2->y1)*=value;
                (p2->y2)*=value;
                (p2->y3)*=value;
                p2++;
              }
          }
      }
  }
static void about_dialog(GtkWidget *widget, struct some_parts *parts)
  {
    GtkWidget *dialog=gtk_about_dialog_new();
    gtk_widget_set_name(dialog, "about_dialog");
    gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(parts->window));
    //gtk_about_dialog_set_logo(GTK_ABOUT_DIALOG(dialog), NULL);
    gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(dialog), "Shadow Font");
    gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog), "Test Version 1.0");
    gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(dialog), "3d fonts with shadows.");
    gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(dialog), "(C) 2019 C. Eric Cashon");

    gtk_widget_show_all(dialog);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
  }


