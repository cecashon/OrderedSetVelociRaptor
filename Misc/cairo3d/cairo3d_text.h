/*
    See cairo3d_text_main.c for the test program.
*/

#include<glib.h>
#include<cairo.h>
#include<math.h>
#include<ft2build.h>
#include FT_FREETYPE_H
#include FT_OUTLINE_H
#include FT_BBOX_H

/*
  A drawing path operation. The R and L on move_to is clockwise and counter clockwise 
  winding of the path.
*/
enum{ 
  MOVE_TO_R,
  MOVE_TO_L,
  LINE_TO,
  CURVE_TO,
  LINE_TO_STROKE,
  CURVE_TO_STROKE
};

/*
   Replace segment in the 3d drawings with segment2. Some wasted space but it cleans up the
   while loops and makes it easier to sort quads for drawing a blur.
*/
struct segment2{
  gint move_id;
  gdouble x1;
  gdouble y1;
  gdouble z1;
  gdouble x2;
  gdouble y2;
  gdouble z2;
  gdouble x3;
  gdouble y3;
  gdouble z3;
}; 

//A quad that can be sorted by z-value.
struct quad{
  struct segment2 a;
  struct segment2 b;
  struct segment2 c;
  struct segment2 d;
};

//The cairo3d text and the transformed text. Two paths per char in the pointer arrays.
struct text3d{
    //original text.
    GString *text;
    //The char points that cairo draws with.
    GPtrArray *cairo_text;
    GPtrArray *cairo_text_t;
    //A box around each char.
    GArray *box_size;
    gdouble spacing;
   };

//The same as above but there is only one path per char in the pointer array.
struct text2d{
    GString *text;
    GPtrArray *cairo_text;
    GPtrArray *cairo_text_t;
    GArray *box_size;
    gdouble spacing;
   };

//The char drawing functions
void draw_path_contours_line(cairo_t *cr, GArray *path, gboolean clip);
void draw_path_contours_connect(cairo_t *cr, GArray *path1, GArray *path2);
void draw_path_side_fill(cairo_t *cr, GArray *path1, GArray *path2, gdouble rgba[4]);
void draw_path_side_fill_blur(cairo_t *cr, GArray *path1, GArray *path2, gdouble rgba1[4], gdouble rgba2[4]);
void draw_path_side_fill_blur_linear(cairo_t *cr, GArray *path1, GArray *path2, gdouble rgba1[4], gdouble rgba2[4], gboolean with_normal);
void draw_path_contours_fill(cairo_t *cr, GArray *path);
//Get a string of char paths for 3d text.
struct text3d* text3d_new(FT_Face face, GString *string, gdouble scale, gdouble spacing, gdouble z_depth, gboolean use_advance);
void reset_text3d(struct text3d *t3d);
void free_text3d(struct text3d *t3d);
//Get a string of 2d char paths for use with drawing shadows.
struct text2d* text2d_new(FT_Face face, GString *string, gdouble scale, gdouble spacing, gdouble z_depth, gboolean use_advance);
void reset_text2d(struct text2d *t2d);
void free_text2d(struct text2d *t2d);
/*
    Get arrays of char points to use with the drawing functions. The 2d is one copy of the
    path. This is used to draw the char with a movable shadow plane. The 3d function returns
    2 copies of the path that are connected for the 3d char. The end result for 2d is similar
    to 3d since the shadow path is connected to the single path from the 2d function.
*/
GPtrArray* get_font_paths_2d(FT_Face face, gdouble scale, const gchar *text, GArray *index, GArray *box_size, gdouble *max_advance_y, gdouble *max_drop_y, gboolean use_advance, gboolean single_char);
GPtrArray* get_font_paths_3d(FT_Face face, gdouble scale, const gchar *text, GArray *index, GArray *box_size, gdouble *max_advance_y, gdouble *max_drop_y, gboolean use_advance, gboolean single_char);
GArray* get_font_path(FT_Face face, gdouble scale, const gchar *text, gint index, gdouble *box_w, gdouble *box_h, gdouble *max_advance_y, gdouble *max_drop_y, gboolean use_advance, gboolean single_char);
void curve_font(GArray *path, gdouble radius);
GArray* get_shadow_path(GArray *path, const gdouble light[3], const gdouble plane_distance, const gdouble qrs_plane[9]);

//Transform functions.
void quaternion_rotation_text(const gdouble yaw, const gdouble roll, const gdouble pitch, gdouble qrs[9]);
void transform_letter(GArray *path, const gdouble qrs[9]);
void matrix_multiply(const gdouble in1[9], const gdouble in2[9], gdouble out[9]);
//Standard vectors to use for revolving and determining the top or z direction of the drawing.
void set_revolve_standard_point_text(gdouble x, gdouble y, gdouble z);
void get_revolved_point_text(gdouble *x, gdouble *y, gdouble *z);
void revolve_standard_point_text(gdouble qrs[9]);
gboolean standard_vector_is_top();
void set_plane_points(gdouble plane_points[9]);





