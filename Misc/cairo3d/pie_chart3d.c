/* 
    See pie_chart3d_main.c for the test program.
    
    gcc -Wall -c pie_chart3d.c -o pie_chart3d.o `pkg-config --cflags --libs glib-2.0` `pkg-config --cflags --libs cairo` -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include"pie_chart3d.h"

static void initialize_bezier_ring(GArray **ring, GArray **ring_t, guint pieces, gdouble radius, gdouble z);
static void reset_ring(GArray *ring, GArray *ring_t);
static void rotate_reference_vector(struct pie_chart *pc, gdouble qrs[9]);
static void rotate_pie_reference_vectors(struct pie_chart *pc, gdouble qrs[9]);
static void rotate_ring(GArray *ring, gdouble qrs[9]);
//A sort function for the inside quads.
static int compare_quads(const void *q1, const void *q2);

struct pie_chart* pie_chart_new(guint pieces, gdouble radius, gdouble depth)
  {
    //Enforce some semi-arbitrary bounds.
    if(pieces>120||pieces<30)
      {
        pieces=60;
        g_warning("Pieces bounds, 30<=pieces<=120.0. Default pieces set to 60.0.\n");
      }   
    if(radius>200.0||radius<1.0)
      {
        radius=100.0;
        g_warning("Radius bounds, 1.0<=radius<=200.0. Default radius set to 100.0.\n");
      }
    if(depth>100.0||depth<0.0)
      {
        depth=20.0;
        g_warning("Depth bounds, 0.0<=depth<=100.0. Default depth set to 20.0.\n");
      }

    struct pie_chart *pc=g_new(struct pie_chart, 1);
    pc->pieces=pieces;
    pc->radius=radius;
    pc->pie_chart1=NULL;
    pc->pie_chart1_t=NULL;
    pc->pie_chart2=NULL;
    pc->pie_chart2_t=NULL;
    pc->v_pie=NULL;
    pc->v_pie_t=NULL;
    pc->sections=NULL;
    pc->labels=NULL;
    pc->top[0]=0.0;
    pc->top[1]=0.0;
    pc->top[2]=1.0;
    pc->top_t[0]=0.0;
    pc->top_t[1]=0.0;
    pc->top_t[2]=1.0;
    pc->depth=depth;
    pc->outline=TRUE;

    initialize_bezier_ring(&(pc->pie_chart1), &(pc->pie_chart1_t), pieces, radius, depth);
    initialize_bezier_ring(&(pc->pie_chart2), &(pc->pie_chart2_t), pieces, radius, -depth);

    pc->v_pie=g_array_new(FALSE, FALSE, sizeof(struct pie_point3d));
    pc->v_pie_t=g_array_new(FALSE, FALSE, sizeof(struct pie_point3d));
    pc->sections=g_array_new(FALSE, FALSE, sizeof(struct section));
    pc->labels=g_ptr_array_new_with_free_func(g_free);

    return pc;
  }
gboolean pie_chart_append_section(struct pie_chart *pc, guint end, gdouble rgba[4])
  {
    gboolean error=FALSE;
    struct section *p1=NULL;

    //Check some error conditions.
    if(pc==NULL)
      {
        error=TRUE;
        g_warning("There isn't a valid pie chart. No section appended.\n");
      }
    else if(end<0||end>(pc->pie_chart1->len)-1)
      {
        error=TRUE;
        g_warning("Section end is outside of the pie chart boundry. No section appended.\n");
      }
    else if((pc->sections->len)>0)
      {
        p1=&g_array_index(pc->sections, struct section, (pc->sections->len)-1);
        if(end<=(p1->end))
          {
            error=TRUE;
            g_warning("The end is contained in an existing section. No section appended.\n");
          }
      }

    //Add the new section and vectors.
    if(!error)
      {
        gdouble pieces=(gdouble)(pc->pie_chart1->len)/2.0;
        struct section s1;
        s1.end=end;
        s1.color[0]=rgba[0];s1.color[1]=rgba[1];s1.color[2]=rgba[2];s1.color[3]=rgba[3];
        s1.section_explode=0.0;
        //Get the start from the last section.
        if((pc->sections->len)==0)
          {
            s1.start=0;
          }
        else
          {
            p1=&g_array_index(pc->sections, struct section, (pc->sections->len)-1);
            s1.start=(p1->end)+1;
          }
        gdouble vc=s1.start+((end+1)-s1.start)/2.0;
        struct pie_point3d v1;
        v1.x=cos(vc*G_PI/pieces);v1.y=-sin(vc*G_PI/pieces);v1.z=0.0;
        s1.section_id=(pc->sections->len);
        g_array_append_val(pc->sections, s1);
        g_array_append_val(pc->v_pie, v1);
        g_array_append_val(pc->v_pie_t, v1);
      }
  
    return error;
  }
void pie_chart_clear_sections(struct pie_chart *pc)
  {
    gint i=0;
    gint len=(pc->sections->len);

    if(len>0)
      {
        for(i=len;i>0;i--) g_array_remove_index_fast(pc->sections, i-1);
        for(i=len;i>0;i--) g_array_remove_index_fast(pc->v_pie, i-1);
        for(i=len;i>0;i--) g_array_remove_index_fast(pc->v_pie_t, i-1);
      }
    else
      {
        g_warning("The sections are already cleared.\n");
      }

  }
void pie_chart_append_section_label(struct pie_chart *pc, guint section, const gchar *label)
  {
    if(section<=(pc->sections->len))
      {
        gchar *string=g_strdup(label);
        g_ptr_array_add(pc->labels, (gpointer)string);
      }
    else
      {
        g_warning("The pie chart needs a section to add a label to.\n");
      }
  }
void pie_chart_clear_section_labels(struct pie_chart *pc)
  {
    gint i=0;

    if((pc->labels->len)>0)
      {
        gint len=(pc->labels->len);
        for(i=len;i>0;i--) g_ptr_array_remove_index_fast(pc->labels, i-1);
      } 
  }
void pie_chart_explode_section(struct pie_chart *pc, gint section_num, gdouble radius)
  {
    guint len=(pc->sections->len);
    if(len>0&&section_num>=0&&section_num<len&&radius>=0.0)
      {
        struct section *s1=&g_array_index(pc->sections, struct section, section_num);
        s1->section_explode=radius;
      }
    else
      {
        g_warning("The radius range is >=0.0 and the section needs to be present.\n");
      }
  }
void pie_chart_explode_all(struct pie_chart *pc, gdouble radius)
  {
    gint i=0;
    struct section *s1=&g_array_index(pc->sections, struct section, 0);

    if(radius>=0.0)
      {
        for(i=0;i<(pc->sections->len);i++)
          {
            s1->section_explode=radius;
            s1++;
          }
      }
    else
      {
        g_warning("The radius range is >=0.0.\n");
      }
  }
void pie_chart_set_z(struct pie_chart *pc, gdouble z)
  {
    gint i=0;
    struct pie_arc *p1=&g_array_index(pc->pie_chart1, struct pie_arc, 0);

    pc->depth=z;
    for(i=0;i<(pc->pie_chart1->len);i++)
      {
        (p1->a1).z=z;
        (p1->a2).z=z;
        (p1->a3).z=z;
        (p1->a4).z=z;
        p1++;
      }

     p1=&g_array_index(pc->pie_chart2, struct pie_arc, 0);
     for(i=0;i<(pc->pie_chart2->len);i++)
      {
        (p1->a1).z=-z;
        (p1->a2).z=-z;
        (p1->a3).z=-z;
        (p1->a4).z=-z;
        p1++;
      }
  }
void pie_chart_free(struct pie_chart *pc)
  {
    if((pc->pie_chart1)!=NULL) g_array_free((pc->pie_chart1), TRUE);
    if((pc->pie_chart1_t)!=NULL) g_array_free((pc->pie_chart1_t), TRUE);
    if((pc->pie_chart2)!=NULL) g_array_free((pc->pie_chart2), TRUE);
    if((pc->pie_chart2_t)!=NULL) g_array_free((pc->pie_chart2_t), TRUE);
    if((pc->v_pie)!=NULL) g_array_free((pc->v_pie), TRUE);
    if((pc->v_pie_t)!=NULL) g_array_free((pc->v_pie_t), TRUE);
    if((pc->sections)!=NULL) g_array_free((pc->sections), TRUE);
    if((pc->labels)!=NULL) g_ptr_array_free((pc->labels), TRUE);
    g_free(pc);
    pc=NULL;
  }
static void initialize_bezier_ring(GArray **ring, GArray **ring_t, guint pieces, gdouble radius, gdouble z)
  {
    /*
         Bezier control points. 
         https://en.wikipedia.org/wiki/Composite_B%C3%A9zier_curve#Approximating_circular_arcs 

         Get the points in one arc of the ring and rotate them for the other points in the ring.
    */  
    gint i=0;
    //Get one section arc.
    gdouble arc_top=-(2.0*G_PI/pieces)/2.0;
    gdouble arc_offset=-2.0*G_PI/pieces;
    gdouble start_x=radius*cos(arc_top);
    gdouble start_y=radius*sin(arc_top);
    gdouble end_x=start_x;
    gdouble end_y=-start_y;
    gdouble c1_x=(4.0*radius-start_x)/3.0;
    gdouble c1_y=((radius-start_x)*(3.0*radius-start_x))/(3.0*start_y);
    gdouble c2_x=c1_x;
    gdouble c2_y=-c1_y;
    gdouble offset1=0.0;
    gdouble offset2=0.0;

    *ring=g_array_sized_new(FALSE, FALSE, sizeof(struct pie_arc), pieces);
    g_array_set_size(*ring, pieces);
    *ring_t=g_array_sized_new(FALSE, FALSE, sizeof(struct pie_arc), pieces);
    g_array_set_size(*ring_t, pieces);

    struct pie_arc *p1=&g_array_index(*ring, struct pie_arc, 0);
    struct pie_arc *p2=&g_array_index(*ring, struct pie_arc, 1);

    //Rotate the points in the section to align with the zero point on the unit circle.
    offset1=cos(arc_top);
    offset2=sin(arc_top);
    (p1->a4).x=start_x*offset1-start_y*offset2;
    (p1->a4).y=start_x*offset2+start_y*offset1;
    (p1->a4).z=z;
    (p1->a3).x=c1_x*offset1-c1_y*offset2;
    (p1->a3).y=c1_x*offset2+c1_y*offset1;
    (p1->a3).z=z;
    (p1->a2).x=c2_x*offset1-c2_y*offset2;
    (p1->a2).y=c2_x*offset2+c2_y*offset1;
    (p1->a2).z=z;
    (p1->a1).x=end_x*offset1-end_y*offset2;
    (p1->a1).y=end_x*offset2+end_y*offset1;
    (p1->a1).z=z;

    offset1=cos(arc_offset);
    offset2=sin(arc_offset);
    for(i=1;i<pieces;i++)
      {
        (p2->a1).x=(p1->a1).x*offset1-(p1->a1).y*offset2;
        (p2->a1).y=(p1->a1).x*offset2+(p1->a1).y*offset1;
        (p2->a1).z=z;
        (p2->a2).x=(p1->a2).x*offset1-(p1->a2).y*offset2;
        (p2->a2).y=(p1->a2).x*offset2+(p1->a2).y*offset1;
        (p2->a2).z=z;
        (p2->a3).x=(p1->a3).x*offset1-(p1->a3).y*offset2;
        (p2->a3).y=(p1->a3).x*offset2+(p1->a3).y*offset1;
        (p2->a3).z=z;
        (p2->a4).x=(p1->a4).x*offset1-(p1->a4).y*offset2;
        (p2->a4).y=(p1->a4).x*offset2+(p1->a4).y*offset1;
        (p2->a4).z=z;
        p1++;p2++;
      }
  }
void draw_pie_chart(cairo_t *cr, struct pie_chart *pc, gdouble qrs[9], guint label_lines)
  {
    //An eploding 3d pie chart.
    gint i=0;
    gint j=0;
    gint offset=0;
    gdouble line_start=0.0;
    cairo_text_extents_t extents;

    cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, 24);

    reset_ring(pc->pie_chart1, pc->pie_chart1_t); 
    reset_ring(pc->pie_chart2, pc->pie_chart2_t);     
    rotate_ring(pc->pie_chart1_t, qrs);
    rotate_ring(pc->pie_chart2_t, qrs); 
    rotate_reference_vector(pc, qrs); 
    rotate_pie_reference_vectors(pc, qrs); 
      
    //Draw the pie chart. 60 arcs or pieces of pie.
    struct pie_arc *p1=NULL;
    struct pie_arc *p2=NULL;
    struct pie_point3d *pt=NULL;
    struct section *s1=NULL;

    /*
      Draw the bottom of pie chart. This doesn't need to be drawn for solid colors.
      Needed for transparent colors.
    */
    cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
    if((pc->top_t)[2]>0.0) p2=&g_array_index(pc->pie_chart2_t, struct pie_arc, 0);
    else p2=&g_array_index(pc->pie_chart1_t, struct pie_arc, 0);
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct pie_point3d, 0);

    for(i=0;i<(pc->sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;    
        cairo_save(cr);
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y)); 
        cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]);
        if((pc->top_t)[2]>0.0) cairo_move_to(cr, -(pc->depth)*(pc->top_t)[0], -(pc->depth)*(pc->top_t)[1]);
        else cairo_move_to(cr, (pc->depth)*(pc->top_t)[0], (pc->depth)*(pc->top_t)[1]);
        cairo_line_to(cr, (p2->a1).x, (p2->a1).y); 
        for(j=0;j<offset;j++)
          {
            cairo_curve_to(cr, (p2->a2).x, (p2->a2).y, (p2->a3).x, (p2->a3).y, (p2->a4).x, (p2->a4).y);
            p2++;
          }
        cairo_close_path(cr);
        if((pc->outline))
          {
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT);
            cairo_stroke(cr);
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
          }
        else
          {
            cairo_fill(cr);
          }
        cairo_restore(cr);
        s1++;pt++;
      }

    //An array to sort inside quads.
    struct pie_quad q;
    GArray *quads=g_array_new(FALSE, FALSE, sizeof(struct pie_quad));
     
    //Get the inside quad points.
    if((pc->top_t)[2]>0.0)
      {
        p1=&g_array_index(pc->pie_chart1_t, struct pie_arc, 0);
        p2=&g_array_index(pc->pie_chart2_t, struct pie_arc, 0);
      }
    else
      {
        p1=&g_array_index(pc->pie_chart2_t, struct pie_arc, 0);
        p2=&g_array_index(pc->pie_chart1_t, struct pie_arc, 0);
      } 

    s1=&g_array_index(pc->sections, struct section, 0);
    for(i=0;i<(pc->sections->len);i++)
      {
        q.color[0]=(s1->color)[0];q.color[1]=(s1->color)[1];q.color[2]=(s1->color)[2];q.color[3]=(s1->color)[3];  
        if((pc->top_t)[2]>0.0)
          {
            q.a.x=(pc->depth)*(pc->top_t)[0];q.a.y=(pc->depth)*(pc->top_t)[1];q.a.z=(pc->depth)*(pc->top_t)[2];
          }
        else
          {
            q.a.x=-(pc->depth)*(pc->top_t)[0];q.a.y=-(pc->depth)*(pc->top_t)[1];q.a.z=-(pc->depth)*(pc->top_t)[2];
          }
        q.b.x=(p1->a1).x;q.b.y=(p1->a1).y;q.b.z=(p1->a1).z;
        q.c.x=(p2->a1).x;q.c.y=(p2->a1).y;q.c.z=(p2->a1).z;
        if((pc->top_t)[2]>0.0)
          {
            q.d.x=-(pc->depth)*(pc->top_t)[0];q.d.y=-(pc->depth)*(pc->top_t)[1];q.d.z=-(pc->depth)*(pc->top_t)[2];
          }
        else
          {
            q.d.x=(pc->depth)*(pc->top_t)[0];q.d.y=(pc->depth)*(pc->top_t)[1];q.d.z=(pc->depth)*(pc->top_t)[2];
          }
        q.section_id=(s1->section_id);
        q.section_explode=(s1->section_explode);
        g_array_append_val(quads, q);

        //Second side in pie section.
        offset=(s1->end)-(s1->start);
        p1+=offset;p2+=offset; 
        q.color[0]=(s1->color)[0];q.color[1]=(s1->color)[1];q.color[2]=(s1->color)[2];q.color[3]=(s1->color)[3];  
        if((pc->top_t)[2]>0.0)
          {
            q.a.x=(pc->depth)*(pc->top_t)[0];q.a.y=(pc->depth)*(pc->top_t)[1];q.a.z=(pc->depth)*(pc->top_t)[2];
          }
        else
          {
            q.a.x=-(pc->depth)*(pc->top_t)[0];q.a.y=-(pc->depth)*(pc->top_t)[1];q.a.z=-(pc->depth)*(pc->top_t)[2];
          }
        q.b.x=(p1->a4).x;q.b.y=(p1->a4).y;q.b.z=(p1->a4).z;
        q.c.x=(p2->a4).x;q.c.y=(p2->a4).y;q.c.z=(p2->a4).z;
        if((pc->top_t)[2]>0.0)
          {
            q.d.x=-(pc->depth)*(pc->top_t)[0];q.d.y=-(pc->depth)*(pc->top_t)[1];q.d.z=-(pc->depth)*(pc->top_t)[2];
          }
        else
          {
            q.d.x=(pc->depth)*(pc->top_t)[0];q.d.y=(pc->depth)*(pc->top_t)[1];q.d.z=(pc->depth)*(pc->top_t)[2];
          }
        q.section_id=(s1->section_id);
        q.section_explode=(s1->section_explode);
        g_array_append_val(quads, q);
        p1++;p2++;s1++;
      }

    //Sort and draw the inside quads.
    g_array_sort(quads, compare_quads);
    struct pie_quad *q1=&g_array_index(quads, struct pie_quad, 0);
    pt=&g_array_index(pc->v_pie_t, struct pie_point3d, 0);
    for(i=0;i<(quads->len);i++)
      {
        cairo_save(cr);
        cairo_translate(cr, (q1->section_explode)*((pt+(q1->section_id))->x), (q1->section_explode)*((pt+(q1->section_id))->y));        
        cairo_set_source_rgba(cr, (q1->color)[0], (q1->color)[1], (q1->color)[2], (q1->color)[3]); 
        cairo_move_to(cr, (q1->a).x, (q1->a).y);
        cairo_line_to(cr, (q1->b).x, (q1->b).y);
        cairo_line_to(cr, (q1->c).x, (q1->c).y);
        cairo_line_to(cr, (q1->d).x, (q1->d).y);
        cairo_close_path(cr);
        cairo_fill(cr);
        cairo_restore(cr);
        q1++;
      }
    g_array_free(quads, TRUE);

    //Draw circular outside sides of the pieces of the pie chart.
    if((pc->top_t)[2]>0.0)
      {
        p1=&g_array_index(pc->pie_chart1_t, struct pie_arc, 0);
        p2=&g_array_index(pc->pie_chart2_t, struct pie_arc, 0);
      }
    else
      {
        p1=&g_array_index(pc->pie_chart2_t, struct pie_arc, 0);
        p2=&g_array_index(pc->pie_chart1_t, struct pie_arc, 0);
      } 
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct pie_point3d, 0);

    for(i=0;i<(pc->sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;
        cairo_save(cr);
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y));
        for(j=0;j<offset;j++)
          {
            if((p1->a1).z>0.0)
              {
                cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]); 
                cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
                cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
                cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
                cairo_curve_to(cr, (p2->a3).x, (p2->a3).y, (p2->a2).x, (p2->a2).y, (p2->a1).x, (p2->a1).y);
                cairo_close_path(cr);
                cairo_fill(cr);
              }
            p1++;p2++;
          }
        cairo_restore(cr);
        s1++;pt++;
      }

    //Draw the top of pie chart.
    if((pc->top_t)[2]>0.0) p1=&g_array_index(pc->pie_chart1_t, struct pie_arc, 0);
    else p1=&g_array_index(pc->pie_chart2_t, struct pie_arc, 0); 
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct pie_point3d, 0);

    for(i=0;i<(pc->sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;    
        cairo_save(cr);
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y)); 
        cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]);
        if((pc->top_t)[2]>0.0) cairo_move_to(cr, (pc->depth)*(pc->top_t)[0], (pc->depth)*(pc->top_t)[1]);
        else cairo_move_to(cr, -(pc->depth)*(pc->top_t)[0], -(pc->depth)*(pc->top_t)[1]);
        cairo_line_to(cr, (p1->a1).x, (p1->a1).y); 
        for(j=0;j<offset;j++)
          {
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            p1++;
          }
        cairo_close_path(cr);
        if((pc->outline))
          {
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT);
            cairo_stroke(cr);
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
          }
        else
          {
            cairo_fill(cr);
          }

        //Add label lines.
        if(label_lines)
          {
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_set_line_width(cr, 3.0);
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT); 
            line_start=(pc->radius)/1.5;
            cairo_move_to(cr, (pt->x)*line_start, (pt->y)*line_start);
            cairo_line_to(cr, (pt->x)*line_start*1.8, (pt->y)*line_start*1.8);
            if((pt->x)>0) cairo_rel_line_to(cr, line_start/2.0, 0.0);
            else cairo_rel_line_to(cr, -line_start/2.0, 0.0);
            cairo_stroke(cr);

            if(i<(pc->labels->len))
              {
                cairo_text_extents(cr, (gchar*)g_ptr_array_index(pc->labels, i), &extents);
                if((pt->x)>0) cairo_move_to(cr, (pt->x)*line_start*1.8+line_start/2.0+8.0, (pt->y)*line_start*1.8+extents.height/4.0);
                else cairo_move_to(cr, (pt->x)*line_start*1.8-line_start/2.0-extents.width-8.0, (pt->y)*line_start*1.8+extents.height/4.0);             
                cairo_show_text(cr, (gchar*)g_ptr_array_index(pc->labels, i));
              }
          }

        cairo_restore(cr);
        s1++;pt++;
      }

  }
void draw_pie_chart_ring(cairo_t *cr, struct pie_chart *pc, gdouble qrs[9], gdouble inside_ring, guint label_lines)
  {
    /*
      Draw a pie chart ring. It isn't z-sorted so the quads can be drawn out of order. Use cairo
      to transform and draw the inside ring from the rotated outside rings.
    */
    gint i=0;
    gint j=0;
    gint offset=0;
    gdouble line_start=0.0;
    gdouble line_length=0.0;
    cairo_text_extents_t extents;

    cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, 24);

    if(inside_ring>0.95||inside_ring<0.0)
      {
        inside_ring=0.5;
        g_warning("Inside ring bounds, 0.0<=inside_ring<=0.95. Default inside_ring set to 0.50.\n");
      }  

    reset_ring(pc->pie_chart1, pc->pie_chart1_t); 
    reset_ring(pc->pie_chart2, pc->pie_chart2_t);     
    rotate_ring(pc->pie_chart1_t, qrs);
    rotate_ring(pc->pie_chart2_t, qrs); 
    rotate_reference_vector(pc, qrs); 
    rotate_pie_reference_vectors(pc, qrs);
    gdouble scale=inside_ring;
    gdouble scale_inv=(1.0/scale)*(1.0-scale);      
 
    struct pie_arc *p1=NULL;
    struct pie_arc *p2=NULL;
    struct pie_point3d *pt=NULL;
    struct section *s1=NULL;

    //Draw the bottom of pie chart.
    if((pc->top_t)[2]>0.0) p2=&g_array_index(pc->pie_chart2_t, struct pie_arc, 0);
    else p2=&g_array_index(pc->pie_chart1_t, struct pie_arc, 0);
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct pie_point3d, 0);

    cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); 
    for(i=0;i<(pc->sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;    
        cairo_save(cr); 
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y));        
        cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]);       
        cairo_move_to(cr, (p2->a1).x, (p2->a1).y); 
        for(j=0;j<offset;j++)
          {
            cairo_curve_to(cr, (p2->a2).x, (p2->a2).y, (p2->a3).x, (p2->a3).y, (p2->a4).x, (p2->a4).y);
            p2++;
          }
        cairo_scale(cr, scale, scale);
        if((pc->top_t)[2]<0.0) cairo_translate(cr, scale_inv*pc->top_t[0]*pc->depth, scale_inv*pc->top_t[1]*pc->depth);
        else cairo_translate(cr, scale_inv*pc->top_t[0]*(-pc->depth), scale_inv*pc->top_t[1]*(-pc->depth));
        p2--;
        cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
        for(j=0;j<offset;j++)
          {
            cairo_curve_to(cr, ((p2-j)->a3).x, ((p2-j)->a3).y, ((p2-j)->a2).x, ((p2-j)->a2).y, ((p2-j)->a1).x, ((p2-j)->a1).y);
          }
        p2++;
        cairo_close_path(cr);
        if((pc->outline))
          {
            cairo_fill_preserve(cr);
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT); 
            gdouble dx=1.0;
            gdouble dy=0.0;
            cairo_device_to_user_distance(cr, &dx, &dy);
            cairo_set_line_width(cr, 2.0*dx); 
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_stroke(cr);
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); 
          }
        else
          {
            cairo_fill(cr);
          }
        cairo_restore(cr);
        s1++;pt++;
      }

    //Draw one end cap of the sections of the pie chart.
    if((pc->top_t)[2]>0.0)
      {
        p1=&g_array_index(pc->pie_chart1_t, struct pie_arc, 0);
        p2=&g_array_index(pc->pie_chart2_t, struct pie_arc, 0);
      }
    else
      {
        p1=&g_array_index(pc->pie_chart2_t, struct pie_arc, 0);
        p2=&g_array_index(pc->pie_chart1_t, struct pie_arc, 0);
      } 
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct pie_point3d, 0);

    for(i=0;i<(pc->sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;
        cairo_save(cr);    
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y));        
        cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]);       
        cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
        cairo_save(cr);
        cairo_scale(cr, scale, scale);
        if((pc->top_t)[2]>0.0) cairo_translate(cr, scale_inv*pc->top_t[0]*pc->depth, scale_inv*pc->top_t[1]*pc->depth);
        else cairo_translate(cr, scale_inv*pc->top_t[0]*(-pc->depth), scale_inv*pc->top_t[1]*(-pc->depth));        
        cairo_line_to(cr, (p1->a1).x, (p1->a1).y); 
        cairo_restore(cr);
        cairo_save(cr);
        cairo_scale(cr, scale, scale);
        if((pc->top_t)[2]<0.0) cairo_translate(cr, scale_inv*pc->top_t[0]*pc->depth, scale_inv*pc->top_t[1]*pc->depth);
        else cairo_translate(cr, scale_inv*pc->top_t[0]*(-pc->depth), scale_inv*pc->top_t[1]*(-pc->depth));
        cairo_line_to(cr, (p2->a1).x, (p2->a1).y);
        cairo_restore(cr);
        cairo_line_to(cr, (p2->a1).x, (p2->a1).y);
        cairo_close_path(cr);
        cairo_fill(cr);
        cairo_restore(cr);
        p1+=offset;p2+=offset;
        s1++;pt++;
      }

    //Draw the second end cap of the sections of the pie chart.
    if((pc->top_t)[2]>0.0)
      {
        p1=&g_array_index(pc->pie_chart1_t, struct pie_arc, 0);
        p2=&g_array_index(pc->pie_chart2_t, struct pie_arc, 0);
      }
    else
      {
        p1=&g_array_index(pc->pie_chart2_t, struct pie_arc, 0);
        p2=&g_array_index(pc->pie_chart1_t, struct pie_arc, 0);
      } 
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct pie_point3d, 0);

    offset=(s1->end)-(s1->start);
    p1+=offset;p2+=offset;
    for(i=0;i<(pc->sections->len);i++)
      {
        cairo_save(cr);    
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y));        
        cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]);       
        cairo_move_to(cr, (p1->a4).x, (p1->a4).y);
        cairo_save(cr);
        cairo_scale(cr, scale, scale);
        if((pc->top_t)[2]>0.0) cairo_translate(cr, scale_inv*pc->top_t[0]*pc->depth, scale_inv*pc->top_t[1]*pc->depth);
        else cairo_translate(cr, scale_inv*pc->top_t[0]*(-pc->depth), scale_inv*pc->top_t[1]*(-pc->depth));        
        cairo_line_to(cr, (p1->a4).x, (p1->a4).y); 
        cairo_restore(cr);
        cairo_save(cr);
        cairo_scale(cr, scale, scale);
        if((pc->top_t)[2]<0.0) cairo_translate(cr, scale_inv*pc->top_t[0]*pc->depth, scale_inv*pc->top_t[1]*pc->depth);
        else cairo_translate(cr, scale_inv*pc->top_t[0]*(-pc->depth), scale_inv*pc->top_t[1]*(-pc->depth));
        cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
        cairo_restore(cr);
        cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
        cairo_close_path(cr);
        cairo_fill(cr);
        cairo_restore(cr);
        s1++;pt++;
        offset=(s1->end)-(s1->start)+1;
        p1+=offset;p2+=offset;
      }

    //Draw circular inside sides of the pieces of the pie chart.
    if((pc->top_t)[2]>0.0)
      {
        p1=&g_array_index(pc->pie_chart1_t, struct pie_arc, 0);
        p2=&g_array_index(pc->pie_chart2_t, struct pie_arc, 0);
      }
    else
      {
        p1=&g_array_index(pc->pie_chart2_t, struct pie_arc, 0);
        p2=&g_array_index(pc->pie_chart1_t, struct pie_arc, 0);
      } 
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct pie_point3d, 0);

    for(i=0;i<(pc->sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;
        cairo_save(cr);
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y));
        cairo_scale(cr, scale, scale);
        for(j=0;j<offset;j++)
          {
            cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]);
            //If drawing solid color only draw the back part of ring.
            if((s1->color)[3]<0.9||(p1->a1).z<0.0)
              {
                cairo_save(cr);
                if((pc->top_t)[2]>0.0) cairo_translate(cr, scale_inv*pc->top_t[0]*pc->depth, scale_inv*pc->top_t[1]*pc->depth);
                else cairo_translate(cr, scale_inv*pc->top_t[0]*(-pc->depth), scale_inv*pc->top_t[1]*(-pc->depth));
                cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
                cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
                cairo_restore(cr);
                cairo_save(cr);
                if((pc->top_t)[2]>0.0) cairo_translate(cr, scale_inv*pc->top_t[0]*(-pc->depth), scale_inv*pc->top_t[1]*(-pc->depth));
                else cairo_translate(cr, scale_inv*pc->top_t[0]*pc->depth, scale_inv*pc->top_t[1]*pc->depth);
                cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
                cairo_curve_to(cr, (p2->a3).x, (p2->a3).y, (p2->a2).x, (p2->a2).y, (p2->a1).x, (p2->a1).y);
                cairo_close_path(cr);
                cairo_fill(cr);
                cairo_restore(cr);
              }
            p1++;p2++;
          }
        cairo_restore(cr);
        s1++;pt++;
      }

    //Draw circular outside sides of the pieces of the pie chart.
    if((pc->top_t)[2]>0.0)
      {
        p1=&g_array_index(pc->pie_chart1_t, struct pie_arc, 0);
        p2=&g_array_index(pc->pie_chart2_t, struct pie_arc, 0);
      }
    else
      {
        p1=&g_array_index(pc->pie_chart2_t, struct pie_arc, 0);
        p2=&g_array_index(pc->pie_chart1_t, struct pie_arc, 0);
      } 
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct pie_point3d, 0);

    for(i=0;i<(pc->sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;
        cairo_save(cr);
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y));
        for(j=0;j<offset;j++)
          {
            cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]);
            //Only draw front of ring if color is solid.
            if((s1->color)[3]<0.9||(p1->a1).z>0.0)
              { 
                cairo_move_to(cr, (p1->a1).x, (p1->a1).y);
                cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
                cairo_line_to(cr, (p2->a4).x, (p2->a4).y);
                cairo_curve_to(cr, (p2->a3).x, (p2->a3).y, (p2->a2).x, (p2->a2).y, (p2->a1).x, (p2->a1).y);
                cairo_close_path(cr);
                cairo_fill(cr);
              }
            p1++;p2++;
          }
        cairo_restore(cr);
        s1++;pt++;
      }    

    //Draw the top of pie chart.
    if((pc->top_t)[2]>0.0) p1=&g_array_index(pc->pie_chart1_t, struct pie_arc, 0);
    else p1=&g_array_index(pc->pie_chart2_t, struct pie_arc, 0); 
    s1=&g_array_index(pc->sections, struct section, 0);
    pt=&g_array_index(pc->v_pie_t, struct pie_point3d, 0);

    for(i=0;i<(pc->sections->len);i++)
      {
        offset=(s1->end)-(s1->start)+1;    
        cairo_save(cr); 
        cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y));        
        cairo_set_source_rgba(cr, (s1->color)[0], (s1->color)[1], (s1->color)[2], (s1->color)[3]);       
        cairo_move_to(cr, (p1->a1).x, (p1->a1).y); 
        for(j=0;j<offset;j++)
          {
            cairo_curve_to(cr, (p1->a2).x, (p1->a2).y, (p1->a3).x, (p1->a3).y, (p1->a4).x, (p1->a4).y);
            p1++;
          }
        cairo_scale(cr, scale, scale);
        if((pc->top_t)[2]>0.0) cairo_translate(cr, scale_inv*pc->top_t[0]*pc->depth, scale_inv*pc->top_t[1]*pc->depth);
        else cairo_translate(cr, scale_inv*pc->top_t[0]*(-pc->depth), scale_inv*pc->top_t[1]*(-pc->depth));
        p1--;
        cairo_line_to(cr, (p1->a4).x, (p1->a4).y);
        for(j=0;j<offset;j++)
          {
            cairo_curve_to(cr, ((p1-j)->a3).x, ((p1-j)->a3).y, ((p1-j)->a2).x, ((p1-j)->a2).y, ((p1-j)->a1).x, ((p1-j)->a1).y);
          }
        p1++;
        cairo_close_path(cr);
        if((pc->outline))
          {
            cairo_fill_preserve(cr);
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            gdouble dx=1.0;
            gdouble dy=0.0;
            cairo_device_to_user_distance(cr, &dx, &dy);
            cairo_set_line_width(cr, 2.0*dx); 
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT); 
            cairo_stroke(cr);
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); 
          }
        else
          {
            cairo_fill(cr);
          }
        cairo_restore(cr);

        //Add label lines.
        if(label_lines)
          {
            cairo_save(cr);
            cairo_translate(cr, (s1->section_explode)*(pt->x), (s1->section_explode)*(pt->y)); 
            cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
            cairo_set_line_width(cr, 3.0);
            cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT); 
            line_start=(pc->radius)*(1.0+inside_ring)/2.0;
            line_length=(pc->radius)+36.0;
            cairo_move_to(cr, (pt->x)*line_start, (pt->y)*line_start);
            cairo_line_to(cr, (pt->x)*line_length, (pt->y)*line_length);
            if((pt->x)>0) cairo_rel_line_to(cr, line_start/2.0, 0.0);
            else cairo_rel_line_to(cr, -line_start/2.0, 0.0);
            cairo_stroke(cr);

            if(i<(pc->labels->len))
              {
                cairo_text_extents(cr, (gchar*)g_ptr_array_index(pc->labels, i), &extents);
                if((pt->x)>0) cairo_move_to(cr, (pt->x)*line_length+line_start/1.8+8.0, (pt->y)*line_length+extents.height/4.0);
                else cairo_move_to(cr, (pt->x)*line_length-line_start/1.8-extents.width-8.0, (pt->y)*line_length+extents.height/4.0);
                cairo_show_text(cr, (gchar*)g_ptr_array_index(pc->labels, i));
              }
            cairo_restore(cr);
          }

        s1++;pt++;
      }
  }
static void reset_ring(GArray *ring, GArray *ring_t)
  {
    struct pie_arc *p1=&g_array_index(ring, struct pie_arc, 0);
    struct pie_arc *p2=&g_array_index(ring_t, struct pie_arc, 0);
    memcpy(p2, p1, sizeof(struct pie_arc)*(ring->len));
  }
void pie_quaternion_rotation(gdouble yaw, gdouble roll, gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    roll=roll/2.0;
    pitch=pitch/2.0;
    yaw=yaw/2.0;
    gdouble qi=sin(pitch)*cos(roll)*cos(yaw)-cos(pitch)*sin(roll)*sin(yaw);
    gdouble qj=cos(pitch)*sin(roll)*cos(yaw)+sin(pitch)*cos(roll)*sin(yaw);
    gdouble qk=cos(pitch)*cos(roll)*sin(yaw)-sin(pitch)*sin(roll)*cos(yaw);
    gdouble qr=cos(pitch)*cos(roll)*cos(yaw)+sin(pitch)*sin(roll)*sin(yaw);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
static void rotate_reference_vector(struct pie_chart *pc, gdouble qrs[9])
  {
    //Rotate the top[] reference vector.
    gdouble cr1=(pc->top)[0];
    gdouble cr2=(pc->top)[1];
    gdouble cr3=(pc->top)[2];         
    (pc->top_t)[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
    (pc->top_t)[1]=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
    (pc->top_t)[2]=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);  
  }
static void rotate_pie_reference_vectors(struct pie_chart *pc, gdouble qrs[9])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0; 
    struct pie_point3d *p1=&g_array_index(pc->v_pie, struct pie_point3d, 0);
    struct pie_point3d *p2=&g_array_index(pc->v_pie_t, struct pie_point3d, 0);

    for(i=0;i<(pc->v_pie->len);i++)
      {
        cr1=(p1->x);
        cr2=(p1->y);
        cr3=(p1->z);          
        (p2->x)=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p2->y)=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p2->z)=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]); 
        p1++;p2++;
      } 
  }
static void rotate_ring(GArray *ring, gdouble qrs[9])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0;   
    struct pie_arc *p1=&g_array_index(ring, struct pie_arc, 0);

    for(i=0;i<(ring->len);i++)
      {
        cr1=(p1->a1).x;
        cr2=(p1->a1).y;
        cr3=(p1->a1).z;         
        (p1->a1).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p1->a1).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p1->a1).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p1->a2).x;
        cr2=(p1->a2).y;
        cr3=(p1->a2).z;         
        (p1->a2).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p1->a2).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p1->a2).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p1->a3).x;
        cr2=(p1->a3).y;
        cr3=(p1->a3).z;         
        (p1->a3).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p1->a3).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p1->a3).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        cr1=(p1->a4).x;
        cr2=(p1->a4).y;
        cr3=(p1->a4).z;         
        (p1->a4).x=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        (p1->a4).y=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        (p1->a4).z=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        p1++;
      }  
  }
static int compare_quads(const void *q1, const void *q2)
  {
    return((((struct pie_quad*)q1)->a.z+((struct pie_quad*)q1)->b.z+((struct pie_quad*)q1)->c.z+((struct pie_quad*)q1)->d.z) - (((struct pie_quad*)q2)->a.z+((struct pie_quad*)q2)->b.z+((struct pie_quad*)q2)->c.z+((struct pie_quad*)q2)->d.z));
  }



