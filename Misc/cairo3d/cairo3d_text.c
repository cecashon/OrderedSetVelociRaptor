
/* 
    See cairo3d_text_main.c for the test program.
    
    gcc -Wall -c cairo3d_text.c -o cairo3d_text.o -I/usr/include/freetype2 -L/usr/local/lib `pkg-config --cflags --libs glib-2.0` `pkg-config --cflags --libs cairo` -lfreetype -lm

    Tested on Ubuntu16.04 and GTK3.18

    C. Eric Cashon
*/

#include"cairo3d_text.h"

static FT_Outline get_font_outline(FT_Face face, gdouble scale, const gchar *text, gint index, gdouble *box_w, gdouble *box_h, gdouble *max_advance_y, gdouble *max_drop_y, gboolean use_advance, gboolean single_char);
static void reset_plane_pts(const gdouble qrs_plane[9]);
static void rotate_plane(const gdouble qrs_plane[9]);
//Sort the quads from -z to +z for blur.
static int compare_quads_sides(const void *a, const void *b);

//A reference vector to keep track of the top or z direction of the drawing.
static gdouble v1[3]={0.0, 0.0, 1.0};
static gdouble v1_r[3]={0.0, 0.0, 1.0};
//A plane to project the char onto and a transformed plane for revolving the plane.
static gdouble plane_pts[9]={0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.5, 1.0, -1.0};
static gdouble plane_pts_t[9];
/*
  A spherical point to revolve or rotate on a spherical path with quaternions. This is used
  for the sun position in the shadow drawing. These variables are similar to revolve[] variables
  in cairo3d.c.
*/
static gdouble revolve_text[3]={1.0, 0.0, 0.0};
static gdouble revolve_text_r[3]={1.0, 0.0, 0.0};

void draw_path_contours_line(cairo_t *cr, GArray *path, gboolean clip)
 {
   gint i=0;
   struct segment2 *p1=&g_array_index(path, struct segment2, 0);
   for(i=0;i<(path->len);i++)
     {
       if((p1->move_id)==MOVE_TO_R||(p1->move_id)==MOVE_TO_L)
         {
           cairo_move_to(cr, p1->x3, p1->y3);           
         }
       else if((p1->move_id)==LINE_TO)
         {
           cairo_line_to(cr, p1->x3, p1->y3); 
         }
       else if((p1->move_id)==CURVE_TO)
         {
           cairo_curve_to(cr, p1->x1, p1->y1, p1->x2, p1->y2, p1->x3, p1->y3);
         }
       else if((p1->move_id)==LINE_TO_STROKE)
         {
           cairo_line_to(cr, p1->x3, p1->y3);
           //Just draw the inside part of the line when clipped.
           cairo_save(cr);
           if(clip) cairo_clip_preserve(cr);
           cairo_stroke(cr);
           cairo_restore(cr);
         }
       else //((p1->move_id)==CURVE_TO_STROKE)
         {
           cairo_curve_to(cr, p1->x1, p1->y1, p1->x2, p1->y2, p1->x3, p1->y3);
           //Just draw the inside part of the line when clipped.
           cairo_save(cr);
           if(clip) cairo_clip_preserve(cr);
           cairo_stroke(cr);
           cairo_restore(cr); 
         }
       p1++;
     }
 }
void draw_path_contours_connect(cairo_t *cr, GArray *path1, GArray *path2)
 {
   gint i=0;
   struct segment2 *p1=&g_array_index(path1, struct segment2, 0);
   struct segment2 *p2=&g_array_index(path2, struct segment2, 0);

   for(i=0;i<(path1->len);i++)
     {      
       cairo_move_to(cr, p1->x3, p1->y3);
       cairo_line_to(cr, p2->x3, p2->y3);
       cairo_stroke(cr);        
       p1++;p2++;          
     }
 }
void draw_path_side_fill(cairo_t *cr, GArray *path1, GArray *path2, gdouble rgba[4])
 {
   gint i=0;
   gint j=0;
   struct segment2 *p1=&g_array_index(path1, struct segment2, 0);
   struct segment2 *p2=&g_array_index(path2, struct segment2, 0);
   for(i=0;i<(path1->len-1);i++)
     {
       //If at the end of a contour move again.
       if((p1+1)->move_id==MOVE_TO_R||(p1+1)->move_id==MOVE_TO_L)
         {
           //Move to next point.
         }
       else
         {
           for(j=0;j<5;j++)
             {
               if(j==0)
                 {
                   cairo_move_to(cr, p1->x3, p1->y3);           
                 }
               else if(j==1&&(((p1+1)->move_id)==LINE_TO||((p1+1)->move_id)==LINE_TO_STROKE))
                 {
                   cairo_line_to(cr, (p1+1)->x3, (p1+1)->y3); 
                 }
               else if(j==1&&(((p1+1)->move_id)==CURVE_TO||((p1+1)->move_id)==CURVE_TO_STROKE))
                 {
                   cairo_curve_to(cr, (p1+1)->x1, (p1+1)->y1, (p1+1)->x2, (p1+1)->y2, (p1+1)->x3, (p1+1)->y3);
                 }
               else if(j==2)
                 {
                   cairo_line_to(cr, (p2+1)->x3, (p2+1)->y3); 
                 }
               else if(j==3&&(((p2+1)->move_id)==LINE_TO||((p2+1)->move_id)==LINE_TO_STROKE))
                 {
                   cairo_line_to(cr, p2->x3, p2->y3); 
                 }
               else if(j==3&&(((p2+1)->move_id)==CURVE_TO||((p2+1)->move_id)==CURVE_TO_STROKE))
                 {
                   cairo_curve_to(cr, (p2+1)->x2, (p2+1)->y2, (p2+1)->x1, (p2+1)->y1, p2->x3, p2->y3);
                 }
               else
                 {
                   cairo_close_path(cr);
                   cairo_fill(cr);
                 } 
             }         
         }
       p1++;p2++;
     }
 }
void draw_path_side_fill_blur(cairo_t *cr, GArray *path1, GArray *path2, gdouble rgba1[4], gdouble rgba2[4])
 {
   gint i=0;
   gint j=0;
   struct segment2 *p1=&g_array_index(path1, struct segment2, 0);
   struct segment2 *p2=&g_array_index(path2, struct segment2, 0);

   //An array to sort quads.
   struct quad q;
   GArray *quads=g_array_new(FALSE, FALSE, sizeof(struct quad));

   for(i=0;i<(path1->len-1);i++)
     {
       //If at the end of a contour move again.
       if((p1+1)->move_id==MOVE_TO_R||(p1+1)->move_id==MOVE_TO_L)
         {
           //Move to next point.
         }
       else
         {
           for(j=0;j<5;j++)
             {
               if(j==0)
                 {
                   //cairo_move_to(cr, p1->x3, p1->y3);
                   q.a.move_id=MOVE_TO_R;
                   q.a.x1=0.0;q.a.y1=0.0;q.a.z1=0.0;
                   q.a.x2=0.0;q.a.y2=0.0;q.a.z2=0.0;
                   q.a.x3=p1->x3;q.a.y3=p1->y3;q.a.z3=p1->z3;          
                 }
               else if(j==1&&(((p1+1)->move_id)==LINE_TO||((p1+1)->move_id)==LINE_TO_STROKE))
                 {
                   //cairo_line_to(cr, (p1+1)->x3, (p1+1)->y3);
                   q.b.move_id=LINE_TO; 
                   q.b.x1=0.0;q.b.y1=0.0;q.b.z1=0.0;
                   q.b.x2=0.0;q.b.y2=0.0;q.b.z2=0.0;
                   q.b.x3=(p1+1)->x3;q.b.y3=(p1+1)->y3;q.b.z3=(p1+1)->z3; 
                 }
               else if(j==1&&(((p1+1)->move_id)==CURVE_TO||((p1+1)->move_id)==CURVE_TO_STROKE))
                 {
                   //cairo_curve_to(cr, (p1+1)->x1, (p1+1)->y1, (p1+1)->x2, (p1+1)->y2, (p1+1)->x3, (p1+1)->y3);
                   q.b.move_id=CURVE_TO;
                   q.b.x1=(p1+1)->x1;q.b.y1=(p1+1)->y1;q.b.z1=(p1+1)->z1;
                   q.b.x2=(p1+1)->x2;q.b.y2=(p1+1)->y2;q.b.z2=(p1+1)->z2;
                   q.b.x3=(p1+1)->x3;q.b.y3=(p1+1)->y3;q.b.z3=(p1+1)->z3; 
                 }
               else if(j==2)
                 {
                   //cairo_line_to(cr, (p2+1)->x3, (p2+1)->y3);
                   q.c.move_id=LINE_TO;
                   q.c.x1=0.0;q.c.y1=0.0;q.c.z1=0.0;
                   q.c.x2=0.0;q.c.y2=0.0;q.c.z2=0.0;
                   q.c.x3=(p2+1)->x3;q.c.y3=(p2+1)->y3;q.c.z3=(p2+1)->z3; 
                 }
               else if(j==3&&(((p2+1)->move_id)==LINE_TO||((p2+1)->move_id)==LINE_TO_STROKE))
                 {
                   //cairo_line_to(cr, p2->x3, p2->y3); 
                   q.d.move_id=LINE_TO;
                   q.d.x1=0.0;q.d.y1=0.0;q.d.z1=0.0;
                   q.d.x2=0.0;q.d.y2=0.0;q.d.z2=0.0;
                   q.d.x3=p2->x3;q.d.y3=p2->y3;q.d.z3=p2->z3; 
                 }
               else if(j==3&&(((p2+1)->move_id)==CURVE_TO||((p2+1)->move_id)==CURVE_TO_STROKE))
                 {
                   //cairo_curve_to(cr, (p2+1)->x2, (p2+1)->y2, (p2+1)->x1, (p2+1)->y1, p2->x3, p2->y3);
                   q.d.move_id=CURVE_TO;
                   q.d.x1=(p2+1)->x2;q.d.y1=(p2+1)->y2;q.d.z1=(p2+1)->z2;
                   q.d.x2=(p2+1)->x1;q.d.y2=(p2+1)->y1;q.d.z2=(p2+1)->z1;
                   q.d.x3=p2->x3;q.d.y3=p2->y3;q.d.z3=p2->z3; 
                 }
               else
                 {
                   g_array_append_val(quads, q);
                 } 
             }         
         }
       p1++;p2++;
     }

   g_array_sort(quads, compare_quads_sides);

   struct quad *q1=&g_array_index(quads, struct quad, 0);
   for(i=0;i<quads->len;i++)
     {
       cairo_pattern_t *pattern1=cairo_pattern_create_mesh();
       cairo_mesh_pattern_begin_patch(pattern1);
       cairo_mesh_pattern_move_to(pattern1, (q1->a).x3, (q1->a).y3);
       if((q1->b).move_id==LINE_TO)
         {
           cairo_mesh_pattern_line_to(pattern1, (q1->b).x3, (q1->b).y3);
         }
       else
         {
           cairo_mesh_pattern_curve_to(pattern1, (q1->b).x1, (q1->b).y1, (q1->b).x2, (q1->b).y2, (q1->b).x3, (q1->b).y3);
         }
       cairo_mesh_pattern_line_to(pattern1, (q1->c).x3, (q1->c).y3);
       if((q1->d).move_id==LINE_TO)
         {
           cairo_mesh_pattern_line_to(pattern1, (q1->d).x3, (q1->d).y3);
         }
       else
         {
           cairo_mesh_pattern_curve_to(pattern1, (q1->d).x1, (q1->d).y1, (q1->d).x2, (q1->d).y2, (q1->d).x3, (q1->d).y3);
         } 
       cairo_mesh_pattern_set_corner_color_rgba(pattern1, 0, rgba1[0], rgba1[1], rgba1[2], rgba1[3]);
       cairo_mesh_pattern_set_corner_color_rgba(pattern1, 1, rgba1[0], rgba1[1], rgba1[2], rgba1[3]);
       cairo_mesh_pattern_set_corner_color_rgba(pattern1, 2, rgba2[0], rgba2[1], rgba2[2], rgba2[3]);
       cairo_mesh_pattern_set_corner_color_rgba(pattern1, 3, rgba2[0], rgba2[1], rgba2[2], rgba2[3]);
       cairo_mesh_pattern_end_patch(pattern1);
       cairo_set_source(cr, pattern1);

       cairo_move_to(cr, (q1->a).x3, (q1->a).y3);
       if((q1->b).move_id==LINE_TO)
         {
           cairo_line_to(cr, (q1->b).x3, (q1->b).y3);
         }
       else
         {
           cairo_curve_to(cr, (q1->b).x1, (q1->b).y1, (q1->b).x2, (q1->b).y2, (q1->b).x3, (q1->b).y3);
         }
       cairo_line_to(cr, (q1->c).x3, (q1->c).y3);
       if((q1->d).move_id==LINE_TO)
         {
           cairo_line_to(cr, (q1->d).x3, (q1->d).y3);
         }
       else
         {
           cairo_curve_to(cr, (q1->d).x1, (q1->d).y1, (q1->d).x2, (q1->d).y2, (q1->d).x3, (q1->d).y3);
         }
       cairo_close_path(cr);
       cairo_fill(cr);
       cairo_pattern_destroy(pattern1);
       q1++;
     }

   g_array_free(quads, TRUE);
 }
void draw_path_side_fill_blur_linear(cairo_t *cr, GArray *path1, GArray *path2, gdouble rgba1[4], gdouble rgba2[4], gboolean with_normal)
 {
   gint i=0;
   gint j=0;
   struct segment2 *p1=&g_array_index(path1, struct segment2, 0);
   struct segment2 *p2=&g_array_index(path2, struct segment2, 0);

   //An array to sort quads.
   struct quad q;
   GArray *quads=g_array_new(FALSE, FALSE, sizeof(struct quad));

   for(i=0;i<(path1->len-1);i++)
     {
       //If at the end of a contour move again.
       if((p1+1)->move_id==MOVE_TO_R||(p1+1)->move_id==MOVE_TO_L)
         {
           //Move to next point.
         }
       else
         {
           for(j=0;j<5;j++)
             {
               if(j==0)
                 {
                   //cairo_move_to(cr, p1->x3, p1->y3);
                   q.a.move_id=MOVE_TO_R;
                   q.a.x1=0.0;q.a.y1=0.0;q.a.z1=0.0;
                   q.a.x2=0.0;q.a.y2=0.0;q.a.z2=0.0;
                   q.a.x3=p1->x3;q.a.y3=p1->y3;q.a.z3=p1->z3;          
                 }
               else if(j==1&&(((p1+1)->move_id)==LINE_TO||((p1+1)->move_id)==LINE_TO_STROKE))
                 {
                   //cairo_line_to(cr, (p1+1)->x3, (p1+1)->y3);
                   q.b.move_id=LINE_TO; 
                   q.b.x1=0.0;q.b.y1=0.0;q.b.z1=0.0;
                   q.b.x2=0.0;q.b.y2=0.0;q.b.z2=0.0;
                   q.b.x3=(p1+1)->x3;q.b.y3=(p1+1)->y3;q.b.z3=(p1+1)->z3; 
                 }
               else if(j==1&&(((p1+1)->move_id)==CURVE_TO||((p1+1)->move_id)==CURVE_TO_STROKE))
                 {
                   //cairo_curve_to(cr, (p1+1)->x1, (p1+1)->y1, (p1+1)->x2, (p1+1)->y2, (p1+1)->x3, (p1+1)->y3);
                   q.b.move_id=CURVE_TO;
                   q.b.x1=(p1+1)->x1;q.b.y1=(p1+1)->y1;q.b.z1=(p1+1)->z1;
                   q.b.x2=(p1+1)->x2;q.b.y2=(p1+1)->y2;q.b.z2=(p1+1)->z2;
                   q.b.x3=(p1+1)->x3;q.b.y3=(p1+1)->y3;q.b.z3=(p1+1)->z3; 
                 }
               else if(j==2)
                 {
                   //cairo_line_to(cr, (p2+1)->x3, (p2+1)->y3);
                   q.c.move_id=LINE_TO;
                   q.c.x1=0.0;q.c.y1=0.0;q.c.z1=0.0;
                   q.c.x2=0.0;q.c.y2=0.0;q.c.z2=0.0;
                   q.c.x3=(p2+1)->x3;q.c.y3=(p2+1)->y3;q.c.z3=(p2+1)->z3; 
                 }
               else if(j==3&&(((p2+1)->move_id)==LINE_TO||((p2+1)->move_id)==LINE_TO_STROKE))
                 {
                   //cairo_line_to(cr, p2->x3, p2->y3); 
                   q.d.move_id=LINE_TO;
                   q.d.x1=0.0;q.d.y1=0.0;q.d.z1=0.0;
                   q.d.x2=0.0;q.d.y2=0.0;q.d.z2=0.0;
                   q.d.x3=p2->x3;q.d.y3=p2->y3;q.d.z3=p2->z3; 
                 }
               else if(j==3&&(((p2+1)->move_id)==CURVE_TO||((p2+1)->move_id)==CURVE_TO_STROKE))
                 {
                   //cairo_curve_to(cr, (p2+1)->x2, (p2+1)->y2, (p2+1)->x1, (p2+1)->y1, p2->x3, p2->y3);
                   q.d.move_id=CURVE_TO;
                   q.d.x1=(p2+1)->x2;q.d.y1=(p2+1)->y2;q.d.z1=(p2+1)->z2;
                   q.d.x2=(p2+1)->x1;q.d.y2=(p2+1)->y1;q.d.z2=(p2+1)->z1;
                   q.d.x3=p2->x3;q.d.y3=p2->y3;q.d.z3=p2->z3; 
                 }
               else
                 {
                   g_array_append_val(quads, q);
                 } 
             }         
         }
       p1++;p2++;
     }

   g_array_sort(quads, compare_quads_sides);

   struct quad *q1=&g_array_index(quads, struct quad, 0);
   gdouble normal=0.0;
   for(i=0;i<quads->len;i++)
     {
       if(with_normal) normal=(((q1->a).x3-(q1->b).x3)*((q1->a).y3-(q1->d).y3))-(((q1->a).y3-(q1->b).y3)*((q1->a).x3-(q1->d).x3));
       if(!with_normal||normal>0.0)
         {
           cairo_pattern_t *pattern1=cairo_pattern_create_linear((q1->b).x3, (q1->b).y3, (q1->c).x3, (q1->c).y3);
           cairo_pattern_add_color_stop_rgba(pattern1, 0.0, rgba1[0], rgba1[1], rgba1[2], rgba1[3]);
           cairo_pattern_add_color_stop_rgba(pattern1, 1.0, rgba2[0], rgba2[1], rgba2[2], rgba2[3]);
           cairo_move_to(cr, (q1->a).x3, (q1->a).y3);
           if((q1->b).move_id==LINE_TO)
             {
               cairo_line_to(cr, (q1->b).x3, (q1->b).y3);
             }
           else
             {
               cairo_curve_to(cr, (q1->b).x1, (q1->b).y1, (q1->b).x2, (q1->b).y2, (q1->b).x3, (q1->b).y3);
             }
           cairo_line_to(cr, (q1->c).x3, (q1->c).y3);
           if((q1->d).move_id==LINE_TO)
             {
               cairo_line_to(cr, (q1->d).x3, (q1->d).y3);
             }
           else
             {
               cairo_curve_to(cr, (q1->d).x1, (q1->d).y1, (q1->d).x2, (q1->d).y2, (q1->d).x3, (q1->d).y3);
             } 
       
           cairo_close_path(cr);
           cairo_set_source(cr, pattern1);
           cairo_fill(cr);
           cairo_pattern_destroy(pattern1);
         }
       q1++;
     }

   g_array_free(quads, TRUE);
 }
void draw_path_contours_fill(cairo_t *cr, GArray *path)
 {
   gint i=0;
   struct segment2 *p1=&g_array_index(path, struct segment2, 0);

   for(i=0;i<(path->len);i++)
     {
       if((p1->move_id)==MOVE_TO_R||(p1->move_id)==MOVE_TO_L)
         {
           //For counter clockwise fill. 
           if((p1->move_id)==MOVE_TO_L)
             {
               //cairo_set_fill_rule(cr, CAIRO_FILL_RULE_EVEN_ODD);
               cairo_set_fill_rule(cr, CAIRO_FILL_RULE_WINDING);
               cairo_new_sub_path(cr);
             }
           cairo_move_to(cr, p1->x3, p1->y3);           
         }
       else if((p1->move_id)==LINE_TO)
         {
           cairo_line_to(cr, p1->x3, p1->y3);
         }
       else if((p1->move_id)==CURVE_TO)
         {
           cairo_curve_to(cr, p1->x1, p1->y1, p1->x2, p1->y2, p1->x3, p1->y3);
         }
       else if((p1->move_id)==LINE_TO_STROKE)
         {
           cairo_line_to(cr, p1->x3, p1->y3);
           cairo_close_path(cr);
         }
       else //((p1->move_id)==CURVE_TO_STROKE)
         {
           cairo_curve_to(cr, p1->x1, p1->y1, p1->x2, p1->y2, p1->x3, p1->y3);
           cairo_close_path(cr);
         }
       p1++;     
     }
   cairo_fill(cr);
 }
struct text3d* text3d_new(FT_Face face, GString *string, gdouble scale, gdouble spacing, gdouble z_depth, gboolean use_advance)
  {   
    gint i=0;
    gint j=0;
    GArray *path1=NULL;
    GArray *path2=NULL;
    struct segment2 *p1=NULL;
    struct segment2 *p2=NULL;
    gdouble max_advance_y=0.0;
    gdouble max_drop_y=0.0;

    //Initialize text for cairo3d. 
    GString *text=g_string_new(string->str);  
    GArray *box_size=g_array_new(FALSE, FALSE, sizeof(gdouble));
    GArray *index=g_array_new(FALSE, FALSE, sizeof(gint));
    //This text string is in order.
    for(i=0;i<string->len;i++) g_array_append_val(index, i);    
    GPtrArray *cairo_text=get_font_paths_3d(face, scale, string->str, index, box_size, &max_advance_y, &max_drop_y, use_advance, FALSE);
    g_array_free(index, TRUE);

    gdouble total_width=0.0;
    for(i=0;i<(box_size->len);i+=2)
      {
        total_width+=g_array_index(box_size, gdouble, i)+spacing;
      }
    total_width-=spacing;

    //Add the text width and height to the end of the box_size array.
    gdouble move_y=max_advance_y+max_drop_y;
    g_array_append_val(box_size, total_width);
    g_array_append_val(box_size, move_y);

    //Translate characters to center of rotation.
    gdouble move_x=-total_width/2.0;
    move_y=(max_advance_y+max_drop_y)/2.0-max_drop_y;    
    for(i=0;i<(cairo_text->len); i+=2)
      {
        path1=(GArray*)g_ptr_array_index(cairo_text, i);
        if(path1!=NULL) p1=&g_array_index(path1, struct segment2, 0);
        path2=(GArray*)g_ptr_array_index(cairo_text, i+1);
        if(path2!=NULL) p2=&g_array_index(path2, struct segment2, 0);
        move_x+=g_array_index(box_size, gdouble, i)/2.0;  
        if(path1!=NULL)
          {
            for(j=0;j<path1->len;j++)
              {
                (p1->x1)+=move_x;
                (p1->x2)+=move_x;
                (p1->x3)+=move_x;
                (p2->x1)+=move_x;
                (p2->x2)+=move_x;
                (p2->x3)+=move_x;
            
                (p1->y1)+=move_y;
                (p1->y2)+=move_y;
                (p1->y3)+=move_y;
                (p2->y1)+=move_y;
                (p2->y2)+=move_y;
                (p2->y3)+=move_y;
            
                p1++;p2++;
              } 
          }
        move_x+=g_array_index(box_size, gdouble, i)/2.0+spacing;       
      }   
  
    //Split slider extrude to keep z-axis centered for 3d text.
    for(i=0;i<(cairo_text->len); i+=2)
      {
        path1=(GArray*)g_ptr_array_index(cairo_text, i);
        if(path1!=NULL) p1=&g_array_index(path1, struct segment2, 0);
        path2=(GArray*)g_ptr_array_index(cairo_text, i+1);
        if(path2!=NULL) p2=&g_array_index(path2, struct segment2, 0);
        if(path1!=NULL)
          {
            for(j=0;j<path1->len;j++)
              {
                (p1->z1)=z_depth;
                (p1->z2)=z_depth;
                (p1->z3)=z_depth;
                (p2->z1)=-z_depth;
                (p2->z2)=-z_depth;
                (p2->z3)=-z_depth;
                p1++;p2++;
              } 
          }
       } 

     //Initialize the transformed array
     GPtrArray *cairo_text_t=g_ptr_array_new();
     for(i=0;i<(cairo_text->len); i++)
       {
         path1=(GArray*)g_ptr_array_index(cairo_text, i);
         if(path1!=NULL)
           {
             path2=g_array_sized_new(FALSE, FALSE, sizeof(struct segment2), path1->len);
             p1=&g_array_index(path1, struct segment2, 0);
             p2=&g_array_index(path2, struct segment2, 0);
             memcpy(p2, p1, sizeof(struct segment2)*(path1->len));
             g_array_set_size(path2, path1->len);
             g_ptr_array_add(cairo_text_t, path2);
           }
         else g_ptr_array_add(cairo_text_t, NULL);
       }

    struct text3d *p=g_new(struct text3d, 1);
    p->text=text;
    p->cairo_text=cairo_text;
    p->cairo_text_t=cairo_text_t;
    p->box_size=box_size;
    p->spacing=spacing; 

    return p;  
  }
void reset_text3d(struct text3d *t3d)
  {
    //Reset the transformed text.
    gint i=0;
    GArray *path1=NULL;
    GArray *path2=NULL;
    struct segment2 *p1=NULL;
    struct segment2 *p2=NULL;
    for(i=0;i<(t3d->cairo_text->len);i++)
      {
        path1=(GArray*)g_ptr_array_index(t3d->cairo_text, i);
        path2=(GArray*)g_ptr_array_index(t3d->cairo_text_t, i);
        if(path1!=NULL&&path2!=NULL)
          {
            p1=&g_array_index(path1, struct segment2, 0);
            p2=&g_array_index(path2, struct segment2, 0);
            memcpy(p2, p1, sizeof(struct segment2)*(path1->len));
          }
      }
  }
void free_text3d(struct text3d *t3d)
  {
    gint i=0;

    if(t3d!=NULL)
      {
        if(t3d->cairo_text!=NULL)
          {
            for(i=0;i<(t3d->cairo_text->len);i++)
              {
                if((g_ptr_array_index(t3d->cairo_text, i))!=NULL)
                  {
                    g_array_free((GArray*)g_ptr_array_index(t3d->cairo_text, i), TRUE);
                  }
              }
            g_ptr_array_free(t3d->cairo_text, TRUE);
          }
        if(t3d->cairo_text_t!=NULL)
          {
            for(i=0;i<(t3d->cairo_text_t->len);i++)
              {
                if((g_ptr_array_index(t3d->cairo_text_t, i))!=NULL)
                  {
                    g_array_free((GArray*)g_ptr_array_index(t3d->cairo_text_t, i), TRUE);
                  }
              }
            g_ptr_array_free(t3d->cairo_text_t, TRUE);
          } 
        if(t3d->box_size!=NULL) g_array_free(t3d->box_size, TRUE);
        if(t3d->text!=NULL) g_string_free(t3d->text, TRUE);
        g_free(t3d);
      } 
  }
struct text2d* text2d_new(FT_Face face, GString *string, gdouble scale, gdouble spacing, gdouble z_depth, gboolean use_advance)
  {   
    gint i=0;
    gint j=0;
    GArray *path1=NULL;
    GArray *path2=NULL;
    struct segment2 *p1=NULL;
    struct segment2 *p2=NULL;
    gdouble max_advance_y=0.0;
    gdouble max_drop_y=0.0;

    //Initialize text. 
    GString *text=g_string_new(string->str);  
    GArray *box_size=g_array_new(FALSE, FALSE, sizeof(gdouble));
    GArray *index=g_array_new(FALSE, FALSE, sizeof(gint));
    //This text string is in order.
    for(i=0;i<string->len;i++) g_array_append_val(index, i);    
    GPtrArray *cairo_text=get_font_paths_2d(face, scale, string->str, index, box_size, &max_advance_y, &max_drop_y, use_advance, FALSE);
    g_array_free(index, TRUE);

    gdouble total_width=0.0;
    for(i=0;i<(box_size->len);i+=2)
      {
        total_width+=g_array_index(box_size, gdouble, i)+spacing;
      }
    total_width-=spacing;

    //Add the text width and height to the end of the box_size array.
    gdouble move_y=max_advance_y+max_drop_y;
    g_array_append_val(box_size, total_width);
    g_array_append_val(box_size, move_y);

    //Translate characters to center of rotation.
    gdouble move_x=-total_width/2.0;
    move_y=(max_advance_y+max_drop_y)/2.0-max_drop_y;     
    for(i=0;i<(cairo_text->len); i++)
      {
        path1=(GArray*)g_ptr_array_index(cairo_text, i);
        if(path1!=NULL) p1=&g_array_index(path1, struct segment2, 0);
        move_x+=g_array_index(box_size, gdouble, 2*i)/2.0;  
        if(path1!=NULL)
          {
            for(j=0;j<path1->len;j++)
              {
                (p1->x1)+=move_x;
                (p1->x2)+=move_x;
                (p1->x3)+=move_x;            
                (p1->y1)+=move_y;
                (p1->y2)+=move_y;
                (p1->y3)+=move_y;            
                p1++;
              }
          } 
        move_x+=g_array_index(box_size, gdouble, 2*i)/2.0+spacing;          
      }   
  
     //Initialize the transformed array
     GPtrArray *cairo_text_t=g_ptr_array_new();
     for(i=0;i<(cairo_text->len); i++)
       {
         path1=(GArray*)g_ptr_array_index(cairo_text, i);
         if(path1!=NULL)
           {
             path2=g_array_sized_new(FALSE, FALSE, sizeof(struct segment2), path1->len);
             p1=&g_array_index(path1, struct segment2, 0);
             p2=&g_array_index(path2, struct segment2, 0);
             memcpy(p2, p1, sizeof(struct segment2)*(path1->len));
             g_array_set_size(path2, path1->len);
             g_ptr_array_add(cairo_text_t, path2);
           }
         else g_ptr_array_add(cairo_text_t, NULL);
       }

    struct text2d *p=g_new(struct text2d, 1);
    p->text=text;
    p->cairo_text=cairo_text;
    p->cairo_text_t=cairo_text_t;
    p->box_size=box_size;
    p->spacing=spacing; 

    return p;  
  }
void reset_text2d(struct text2d *t2d)
  {
    //Reset the transformed text.
    gint i=0;
    GArray *path1=NULL;
    GArray *path2=NULL;
    struct segment2 *p1=NULL;
    struct segment2 *p2=NULL;
    for(i=0;i<(t2d->cairo_text->len);i++)
      {
        path1=(GArray*)g_ptr_array_index(t2d->cairo_text, i);
        path2=(GArray*)g_ptr_array_index(t2d->cairo_text_t, i);
        if(path1!=NULL&&path2!=NULL)
          {
            p1=&g_array_index(path1, struct segment2, 0);
            p2=&g_array_index(path2, struct segment2, 0);
            memcpy(p2, p1, sizeof(struct segment2)*(path1->len));
          }
      }
  }
void free_text2d(struct text2d *t2d)
  {
    gint i=0;

    if(t2d!=NULL)
      {
        if(t2d->cairo_text!=NULL)
          {
            for(i=0;i<(t2d->cairo_text->len);i++)
              {
                if((GArray*)g_ptr_array_index(t2d->cairo_text, i)!=NULL)
                  {
                    g_array_free((GArray*)g_ptr_array_index(t2d->cairo_text, i), TRUE);
                  }
              }
            g_ptr_array_free(t2d->cairo_text, TRUE);
          } 
        if(t2d->cairo_text_t!=NULL)
          {
            for(i=0;i<(t2d->cairo_text_t->len);i++)
              {
                if((GArray*)g_ptr_array_index(t2d->cairo_text_t, i)!=NULL)
                  {
                    g_array_free((GArray*)g_ptr_array_index(t2d->cairo_text_t, i), TRUE);
                  }
              }
            g_ptr_array_free(t2d->cairo_text_t, TRUE);
          } 
        if(t2d->box_size!=NULL) g_array_free(t2d->box_size, TRUE);
        if(t2d->text!=NULL) g_string_free(t2d->text, TRUE);
        g_free(t2d);
      } 
  }
GPtrArray* get_font_paths_2d(FT_Face face, gdouble scale, const gchar *text, GArray *index, GArray *box_size, gdouble *max_advance_y, gdouble *max_drop_y, gboolean use_advance, gboolean single_char)
 {
   gint i=0;
   GPtrArray *text2d=g_ptr_array_new();
   GArray *path1=NULL;
   gdouble box_w=0.0;
   gdouble box_h=0.0;
   gdouble temp_advance=0.0;
   gdouble temp_drop=0.0;

   for(i=0;i<(index->len);i++)
     {
       path1=get_font_path(face, scale, text, g_array_index(index, gint, i), &box_w, &box_h, max_advance_y, max_drop_y, use_advance, single_char);
       g_ptr_array_add(text2d, (gpointer)path1);
       g_array_append_val(box_size, box_w);
       g_array_append_val(box_size, box_h);
       if(max_advance_y!=NULL&&*max_advance_y>temp_advance) temp_advance=*max_advance_y;
       if(max_drop_y!=NULL&&*max_drop_y>temp_drop) temp_drop=*max_drop_y;
     }
   if(max_advance_y!=NULL) *max_advance_y=temp_advance;
   if(max_drop_y!=NULL) *max_drop_y=temp_drop;

   return text2d;
 }
GPtrArray* get_font_paths_3d(FT_Face face, gdouble scale, const gchar *text, GArray *index, GArray *box_size, gdouble *max_advance_y, gdouble *max_drop_y, gboolean use_advance, gboolean single_char)
 {
   gint i=0;
   GPtrArray *text3d=g_ptr_array_new();
   GArray *path1=NULL;
   GArray *path2=NULL;
   struct segment2 *p1=NULL;
   struct segment2 *p2=NULL;
   gdouble box_w=0.0;
   gdouble box_h=0.0;
   gdouble temp_advance=0.0;
   gdouble temp_drop=0.0;

   for(i=0;i<(index->len);i++)
     {
       path1=get_font_path(face, scale, text, g_array_index(index, gint, i), &box_w, &box_h, max_advance_y, max_drop_y, use_advance, single_char);
       g_array_append_val(box_size, box_w);
       g_array_append_val(box_size, box_h);
       if(max_advance_y!=NULL&&*max_advance_y>temp_advance) temp_advance=*max_advance_y;
       if(max_drop_y!=NULL&&*max_drop_y>temp_drop) temp_drop=*max_drop_y;
       g_ptr_array_add(text3d, (gpointer)path1);
       if(path1!=NULL)
         {
           path2=g_array_sized_new(FALSE, FALSE, sizeof(struct segment2), path1->len);
           p1=&g_array_index(path1, struct segment2, 0);
           p2=&g_array_index(path2, struct segment2, 0);
           memcpy(p2, p1, sizeof(struct segment2)*(path1->len));
           g_array_set_size(path2, path1->len);
           g_ptr_array_add(text3d, (gpointer)path2);
         }
       else g_ptr_array_add(text3d, NULL);
     }
   if(max_advance_y!=NULL) *max_advance_y=temp_advance;
   if(max_drop_y!=NULL) *max_drop_y=temp_drop;

   return text3d;
 }
GArray* get_font_path(FT_Face face, gdouble scale, const gchar *text, gint index, gdouble *box_w, gdouble *box_h, gdouble *max_advance_y, gdouble *max_drop_y, gboolean use_advance, gboolean single_char)
 {
   gint i=0;
   gint j=0;

   FT_Outline outline=get_font_outline(face, scale, text, index, box_w, box_h, max_advance_y, max_drop_y, use_advance, single_char);

   GArray *path=NULL;
   struct segment2 seg;
   if(outline.n_points<1)
     {
       goto exit;
     }
   else
     {
       path=g_array_new(FALSE, FALSE, sizeof(struct segment2));
     }

   FT_Vector *p1=outline.points;
   char *c1=outline.tags;

   p1=outline.points;
   gboolean draw=FALSE;
   //For converting quadratic bezier point to cubic.
   gdouble q1_x=0.0;
   gdouble q1_y=0.0;
   gdouble q2_x=0.0;
   gdouble q2_y=0.0;
   //Save the last point whether virtual or in the array.
   gdouble last_x=0.0;
   gdouble last_y=0.0;
   //For counting bezier points.
   gint count=0;
   gdouble v_pointx=0.0;
   gdouble v_pointy=0.0;
   //The start of the first contour.
   gdouble c_startx=0.0;
   gdouble c_starty=0.0;
   gint j_start=0;
   //The points in each contour.
   gint contour_points=0;
   //Draw the contours.
   for(i=0;i<outline.n_contours;i++)
     {
       if(i==0) contour_points=outline.contours[i]+1;
       else contour_points=outline.contours[i]-outline.contours[i-1];
       //If the start of the contour is a conic off.
       if(*c1==FT_CURVE_TAG_CONIC)
         {
           if(*(c1+contour_points-1)==FT_CURVE_TAG_CONIC)
             {
               c_startx=((((p1+contour_points-1)->x)+(p1->x))/2.0)*scale;
               c_starty=((((p1+contour_points-1)->y)+(p1->y))/2.0)*scale;
             }
           else
             {
               c_startx=((p1+contour_points-1)->x)*scale;
               c_starty=((p1+contour_points-1)->y)*scale;
             }
           last_x=c_startx;
           last_y=c_starty;
           j_start=0;
         }
       else
         {
           last_x=(p1->x)*scale;
           last_y=(p1->y)*scale;
           c_startx=(p1->x)*scale;
           c_starty=(p1->y)*scale;
           j_start=1;
           p1++;c1++;
         }
      
       seg.move_id=MOVE_TO_R;seg.x1=0.0;seg.y1=0.0;seg.z1=0.0;
       seg.x2=0.0;seg.y2=0.0;seg.z2=0.0;
       seg.x3=c_startx;seg.y3=c_starty;seg.z3=0.0;
       g_array_append_val(path, seg);
       
       for(j=j_start;j<contour_points;j++)
         {  
           if(*c1==FT_CURVE_TAG_CONIC&&!draw)
             {
               count++;
               if(count==2) draw=TRUE;             
             }          
           else if(*c1==FT_CURVE_TAG_ON&&!draw)
             {
               draw=TRUE;
             }
      
           //Draw the contour segments.
           if(draw)
             {       
               if(count==1)
                 {
                   q1_x=(last_x+2.0*((p1-1)->x)*scale)/3.0;
	           q1_y=(last_y+2.0*((p1-1)->y)*scale)/3.0;
                   q2_x=((p1->x)*scale+2.0*((p1-1)->x)*scale)/3.0;
	           q2_y=((p1->y)*scale+2.0*((p1-1)->y)*scale)/3.0;
                   seg.move_id=CURVE_TO;seg.x1=q1_x;seg.y1=q1_y;seg.z1=0.0;
                   seg.x2=q2_x;seg.y2=q2_y;seg.z2=0.0;
                   seg.x3=(p1->x)*scale;seg.y3=(p1->y)*scale;seg.z3=0.0;
                   g_array_append_val(path, seg);
                   count=0;
                   last_x=(p1->x)*scale;
                   last_y=(p1->y)*scale;
                 }
               else if(count==2)
                 {
                   v_pointx=((((p1-1)->x)+(p1->x))/2)*scale;
                   v_pointy=((((p1-1)->y)+(p1->y))/2)*scale;
                   q1_x=(last_x+2.0*((p1-1)->x)*scale)/3.0;
	           q1_y=(last_y+2.0*((p1-1)->y)*scale)/3.0;
                   q2_x=(v_pointx+2.0*((p1-1)->x)*scale)/3.0;
	           q2_y=(v_pointy+2.0*((p1-1)->y)*scale)/3.0;
                   seg.move_id=CURVE_TO;seg.x1=q1_x;seg.y1=q1_y;seg.z1=0.0;
                   seg.x2=q2_x;seg.y2=q2_y;seg.z2=0.0;
                   seg.x3=v_pointx;seg.y3=v_pointy;seg.z3=0.0;
                   g_array_append_val(path, seg);
                   count--;
                   last_x=v_pointx;
                   last_y=v_pointy;
                 }
               else
                 {
                   seg.move_id=LINE_TO;seg.x1=0.0;seg.y1=0.0;seg.z1=0.0;
                   seg.x2=0.0;seg.y2=0.0;seg.z2=0.0;
                   seg.x3=(p1->x)*scale;seg.y3=(p1->y)*scale;seg.z3=0.0;
                   g_array_append_val(path, seg);
                   count=0;
                   last_x=(p1->x)*scale;
                   last_y=(p1->y)*scale;
                 }
               draw=FALSE;           
             }

           //End drawing the countour.
           if(j==contour_points-1)
             {
               if(count==1)
                 {
                   q1_x=(last_x+2.0*(p1->x)*scale)/3.0;
	           q1_y=(last_y+2.0*(p1->y)*scale)/3.0;
                   q2_x=(c_startx+2.0*(p1->x)*scale)/3.0;
	           q2_y=(c_starty+2.0*(p1->y)*scale)/3.0;
                   seg.move_id=CURVE_TO_STROKE;seg.x1=q1_x;seg.y1=q1_y;seg.z1=0.0;
                   seg.x2=q2_x;seg.y2=q2_y;seg.z2=0.0;
                   seg.x3=c_startx;seg.y3=c_starty;seg.z3=0.0;
                   g_array_append_val(path, seg);
                 }
               else if(count==2)
                 {
                   v_pointx=((((p1-1)->x)+(p1->x))/2)*scale;
                   v_pointy=((((p1-1)->y)+(p1->y))/2)*scale;
                   q1_x=(last_x+2.0*((p1-1)->x)*scale)/3.0;
	           q1_y=(last_y+2.0*((p1-1)->y)*scale)/3.0;
                   q2_x=(v_pointx+2.0*((p1-1)->x)*scale)/3.0;
	           q2_y=(v_pointy+2.0*((p1-1)->y)*scale)/3.0;
                   seg.move_id=CURVE_TO_STROKE;seg.x1=q1_x;seg.y1=q1_y;seg.z1=0.0;                  
                   seg.x2=q2_x;seg.y2=q2_y;seg.z2=0.0;
                   seg.x3=v_pointx;seg.y3=v_pointy;seg.z3=0.0;
                   g_array_append_val(path, seg);
                   q1_x=(v_pointx+2.0*(p1->x)*scale)/3.0;
	           q1_y=(v_pointy+2.0*(p1->y)*scale)/3.0;
                   q2_x=(c_startx+2.0*(p1->x)*scale)/3.0;
	           q2_y=(c_starty+2.0*(p1->y)*scale)/3.0;
                   seg.move_id=CURVE_TO_STROKE;seg.x1=q1_x;seg.y1=q1_y;seg.z1=0.0;                   
                   seg.x2=q2_x;seg.y2=q2_y;seg.z2=0.0;
                   seg.x3=c_startx;seg.y3=c_starty;seg.z3=0.0;
                   g_array_append_val(path, seg);
                 }
               else
                 {
                   seg.move_id=LINE_TO;seg.x1=0.0;seg.y1=0.0;seg.z1=0.0;
                   seg.x2=0.0;seg.y2=0.0;seg.z2=0.0;
                   seg.x3=(p1->x)*scale;seg.y3=(p1->y)*scale;seg.z3=0.0;
                   g_array_append_val(path, seg);
                   seg.move_id=LINE_TO_STROKE;seg.x1=0.0;seg.y1=0.0;seg.z1=0.0;
                   seg.x2=0.0;seg.y2=0.0;seg.z2=0.0;
                   seg.x3=c_startx;seg.y3=c_starty;seg.z3=0.0;
                   g_array_append_val(path, seg);
                 }
                count=0.0;
                draw=FALSE;
              }

           p1++;c1++;
         }
     }

    /*
      Find the contour directions.
      https://stackoverflow.com/questions/1165647/how-to-determine-if-a-list-of-polygon-points-are-in-clockwise-order
      note "57 A minor caveat:" comment.
    */
    struct segment2 *s1=&g_array_index(path, struct segment2, 0);
    struct segment2 *s2=&g_array_index(path, struct segment2, 0);
    struct segment2 *s3=&g_array_index(path, struct segment2, 0);
    gdouble sum=0.0;
    for(i=0;i<(path->len);i++)
      {
        if((s1->move_id)==MOVE_TO_R)
          {
            s2=s1;
            s3=s1;
            sum=0.0;
          }
        else if((s1->move_id)==CURVE_TO_STROKE||(s1->move_id)==LINE_TO_STROKE)
          {
            while(s2!=s1)
              {
                sum+=((((s2+1)->x3)-(s2->x3))*(((s2+1)->y3)+(s2->y3)));
                s2++;
              }
            if(sum>=0.0) s3->move_id=MOVE_TO_L;
            else s3->move_id=MOVE_TO_R;
          }
        s1++;
      }

    exit:
    return path;
  }
void curve_font(GArray *path, gdouble radius)
  {
    gint i=0;
    gdouble x=0.0;
    gdouble y=0.0;
   
    struct segment2 *p1=&g_array_index(path, struct segment2, 0);
    struct segment2 *p2=&g_array_index(path, struct segment2, 0);

    for(i=0;i<path->len;i++)
      {
        //Add bezier points to line segments.
        if((p1->move_id)==LINE_TO||(p1->move_id)==LINE_TO_STROKE)
          {
            p2=p1-1;
            x=(p1->x3)-(p2->x3);
            y=(p1->y3)-(p2->y3);
            (p1->x1)=(p2->x3)+x/3.0;
            (p1->y1)=(p2->y3)+y/3.0;
            (p1->x2)=(p2->x3)+2.0*x/3.0;
            (p1->y2)=(p2->y3)+2.0*y/3.0;
            if((p1->move_id)==LINE_TO) (p1->move_id)=CURVE_TO;
            else (p1->move_id)=CURVE_TO_STROKE;
          }
        //Curve points in the segment to a circle.
        (p1->z1)=sqrt(radius*radius-(p1->x1)*(p1->x1));
        (p1->z2)=sqrt(radius*radius-(p1->x2)*(p1->x2));
        (p1->z3)=sqrt(radius*radius-(p1->x3)*(p1->x3));
        p1++;
      }
  }
GArray* get_shadow_path(GArray *path, const gdouble light[3], const gdouble plane_distance, const gdouble qrs_plane[9])
 {
    gint i=0;
    gdouble delta_x=0.0;
    gdouble delta_y=0.0;
    gdouble delta_z=0.0;
    //For the normal vector to the plane.
    gdouble vec1[3];
    gdouble vec2[3];
    gdouble normal[3];
    gdouble unit=1.0;
    gdouble light2[3];

    GArray *shadow=g_array_sized_new(FALSE, FALSE, sizeof(struct segment2), path->len);
    g_array_set_size(shadow, path->len);
    struct segment2 *p1=&g_array_index(path, struct segment2, 0);
    struct segment2 *p2=&g_array_index(shadow, struct segment2, 0);

    unit=sqrt(light[0]*light[0]+light[1]*light[1]+light[2]*light[2]);
    light2[0]=plane_distance*light[0]/unit;
    light2[1]=plane_distance*light[1]/unit;
    light2[2]=plane_distance*light[2]/unit;

    //A reference plane to stretch the shadow on.
    reset_plane_pts(qrs_plane);
    plane_pts_t[0]=plane_pts_t[0]-light2[0];
    plane_pts_t[1]=plane_pts_t[1]-light2[1];
    plane_pts_t[2]=plane_pts_t[2]-light2[2];
    plane_pts_t[3]=plane_pts_t[3]-light2[0];
    plane_pts_t[4]=plane_pts_t[4]-light2[1];
    plane_pts_t[5]=plane_pts_t[5]-light2[2];
    plane_pts_t[6]=plane_pts_t[6]-light2[0];
    plane_pts_t[7]=plane_pts_t[7]-light2[1];
    plane_pts_t[8]=plane_pts_t[8]-light2[2];

    //Get a normal vector.
    vec1[0]=plane_pts_t[0]-plane_pts_t[3];
    vec1[1]=plane_pts_t[1]-plane_pts_t[4];
    vec1[2]=plane_pts_t[2]-plane_pts_t[5];
    vec2[0]=plane_pts_t[0]-plane_pts_t[6];
    vec2[1]=plane_pts_t[1]-plane_pts_t[7];
    vec2[2]=plane_pts_t[2]-plane_pts_t[8];
    //Cross product.
    normal[0]=((vec1[1]*vec2[2])-(vec1[2]*vec2[1]));
    normal[1]=((vec1[2]*vec2[0])-(vec1[0]*vec2[2]));
    normal[2]=((vec1[0]*vec2[1])-(vec1[1]*vec2[0]));
    //For unit normal vector.
    unit=sqrt(normal[0]*normal[0]+normal[1]*normal[1]+normal[2]*normal[2]);
    normal[0]=normal[0]/unit;
    normal[1]=normal[1]/unit;
    normal[2]=normal[2]/unit;

    //From http://paulbourke.net/geometry/pointlineplane/
    gdouble num=(normal[0]*(plane_pts_t[0]-light[0]))+(normal[1]*(plane_pts_t[1]-light[1]))+(normal[2]*(plane_pts_t[2]-light[2]));
    gdouble den=1.0;
    gdouble u=1.0;
    if(num<0.0001&&num>-0.0001)
      {
        g_warning("The projection plane is parallel with the light.\n");
        for(i=0;i<(path->len);i++)
          {
            (p2->move_id)=(p1->move_id);
            delta_x=light[0]-(p1->x1);
            delta_y=light[1]-(p1->y1); 
            delta_z=light[2]-(p1->z1); 
            (p2->x1)=(p1->x1)-delta_x;
            (p2->y1)=(p1->y1)-delta_y;
            (p2->z1)=(p1->z1)-delta_z;
            delta_x=light[0]-(p1->x2);
            delta_y=light[1]-(p1->y2);
            delta_z=light[2]-(p1->z2);  
            (p2->x2)=(p1->x2)-delta_x;
            (p2->y2)=(p1->y2)-delta_y;
            (p2->z2)=(p1->z2)-delta_z;
            delta_x=light[0]-(p1->x3);
            delta_y=light[1]-(p1->y3); 
            delta_z=light[2]-(p1->z3);  
            (p2->x3)=(p1->x3)-delta_x;
            (p2->y3)=(p1->y3)-delta_y;
            (p2->z3)=(p1->z3)-delta_z;
            p1++;p2++;
          }
      }
    else
      {
        for(i=0;i<(path->len);i++)
          {
            (p2->move_id)=(p1->move_id);
            den=(normal[0]*((p1->x1)-light[0]))+(normal[1]*((p1->y1)-light[1]))+(normal[2]*((p1->z1)-light[2]));
            if(den<1.0&&den>0.0) den=1.0;
            if(den>-1.0&&den<=0.0) den=-1.0;
            u=num/den;
            if(u<1.0) u=1.0; 
            (p2->x1)=light[0]+u*((p1->x1)-light[0]);
            (p2->y1)=light[1]+u*((p1->y1)-light[1]);
            (p2->z1)=light[2]+u*((p1->z1)-light[2]);;         
            den=(normal[0]*((p1->x2)-light[0]))+(normal[1]*((p1->y2)-light[1]))+(normal[2]*((p1->z2)-light[2]));
            if(den<1.0&&den>0.0) den=1.0;
            if(den>-1.0&&den<=0.0) den=-1.0;
            u=num/den;
            if(u<1.0) u=1.0; 
            (p2->x2)=light[0]+u*((p1->x2)-light[0]);
            (p2->y2)=light[1]+u*((p1->y2)-light[1]);
            (p2->z2)=light[2]+u*((p1->z2)-light[2]);; 
            den=(normal[0]*((p1->x3)-light[0]))+(normal[1]*((p1->y3)-light[1]))+(normal[2]*((p1->z3)-light[2]));
            if(den<1.0&&den>0.0) den=1.0;
            if(den>-1.0&&den<=0.0) den=-1.0;
            u=num/den;
            if(u<1.0) u=1.0;
            (p2->x3)=light[0]+u*((p1->x3)-light[0]);
            (p2->y3)=light[1]+u*((p1->y3)-light[1]);
            (p2->z3)=light[2]+u*((p1->z3)-light[2]);
            p1++;p2++;
          }
      }
   
    return shadow;
  }
static FT_Outline get_font_outline(FT_Face face, gdouble scale, const gchar *text, gint index, gdouble *box_w, gdouble *box_h, gdouble *max_advance_y, gdouble *max_drop_y, gboolean use_advance, gboolean single_char)
  {
    gint i=0;
    FT_Error error;
    FT_GlyphSlot slot;
    FT_Outline outline;
    error=FT_Load_Char(face, text[index], FT_LOAD_NO_SCALE|FT_LOAD_NO_BITMAP);
    if(error) g_print("FT_Load_Char error.\n");
    slot=face->glyph;
    outline=slot->outline;

    //Invert points and letter.
    FT_Vector *p=outline.points;
    for(i=0;i<outline.n_points;i++)
      {
        (p->y)*=-1;
        p++;
      }

    //Get a bounding box.
    FT_BBox box;
    gdouble center_x=0.0;
    gdouble center_y=0.0;
    FT_Outline_Get_BBox(&outline, &box);
    center_x=((gdouble)box.xMax*scale-(gdouble)box.xMin*scale)/2.0;
    center_y=((gdouble)box.yMax*scale-(gdouble)box.yMin*scale)/2.0;
    if(box_w!=NULL)
      {
        if(use_advance) *box_w=(gdouble)(slot->advance.x)*scale;
        else *box_w=((gdouble)box.xMax*scale-(gdouble)box.xMin*scale);
      }
    if(box_h!=NULL) 
      {
        *box_h=((gdouble)box.yMax*scale-(gdouble)box.yMin*scale);
      }
    if(max_advance_y!=NULL) *max_advance_y=(slot->metrics.horiBearingY)*scale;
    if(max_advance_y!=NULL&&box_h!=NULL) *max_drop_y=(*box_h)-(*max_advance_y);
    
    //Center points for rotation around the center.
    p=outline.points;
    for(i=0;i<outline.n_points;i++)
      {
        (p->x)=(p->x)-center_x*1.0/scale-box.xMin;
        //For strings the y center of the string is set in text3d_new() and text2d_new().
        if(single_char) (p->y)=(p->y)-center_y*1.0/scale-box.yMin;
        p++;
      }

    return outline;
  }
void quaternion_rotation_text(const gdouble yaw, const gdouble roll, const gdouble pitch, gdouble qrs[9])
  {
    /*
       https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
       Euler angles (z-y′-x″ intrinsic) → quaternion
           For quaternion calculations.

       https://en.wikipedia.org/wiki/Rotation
       Main article: Aircraft principal axes
           For a picture of the rotations.

       Rotate the shapes with Cairo in 3d.

       Yaw, roll and pitch in the domain of real numbers. The yaw, roll and pitch angles are
       in radians. 
    */ 
    gdouble roll1=roll/2.0;
    gdouble pitch1=pitch/2.0;
    gdouble yaw1=yaw/2.0;
    gdouble qi=sin(pitch1)*cos(roll1)*cos(yaw1)-cos(pitch1)*sin(roll1)*sin(yaw1);
    gdouble qj=cos(pitch1)*sin(roll1)*cos(yaw1)+sin(pitch1)*cos(roll1)*sin(yaw1);
    gdouble qk=cos(pitch1)*cos(roll1)*sin(yaw1)-sin(pitch1)*sin(roll1)*cos(yaw1);
    gdouble qr=cos(pitch1)*cos(roll1)*cos(yaw1)+sin(pitch1)*sin(roll1)*sin(yaw1);
    qrs[0]=1.0-2.0*qj*qj-2.0*qk*qk;
    qrs[1]=2.0*(qi*qj-qk*qr);
    qrs[2]=2.0*(qi*qk+qj*qr);
    qrs[3]=2.0*(qi*qj+qk*qr);
    qrs[4]=1.0-2.0*qi*qi-2.0*qk*qk;
    qrs[5]=2.0*(qj*qk-qi*qr);
    qrs[6]=2.0*(qi*qk-qj*qr);
    qrs[7]=2.0*(qj*qk+qi*qr);
    qrs[8]=1.0-2.0*(qi*qi+qj*qj);
  }
void transform_letter(GArray *path, const gdouble qrs[9])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0;  
    gdouble cr4=0.0;
    gdouble cr5=0.0;
    gdouble cr6=0.0;   
    gdouble cr7=0.0;
    gdouble cr8=0.0;
    gdouble cr9=0.0;    
    struct segment2 *p1=NULL;

    p1=&g_array_index(path, struct segment2, 0);
    for(i=0;i<(path->len);i++)
      {
        cr1=(p1->x1);
        cr2=(p1->y1);
        cr3=(p1->z1); 
        cr4=(p1->x2);
        cr5=(p1->y2);
        cr6=(p1->z2);  
        cr7=(p1->x3);
        cr8=(p1->y3);
        cr9=(p1->z3);          
        p1->x1=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
        p1->y1=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
        p1->z1=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
        p1->x2=(cr4*qrs[0])+(cr5*qrs[1])+(cr6*qrs[2]);
        p1->y2=(cr4*qrs[3])+(cr5*qrs[4])+(cr6*qrs[5]);
        p1->z2=(cr4*qrs[6])+(cr5*qrs[7])+(cr6*qrs[8]);
        p1->x3=(cr7*qrs[0])+(cr8*qrs[1])+(cr9*qrs[2]);
        p1->y3=(cr7*qrs[3])+(cr8*qrs[4])+(cr9*qrs[5]);
        p1->z3=(cr7*qrs[6])+(cr8*qrs[7])+(cr9*qrs[8]);
        p1++;
      }

    //Transform reference vector.
    cr1=v1[0];
    cr2=v1[1];
    cr3=v1[2];         
    v1_r[0]=(cr1*qrs[0])+(cr2*qrs[1])+(cr3*qrs[2]);
    v1_r[1]=(cr1*qrs[3])+(cr2*qrs[4])+(cr3*qrs[5]);
    v1_r[2]=(cr1*qrs[6])+(cr2*qrs[7])+(cr3*qrs[8]);
  }
void matrix_multiply(const gdouble in1[9], const gdouble in2[9], gdouble out[9])
  {
    //Multiply two 3x3 matrices.
    out[0]=in1[0]*in2[0]+in1[3]*in2[1]+in1[6]*in2[2];
    out[1]=in1[1]*in2[0]+in1[4]*in2[1]+in1[7]*in2[2];
    out[2]=in1[2]*in2[0]+in1[5]*in2[1]+in1[8]*in2[2];

    out[3]=in1[0]*in2[3]+in1[3]*in2[4]+in1[6]*in2[5];
    out[4]=in1[1]*in2[3]+in1[4]*in2[4]+in1[7]*in2[5];
    out[5]=in1[2]*in2[3]+in1[5]*in2[4]+in1[8]*in2[5];

    out[6]=in1[0]*in2[6]+in1[3]*in2[7]+in1[6]*in2[8];
    out[7]=in1[1]*in2[6]+in1[4]*in2[7]+in1[7]*in2[8];
    out[8]=in1[2]*in2[6]+in1[5]*in2[7]+in1[8]*in2[8];
  }
static void reset_plane_pts(const gdouble qrs_plane[9])
  {
    gint i=0; 

    for(i=0;i<3;i++)
      {
        plane_pts_t[3*i]=plane_pts[3*i];
        plane_pts_t[3*i+1]=plane_pts[3*i+1];
        plane_pts_t[3*i+2]=plane_pts[3*i+2];
      }
    rotate_plane(qrs_plane);
  }
static void rotate_plane(const gdouble qrs_plane[9])
  {
    gint i=0;
    gdouble cr1=0.0;
    gdouble cr2=0.0;
    gdouble cr3=0.0;   

    for(i=0;i<3;i++)
      {
        cr1=plane_pts_t[3*i];
        cr2=plane_pts_t[3*i+1];
        cr3=plane_pts_t[3*i+2];;         
        plane_pts_t[3*i]=(cr1*qrs_plane[0])+(cr2*qrs_plane[1])+(cr3*qrs_plane[2]);
        plane_pts_t[3*i+1]=(cr1*qrs_plane[3])+(cr2*qrs_plane[4])+(cr3*qrs_plane[5]);
        plane_pts_t[3*i+2]=(cr1*qrs_plane[6])+(cr2*qrs_plane[7])+(cr3*qrs_plane[8]);
      }    
  }
static int compare_quads_sides(const void *q1, const void *q2)
  {
    return((((struct quad*)q1)->a.z3+((struct quad*)q1)->b.z3+((struct quad*)q1)->c.z3+((struct quad*)q1)->d.z3) - (((struct quad*)q2)->a.z3+((struct quad*)q2)->b.z3+((struct quad*)q2)->c.z3+((struct quad*)q2)->d.z3));
  }
void set_revolve_standard_point_text(gdouble x, gdouble y, gdouble z)
  {
    revolve_text[0]=x;
    revolve_text[1]=y;
    revolve_text[2]=z;
  }
void get_revolved_point_text(gdouble *x, gdouble *y, gdouble *z)
  {
    *x=revolve_text_r[0];
    *y=revolve_text_r[1];
    *z=revolve_text_r[2];
  }
void revolve_standard_point_text(gdouble qrs[9])
  {
    revolve_text_r[0]=(revolve_text[0]*qrs[0])+(revolve_text[1]*qrs[1])+(revolve_text[2]*qrs[2]);
    revolve_text_r[1]=(revolve_text[0]*qrs[3])+(revolve_text[1]*qrs[4])+(revolve_text[2]*qrs[5]);
    revolve_text_r[2]=(revolve_text[0]*qrs[6])+(revolve_text[1]*qrs[7])+(revolve_text[2]*qrs[8]);
  }
gboolean standard_vector_is_top()
  {
    if(v1_r[2]>=0.0) return TRUE;
    else return FALSE;
  }
void set_plane_points(gdouble plane_points[9])
  {
    //Three points in a plane.
    plane_pts[0]=plane_points[0];plane_pts[1]=plane_points[1];plane_pts[2]=plane_points[2];
    plane_pts[3]=plane_points[3];plane_pts[4]=plane_points[4];plane_pts[5]=plane_points[5];
    plane_pts[6]=plane_points[6];plane_pts[7]=plane_points[7];plane_pts[8]=plane_points[8];
  }






