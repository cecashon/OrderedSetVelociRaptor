

--This is a stepped progress bar widget derived from a GTK drawing area widget. The progress bar can be either horizontal or vertical. The colors and number of steps can be changed in the widget.

--There are some instructions in the stepped_progress_bar.c file for setting up introspection. Then
the widget can be used from higher level languages like Python, Perl or Java Script.

Stepped Progress Bars

![ScreenShot](/Misc/SteppedProgressBar/stepped1.png)

