
//For use with introspection and the stepped_progress_bar widget. See stepped_progress_bar.c for setting it up.

const Lang = imports.lang;
const Gtk = imports.gi.Gtk;
const Stepped = imports.gi.Stepped; 

Gtk.init(null);

const TestBar = new Lang.Class({
    Name: 'Bar',
    Extends: Gtk.Window,

    _init: function() {
        this.parent({title: "SteppedProgressBar"});
        this.set_default_size(75, 300);

        this.bar = new Stepped.ProgressBar();
        
        this.add(this.bar);
    },
});

let win = new TestBar();
win.connect("delete-event", Gtk.main_quit);
win.show_all();
Gtk.main();
