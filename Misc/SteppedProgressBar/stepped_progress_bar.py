
#For use with introspection and the stepped_progress_bar widget. See stepped_progress_bar.c for setting it up.

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Stepped', '0.1')
from gi.repository import Gtk, Stepped

class TestBar(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="SteppedProgressBar")
        self.set_default_size(75, 300)

        self.bar=Stepped.ProgressBar()

        self.add(self.bar)

win = TestBar()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()
