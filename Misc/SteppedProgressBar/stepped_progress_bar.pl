
#For use with introspection and the stepped_progress_bar widget. See stepped_progress_bar.c for setting it up.

use strict;
use diagnostics;
use warnings;
use Gtk3 '-init';

Glib::Object::Introspection->setup(basename => 'Stepped', version => '0.1', package => 'Stepped');

package TestBar;
sub new
{
    my $class = shift;
    my $self = {};
    bless $self, $class;

    $self->{'window'} = Gtk3::Window->new();
    $self->{'window'}->signal_connect('delete_event' => sub { Gtk3->main_quit; });
    $self->{'window'}->set_default_size(75, 300);
    $self->{'window'}->set_title("SteppedProgressBar");

    $self->{'bar'} = Stepped::ProgressBar->new();

    $self->{'window'}->add($self->{'bar'});

    return $self;
}
my $win = new TestBar();
$win->{'window'}->show_all();
Gtk3->main;




