
/*
  Test a GTK entry completion with sqlite. Look at table load times using SQL or a prepared statement.
Compare table access times.

  gcc -Wall entry_completion3.c -o entry_completion3 `pkg-config --cflags --libs gtk+-3.0` -lsqlite3

  Ubuntu18.04 with GTK3.22
*/

#include<gtk/gtk.h>
#include<sqlite3.h>

static sqlite3 *db=NULL;

static void get_sql_list(GtkEditable *entry, gpointer user_data);
static void initialize_database();
static void insert_data_sql();
static void insert_data_prepared();
static void close_program(GtkWidget *widget, gpointer data);               

int main(int argc, char *argv[])
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Entry Completion");
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(window), 10);
    gtk_window_set_default_size(GTK_WINDOW(window), 300, 100);
    g_signal_connect(window, "destroy", G_CALLBACK(close_program), NULL);

    initialize_database();

    GtkEntryCompletion *completion=gtk_entry_completion_new();

    GtkWidget *entry=gtk_entry_new();
    gtk_widget_set_hexpand(entry, TRUE);
    gtk_entry_set_completion(GTK_ENTRY(entry), completion);
    g_signal_connect(entry, "changed", G_CALLBACK(get_sql_list), completion);

    GtkWidget *grid=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid), entry, 0, 0, 1, 1);
    
    gtk_container_add(GTK_CONTAINER(window), grid);
   
    gtk_widget_show_all(window);

    gtk_main();
    return 0;   
  }
static void get_sql_list(GtkEditable *entry, gpointer user_data)
  {
    GtkTreeIter iter;
    gint ret_val=0;
    sqlite3_stmt *stmt1=NULL;
    GtkListStore *new_store=NULL;

    GTimer *timer=g_timer_new();

    //Build a SQL string.
    gchar *sql_string=g_strdup_printf("SELECT CritterName FROM Critters WHERE CritterName LIKE'%s%%' LIMIT 5;", gtk_entry_get_text(GTK_ENTRY(entry)));
    //g_print("%s\n", sql_string);

    //Prepare the SQL statement.
    sqlite3_prepare_v2(db, sql_string, -1, &stmt1, 0);
    new_store=gtk_list_store_new(1, G_TYPE_STRING);
    g_free(sql_string);

    //Load the list store.
    if(stmt1!=NULL)
      {
        ret_val=sqlite3_step(stmt1);
        while(ret_val==SQLITE_ROW)
          {            
            gtk_list_store_append(new_store, &iter);
            gtk_list_store_set(new_store, &iter, 0, sqlite3_column_text(stmt1, 0), -1);            
            ret_val=sqlite3_step(stmt1);
          }
       }
    if(stmt1!=NULL) sqlite3_finalize(stmt1); 

    //Set up the completion.
    gtk_entry_completion_set_model(GTK_ENTRY_COMPLETION(user_data), GTK_TREE_MODEL(new_store));
    g_object_unref(new_store);
    gtk_entry_set_completion(GTK_ENTRY(entry), GTK_ENTRY_COMPLETION(user_data));
    gtk_entry_completion_set_text_column(GTK_ENTRY_COMPLETION(user_data), 0);
    gtk_entry_completion_complete(GTK_ENTRY_COMPLETION(user_data));

    g_print("Time %f\n", g_timer_elapsed(timer, NULL));
    g_timer_destroy(timer);
  }
static void initialize_database()
  {
    //Use an in-memory database for testing.
    gint rc=sqlite3_open(":memory:", &db);

    if(rc!=SQLITE_OK) 
      {       
        g_print("Cannot open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
      }

    //Time batched inserts and prepared statement inserts for comparison.
    GTimer *timer=g_timer_new();
    insert_data_sql();
    g_print("SQL %f\n", g_timer_elapsed(timer, NULL));
    g_timer_start(timer);

    insert_data_prepared();
    g_print("Prepared %f\n", g_timer_elapsed(timer, NULL));
    g_timer_destroy(timer);
  }
static void insert_data_sql()
  {
    gint i=0;
    gchar *err_msg=NULL;
    gint rc=SQLITE_OK;

    //Add some test data.
    gchar *sql1="CREATE TABLE Critters(CritterId INTEGER PRIMARY KEY, CritterName TEXT);"; 

    gchar *sql2="BEGIN TRANSACTION;"
               "INSERT INTO Critters(CritterName) VALUES('Parrot');" 
               "INSERT INTO Critters(CritterName) VALUES('Chimpanzee');" 
               "INSERT INTO Critters(CritterName) VALUES('Crow');"
               "INSERT INTO Critters(CritterName) VALUES('Dolphin');" 
               "INSERT INTO Critters(CritterName) VALUES('Alligator');" 
               "INSERT INTO Critters(CritterName) VALUES('Whale');" 
               "INSERT INTO Critters(CritterName) VALUES('Zebra');" 
               "INSERT INTO Critters(CritterName) VALUES('Owl');" 
               "INSERT INTO Critters(CritterName) VALUES('Leopard');" 
               "INSERT INTO Critters(CritterName) VALUES('Koala');" 
               "INSERT INTO Critters(CritterName) VALUES('Eagle');" 
               "INSERT INTO Critters(CritterName) VALUES('Frog');" 
               "INSERT INTO Critters(CritterName) VALUES('Moose');" 
               "INSERT INTO Critters(CritterName) VALUES('Kangaroo');" 
               "INSERT INTO Critters(CritterName) VALUES('Tortoise');" 
               "INSERT INTO Critters(CritterName) VALUES('Bear');" 
               "INSERT INTO Critters(CritterName) VALUES('Shark');"
               "INSERT INTO Critters(CritterName) VALUES('Panda');" 
               "INSERT INTO Critters(CritterName) VALUES('Penguin');"
               "INSERT INTO Critters(CritterName) VALUES('Walrus');"
               "INSERT INTO Critters(CritterName) VALUES('Wolf');"
               "INSERT INTO Critters(CritterName) VALUES('Woodpecker');"
               "INSERT INTO Critters(CritterName) VALUES('Piranha');"
               "INSERT INTO Critters(CritterName) VALUES('Wolvirine');"
               "INSERT INTO Critters(CritterName) VALUES('Elephant');"
               "INSERT INTO Critters(CritterName) VALUES('Turtle');"
               "COMMIT TRANSACTION;";

    gchar *sql3="CREATE INDEX IndexCritterName ON Critters(CritterName);"; 

    gchar *sql4="BEGIN TRANSACTION;"
               "INSERT INTO Critters(CritterName) VALUES('Squirrel');" 
               "INSERT INTO Critters(CritterName) VALUES('Warbler');" 
               "INSERT INTO Critters(CritterName) VALUES('Wombat');"
               "INSERT INTO Critters(CritterName) VALUES('Crocodile');" 
               "INSERT INTO Critters(CritterName) VALUES('Lion');" 
               "END TRANSACTION;";
     
    //Create the table.          
    rc=sqlite3_exec(db, sql1, 0, 0, &err_msg);
    if(rc!=SQLITE_OK) 
      {        
        g_print("SQL error: %s\n", err_msg);
        sqlite3_free(err_msg);
        err_msg=NULL;
      }

    //Load the table with some data.
    for(i=0;i<1000;i++)
      {
        sqlite3_exec(db, sql2, 0, 0, &err_msg);
      }

    //Append some extra data to the end of the table.
    rc=sqlite3_exec(db, sql4, 0, 0, &err_msg);
    if(rc!=SQLITE_OK) 
      {        
        g_print("SQL error: %s\n", err_msg);
        sqlite3_free(err_msg);
        err_msg=NULL;
      }

    //Create an index.
    rc=sqlite3_exec(db, sql3, 0, 0, &err_msg);
    if(rc!=SQLITE_OK) 
      {        
        g_print("SQL error: %s\n", err_msg);
        sqlite3_free(err_msg);
        err_msg=NULL;
      }
  }
static void insert_data_prepared()
  {
    //Test a prepared statement. Just load a second table for comparison.
    gint i=0;
    gint j=0;
    gchar *err_msg=NULL;
    gint rc=SQLITE_OK;
    sqlite3_stmt *stmt1=NULL;

    //Some critters.
    gchar *strings[25]={"Parrot", "Chimpanzee", "Crow", "Dolphin", "Alligator", "Whale", "Zebra", "Owl", "Leopard", "Koala", "Eagle", "Frog", "Moose", "Kangaroo", "Tortoise", "Bear", "Shark", "Panda", "Penguin", "Walrus", "Woodpecker", "Piranha", "Wolverine", "Elephant", "Turtle"};

    //Create table.
    gchar *sql1="CREATE TABLE Critters2(CritterId INTEGER PRIMARY KEY, CritterName TEXT);";           
    rc=sqlite3_exec(db, sql1, 0, 0, &err_msg);
    if(rc!=SQLITE_OK) 
      {        
        g_print("SQL error: %s\n", err_msg);
        sqlite3_free(err_msg);
        err_msg=NULL;
      }

    //Load the data with a prepared statement.
    sqlite3_prepare(db, "INSERT INTO Critters2(CritterName) VALUES(?);", -1, &stmt1, 0);
    sqlite3_exec(db, "BEGIN TRANSACTION;", NULL, NULL, NULL);      
    for(i=0;i<1000;i++)
      {
        for(j=0;j<25;j++)
          {
            sqlite3_bind_text(stmt1, 1, strings[j], -1, SQLITE_STATIC);
            sqlite3_step(stmt1);
            sqlite3_reset(stmt1); 
          }
       }
    sqlite3_exec(db, "COMMIT TRANSACTION;", NULL, NULL, NULL);
    sqlite3_finalize(stmt1);

    gchar *sql3="CREATE INDEX IndexCritterName2 ON Critters2(CritterName);"; 

    gchar *sql4="BEGIN TRANSACTION;"
               "INSERT INTO Critters2(CritterName) VALUES('Squirrel');" 
               "INSERT INTO Critters2(CritterName) VALUES('Warbler');" 
               "INSERT INTO Critters2(CritterName) VALUES('Wombat');"
               "INSERT INTO Critters2(CritterName) VALUES('Crocodile');" 
               "INSERT INTO Critters2(CritterName) VALUES('Lion');" 
               "END TRANSACTION;";

    //Append some extra data to the end of the table.
    rc=sqlite3_exec(db, sql4, 0, 0, &err_msg);
    if(rc!=SQLITE_OK) 
      {        
        g_print("SQL error: %s\n", err_msg);
        sqlite3_free(err_msg);
        err_msg=NULL;
      }

    //Create an index.
    rc=sqlite3_exec(db, sql3, 0, 0, &err_msg);
    if(rc!=SQLITE_OK) 
      {        
        g_print("SQL error: %s\n", err_msg);
        sqlite3_free(err_msg);
        err_msg=NULL;
      }
  }
static void close_program(GtkWidget *widget, gpointer data)
  {
    sqlite3_close(db);
    gtk_main_quit();
  }    
