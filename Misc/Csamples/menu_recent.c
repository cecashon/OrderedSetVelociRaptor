
/*
     Test a gtk_recent_chooser_menu with a image widget.

    Make sure on Ubuntu that Settings/Privacy/Usage and History is turned on for the recent menu
to work.

    gcc -Wall menu_recent.c -o menu_recent `pkg-config --cflags --libs gtk+-3.0`

    Tested on Ubuntu18.04 and GTK3.22
*/

#include<gtk/gtk.h>

static void item_activated(GtkRecentChooser *chooser, GtkWidget **widgets);
static void open_image_dialog(GtkWidget *menu, GtkWidget **widgets);
static void close_current_image(GtkWidget *menu, GtkWidget **widgets);

int main(int argc, char **argv)
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Menu Recent");
    gtk_window_set_default_size(GTK_WINDOW(window), 400, 400);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    
    GtkWidget *menu=gtk_menu_new();
    GtkWidget *open_image=gtk_menu_item_new_with_label("Open Image");
    GtkWidget *close_image=gtk_menu_item_new_with_label("Close Image");
    GtkWidget *recent_image=gtk_menu_item_new_with_label("Recent Image");
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), open_image);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), close_image);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), recent_image);

    GtkRecentManager *manager=gtk_recent_manager_get_default();
    GtkWidget *chooser=gtk_recent_chooser_menu_new_for_manager(manager);
    GtkRecentFilter *filter=gtk_recent_filter_new();
    gtk_recent_filter_add_pixbuf_formats(filter);
    gtk_recent_chooser_set_filter(GTK_RECENT_CHOOSER(chooser), filter);

    gtk_menu_item_set_submenu(GTK_MENU_ITEM(recent_image), chooser);
    gtk_recent_chooser_set_show_not_found(GTK_RECENT_CHOOSER(chooser), FALSE);
    gtk_recent_chooser_set_limit(GTK_RECENT_CHOOSER(chooser), 5);
    gtk_recent_chooser_set_local_only(GTK_RECENT_CHOOSER(chooser), TRUE);
    gtk_recent_chooser_set_sort_type(GTK_RECENT_CHOOSER(chooser), GTK_RECENT_SORT_MRU);

    GtkWidget *menu_bar=gtk_menu_bar_new();
    GtkWidget *file=gtk_menu_item_new_with_label("File");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(file), menu);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), file);

    GtkWidget *image=gtk_image_new();
    gtk_widget_set_vexpand(image, TRUE);
    gtk_widget_set_hexpand(image, TRUE);

    GtkWidget *widgets[]={window, image};
    g_signal_connect(open_image, "activate", G_CALLBACK(open_image_dialog), widgets);
    g_signal_connect(close_image, "activate", G_CALLBACK(close_current_image), widgets);
    g_signal_connect(chooser, "item-activated", G_CALLBACK(item_activated), widgets);

    GtkWidget *scrolled_win=gtk_scrolled_window_new(NULL, NULL);
    gtk_container_add(GTK_CONTAINER(scrolled_win), image);

    GtkWidget *grid=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid), menu_bar, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), scrolled_win, 0, 1, 1, 1);

    gtk_container_add(GTK_CONTAINER(window), grid);

    gtk_widget_show_all(window);

    gtk_main();

    return 0;  
  }
static void item_activated(GtkRecentChooser *chooser, GtkWidget **widgets)
  {
    GtkRecentInfo *info=gtk_recent_chooser_get_current_item(chooser);
    //g_print("%s\n", gtk_recent_info_get_uri(info));
    gchar *file_name=g_filename_from_uri(gtk_recent_info_get_uri(info), NULL, NULL);
    gtk_image_set_from_file(GTK_IMAGE(widgets[1]), file_name);
    gtk_recent_info_unref(info);
    g_free(file_name);
  }
static void open_image_dialog(GtkWidget *menu, GtkWidget **widgets)
  {
    GtkWidget *dialog=gtk_file_chooser_dialog_new("Open File", GTK_WINDOW(widgets[0]), GTK_FILE_CHOOSER_ACTION_OPEN, "Cancel", GTK_RESPONSE_CANCEL, "Open", GTK_RESPONSE_ACCEPT, NULL);
    GtkFileFilter *filter=gtk_file_filter_new();
    gtk_file_filter_add_pixbuf_formats(filter);
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);

    gint result=gtk_dialog_run(GTK_DIALOG(dialog));
    if(result==GTK_RESPONSE_ACCEPT)
      {
        gchar *file_name=gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
        gchar *file_uri=gtk_file_chooser_get_uri(GTK_FILE_CHOOSER(dialog));
        //g_print("%s\n", file_name);
        //g_print("%s\n", file_uri);
        gtk_image_set_from_file(GTK_IMAGE(widgets[1]), file_name);
        GtkRecentManager *manager=gtk_recent_manager_get_default();
        gtk_recent_manager_add_item(manager, file_uri);        
        g_free(file_name);
        g_free(file_uri);
      }

    gtk_widget_destroy(dialog);
  }
static void close_current_image(GtkWidget *menu, GtkWidget **widgets)
  {
    gtk_image_clear(GTK_IMAGE(widgets[1]));
  }
