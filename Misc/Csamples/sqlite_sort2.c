
/*
   Use an in memory sqlite database to update a treeview.

   gcc -Wall sqlite_sort2.c -o sqlite_sort2 `pkg-config --cflags --libs gtk+-3.0` -lsqlite3

   Ubuntu18.04 with GTK3.22
*/

#include<gtk/gtk.h>
#include<sqlite3.h>

static sqlite3 *db=NULL;

static void initialize_database();
static gboolean update_sql_table(gpointer *data);
//Initial sql liststore for the treeview on startup.
static void run_sql(GtkWidget *button, gpointer *data);
static void get_sqlite_data(const gchar *sql_string, gpointer *data);
//Save sort order of third column.
static void column_clicked(GtkWidget *column, gpointer data);
static void close_program(GtkWidget *widget, gpointer data);

static gint current_sort=1;
static gchar *update_sql[7]={"INSERT INTO Tree VALUES(4, 'Spruce');", 
                             "INSERT INTO Trees VALUES(10, 4, 1, 'Norway Spruce');", 
                             "INSERT INTO Trees VALUES(11, 4, 2, 'Blue Spruce');",                
                             "INSERT INTO Trees VALUES(12, 2, 3, 'Stone Pine');", 
                             "INSERT INTO Trees VALUES(13, 2, 4, 'Scots Pine');",
                             "INSERT INTO Trees VALUES(14, 2, 5, 'Sugar Pine');",
                             "INSERT INTO Trees VALUES(15, 2, 6, 'Jack Pine');"};

               

int main(int argc, char *argv[])
  {
    gtk_init(&argc, &argv);

    GtkWidget *window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "SQLite Sort");
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(window), 10);
    gtk_window_set_default_size(GTK_WINDOW(window), 600, 400);
    g_signal_connect(window, "destroy", G_CALLBACK(close_program), NULL);

    initialize_database();

    GtkWidget *tree=gtk_tree_view_new();
    gtk_tree_view_set_grid_lines(GTK_TREE_VIEW(tree), GTK_TREE_VIEW_GRID_LINES_BOTH);
    gtk_widget_set_hexpand(tree, TRUE);
    gtk_widget_set_vexpand(tree, TRUE);

    GtkTreeSelection *selection=gtk_tree_view_get_selection(GTK_TREE_VIEW(tree));
    gtk_tree_selection_set_mode(selection, GTK_SELECTION_SINGLE);

    GtkWidget *scroll=gtk_scrolled_window_new(NULL, NULL);
    gtk_container_add(GTK_CONTAINER(scroll), tree);

    GtkWidget *textview=gtk_text_view_new();
    gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(textview), GTK_WRAP_WORD);
    gtk_widget_set_hexpand(textview, TRUE);
    GtkTextBuffer *buffer=gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
    gtk_text_buffer_set_text(buffer, "SELECT T2.Treesid, T1.Treeid, T2.Fk, T1.TreeType, T2.NameID, T2.TreeName FROM Tree T1 INNER JOIN Trees T2 ON T1.Treeid=T2.Fk ORDER BY T1.Treeid DESC;", -1);

    GtkWidget *button=gtk_button_new_with_label("Run SQL");
    gpointer widgets[2]={buffer, tree};
    g_signal_connect(button, "clicked", G_CALLBACK(run_sql), widgets);

    GtkWidget *grid=gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid), scroll, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), textview, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), button, 0, 2, 1, 1);

    gtk_container_add(GTK_CONTAINER(window), grid);
   
    gtk_widget_show_all(window);

    //Get initial data sorted for the treeview.
    run_sql(button, widgets);
    //Feed the sqllite tables a few records.
    g_timeout_add_seconds(2, (GSourceFunc)update_sql_table, widgets);

    gtk_main();
    return 0;   
  }
static void initialize_database()
  {
    gchar *err_msg=NULL;
    gint rc=sqlite3_open(":memory:", &db);

    if(rc!=SQLITE_OK) 
      {       
        g_print("Cannot open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
      }

    gchar *sql="CREATE TABLE Tree(TreeId INTEGER PRIMARY KEY, TreeType TEXT);" 
               "INSERT INTO Tree VALUES(1, 'Cedar');" 
               "INSERT INTO Tree VALUES(2, 'Pine');" 
               "INSERT INTO Tree VALUES(3, 'Fir');"
               "CREATE TABLE Trees(TreesId INTEGER PRIMARY KEY, Fk INT, NameID INT, TreeName TEXT);" 
               "INSERT INTO Trees VALUES(1, 1, 1, 'Western Red Cedar');" 
               "INSERT INTO Trees VALUES(2, 1, 2, 'Deodar Cedar');" 
               "INSERT INTO Trees VALUES(3, 1, 3, 'Insense Cedar');" 
               "INSERT INTO Trees VALUES(4, 2, 1, 'Western White Pine');" 
               "INSERT INTO Trees VALUES(5, 2, 2, 'Long Leaf Pine');"
               "INSERT INTO Trees VALUES(6, 3, 1, 'Grand Fir');"
               "INSERT INTO Trees VALUES(7, 3, 2, 'Noble Fir');"
               "INSERT INTO Trees VALUES(8, 3, 3, 'Balsam Fir');"
               "INSERT INTO Trees VALUES(9, 3, 4, 'Fraser Fir');";

    rc=sqlite3_exec(db, sql, 0, 0, &err_msg);

    if (rc!=SQLITE_OK) 
      {        
        g_print("Failed to create table\n");
        g_print("SQL error: %s\n", err_msg);
        sqlite3_free(err_msg);
      }
  }
static gboolean update_sql_table(gpointer *data)
  {
    static int i=0;
    gchar *err_msg=NULL;
    gint rc=0;

    g_print("%i %s\n", i, update_sql[i]);
    rc=sqlite3_exec(db, update_sql[i], 0, 0, &err_msg);

    if (rc!=SQLITE_OK) 
      {        
        g_print("Failed to create table\n");
        g_print("SQL error: %s\n", err_msg);
        sqlite3_free(err_msg);
      }

    run_sql(NULL, data);

    i++;
    if(i<7) return TRUE;
    else return FALSE;
  }
static void run_sql(GtkWidget *button, gpointer *data)
  {
    GtkTextIter start;
    GtkTextIter end;
    gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(data[0]), &start, &end);
    gchar *sql_string=gtk_text_buffer_get_text(GTK_TEXT_BUFFER(data[0]), &start, &end, -1);
    get_sqlite_data(sql_string, data);
    g_free(sql_string);         
  }
static void get_sqlite_data(const gchar *sql_string, gpointer *data)
  {
    gint i=0;
    gint columns=0;
    GtkTreeIter iter;
    GtkTreeIter s_iter;
    gint ret_val=0;
    sqlite3_stmt *stmt1=NULL;
    GPtrArray *column_names=g_ptr_array_new_with_free_func(g_free);
    GtkListStore *new_store=NULL;

    //Prepare the incoming SQL statement.
    sqlite3_prepare_v2(db, sql_string, -1, &stmt1, 0);
    columns=sqlite3_column_count(stmt1);
    new_store=gtk_list_store_new(1, G_TYPE_STRING);

    gint row_id=0;
    GtkTreeModel *model=gtk_tree_view_get_model(GTK_TREE_VIEW(data[1]));
    GtkTreeSelection *selection=gtk_tree_view_get_selection(GTK_TREE_VIEW(data[1]));
    gboolean sel=gtk_tree_selection_get_selected(selection, NULL, &s_iter);
    if(sel) gtk_tree_model_get(model, &s_iter, 0, &row_id, -1);

    //Check the statement.
    if(stmt1!=NULL)
      {
        ret_val=sqlite3_step(stmt1);
        //Check generalized column types. SQLite dynamically typed so this may be trouble for mixed data.
        gint column_types[columns];
        GType type_array[columns];
        for(i=0;i<columns;i++)
          {
            column_types[i]=sqlite3_column_type(stmt1, i);
            switch(column_types[i])
              {
                case 1:
                  type_array[i]=G_TYPE_INT;
                  break;
                case 2:
                  type_array[i]=G_TYPE_DOUBLE;
                  break;
                case 3:
                  type_array[i]=G_TYPE_STRING;
                  break;
                default:
                  g_print("Column Type Error\n");
               }                  
            g_ptr_array_add(column_names, g_strdup(sqlite3_column_name(stmt1, i)));
          }
        //Reset the list store based on column types.
        if(sql_string!=NULL)
          {
            gtk_list_store_set_column_types(GTK_LIST_STORE(new_store), columns, type_array);
          }
        //Get the rows.
        while(ret_val==SQLITE_ROW)
          {            
            //Load the list store with data.
            gtk_list_store_append(new_store, &iter);
            for(i=0;i<columns;i++)
              {
                switch(column_types[i])
                  {
                    case SQLITE_INTEGER:
                      gtk_list_store_set(new_store, &iter, i, sqlite3_column_int(stmt1, i), -1);
                      break;
                    case SQLITE_FLOAT:
                      gtk_list_store_set(new_store, &iter, i, sqlite3_column_double(stmt1, i), -1);
                      break;
                    case SQLITE_TEXT:
                      gtk_list_store_set(new_store, &iter, i, sqlite3_column_text(stmt1, i), -1);
                      break;
                    default:
                      g_print("Column Type Error\n");
                  }
              }
            
            ret_val=sqlite3_step(stmt1);
          }
      }
    else            
      {
        const gchar *message=sqlite3_errmsg(db);
        g_print("%s\n", message);
      }
    if(stmt1!=NULL) sqlite3_finalize(stmt1);

    //Setup new treeview.
    if(stmt1!=NULL)
      {
        //Set new model.
        gtk_tree_view_set_model(GTK_TREE_VIEW(data[1]), GTK_TREE_MODEL(new_store));
        g_object_unref(new_store);
        //Drop old columns. 
        gint n_columns=gtk_tree_view_get_n_columns(GTK_TREE_VIEW(data[1]));
        GtkTreeViewColumn *t_column=NULL;
        for(i=0;i<n_columns;i++)
          {
            t_column=gtk_tree_view_get_column(GTK_TREE_VIEW(data[1]), 0);
            gtk_tree_view_remove_column(GTK_TREE_VIEW(data[1]), t_column);
          }
        //Setup new columns.
        GtkCellRenderer *renderer=gtk_cell_renderer_text_new();
        g_object_set(renderer, "xalign", 0.5, "editable", FALSE, NULL);
        for(i=0;i<columns;i++)
          {
            t_column=gtk_tree_view_column_new_with_attributes(g_ptr_array_index(column_names, i), GTK_CELL_RENDERER(renderer) , "text", i, NULL);
            //Sort on TreeType column. Maybe compare name.
            if(i==3)
              {
                gtk_tree_view_column_set_sort_column_id(t_column, 3);
                g_signal_connect(t_column, "clicked", G_CALLBACK(column_clicked), NULL);
              }
            gtk_tree_view_column_set_alignment(t_column, 0.5);
            gtk_tree_view_column_set_resizable(t_column, TRUE);
            gtk_tree_view_append_column(GTK_TREE_VIEW(data[1]), t_column);
          }
      }
    
    //Apply the previous selection and scroll to the selection if need be.
    gint value=0;
    GtkTreeModel *model2=gtk_tree_view_get_model(GTK_TREE_VIEW(data[1]));
    if(sel)
      {
        if(gtk_tree_model_get_iter_first(model2, &s_iter))
          {
            do{
                gtk_tree_model_get(model2, &s_iter, 0, &value, -1);
                if(value==row_id)
                  {
                    gtk_tree_selection_select_iter(selection, &s_iter);
                    GtkTreePath *path=gtk_tree_model_get_path(model2, &s_iter);
                    gtk_tree_view_scroll_to_cell(GTK_TREE_VIEW(data[1]), path, NULL, FALSE, 0.0, 0.0);
                    gtk_tree_path_free(path);
                  }
              }while(gtk_tree_model_iter_next(model2, &s_iter));
          }
      }

    //Apply previous sort.
    gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(model2), 3, current_sort);

    g_ptr_array_free(column_names, TRUE);
  }
static void column_clicked(GtkWidget *column, gpointer data)
  {
    current_sort=gtk_tree_view_column_get_sort_order(GTK_TREE_VIEW_COLUMN(column));
    if(current_sort==GTK_SORT_ASCENDING) g_print("Sort Ascending\n");
    else g_print("Sort Descending\n");
  }
static void close_program(GtkWidget *widget, gpointer data)
  {
    sqlite3_close(db);
    gtk_main_quit();
  }    
