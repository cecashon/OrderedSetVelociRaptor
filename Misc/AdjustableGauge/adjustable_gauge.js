
//For use with introspection and the adjustable_gauge widget. See adjustable_gauge.c for setting it up.

const Lang = imports.lang;
const Gtk = imports.gi.Gtk;
const Adjustable = imports.gi.Adjustable; 

Gtk.init(null);

const TestGauge = new Lang.Class({
    Name: 'Gauge',
    Extends: Gtk.Window,

    _init: function() {
        this.parent({title: "AdjustableGauge2"});
        this.set_default_size(400, 400);

        this.gauge = new Adjustable.Gauge();
        this.gauge.set_draw_gradient(true);
        this.gauge.set_drawing(1);
        
        this.add(this.gauge);
    },
});

let win = new TestGauge();
win.connect("delete-event", Gtk.main_quit);
win.show_all();
Gtk.main();
