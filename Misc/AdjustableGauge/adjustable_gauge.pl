
#For use with introspection and the adjustable_gauge widget. See adjustable_gauge.c for setting it up.

use strict;
use diagnostics;
use warnings;
use Gtk3 '-init';

Glib::Object::Introspection->setup(basename => 'Adjustable', version => '0.1', package => 'Adjustable');

package TestGauge;
sub new
{
    my $class = shift;
    my $self = {};
    bless $self, $class;

    $self->{'window'} = Gtk3::Window->new();
    $self->{'window'}->signal_connect('delete_event' => sub { Gtk3->main_quit; });
    $self->{'window'}->set_default_size(400, 400);
    $self->{'window'}->set_title("AdjustableGauge2");

    $self->{'gauge'} = Adjustable::Gauge->new();
    $self->{'gauge'}->set_draw_gradient(1);
    $self->{'gauge'}->set_drawing(1);

    $self->{'window'}->add($self->{'gauge'});

    return $self;
}
my $win = new TestGauge();
$win->{'window'}->show_all();
Gtk3->main;




