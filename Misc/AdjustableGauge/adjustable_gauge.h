
/*

    For use with adjustable_gauge.c. Look in adjustable_gauge_main.c for more information.

    C. Eric Cashon

*/

#ifndef __ADJUSTABLE_GAUGE_H__
#define __ADJUSTABLE_GAUGE_H__

#include<gtk/gtk.h>

G_BEGIN_DECLS

/**
 * AdjustableGauge:
 *
 * A AdjustableGauge structure.
 *
 */
struct _AdjustableGauge
{
  /*< private >*/
  GtkDrawingArea da;
};

#define ADJUSTABLE_GAUGE_TYPE adjustable_gauge_get_type()

G_DECLARE_FINAL_TYPE(AdjustableGauge, adjustable_gauge, ADJUSTABLE, GAUGE, GtkDrawingArea)

//Adjustable gauge names are just how they were thought of while doing the drawing.
enum
{
  VOLTAGE_GAUGE,
  SPEEDOMETER_GAUGE
};

//Public functions.
GtkWidget* adjustable_gauge_new();

void adjustable_gauge_set_first_cutoff(AdjustableGauge *da, gdouble first_cutoff);
gdouble adjustable_gauge_get_first_cutoff(AdjustableGauge *da);
void adjustable_gauge_set_second_cutoff(AdjustableGauge *da, gdouble second_cutoff);
gdouble adjustable_gauge_get_second_cutoff(AdjustableGauge *da);
void adjustable_gauge_set_needle(AdjustableGauge *da, gdouble needle);
gdouble adjustable_gauge_get_needle(AdjustableGauge *da);
void adjustable_gauge_set_scale_bottom(AdjustableGauge *da, gdouble scale_bottom);
gdouble adjustable_gauge_get_scale_bottom(AdjustableGauge *da);
void adjustable_gauge_set_scale_top(AdjustableGauge *da, gdouble scale_top);
gdouble adjustable_gauge_get_scale_top(AdjustableGauge *da);
void adjustable_gauge_set_inside_radius(AdjustableGauge *da, gdouble inside_radius);
gdouble adjustable_gauge_get_inside_radius(AdjustableGauge *da);
void adjustable_gauge_set_outside_radius(AdjustableGauge *da, gdouble outside_radius);
gdouble adjustable_gauge_get_outside_radius(AdjustableGauge *da);
void adjustable_gauge_set_drawing(AdjustableGauge *da, gint drawing_name);
gint adjustable_gauge_get_drawing(AdjustableGauge *da);
void adjustable_gauge_set_draw_gradient(AdjustableGauge *da, gboolean draw_gradient);
gboolean adjustable_gauge_get_draw_gradient(AdjustableGauge *da);
void adjustable_gauge_set_units_text(AdjustableGauge *da, const gchar *units_text_string);
const gchar* adjustable_gauge_get_units_text(AdjustableGauge *da);
void adjustable_gauge_set_use_thread(AdjustableGauge *da, gboolean use_thread);
gboolean adjustable_gauge_get_use_thread(AdjustableGauge *da);
//Set and get colors.
void adjustable_gauge_set_background(AdjustableGauge *da, const gchar *background_string);
const gchar* adjustable_gauge_get_background(AdjustableGauge *da);
void adjustable_gauge_set_text_color(AdjustableGauge *da, const gchar *text_color_string);
const gchar* adjustable_gauge_get_text_color(AdjustableGauge *da);
void adjustable_gauge_set_arc_color1(AdjustableGauge *da, const gchar *arc_color_string1);
const gchar* adjustable_gauge_get_arc_color1(AdjustableGauge *da);
void adjustable_gauge_set_arc_color2(AdjustableGauge *da, const gchar *arc_color_string2);
const gchar* adjustable_gauge_get_arc_color2(AdjustableGauge *da);
void adjustable_gauge_set_arc_color3(AdjustableGauge *da, const gchar *arc_color_string3);
const gchar* adjustable_gauge_get_arc_color3(AdjustableGauge *da);
void adjustable_gauge_set_needle_color(AdjustableGauge *da, const gchar *needle_color_string);
const gchar* adjustable_gauge_get_needle_color(AdjustableGauge *da);

G_END_DECLS

#endif 
