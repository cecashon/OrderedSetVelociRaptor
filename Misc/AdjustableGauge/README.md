

--This is a simple gauge widget derived from a GTK drawing area widget. There is a "voltage" and "speedometer" gauge to choose from. The colors can be drawn with a gradient or as solid colors.

--There are some instructions to setup introspection in the adjustable_gauge.c file. That way the gauges can be used from higher level languages like Python, Perl or Java Script.

Gauges

![ScreenShot](/Misc/AdjustableGauge/gauges2.png)

