
#For use with introspection and the adjustable_gauge widget. See adjustable_gauge.c for setting it up.

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Adjustable', '0.1')
from gi.repository import Gtk, Adjustable

class TestGauge(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="AdjustableGauge2")
        self.set_default_size(400, 400)

        self.gauge=Adjustable.Gauge()
        self.gauge.set_draw_gradient(True)
        self.gauge.set_drawing(1)

        self.add(self.gauge)

win = TestGauge()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()
