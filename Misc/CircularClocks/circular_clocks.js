
//For use with introspection and the circular_clocks widget. See circular_clocks.c for setting it up.

const Lang = imports.lang;
const Gtk = imports.gi.Gtk;
const Circular = imports.gi.Circular; 

Gtk.init(null);

const TestClock = new Lang.Class({
    Name: 'Clock',
    Extends: Gtk.Window,

    _init: function() {
        this.parent({title: "CircularClocks"});
        this.set_default_size(400, 400);

        this.clock = new Circular.Clocks();
        this.clock.set_clock(1);
        
        this.add(this.clock);
    },
});

let win = new TestClock();
win.connect("delete-event", Gtk.main_quit);
win.show_all();
Gtk.main();
