

--A circular clock widget. It has seven different types of clocks to choose from. There are some good examples of using trig, linear algebra, calculus, etc. to draw and animate with cairo and gtk. Getting a little artsy with some math. If you have a clock idea, maybe it has a few things to help get started.

--There are some instructions in the circular_clocks.c file for setting up introspection. Then
the widget can be used from higher level languages like Python, Perl or Java Script. 

Circular Clocks

![ScreenShot](/Misc/CircularClocks/clocks.png)

