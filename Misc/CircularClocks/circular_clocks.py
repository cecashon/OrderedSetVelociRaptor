
#For use with introspection and the circular_clocks widget. See circular_clocks.c for setting it up.

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Circular', '0.1')
from gi.repository import Gtk, Circular

class TestClock(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="CircularClocks")
        self.set_default_size(400, 400)

        self.clock=Circular.Clocks()
        self.clock.set_clock(1)

        self.add(self.clock)

win = TestClock()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()
