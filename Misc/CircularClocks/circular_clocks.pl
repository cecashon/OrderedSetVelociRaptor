
#For use with introspection and the circular_clocks widget. See circular_clocks.c for setting it up.

use strict;
use diagnostics;
use warnings;
use Gtk3 '-init';

Glib::Object::Introspection->setup(basename => 'Circular', version => '0.1', package => 'Circular');

package TestClock;
sub new
{
    my $class = shift;
    my $self = {};
    bless $self, $class;

    $self->{'window'} = Gtk3::Window->new();
    $self->{'window'}->signal_connect('delete_event' => sub { Gtk3->main_quit; });
    $self->{'window'}->set_default_size(400, 400);
    $self->{'window'}->set_title("CircularClocks");

    $self->{'clock'} = Circular::Clocks->new();
    $self->{'clock'}->set_clock(1);

    $self->{'window'}->add($self->{'clock'});

    return $self;
}
my $win = new TestClock();
$win->{'window'}->show_all();
Gtk3->main;




