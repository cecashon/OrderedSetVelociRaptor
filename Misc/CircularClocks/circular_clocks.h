
/*

    For use with circular_clocks.c. Look in circular_clocks_main.c for more information.

    C. Eric Cashon

*/

#ifndef __CIRCULAR_CLOCKS_H__
#define __CIRCULAR_CLOCKS_H__

#include<gtk/gtk.h>

G_BEGIN_DECLS

/**
 * CircularClocks:
 *
 * A CircularClocks structure.
 *
 */
struct _CircularClocks
{
  /*< private >*/
  GtkDrawingArea da;
};

#define CIRCULAR_CLOCKS_TYPE circular_clocks_get_type()

G_DECLARE_FINAL_TYPE(CircularClocks, circular_clocks, CIRCULAR, CLOCKS, GtkDrawingArea)

//The different types of circular clocks.
enum
{
  GRADIENT_CLOCK,
  WAVE_CLOCK,
  BRAID_CLOCK,
  GEAR_CLOCK,
  GOURAUD_MESH_CLOCK,
  GEM_CLOCK,
  SPACE_CLOCK
};

//Public functions.
GtkWidget* circular_clocks_new();
void circular_clocks_set_clock(CircularClocks *da, gint clock_name);
gint circular_clocks_get_clock(CircularClocks *da);
void circular_clocks_set_background_color(CircularClocks *da, const gchar *bc_string);
const gchar* circular_clocks_get_background_color(CircularClocks *da);

G_END_DECLS

#endif 
