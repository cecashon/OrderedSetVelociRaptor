
#For use with introspection and the circuit_breaker_switch widget. See circuit_breaker_switch.c for setting it up.

use strict;
use diagnostics;
use warnings;
use Gtk3 '-init';

Glib::Object::Introspection->setup(basename => 'Circuit', version => '0.1', package => 'Circuit');

package TestBreaker;
sub new
{
    my $class = shift;
    my $self = {};
    bless $self, $class;

    $self->{'window'} = Gtk3::Window->new();
    $self->{'window'}->signal_connect('delete_event' => sub { Gtk3->main_quit; });
    $self->{'window'}->set_default_size(200, 100);
    $self->{'window'}->set_title("CircuitBreakerSwitch");

    $self->{'breaker'} = Circuit::BreakerSwitch->new();

    $self->{'window'}->add($self->{'breaker'});

    return $self;
}
my $win = new TestBreaker();
$win->{'window'}->show_all();
Gtk3->main;




