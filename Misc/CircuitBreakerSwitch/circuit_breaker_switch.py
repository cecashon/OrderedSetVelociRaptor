
#For use with introspection and the circuit_breaker_switch widget. See circuit_breaker_switch.c for setting it up.

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Circuit', '0.1')
from gi.repository import Gtk, Circuit

class TestBreaker(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="CircuitBreakerSwitch")
        self.set_default_size(200, 100)

        self.breaker=Circuit.BreakerSwitch()

        self.add(self.breaker)

win = TestBreaker()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()
