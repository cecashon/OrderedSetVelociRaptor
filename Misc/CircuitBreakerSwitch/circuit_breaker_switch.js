
//For use with introspection and the circuit_breaker_switch widget. See circuit_breaker_switch.c for setting it up.

const Lang = imports.lang;
const Gtk = imports.gi.Gtk;
const Circuit = imports.gi.Circuit; 

Gtk.init(null);

const TestBreaker = new Lang.Class({
    Name: 'Circuit',
    Extends: Gtk.Window,

    _init: function() {
        this.parent({title: "CircuitBreakerSwitch"});
        this.set_default_size(200, 100);

        this.breaker = new Circuit.BreakerSwitch();
        
        this.add(this.breaker);
    },
});

let win = new TestBreaker();
win.connect("delete-event", Gtk.main_quit);
win.show_all();
Gtk.main();
