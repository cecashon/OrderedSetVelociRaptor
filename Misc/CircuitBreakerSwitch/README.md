

--A circuit breaker switch widget with four states. Watch the electrons flow. Easy to change colors and things around. The electrons might not be needed. Experimental. There is a lightning bolt icon or you can use the text option for the different states.

--There are some instructions in the circuit_breaker_switch.c file for setting up introspection. Then
the widget can be used from higher level languages like Python, Perl or Java Script.

Circuit Breaker Switches

![ScreenShot](/Misc/CircuitBreakerSwitch/circuit_breaker.png)

