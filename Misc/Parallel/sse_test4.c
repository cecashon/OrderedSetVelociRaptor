
/* 
    Compare pointers, vectors(sse) and openmp to see if there is a difference in performance. See if
vectors might be a good option for 3d rotations in /cairo_drawings/spring1.c.
    Direct access to graphene_simd4f_t fields was removed in graphene so the spring5 test uses __m128.
    
    https://github.com/ebassi/graphene

    gcc -Wall -fopenmp -msse2 -msse3 -g -std=c11 sse_test4.c -o sse_test4 -lm -Wl,-rpath=/home/eric/Graphene/graphene-master/build/src libgraphene-1.0.so

    Valgrind it.

    valgrind --leak-check=full --show-leak-kinds=all ./sse_test4

    Some info about openmp and valgrind.
    https://gcc.gnu.org/bugzilla/show_bug.cgi?id=36298

    Tested on Ubuntu20.04 with a 64-bit Intel i7 Sandy Bridge.

*/

#include<x86intrin.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>
#include<stdbool.h> 
#include<math.h>
#include"graphene.h"

#define M_PI 3.14159265358979323846
#define points 10000

float *spring1=NULL;
float *spring_rotated1=NULL;
typedef float v4sf __attribute__ ((vector_size(16)));
v4sf *spring2=NULL;
v4sf *spring_rotated2=NULL;
graphene_point3d_t *spring3=NULL;
graphene_point3d_t *spring_rotated3=NULL;
v4sf *spring4=NULL;
v4sf *spring_rotated4=NULL;
__m128 *spring5=NULL;
__m128 *spring_rotated5=NULL;

static void initialize_springs();
static void rotate_spring1(bool print_out);
static void rotate_spring2(bool print_out);
static void rotate_spring3(bool print_out);
static void rotate_spring4(bool print_out);
static void rotate_spring5(bool print_out);

int main()
  {
    initialize_springs();

    clock_t start1, end1, start2, end2, start3, end3, start4, end4, start5, end5;
    //Rotate once and then time.
    rotate_spring1(false);
    start1=clock();
    rotate_spring1(true);
    end1=clock();
    printf("  Pointer time %f\n", (double)(end1-start1)/(double)CLOCKS_PER_SEC);

    rotate_spring2(false);
    start2=clock();
    rotate_spring2(true);
    end2=clock();
    printf("  Vector time %f\n", (double)(end2-start2)/(double)CLOCKS_PER_SEC);

    rotate_spring3(false);
    start3=clock();
    rotate_spring3(true);
    end3=clock();
    printf("  OpenMP SSE2 time %f\n", (double)(end3-start3)/(double)CLOCKS_PER_SEC);

    rotate_spring4(false);
    start4=clock();
    rotate_spring4(true);
    end4=clock();
    printf("  Graphene time %f\n", (double)(end4-start4)/(double)CLOCKS_PER_SEC);

    rotate_spring5(false);
    start5=clock();
    rotate_spring5(true);
    end5=clock();
    printf("  SSE3 time %f\n", (double)(end5-start5)/(double)CLOCKS_PER_SEC);

    free(spring1);
    free(spring_rotated1);
    free(spring2);
    free(spring_rotated2);
    free(spring3);
    free(spring_rotated3);
    free(spring4);
    free(spring_rotated4);
    free(spring5);
    free(spring_rotated5);

    return 0;
  }
static void initialize_springs()
  {
    int i=0;

    spring1=(float*)malloc(sizeof(float)*points*4);
    spring_rotated1=(float*)malloc(sizeof(float)*points*4);
    spring2=(v4sf*)aligned_alloc(16, sizeof(v4sf)*points);
    spring_rotated2=(v4sf*)aligned_alloc(16, sizeof(v4sf)*points);
    spring3=(graphene_point3d_t*)aligned_alloc(sizeof(graphene_point3d_t), sizeof(graphene_point3d_t)*points);
    spring_rotated3=(graphene_point3d_t*)aligned_alloc(sizeof(graphene_point3d_t), sizeof(graphene_point3d_t)*points);
    spring4=(v4sf*)aligned_alloc(16, sizeof(v4sf)*points);
    spring_rotated4=(v4sf*)aligned_alloc(16, sizeof(v4sf)*points);
    spring5=(__m128*)aligned_alloc(sizeof(__m128), sizeof(__m128)*points);
    spring_rotated5=(__m128*)aligned_alloc(sizeof(__m128), sizeof(__m128)*points);

    float *f1=spring1;
    for(i=0;i<points;i++)
      {
        (*f1)=(float)(cos(45.0*(double)i*M_PI/180.0f));
        (*(f1+1))=(float)(sin(45.0*(double)i*M_PI/180.0));
        (*(f1+2))=(float)((double)i/20.0);
        (*(f1+3))=1.0f;
        f1+=4;
      }

    v4sf *p1=spring2;
    for(i=0;i<points;i++)
      {
        (*p1)[0]=(float)(cos(45.0*(double)i*M_PI/180.0));
        (*p1)[1]=(float)(sin(45.0*(double)i*M_PI/180.0));
        (*p1)[2]=(float)((double)i/20.0);
        (*p1)[3]=1.0f;
        p1++;
      }

    graphene_point3d_t *g1=spring3;
    for(i=0;i<points;i++)
      {
        g1->x=(float)(cos(45.0*(double)i*M_PI/180.0));
        g1->y=(float)(sin(45.0*(double)i*M_PI/180.0));
        g1->z=(float)((double)i/20.0);
        g1++;
      }

    p1=spring4;
    for(i=0;i<points;i++)
      {
        (*p1)[0]=(float)(cos(45.0*(double)i*M_PI/180.0));
        (*p1)[1]=(float)(sin(45.0*(double)i*M_PI/180.0));
        (*p1)[2]=(float)((double)i/20.0);
        (*p1)[3]=1.0f;
        p1++;
      }

    __m128 *m1=spring5;
    for(i=0;i<points;i++)
      {
        (*m1)[0]=(float)(cos(45.0*(double)i*M_PI/180.0));
        (*m1)[1]=(float)(sin(45.0*(double)i*M_PI/180.0));
        (*m1)[2]=(float)((double)i/20.0);
        (*m1)[3]=1.0f;
        m1++;
      }

  }
static void rotate_spring1(bool print_out)
  {
    //Just use pointers.
    int i=0;
    float r1[4], r2[4], r3[4];
    r1[0]=0.5;r1[1]=0.5;r1[2]=0.5;r1[3]=0.0;
    r2[0]=0.5;r2[1]=0.5;r2[2]=0.5;r2[3]=0.0;
    r3[0]=0.5;r3[1]=0.5;r3[2]=0.5;r3[3]=0.0;
    
    float *p1=spring1;
    float *p2=spring_rotated1;

    if(print_out) printf("Pointer Rotate\n");
    for(i=0;i<points;i++)
      {
        (*p2)=((*p1)*r1[0])+((*(p1+1))*r1[1])+((*(p1+2))*r1[2]);
        (*(p2+1))=((*p1)*r2[0])+((*(p1+1))*r2[1])+((*(p1+2))*r2[2]);
        (*(p2+2))=((*p1)*r3[0])+((*(p1+1))*r3[1])+((*(p1+2))*r3[2]);
        if(i<5&&print_out) printf("    %i %f %f %f\n", i, (*p2), (*(p2+1)), (*(p2+2)));
        p1+=4;p2+=4;
      }
  }
static void rotate_spring2(bool print_out)
  {
    //Use some vector multiplication with the help of gcc.
    int i=0;
    v4sf r1, r2, r3, temp;
    r1[0]=0.5;r1[1]=0.5;r1[2]=0.5;r1[3]=0.0;
    r2[0]=0.5;r2[1]=0.5;r2[2]=0.5;r2[3]=0.0;
    r3[0]=0.5;r3[1]=0.5;r3[2]=0.5;r3[3]=0.0;
    
    v4sf *p1=spring2;
    v4sf *p2=spring_rotated2;

    if(print_out) printf("Vector Rotate\n");
    for(i=0;i<points;i++)
      {
        temp=(*p1)*r1;
        (*p2)[0]=temp[0]+temp[1]+temp[2];
        temp=(*p1)*r2;
        (*p2)[1]=temp[0]+temp[1]+temp[2];
        temp=(*p1)*r3;
        (*p2)[2]=temp[0]+temp[1]+temp[2];
        if(i<5&&print_out) printf("    %i %f %f %f\n", i, (*p2)[0], (*p2)[1], (*p2)[2]);
        p1++;p2++;
      }
  }
static void rotate_spring3(bool print_out)
  {
    //Use openmp with vector multiplication.
    int i=0;
    v4sf r1, r2, r3;
    r1[0]=0.5;r1[1]=0.5;r1[2]=0.5;r1[3]=0.0;
    r2[0]=0.5;r2[1]=0.5;r2[2]=0.5;r2[3]=0.0;
    r3[0]=0.5;r3[1]=0.5;r3[2]=0.5;r3[3]=0.0;
    
    if(print_out) printf("OpenMP SSE2 Rotate\n");
    #pragma omp parallel for
    for(i=0;i<points;i++)
      {
        v4sf *p1=spring4;
        v4sf *p2=spring_rotated4;
        v4sf temp=(*(p1+i))*r1;
        (*(p2+i))[0]=temp[0]+temp[1]+temp[2];
        temp=(*(p1+i))*r2;
        (*(p2+i))[1]=temp[0]+temp[1]+temp[2];
        temp=(*(p1+i))*r3;
        (*(p2+i))[2]=temp[0]+temp[1]+temp[2];
        if(i<5&&print_out) printf("    %i %f %f %f\n", i, (*(p2+i))[0], (*(p2+i))[1], (*(p2+i))[2]);
      }
  }
static void rotate_spring4(bool print_out)
  {
    //Try graphene.
    int i=0;

    const float v[16]={0.5, 0.5, 0.5, 0.0,
                 0.5, 0.5, 0.5, 0.0,
                 0.5, 0.5, 0.5, 0.0,
                 0.0, 0.0, 0.0, 0.0};
    graphene_matrix_t m;
    graphene_matrix_init_from_float(&m, v); 
    
    graphene_point3d_t *p1=spring3;
    graphene_point3d_t *p2=spring_rotated3;

    if(print_out) printf("Graphene Rotate\n");
    for(i=0;i<points;i++)
      {
        graphene_matrix_transform_point3d(&m, p1, p2);
        if(i<5&&print_out) printf("    %i %f %f %f\n", i, p2->x, p2->y, p2->z);
        p1++;p2++;
      }
  }
static void rotate_spring5(bool print_out)
  {
    int i=0;

    __m128 r1, r2, r3, temp;
    r1[0]=0.5;r1[1]=0.5;r1[2]=0.5;r1[3]=0.0;
    r2[0]=0.5;r2[1]=0.5;r2[2]=0.5;r2[3]=0.0;
    r3[0]=0.5;r3[1]=0.5;r3[2]=0.5;r3[3]=0.0;
    
    __m128 *p1=spring5;
    __m128 *p2=spring_rotated5;

    //https://community.intel.com/t5/Intel-C-Compiler/What-s-the-best-way-to-sum-up-values-in-m128/td-p/803575
    if(print_out) printf("SSE3 Rotate\n");
    for(i=0;i<points;i++)
      {
        temp=_mm_mul_ps((*p1), r1);
        temp=_mm_hadd_ps(temp, temp); 
        temp=_mm_hadd_ps(temp, temp);        
        (*p2)[0]=temp[0];
        
        temp=_mm_mul_ps((*p1), r2);
        temp=_mm_hadd_ps(temp, temp);
        temp=_mm_hadd_ps(temp, temp);         
        (*p2)[1]=temp[0];
        
        temp=_mm_mul_ps((*p1), r3);
        temp=_mm_hadd_ps(temp, temp); 
        temp=_mm_hadd_ps(temp, temp);        
        (*p2)[2]=temp[0];
        if(i<5&&print_out) printf("    %i %f %f %f\n", i, (*p2)[0], (*p2)[1], (*p2)[2]);
        p1++;p2++;
      }
  }

  
  
  
