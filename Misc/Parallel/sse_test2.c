
/* 
    Compare pointers and vectors to see if there is a difference in performance. See if
vectors might be a good option for 3d rotations in /cairo_drawings/spring1.c.

    gcc -Wall -msse2 -std=c11 sse_test2.c -o sse_test2 -lm
   
    For assembly output only. Look for xmm registers in the assembly text file for the
vector add function
.
    gcc -Wall -msse2 -std=c11 -S sse_test2.c -o sse_asm2 -lm

    Valgrind it.

    valgrind --leak-check=full --show-leak-kinds=all ./sse_test2

    Tested on Ubuntu16.04 with a 32-bit Intel Atom CPU N270, 1.60GHz

*/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>
#include<math.h>

#define M_PI 3.14159265358979323846

float *spring1=NULL;
float *spring_rotated1=NULL;
//For sse2.
typedef float v4sf __attribute__ ((vector_size(16)));
v4sf *spring2=NULL;
v4sf *spring_rotated2=NULL;

static void initialize_springs();
static void rotate_spring1();
static void rotate_spring2();

int main()
  {
    initialize_springs();

    clock_t start1, end1, start2, end2;
    start1=clock();
    rotate_spring1();
    end1=clock();
    printf("Pointer time %f\n", (double)(end1-start1)/(double)CLOCKS_PER_SEC);

    start2=clock();
    rotate_spring2();
    end2=clock();
    printf("Vector time %f\n", (double)(end2-start2)/(double)CLOCKS_PER_SEC);

    free(spring1);
    free(spring_rotated1);
    free(spring2);
    free(spring_rotated2);

    return 0;
  }
static void initialize_springs()
  {
    int i=0;

    spring1=(float*)malloc(sizeof(float)*320);
    spring_rotated1=(float*)malloc(sizeof(float)*320);
    spring2=(v4sf*)aligned_alloc(16, sizeof(v4sf)*80);
    spring_rotated2=(v4sf*)aligned_alloc(16, sizeof(v4sf)*80);

    float *f1=spring1;
    for(i=0;i<80;i++)
      {
        (*f1)=cos(45.0*(double)i*M_PI/180.0);
        (*(f1+1))=sin(45.0*(double)i*M_PI/180.0);
        (*(f1+2))=(double)i/20.0;
        (*(f1+3))=0.0;
        f1+=4;
      }

    v4sf *p1=spring2;
    for(i=0;i<80;i++)
      {
        (*p1)[0]=cos(45.0*(double)i*M_PI/180.0);
        (*p1)[1]=sin(45.0*(double)i*M_PI/180.0);
        (*p1)[2]=(double)i/20.0;
        (*p1)[3]=0.0;
        p1++;
      }
  }
static void rotate_spring1()
  {
    //Just use pointers.
    int i=0;
    float r1[4], r2[4], r3[4];
    r1[0]=0.5;r1[1]=0.5;r1[2]=0.5;r1[3]=0.5;
    r2[0]=0.5;r2[1]=0.5;r2[2]=0.5;r2[3]=0.5;
    r3[0]=0.5;r3[1]=0.5;r3[2]=0.5;r3[3]=0.5;
    
    float *p1=spring1;
    float *p2=spring_rotated1;

    for(i=0;i<80;i++)
      {
        (*p2)=((*p1)*r1[0])+((*(p1+1))*r1[1])+((*(p1+2))*r1[2]);
        (*(p2+1))=((*p1)*r2[0])+((*(p1+1))*r2[1])+((*(p1+2))*r2[2]);
        (*(p2+2))=((*p1)*r3[0])+((*(p1+1))*r3[1])+((*(p1+2))*r3[2]);
        //if(i<10) printf("%i %f %f %f\n", i, (*p2), (*(p2+1)), (*(p2+2)));
        p1+=4;p2+=4;
      }
  }
static void rotate_spring2()
  {
    //Use some vector multiplication.
    int i=0;
    v4sf r1, r2, r3, temp;
    r1[0]=0.5;r1[1]=0.5;r1[2]=0.5;r1[3]=0.5;
    r2[0]=0.5;r2[1]=0.5;r2[2]=0.5;r2[3]=0.5;
    r3[0]=0.5;r3[1]=0.5;r3[2]=0.5;r3[3]=0.5;
    
    v4sf *p1=spring2;
    v4sf *p2=spring_rotated2;

    for(i=0;i<80;i++)
      {
        temp=(*p1)*r1;
        (*p2)[0]=temp[0]+temp[1]+temp[2];
        temp=(*p1)*r2;
        (*p2)[1]=temp[0]+temp[1]+temp[2];
        temp=(*p1)*r3;
        (*p2)[2]=temp[0]+temp[1]+temp[2];
        //if(i<10) printf("%i %f %f %f\n", i, (*p2)[0], (*p2)[1], (*p2)[2]);
        p1++;p2++;
      }
  }
