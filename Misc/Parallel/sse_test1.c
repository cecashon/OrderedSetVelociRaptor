
/* 
    Compare pointers and vectors for adding.

    gcc -Wall -msse2 -std=c11 sse_test1.c -o sse_test1
   
    For assembly output only. Look for xmm registers in the assembly text file for the
vector add function
.
    gcc -Wall -msse2 -std=c11 -S sse_test1.c -o sse_asm 

    Valgrind it.

    valgrind --leak-check=full --show-leak-kinds=all ./sse_test1

    Tested on Ubuntu16.04 with a 32-bit Intel Atom CPU N270, 1.60GHz × 2 

*/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>

//The number of floats to test.
#define num_floats 100000

//For sse2.
typedef float v4sf __attribute__ ((vector_size (16)));

//For using pointers.
struct point4f{
    float x;
    float y;
    float z;
    float w;
  };

static void vector_add();
static void pointer_add();

int main()
  {
    clock_t start1, end1, start2, end2;
    start1=clock();
    vector_add();
    end1=clock();
    printf("Vector time %f\n", (double)(end1-start1)/(double)CLOCKS_PER_SEC);

    start2=clock();
    pointer_add();
    end2=clock();
    printf("Pointer time %f\n", (double)(end2-start2)/(double)CLOCKS_PER_SEC);

    return 0;
  }
static void vector_add()
  {
    int i=0;
    v4sf q;
    q[0]=2.0f;
    q[1]=4.0f;
    q[2]=6.0f;
    q[3]=8.0f;
    
    printf("Size of v4sf %i\n", sizeof(v4sf));

    v4sf *v1=(v4sf*)aligned_alloc(16, sizeof(v4sf)*num_floats);

    if(v1!=NULL)
      {
        //initialize elements to 0.
        memset(v1, 0, sizeof(v4sf)*num_floats);

        //The pointer to move. Don't move v1 and then free it.
        v4sf *move_v1=v1;

        //Check a value.
        (*move_v1)[0]=1.0;
        (*move_v1)[1]=1.0;
        (*move_v1)[2]=1.0;
        (*move_v1)[3]=1.0;
     
        for(i=0;i<num_floats;i++)
          {
            q[0]=(float)i;
            //Vector math.
            (*move_v1)=(*move_v1)+q;
            //printf("%i %f %f %f %f\n", i, (*move_v1)[0], (*move_v1)[1], (*move_v1)[2], (*move_v1)[3]);
            move_v1++;
          }
      }
    else printf("Error: malloc returned NULL.");

    if(v1!=NULL) free(v1);
    
  }
static void pointer_add()
  {
    int i=0;
    struct point4f q;
    q.x=2.0f;
    q.y=4.0f;
    q.z=6.0f;
    q.w=8.0f;
    
    printf("Size of point4f %i\n", sizeof(struct point4f));

    struct point4f *p1=(struct point4f*)calloc(num_floats, sizeof(struct point4f));

    if(p1!=NULL)
      {
        //The pointer to move. Don't move p1 and then free it.
        struct point4f *move_p1=p1;

        //Check a value.
        (*move_p1).x=1.0;
        (*move_p1).y=1.0;
        (*move_p1).z=1.0;
        (*move_p1).w=1.0;
     
        for(i=0;i<num_floats;i++)
          {
            q.x=(float)i;
            //pointer math.
            (*move_p1).x=(*move_p1).x+q.x;
            (*move_p1).y=(*move_p1).y+q.y;
            (*move_p1).z=(*move_p1).z+q.z;
            (*move_p1).w=(*move_p1).w+q.w;
            //printf("%i %f %f %f %f\n", i, (*move_p1).x, (*move_p1).y, (*move_p1).z, (*move_p1).w);
            move_p1++;
          }
      }
    else printf("Error: malloc returned NULL.");

    if(p1!=NULL) free(p1);
  }
