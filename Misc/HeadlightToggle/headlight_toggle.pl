
#For use with introspection and the headlight_toggle widget. See headlight_toggle.c for setting it up.

use strict;
use diagnostics;
use warnings;
use Gtk3 '-init';

Glib::Object::Introspection->setup(basename => 'Headlight', version => '0.1', package => 'Headlight');

package TestToggle;
sub new
{
    my $class = shift;
    my $self = {};
    bless $self, $class;

    $self->{'window'} = Gtk3::Window->new();
    $self->{'window'}->signal_connect('delete_event' => sub { Gtk3->main_quit; });
    $self->{'window'}->set_default_size(200, 100);
    $self->{'window'}->set_title("HeadlightToggle");

    $self->{'toggle'} = Headlight::Toggle->new();

    $self->{'window'}->add($self->{'toggle'});

    return $self;
}
my $win = new TestToggle();
$win->{'window'}->show_all();
Gtk3->main;




