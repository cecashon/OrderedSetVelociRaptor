
//For use with introspection and the headlight_toggle widget. See headlight_toggle.c for setting it up.

const Lang = imports.lang;
const Gtk = imports.gi.Gtk;
const Headlight = imports.gi.Headlight; 

Gtk.init(null);

const TestToggle = new Lang.Class({
    Name: 'Clock',
    Extends: Gtk.Window,

    _init: function() {
        this.parent({title: "HeadlightToggle"});
        this.set_default_size(200, 100);

        this.toggle = new Headlight.Toggle();
        
        this.add(this.toggle);
    },
});

let win = new TestToggle();
win.connect("delete-event", Gtk.main_quit);
win.show_all();
Gtk.main();
