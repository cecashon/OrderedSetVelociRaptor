

--A headlight toggle widget. It has two states. On and off. Easy to change the drawing or colors for different types of toggle switches.

--There are some instructions in the headlight_toggle.c file for setting up introspection. Then
the widget can be used from higher level languages like Python, Perl or Java Script. 

Headlight Toggle

![ScreenShot](/Misc/HeadlightToggle/headlight_toggle1.png)

