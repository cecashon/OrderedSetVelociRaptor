
/*

    For use with headlight_toggle.c. Look in headlight_toggle_main.c for more information.

    C. Eric Cashon

*/

#ifndef __HEADLIGHT_TOGGLE_H__
#define __HEADLIGHT_TOGGLE_H__

#include<gtk/gtk.h>

G_BEGIN_DECLS

/**
 * HeadlightToggle:
 *
 * A HeadlightToggle structure.
 *
 */
struct _HeadlightToggle
{
  /*< private >*/
  GtkDrawingArea da;
};

#define HEADLIGHT_TOGGLE_TYPE headlight_toggle_get_type()

G_DECLARE_FINAL_TYPE(HeadlightToggle, headlight_toggle, HEADLIGHT, TOGGLE, GtkDrawingArea)

//Toggle state
enum
{
  HEADLIGHT_TOGGLE_OFF,
  HEADLIGHT_TOGGLE_ON
};

//Toggle direction
enum
{
  HORIZONTAL_RIGHT,
  VERTICAL_UP
};

//Toggle drawing icon.
enum
{
  HEADLIGHT_ICON,
  EMERGENCY_LIGHT_ICON,
  FAN_ICON,
  HEATER_ICON
};

//Public functions.
GtkWidget* headlight_toggle_new();
void headlight_toggle_set_state(HeadlightToggle *da, gint headlight_toggle_state);
gint headlight_toggle_get_state(HeadlightToggle *da);
void headlight_toggle_set_direction(HeadlightToggle *da, gint headlight_toggle_direction);
gint headlight_toggle_get_direction(HeadlightToggle *da);
void headlight_toggle_set_icon(HeadlightToggle *da, gint headlight_toggle_icon);
gint headlight_toggle_get_icon(HeadlightToggle *da);
//Set some toggle colors.
void headlight_toggle_set_dim_color(HeadlightToggle *da, const gchar *dim_color_string);
const gchar* headlight_toggle_get_dim_color(HeadlightToggle *da);
void headlight_toggle_set_lit_color(HeadlightToggle *da, const gchar *lit_color_string);
const gchar* headlight_toggle_get_lit_color(HeadlightToggle *da);
void headlight_toggle_set_icon_dim_color(HeadlightToggle *da, const gchar *icon_dim_color_string);
const gchar* headlight_toggle_get_icon_dim_color(HeadlightToggle *da);
void headlight_toggle_set_icon_lit_color(HeadlightToggle *da, const gchar *icon_lit_color_string);
const gchar* headlight_toggle_get_icon_lit_color(HeadlightToggle *da);

G_END_DECLS

#endif 
