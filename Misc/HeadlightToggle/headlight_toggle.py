
#For use with introspection and the headlight_toggle widget. See headlight_toggle.c for setting it up.

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Headlight', '0.1')
from gi.repository import Gtk, Headlight

class TestToggle(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="HeadlightToggle")
        self.set_default_size(200, 100)

        self.toggle=Headlight.Toggle()

        self.add(self.toggle)

win = TestToggle()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()
