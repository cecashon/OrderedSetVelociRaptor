
#For use with introspection and the multi_graph widget. See multi_graph.c for setting it up.

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Multi', '0.1')
from gi.repository import Gtk, GObject, Multi
import random

class TestGraph(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="MultiGraph")
        self.set_default_size(600, 400)

        self.graph=Multi.Graph()
        self.graph.set_rows(3)
        self.graph.set_columns(3)
        self.graph.set_draw_lines(2)
        self.graph.set_scale_dots(2)

        self.add(self.graph)

        GObject.timeout_add(300, self.animate)

    def animate(self):
        for i in range(9):
            self.graph.feed_point(i, 0.0, random.random())
        self.graph.queue_draw()
        return True 

win = TestGraph()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()
