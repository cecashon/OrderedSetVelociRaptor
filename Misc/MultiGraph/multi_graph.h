
/*

    For use with multi_graph.c. Look in multi_graph_main.c for more information.

    C. Eric Cashon

*/

#ifndef __MULTI_GRAPH_H__
#define __MULTI_GRAPH_H__

#include<gtk/gtk.h>

G_BEGIN_DECLS

/**
 * MultiGraph:
 *
 * A MultiGraph structure.
 *
 */
struct _MultiGraph
{
  /*< private >*/
  GtkDrawingArea da;
};

#define MULTI_GRAPH_TYPE multi_graph_get_type()

G_DECLARE_FINAL_TYPE(MultiGraph, multi_graph, MULTI, GRAPH, GtkDrawingArea)

GtkWidget* multi_graph_new(void);
void multi_graph_set_background_color(MultiGraph *da, const gchar *background_color_string);
const gchar* multi_graph_get_background_color(MultiGraph *da);
void multi_graph_set_font_color(MultiGraph *da, const gchar *font_color_string);
const gchar* multi_graph_get_font_color(MultiGraph *da);
void multi_graph_set_grid_color(MultiGraph *da, const gchar *grid_color_string);
const gchar* multi_graph_get_grid_color(MultiGraph *da);
void multi_graph_set_tick_color(MultiGraph *da, const gchar *tick_color_string);
const gchar* multi_graph_get_tick_color(MultiGraph *da);
void multi_graph_set_rows(MultiGraph *da, gint rows);
gint multi_graph_get_graph_rows(MultiGraph *da);
void multi_graph_set_columns(MultiGraph *da, gint columns);
gint multi_graph_get_graph_columns(MultiGraph *da);
void multi_graph_set_x_font_scale(MultiGraph *da, gint x_font_scale);
gint multi_graph_get_x_font_scale(MultiGraph *da);
void multi_graph_set_y_font_scale(MultiGraph *da, gint y_font_scale);
gint multi_graph_get_y_font_scale(MultiGraph *da);
void multi_graph_set_legend_scale(MultiGraph *da, gint y_font_scale);
gint multi_graph_get_legend_scale(MultiGraph *da);
void multi_graph_set_draw_lines(MultiGraph *da, gint draw_lines);
gint multi_graph_get_draw_lines(MultiGraph *da);
void multi_graph_set_scale_dots(MultiGraph *da, gint scale_dots);
gint multi_graph_get_scale_dots(MultiGraph *da);
void multi_graph_set_compose(MultiGraph *da, gint compose);
gint multi_graph_get_compose(MultiGraph *da);
//Set point, line, curve or rectangle color to draw with for each graph. Doesn't have a set get pair. 
void multi_graph_set_line_color(MultiGraph *da, gint graph_id, const gchar *line_color_string);
//Set the number of points to draw for each graph. This adjusts the x_ticks as well.
void multi_graph_set_points(MultiGraph *da, gint graph_id, gint points);
gint multi_graph_get_points(MultiGraph *da, gint graph_id);
//Set the number of y_ticks for the graph.
void multi_graph_set_y_ticks(MultiGraph *da, gint graph_id, gint ticks);
gint multi_graph_get_y_ticks(MultiGraph *da, gint graph_id);
//Set the max value of the y-axis.
void multi_graph_set_y_max(MultiGraph *da, gint graph_id, gdouble y_max);
gdouble multi_graph_get_y_max(MultiGraph *da, gint graph_id);
//Set the space between ticks and x-axis labels.
void multi_graph_set_tick_increment_x(MultiGraph *da, gint graph_id, gint increment);
gint multi_graph_get_tick_increment_x(MultiGraph *da, gint graph_id);
//Add a point to the start of the graph.
void multi_graph_feed_point(MultiGraph *da, gint graph_id, gdouble x, gdouble y);
//Swap the positions of two graphs.
void multi_graph_swap_graphs(MultiGraph *da, gint id1, gint id2);
//Legend labels.
void multi_graph_set_legend_label(MultiGraph *da, gint graph_id, const gchar *legend_label);
const gchar* multi_graph_get_legend_label(MultiGraph *da, gint graph_id);
void multi_graph_set_legend_show(MultiGraph *da, gboolean show_legend);
void multi_graph_set_legend_move(MultiGraph *da, gdouble x, gdouble y);

G_END_DECLS

#endif 
