
#For use with introspection and the multi_graph widget. See multi_graph.c for setting it up.

use strict;
use diagnostics;
use warnings;
use Gtk3 '-init';
use Glib;

Glib::Object::Introspection->setup(basename => 'Multi', version => '0.1', package => 'Multi');

package TestGraph;
sub new
{
    my $class = shift;
    my $self = {};
    bless $self, $class;

    $self->{'window'} = Gtk3::Window->new();
    $self->{'window'}->signal_connect('delete_event' => sub { Gtk3->main_quit; });
    $self->{'window'}->set_default_size(600, 400);
    $self->{'window'}->set_title("MultiGraph");

    $self->{'graph'} = Multi::Graph->new();
    $self->{'graph'}->set_rows(3);
    $self->{'graph'}->set_columns(3);
    $self->{'graph'}->set_draw_lines(2);
    $self->{'graph'}->set_scale_dots(2);

    $self->{'window'}->add($self->{'graph'});

    Glib::Timeout->add(300, sub
      {
        for (my $i=0; $i < 9; $i++)
          {
            $self->{'graph'}->feed_point($i, 0.0, rand());
          }   
        $self->{'graph'}->queue_draw();
        return 1;
      });

    return $self;
}
my $win = new TestGraph();
$win->{'window'}->show_all();
Gtk3->main;




