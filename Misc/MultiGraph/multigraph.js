
//For use with introspection and the multi_graph widget. See multi_graph.c for setting it up.

const Lang = imports.lang;
const Gtk = imports.gi.Gtk;
const GLib = imports.gi.GLib;
const Multi = imports.gi.Multi; 

Gtk.init(null);

const TestGraph = new Lang.Class({
    Name: 'TestGraph',
    Extends: Gtk.Window,

    _init: function() {
        this.parent({title: "MultiGraph"});
        this.set_default_size(600, 400);

        this.graph = new Multi.Graph();
        this.graph.set_rows(3);
        this.graph.set_columns(3);
        this.graph.set_draw_lines(2);
        this.graph.set_scale_dots(2);

        this.animate = function() {
            var i = 0;
            for (i = 0; i < 9; i++) { 
                this.graph.feed_point(i, 0.0, Math.random());
                this.graph.queue_draw();
            
            }
            return true;
        };

        GLib.timeout_add(GLib.PRIORITY_DEFAULT, 300, Lang.bind(this, this.animate));
        
        this.add(this.graph);
    },
});

let win = new TestGraph();
win.connect("delete-event", Gtk.main_quit);
win.show_all();
Gtk.main();
