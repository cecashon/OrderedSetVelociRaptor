

--A multigraph widget for drawing up to 16 graphs in one drawing area. It can receive and draw data for 16 different feeds and also compose the data into one graph. It can draw points, lines, curves or rectangles. This is the conversion of the four_graphs2.c program in the cairo_drawings folder into a widget. 

--In the multi_graph.c file there are some instructions for testing introspection. This will allow the C code to be used by a higher level language like Perl, Python, JavaScript, etc. There are three simple files  (multigraph.pl, multigraph.py and multigraph.js) that can be used to test if the introspection is setup and working.

--After setting up introspection, documentation can be created with with gtk-doc. This creates an html layout that has a similar look to the GTK+ documentation.

MultiGraph

![ScreenShot](/Misc/MultiGraph/multigraph.png)

